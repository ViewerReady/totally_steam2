﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaler : MonoBehaviour {
    [SerializeField]
    protected float initialScale = 150f;
    public float maxScale = 200;
    public float minScale = 30;
    public Camera cam;
    public bool playOneToOne;
    public float scale
    {
        get { return _scale; }
        set
        {
            _scale = Mathf.Clamp(value, minScale, maxScale);
#if LUMIN_UNITY
            if (Application.isEditor)
            {
                if (_scale > 6)
                {
                    _scale = 6f;
                }
            }
#endif
            SetTransformScale(_scale);
        }
    }

    public static bool isZooming
    {
        get;private set;
    }

    public static CameraScaler instance
    {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<CameraScaler>();
            }
            return _instance;
        }
    }
    private static CameraScaler _instance;

    public GameObject pivotPrefab;
    protected Transform scalePivot
    {
        get
        {
            if (!_pivot)
            {
                if(pivotPrefab)
                    _pivot = GameObject.Instantiate(pivotPrefab).transform;
                else
                    _pivot = new GameObject("ScalePivot").transform;
                pivotAudio = _pivot.GetComponent<AudioSource>();
				if (FullGameDirector.Instance) {
					_pivot.transform.SetParent (FullGameDirector.Instance.transform);
				}
            }
            return _pivot;
        }
    }
    private Transform _pivot;
    private float _scale;

    private AudioSource pivotAudio;
    public AudioClip scaleUpSound;
    public AudioClip scaleDownSound;

    private LineRenderer[] lines;

    void Awake()
    {
        _instance = this;
        if (playOneToOne) {
            //maxScale = 1f;
            //minScale = 1f;
            //initialScale = 1f;
            Shader.SetGlobalFloat("_SnowglobeOcclusionRadius", 500f);
        }
        scale = initialScale;

    }

    // Use this for initialization
    void Start() {
        lines = GetComponentsInChildren<LineRenderer>();
        if (cam == null) cam = GetComponentInChildren<Camera>();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="adjustForCamOffset">if the localPosition of the camera is not vector3.zero, this will adjust the pivot position so that the player will end up standing on the desired pivot</param>
    public void SetPivotPosition(Vector3 pos, bool adjustForCamOffset = true)
    {
        if (adjustForCamOffset)
        {
            pos -= cam.transform.localPosition;
        }
        scalePivot.transform.position = pos;
    }


    public void SetPivotToReachHeadPos(Vector3 targetHeadPos)
    {
        float endScale = 1;
        float endHeight = cam.transform.position.y * (endScale/scale);
        targetHeadPos.y = endHeight;

        Vector3 currentCamRootPos = transform.position;
        Vector3 targetCamRootPos = (((currentCamRootPos - cam.transform.position) * endScale) / scale) + targetHeadPos;
        float dScale = scale / endScale;

        Vector3 pivotPos = -(currentCamRootPos - targetCamRootPos * dScale)/(dScale -1);
        pivotPos.y = (Vector3.zero).y;
        if (pivotPos.x == Mathf.Infinity || pivotPos.y == Mathf.Infinity || pivotPos.z == Mathf.Infinity) return;
        scalePivot.transform.position = pivotPos;
    }

    protected void SetTransformScale(float newScale)
    {
        if (lines == null) {
            lines = GetComponentsInChildren<LineRenderer>();
        }
        Vector3 last_pos = cam.transform.position;
        ScaleAround(this.gameObject, scalePivot.position, Vector3.one * newScale);
        //this.transform.localScale = Vector3.one * newScale;
        cam.ResetProjectionMatrix();
        //cam.nearClipPlane = .37f;
        //       Debug.Log(cam.nearClipPlane + " " + cam.farClipPlane);
        if (lines == null) lines = GetComponentsInChildren<LineRenderer>();

        foreach (LineRenderer line in lines)
        {
            line.startWidth = newScale * .01f;
            line.endWidth = newScale * .01f;
        }
        //Vector3 newPos = new Vector3(last_pos.x, transform.position.y, last_pos.z);
        //cam.transform.position = newPos;

    }

    void PlaySoundOnPivot(AudioClip sound)
    {
        if (!pivotAudio) return;
        if (sound == null) return;

        pivotAudio.clip = sound;
        pivotAudio.Play();
    }

    public void ScaleAround(GameObject target, Vector3 pivot, Vector3 newScale)
    {
        Vector3 A = target.transform.localPosition;
        Vector3 B = pivot;

        Vector3 C = A - B; // diff from object pivot to desired pivot/origin

        float RS = newScale.x / target.transform.localScale.x; // relative scale factor

        // calc final position post-scale
        Vector3 FP = B + C * RS;

        // finally, actually perform the scale/translation
        target.transform.localScale = newScale;
        target.transform.localPosition = FP;
    }

    public void SmoothZoom(float endScale)
    {
        Debug.Log("Begin Smooth Zoom");
        StopCoroutine("SmoothZoomRoutine");
        pivotAudio.Stop();
        StartCoroutine("SmoothZoomRoutine", endScale);
    }

    public void SmoothZoomIn()
    {
        SmoothZoom(minScale);
    }

    public void SmoothZoomOut()
    {
        SmoothZoom(maxScale);
    }


    public float smoothTime = 0.3F;
    private float scaleVelocity = 0;
    IEnumerator SmoothZoomRoutine(float endScale)
    {
        isZooming = true;

        yield return null;
        if (Mathf.Abs(scale - endScale) > 1)
        {
            pivotAudio.Play();
        }


        scaleVelocity = 0;

        Vector3 camLocalPos_initial = cam.transform.localPosition;
        camLocalPos_initial.y = 0;


        while (Mathf.Abs(endScale - scale) > .01f)
        {
            scale = Mathf.SmoothDamp(scale, endScale, ref scaleVelocity, smoothTime);// 100, Time.unscaledDeltaTime);
            Debug.DrawLine(scalePivot.transform.position, scalePivot.transform.position + Vector3.up * 5, Color.yellow);
            yield return null;
        }
        isZooming = false;
    }
}
