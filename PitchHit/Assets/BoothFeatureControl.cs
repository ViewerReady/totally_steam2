﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoothFeatureControl : MonoBehaviour
{

    public bool _simpleBooth;

    public GameObject _minimap;
    public GameObject _playerBios;
    public GameObject _roster;


    public GameObject[] broadcastOnlyObjects;

    private void Awake()
    {
        if (_simpleBooth)
        {
            _minimap.SetActive(false);
            _playerBios.SetActive(false);
            _roster.SetActive(false);
        }
    }

    public void SetBroadcastOnlyObjectsActive(bool active)
    {
        if (_simpleBooth) active = false;
        foreach (GameObject go in broadcastOnlyObjects)
        {
            go.SetActive(active);
        }
    }
}
