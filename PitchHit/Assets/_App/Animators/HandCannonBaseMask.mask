%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: HandCannonBaseMask
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: turbine
    m_Weight: 1
  - m_Path: turbine/Blocker
    m_Weight: 1
  - m_Path: turbine/Blocker002
    m_Weight: 1
  - m_Path: turbine/Cannon Tube
    m_Weight: 1
  - m_Path: turbine/fatblade007
    m_Weight: 1
  - m_Path: turbine/lite
    m_Weight: 1
  - m_Path: turbine/meter mask
    m_Weight: 0
  - m_Path: turbine/meter rainbow
    m_Weight: 0
