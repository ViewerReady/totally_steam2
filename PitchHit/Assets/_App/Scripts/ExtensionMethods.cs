﻿using UnityEngine;

public static class ExtensionMethods
{
    private static Quaternion m_lastRotation;
    private static Vector3 m_lastPosition;

    public static Quaternion LookAtSmoothly(this Transform transform, Vector3 target, float speed, Vector3 upwards)
    {
        Vector3 direction = (target - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction, upwards);

        Quaternion tempRotation = transform.localRotation;

        transform.rotation = Quaternion.Lerp(m_lastRotation, lookRotation, speed);
        m_lastRotation = transform.rotation;
        return transform.rotation *= tempRotation;
    }

    public static void LookAtSmoothly(this Transform transform, Vector3 target, float speed, Vector3 upwards, bool limitRotation, float clampValue)
    {
        Quaternion targetRotation = Quaternion.identity;
        Vector3 direction = (target - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction, upwards);

        Quaternion tempRotation = transform.localRotation;        

        if (limitRotation == true)
        {
            lookRotation = new Quaternion(0, 0, Mathf.Clamp(tempRotation.z, -clampValue, clampValue), tempRotation.w);
        }
        transform.rotation = Quaternion.Lerp(m_lastRotation, lookRotation, speed);
        m_lastRotation = transform.rotation;
        transform.rotation *= tempRotation;

        if (Quaternion.Angle(lookRotation, tempRotation) <= 90)
        {
            targetRotation = lookRotation;
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);
    }

    public static void RotateTowardsSmoothly(this Transform transform, float angle, float angleOffset, float speed)
    {
        Vector3 euler = transform.eulerAngles;        
        euler.y = Mathf.MoveTowardsAngle(euler.y, angle + angleOffset, speed * Time.deltaTime);
        transform.eulerAngles = euler;
    }

    public static void MoveTowardsSmoothly(this Transform transform, Vector3 targetPosition, float movementSpeed)
    {
        transform.position = Vector3.Lerp(m_lastPosition, targetPosition, Time.deltaTime * movementSpeed);
        m_lastPosition = transform.position;
    }

    public static void MoveTowardsSmoothly(this Transform transform, Vector3 targetPosition, float movementSpeed, bool canLimitMovement, float clampValue)
    {
        if (canLimitMovement == true)
        {
            targetPosition = new Vector3(Mathf.Clamp(targetPosition.x, -clampValue, clampValue),
                targetPosition.y,
                Mathf.Clamp(targetPosition.z, -clampValue, clampValue));
        }
        transform.position = Vector3.Lerp(m_lastPosition, targetPosition, Time.deltaTime * movementSpeed);

        m_lastPosition = transform.position;
    }
}
