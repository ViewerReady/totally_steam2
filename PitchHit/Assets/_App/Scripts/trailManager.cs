﻿using UnityEngine;
using System.Collections;

public class trailManager : MonoBehaviour {



	//public float maxVelocityColor = 50f;

	public bool trackVelocity = false;

	private bool triggered;

	public float[] trailThresholds;

	public TrailRenderer[] trails;

	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		//for (int i = 0; i < trails.Length; i++) {
		//	trails [i].enabled = false;
		//}
		triggered = false;
		trackVelocity = false;

        /*
        foreach(TrailRenderer t in trails)
        {
            t.startWidth *= WorldScaler.worldScale;
            t.endWidth *= WorldScaler.worldScale;
            t.minVertexDistance *= WorldScaler.worldScale;
        }
        */
        }

	IEnumerator disableAtLowVelocity(int trailIndex){
		float timeToFade = 2f;

		while (trailIndex > 0) {
			yield return new WaitForSeconds(0.2f);
			if (rb.velocity.magnitude < trailThresholds [trailIndex - 1]) {	//if we should display the next step down in trails
				trails[trailIndex].enabled = false;
				trailIndex--;
				trails [trailIndex].enabled = true;
			}
		}

		TrailRenderer trail = trails [0];
		float originalTime = trail.time;
		for (float i = trail.time; i > 0; i-=(Time.deltaTime/timeToFade)) {
			trail.time = i;
			yield return null;
		}
		trail.time = 0;
		trail.enabled = false;
		yield return null;
		trail.time = originalTime;


	}

    public void ClearAll()
    {
        foreach(TrailRenderer t in trails)
        {
            t.Clear();
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
		if (!triggered && trackVelocity) {
			if (rb.velocity.magnitude > trailThresholds[0]) {// *WorldScaler.worldScale) {
				//Debug.Log ("Enabling trail with");
				//Debug.Log (rb.velocity.magnitude);
				int thresholdIndex = -1;
				//find the highest trail we can do
				for (int i = 1; i < trailThresholds.Length; i++) {
					if (i == (trailThresholds.Length - 1)) {
						trails [i].enabled = true;
						thresholdIndex = i;
						break;
					}else{
						if (rb.velocity.magnitude < trailThresholds[i]) {// *WorldScaler.worldScale) {
							trails [i - 1].enabled = true;
							thresholdIndex = i - 1;
							break;
						}
					}
				}

				if (thresholdIndex != -1) {
					StartCoroutine (disableAtLowVelocity (thresholdIndex));
				} else {
					Debug.LogWarning("Error with the threshold on the trail for this ball");
				}
				triggered = true;
			}
		}
	}

}
