﻿using UnityEngine;
using System.Collections;
using System;

public class SteamworksAchievementTrigger : MonoBehaviour {

#if STEAM_VR
    SteamworksClientManager steamworksMgr;

    public SteamworksClientManager.Achievement achievement;

    // Use this for initialization
    void Start () {
        steamworksMgr = GameObject.Find("GameController").GetComponent<SteamworksClientManager>();
	}
	
	// Update is called once per frame
	//void Update () {
	//
	//}

    void OnCollisionEnter()
    {
        //Debug.Log("Unlocking achievement based on trigger: " + Enum.GetName(typeof(SteamworksClientManager.Achievement), achievement));
        //steamworksMgr.UnlockAchievement((int)achievement);
        
        // If there's no sound effect, we need to call score method
        // TODO: Refactor on trigger methods to not be spaghet
        if (GetComponent<playSoundEffect>() == null)
        {
            //GetComponent<pointsOnShatter>().PostSplit();
        }
        else
        {
            GetComponent<playSoundEffect>().playEffect();
        }
    }
#endif
}
