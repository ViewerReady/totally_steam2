﻿using UnityEngine;
using System.Collections;
#if STEAM_VR
using Steamworks;
#endif
using System;

public class SteamworksClientManager : MonoBehaviour {
#if STEAM_VR
    public class Achievement_t
    {
        public Achievement m_eAchievementID;
        public string m_strName;
        public string m_strDescription;
        public bool m_bAchieved;
        public int m_iIconImage; // achieved or not, separate images

        public Achievement_t(Achievement achievement, string name, string desc)
        {
            m_eAchievementID = achievement;
            m_strName = name;
            m_strDescription = desc;
        }
    }

    // Debug mode creates GUI interface in Unity Editor to make Steamworks calls manually
    public bool DebugMode = false;

    // Our GameID
    private CGameID m_GameID;

    // Values
    private int m_PlayerScore;

    // Did we get the stats from Steam?
    private bool m_bRequestedStats;
    private bool m_bStatsValid;

    // Should we store stats this frame?
    private bool m_bStoreStats;

    // Achievements
    public enum Achievement : int
    {
        BALLS_50,
        BALLS_100,
        BALLS_250,
        BALLS_500,
        BALLS_1000,
        BALLS_2500,
        BALLS_5000,
        GONG_SHOW,
        FAR_CAR,
        PIGS_FLY,
        BRIGHT_IDEA,
        WINDOW_WASHING,
        MELON_MASHER
    };

    private Achievement_t[] m_Achievements = new Achievement_t[]
    {
        new Achievement_t(Achievement.BALLS_50, "50 Balls", "Hit 50 balls total"),
        new Achievement_t(Achievement.BALLS_100, "100 Balls", "Hit 100 balls total"),
        new Achievement_t(Achievement.BALLS_250, "250 Balls", "Hit 250 balls total"),
        new Achievement_t(Achievement.BALLS_500, "500 Balls", "Hit 500 balls total"),
        new Achievement_t(Achievement.BALLS_1000, "1000 Balls", "Hit 1000 balls total"),
        new Achievement_t(Achievement.BALLS_2500, "2500 Balls", "Hit 2500 balls total"),
        new Achievement_t(Achievement.BALLS_5000, "5000 Balls", "Hit 5000 balls total"),
        new Achievement_t(Achievement.GONG_SHOW, "Gong Show", "Hits from the gong!"),
        new Achievement_t(Achievement.FAR_CAR, "Far Car", "Can you hit it?"),
        new Achievement_t(Achievement.PIGS_FLY, "Flying Pigs", "Frying Bacon with Baseballs"),
        new Achievement_t(Achievement.BRIGHT_IDEA, "BRIGHT IDEA!", "Your bulb skills are smashing!"),
        new Achievement_t(Achievement.WINDOW_WASHING, "WINDOW WASHER", "that's one way to do it"),
        new Achievement_t(Achievement.MELON_MASHER, "MELON MASHER", "This achievement taste so good"),

    };

    // Stat names and details
    string[] statApiNames = {
        "num_games_played",
        "num_balls_hit"
    };
    // Current game stats
    private int m_numBallsHit;

    // Persisted game stats
    private int m_numGamesPlayed;

    private SteamLeaderboard_t m_SteamLeaderboard;
    private SteamLeaderboardEntries_t m_SteamLeaderboardEntries;

    protected Callback<UserStatsReceived_t> m_UserStatsReceived;
    protected Callback<UserStatsStored_t> m_UserStatsStored;
    protected Callback<UserAchievementIconFetched_t> m_UserAchievementIconFetched;

    //private CallResult<UserStatsReceived_t> UserStatsReceived;
    //private CallResult<LeaderboardFindResult_t> LeaderboardFindResult;
    //private CallResult<LeaderboardScoresDownloaded_t> LeaderboardScoresDownloaded;
    //private CallResult<LeaderboardScoresDownloaded_t> LeaderboardScoresSelfDownloaded;
    //private CallResult<LeaderboardScoreUploaded_t> LeaderboardScoreUploaded;
    //private CallResult<LeaderboardUGCSet_t> LeaderboardUGCSet;

    private Texture2D m_Icon;

    void Start()
    {
        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            CSteamID userId = SteamUser.GetSteamID();
            Debug.Log(name);

            m_PlayerScore = 500;

            // Cache the GameID for use in the Callbacks
            m_GameID = new CGameID(SteamUtils.GetAppID());
            

            m_UserStatsReceived = Callback<UserStatsReceived_t>.Create(OnUserStatsReceived);
            m_UserStatsStored = Callback<UserStatsStored_t>.Create(OnUserStatsStored);
            m_UserAchievementIconFetched = Callback<UserAchievementIconFetched_t>.Create(OnUserAchievementIconFetched);

            // These need to be reset to get the stats upon an Assembly reload in the Editor.
            m_bRequestedStats = false;
            m_bStatsValid = false;

            //UserStatsReceived = CallResult<UserStatsReceived_t>.Create(OnUserStatsReceived);
            //LeaderboardFindResult = CallResult<LeaderboardFindResult_t>.Create(OnLeaderboardFindResult);
           // LeaderboardScoresDownloaded = CallResult<LeaderboardScoresDownloaded_t>.Create(OnLeaderboardScoresDownloaded);
            // TODO: Identify player in leaderboards
            // LeaderboardScoresSelfDownloaded = CallResult<LeaderboardScoresDownloaded_t>.Create(OnLeaderboardScoresSelfDownloaded);
            //LeaderboardScoreUploaded = CallResult<LeaderboardScoreUploaded_t>.Create(OnLeaderboardScoreUploaded);

            // Get leaderboard
            //SteamAPICall_t leaderboardHandle = SteamUserStats.FindLeaderboard("Top Scores");
            //LeaderboardFindResult.Set(leaderboardHandle);
            //print("FindLeaderboard(\"Top Scores\") - " + leaderboardHandle);

            /*
            CSteamID[] selfUser = new CSteamID[1];
            selfUser[0] = SteamUser.GetSteamID();
            SteamAPICall_t leaderboardSelfHandle = SteamUserStats.DownloadLeaderboardEntriesForUsers(m_SteamLeaderboard, selfUser, 1);
            LeaderboardScoresSelfDownloaded.Set(leaderboardSelfHandle);
            */
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!SteamManager.Initialized)
            return;

        // If Steam is up, get our stats
        if (!m_bRequestedStats)
        {
            if (!SteamManager.Initialized)
            {
                m_bRequestedStats = true;
                return;
            }

            bool bSuccess = SteamUserStats.RequestCurrentStats();

            // Should only return false if not logged in, but handle
            // case where it's false anyway.
            m_bRequestedStats = bSuccess;
        }

        // Stop if stats are invalid
        if (!m_bStatsValid)
            return;

        // Evaluate achievements
        foreach (Achievement_t achievement in m_Achievements)
        {
            if (achievement.m_bAchieved)
                continue;
            /*
            switch (achievement.m_eAchievementID)
            {
                case Achievement.BALLS_50:
                    if (m_numBallsHit >= 50)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_100:
                    if (m_numBallsHit >= 100)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_250:
                    if (m_numBallsHit >= 250)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_500:
                    if (m_numBallsHit >= 500)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_1000:
                    if (m_numBallsHit >= 1000)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_2500:
                    if (m_numBallsHit >= 2500)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
                case Achievement.BALLS_5000:
                    if (m_numBallsHit >= 5000)
                    {
                        UnlockAchievement(achievement);
                    }
                    break;
            }
            */
        }
        
        // Should we store stats this frame?
        if (m_bStoreStats)
        {
            // Debug.Log(string.Format("Storing stats: Games played: {0}, balls hit: {1}", m_numGamesPlayed, m_numBallsHit));

            SteamUserStats.SetStat(statApiNames[0], m_numGamesPlayed);  // num_games_played
            SteamUserStats.SetStat(statApiNames[1], m_numBallsHit);     // num_balls_hit

            bool bSuccess = SteamUserStats.StoreStats();

            // If this failed, we never sent anything to the server, try
            // again later.
            m_bStoreStats = !bSuccess;
        }
    }

    /**
     * Unlock an achievement by its index in the achievement list.
     * For now, this requires the enum list to match the contents
     * of m_Achievements.
     **/
    public void UnlockAchievement(int achievementIndex)
    {
        UnlockAchievement(m_Achievements[achievementIndex]);
    }

    /**
     * Unlock an achievement by its object in m_Achievements.
     **/
    public bool UnlockAchievement(Achievement_t ach)
    {
        ach.m_bAchieved = true;

        // Icon may change once unlocked
        ach.m_iIconImage = 0;

        // Count it
        return SteamUserStats.SetAchievement(ach.m_eAchievementID.ToString());
    }

    bool ClearAchievement(Achievement_t ach)
    {
        return SteamUserStats.ClearAchievement(ach.m_eAchievementID.ToString());
    }

    public void EndGame(int finalScore, int numBallsHit)
    {
        //CommitPlayerScore(finalScore);
        IncrementGamesPlayed();
        AddBallsHit(numBallsHit);

        // Trigger a stats save on next frame
        m_bStoreStats = true;
    }
	/*
    public void CommitPlayerScore(int value)
    {
        m_PlayerScore = value;

        if (!SteamManager.Initialized)
        {
            print("Steamworks not initialized!");
            return;
        }

        SteamAPICall_t leaderboardUploadHandle = SteamUserStats.UploadLeaderboardScore(m_SteamLeaderboard, ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, m_PlayerScore, null, 0);
        LeaderboardScoreUploaded.Set(leaderboardUploadHandle);
        print("UploadLeaderboardScore(" + m_SteamLeaderboard + ", ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, " + m_PlayerScore + ", null, 0) - " + leaderboardUploadHandle);
    }
*/
    public void IncrementGamesPlayed()
    {
        m_numGamesPlayed++;
    }

    public void AddBallsHit(int value)
    {
        m_numBallsHit += value;
    }
	/*
    void OnGUI()
    {
        if (!DebugMode)
            return;

        GUILayout.BeginArea(new Rect(Screen.width - 200, 0, 200, Screen.height));
        GUILayout.Label("Variables:");
        GUILayout.Label("m_SteamLeaderboard: " + m_SteamLeaderboard);
        GUILayout.Label("m_SteamLeaderboardEntries: " + m_SteamLeaderboardEntries);
        GUILayout.Label("m_PlayerScore: " + m_PlayerScore);
        GUILayout.Label("m_Icon:");
        GUILayout.Label(m_Icon);
        GUILayout.EndArea();

        RenderSteamInfo();
    }
*/
    private void RenderSteamInfo()
    {
        if (GUILayout.Button("Clear achievements"))
        {
            foreach(Achievement_t ach in m_Achievements)
            {
                bool ret = ClearAchievement(ach);

                if (!ret)
                {
                    print("Error clearing achievement: " + ach.m_eAchievementID);
                }
                else
                {
                    print("Cleared achievement: " + ach.m_eAchievementID);
                }
            }

            m_bStoreStats = true;
        }

        if (GUILayout.Button("Reset all user stats"))
        {
            SteamUserStats.ResetAllStats(true);
        }
		/*
        if (GUILayout.Button("FindOrCreateLeaderboard(\"Top Scores\", k_ELeaderboardSortMethodAscending, k_ELeaderboardDisplayTypeNumeric)"))
        {
            SteamAPICall_t handle = SteamUserStats.FindOrCreateLeaderboard("Top Scores", ELeaderboardSortMethod.k_ELeaderboardSortMethodAscending, ELeaderboardDisplayType.k_ELeaderboardDisplayTypeNumeric);
            LeaderboardFindResult.Set(handle);
            print("FindOrCreateLeaderboard(\"Top Scores\", ELeaderboardSortMethod.k_ELeaderboardSortMethodAscending, ELeaderboardDisplayType.k_ELeaderboardDisplayTypeNumeric) - " + handle);
        }
		*/
		/*
        if (GUILayout.Button("FindLeaderboard(\"Top Scores\")"))
        {
            SteamAPICall_t handle = SteamUserStats.FindLeaderboard("Top Scores");
            LeaderboardFindResult.Set(handle);
            print("FindLeaderboard(\"Top Scores\") - " + handle);
        }
		*/
		/*
        // Spams SteamAPI Warnings that the SteamLeaderboard does not exist.
        if (m_SteamLeaderboard != new SteamLeaderboard_t(0))
        {
            GUILayout.Label("GetLeaderboardName(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardName(m_SteamLeaderboard));
            GUILayout.Label("GetLeaderboardEntryCount(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardEntryCount(m_SteamLeaderboard));
            GUILayout.Label("GetLeaderboardSortMethod(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardSortMethod(m_SteamLeaderboard));
            GUILayout.Label("GetLeaderboardDisplayType(m_SteamLeaderboard) : " + SteamUserStats.GetLeaderboardDisplayType(m_SteamLeaderboard));
        }
        else
        {
            GUILayout.Label("GetLeaderboardName(m_SteamLeaderboard) : ");
            GUILayout.Label("GetLeaderboardEntryCount(m_SteamLeaderboard) : ");
            GUILayout.Label("GetLeaderboardSortMethod(m_SteamLeaderboard) : ");
            GUILayout.Label("GetLeaderboardDisplayType(m_SteamLeaderboard) : ");
        }
		*/
		/*
        if (GUILayout.Button("DownloadLeaderboardEntries(m_SteamLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 5)"))
        {
            SteamAPICall_t handle = SteamUserStats.DownloadLeaderboardEntries(m_SteamLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 5);
            LeaderboardScoresDownloaded.Set(handle);
            print("DownloadLeaderboardEntries(" + m_SteamLeaderboard + ", ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 5) - " + handle);
        }
		*/
		/*
        if (GUILayout.Button("DownloadLeaderboardEntriesForUsers(m_SteamLeaderboard, Users, Users.Length)"))
        {
            CSteamID[] Users = { SteamUser.GetSteamID() };
            SteamAPICall_t handle = SteamUserStats.DownloadLeaderboardEntriesForUsers(m_SteamLeaderboard, Users, Users.Length);
            LeaderboardScoresDownloaded.Set(handle);
            print("DownloadLeaderboardEntriesForUsers(" + m_SteamLeaderboard + ", " + Users + ", " + Users.Length + ") - " + handle);
        }
		*/
		/*
        if (GUILayout.Button("GetDownloadedLeaderboardEntry(m_SteamLeaderboardEntries, 0, out LeaderboardEntry, null, 0)"))
        {
            LeaderboardEntry_t LeaderboardEntry;
            bool ret = SteamUserStats.GetDownloadedLeaderboardEntry(m_SteamLeaderboardEntries, 0, out LeaderboardEntry, null, 0);
            print("GetDownloadedLeaderboardEntry(" + m_SteamLeaderboardEntries + ", 0, out LeaderboardEntry, null, 0) - " + ret + " -- " + LeaderboardEntry.m_steamIDUser + " -- " + LeaderboardEntry.m_nGlobalRank + " -- " + LeaderboardEntry.m_nScore + " -- " + LeaderboardEntry.m_cDetails + " -- " + LeaderboardEntry.m_hUGC);
        }
		*/
		/*
        if (GUILayout.Button("UploadLeaderboardScore(m_SteamLeaderboard, k_ELeaderboardUploadScoreMethodForceUpdate, (int)m_FeetTraveledStat, ScoreDetails, 0)"))
        {
            SteamAPICall_t handle = SteamUserStats.UploadLeaderboardScore(m_SteamLeaderboard, ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodForceUpdate, m_PlayerScore, null, 0);
            LeaderboardScoreUploaded.Set(handle);
            print("UploadLeaderboardScore(" + m_SteamLeaderboard + ", ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodForceUpdate, " + m_PlayerScore + ", null, 0) - " + handle);
        }
		*/
        //if (GUILayout.Button("SteamUserStats.AttachLeaderboardUGC(m_SteamLeaderboard, RemoteStorageTest.m_UGCHandle)"))
        //{
        //    SteamAPICall_t handle = SteamUserStats.AttachLeaderboardUGC(m_SteamLeaderboard, UGCHandle_t.Invalid);
        //    LeaderboardUGCSet.Set(handle);
         //   print("SteamUserStats.AttachLeaderboardUGC(" + m_SteamLeaderboard + ", " + UGCHandle_t.Invalid + ") - " + handle);
        //}
    }

    private void OnUserStatsReceived(UserStatsReceived_t pCallback)
    {
        if (!SteamManager.Initialized)
            return;

        Debug.Log("[" + UserStatsReceived_t.k_iCallback + " - UserStatsReceived] - " + pCallback.m_nGameID + " -- " + pCallback.m_eResult + " -- " + pCallback.m_steamIDUser);
        if (EResult.k_EResultOK == pCallback.m_eResult)
        {
            m_bStatsValid = true;

            // Load stats
            SteamUserStats.GetStat(statApiNames[0], out m_numGamesPlayed);
            SteamUserStats.GetStat(statApiNames[1], out m_numBallsHit);

            Debug.Log(string.Format("Stats loaded: Games played: {0}, balls hit: {1}", m_numGamesPlayed, m_numBallsHit));

            // Load achievements
            foreach (Achievement_t ach in m_Achievements)
            {
                //bool ret = false;
                //SteamUserStats.GetAchievement(Enum.GetName(typeof(Achievement), i), out ret);
                bool ret = SteamUserStats.GetAchievement(ach.m_eAchievementID.ToString(), out ach.m_bAchieved);

                if (ret)
                {
                    ach.m_strName = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "name");
                    ach.m_strDescription = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "desc");
                }
                else
                { 
                    Debug.LogWarning("SteamUserStats.GetAchievement failed for Achievement " + ach.m_eAchievementID);
                }
            }
        }
    }

    // CallResult version for: SteamUserStats.RequestUserStats() (Other Players)
    private void OnUserStatsReceived(UserStatsReceived_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + UserStatsStored_t.k_iCallback + " - UserStatsReceived] - " + pCallback.m_nGameID + " -- " + pCallback.m_eResult + " -- " + pCallback.m_steamIDUser);

    }

    private void OnUserStatsStored(UserStatsStored_t pCallback)
    {
        Debug.Log("[" + UserStatsStored_t.k_iCallback + " - UserStatsStored] - " + pCallback.m_nGameID + " -- " + pCallback.m_eResult);

        // we may get callbacks for other games' stats arriving, ignore them
        if ((ulong)m_GameID == pCallback.m_nGameID)
        {
            if (EResult.k_EResultOK == pCallback.m_eResult)
            {
                Debug.Log("StoreStats - success");
            }
            else if (EResult.k_EResultInvalidParam == pCallback.m_eResult)
            {
                // One or more stats we set broke a constraint. They've been reverted,
                // and we should re-iterate the values now to keep in sync.
                Debug.Log("StoreStats - some failed to validate");
                // Fake up a callback here so that we re-load the values.
                UserStatsReceived_t callback = new UserStatsReceived_t();
                callback.m_eResult = EResult.k_EResultOK;
                callback.m_nGameID = (ulong)m_GameID;
                OnUserStatsReceived(callback);
            }
            else
            {
                Debug.Log("StoreStats - failed, " + pCallback.m_eResult);
            }
        }
    }
	/*
    private void OnLeaderboardFindResult(LeaderboardFindResult_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LeaderboardFindResult_t.k_iCallback + " - LeaderboardFindResult] - " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_bLeaderboardFound);

        if (pCallback.m_bLeaderboardFound != 0)
        {
            m_SteamLeaderboard = pCallback.m_hSteamLeaderboard;
        }

        SteamAPICall_t leaderboardEntriesHandle = SteamUserStats.DownloadLeaderboardEntries(m_SteamLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, -10, 9);
        LeaderboardScoresDownloaded.Set(leaderboardEntriesHandle);
        print("DownloadLeaderboardEntries(" + m_SteamLeaderboard + ", ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, -10, 9) - " + leaderboardEntriesHandle);
    }
*/
    private void OnUserAchievementIconFetched(UserAchievementIconFetched_t pCallback)
    {
        Debug.Log("[" + UserAchievementIconFetched_t.k_iCallback + " - UserAchievementIconFetched_t] - " + pCallback.m_nGameID + " -- " + pCallback.m_rgchAchievementName + " -- " + pCallback.m_bAchieved + " -- " + pCallback.m_nIconHandle);
    }
	/*
    private void OnLeaderboardScoresDownloaded(LeaderboardScoresDownloaded_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LeaderboardScoresDownloaded_t.k_iCallback + " - LeaderboardScoresDownloaded] - " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_hSteamLeaderboardEntries + " -- " + pCallback.m_cEntryCount);
        m_SteamLeaderboardEntries = pCallback.m_hSteamLeaderboardEntries;

        string leaderboardText = "YOUR POSITION\n\n";
        for (int i=0; i < pCallback.m_cEntryCount; i++)
        {
            LeaderboardEntry_t entry = new LeaderboardEntry_t();
            int[] details = new int[1];
            SteamUserStats.GetDownloadedLeaderboardEntry(m_SteamLeaderboardEntries, i, out entry, details, 1);

            //print(string.Format("Leaderboard entry {0}: {1} - {2}", entry.m_nGlobalRank, SteamFriends.GetFriendPersonaName(entry.m_steamIDUser), entry.m_nScore));
            leaderboardText += string.Format("{0}: {1} - {2}\n", entry.m_nGlobalRank, SteamFriends.GetFriendPersonaName(entry.m_steamIDUser), entry.m_nScore);
        }
        GetComponent<LeaderboardManager>().SetYourPositionText(leaderboardText);
    }
*/
	/*
    private void OnLeaderboardScoreUploaded(LeaderboardScoreUploaded_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LeaderboardScoreUploaded_t.k_iCallback + " - LeaderboardScoreUploaded] - " + pCallback.m_bSuccess + " -- " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_nScore + " -- " + pCallback.m_bScoreChanged + " -- " + pCallback.m_nGlobalRankNew + " -- " + pCallback.m_nGlobalRankPrevious);
    }
	*/
	/*
    private void OnLeaderboardUGCSet(LeaderboardUGCSet_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LeaderboardUGCSet_t.k_iCallback + " - LeaderboardUGCSet] - " + pCallback.m_eResult + " -- " + pCallback.m_hSteamLeaderboard);
    }
	*/
#endif
}
