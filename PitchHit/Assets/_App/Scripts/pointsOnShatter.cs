﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class pointsOnShatter : MonoBehaviour {
    public UnityEvent OnShatter;
    public bool shouldAwardPoints = true;
	public int points = 5;

    // ShatterToolkit.ShatterTool shatterTool;
    private FracturedObject fractureEngine;


	// Use this for initialization
	//void Awake() {
        /*
        shatterTool = GetComponent<ShatterToolkit.ShatterTool> ();
		if (shatterTool.IsFirstGeneration) {
			score = (StatsController)FindObjectOfType (typeof(StatsController));
		}
        */
   // }

    public void PostSplit(){
        //so random breaks at start don't count.
        if (Time.timeSinceLevelLoad < .1f)
        {
            return;
        }

        if (shouldAwardPoints)
        {
            //Debug.Log("should award points");
            if (ScoreboardController.instance)
            {
                //Debug.Log("there's an instance.");
                //ScoreboardController.instance.scorePopup(points, transform.position);

                if (ScoreboardController.instance.scorable)
                {
                    ScoreboardController.instance.AddScore(points);

                    //Debug.Log("and it's scorable!");
                    OnShatter.Invoke();
                }
            }
            
        }
	}

}
