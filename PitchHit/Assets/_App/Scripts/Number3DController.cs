﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Number3DController : MonoBehaviour {
    public GameObject[] numbers;
    public int currentDisplayedNumber;

    public void DisplayNumber(int i)
    {
        if(i==currentDisplayedNumber || numbers.Length==0)
        {
            return;
        }
        numbers[currentDisplayedNumber].SetActive(false);

        i = Mathf.Clamp(i, 0, numbers.Length-1);
        currentDisplayedNumber = i;
        numbers[i].SetActive(true);

    }
}
