﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//#if STEAM_VR
using Valve.VR.InteractionSystem;
using Valve.VR;
//#endif

public class ballController : MonoBehaviour
{
    SteamVR_Action_Boolean ballSpawnAction;
    public static bool readyForBalls = true;
    private static int _maxBallsAtOnce;
    public int maxBallsAtOnce = 25;
    public GameObject ballPrefab;
    public GameObject teeHandle;
    public GameObject teeBody;
    public GameObject pitcher;
    public GameObject pitcherTarget;
    public GameObject ballBucket;
    public Transform bucketCenter;
    public HumanGloveController glove;
    public RealButton ballsButton;
    //public MenuButton ballsMenuButton;
    private spawnBall[] bucketSpawners;
    public bool spawnTeeBallAutomatically = true;
    public bool spawnTeeBallsConstantly = false;
    public bool spawnPitchedBallsConstantly = false;
    [HideInInspector]
    public bool genericTrackerHasBeenSeen;

    public static ballController instance;
    //public bool offsetTeeWhenRoomSmall = true;
    //public float tinyAreaWidth = 2.5f;

    //HmdQuad_t playBounds = new HmdQuad_t();
    private AudioSource bucketSound;
    //public bool forceLefty;
    //public bool forceRighty;
    //private spawnBall.ballType _currBallType;
    //public spawnBall.ballType currBallType{
    //	get{
    //		return _currBallType;
    //	}
    //	set{
    //		changeBalls (value);
    //		_currBallType = value;
    //	}
    //}

    public enum ballServiceType
    {
        tee, pitcher, self, none
    }

    public ballServiceType currentServiceType = ballServiceType.tee;
    public static float ballRadius;
    public static hittable currentlyActiveBall;
    //private GameObject[] balls;
    public static Queue<hittable> activeBalls;
    private static List<hittable> ballsInBucket;

    //public int ballsUsed;

    //private difficultyController difficulty;

    public pitchBall pitcherController;
    public spawnBall teeController;
    //public bool makeBalls = false;

    void Awake()
    {
        if (activeBalls != null)
        {
            activeBalls.Clear();
            activeBalls = null;
        }
        ballSpawnAction = SteamVR_Actions.baseballSet_ReverseButton;
        instance = this;

        ballRadius = ballPrefab.GetComponentInChildren<SphereCollider>().radius;
    }

    // Use this for initialization
    void Start()
    {
        /*
        if(offsetTeeWhenRoomSmall)
        {
            float width = 0;
            if (GameController.isTrueOculus)
            {
                Vector3 oculusWalls = OVRManager.boundary.GetDimensions(OVRBoundary.BoundaryType.PlayArea);
                width = Mathf.Max(width, Mathf.Abs(oculusWalls.x));
            }
            else
            {
                if (SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref playBounds))
                {
                    width = playBounds.vCorners0.v0;
                    width = Mathf.Max(width, playBounds.vCorners1.v0);
                    width = Mathf.Max(width, playBounds.vCorners2.v0);
                    width = Mathf.Max(width, playBounds.vCorners3.v0);
                }
            }
            if(width != 0)
            { 
                if (width < tinyAreaWidth/2f)
                {
                    if(forceRighty || (dominantHandManager.dominantHand == dominantHandManager.Hand.right && !forceLefty))
                    {
                        if (tee)
                        {
                            tee.transform.position += tee.transform.right * width / 4f;
                            if (pitcher)
                            {
                                pitcher.transform.position += tee.transform.right * width / 4f;
                            }
                        }
                    }
                    else
                    {
                        if (dominantHandManager.dominantHand == dominantHandManager.Hand.left || forceLefty)
                        {
                            if (tee)
                            {
                                tee.transform.position -= tee.transform.right * width / 4f;
                                if(pitcher)
                                {
                                    pitcher.transform.position -= tee.transform.right * width / 4f;
                                }
                            }
                        }
                    }
                }
            }
        }
        */

        if (teeHandle)
        {
            //CHECK TO MAKE SURE THIS ISN'T A FULL BASEBALL GAME
            if (FullGameDirector.Instance == null)
            {
                CustomLinearDrive teeDrive = teeHandle.transform.parent.GetComponentInChildren<CustomLinearDrive>();
                LinearMapping map = teeDrive.linearMapping;
                linearMappingForce slider = map.GetComponent<linearMappingForce>();
                if (slider)
                {
                    slider.Calibrate();
                }
            }
            teeBody.SetActive(true);
        }
        if (teeHandle)
        {
            teeHandle.SetActive(true);
        }
        if (ballBucket)
        {
            bucketSound = ballBucket.GetComponentInChildren<AudioSource>();
            //Debug.Log("look for spawners in bucket "+);
            bucketSpawners = ballBucket.GetComponentsInChildren<spawnBall>(true);
            //Debug.Log(bucketSpawners.Length+" bucket spawners found----------------------------------------------------------------------------");
        }
        _maxBallsAtOnce = maxBallsAtOnce;
        switch (currentServiceType)
        {
            case ballServiceType.tee:
                if (teeHandle)
                {
                    teeHandle.SetActive(true);
                }
                if (teeBody)
                {
                    teeBody.SetActive(true);
                }
                if (pitcher)
                {
                    pitcher.SetActive(false);
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(false);
                }
                if (ballBucket)
                {
                    ballBucket.SetActive(false);
                }
                if (ballsButton)
                {
                    ballsButton.SetIsLocked(true);
                }
                if (GameController.levelIsBegun)
                {
                    if (teeController && (spawnTeeBallAutomatically || spawnTeeBallsConstantly || GameController.fullBatterMode))
                    {
                        newBall();
                        //teeController.NewBall(ballPrefab).SetOrigin(ballServiceType.tee, teeController.transform);
                    }
                }
                break;
            case ballServiceType.pitcher:
                if (teeHandle)
                {
                    teeHandle.SetActive(false);
                }
                if (teeBody)
                {
                    teeBody.SetActive(false);
                }
                if (pitcher)
                {
                    pitcher.SetActive(true);
                    if (spawnPitchedBallsConstantly || GameController.fullBatterMode)
                    {
                        //Debug.Log("STA");
                        newBall();
                    }
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(true);
                }
                if (ballBucket)
                {
                    ballBucket.SetActive(false);
                }
                if (ballsButton)
                {
                    ballsButton.SetIsLocked(true);
                }
                break;
            case ballServiceType.self:
                if (teeHandle)
                {
                    teeHandle.SetActive(false);
                }
                if (teeBody)
                {
                    teeBody.SetActive(false);
                }
                if (pitcher)
                {
                    pitcher.SetActive(false);
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(false);
                }
                if (ballBucket)
                {
                    ballBucket.SetActive(true);
                    StartCoroutine(RefillBallBucket());
                    if (ballsButton)
                    {
                        ballsButton.SetIsLocked(false);
                    }
                }

                break;

        }
        //difficulty = GetComponent<difficultyController> (); 

        //resetBallCount ();
        //StartCoroutine ("createBallPool");
    }

    public void OnLevelBegun()
    {
        Start();
    }

    void Update()
    {
#if RIFT
        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.LTouch) || OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
        {
            newBall();
        }
#elif GEAR_VR
        
        //if (Time.time>5f)
        {
            if (spawnTeeBallAutomatically && currentServiceType == ballServiceType.tee && Time.timeSinceLevelLoad>2f)
            {
                Collider[] collides = Physics.OverlapSphere(teeController.transform.position, .05f);
                if (collides.Length == 0)
                {
                    newBall();
                }
                /*
                else
                {
                    foreach(Collider c in collides)
                    {
                        Debug.Log("can't spawn ball because of "+c.name);
                    }
                }
                */
            }

            if ((OVRInput.GetConnectedControllers() & OVRInput.Controller.RTrackedRemote) != 0)
            {
                if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote))
                {
                    newBall();
                }
            }
        }
        
#elif STEAM_VR

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //newBall();
        }

        if (Player.instance)
        {
            if (currentServiceType == ballServiceType.pitcher)
            {
                if ((HandManager.currentLeftCustomHand && HandManager.currentLeftCustomHand.GetMenuButtonDown()) || (HandManager.currentRightCustomHand && HandManager.currentRightCustomHand.GetMenuButtonDown()))
                {
                    pitcherTarget.SetActive(true);
                }
            }
            if (GameController.fullBatterMode)
            {
                genericTrackerHasBeenSeen = true;
                if ((currentServiceType == ballServiceType.pitcher && !spawnPitchedBallsConstantly) || (currentServiceType == ballServiceType.tee && !spawnTeeBallsConstantly))
                {
                    if (pitcherController && !GameController.fullBatterMode)
                    {
                        Debug.Log("stop the pitching.");
                        pitcherController.StopConstantPitching(false);
                    }

                    CheckForBallSpawnOnButton();
                }
            }
            else
            {
                if (genericTrackerHasBeenSeen)
                {
                    /*
                    if (HandManager.currentLeftCustomHand == null && HandManager.currentRightCustomHand == null && HandManager.genericTracker && HandManager.genericTracker.IsValid())
                    {
                        GameController.instance.SetOnlyFullBatterAvailable(true);
                    }
                    else
                    {
                        GameController.instance.SetOnlyFullBatterAvailable(false);
                    }
                    */
                }
                else
                {
                    if (HandManager.genericTracker && HandManager.genericTracker.IsValid())
                    {
                        genericTrackerHasBeenSeen = true;
                    }
                }
                if ((currentServiceType == ballServiceType.pitcher && spawnPitchedBallsConstantly) || (currentServiceType == ballServiceType.tee && spawnTeeBallsConstantly))
                {
                    //autoreplace
                    //newBall();
                }
                else
                {
                    if (pitcherController)
                    {
                        pitcherController.StopConstantPitching(true);
                    }
                    //button presses only.
                    CheckForBallSpawnOnButton();
                }
            }



            if (activeBalls != null)
            {
                hittable[] temp = activeBalls.ToArray();
                foreach (hittable h in temp)
                {
                    if (h)
                    {
                        Debug.DrawRay(h.transform.position, Vector3.up, Color.blue);
                    }
                }
            }
        }
#endif
    }

    public void CheckForBallSpawnOnButton()
    {

        if (!GameController.isAtBat || FullGameDirector.Instance != null)
        {
            return;
        }

        if (!((currentServiceType == ballServiceType.tee && spawnTeeBallsConstantly) || (currentServiceType == ballServiceType.pitcher && spawnPitchedBallsConstantly)))
        {
            for (int i = 0; i < Player.instance.handCount; i++)
            {
                Hand hand = Player.instance.GetHand(i);
                if (ballSpawnAction.GetLastStateDown(hand.handType))
                {
                    newBall(true);
                }
            }
        }
    }

    public void SetServiceToTee()
    {
        SetServiceType(ballServiceType.tee);
    }

    public void SetServiceToPitcher()
    {
        SetServiceType(ballServiceType.pitcher);
    }

    public void SetServiceToSelf()
    {
        SetServiceType(ballServiceType.self);
    }

    public void SetServiceType(ballServiceType type)
    {
        if (currentServiceType == type)
        {
            return;
        }
        currentServiceType = type;

        switch (type)
        {
            case ballServiceType.tee:
                if (teeBody)
                {
                    teeBody.SetActive(true);
                    teeHandle.SetActive(true);
                    /*
                    CustomLinearDrive teeDrive = tee.GetComponentInChildren<CustomLinearDrive>();
                    
                    float adjustmentToMake = (pitcher.GetComponent<pitchBall>().pitchTarget.transform.position.y - teeController.transform.position.y);
                    teeDrive.transform.position += Vector3.up * adjustmentToMake;
                    teeDrive.linearMapping.GetComponent<linearMappingForce>().adjustableRB.transform.position += Vector3.up * adjustmentToMake;

                    teeDrive.ClearGrab();
                    tee.GetComponent<linearMappingForce>().Calibrate();
                    */
                }
                if (pitcher)
                {
                    pitcher.SetActive(false);
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(false);
                }
                if (ballBucket)
                {
                    EmptyTheBucket();
                    ballBucket.SetActive(false);
                    if (ballsButton)
                    {
                        ballsButton.SetIsLocked(true);
                    }
                }
                if (spawnTeeBallAutomatically || GameController.fullBatterMode)
                {
                    newBall();
                }
                break;
            case ballServiceType.pitcher:
                if (teeHandle)
                {
                    teeHandle.SetActive(false);
                }
                if (teeBody)
                {
                    teeBody.SetActive(false);
                }
                if (teeController)
                {
                    teeController.StopAutomaticSpawning();
                }

                if (pitcher)
                {
                    if (pitcherController)
                    {
                        pitcherController.StopConstantPitching(false);
                    }

                    pitcher.SetActive(true);
                    if (spawnPitchedBallsConstantly || GameController.fullBatterMode)
                    {
                        newBall();
                    }
                }
                if (pitcherTarget)
                {
                    pitcherTarget.transform.parent.position += Vector3.up * (teeController.transform.position.y - pitcher.GetComponentInChildren<pitchBall>().pitchTarget.transform.position.y);
                    pitcherTarget.SetActive(true);
                }
                if (ballBucket)
                {
                    EmptyTheBucket();
                    ballBucket.SetActive(false);
                    if (ballsButton)
                    {
                        ballsButton.SetIsLocked(true);
                    }
                }
                break;
            case ballServiceType.self:
                if (teeHandle)
                {
                    teeHandle.SetActive(false);
                }
                if (teeBody)
                {
                    teeBody.SetActive(false);
                }
                if (teeController)
                {
                    teeController.StopAutomaticSpawning();
                }
                if (pitcher)
                {
                    pitcher.SetActive(false);
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(false);
                }
                if (ballBucket)
                {
                    ballBucket.SetActive(true);
                    StartCoroutine(RefillBallBucket());
                    if (ballsButton)
                    {
                        ballsButton.SetIsLocked(false);
                    }
                    if (bucketSound)
                    {
                        bucketSound.Play();
                    }
                }
                break;
            default:
                break;
        }
    }

    public void DestroyActiveCaughtBall()
    {/*
        Debug.Log(">>>>>>>>>>>>>>>>>> DESTROY ACTIVE BALL <<<<<<<<<<<<<<<<<<<<<");
        // if the currently active ball is not null
        if (currentlyActiveBall != null)
        {
            var ballTracker = currentlyActiveBall.GetComponent<BallTracker>();

            if (ballTracker.photonView.isMine == false)
            {
                Debug.Log("Photon view is not owned by me. Exiting out of destroying the active ball.");
                return;
            }

            Debug.Log("Currently Active Ball has a photon view attached and is DESTROYED.");
            PhotonNetwork.Destroy(ballTracker.photonView);
        }
        */
    }

    public void ResetBalls()
    {
        if (currentServiceType == ballServiceType.self)
        {
            StopAllCoroutines();
            if (bucketSound)
            {
                bucketSound.Play();
            }
            StartCoroutine(RefillBallBucket());
        }
        else
        {
            if (activeBalls != null && activeBalls.Count > 0)
            {
                Debug.Log("e");
                hittable nextInLine;
                while (activeBalls.Count > (maxBallsAtOnce))
                {
                    Debug.Log("a");
                    nextInLine = activeBalls.Dequeue();
                    if (nextInLine)
                    {
                        //Debug.Log("d");
                        //PhotonView p = nextInLine.GetComponent<PhotonView>();
                        //if (p && p.isMine)
                        //{
                        //    Debug.Log("p");
                        //    PhotonNetwork.Destroy(p.gameObject);
                        //}
                        //else
                        {
                            Debug.Log("v");
                            Destroy(nextInLine.gameObject);
                        }
                    }
                }
            }
        }
    }

    public Transform GetBucketCenter()
    {
        return bucketCenter;
    }

    private void EmptyTheBucket()
    {
        if (ballsInBucket != null && ballsInBucket.Count > 0)
        {
            foreach (hittable ball in ballsInBucket)
            {
                if (ball)
                {
                    //Debug.Log("destroy 3");
                    //if (ball.GetComponent<PhotonView>())
                    {
                        //    PhotonNetwork.Destroy(ball.gameObject);
                    }
                    //else
                    {
                        Destroy(ball.gameObject);
                    }
                }
            }
            ballsInBucket.Clear();
        }
    }

    public IEnumerator RefillBallBucket()
    {
        while (!GameController.levelIsBegun)
        {
            yield return null;
        }

        yield return null;
        if (activeBalls != null && activeBalls.Count > 0)
        {
            hittable nextInLine;
            while (activeBalls.Count > (maxBallsAtOnce / 2))
            {
                nextInLine = activeBalls.Dequeue();
                if (nextInLine)
                {
                    Debug.Log("destroy 2");
                    //if (nextInLine.GetComponent<PhotonView>())
                    {
                        //    PhotonNetwork.Destroy(nextInLine.gameObject);
                    }
                    //else
                    {
                        Destroy(nextInLine.gameObject);
                    }
                }
            }
        }

        EmptyTheBucket();

        ballsInBucket = new List<hittable>();

        int spawnerIndex = 0;
        for (int i = 0; i < maxBallsAtOnce; i++)
        {
            yield return new WaitForSeconds(0.1f);
            if (bucketSpawners[spawnerIndex].gameObject.activeInHierarchy)
            {
                hittable temp = bucketSpawners[spawnerIndex].NewBall(ballPrefab);
                if (temp)
                {
                    temp.SetOrigin(ballServiceType.self, bucketSpawners[spawnerIndex].transform);
                    ballsInBucket.Add(temp);
                }
            }
            spawnerIndex++;
            spawnerIndex = spawnerIndex % bucketSpawners.Length;
        }

    }

    public static void AddToActiveBalls(hittable ball)
    {

        if (activeBalls == null)
        {
            activeBalls = new Queue<hittable>();
        }

        if (activeBalls.Count >= _maxBallsAtOnce)
        {
            //Debug.Log("---------------------------------------- " + activeBalls.Count + " EXCEEDED active balls max value. MAX_VALUE = " + _maxBallsAtOnce);

            bool seekingToDestroy = true;
            while (seekingToDestroy)
            {
                hittable candidate = activeBalls.Peek();
                if (candidate)
                {
                    /*
                    BallTracker tracker = candidate.GetComponent<BallTracker>();
                    if (tracker)
                    {
                        if (tracker.isHeld)
                        {
                            Debug.Log("---------------------------------------- put it at the back!" + activeBalls.Count);
                            activeBalls.Enqueue(activeBalls.Dequeue());
                            Debug.Log("-----------------------+++++++++++++++++ it's been put to the back." + activeBalls.Count);
                        }
                        else
                        {
                            if (tracker.photon.isMine)
                            {
                                Debug.Log("=========== not held so destroy it");
                                PhotonNetwork.Destroy(activeBalls.Dequeue().GetComponent<PhotonView>());
                            }
                            else
                            {
                                Debug.Log("----------------------000000000000000 it's someone else's to destroy.");
                                //leave it be! someone else will get it!
                                seekingToDestroy = false;
                            }
                        }
                    }
                    else*/
                    {
                        Debug.Log("destroy because it doesn't have a tracker.");
                        Destroy(activeBalls.Dequeue());
                    }
                }
                else
                {
                    Debug.Log("it's already null so just dequeue it.");
                    activeBalls.Dequeue();
                }

                if (activeBalls.Count < _maxBallsAtOnce)
                {
                    seekingToDestroy = false;
                    Debug.Log("there are now few enough balls.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx " + activeBalls.Count);
                }
            }
            /*
            int ballsToKill = activeBalls.Count - _maxBallsAtOnce + 1;
            for (int x = 0; x < ballsToKill; x++)
            {
                hittable toDestroy = activeBalls.Dequeue();
                if (toDestroy)
                {
                    if (toDestroy.GetComponent<PhotonView>())
                    {
                        PhotonNetwork.Destroy(toDestroy.gameObject);
                    }
                    else
                    {
                        Destroy(toDestroy.gameObject);
                    }
                }
            }
            */
        }
        activeBalls.Enqueue(ball);
        currentlyActiveBall = ball;



        //Debug.Log("Active Balls: " + activeBalls.Count);
        if (ball.GetOrigin() == ballServiceType.self)
        {
            ballsInBucket.Remove(ball);
        }
    }

    public void newBall(bool urgent = false)
    {
        Debug.Log("new ball??");
        if (!readyForBalls || !GameController.levelIsBegun)
        {
            //return;
        }
        Debug.Log("new ball!!!!");
        switch (currentServiceType)
        {
            case ballServiceType.tee:
                //if(GameTracker.instance && GameTracker.instance.photonView.isMine==false)
                //{
                //    return;
                //}

                if (activeBalls == null)
                {
                    activeBalls = new Queue<hittable>();
                }

                Debug.Log("urg:" + urgent + " " + spawnTeeBallsConstantly + " " + GameController.fullBatterMode);
                if (!urgent && (spawnTeeBallsConstantly || GameController.fullBatterMode))
                {
                    Debug.Log("start automatic.");
                    teeController.StartAutomaticSpawning(ballPrefab, ballServiceType.tee);
                }
                else
                {
                    Debug.Log("just the one.");
                    if (teeController)
                    {
                        hittable temp = teeController.NewBall(ballPrefab);
                        currentlyActiveBall = temp;
                        temp.SetOrigin(ballServiceType.tee, teeController.transform);
                    }
                }
                break;

            case ballServiceType.pitcher:
                Debug.Log("urg2:" + urgent + " " + spawnPitchedBallsConstantly + " " + GameController.fullBatterMode);

                if (!urgent && (spawnPitchedBallsConstantly || GameController.fullBatterMode))
                {
                    Debug.Log("start automatic. 2");
                    pitcherController.StartConstantPitching(ballPrefab);
                }
                else
                {
                    Debug.Log("just the one.2");
                    pitcherController.pitch(ballPrefab);
                }
                if (pitcherTarget)
                {
                    pitcherTarget.SetActive(false);
                }

                break;

        }
        /*
        if (MultiplayerManager.instance)
        {
            Debug.Log("multiplayer ball made )))))))))))))))))))))))))))))))))))))))");
            //MultiplayerManager.instance.SetIsPlayOver(false);
            readyForBalls = false;
        }
*/
    }

}
