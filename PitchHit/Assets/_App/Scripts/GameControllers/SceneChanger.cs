﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Valve.VR;

public class SceneChanger : MonoBehaviour {
    //public float fadeInTime = 2f;
    //public float fadeOutTime = 2f;
    public GameObject blackoutPrefab;
    public float delayTime = 2f;
    public float replayTime = 2f;
#if STEAM_VR
    public Texture blackTexture;
#endif
    private float countdown;
    private float currentDelayTime;
    private bool busy;
    public GameObject loadIndicator;
    //public AudioSource music;
    public AudioSource[] audiosToFadeOut;
    private float[] initialVolumes;

    void Awake()
    {
        
        countdown = delayTime;
#if STEAM_VR
        SteamVR_Fade.Start(Color.clear, .0f); 
#endif
    }

    void SetInitialVolumes()
    {
        if (audiosToFadeOut != null && audiosToFadeOut.Length > 0)
        {
            initialVolumes = new float[audiosToFadeOut.Length];

            for (int v = 0; v < initialVolumes.Length; v++)
            {
                initialVolumes[v] = audiosToFadeOut[v].volume;
            }
        }
    }

    public void GoToScene(string sceneName, float time)
    {
        if (!busy)
        {
            currentDelayTime = time;
            countdown = currentDelayTime;

            SetInitialVolumes();
            busy = true;
#if RIFT || GEAR_VR
            StartCoroutine(OculusSceneChange(sceneName,time));
#elif GEAR_VR
          
            StartCoroutine(GearSceneChange(sceneName, time));
#elif STEAM_VR
            SteamVR_LoadLevel.Begin(sceneName, false, time);
            
            var loader = new GameObject("loader").AddComponent<SteamVR_LoadLevel>();
            if (blackoutPrefab)
            {
                Instantiate(blackoutPrefab, Vector3.zero, Quaternion.identity);
            }
            loader.loadAsync = true;
            loader.loadingScreen = blackTexture;
            loader.levelName = sceneName;
            loader.showGrid = false;
            loader.fadeOutTime = time;
            loader.backgroundColor = Color.black;
            loader.front = blackTexture;
            loader.back = blackTexture;
            loader.top = blackTexture;
            loader.bottom = blackTexture;
            loader.left = blackTexture;
            loader.right = blackTexture;

         

            loader.Trigger();
            
#endif

        }
    }

    public void ChangeToGame(string exeName)
    {
        if(busy)
        {
            return;
        }

        busy = true;
        //will open if exe is in same directory as launchers exe file.
        System.Diagnostics.Process.Start(exeName);//"RacingPlanet2Launch.exe"

        //Logs the dataPath so you can see where your applications should be
        //Debug.Log(Application.dataPath);

        //will open from the applications dataPath directory
        //System.Diagnostics.Process.Start(Application.dataPath + "/RacingPlanet2Launch.exe");

    }

    IEnumerator OculusSceneChange(string scene, float time)
    {
        Debug.Log("start oculus scene change. " + scene);

        OVRScreenFade temp = Camera.main.GetComponent<OVRScreenFade>();

        //if (temp)
        //{
            temp.fadeTime = time;
            temp.FadeOut();
            //StartCoroutine(temp.FadeOut());
            Debug.Log("fade created.");
        //}
        float startTime = Time.time;
        
        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.showRenderModel = true;
            HandManager.currentLeftCustomHand.MakeHandInvisible(true);
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.showRenderModel = true;
            HandManager.currentRightCustomHand.MakeHandInvisible(true);
        }

        //yield return null;
        //yield return new WaitForSeconds(time * 2f);
        Debug.Log("before async "+Time.time);
        AsyncOperation status = SceneManager.LoadSceneAsync(scene);
        status.allowSceneActivation = false;
        while(status.progress<.9f || (Time.time-startTime)<time)
        {
            yield return null;
        }
        status.allowSceneActivation = true;
        
        Debug.Log("after async " + Time.time);
        //yield return null;


    }
    IEnumerator SimpleGearSceneChange(string scene, float minLoadingTime)
    {
        yield return new WaitForSeconds(minLoadingTime);
        SceneManager.LoadScene(scene);
    }

    IEnumerator GearSceneChange(string scene, float minLoadingTime)
    {
        Debug.Log("start gear scene change. " + scene);
  
        float startTime = Time.time;

        float fullWidthOfLoaderBar = loadIndicator.transform.localScale.x;
        Vector3 scale = loadIndicator.transform.localScale;

        scale.x = .1f;
        loadIndicator.transform.localScale = scale;
        loadIndicator.SetActive(false);

        //



        AsyncOperation status = SceneManager.LoadSceneAsync(scene);
        status.allowSceneActivation = false;

        while (status.progress < .90f || (Time.time - startTime) < minLoadingTime)
        {
            //print("status.progress " + status.progress);
        
            //waiting till loaded or min time has passed
            scale.x = fullWidthOfLoaderBar * status.progress;
            loadIndicator.transform.localScale = scale;
            loadIndicator.SetActive(true);
            yield return new WaitForEndOfFrame();
        }
        //print("DONE LOADING " + scene + " " + status.progress);
        scale.x = fullWidthOfLoaderBar; ;
        loadIndicator.transform.localScale = scale;
        status.allowSceneActivation = true;


        yield return null;
    }

    public void GoToScene(string sceneName)
    {
        GoToScene(sceneName, delayTime);
    }

    public void ReloadScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        GoToScene(scene.name,replayTime);
    }

    void LateUpdate()
    {
        if(busy)
        {
            countdown -= Time.deltaTime;

            if (audiosToFadeOut != null)
            {
                for (int a = 0; a < audiosToFadeOut.Length; a++)
                {
                    if (audiosToFadeOut[a])
                    {
                        audiosToFadeOut[a].volume = initialVolumes[a] - (((currentDelayTime - countdown) / currentDelayTime) * initialVolumes[a]);
                    }
                }
            }  
           
        }
    }

  
}
