﻿using UnityEngine;
using System.Collections;
#if STEAM_VR
using Valve.VR;
using Valve.VR.InteractionSystem;
#endif

public class HandManager : MonoBehaviour
{
    private static HandManager _instance;
    public static HandManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<HandManager>();
            }
            return _instance;
        }
        private set
        {
            _instance = value;
        }
    }


#if RIFT
    public static OVRInput.Controller dominantOculusHand = OVRInput.Controller.RTouch;
    public static OVRInput.Controller secondaryOculusHand = OVRInput.Controller.LTouch;
    public static OVRInput.Controller dominantOculusBattingHand = OVRInput.Controller.RTouch;
    public static OVRInput.Controller secondaryOculusBattingHand = OVRInput.Controller.LTouch;

    private static CustomHand oculusRightCustomHand;
    private static CustomHand oculusLeftCustomHand;
#elif STEAM_VR
    SteamVR_Action_Vibration hapticsAction;

    public static Hand currentLeftSteamVRHand
    {
        get
        {
            if (!Player.instance)
            {
                //Debug.Log("Player instance is null. Returning null");
                return null;
            }
            if(Player.instance.leftHand == null)
            {
                //Debug.Log("Player instance left hand is null. Returning null");
                return null;
            }
            else
            {
                return Player.instance.leftHand;
            }
        }
    }

    public static Hand currentRightSteamVRHand
    {
        get
        {
            if (!Player.instance) return null;
            return Player.instance.rightHand;
        }
    }
#endif

    public enum HandType
    {
        right, left, both
    }

    public static HandType dominantHand = HandType.right;
    public static CustomHand currentLeftCustomHand
    {
        get
        {

#if STEAM_VR
            if (!currentLeftSteamVRHand)
            {
                //Debug.Log("currentLeftSteamVRHand was null, returning null");
                return null;
            }
            CustomHand returnHand = currentLeftSteamVRHand.GetComponent<CustomHand>();
            if(returnHand == null)
            {
                //Debug.Log("returnHand was null, returning null");
                return null;
            }
            else
            {
                return returnHand;
            }
#elif RIFT
            return oculusLeftCustomHand;
#endif
            
        }
    }

    public static CustomHand currentRightCustomHand
    {
        get
        {

#if STEAM_VR
            if (!currentRightSteamVRHand) return null;
            return currentRightSteamVRHand.GetComponent<CustomHand>();
#elif RIFT
            return oculusRightCustomHand;
#endif
        

        }
    }

    public static MixedRealityBat genericTracker;

    public bool humanHoldingBall
    {
        get
        {
            return glove.hasBall;
        }
    }

    public HumanGloveController glovePrefab;
    public HumanGloveController glove { get
        {
            if (_glove == null) { _glove = GameObject.Instantiate(glovePrefab, Vector3.down, Quaternion.identity); _glove.gameObject.SetActive(false); }
            return _glove;
        }
    }
    private HumanGloveController _glove;

    public BatController batPrefab;
    public BatController bat
    {
        get
        {
            if (_bat == null) { _bat = GameObject.Instantiate(batPrefab, Vector3.down, Quaternion.identity); _bat.gameObject.SetActive(false); }
            return _bat;
        }
    }
    private BatController _bat;

    public HandCannonController cannonPrefab;
    public HandCannonController cannon
    {
        get
        {
            if (_cannon == null)
            {
                _cannon = GameObject.Instantiate(cannonPrefab, Vector3.down, Quaternion.identity);
                _cannon.gameObject.SetActive(true);
                _cannon.Disappear_Immediate();
            }
            return _cannon;
        }
    }
    private HandCannonController _cannon;

    public enum HandGear
    {
        BAT,
        GLOVE,
        CANNON,
        HAND,
        CONTROLLER,
        NONE,
    }

    public enum ThrowingVariant
    {
        CANNON,
        MANUAL
    }

    private const string throwTypePrefsKey = "THROW_VAR";
    public static ThrowingVariant throwingType
    {
        get { return (ThrowingVariant)PlayerPrefs.GetInt(throwTypePrefsKey); }
        set { PlayerPrefs.SetInt(throwTypePrefsKey, (int)value);
            useCannon = (value == ThrowingVariant.CANNON);
            useCannon_setFromPrefs = true;
        }
    }
    public static bool useCannon
    {
        get
        {
            if (!useCannon_setFromPrefs)
            {
                _useCannon = (throwingType == ThrowingVariant.CANNON);
                useCannon_setFromPrefs = true;
            }
            return _useCannon;
        }
        private set
        {
            _useCannon = value;
        }
    }
    private static bool _useCannon;
    private static bool useCannon_setFromPrefs;


    void Awake()
    {
#if STEAM_VR
        hapticsAction = SteamVR_Actions.baseballSet_Haptic;
#endif
    }

    void Start()
    {
        instance = this;

#if RIFT || GEAR_VR
        //Debug.Log("----------------------------------------------------------------------- HANDMANAGER STARTING");
        if (OVRManager.instance)
        {
//            Debug.Log("ovr instance found.");
            Transform oculusParent = OVRManager.instance.transform.Find("TrackingSpace");
            CustomHand[] ch = oculusParent.gameObject.GetComponentsInChildren<CustomHand>();
//            Debug.Log("ovr instance found." +ch.Length);
            foreach (CustomHand c in ch)
            {

                if (c.isOculusRight)
                {
                    oculusRightCustomHand = c;
//                    Debug.Log("ovr right hand found");
                }
                else
                {
                    oculusLeftCustomHand = c;
//                    Debug.Log("ovr left hand found");
                }

            }
        }
#elif STEAM_VR

#endif


    }

    void Update()
    {
        UpdateHands();
        RefreshThrowingEquipment();
        //print("dominantOculusHand " + dominantOculusHand);
        if (currentLeftCustomHand)
        {
            Debug.DrawRay(currentLeftCustomHand.transform.position, Vector3.up, Color.red);
        }

        if (currentRightCustomHand)
        {
            Debug.DrawRay(currentRightCustomHand.transform.position + (Vector3.forward * .02f), Vector3.up, Color.blue);
        }
    }

    public void DetermineDominantHandByGrab(GameObject grabbed)
    {
        int attempts = 0;
        while (attempts < 10)
        {
            hitThings.SelectNextGripType();
            if (hitThings.GetCurrentGripType() == hitThings.gripType.always)
            {
                attempts = 11;
            }
            else
            {
                attempts++;
            }
        }
#if STEAM_VR
        if (currentLeftCustomHand && currentLeftCustomHand.actualHand)
        {
            foreach (Hand.AttachedObject a in currentLeftCustomHand.actualHand.AttachedObjects)
            {
                if (a.attachedObject == grabbed)
                {
                    SetDominantHand(HandType.left);
                    return;
                }
            }
        }

        if (currentRightCustomHand && currentRightCustomHand.actualHand)
        {
            foreach (Hand.AttachedObject a in currentRightCustomHand.actualHand.AttachedObjects)
            {
                if (a.attachedObject == grabbed)
                {
                    SetDominantHand(HandType.right);
                    return;
                }
            }
        }
#elif RIFT
        if (currentLeftCustomHand)
        {
            OVRGrabber leftGrab = currentLeftCustomHand.GetComponentInChildren<OVRGrabber>();
            if (leftGrab && leftGrab.grabbedObject)
            {
                if (leftGrab.grabbedObject.gameObject == grabbed)
                {
                    SetDominantHand(HandType.left);
                    return;
                }
            }
        }

        if (currentRightCustomHand)
        {
            OVRGrabber rightGrab = currentLeftCustomHand.GetComponentInChildren<OVRGrabber>();
            if (rightGrab && rightGrab.grabbedObject)
            {
                if (rightGrab.grabbedObject.gameObject == grabbed)
                {
                    SetDominantHand(HandType.right);
                    return;
                }
            }
        }

#endif
    }

    public static void SetDominantHand(HandType dom)
    {
#if RIFT
        if(dom == HandType.right)
            {
                dominantOculusHand = OVRInput.Controller.RTouch;
                secondaryOculusHand = OVRInput.Controller.LTouch;
            }
            else
            {
                dominantOculusHand = OVRInput.Controller.LTouch;
                secondaryOculusHand = OVRInput.Controller.RTouch;
            }
#endif


        dominantHand = dom;

        //save to userprefs.

        string temp = "BOTH";
        if (dom == HandType.right)
        {
            temp = "RIGHT";
        }
        else if (dom == HandType.left)
        {
            temp = "LEFT";
        }

        PlayerPrefs.SetString("HAND", temp);
    }

   
    public static OVRInput.Controller OculusHand(CustomHand customHand)
    {
        if (customHand == currentLeftCustomHand)
        {
            return OVRInput.Controller.LTouch;
        }
        if (customHand == currentRightCustomHand)
        {
            return OVRInput.Controller.RTouch;
        }
        return OVRInput.Controller.RTouch;
    }

    private static float timeWithAnyHands = 0;
    private static int previousNumberOfHands = 0;
    public static void UpdateHands()
    {

#if RIFT
        //only keep hands assigned if that controller is active?

#elif GEAR_VR
        //Debug.Log("current right hand. "+currentRightCustomHand);
        //if((OVRInput.GetConnectedControllers() & OVRInput.Controller.Touchpad) != 0)
        //{

        //}

#elif STEAM_VR

       
#endif
    }

    public static bool IsHandValid(HandType type)
    {
#if RIFT
        OVRInput.Controller controller = OVRInput.GetConnectedControllers();
        //Debug.Log("active? "+ ((controller & OVRInput.Controller.LTouch) !=0));
        if(type == HandType.left)
        {
            if ((controller & OVRInput.Controller.LTouch) !=0)
            {
                return true;
            }
        }
        else if(type == HandType.right)
        {
            if((controller & OVRInput.Controller.RTouch) != 0)
            {
                return true;
            }
        }
#elif GEAR_VR
        if(Application.isEditor)
        {
            if(type == HandType.left)
            {
                return currentLeftCustomHand != null;
            }
            if(type == HandType.right)
            {
                return currentRightCustomHand != null;
            }
        }

        OVRInput.Controller controller = OVRInput.GetConnectedControllers();
        //Debug.Log("active? "+ ((controller & OVRInput.Controller.LTouch) !=0));
        if (type == HandType.left)
        {
            if ((controller & OVRInput.Controller.LTrackedRemote) != 0)
            {
                return true;
            }
        }
        else if (type == HandType.right)
        {
            if ((controller & OVRInput.Controller.RTrackedRemote) != 0)
            {
                return true;
            }
        }
#elif STEAM_VR

        if (type == HandType.left)
        {
            if (currentLeftSteamVRHand != null)
            {
                return true;
            }
        }
        else if (type == HandType.right)
        {
            if (currentRightSteamVRHand != null)
            {
                return true;
            }
        }
#endif
        return false;
    }

    public static CustomHand GetAvailableDominantHand()
    {
        if (dominantHand == HandType.left)
        {
            if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
            else if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
        }
        else
        {
            if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
            else if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
        }

        return null;
    }

    public static CustomHand GetAvailableOffHand()
    {
        if (dominantHand == HandType.right)
        {
            if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
            else if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
        }
        else
        {
            if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
            else if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
        }

        return null;
    }

    public static CustomHand GetAvailableBattingHand()
    {
        if (dominantHand == HandType.left)
        {
            if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
            else if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
        }
        else
        {
            if (IsHandValid(HandType.right))
            {
                return currentRightCustomHand;
            }
            else if (IsHandValid(HandType.left))
            {
                return currentLeftCustomHand;
            }
        }

        return null;
    }

    //This is for Oculus in steam. It's the best way I could think as to deal with the hands being judged wrongly at the start.
    private static void SwapHandVisuals(CustomHand handOne, CustomHand handTwo)
    {
        Animator tempHandAnimator = handOne.handAnimator;
        Transform tempHandOneVisual = tempHandAnimator.transform.parent;
        Transform tempHandOneVisualParent = tempHandOneVisual.parent;

        Vector3 tempHandVisualLocalPosition = tempHandOneVisual.localPosition;
        Quaternion tempHandVisualLocalRotation = tempHandOneVisual.localRotation;
        Vector3 tempHandVisualLocalScale = tempHandOneVisual.localScale;

        Transform tempHandTwoVisual = handTwo.handAnimator.transform.parent;

        //move hand one over to hand two's spot.
        handOne.handAnimator = handTwo.handAnimator;
        tempHandOneVisual.SetParent(tempHandTwoVisual.parent);
        //reinstate hand one's old local orientation
        tempHandOneVisual.localPosition = tempHandVisualLocalPosition;
        tempHandOneVisual.localRotation = tempHandVisualLocalRotation;

        //save hand two's local orientation
        tempHandVisualLocalPosition = tempHandTwoVisual.localPosition;
        tempHandVisualLocalRotation = tempHandTwoVisual.localRotation;

        //move hand two over to hand one's spot.
        handTwo.handAnimator = tempHandAnimator;
        tempHandTwoVisual.SetParent(tempHandOneVisualParent);
        //reinstate hand two's old local orientation
        tempHandTwoVisual.localPosition = tempHandVisualLocalPosition;
        tempHandTwoVisual.localRotation = tempHandVisualLocalRotation;
    }

    public void RemoveAllBaseballEquipment()
    {
        SetHandEquipmentActive(HandGear.BAT, false);
        SetHandEquipmentActive(HandGear.GLOVE, false);
        //SetHandEquipmentActive(HandGear.CANNON, false);
        /*
        for (int i = 0; i < System.Enum.GetValues(typeof(HandGear)).Length; i++)
        {
            SetHandEquipmentActive((HandGear)i, false);
        }
        */
    }

    public void SetHandEquipmentActive(HandGear equip, bool isOn)
    {
        //Debug.Log("SetPlayer Equipment " + equip + " " + isOn);
        switch (equip)
        {
            case HandGear.BAT:
                bat.gameObject.SetActive(isOn);
                bat.SetHoldable(isOn);
                bat.enabled = isOn;
                break;
            case HandGear.CANNON:
                //MARC: cannon is always active, use Prepare and Disappear to control
                /*
                isOn = isOn && useCannon;
                cannon.gameObject.SetActive(isOn);
                cannon.enabled = isOn;
                */
                break;
            case HandGear.GLOVE:

                if (isOn)
                {
                    glove.shouldBeOnLeft = (dominantHand == HandType.right);
                    glove.gameObject.SetActive(true);
                    glove.attachToHandAutomatically = true;
                    glove.SetAttached(true);
                    glove.enabled = true;
                }
                else
                {
                    glove.attachToHandAutomatically = false;
                    glove.enabled = false;
                    glove.SetAttached(false);
                    glove.gameObject.SetActive(false);
                }
                break;
            case HandGear.CONTROLLER:
                break;
            case HandGear.HAND:
                break;
            case HandGear.NONE:
                break;
        }
        if(currentLeftCustomHand) currentLeftCustomHand.RefreshHandState();
        if(currentRightCustomHand) currentRightCustomHand.RefreshHandState();

        UpdateHands();
    }

    void RefreshThrowingEquipment()
    {
        if (humanHoldingBall)
        {
            if (useCannon && !cannon.isActivated)
            {
                    cannon.PrepareForUse();
            }
            else if (!useCannon && cannon.isActivated)
            {
                cannon.Disappear_Immediate();
            }

        }
        else
        {
            if (cannon.isActivated)
            {
                if (useCannon)
                {
                    cannon.Disappear();
                }
                else
                {
                    cannon.Disappear_Immediate();
                }
            }
        }
    }

    public static void ToggleThrowingType()
    {

        if (throwingType == ThrowingVariant.MANUAL) throwingType = ThrowingVariant.CANNON;
        else if (throwingType == ThrowingVariant.CANNON) throwingType = ThrowingVariant.MANUAL;


        Debug.Log("Toggle Throwing Type " + throwingType);
    }

    public static void SetThrowingType(ThrowingVariant type)
    {
        throwingType = type;
    }

    public void HideCannonImmediately()
    {
        cannon.Disappear_Immediate();
    }

    public void EquipManualThrowing()
    {
        SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);
        SetThrowingType(HandManager.ThrowingVariant.MANUAL);
        FieldMonitor.Instance.SpawnFreshBall(destroyExisting: true);
        glove.CatchBall(FieldMonitor.ballInPlay);
    }

    public void EquipNothing()
    {
        SetHandEquipmentActive(HandManager.HandGear.GLOVE, false);
        SetHandEquipmentActive(HandManager.HandGear.CANNON, false);
        SetHandEquipmentActive(HandManager.HandGear.BAT, false);
    }

    public void DropAllHeldObjects()
    {
        if (currentLeftCustomHand != null) currentLeftCustomHand.BroadcastMessage("DropAll", SendMessageOptions.DontRequireReceiver);
        if (currentRightCustomHand != null) currentRightCustomHand.BroadcastMessage("DropAll", SendMessageOptions.DontRequireReceiver);
    }

    #region Vibration

    /// <summary>
    /// 
    /// </summary>
    /// <param name="delay">start seconds from now</param>
    /// <param name="duration">duration seconds</param>
    /// <param name="frequency">frequency, 0-320 hz</param>
    /// <param name="amplitude">intensity, 0-1</param>
    /// <param name="hand"></param>
    public static void VibrateController(float delay, float duration, float frequency, float amplitude, CustomHand hand)
    {
        if (!hand) return;
//        Debug.Log("Vibrate controller " + hand.name + " " + delay + " " + duration + " " + frequency + " " + amplitude);
        if (!instance) return;
#if STEAM_VR
        if (!hand.actualHand) return;
        VibrateController(delay, duration, frequency, amplitude, hand.actualHand.handType);
#elif RIFT
        instance.StartCoroutine(instance.VibrationRoutine(duration, frequency/320f, amplitude, OculusHand(hand)));
#endif
    }

#if RIFT
    private IEnumerator VibrationRoutine(float duration, float frequency, float amplitude, OVRInput.Controller hand)
    {
        OVRInput.SetControllerVibration(frequency, amplitude, hand);
        yield return new WaitForSeconds(duration);
        OVRInput.SetControllerVibration(0, 0, hand);
    }
#endif

#if STEAM_VR
    public static void VibrateController(float delay, float duration, float frequency, float amplitude, SteamVR_Input_Sources hand)
    {
        instance.hapticsAction.Execute(delay, duration, frequency, amplitude, hand);
    }
#endif






    #endregion
    public float GetHumanThrowStrength()
    {
        if (useCannon)
        {
            return cannon.strength;
        }
        return 20f;//MAGIC NUMBER, ARBITRARY
    }

    public void HideHandsFromLIV()
    {
        Debug.Log("HIDING HANDS FROM LIV");
        ChangeLayersRecursively(currentLeftCustomHand.handAnimator.transform, "FieldPlayer");
        ChangeLayersRecursively(currentRightCustomHand.handAnimator.transform, "FieldPlayer");
    }

    public void UnhideHandsFromLIV()
    {
        Debug.Log("UNHIDING HANDS FROM LIV");
        ChangeLayersRecursively(currentLeftCustomHand.handAnimator.transform, "Default");
        ChangeLayersRecursively(currentRightCustomHand.handAnimator.transform, "Default");
    }

    void ChangeLayersRecursively(Transform trans, string name)
    {
        if (trans.gameObject.layer != LayerMask.NameToLayer("SpectatorOnly"))
        {
            trans.gameObject.layer = LayerMask.NameToLayer(name);
        }
        foreach (Transform child in trans)
        {
            if (child.gameObject.layer != LayerMask.NameToLayer("SpectatorOnly"))
            {
                child.gameObject.layer = LayerMask.NameToLayer(name);
            }
            ChangeLayersRecursively(child, name);
        }
    }
}
