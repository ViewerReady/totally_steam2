﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Valve.VR;
#if RIFT
using OVRTouchSample;
#elif STEAM_VR
using Valve.VR.InteractionSystem;
#endif
public class TutorialController : MonoBehaviour
{
    public bool teachBatGrip;
    public bool teachGripThings;
    public bool teachBallSpawning;
    public bool demoTeachBallSpawn;
    public bool teachMenuButton;

    public GameObject tutorialObjectCollection;
    //public int tutorialTime = 300;

#if RIFT
#elif STEAM_VR
        	public LinearMapping teePosition;
            private Player thePlayer;
#endif

    public GameObject teeTutorialObj;

    public hitThings batObj;

    public GameObject ballTutorialObj;
    public ballController ballSpawner;

    public GameObject ballRespawnTutorialObj;
    public GameObject ballRespawnTutorialOculus;

    public GameObject grabTutorialObject;

    public HumanGloveController glove;
    //public GameObject timeWarning;

    //private levelTypes levelToGoTo;

    //private difficultyController difficultyObj;
    //private challengeController challengeObj;

    //public GameObject countdownObj;
    //private AudioSource countdownController;
    //private StatsController statsController;
    //private TimeController timeController;

    //private int initPosition;

    private static bool alreadyTaughtBat = false;
    private static bool alreadyTaughtPickup = false;
    private static bool alreadyTaughtSpawn = false;
    private static bool alreadyTaughtReturn = false;
    private static bool alreadyTaughtTurning = false;
    private static bool alreadyTaughtMenu = false;

    // Use this for initialization
    void Start()
    {

        if (teeTutorialObj)
        {
            teeTutorialObj.SetActive(false);
        }
        if (ballTutorialObj)
        {
            ballTutorialObj.SetActive(false);
        }

        //timeWarning.SetActive (false);
        if (ballRespawnTutorialObj)
        {
            ballRespawnTutorialObj.SetActive(false);
        }
        if (ballRespawnTutorialOculus)
        {
            ballRespawnTutorialOculus.SetActive(false);
        }
        Debug.Log("teach bat? " + teachBatGrip + "  " + !alreadyTaughtBat);
        if (teachBatGrip && !alreadyTaughtBat)
        {
            StartCoroutine(CheckForGrabbedBat());
        }

#if !GEAR_VR
        //Debug.Log("grip? "+teachGripThings+" "+alreadyTaughtPickup);
        if (teachGripThings && !alreadyTaughtPickup)
        {
            StartCheckForGrabThings();
        }
        else
        {
            if (grabTutorialObject)
            {
                grabTutorialObject.SetActive(false);
            }
        }
#endif
        Debug.Log((LockerRoomLevelController.isDemo && demoTeachBallSpawn) + "");
        //Debug.Log("spawn? "+teachBallSpawning+" "+alreadyTaughtSpawn);
        if ((teachBallSpawning || (LockerRoomLevelController.isDemo && demoTeachBallSpawn)) && !alreadyTaughtSpawn)
        {

            if (!GameController.fullBatterMode)
            {
                ballSpawner.spawnTeeBallAutomatically = false;
                StartCoroutine(CheckForBallSpawned());
            }
        }
        else
        {
            if (ballRespawnTutorialObj)
            {
                ballRespawnTutorialObj.SetActive(false);
            }
        }

        Debug.Log("?????????????????????????????????????????????????????");
        if (teachMenuButton && !alreadyTaughtMenu)
        {
            Debug.Log("check for teach menu button at start.");
            StartCheckingForMenuButton();
        }
    }

    public void StartCheckForGrabThings()
    {
        if (GameController.fullBatterMode)
        {
            return;
        }

        StartCoroutine(CheckForGrabbedAnything());

    }

    public void TryBatTutorialAgain()
    {
        Debug.Log("bat tut? " + teachBatGrip + " " + alreadyTaughtBat);
        if (teachBatGrip && !alreadyTaughtBat)
        {
            StartCoroutine(CheckForGrabbedBat());
        }
    }

    IEnumerator CheckForGrabbedBat()
    {
        //yield return new WaitForSeconds (5f);
        if (hitThings.GetCurrentGripType() == hitThings.gripType.always || GameController.fullBatterMode)
        {
            Debug.Log("dont try to teach the always");
            yield break;
        }


        CustomHand previousHand = HandManager.GetAvailableDominantHand();

        while (previousHand == null)
        {
            if (GameController.fullBatterMode)
            {
                yield break;
            }

            //Debug.Log("waiting for a dominant hand.");
            yield return null;
            previousHand = HandManager.GetAvailableDominantHand();

            if (previousHand)
            {
                while (previousHand && !previousHand.renderModelsLoaded)
                {
                    yield return null;
                }
            }

        }
        Debug.Log("set grip bat hint true " + previousHand.name);
        previousHand.SetGripBatHint(true);

        while (!batObj.attached)
        {
            if (GameController.fullBatterMode)
            {
                if (previousHand != null)
                {
                    //why is this null
                    previousHand.SetGripBatHint(false);
                    previousHand.DisableText();
                }
                yield break;
            }

            CustomHand dominantCustomHand = HandManager.GetAvailableDominantHand();
            if (dominantCustomHand)
            {
                //Debug.Log("there IS a custom hand.");
                if (previousHand && previousHand != dominantCustomHand)
                {
                    previousHand.DisableText();
                    previousHand.SetGripBatHint(false);
                    dominantCustomHand.SetGripBatHint(true);
                }
                //Debug.Log("waiting for bad in hand " + dominantCustomHand.name);
                if (hitThings.GetCurrentGripType() == hitThings.gripType.toggle)
                {
                    dominantCustomHand.DisplayText("Squeeze grip\non side to\ntoggle bat");
                    //ControllerButtonHints.ShowTextHint(dominantHand, EVRButtonId.k_EButton_Grip, "Squeeze grip\non side to\ntoggle bat");
                }
                else
                {
                    dominantCustomHand.DisplayText("Squeeze grip on side to grip bat");
                }

            }

            previousHand = dominantCustomHand;

            yield return null;
        }
        Debug.Log("done teaching bat");
        alreadyTaughtBat = true;

        //remove bat tutorial
        if (previousHand != null)
        {
            previousHand.SetGripBatHint(false);
        }

#if RIFT
        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }
        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }
#endif

        yield return new WaitForSeconds(.5f);

        HandManager.GetAvailableDominantHand().DisableText();

        StartCheckForTurning();
    }

    public void StartCheckForTurning()
    {
#if RIFT
        if (!alreadyTaughtTurning)
        {
            StartCoroutine(CheckingForTurning());
        }
#elif STEAM_VR
        if ((GameController.isOculusInSteam) && !alreadyTaughtTurning)
        {
            StartCoroutine(CheckingForTurning());
        }
#endif
    }

    private Quaternion initialRoomRotation;
    IEnumerator CheckingForTurning()
    {
        CustomHand leftCustom = HandManager.currentLeftCustomHand;
        CustomHand rightCustom = HandManager.currentRightCustomHand;

        //if (GameController.isOculusInSteam)
        //{
        //    HandManager.GetAvailableDominantHand().DisplayText("Press B or Y \nto turn around");
        //}
        //else
        //{
            HandManager.GetAvailableDominantHand().DisplayText("Use the thumbsticks\nto turn around");
        //}
        leftCustom = HandManager.currentLeftCustomHand;
        rightCustom = HandManager.currentRightCustomHand;

        if (leftCustom)
        {
            leftCustom.SetTurnAroundHint(true);

        }
        if (rightCustom)
        {
            rightCustom.SetTurnAroundHint(true);
        }
#if RIFT
        initialRoomRotation = OVRManager.instance.transform.rotation;
        while (initialRoomRotation == OVRManager.instance.transform.rotation)
#elif STEAM_VR
        initialRoomRotation = Player.instance.transform.rotation;
        while (initialRoomRotation == Player.instance.transform.rotation)
#endif
        {
            yield return null;

        }
        if (leftCustom)
        {
            leftCustom.SetTurnAroundHint(false);
            leftCustom.DisableText();
        }
        if (rightCustom)
        {
            rightCustom.SetTurnAroundHint(false);
            rightCustom.DisableText();
        }

#if RIFT
        if (leftCustom)
        {
            leftCustom.RefreshHandState();
        }
        if (rightCustom)
        {
            rightCustom.RefreshHandState();
        }
#endif

        alreadyTaughtTurning = true;
    }

    public void StartCheckForReturnToBat()
    {
        if (GameController.fullBatterMode)
        {
            return;
        }

        //print(">>>>> StartCheckForReturnToBat");
        if (!alreadyTaughtReturn && GameController.isAtBat==false)
        {
            StartCoroutine(CheckForReturnToBat());
        }
    }

    IEnumerator CheckForReturnToBat()
    {
        CustomHand previousHand = HandManager.GetAvailableDominantHand();

        while (previousHand == null)
        {
            //Debug.Log("waiting for a dominant hand.");
            yield return null;
            previousHand = HandManager.GetAvailableDominantHand();

            if (previousHand)
            {
                while (previousHand && !previousHand.renderModelsLoaded)
                {
                    yield return null;
                }
            }

        }

#if RIFT
        HandManager.currentLeftCustomHand.showRenderModel = true;
        HandManager.currentLeftCustomHand.SetMenuButtonHint(true);
        HandManager.currentLeftCustomHand.DisplayText("Press button \nto return to bat");
        //HandManager.GetAvailableDominantHand().DisplayText("Press the joystick \nto return to bat");
#elif STEAM_VR
        //previousHand.SetReturnToBatHint(true);
        Debug.Log("proper menu button hint on.");
        previousHand.SetMenuButtonHint(true);
        previousHand.showRenderModel = true;
        if (previousHand.otherHand)
        {
            Debug.Log("other hand show menu button.");
            previousHand.otherHand.showRenderModel = true;
            //previousHand.otherHand.SetReturnToBatHint(true);
            previousHand.otherHand.SetMenuButtonHint(true);
        }
        HandManager.GetAvailableDominantHand().DisplayText("Press button \nto return to bat");
#endif


        while (glove.gameObject.activeSelf)
        {
            yield return null;
        }

        //unhighlight both app buttons
        previousHand.SetMenuButtonHint(false);
        //previousHand.SetReturnToBatHint(false);
        previousHand.DisableText();
#if RIFT
        previousHand.RefreshHandState();
#endif
        if (previousHand.otherHand)
        {
            //previousHand.otherHand.showRenderModel = false;
            //previousHand.otherHand.SetReturnToBatHint(false);
            previousHand.otherHand.SetMenuButtonHint(false);
            previousHand.otherHand.DisableText();
#if RIFT
            previousHand.otherHand.RefreshHandState();
#endif
        }
        //HandManager.GetAvailableDominantHand().DisableText();
        alreadyTaughtReturn = true;
    }

    IEnumerator CheckForGrabbedAnything()
    {
#if RIFT
        grabTutorialObject.SetActive(true);
        CustomHand leftHand = null;
        CustomHand rightHand = null;
        OVRGrabber leftGrabber = null;
        OVRGrabber rightGrabber = null;
        bool somethingGrabbed = false;
        while (!somethingGrabbed)
        {
            leftHand = HandManager.currentLeftCustomHand;
            rightHand = HandManager.currentRightCustomHand;
            if (leftHand)
            {
                leftGrabber = leftHand.GetComponentInChildren<OVRGrabber>();
            }

            if (rightHand)
            {
                rightGrabber = rightHand.GetComponentInChildren<OVRGrabber>();
            }

            if (rightGrabber && rightGrabber.grabbedObject)
            {
                somethingGrabbed = true;
            }
            else if (leftGrabber && leftGrabber.grabbedObject)
            {
                somethingGrabbed = true;
            }

            yield return null;
        }

        grabTutorialObject.SetActive(true);
        alreadyTaughtPickup = true;
#elif STEAM_VR
        if (!thePlayer) {
			thePlayer = FindObjectOfType<Player> ();
		}
        Debug.Log(thePlayer);
        if (grabTutorialObject)
        {
            grabTutorialObject.SetActive(true);
        }

        bool somethingGrabbed = false;
        while(!somethingGrabbed)
        {
            if(GameController.fullBatterMode)
            {
                if (grabTutorialObject)
                {
                    grabTutorialObject.SetActive(false);
                }
                yield break;
            }

            if(thePlayer.leftHand)
            {
                foreach (Hand.AttachedObject a in thePlayer.leftHand.AttachedObjects)
                {
                    if(a.attachedObject.GetComponent<Interactable>())
                    {
                        somethingGrabbed = true;
                    }
                }
                if(!somethingGrabbed)
                {
                    foreach (Hand.AttachedObject a in thePlayer.rightHand.AttachedObjects)
                    {
                        //Debug.Log("right hand is attached to. "+a.attachedObject.name);
                        if (a.attachedObject.GetComponent<Interactable>())
                        {
                            somethingGrabbed = true;
                        }
                    }
                }
            }

            yield return null;
        }

        //Debug.Log("SOMETHING IS GRABBED!");

		//while ((thePlayer.leftHand==null || thePlayer.leftHand.currentAttachedObject==null) && (thePlayer.rightHand==null || thePlayer.rightHand.currentAttachedObject==null)) {
		//	yield return null;
		//}
		
#endif
        alreadyTaughtPickup = true;
        yield return new WaitForSeconds(1f);

        if (grabTutorialObject)
        {
            grabTutorialObject.SetActive(false);
        }
    }

    IEnumerator CheckForBallSpawned()
    {

        ballController.activeBalls = null;
        //Renderer[] highlights = ballRespawnTutorialObj.GetComponentsInChildren<Renderer> ();


        //yield return new WaitForSeconds(3f);

        //ballRespawnTutorialObj.SetActive(true);
        while (!GameController.levelIsBegun)
        {
            yield return null;
        }


        CustomHand previousHand = HandManager.GetAvailableDominantHand();

        while (previousHand == null)
        {
            //Debug.Log("waiting for a dominant hand.");
            yield return null;
            previousHand = HandManager.GetAvailableDominantHand();

            if (previousHand)
            {
                while (previousHand && !previousHand.renderModelsLoaded)
                {
                    yield return null;
                }
            }

        }

        previousHand.SetBallSpawnHint(true);
        if (previousHand.otherHand)
        {
            previousHand.otherHand.SetBallSpawnHint(true);
        }

        while ((ballController.activeBalls == null) && ballSpawner.currentServiceType != ballController.ballServiceType.self)
        {
            CustomHand dominantCustomHand = HandManager.GetAvailableDominantHand();
            if (dominantCustomHand)
            {
                //Debug.Log("there IS a custom hand.");
                if (previousHand && previousHand != dominantCustomHand)
                {
                    previousHand.DisableText();
                    previousHand.SetBallSpawnHint(false);
                    dominantCustomHand.SetBallSpawnHint(true);
                    if (dominantCustomHand.otherHand)
                    {
                        dominantCustomHand.otherHand.SetBallSpawnHint(true);
                    }
                }
#if RIFT
                /*
                if (customLeft)
        {
            if (HandManager.dominantHand == HandManager.HandType.left)
            {
                customLeft.SetBallSpawnHint(true);

                customLeft.DisplayText("Press the X button \nto spawn a ball on the tee");
            }
        }
        if (customRight)
        {
            if (HandManager.dominantHand == HandManager.HandType.right)
            {
                customRight.SetBallSpawnHint(true);
                customRight.DisplayText("Press the A button \nto spawn a ball on the tee");
            }
        }
        */
                dominantCustomHand.DisplayText("Press A or X \nto spawn a ball on the tee");

#elif STEAM_VR
                if (GameController.isOculusInSteam)
                {
                    dominantCustomHand.DisplayText("Press A or X \nto spawn a ball on the tee");
                }
                else
                {
                    dominantCustomHand.DisplayText("Press trackpad \nto spawn a ball on the tee");
                }
#endif
            }

            previousHand = dominantCustomHand;

            yield return null;
        }

        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.SetBallSpawnHint(false);
        }
        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.SetBallSpawnHint(false);
        }

#if RIFT
        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }
#endif

        alreadyTaughtSpawn = true;
        yield return new WaitForSeconds(1f);

        HandManager.GetAvailableDominantHand().DisableText();
        //end the tutorial.
    }

    public void StartCheckingForMenuButton()
    {
        if(alreadyTaughtMenu)
        {
            return;
        }
        Debug.Log("start checking for menu.");
        StartCoroutine(CheckForMenuButtonRoutine());
    }

    public static void AppearingMenuHasAppeared()
    {
        alreadyTaughtMenu = true;
    }

    IEnumerator CheckForMenuButtonRoutine()
    {
        AppearingMenu theMenu = FindObjectOfType<AppearingMenu>();

        bool previouslyOn = false;
        yield return new WaitForSeconds(1f);

        while (!alreadyTaughtMenu)
        {
            if (GameController.isAtBat)
            {
                if (!previouslyOn)
                {
                    if (HandManager.currentLeftCustomHand)
                    {
                        Debug.Log("menu hint left on");
                        HandManager.currentLeftCustomHand.SetMenuButtonHint(true);
                    }
                    if (HandManager.currentRightCustomHand)
                    {
                        Debug.Log("menu hint right on.");
                        HandManager.currentRightCustomHand.SetMenuButtonHint(true);
                    }
                    previouslyOn = true;
                }
                if (theMenu && theMenu.state != AppearingMenu.State.Invisible)
                {
                    Debug.Log("appearing menu is " + theMenu.state.ToString());
                    alreadyTaughtMenu = true;
                }
                
            }
            else
            {
                if (previouslyOn)
                {
                    if (HandManager.currentLeftCustomHand)
                    {
                        HandManager.currentLeftCustomHand.SetMenuButtonHint(false);
                    }
                    if (HandManager.currentRightCustomHand)
                    {
                        HandManager.currentRightCustomHand.SetMenuButtonHint(false);
                    }
                    previouslyOn = false;
                }
            }

            yield return null;
        }
        yield return null;
        Debug.Log("done teaching menu appearing.");
        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.SetMenuButtonHint(false);
#if RIFT
            HandManager.currentLeftCustomHand.RefreshHandState();
#endif 
        }
        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.SetMenuButtonHint(false);
#if RIFT
            HandManager.currentRightCustomHand.RefreshHandState();
#endif
        }



    }

}
