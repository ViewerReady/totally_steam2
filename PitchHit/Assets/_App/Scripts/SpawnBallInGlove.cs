﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBallInGlove : MonoBehaviour {
    public float minimumDistance = 1f;
    public HumanGloveController glove;
    public float delay = .5f;

    private spawnBall spawner;
    private hittable previousBall;
	// Use this for initialization
	void Start () {
        spawner = GetComponent<spawnBall>();
        StartCoroutine(SpawningRoutine());
	}

    IEnumerator SpawningRoutine()
    {
        while(GameController.levelIsBegun == false)
        {
            yield return null;
        }

        while(!glove || glove.attached==false)
        {
            yield return null;
        }

        while(true)
        {
            if (previousBall && Vector3.Distance(previousBall.transform.position, transform.position) > minimumDistance)
            {
                previousBall = null;
            }

            if (previousBall == null)
            {
                yield return new WaitForSeconds(delay);
                previousBall = spawner.NewBall(ballController.instance.ballPrefab);
                Debug.Log("Catch Previous ball. D");
                glove.CatchBall(previousBall);
                //glove.trajToWatch = previousBall.GetComponentInChildren<TrajTester1>();
                //previousBall.GetRigidbody().isKinematic = true;
                //previousBall.gameObject.tag = "ball_in_use";
            }
            yield return null;
        }
    }

}
