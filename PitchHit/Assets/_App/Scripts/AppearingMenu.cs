﻿using DarkTonic.MasterAudio;
using Michsky.UI.Zone;
using System.Collections;
using System.Collections.Generic;
using TButt;
using UnityEngine;
using UnityEngine.UI;

public class AppearingMenu : MonoBehaviour {
    public static bool isVisible;
    public bool isLockerRoomMenu;
    public bool hasBeenInitialized;
    public MainPanelManager mainPanelManager;
    public GameObject[] objectsToToggle;
    public bool isAlwaysAvailable;

    public GameObject uiPointer;
    public LineRenderer uiPointerVisual;

    [SerializeField] Transform announcerBoothPosition;

    TeamInfo selectedOpponent;
    [SerializeField] TeamInfo foxesTeamData;
    [SerializeField] TeamInfo slothsTeamData;
    [SerializeField] TeamInfo robinsTeamData;
    [SerializeField] TeamInfo phantsTeamData;
    int selectedNumInnings;

    [SerializeField] Image opponentSelectionScreenLogo;

    public bool determineBroadcastMode = false;

    protected DebugPanel debugPanel;

    #region UIElements
    [SerializeField] private UIOnOffToggle slowMotionToggle;
    [SerializeField] private UIOnOffToggle vignetteToggle;
    [SerializeField] private UIOnOffToggle ballIndicatorToggle;
    [SerializeField] private UIOnOffToggle handCannonToggle;
    #endregion

    [SerializeField]
    float menuDistance = 1f;
    Camera cam;
    float defaultY;
    SceneChanger sceneChanger;
    public string lockerSceneName = "FULL_LOCKERS";
    public string fullSceneName = "AI_LAB";
    public string tutorialSceneName = "TutorialField";

    private string rightHandedWord = "Righty";
    private string leftHandedWord = "Lefty";

    //public MenuButton refillButton;
    public enum State
    {
        Invisible,
        Appearing,
        Visible
    }
    State _state = State.Invisible;
    public State state
    {
        get
        { return _state; }
    }

    void Awake()
    {
        slowMotionToggle.Refresh(HumanPosessionManager.slowmoEnabled);
        vignetteToggle.Refresh(HumanPosessionManager.locomotionVignetteEnabled);
        ballIndicatorToggle.Refresh(HumanHUDManager.indicatorEnabled_saved);
        handCannonToggle.Refresh(HandManager.throwingType == HandManager.ThrowingVariant.CANNON);
    }

    // Use this for initialization
    void Start() {
        debugPanel = GetComponentInChildren<DebugPanel>();
        sceneChanger = GetComponent<SceneChanger>();

        if (isLockerRoomMenu)
        {
            if (uiPointer != null)
            {
                uiPointer.SetActive(true);
                if (HandManager.instance.cannon.isActivated)
                {
                    uiPointerVisual.enabled = false;
                }
                else
                {
                    uiPointerVisual.enabled = true;
                }
            }
            mainPanelManager.PanelAnim(4);
            return;
        }
        else
        {
            //Please go read the text above TurnOffAfterDelay's implementation.
            if (!hasBeenInitialized) StartCoroutine(TurnOffAfterDelay());
        }

        //defaultY = transform.position.y;  //This is the old line. If we figure out what the problem was we can change it back to this.
        defaultY = 0.879f;  //This is what the defaultY should always be for both fields. Sometimes (VERY RARELY) it wasn't this value. That's bad. So I'm making it always this value.
        transform.position = new Vector3(0, -10000, 0);
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update() {
        if (isLockerRoomMenu) return;
        if (true)//GameController.isAtBat || isAlwaysAvailable)
        {
#if RIFT
            if (HandManager.currentLeftCustomHand && HandManager.currentLeftCustomHand.GetMenuButtonDown())
#else
            if ((HandManager.currentLeftCustomHand && HandManager.currentLeftCustomHand.GetMenuButtonDown()) || (HandManager.currentRightCustomHand && HandManager.currentRightCustomHand.GetMenuButtonDown()))
#endif
            {
                if (_state == State.Invisible)
                {
                    TurnOn();
                }
                else
                {
                    TurnOff();
                }

            }
        }
    }

    void OnEnable()
    {
        HumanPosessionManager.OnPlayerPosessed += OnPlayerPossesed;
    }

    void OnDisable()
    {
        isVisible = false;
        HumanPosessionManager.OnPlayerPosessed -= OnPlayerPossesed;
    }

    public LeaderboardManager leaderboardManager;

    void TurnOn()
    {
        foreach (GameObject go in objectsToToggle) go.SetActive(true);
        if (uiPointer != null)
        {
            uiPointer.SetActive(true);
            if (HandManager.instance.cannon.isActivated)
            {
                uiPointerVisual.enabled = false;
            }
            else
            {
                uiPointerVisual.enabled = true;
            }
        }
        isVisible = true;
        Vector3 cameraForward = cam.transform.forward;
        Vector3 cameraForwardRotated = Vector3.Normalize(new Vector3(cameraForward.x, 0.0f, cameraForward.z));
        Vector3 cameraScale = cam.transform.lossyScale;
        Vector3 newMenuPosition = cam.transform.position + menuDistance * cameraScale.z * cameraForwardRotated;

        if(announcerBoothPosition != null)
        {
            if (FullGameDirector.currentHumanPlayerRole == BaseGameDirector.HumanPlayerRole.BROADCAST)
            {
                transform.position = announcerBoothPosition.position;
                transform.forward = announcerBoothPosition.forward;
                transform.localScale = cameraScale;

                TutorialController.AppearingMenuHasAppeared();
                _state = State.Appearing;
                AppearingFinished();
                if (leaderboardManager)
                {
                    leaderboardManager.OnAppearingMenu(true, transform.position);
                }
                return;
            }
        }

        if (FullGameDirector.currentHumanPlayerRole == BaseGameDirector.HumanPlayerRole.OFFENSE)
        {
            HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, false);
        }

        transform.forward = -cameraForwardRotated;

        if (HumanPosessionManager.playArea)
        {
            transform.position = new Vector3(newMenuPosition.x, (defaultY * cameraScale.y) + HumanPosessionManager.playArea.position.y, newMenuPosition.z);
        }
        else
        {
            transform.position = new Vector3(newMenuPosition.x, defaultY * cameraScale.y, newMenuPosition.z);
        }
        transform.localScale = cameraScale;

        TutorialController.AppearingMenuHasAppeared();
        _state = State.Appearing;
        AppearingFinished();
        if (leaderboardManager)
        {
            leaderboardManager.OnAppearingMenu(true, transform.position);
        }
    }

    public void TurnOff()  //AIFieldPlayer argument is only here so that we can subscribe this to a HumanPossessionManager Action.
    {
        foreach (GameObject go in objectsToToggle) go.SetActive(false);
        if (uiPointer != null) uiPointer.SetActive(false);
        isVisible = false;

        CancelInvoke();
        transform.position = new Vector3(0, -1000, 0);
        _state = State.Invisible;

        if (leaderboardManager)
        {
            leaderboardManager.OnAppearingMenu(false, transform.position);
        }

        if (FullGameDirector.currentHumanPlayerRole == BaseGameDirector.HumanPlayerRole.OFFENSE)
        {
            HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, true);
        }
    }

    void OnPlayerPossesed(AIFieldPlayer fieldPlayer)    //AIFieldPlayer argument is only here so that we can subscribe this to a HumanPossessionManager Action.
    {
        TurnOff();
    }

    //The below is hacky, temporary solution that needs to be improved.
    //If the menu isn't off in the scene by default, the game will run slightly slower.
    //But if the menu IS off by default, then it won't apply stuff like sound settings until it is turned on.
    //This hacky solution just gives it a tenth of a second to apply settings before turning the menu off. 
    //We need a solution where it applies settings even if the menu is off.
    //   - Bradley  1-29-2020
    private IEnumerator TurnOffAfterDelay()
    {
        yield return new WaitForSeconds(0.1f);
        hasBeenInitialized = true;
        TurnOff();
    }

    void AppearingFinished()
    {
        _state = State.Visible;
        slowMotionToggle.Refresh(HumanPosessionManager.slowmoEnabled);
        vignetteToggle.Refresh(HumanPosessionManager.locomotionVignetteEnabled);
        ballIndicatorToggle.Refresh(HumanHUDManager.indicatorEnabled_saved);
        handCannonToggle.Refresh(HandManager.throwingType == HandManager.ThrowingVariant.CANNON);
    }

    public void OnRestartButtonActivated()
    {
        sceneChanger.GoToScene(fullSceneName);
    }

    public void OnTutorialButtonActivated()
    {
        SetFullSceneName(tutorialSceneName);
        sceneChanger.GoToScene(fullSceneName);
    }

    public void OnExitButtonActivated()
    {
        sceneChanger.GoToScene(lockerSceneName);
    }

    public void OnHandednessButtonActivated()
    {
        ToggleHandedness();
    }

    public void ToggleDebug()
    {
        if (debugPanel) debugPanel._showDebug = !debugPanel._showDebug;
    }


    public void ToggleSlowMotionEnabled()
    {
        SetSlowMotionEnabled(!HumanPosessionManager.slowmoEnabled);
    }

    public void SetSlowMotionEnabled(bool enabled)
    {
        HumanPosessionManager.slowmoEnabled = enabled;
        slowMotionToggle.Refresh(enabled);
    }

    public void ToggleRunningVignetteEnabled()
    {
        SetRunningVignetteEnabled(!HumanPosessionManager.locomotionVignetteEnabled);
    }

    public void SetRunningVignetteEnabled(bool enabled)
    {
        HumanPosessionManager.locomotionVignetteEnabled = enabled;
        vignetteToggle.Refresh(enabled);
    }

    public void SetBallIndicatorEnabled(bool enabled)
    {
        HumanHUDManager.indicatorEnabled_saved = enabled;
        ballIndicatorToggle.Refresh(enabled);
    }

    public void ToggleHandCannonUse()
    {
        HandManager.ToggleThrowingType();
        handCannonToggle.Refresh(HandManager.throwingType == HandManager.ThrowingVariant.CANNON);
        if (uiPointer != null)
        {
            uiPointer.SetActive(true);
            if (HandManager.instance.cannon.isActivated)
            {
                uiPointerVisual.enabled = false;
            }
            else
            {
                uiPointerVisual.enabled = true;
            }
        }
    }

    public void SetHandCannonUse(bool useHandCannon)
    {
        HandManager.SetThrowingType((useHandCannon ? HandManager.ThrowingVariant.CANNON : HandManager.ThrowingVariant.MANUAL));
        handCannonToggle.Refresh(HandManager.throwingType == HandManager.ThrowingVariant.CANNON);
        if (uiPointer != null)
        {
            uiPointer.SetActive(true);
            if (HandManager.instance.cannon.isActivated)
            {
                uiPointerVisual.enabled = true;
            }
            else
            {
                uiPointerVisual.enabled = false;
            }
        }
    }

    public void SetLeftHanded(bool lefty)
    {
        if (lefty)
        {
//            Debug.Log("SetLefty " + Time.time);
            HandManager.SetDominantHand(HandManager.HandType.left);
        }
        else
        {
//            Debug.Log("SetLefty " + Time.time);

            HandManager.SetDominantHand(HandManager.HandType.right);
        }

        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }

#if RIFT
        /*
        OVRInput.SetControllerVibration(0f, 0f, HandManager.secondaryOculusHand);
        StartCoroutine(OculusHapticOverride(.25f));
        */
#else

        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.showRenderModel = true;
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.showRenderModel = true;
        }

        if (hitThings.instance && hitThings.instance.attached)
        {
            hitThings.instance.attachedCustomHand.showRenderModel = false;
        }
#endif

        HumanGloveController tempGlove = FindObjectOfType<HumanGloveController>();
        if (tempGlove && tempGlove.attached)
        {
            if (HandManager.dominantHand == HandManager.HandType.right)
            {
                tempGlove.shouldBeOnLeft = true;
            }
            else
            {
                tempGlove.shouldBeOnLeft = false;
            }
            tempGlove.SetAttached(true);
        }
        HandCannonController pitcherCannon = FindObjectOfType<HandCannonController>();
    }

   

    public void ToggleHandedness()
    {
        SetLeftHanded(HandManager.dominantHand != HandManager.HandType.left);
    }

    public void SelectOpponentSloths()
    {
        selectedOpponent = slothsTeamData;
        opponentSelectionScreenLogo.sprite = slothsTeamData.teamLogo;
    }

    public void SelectOpponentRobins()
    {
        selectedOpponent = robinsTeamData;
        opponentSelectionScreenLogo.sprite = robinsTeamData.teamLogo;
    }

    public void SelectOpponentPhants()
    {
        selectedOpponent = phantsTeamData;
        opponentSelectionScreenLogo.sprite = phantsTeamData.teamLogo;
    }

    public void SetFullSceneName(string newScene)
    {
        fullSceneName = newScene;
        if (TButt.TBCameraRig.isMobile)
        {
            fullSceneName += "_MOBILE";
        }
    }

    public void SetNumberOfInnings(int numInnings)
    {
        selectedNumInnings = numInnings;
    }

    public void DetermineGame_Full()
    {
        DebugTeamLoader.forceSettings = false;
        FullGameDirector.SetHomeTeam(foxesTeamData);
        FullGameDirector.SetAwayTeam(selectedOpponent);
        FullGameDirector.SetNumInnings(selectedNumInnings);
        if (determineBroadcastMode) FullGameDirector.PrepForBroadcast();
        else FullGameDirector.PrepForFullGame();
    }

    public void DetermineGame_OffenseOnly()
    {
        FullGameDirector.PrepForOffenseOnly();
    }

    public void DetermineGame_DefenseOnly()
    {
        FullGameDirector.PrepForDefenseOnly();
    }

    public void DetermineGame_Broadcast()
    {
        FullGameDirector.SetHomeTeam(foxesTeamData);
        FullGameDirector.SetAwayTeam(selectedOpponent);
        FullGameDirector.PrepForBroadcast();
    }

    public void SetBroadcastMode(bool b)
    {
        determineBroadcastMode = b;
    }

    public void SetAnnouncerVolume(float volume)
    {
        MasterAudio.SetBusVolumeByName("VO", volume);
    }

    public void SetAmbienceVolume(float volume)
    {
        MasterAudio.SetBusVolumeByName("Ambience", volume);
    }

    public void SetMenuSfxVolume(float volume)
    {
        MasterAudio.SetBusVolumeByName("Menu_SFX", volume);
    }

    public void SetGameSfxVolume(float volume)
    {
        MasterAudio.SetBusVolumeByName("SFX", volume);
    }

    void OnDestroy()
    {
        isVisible = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}