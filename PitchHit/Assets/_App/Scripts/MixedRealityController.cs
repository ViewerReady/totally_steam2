﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixedRealityController : MonoBehaviour {
    public GameObject[] adWords;
    // Use this for initialization
    //void Start () {

    //}

    private bool wordsVisible = true;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("f"))
        {
            transform.Rotate(Vector3.up * 180f);
        }

        if(Input.GetKeyDown("t"))
        {
            wordsVisible = !wordsVisible;

            foreach(GameObject g in adWords)
            {
                g.SetActive(wordsVisible);
            }
        }
	}
}
