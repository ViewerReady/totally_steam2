﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPitcher : pitchBall {

    private Rigidbody heldBall;
    public BallGate strikeZone;
    //public Transform ballGlovePositin;

    //public Transform rightHandRoot;

    //public Animator anim;

    public DefensePlayer pitcherPrefab;
    protected DefensePlayer pitcher;

    void Start()
    {
        pitcher = GameObject.Instantiate(pitcherPrefab, this.transform);
        pitcher.SetFieldPosition(DefenseFieldPosition.PITCHER, Vector3.forward * 28);
        pitcher.transform.localPosition = Vector3.zero;
        pitcher.transform.rotation = Quaternion.identity;

        pitcher.OnThrow += OnReleasePitch;
        pitcher.OverrideStrikeGate(strikeZone);
        //pitcher.position = this.transform.position;
        //pitcher.

    }

    new void OnEnable()
    {
        base.OnEnable();
        //anim.Play("Idle", 0);
    }

    new void OnDisable()
    {
        base.OnDisable();
        if(heldBall)
        {
            Destroy(heldBall.gameObject);
        }
    }

    

    public override void pitch(GameObject prefab)
    {
        hasPitched = false;
        Debug.Log("Dummy Pitcher pitch");
        Rigidbody theBall = Instantiate(prefab).GetComponent<Rigidbody>();

        pitcher.glove.ForceCatchBall(theBall.GetComponent<hittable>());
        pitcher.PlayPitchAnimation();

        
    }

    void OnReleasePitch(DefensePlayer player)
    {
        hasPitched = true;
    }




}
