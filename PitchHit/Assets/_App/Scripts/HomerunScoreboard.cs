﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class HomerunScoreboard : MonoBehaviour {

    SpriteRenderer ren;
    Animator anim;
    [SerializeField] float animationDuration;

    void Awake()
    {
        ren = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        ren.enabled = false;
    }

	public void PlayHomerunAnimation()
    {
        ren.enabled = true;
        StartCoroutine(AnimationRoutine());
    }

    IEnumerator AnimationRoutine()
    {
        int animNumber = Random.Range(1, 4);
        string animName = "Anim_" + animNumber.ToString();

        anim.SetBool(animName, true);

        float timer = 0f;
        while (timer < animationDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        anim.SetBool(animName, false);
        ren.enabled = false;
        yield return null;
    }
}
