﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeRunBall : MonoBehaviour {

    public enum LandingType { None, Short, WallShort, Barely, Stands, Sign, Lights, Far, Target}

    public ParticleSystemRenderer trail;
    public Material[] particleMaterials;
    public Collider giantCollider;
    public bool displayed;
    public LayerMask judgementLayerMask;
    private float maxParticleSpeed = 100f;
    

    private bool stillJudging;
    private bool isATrueHomeRun;
    private Vector3 collisionPosition;
    private Vector3 collisionNormal;
    private Vector3 spotBeforeCollision;
    //private Vector3 spotAtPeakElevation;
    private Vector3 directionOnImpact;
    private float timeOfCollision;
    private LandingType landingType;
    private int stepsBeforeCollisionToSave = 3;
    private HomeRunTarget collidingTarget;
    private hittable hittableComponent;

    public hittable GetHittableComponent()
    {
        if(!hittableComponent)
        {
            hittableComponent = GetComponent<hittable>();
        }

        return hittableComponent;
    }

    public void BeginJudgement()
    {
        StopAllCoroutines();
        StartCoroutine(JudgementRoutine());
    }

    public bool isJudging()
    {
        return stillJudging;
    }

    public bool isTrueHomeRun()
    {
        return isATrueHomeRun;
    }

    public Vector3 GetCollisionPosition()
    {
        return collisionPosition;
    }

    public Vector3 GetCollisionNormal()
    {
        return collisionNormal;
    }

    public Vector3 GetDirectionOnImpact()
    {
        return directionOnImpact;
    }

    public LandingType GetLandingType()
    {
        return landingType;
    }

    public void SetLandingType(LandingType type)
    {
        landingType = type;
    }

    public float GetLandingTime()
    {
        return timeOfCollision;
    }

    //public Vector3 GetSpotAtPeakElevation()
    //{
    //    return spotAtPeakElevation;
   // }

    public void SetLandingTime(float time)
    {
        timeOfCollision = time;
    }

    public Vector3 GetPositionBeforeCollision()
    {
        return spotBeforeCollision;
    }

    public void DetermineParticleTrail(float speed)
    {
        if (speed > 10f)
        {
            //Debug.Log("fast enough.");
            trail.gameObject.SetActive(true);
            if (particleMaterials != null && particleMaterials.Length > 0)
            {
                //Debug.Log("I choose material. #" + (Mathf.Min(Mathf.FloorToInt((speed / maxParticleSpeed) * particleMaterials.Length), particleMaterials.Length - 1)));
                trail.material = particleMaterials[Mathf.Min(Mathf.FloorToInt((speed / maxParticleSpeed) * particleMaterials.Length),particleMaterials.Length-1)];
            }
        }
    }

    IEnumerator JudgementRoutine()
    {
        bool everyOther = false;
        stillJudging = true;

        yield return new WaitForSeconds(.1f);
        Rigidbody rb = GetComponent<Rigidbody>();
        Vector3 lastJudgedPosition = transform.position;
       // spotAtPeakElevation = transform.position;
        Vector3 lastJudgedVelocity = rb.velocity;
        Queue<Vector3> oldSpots = new Queue<Vector3>();
        //Queue<Vector3> oldDirections = 

        float timeToMeasurePerFrame = .2f;
        float accumulatedFlightTime = 0f;
        float initialTime = Time.time;
        while (stillJudging)
        {
            Vector3 actualPosition = rb.transform.position;
            Vector3 actualVelocity = rb.velocity;

            rb.transform.position = lastJudgedPosition;
            rb.velocity = lastJudgedVelocity;
            //Debug.Log("still judging");
            RaycastHit ballHit;
            if(Trajectory.GetPointOfCollision(rb,out ballHit,timeToMeasurePerFrame))
            {
                
                stillJudging = false;
                collisionPosition = ballHit.point;
                collisionNormal = ballHit.normal;
                timeOfCollision = initialTime + accumulatedFlightTime;
                directionOnImpact = lastJudgedVelocity;
                switch (ballHit.collider.tag)
                {
                    case "wall":
                        if (Vector3.Angle(ballHit.normal, Vector3.up) < 40f)
                        {
                            isATrueHomeRun = true;
                            landingType = LandingType.Barely;
                        }
                        else
                        {
                            isATrueHomeRun = false;
                            landingType = LandingType.WallShort;
                        }
                        break;
                    case "target":
                        isATrueHomeRun = true;
                        landingType = LandingType.Target;
                        collidingTarget = ballHit.rigidbody.GetComponent<HomeRunTarget>();
                        break;
                    case "lights":
                        isATrueHomeRun = true;
                        landingType = LandingType.Lights;
                        break;
                    case "stands":
                        isATrueHomeRun = true;
                        landingType = LandingType.Stands;
                        break;
                    case "sign":
                        isATrueHomeRun = true;
                        landingType = LandingType.Sign;
                        break;
                    case "ground":
                        if (collisionPosition.magnitude > 190f)
                        {
                            isATrueHomeRun = true;
                            landingType = LandingType.Far;
                        }
                        else
                        {
                            landingType = LandingType.Short;
                            isATrueHomeRun = false;
                        }
                        break;
                    default:
                        isATrueHomeRun = true;
                        landingType = LandingType.None;
                        break;
                }

                if (oldSpots.Count > 1)
                {
                    spotBeforeCollision = oldSpots.Dequeue();
                }
                else
                {
                    spotBeforeCollision = collisionPosition;
                }
            }
            else
            {
                float futureElevation = lastJudgedPosition.y + (rb.velocity.y * timeToMeasurePerFrame) + (.5f * Physics.gravity.y * timeToMeasurePerFrame * timeToMeasurePerFrame);
                //if(futureElevation< lastJudgedPosition.y)
                //{
                //    spotAtPeakElevation = lastJudgedPosition;
                //}

                lastJudgedPosition = new Vector3(lastJudgedPosition.x + (rb.velocity.x * timeToMeasurePerFrame),
                                                   futureElevation,
                                                   lastJudgedPosition.z + (rb.velocity.z * timeToMeasurePerFrame));
                lastJudgedVelocity += Physics.gravity * timeToMeasurePerFrame; 

                if(lastJudgedPosition.y<0)
                {
                    stillJudging = false;
                    isATrueHomeRun = true;
                    landingType = LandingType.Far;
                    collisionPosition = Trajectory.GetLandingSpot(rb);
                    collisionNormal = Vector3.up;
                    timeOfCollision = Trajectory.timeOfPreviousLandingCalculation;
                    directionOnImpact = lastJudgedVelocity;
                }
                else
                {
                    accumulatedFlightTime += timeToMeasurePerFrame;
                }

                oldSpots.Enqueue(lastJudgedPosition);
                if(oldSpots.Count>stepsBeforeCollisionToSave)
                {
                    oldSpots.Dequeue();
                }
            }

            rb.transform.position = actualPosition;
            rb.velocity = actualVelocity;
            if (everyOther)
            {
                yield return null;
            }
            else
            {
                everyOther = true;
            }
        }
    }
    
    public HomeRunTarget GetHomeRunTarget()
    {
        return collidingTarget;
    }

    public void ClobberingTime()
    {
        giantCollider.enabled = true;
        GetComponent<Rigidbody>().mass *= 20f;
    }

}
