﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class CatchLevelController : MonoBehaviour {
    //public GameObject[] fireworksPrefabs;
    //public Transform[] fireworksSources;

    //public Transform[] fenceCorners;//make sure homeplate is first and that they are arranged in a circle;
    //public Transform homePlate;
    //public Transform thirdBase;
    //public Transform firstBase;

    SteamVR_Action_Boolean menuButtonAction;

    public int levelNumber = 1;
    public AppearingMenu appearingMenu;
    public AudioSource mainUISound;
    public AudioClip successClip;
    public int requiredPoints = 7;
    public float delayBeforeOutfield = 1f;
    public static HumanGearManager theHatManager;
#if GEAR_VR
    private float catchPositionVariance = 0f;
    private float maxCatchHeight = 1f;
    private float minCatchHeight = 1f;
#else
    private float catchPositionVariance = .5f;
    private float maxCatchHeight = 1.5f;
    private float minCatchHeight = .5f;
#endif

    private ScoreboardController theScoreboard;
    private HomeRunLevelController homeRunDetector;
    // private Vector2[] polyPoints;
    // private Vector2 homePlatePoint;
    //private Vector2 thirdBasePoint;
    //private Vector2 firstBasePoint;

    private Vector3 initialVRPosition;
    private Quaternion initialVRRotation;
    private bool isOutInField;
    private hitThings bat;
    public HumanGloveController glove;
    private Transform playArea;
    private Vector3 playAreaBounds;
    private Coroutine teleportationRoutine;

    void Awake()
    {
#if STEAM_VR
        menuButtonAction = SteamVR_Actions.baseballSet_MenuButton;
#endif
    }

    // Use this for initialization
    void Start () {
        theScoreboard = FindObjectOfType<ScoreboardController>();
        homeRunDetector = FindObjectOfType<HomeRunLevelController>();
        TrajTester1.catchLevelController = this;
        bat = FindObjectOfType<hitThings>();
        //glove = FindObjectOfType<Ball_caught>();

#if RIFT || GEAR_VR
        playArea = OVRManager.instance.transform;
        playAreaBounds = OVRManager.boundary.GetDimensions(OVRBoundary.BoundaryType.PlayArea)/2f;

#elif STEAM_VR
        playArea = Player.instance.transform;
        playAreaBounds = playArea.GetComponent<MeshRenderer>().bounds.extents;
        
#endif
        initialVRPosition = playArea.position;
        initialVRRotation = playArea.rotation;
        isOutInField = false;

        //StartCoroutine(FakeScoreRoutine());
    }

    IEnumerator FakeScoreRoutine()
    {
        yield return new WaitForSeconds(3f);
        theScoreboard.SetScore(8);
        yield return new WaitForSeconds(3f);
        OnCatch(false);
    }

    void Update()
    {
#if RIFT

        //CustomHand dominantCustomHand = HandManager.GetAvailableDominantHand();
        //if (dominantCustomHand.joystickPressed || dominantCustomHand.otherHand.joystickPressed) 
        if(HandManager.currentLeftCustomHand.GetMenuButtonDown())
        {
           SendBackToBat();
        }
#elif GEAR_VR
        if(OVRInput.GetDown(OVRInput.Button.Back))
        {
            SendBackToBat();
        }
#elif STEAM_VR
        bool sendBackButtonPressed = false;

        if (GameController.isOculusInSteam)
        {
            if (menuButtonAction.GetLastStateDown(SteamVR_Input_Sources.LeftHand))
            {
                SendBackToBat();
                sendBackButtonPressed = true;
            }
            /*
            if (tempController.GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu))
            {
                Player.instance.transform.Rotate(Vector3.up, 180f, Space.World);
            }*/
        }
        else
        {
            if (menuButtonAction.GetLastStateDown(SteamVR_Input_Sources.LeftHand))
            {
                SendBackToBat();
                sendBackButtonPressed = true;
            }
        }

        if (!sendBackButtonPressed)
        {
            if (GameController.isOculusInSteam)
            {
                if (menuButtonAction.GetLastStateDown(SteamVR_Input_Sources.RightHand))
                {
                    SendBackToBat();
                }
                //if (tempController.GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu))
                //{
                //    Player.instance.transform.Rotate(Vector3.up, 180f, Space.World);
                //}
            }
            else
            {
                if (menuButtonAction.GetLastStateDown(SteamVR_Input_Sources.RightHand))
                {
                    SendBackToBat();

                }
            }
        }
#endif
    }

    public void BeginSendToOutfield(TrajTester1 currentTraj)
    {
        if(teleportationRoutine!=null)
        {
            StopCoroutine(teleportationRoutine);
        }
        StartCoroutine(SendToOutfield(currentTraj));
    }

    CustomHand gloveHand;
    public IEnumerator SendToOutfield(TrajTester1 currentTraj)
    {
        if (!ballController.readyForBalls)// && hittableComponent.GetOrigin() ==ballController.ballServiceType.tee)//only one ball at a time
        {
            //Debug.Log("NEVERMIND");
            yield break;
        }

        
        
        if (bat.attachedCustomHand == HandManager.currentRightCustomHand)
        {
            //Debug.Log("the bat was on the right hand!");
            if (HandManager.currentLeftCustomHand)
            {
                glove.shouldBeOnLeft = true;
                gloveHand = HandManager.currentLeftCustomHand;
            }
            else
            {
                glove.shouldBeOnLeft = false;
                gloveHand = HandManager.currentRightCustomHand;
            }
        }
        else
        {
            //Debug.Log("the bat was on the left hand!");
            if (HandManager.currentRightCustomHand)
            {
                glove.shouldBeOnLeft = false;
                gloveHand = HandManager.currentRightCustomHand;
            }
            else
            {
                glove.shouldBeOnLeft = true;
                gloveHand = HandManager.currentLeftCustomHand;
            }
        }

        yield return new WaitForSeconds(delayBeforeOutfield);
        if (!glove || glove.attached)
        {
            Debug.Log("break early no glove or already glove "+glove);
            yield break;
        }

        
        

        Rigidbody rigid = currentTraj.GetComponent<Rigidbody>();

        //teleport and 
        Vector3 landingSpot = Trajectory.GetLandingSpot(rigid);
        Vector3 chestHighSpot = Trajectory.GetLandingSpot(rigid, Random.Range(minCatchHeight, maxCatchHeight));
        //Debug.DrawRay(chestHighSpot, Vector3.up * 10f, Color.blue,30f);
        if ((currentTraj.hittableComponent.isScored==false) && (landingSpot.magnitude > 2f || Trajectory.airTimeCalculation>delayBeforeOutfield+.1f))
        {
            if (homeRunDetector.ContainsPoint(landingSpot))
            {
                //Debug.Log("contains point.");
                if (homeRunDetector.isPointFoulBall(landingSpot))
                {
                    theScoreboard.FoulBall(true);
                    //rend.material.color = Color.black;
                }
                else
                {
                    theScoreboard.GoodBall();
                    //rend.material.color = Color.green;
                }

                initialVRPosition = playArea.position;
                initialVRRotation = playArea.rotation;
                isOutInField = true;

                //playerBounds.enabled = true;

                playArea.position = new Vector3(chestHighSpot.x, playArea.position.y, chestHighSpot.z);

                playArea.position += playArea.forward * ((Random.value * 2f) - 1f) * playAreaBounds.z * catchPositionVariance;
                playArea.position += playArea.right * ((Random.value * 2f) - 1f) * playAreaBounds.x * catchPositionVariance;

                playArea.rotation = Quaternion.AngleAxis(180f, Vector3.up) * initialVRRotation;

                bat.holdable = false;
                bat.onRelease();
                bat.gameObject.SetActive(false);

                glove.SetAttached(true, gloveHand);
#if GEAR_VR
            //glove.ballToWatch = currentTraj.transform;
            glove.trajToWatch = currentTraj;
#endif

                //if (sound)
                //{
                //sound.Play();
                //}
                GameController.isAtBat = false;
                //HumanGearManager.instance.RefreshHatStatus();
                HumanGearManager.instance.ShowDefenderGear();
            }
            else
            {
                Debug.DrawRay(landingSpot, Vector3.up * 10f, Color.red, 300f);
                //Debug.Log("no good.");
                if (homeRunDetector.isPointFoulBall(landingSpot))
                {
                    //GameController.isAtBat = false;
                    //rend.material.color = Color.black;
                    theScoreboard.FoulBall(false);
                }
                else
                {
                    //rend.material.color = Color.red;
                    theScoreboard.HomeRun();
                }
            }
        }
        //markerPrefab.transform.Rotate(Vector3.up, 1 * Time.deltaTime);
        //GameObject.FindGameObjectWithTag("Glove").SetActive(true);

        //GameObject.FindGameObjectWithTag("bat").SetActive(false);

    }

    public void OnCatch(bool success)
    {
        if (GameController.fullBatterMode)
        {
            if(success)
            {
                theScoreboard.AddScore();
            }
            else
            {
                if (theScoreboard)
                {
                    theScoreboard.ClearScore();
                }
            }
            if (teleportationRoutine != null)
            {
                StopCoroutine(teleportationRoutine);
            }
                teleportationRoutine = StartCoroutine(BackToBatAfterDelay());
            return;
        }

        TutorialController tutorial = FindObjectOfType<TutorialController>();
        if(tutorial)
        {
            tutorial.StartCheckForReturnToBat();
        }

        //print("> OnCatch");
        //(showHint());
        if (success)
        {
            Debug.Log("add score to catch level scoreboard.");
            theScoreboard.AddScore();
            //TutorialController tutorial = FindObjectOfType<TutorialController>();
            //if(tutorial)
            //{
            //    tutorial.StartCheckForReturnToBat();
            //}
        }
        else
        {
            Debug.Log("clear score to catch level scoreboard.");
            if (theScoreboard)
            {
                theScoreboard.ClearScore();
            }
           // StartCoroutine(showHint());
        }

    }
    public IEnumerator showHint()
    {
        yield return new WaitForSeconds(1.5f);
        TutorialController tutorial = FindObjectOfType<TutorialController>();
        if (tutorial)
        {
            tutorial.StartCheckForReturnToBat();
        }
    }

    IEnumerator BackToBatAfterDelay(float delay=3f)
    {
        yield return new WaitForSeconds(delay);
        SendBackToBat();
    }

    public void SendBackToBat()
    {
        if (isOutInField)
        {
            GameController.isAtBat = true;

            isOutInField = false;
            //CameraVR.transform.position = new Vector3(0, 0, 0);
            playArea.position = initialVRPosition;
            playArea.rotation = initialVRRotation;
            HumanGearManager.instance.ShowBatterGear();

            //HumanGearManager.instance.RefreshHatStatus();

            Debug.Log("successfully teleported back");
            ballController.readyForBalls = true;
            TrajTester1.bat.gameObject.SetActive(true);
            TrajTester1.bat.holdable = true;
            
            if(glove)
            {
                glove.SetAttached(false);
                Debug.Log("ball in hand? "+glove.hasBall);
                if(glove.hasBall)
                {
                    Debug.Log("there is certainly a ball do destroy.");
                    hittable ball = glove.ReleaseBall();
                    Destroy(ball.gameObject);
                }
            }

            //Player.instance.cap.GetComponent<MeshRenderer>().enabled = false; 
            //HatManager.helmet.SetActive(true);
            //TrajTester1.playerBounds.enabled = false;

            /*
            if (traj)
            {
                //Debug.Log("successfully destroyed "+traj.name);
                Destroy(traj.gameObject);
            }
            else
            {
                if (otherHand.traj)
                {
                    //Debug.Log("destroyed other hand's traj "+otherHand.traj.name);
                    Destroy(otherHand.traj.gameObject);
                }
                else
                {
                   // Debug.Log("there was no traj to destroy");
                }
            }
            */
        }
    }

    public void CheckIfChallengePassed()
    {
        if(LockerRoomLevelController.isDemo)
        {
            return;
        }
        //Debug.Log("check challenge " + Time.time);
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if (levelsUnlocked == levelNumber)
        {
            if (ScoreboardController.instance.myScore >= requiredPoints)
            {
                //Debug.Log("yep");

                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (mainUISound && successClip)
                    {
                        mainUISound.clip = successClip;
                        mainUISound.Play();
                    }
                }
            }
        }
    }
}
