﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundController : MonoBehaviour {
    private static SoundController _instance;
    public static SoundController instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SoundController>();
            }

            return _instance;
        }
    }


    public GameObject sfxPrefab;
    //public AudioSource musicSource;
    //public AudioClip pregameMusic;
    //public AudioClip gameMusic;
    //public AudioClip postGameMusic;
    public int sfxPoolSize = 10;
    //public FlashbackAudioAssist flashbackAudioAssist;
    public bool sfxOnly;
    private AudioSource[] sfxPool;
    private int currentSource;
    private bool setupComplete;
    // Use this for initialization
    private float initialMusicVolume;
    public static bool isPlayerTwo;
    private bool musicShouldBePlaying;

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        
        //isPlayerTwo = MultiplayerManager.instance && SideController.playerNumber == 2; 

        StartCoroutine(CreationRoutine());
        /*
        if (pregameMusic)
        {
            musicShouldBePlaying = true;
            musicSource.clip = pregameMusic;
            if (!isPlayerTwo && !sfxOnly)
            {
                musicSource.Play();
            }
        }

        initialMusicVolume = musicSource.volume;
        */
    }

    IEnumerator CreationRoutine()
    {
        sfxPool = new AudioSource[sfxPoolSize];
        for (int x = 0; x < sfxPoolSize; x++)
        {
            yield return null;

            sfxPool[x] = (Instantiate(sfxPrefab, transform) as GameObject).GetComponent<AudioSource>();
            sfxPool[x].name = "Sound Maker " + x;
        }

        setupComplete = true;
    }
    
   


    public void PlaySoundAt(AudioClip clip, Vector3 position, float volume=1,Transform parent = null)
    {
        //Debug.Log("play sound? "+volume);
        if(!setupComplete)
        {
           // Debug.Log("too soon");
            return;
        }

        //Debug.Log("sound "+currentSource+ " gets "+clip.name);
        sfxPool[currentSource].volume = volume;
        sfxPool[currentSource].transform.position = position;
        sfxPool[currentSource].transform.SetParent(parent);
        sfxPool[currentSource].clip = clip;
        sfxPool[currentSource].Play();

        currentSource++;
        if(currentSource>=sfxPool.Length)
        {
            currentSource = 0;
        }
        
    }
}
