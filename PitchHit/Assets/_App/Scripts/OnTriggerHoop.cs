﻿//Ali

using UnityEngine;
using System.Collections;

public class OnTriggerHoop : MonoBehaviour {

    //scores
    public int plusPoints = 5;

    //audio for a successful hoop
    private AudioSource audioSource;
    public AudioClip audioClip;

    private HoopsLevelController hoopsLevel;

	void Start () 
    {

        audioSource = gameObject.AddComponent<AudioSource>();

        //initializing score

        if (!hoopsLevel)
        {
            hoopsLevel = FindObjectOfType<HoopsLevelController>();
        }
	}
	
	void OnTriggerEnter(Collider other)
    {
		hittable ball = other.GetComponent<hittable> ();
		if (ball && ball.GetWasHitByBat()) {
			ball.distance = transform.InverseTransformPoint (ball.transform.position).z;
		}

		/*
        if(other.tag == "ball")
        {
            //audio
            audioSource.clip = audioClip;
            audioSource.Play();

            //score
            score.scorePopup(plusPoints, transform.position);
            score.score += plusPoints;

            if(hoopsLevel)
            {
                hoopsLevel.CheckIfChallengePassed();
            }
        }
        */
    }

	void OnTriggerStay(Collider other)
	{
		hittable ball = other.GetComponent<hittable> ();
		if (ball && ball.GetWasHitByBat()) {
			if (Mathf.Sign (transform.InverseTransformPoint (ball.transform.position).z) != Mathf.Sign (ball.distance)) {
				ball.distance *= -1f;

				//audio
				audioSource.clip = audioClip;
				audioSource.Play();

                //score
                //ScoreboardController.instance.scorePopup(plusPoints, transform.position);
                ScoreboardController.instance.AddScore(plusPoints);

				if(hoopsLevel)
				{
					hoopsLevel.CheckIfChallengePassed();
				}
			}
		}
	}
}
