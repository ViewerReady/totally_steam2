﻿

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//using OVRTouchSample;
using Valve.VR.InteractionSystem;
public class Grabber : MonoBehaviour
{
    //public enum HandState { None, Empty, Ball, LargeBall, Pointing};

    public CustomHand customHand;
    public GameObject handVisual;
    //public HandState currentHandState;
    //looks better than movingt each frame
    private bool m_parentHeldObject = true;
    public Transform m_gripTransform = null;
    public bool snapBallsToGrip;
    private Collider m_grabCollider = null;

    private bool m_collisionEnabled = true;
    private bool m_grabVolumeEnabled = true;
    private Vector3 m_lastPos;
    private Quaternion m_lastRot;
    private Quaternion m_anchorOffsetRotation;
    private Vector3 m_anchorOffsetPosition;
    private TrackedController m_trackedController = null;
    private CustomGrabbable m_grabbedObj = null;

    public const float THRESH_THROW_SPEED = 1.0f;

    public bool canGrabAnything = true;
    public LayerMask hoverLayerMask = -1;
    private float highestGripLevel = 0;
    //

    private Valve.VR.InteractionSystem.VelocityEstimator m_velocityTracker = null;

    public CustomGrabbable GetCurrentGrabbable()
    {
        return m_grabbedObj;
    }


    public void ForceRelease(CustomGrabbable grabbable)
    {
        bool canRelease = (
            (m_grabbedObj != null) &&
            (m_grabbedObj == grabbable)
        );
        if (canRelease)
        {
            GrabEnd();
        }
    }
    private void Awake()
    {
        //m_anchorOffsetPosition = transform.localPosition;
        //m_anchorOffsetRotation = transform.localRotation; 
    }

    public void HideGrabber()
    {
        if (handVisual != null) handVisual.SetActive(false);
    }

    public void ShowGrabber()
    {
        if (handVisual != null) handVisual.SetActive(true);
    }



    private void Start()
    {

        m_velocityTracker = GetComponentInChildren<VelocityEstimator>();
        m_grabCollider = this.GetComponent<Collider>();
        //  CollisionEnable(false);
        m_lastPos = transform.position;
        m_lastRot = transform.rotation;
        //oculus
        m_trackedController = GetComponentInParent<TrackedController>();
        if (m_gripTransform == null)
        {
            m_gripTransform = this.transform;
        }

    }


    public void DropAll()
    {
        GrabEnd();
    }

    private void Update()
    {


        CheckForGrabOrRelease(0);
    }
  
    private void FixedUpdate()
    {
        if (!m_parentHeldObject && m_grabbedObj != null)
        {
            if (m_grabbedObj.draggable)
            {
                MoveGrabbedObject();
            }
        }

        m_lastPos = transform.position;
        m_lastRot = transform.rotation;
    }

    private void LateUpdate()
    {

    }

    private void OnDestroy()
    {
        if (m_grabbedObj != null)
        {
            GrabEnd();
        }
    }


    private void CollisionEnable(bool enabled)
    {
        if (m_collisionEnabled == enabled)
        {
            return;
        }
        m_collisionEnabled = enabled;

        if (enabled)
        {
            m_grabCollider.enabled = true;
        }
        else
        {
            m_grabCollider.enabled = false;
        }
    }

    private void CheckForGrabOrRelease(float prevFlex)
    {
     
        if (customHand.GetGripDown())
        {
            //print("----------------------------------------------GetGripDown " + Time.time);
            GrabBegin();
        }
        else if (customHand.GetGripUp())
        {
           // print("----------------------------------------------GetGripUp " + Time.time);
            GrabEnd();
        }

    }

    private void GrabBegin()
    {
        if(canGrabAnything == false)
        {
            return;
        }
      //.  Debug.Log("GrabBegin " + Time.time);

        // check if bat is in hand?
        CustomGrabbable closestGrabbable = null;
        Collider closestGrabbableCollider = null;
        float grabRadius = .2f;
        float closestRadiusSqr = Mathf.Infinity;
        //consider adding some sort of grabbable layermask.
        Collider[] grabCandidates = Physics.OverlapSphere(m_gripTransform.position, grabRadius, hoverLayerMask);
      //  Debug.Log("grab candidates "+grabCandidates.Length);
        // Iterate grab candidates and find the closest grabbable candidate
        foreach (Collider grabCollider in grabCandidates)
        {
            //Debug.Log("GRAB CANDIDATE: " + grabCollider.gameObject.name);
            if (grabCollider.attachedRigidbody == null || grabCollider.attachedRigidbody.GetComponent<CustomGrabbable>()==null)
            {
                continue;
            }
            float grabDistanceSq = (grabCollider.ClosestPointOnBounds(m_gripTransform.position) - m_gripTransform.position).sqrMagnitude;

            //float grabbableMagSq = (m_gripTransform.position - closestPointOnBounds).sqrMagnitude;
            if (grabDistanceSq < closestRadiusSqr)
            {
                closestRadiusSqr = grabDistanceSq;
                closestGrabbableCollider = grabCollider;
            }
        }

        if(closestGrabbableCollider != null)
        {
            //Debug.Log("grabbing collider "+closestGrabbableCollider.name);
            //Debug.Log(closestGrabbableCollider.transform.parent.name);
            closestGrabbable = closestGrabbableCollider.attachedRigidbody.GetComponent<CustomGrabbable>();
        }


        // Disable grab volumes to prevent overlaps
        // GrabVolumeEnable(false);

        if (closestGrabbable != null)
        {
            //we found a grabble so grab it

            Debug.Log("closest grabbable " + closestGrabbable.name);
            m_grabbedObj = closestGrabbable;

            customHand.RefreshHandState();

            if (m_grabbedObj.GrabbedHand != null)
            {
                //print("ALREADY GRABBED");
                //already grabbed by other hand
                m_grabbedObj.GrabbedHand.SendMessage("SwitchingFromHand");

            }

            //m_grabbedObj.GrabBegin(this, closestGrabbableCollider);



            // Teleport on grab, to avoid high-speed travel to dest which hits a lot of other objects at high
            // speed and sends them flying. The grabbed object may still teleport inside of other objects, but fixing that
            // is beyond the scope of this demo.

            m_lastPos = transform.position;
            m_lastRot = transform.rotation;

            // MoveGrabbedObject(true);

            m_anchorOffsetPosition = m_grabbedObj.transform.position - transform.position;

            if (m_grabbedObj.RotationFrozen)
            {         
                m_parentHeldObject = false;          
                //Debug.Log("grabbed something with rotation frozen");
            }
            else
            {
                m_parentHeldObject = true;
                //Debug.Log("grabbed something to be parented.");
                m_grabbedObj.transform.SendMessage("OnGrabbed", this, SendMessageOptions.DontRequireReceiver);
                if(snapBallsToGrip)
                {
                    if(m_grabbedObj.tag == "ball")
                    {
                        m_grabbedObj.transform.SetParent(m_gripTransform);
                        m_grabbedObj.transform.localPosition = Vector3.zero;
                    }
                    else
                    {
                        m_grabbedObj.transform.SetParent(transform);
                    }
                }
                else
                {
                    m_grabbedObj.transform.SetParent(transform);
                }
            }
        }
    }

    private void MoveGrabbedObject(bool forceTeleport = false)
    {
        if (m_grabbedObj == null)
        {
            return;
        }
    
        m_grabbedObj.GrabbedRigidbody.MovePosition(transform.position + m_anchorOffsetPosition);

    }

    private void GrabEnd()
    {
        if (m_grabbedObj == null) return;
        if (m_grabbedObj.GrabbedHand != this)
        {
            m_grabbedObj = null;
            return; //makes sure releasing doesn't effect other hand
        }

    }

    public void SwitchingFromHand()
    {
        m_grabbedObj.GrabEnd( Vector3.zero, Vector3.zero);
        m_grabbedObj = null;
        customHand.RefreshHandState();
    }

    private void GrabbableRelease(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        m_grabbedObj.GrabEnd(linearVelocity, angularVelocity);

        if (m_parentHeldObject)
        {
            Vector3 savedPosition = m_grabbedObj.transform.position;
            Vector3 savedScale = m_grabbedObj.transform.lossyScale;
            Quaternion savedRotation = m_grabbedObj.transform.rotation;
            m_grabbedObj.transform.SendMessage("OnUngrabbed", this, SendMessageOptions.DontRequireReceiver);
            m_grabbedObj.transform.parent = null;
            m_grabbedObj.transform.position = savedPosition;
            m_grabbedObj.transform.localScale = savedScale;
            m_grabbedObj.transform.rotation = savedRotation;
        }
        m_grabbedObj = null;
        customHand.RefreshHandState();
    }
}
