﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class VRMovable : MonoBehaviour 
{
    SteamVR_Action_Boolean grabObject;
    SteamVR_Action_Vibration haptics;

    private List<Hand> holdingHands = new List<Hand>();
	private List<Rigidbody> holdingBodies = new List<Rigidbody>();
	private List<Vector3> holdingPoints = new List<Vector3>();

	private Rigidbody rb;

	private bool justAttached = false;

	void Awake()
    {
        grabObject = SteamVR_Actions.baseballSet_GrabObject;
        haptics = SteamVR_Actions.baseballSet_Haptic;

        rb = GetComponent<Rigidbody> ();
	}

	void OnHandHoverBegin( Hand hand )
	{     
        if ( holdingHands.IndexOf( hand ) == -1 )
		{
            haptics.Execute(0f, 0.2f, 150, 75, hand.handType);
		}
	}


	//-----------------------------------------------------
	void OnHandHoverEnd( Hand hand )
	{
        if ( holdingHands.IndexOf( hand ) == -1 )
		{
            if (holdingHands.IndexOf(hand) == -1)
            {
                haptics.Execute(0f, 0.2f, 150, 75, hand.handType);
            }
        }
	}


	//-----------------------------------------------------
	void HandHoverUpdate( Hand hand )
	{
        if (grabObject.GetLastStateDown(hand.handType))
        {
            PhysicsAttach(hand);
        }
	}

	void PhysicsAttach (Hand hand)
    {
		//justAttached = true;
		//StopCoroutine ("toggleJustAttached");
		//StartCoroutine ("toggleJustAttached");
		hand.HoverLock (null);
		//rb.isKinematic = false;
		holdingBodies.Add (rb);
		holdingHands.Add (hand);
        //holdingPoints.Add (hand.hoverSphereTransform.position - rb.position);
        holdingPoints.Add(transform.InverseTransformPoint(hand.hoverSphereTransform.position));

        Debug.Log ("Grabbing bucket");
	}

	//IEnumerator toggleJustAttached()
    //{
	//	yield return new WaitForSeconds (0.5f);
	//	justAttached = false;
	//}


	private bool PhysicsDetach( Hand hand )
	{
        Debug.Log("Detaching bucket");

        if (justAttached) {
			return false;
		}
		int i = holdingHands.IndexOf( hand );

		if ( i != -1 )
		{

			// Allow the hand to do other things
			holdingHands[i].HoverUnlock( null );

			holdingBodies [i].isKinematic = true;

			FastRemove( holdingHands, i );
			FastRemove( holdingBodies, i );
			FastRemove( holdingPoints, i );

			return true;
		}

		return false;
	}
    /*
	void OnCollisionEnter(Collision collisionInfo)
    {
		if (collisionInfo.rigidbody == null || !collisionInfo.rigidbody.CompareTag ("ball")) 
        {
			Rigidbody rb;
			Vector3 movePoint;
			for (int i = 0; i < holdingHands.Count; i++) 
            {
				rb = holdingBodies [i];
				movePoint = collisionInfo.contacts [0].normal * collisionInfo.relativeVelocity.magnitude / 7.5f;
				movePoint.y *= 0.2f;
				//Debug.Log (movePoint.ToString());
				PhysicsDetach (holdingHands [i]);
				rb.MovePosition(rb.position + movePoint);
				//rb.position = new Vector3(rb.position.x, rb.position.y + (collisionInfo.contacts [0].normal.y), rb.position.z);
			}
		}
	}
    */
	float dampenMovementRate = 0.5f;
	void FixedUpdate()
	{
		for ( int i = 0; i < holdingHands.Count; i++ )
		{
			Vector3 targetPoint = transform.TransformPoint( holdingPoints[i] );
			Vector3 vdisplacement = (holdingHands[i].hoverSphereTransform.position - targetPoint);
			vdisplacement.x *= dampenMovementRate;
			vdisplacement.z *= dampenMovementRate;
			vdisplacement.y *= 0.6f;

			holdingBodies [i].MovePosition (holdingBodies[i].position + vdisplacement);

            if (!grabObject.GetLastState(holdingHands[i].handType))
            {
                Debug.Log(holdingHands[i].name + " not holding " + name);
                PhysicsDetach(holdingHands[i]);
            }
		}
	}

	private T FindOrAddComponent<T>( GameObject gameObject ) where T : Component
	{
		T component = gameObject.GetComponent<T>();
		if ( component )
			return component;

		return gameObject.AddComponent<T>();
	}

	private void FastRemove<T>( List<T> list, int index )
	{
		list[index] = list[list.Count - 1];
		list.RemoveAt( list.Count - 1 );
	}
}
