﻿using UnityEngine;
using System.Collections;

public class HoopsLevelController : MonoBehaviour {
    public int levelNumber = 0;
    public AppearingMenu appearingMenu;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredPoints = 15;


    void Awake()
    {
        //PlayerPrefs.SetInt("LEVELSUNLOCKED", levelNumber);
    }

    public void CheckIfChallengePassed()
    {
        if(LockerRoomLevelController.isDemo)
        {
            return;
        }

        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if(levelsUnlocked==levelNumber)
        {
            if(ScoreboardController.instance.myScore >= requiredPoints)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (sound && successClip)
                    {
                        sound.clip = successClip;
                        sound.Play();
                    }
                }
            }
        }
    }
}
