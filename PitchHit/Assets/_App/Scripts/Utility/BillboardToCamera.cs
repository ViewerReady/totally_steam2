﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class BillboardToCamera : MonoBehaviour {
    
    public bool flip_x = false;
    public bool lock_z = true;
    

    void LateUpdate () {

        if (Camera.main != null)
        {
                Vector3 lookPos = Camera.main.transform.position;

                this.transform.LookAt(lookPos, lock_z ? Vector3.up : Camera.current.transform.up);
                if (flip_x) this.transform.Rotate(Vector3.up * 180, Space.Self);
            
        }
	}
}
