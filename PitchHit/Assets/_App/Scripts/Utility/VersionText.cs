﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TextMeshPro))]
public class VersionText : MonoBehaviour {

    private TextMeshPro version;
	// Use this for initialization
	void Start () {
        version = GetComponent<TextMeshPro>();
        string output = "v" + Application.version;
#if RIFT
        output += " [Oculus]";
#endif

#if STEAM_VR
        output += " [SteamVR]";
#endif
        Debug.Log("Pitch Hit " + output);

        version.text = output;
	}

}
