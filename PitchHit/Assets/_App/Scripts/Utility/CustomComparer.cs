﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApproxCompare : IComparer<float>
{
    float tolerance = float.Epsilon;
    public ApproxCompare(float tolerance)
    {
        this.tolerance = tolerance;
    }
    public int Compare(float x, float y)
    {
        float diff = Mathf.Abs(x - y);

        if (diff <= tolerance) return 0;

        return x.CompareTo(y);
        
    }
}
