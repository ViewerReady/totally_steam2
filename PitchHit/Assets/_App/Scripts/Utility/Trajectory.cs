﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// A class of static functions for predicting information about the path of
/// a rigidbody projectile, assuming it has zero drag and constant vertical gravity
/// </summary>
public class Trajectory : MonoBehaviour
{

    public static float timeOfPreviousLandingCalculation;
    public static float airTimeCalculation;
    public static Vector3 previousLandingSpotLocation;

    private const float timeBetweenSteps = .05f;
    private const float defaultMaxPredictionTime = 10f;


    /// <summary>
    /// Assuming NO COLLISIONS and that the projectile has zero drag or thrust and
    /// consistent vertical gravity, this will return the position where it will 
    /// pass through the given elevation.
    /// 
    /// Zero is the default target elevation if none is given.
    /// </summary>
    /// <returns>The landing spot.</returns>
    /// <param name="projectile">Projectile.</param>
    /// <param name="targetElevation">Target elevation.</param>
    public static Vector3 GetLandingSpot(Rigidbody projectile, float targetElevation = 0)
    {
        float t = 0;
        float initialUpwardsSpeed = projectile.velocity.y;
        //Debug.Log("<color=cyan>Projectilve Y Velocity: " + initialUpwardsSpeed +" </color>");

        float a = .5f * Physics.gravity.y;
        float b = initialUpwardsSpeed;
        float c = -(targetElevation - projectile.position.y);
        float cluster = (b * b) - (4f * a * c);

        if (cluster >= 0 && a != 0)
        {
            //I think t2 will always be the answer but whatever. 
            //This will check both halves of the quadratic equation.
            float t1 = (-b + Mathf.Sqrt(cluster)) / (2f * a);
            float t2 = (-b - Mathf.Sqrt(cluster)) / (2f * a);
            t = Mathf.Max(t1, t2);
        }
        else
        {
            Debug.Log("invalid");
        }
        timeOfPreviousLandingCalculation = t + Time.time;
        airTimeCalculation = t;
        //Debug.Log("<color=orange>Air time calculation: " + airTimeCalculation + "</color>");
        previousLandingSpotLocation = new Vector3(projectile.position.x + (projectile.velocity.x * t), targetElevation, projectile.position.z + (projectile.velocity.z * t));
        return previousLandingSpotLocation;
    }

    /// <summary>
    /// Get seconds it will take for the projectile to reach a given altitude
    /// </summary>
    /// <param name="projectile">Rigidbody in motion</param>
    /// <param name="targetAltitude">desired altitude</param>
    /// <param name="crossApex">find value before or after the projectile crosses the apex of its trajectory</param>
    /// <returns></returns>
    public static bool TimeToReachAltitude(Rigidbody projectile, float targetAltitude, out float time, bool crossApex = true)
    {
        return TimeToReachAltitude(projectile.position, projectile.velocity, targetAltitude, out time, crossApex);       
    }

    /// <summary>
    /// Get seconds it will take for the projectile to reach a given altitude
    /// </summary>
    /// <param name="projectile">Rigidbody in motion</param>
    /// <param name="targetAltitude">desired altitude</param>
    /// <param name="crossApex">find value before or after the projectile crosses the apex of its trajectory</param>
    /// <returns></returns>
    public static bool TimeToReachAltitude(Vector3 projectilePos, Vector3 projectileVel, float targetAltitude, out float time, bool crossApex = true)
    {
        float t = 0;
        float initialUpwardsSpeed = projectileVel.y;
        //Debug.Log("<color=cyan>Projectilve Y Velocity: " + initialUpwardsSpeed +" </color>");

        float a = .5f * Physics.gravity.y;
        float b = initialUpwardsSpeed;
        float c = -(targetAltitude - projectilePos.y);
        float cluster = (b * b) - (4f * a * c);

        float t1, t2;
        if (MathUtility.SolveQuadratic(a, b, c, out t1, out t2))
        {
            //I think t2 will always be the answer but whatever. 
            //This will check both halves of the quadratic equation.

            t = crossApex ? Mathf.Max(t1, t2) : Mathf.Min(t1, t2);
        }
        else
        {
            //Debug.LogWarning("Invalid Trajectory calculation");
            time = 0;
            return false;
        }

        time = t;
        return true;

    }

    public static Vector3 GetSpotInFuture(Rigidbody rb, float timePass)
    {
        return GetSpotInFuture(rb.position, rb.velocity, timePass);
    }

    public static Vector3 GetSpotInFuture(Vector3 position, Vector3 velocity, float timePass)
    {
        float futureElevation = position.y + (velocity.y * timePass) + (.5f * Physics.gravity.y * timePass * timePass);
        return new Vector3(position.x + (velocity.x * timePass),
                                           futureElevation,
                                           position.z + (velocity.z * timePass));
    }

    public static Vector3 GetVelocityInFuture(Rigidbody rb, float timePass)
    {
        return GetVelocityInFuture(rb.velocity, timePass) ;
    }
    public static Vector3 GetVelocityInFuture(Vector3 velocity, float timePass)
    {
        return velocity + Physics.gravity * timePass;
    }

    /// <summary>
    /// Predict where the projectile will roll over time.
    /// </summary>
    /// <param name="projectile"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public static Vector3 GetRollingSpot(Rigidbody projectile, float time)
    {
        // Starting position of the projectile
        Vector3 initialStartPosition = projectile.position;
        Debug.Log("<color=green>PROJECTILE POSITION: " + projectile.position + "</color");
        Debug.DrawLine(projectile.position, Vector3.up, Color.green, Mathf.Infinity);

        // Heading direction of the projectile
        Vector3 headingDirection = previousLandingSpotLocation - initialStartPosition;
        Debug.Log("<color=red>FACING DIRECTION: " + headingDirection + "</color");
        Debug.DrawLine(headingDirection, Vector3.up, Color.red, Mathf.Infinity);

        // The distance (length) between the starting position and the landing spot
        float distance = headingDirection.magnitude;
        Debug.Log("<color=cyan>DISTANCE (LENGTH): " + distance + "</color");
        // Debug.DrawLine(projectile.position, Vector3.up, Color.cyan, Mathf.Infinity);

        // This is now the normalized direction.
        Vector3 direction = headingDirection / distance;
        Debug.Log("<color=yellow>NORMALIZED DIRECTION: " + direction + "</color");
        Debug.DrawLine(direction, Vector3.up, Color.yellow, Mathf.Infinity);

        // The initial velocity of the projectile
        Vector3 initialVelocity = projectile.velocity;
        Debug.Log("<color=white>INITIAL VELOCITY of the PROJECTILE: " + initialVelocity + "</color");
        Debug.DrawLine(initialVelocity, Vector3.up, Color.white, Mathf.Infinity);

        //                       landing spot + (facing direction * (speed * how far in the future you want to predict)
        Vector3 futurePosition = previousLandingSpotLocation + (direction * (initialVelocity.magnitude * time));
        Debug.Log("<color=magenta>PREDICTED POSITION: " + futurePosition + "</color");
        Debug.DrawLine(futurePosition, Vector3.up, Color.magenta, Mathf.Infinity);

        return futurePosition;
    }

    public static Vector3[] GetTrajectoryPath(Rigidbody projectile, float maxTime = defaultMaxPredictionTime, float interval = timeBetweenSteps, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        Vector3 initialPosition = projectile.transform.position;
        bool hasCollided = false;
        float timePredicted = 0.0f;
        RaycastHit hit;
        List<Vector3> predictions = new List<Vector3>();
        predictions.Add(initialPosition);

        while (!hasCollided && timePredicted < maxTime)
        {
            timePredicted += interval;
            float futureElevation = initialPosition.y + (projectile.velocity.y * timePredicted) + (.5f * Physics.gravity.y * timePredicted * timePredicted);
            Vector3 nextPosition = new Vector3(initialPosition.x + (projectile.velocity.x * timePredicted),
                                               futureElevation,
                                               initialPosition.z + (projectile.velocity.z * timePredicted));

            Vector3 sweep = nextPosition - predictions[predictions.Count - 1];
            projectile.transform.position = predictions[predictions.Count - 1];
            if (projectile.SweepTest(sweep, out hit, sweep.magnitude, queryTriggerInteraction))
            {
                timePredicted -= interval * (1f - (hit.distance / sweep.magnitude));
                Debug.Log("got hit!!!!");
                hasCollided = true;
                predictions.Add(hit.distance * sweep.normalized + projectile.transform.position);
                Debug.DrawRay(hit.distance * sweep.normalized + projectile.transform.position, Vector3.up, Color.red, 100f);

            }
            else
            {
                //Debug.Log("contnue");
                Debug.DrawRay(nextPosition, Vector3.up, Color.green, 100f);
                predictions.Add(nextPosition);
            }
        }
        timeOfPreviousLandingCalculation = timePredicted + Time.time;
        projectile.transform.position = initialPosition;
        return predictions.ToArray();
    }


    public static bool GetPointOfCollision(Rigidbody projectile, out RaycastHit outHit, float maxTime = defaultMaxPredictionTime, float interval = timeBetweenSteps)
    {
        Vector3 initialPosition = projectile.transform.position;
        bool hasCollided = false;
        float timePredicted = 0.0f;
        RaycastHit hit;
        Vector3 previousPredictedPosition = initialPosition;


        while (!hasCollided && timePredicted < maxTime)
        {
            timePredicted += interval;
            float futureElevation = initialPosition.y + (projectile.velocity.y * timePredicted) + (.5f * Physics.gravity.y * timePredicted * timePredicted);
            Vector3 nextPosition = new Vector3(initialPosition.x + (projectile.velocity.x * timePredicted),
                                               futureElevation,
                                               initialPosition.z + (projectile.velocity.z * timePredicted));

            Vector3 sweep = nextPosition - previousPredictedPosition;
            projectile.transform.position = previousPredictedPosition;
            Debug.DrawRay(projectile.transform.position, sweep, Color.red, 10f);
            if (projectile.SweepTest(sweep, out hit, sweep.magnitude, QueryTriggerInteraction.UseGlobal))
            {
                //Debug.DrawRay(hit.point,(Vector3.up+Vector3.right).normalized,Color.blue,100f);
                //Debug.DrawRay(hit.point,(Vector3.up-Vector3.right).normalized,Color.blue,100f);
                projectile.transform.position = initialPosition;
                outHit = hit;
                return true;
            }
            else
            {
                previousPredictedPosition = nextPosition;
            }
        }
        projectile.transform.position = initialPosition;
        outHit = new RaycastHit();
        return false;
    }

    public static Vector3 GetSlowestVelocityByLanding(Vector3 here, Vector3 there)
    {
        if (Physics.gravity.y == 0)
        {
            return (there - here).normalized;
        }

        Vector3 flatDelta = there - here;
        float initialVerticalVelocity = Mathf.Sqrt((flatDelta.y - flatDelta.magnitude) * 2f / Physics.gravity.y);
        flatDelta = new Vector3(flatDelta.x, 0f, flatDelta.z);

        initialVerticalVelocity = flatDelta.magnitude / initialVerticalVelocity;
        flatDelta = flatDelta.normalized * initialVerticalVelocity;

        return new Vector3(flatDelta.x, initialVerticalVelocity, flatDelta.z);
    }

    public static Vector3 GetLaunchVelocityByLanding(Vector3 here, Vector3 there, float speed, float gravityFactor = 1)
    {
        float g = Physics.gravity.y * -1f * gravityFactor;
        if (g == 0)
        {
            return (there - here).normalized * speed;
        }
        float horizontalDistance = Mathf.Sqrt(Mathf.Pow(here.x - there.x, 2f) + Mathf.Pow(here.z - there.z, 2f));

        float a = g * horizontalDistance;//cannot be zero
        float b = (2f * (there.y - here.y) * speed * speed);
        b += g * horizontalDistance * horizontalDistance;
        b *= g;
        b = Mathf.Pow(speed, 4f) - b;//cannot be less than zero

        if (a == 0 || b < 0)
        {
            return GetSlowestVelocityByLanding(here, there);
        }


        float radians = ((speed * speed) - Mathf.Sqrt(b)) / a;
        //radians = Mathf.Max(radians, ((speed * speed) + Mathf.Sqrt(b)) / a);

        radians = Mathf.Atan(radians);

        Vector3 velocity = there - here;
        velocity = new Vector3(velocity.x, 0f, velocity.z);
        velocity = velocity.normalized * (Mathf.Cos(radians) * speed);
        velocity += Vector3.up * (Mathf.Sin(radians) * speed);

        return velocity;
    }

    public static bool GetPointOfPassingTarget(Rigidbody projectile, Vector3 projectileOrigin, Vector3 target, out Vector3 point, out float timeUntil)
    {
        if (projectile.isKinematic || projectile.velocity.sqrMagnitude == 0)
        {
            //Pitchable.flickDirectionFactor += .2f;
            point = projectile.transform.position;
            timeUntil = 0f;
            return false;
        }

        Vector3 originToTarget = target - projectileOrigin;
        Vector3 flatOriginToTarget = new Vector3(originToTarget.x, 0, originToTarget.z);
        Vector3 flatVelocity = new Vector3(projectile.velocity.x, 0, projectile.velocity.z);

        if (flatVelocity.sqrMagnitude == 0 || flatOriginToTarget.sqrMagnitude == 0)
        {
            //Pitchable.flickDirectionFactor += .3f;

            point = projectile.transform.position;
            timeUntil = 0f;
            return false;
        }
        float flatAngleOffTarget = Vector3.Angle(flatOriginToTarget, flatVelocity);
        //Pitchable.flickDirectionFactor = flatAngleOffTarget;
        if (flatAngleOffTarget > 90)
        {
            //Pitchable.flickDirectionFactor += .5f;

            point = projectile.transform.position;
            timeUntil = 0f;
            return false;
        }

        Vector3 flatIntersectionPoint = projectileOrigin + flatVelocity.normalized * Mathf.Abs(originToTarget.magnitude / Mathf.Cos(flatAngleOffTarget * Mathf.Deg2Rad));
        flatIntersectionPoint = new Vector3(flatIntersectionPoint.x, projectile.transform.position.y, flatIntersectionPoint.z);

        /*
        timeUntil = (flatIntersectionPoint - projectile.transform.position).magnitude / (flatVelocity.magnitude);
        if (Vector3.Angle(flatVelocity, flatIntersectionPoint - projectile.transform.position) > 90f)
        {
            timeUntil *= -1f;
        }
        */
        if (flatIntersectionPoint.z != projectile.transform.position.z)
        {
            timeUntil = ((flatIntersectionPoint.z - projectile.transform.position.z) / flatVelocity.z);
        }
        else if (flatIntersectionPoint.x != projectile.transform.position.x)
        {
            timeUntil = ((flatIntersectionPoint.x - projectile.transform.position.x) / flatVelocity.x);
        }
        else
        {
            timeUntil = Mathf.Sqrt(2f * (flatIntersectionPoint.y - projectile.transform.position.y) / Physics.gravity.y);
        }

        float elevationChangeAtPassing = ((projectile.velocity.y * timeUntil) - (.5f * Physics.gravity.magnitude * Mathf.Pow(timeUntil, 2)));

        flatIntersectionPoint += Vector3.up * elevationChangeAtPassing;

        point = flatIntersectionPoint;
        return true;
    }

    public static Vector3 InitialVelocityNeededToHitPoint(Rigidbody body, Vector3 landingPoint, float airTime)
    {
        Vector3 bodyPosFlat = body.position; bodyPosFlat.y = 0;
        Vector3 landingPointFlat = landingPoint; landingPointFlat.y = 0;
        float vel_x = Vector3.Distance(bodyPosFlat, landingPointFlat) / airTime;
//        Debug.Log("gravity! "+ Physics.gravity.y);
        float vel_y = ((landingPoint.y - body.position.y) - (.5f * Physics.gravity.y * airTime * airTime)) / airTime;

        Vector3 direction = (landingPointFlat - bodyPosFlat).normalized;

        Vector3 result = direction * vel_x;
        result.y = vel_y;

        return result;

    }
}

public struct TrajectoryInfo
{
    public TrajectoryInfo(Rigidbody body)
    {
        rigidbody = body;
        simulatedVelocity = rigidbody.velocity;
        simulatedPosition = rigidbody.position;
    }

    private Rigidbody rigidbody;
    private Vector3 simulatedVelocity; //velocity in world space
    private Vector3 simulatedPosition; //position in world space

    public float drag { get { return rigidbody.drag; } }
    public float weight { get { return Physics.gravity.y * rigidbody.mass; } }
    //Vt = sqrt( (2 * m* g) / (Cd* r * A) )

    public Vector3 groundHitPosition
    {
        get
        {
            return Position_at_T(T_Ground);
        }
    }

	public Vector3 chestHeightPosition
	{
		get
		{
			return Position_at_T(T_Chest);
		}
	}

    public Vector3 currentPosition
    {
        get
        {
            return (rigidbody.position);
        }
    }

    public Vector3 currentVelocity
    {
        get
        {
            //how to get into local space?
            return (rigidbody.velocity);
        }
    }



    public void SimulateVelocity(Vector3 simVelocity, Space space  = Space.World)
    {
        if (space == Space.Self) simVelocity = (simVelocity);
        simulatedVelocity = simVelocity;
    }

    public void SimulatePosition(Vector3 simPosition, Space space = Space.World)
    {
        if (space == Space.Self) simPosition = (simPosition);
        simulatedPosition = simPosition;
    }

    public Vector3 Position_at_T(float t, Space space = Space.Self)
    {
        Vector3 pos = Trajectory.GetSpotInFuture(simulatedPosition, simulatedVelocity, t);
        if (space == Space.Self)
       pos = (pos);

        return pos;
    }

    public Vector3 Velocity_at_T(float t, Space space = Space.Self)
    {
        Vector3 vel = (Trajectory.GetVelocityInFuture(simulatedVelocity, t));
        if (space == Space.Self)
            vel = (vel);

        return vel;
    }

    public Vector3 PositionAtApex
    {
        get
        {
            if (simulatedVelocity.y <= 0) return simulatedPosition;

            float t_after;
            if((Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, simulatedPosition.y, out t_after, true))){
                float t_apex = (t_after) / 2f;
                return (Trajectory.GetSpotInFuture(simulatedPosition, simulatedVelocity, t_apex));
            }
            else return currentPosition;
        }
    }

    public float T_Ground
    {
        get
        {
            float t;
            float alt  = (Vector3.zero).y;
            if ((Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, alt, out t, true)))
            {
                return t;
            }
            return 0;
        }
    }

	public float T_Chest
	{
		get
		{
			float t;
			float alt  = ((Vector3.zero).y + (Camera.main.transform.position.y*2f))/2f;
			if ((Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, alt, out t, true)))
			{
				return t;
			}
			return 0;
		}
	}

    public float T_Apex
    {
        get
        {
            float t_after;
            float alt = (simulatedPosition).y;

            if ((Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, alt, out t_after, true)))

            {
                return (t_after) / 2f;
            }
            return 0;
        }
    }

    public float T_ExitAscendingCatchZone(float maxCatchAltitude)
    {

            float t_exitCatchZone;
        float alt = (Vector3.up * maxCatchAltitude).y;

        if (Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, alt, out t_exitCatchZone, false))
            {
                return Mathf.Max(t_exitCatchZone, 0);
            }
            else
                return T_Apex;
        
    }

    public float T_EnterDescendingCatchZone(float maxCatchAltitude)
    {

            float t_enterCatchZone;
        float altitude_world = (Vector3.up * maxCatchAltitude).y;

        if (Trajectory.TimeToReachAltitude(simulatedPosition, simulatedVelocity, altitude_world, out t_enterCatchZone, true))
        {
            return Mathf.Max(0, t_enterCatchZone);
        }
        else
        {
            return T_Apex;
        }
        
    }

    public float T_ToReachHorizontalDistance(float distance)
    {
        Vector3 horizontalVelocity = simulatedVelocity;
        horizontalVelocity.y = 0;
        return distance / horizontalVelocity.magnitude;
    }

    private static List<Vector3> _pointsList;

    public Vector3[] GetPoints(float sampleInterval, float totalSampleDuration)
    {
        sampleInterval = Mathf.Max(sampleInterval, .1f);
        totalSampleDuration = Mathf.Clamp(totalSampleDuration, 0f, 10f);
        if(_pointsList==null) _pointsList = new List<Vector3>();
        else _pointsList.Clear();
        for(float t = 0; t<totalSampleDuration; t+= sampleInterval)
        {
            _pointsList.Add(Trajectory.GetSpotInFuture(simulatedPosition, simulatedVelocity, t));
        }
        _pointsList.Add(Trajectory.GetSpotInFuture(simulatedPosition, simulatedVelocity, totalSampleDuration));

        return _pointsList.ToArray();
    }

    private static Vector3[] _points;

    public void DebugDrawTrajectory(float startTime, float endTime, Color color, float duration = 0)
    {
        _points = GetPoints(startTime, endTime);
        for (int i = 0; i < _points.Length - 1; i++)
        {
            if(duration !=0)
                Debug.DrawLine(_points[i], _points[i + 1], color, duration);
            else
                Debug.DrawLine(_points[i], _points[i + 1], color);
        }
    }

#if UNITY_EDITOR
    public void DebugDrawTrajectory_Handles(float startTime, float endTime)
    {
        _points = GetPoints(startTime, endTime);
        for (int i = 0; i < _points.Length - 1; i++)
        {
            Handles.DrawLine(_points[i], _points[i + 1]);
        }
    }
    public void DebugDrawTrajectory_Handles()
    {
        DebugDrawTrajectory_Handles(0f, T_Ground);
    }



#endif
}
