﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public static class MathUtility
    {

        public struct StraightLine
        {
            public Vector3 point1;
            public Vector3 point2;
            public StraightLine(Vector3 p1, Vector3 p2)
            {
                point1 = p1;
                point2 = p2;
            }
        }
    public struct Plane
    {
        public Vector3 pointA, pointB, pointC;
            public Plane(Vector3 pA, Vector3 pB, Vector3 pC)
        {
            pointA = pA;
            pointB = pB;
            pointC = pB;
        }
    }

        public static bool SolveQuadratic(float a, float b, float c, out float sol_1, out float sol_2)
        {
            float square = (b * b) - (4f * a * c);
//            Debug.Log(square + " a: " + a + " b: " + b + " c: " +c);
            if (square >= 0 && a != 0)
            {

                //I think t2 will always be the answer but whatever. 
                //This will check both halves of the quadratic equation.
                sol_1 = (-b + Mathf.Sqrt(square)) / (2f * a);
                sol_2 = (-b - Mathf.Sqrt(square)) / (2f * a);
                return true;


            }

            sol_1 = 0;
            sol_2 = 0;
            return false;

        }

        public static Vector3 FindClosestPointOnLineToPoint(Ray line, Vector3 point)
    {

        return Vector3.Dot(point - line.origin, line.direction.normalized) * line.direction.normalized + line.origin;

    }

    public static bool FindPlaneLineIntersection(Ray line, Vector3 plane_normal, Vector3 point_on_plane, out float intersection)
        {
            intersection = 0;

            //d = (p0 - l0) dot n / l dot n


            float det = Vector3.Dot(line.direction, plane_normal);
            if (det == 0)
            {
                return false;
            }

            float d = Vector3.Dot((point_on_plane - line.origin), plane_normal) / det;

            intersection = (d);

            return true;
        }

        public static float ScalarProjection(Vector3 from, Vector3 onto)
        {
            float scalarProjection = Vector3.Dot(from, onto.normalized);
            return scalarProjection;
        }

    public static Vector3 ReflectedVector(Vector3 original, Vector3 reflectionNormal)
    {
        reflectionNormal = reflectionNormal.normalized;
        return original - 2 * (Vector3.Dot(original, reflectionNormal)) * reflectionNormal;
    }
    }

