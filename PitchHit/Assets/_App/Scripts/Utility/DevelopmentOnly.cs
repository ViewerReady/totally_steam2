﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevelopmentOnly : MonoBehaviour
{
#if DEVELOPMENT_BUILD
    void Awake(){
        gameObject.SetActive(false);
    }
#endif
}
