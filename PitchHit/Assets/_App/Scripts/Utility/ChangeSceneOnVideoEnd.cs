﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using Oculus.Platform;

[RequireComponent(typeof(VideoPlayer))]
public class ChangeSceneOnVideoEnd : MonoBehaviour {

    private VideoPlayer video;
    public RawImage image;
    public SpriteRenderer sprite;
    public ParticleSystem exitParticles;
    public float delay = 2;
    [SerializeField] GameObject entitlementCheckErrorText;
    
    void Awake()
    {
        video = GetComponent<VideoPlayer>();
        video.loopPointReached += OnVideoEnd;
    }
	
    void OnVideoEnd(VideoPlayer source)
    {
        StartCoroutine(WaitRoutine());
    }

    IEnumerator WaitRoutine()
    {
        if (exitParticles)
        {
            exitParticles.Play();
        }
        if (image)
        {
            image.enabled = false;
        }
        if (sprite) {
            sprite.enabled = true;
            float t = 0;
            while (t < 1)
            {
                sprite.color = Color.Lerp(Color.white, Color.clear, t);
                t += Time.deltaTime*5f;
                yield return null;
            }
            sprite.enabled = false;
        }
        
        if(exitParticles)
        {
            while (exitParticles.isPlaying)
            {
                yield return null;
            }
        }
        
        yield return new WaitForSecondsRealtime(delay);
#if RIFT
        try
        {
            Core.AsyncInitialize();
            Entitlements.IsUserEntitledToApplication().OnComplete(EntitlementCallback);
        }
        catch (UnityException e)
        {
            Debug.LogError("Platform failed to initialize due to exception.");
            Debug.LogException(e);
            // Immediately quit the application.
            StartCoroutine(ShowEntitlementErrorRoutine());
        }
#elif STEAM_VR
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
#endif
    }

    void EntitlementCallback(Message msg)
    {
        if (msg.IsError)
        {
            Debug.LogError("You are NOT entitled to use this app.");
            StartCoroutine(ShowEntitlementErrorRoutine());
        }
        else
        {
            Debug.Log("You are entitled to use this app.");
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }

    IEnumerator ShowEntitlementErrorRoutine()
    {
        entitlementCheckErrorText.SetActive(true);
        yield return new WaitForSeconds(4f);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        //UnityEngine.Application.Quit();
    }
}