﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BakeMesh : MonoBehaviour
{
#if UNITY_EDITOR
    public void Bake()
    {
        Mesh mesh = new Mesh();
        SkinnedMeshRenderer renderer = GetComponent<SkinnedMeshRenderer>();
        renderer.BakeMesh(mesh);

        string dest = "Assets/_App/Models/Baked/" + gameObject.name + "_StaticFromSkinned" + ".asset";
        UnityEditor.AssetDatabase.CreateAsset(mesh, dest);
        Debug.Log("saved mesh to " + dest);
    }
#endif
}
