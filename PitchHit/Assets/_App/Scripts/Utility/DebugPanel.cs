﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class DebugPanel : MonoBehaviour {

    private Canvas canvas;

    public bool _showDebug;

    public Text defenseStateText;
    public Text ballControlText;
    public Text ballChasersText;
    public Text ballPosessionText;
    public Text baseCoverageText;
    public Text baseCoveragePoolText;
    public Text backupCoverageText;
    public Text backupCoveragePoolText;
    public Text throwInProgressText;

    public Text offenseStateText;
    public Text runnersText;
    public Text lastBasesText;
    public Text nextBasesText;
    public Text allOnBaseText;

    public Text allCamerasText;
    public Text cameraStatesText;

	// Use this for initialization
	void Start () {
        canvas = GetComponent<Canvas>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_showDebug)
        {
            canvas.enabled = true;
            RefreshInfo();
        }
        else
        {
            canvas.enabled = false;
        }
	}

    void RefreshInfo()
    {
        if (!FullGameDirector.Instance)
        {
            return;
        }

        DefenseCoordinator defense = FullGameDirector.Instance.defense;
        if (defense)
        {
            defenseStateText.text = defense._state.ToString();
            ballControlText.text = defense.debug_ballControl;
            ballChasersText.text = defense.debug_ballChasers;
            ballPosessionText.text = defense.BallIsInPosession().ToString();
            baseCoverageText.text = defense.debug_baseCoverage;
            baseCoveragePoolText.text = defense.debug_baseCoveragePool;
            backupCoverageText.text = defense.debug_backupPlayers;
            backupCoveragePoolText.text = defense.debug_backupPool;

            throwInProgressText.text = defense.aIpassInProgress.ToString();
        }

        OffenseCoordinator offense = FullGameDirector.Instance.offense;
        if (offense)
        {
            offenseStateText.text = offense._state.ToString();

            string runners = "";
            string lastBases = "";
            string nextBases = "";

            foreach(OffensePlayer runner in FieldMonitor.offensivePlayers)
            {
                runners += runner.name + "\n";
                lastBases += (runner.baseLastTouched + 1).ToString() + "\n";
                nextBases += (runner.baseTarget_Immediate+1).ToString() + "\n";
            }
            runnersText.text = runners;
            lastBasesText.text = lastBases;
            nextBasesText.text = nextBases;

            allOnBaseText.text = FieldMonitor.Instance.AllRunnersOnBase().ToString();
        }

        BroadcastCoordinator spectator = FullGameDirector.Instance.broadcast;
        if (spectator)
        {
            string cameras = "";
            string states = "";
            foreach(CinemachineVirtualCamera cam in FindObjectsOfType<CinemachineVirtualCamera>())
            {
                bool isLive = CinemachineCore.Instance.IsLive(cam);
                bool isEnabled = cam.isActiveAndEnabled;

                cameras += cam.name + "\n";
                states += (isLive? "-LIVE-" : (isEnabled? "STANDBY" : "DISABLED"))+ "\n";
            }
            allCamerasText.text = cameras;
            cameraStatesText.text = states;
        }

    }

    public void TogglePOV()
    {
        BroadcastCoordinator spectator = FullGameDirector.Instance.broadcast;
        if (spectator)
        {
            spectator.TogglePovOnlyMode();
        }
    }
}
