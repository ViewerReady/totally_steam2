﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System;

public class MixedRealityBat : MonoBehaviour {
    
    private bool calibrated;
    public Transform baseOfBat;
    private Transform grip;
    public SteamVR_TrackedObject trackedObject;
    public Transform HTCRaquetGrip;
    public Transform baseTrackerGrip;
    public Transform tipTrackerGrip;
    public Transform gloveGripRacket;
    public Transform gloveGripBase;
    public Transform gloveGripTip;
    public DeviceChooser deviceChooser;
    public GameObject noBatCollider;
    

    private Valve.VR.InteractionSystem.Player player;
    private GameController.FullBatType currentBatType;

    void Awake()
    {
        HandManager.genericTracker = this;
    }


    // Update is called once per frame
    private int lingerCollider = 10;
	void Update () {
        bool noHandsAvailable = (HandManager.currentLeftCustomHand == null && HandManager.currentRightCustomHand == null);
        //Debug.Log(GameController.batType.ToString());
        if (deviceChooser)
        {

            if (GameController.batType == GameController.FullBatType.None || GameController.fullBatterMode == false)
            {
                deviceChooser.ShowRenderModel(true);
                noBatCollider.SetActive(true);
                lingerCollider = 10;
            }
            else
            {
                if (lingerCollider>0)
                {
                    lingerCollider--;
                }
                else
                {
                    //noBatCollider.SetActive(false);
                }
            }
        }

        if(GameController.batType != currentBatType)
        {
            currentBatType = GameController.batType;
            if(currentBatType== GameController.FullBatType.HTCRacquet)
            {
                baseOfBat.localPosition = Vector3.zero;
                grip = HTCRaquetGrip;
            }
            else if(currentBatType == GameController.FullBatType.TrackerAtBase)
            {
                baseOfBat.localPosition = Vector3.zero;
                grip = baseTrackerGrip;
            }
            else
            {
                float delta = PlayerPrefs.GetFloat("MIXEDBATLENGTH", .54f);
                baseOfBat.localPosition = Vector3.forward * -delta;
                grip = tipTrackerGrip;
            }

            if (currentBatType == GameController.FullBatType.None || noHandsAvailable)
            {
                deviceChooser.ShowRenderModel(true);
            }
            else
            {
                deviceChooser.ShowRenderModel(false);
            }

        }

        

        if (player)
        {
            if (currentBatType == GameController.FullBatType.TrackerAtTip)
            {
                if (Input.GetKey("c") && Input.GetKey("x"))
                {
                    CalibrateTipLength();
                }
            }
        }
        else
        {
            player = Valve.VR.InteractionSystem.Player.instance;
        }
    }
    public void CalibrateTipLength()
    {
        Debug.Log("Calibrate tip length.");
        float delta = transform.position.y - player.transform.position.y;
        baseOfBat.localPosition = Vector3.forward * -delta;
        PlayerPrefs.SetFloat("MIXEDBATLENGTH", delta);

    }

    public bool IsValid()
    {
        return trackedObject && trackedObject.isValid;
    }

    public Vector3 GetCurrentBatGripPosition()
    {
        if(grip==null)
        {
            return transform.position;
        }

        return grip.position;
    }

    public Quaternion GetCurrentBatGripRotation()
    {
        if (grip == null)
        {
            return transform.rotation;
        }

        return grip.rotation;
    }

    public void OnBatMatching()
    {
        deviceChooser.ShowRenderModel(false);
    }
}
