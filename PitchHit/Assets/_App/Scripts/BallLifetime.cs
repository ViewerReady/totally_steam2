﻿using UnityEngine;
using System.Collections.Generic;

public class BallLifetime : MonoBehaviour {
    public bool killBall = false;
    public float timeToLive = 10f;
    private float countDown;
    //private Rigidbody rb;

    // how slow is too slow to still be considered (this value will be squared at start for efficiency)
    //private float dieSpeed = .5f;

    //how low is too low to still be considered.
    //private float dieElevation = -1f;

	// Use this for initialization
	void Start () {
        countDown = timeToLive;

        //rb = GetComponent<Rigidbody>();
        //dieSpeed *= dieSpeed;
	}

    public void StartKillCount()
    {
        // Update alive time to when kill count started
        countDown = timeToLive;
        killBall = true;
        //activeBalls.Add(this);
        //transform.SetParent(null);
        gameObject.layer = LayerMask.NameToLayer("ActiveBalls");
    }

    public void StopKillCount()
    {
        killBall = false;
        countDown = timeToLive;
    }
	
	// Update is called once per frame
	void Update () {
        /*
        if(transform.localPosition.y<0)
        {
            if (transform.parent)
            {
                transform.localPosition -= transform.parent.up * (transform.localPosition.y - .5f);
            }
            else
            {
                transform.localPosition -= Vector3.up * (transform.localPosition.y-.5f);
            }
        }
        */
        if(killBall)
        {
            //if(rb.velocity.sqrMagnitude < dieSpeed || transform.position.y<dieElevation)
            //{
            //  Die();
            //}
            countDown -= Time.deltaTime;
            //Debug.Log("time left "+ countDown);
	        if (countDown <0)
            {
                //Debug.Log("age out ");
                //activeBalls.Remove(this);
                Destroy(gameObject);
            }
        }
	}

    //void OnDestroy()
    //{
        //Debug.Log(name + "was destroyed");
    //}

   
}
