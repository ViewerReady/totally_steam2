﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSound : MonoBehaviour {
	public AudioSource source;

	// Use this for initialization
	void Start () {
		if (source && source.clip) {
			Destroy (gameObject, source.clip.length + 1f);	
		}
		else
		{
			Destroy (gameObject);
		}
	}

	public void AssignSound(AudioClip clip)
	{
		source.clip = clip;
		source.Play ();
	}

	//private bool immune;
	//public void BecomeImmuneToTime()
	//{
	//	immune = true; 
	//}
    /*
	private bool interrupt;
	public void Interrupt()
	{
		interrupt = true;
	}

	void Update()
	{
		if (!immune) {
			source.pitch = Time.timeScale;
		}

		if (interrupt) {
			source.volume = Mathf.Lerp (source.volume, 0, Time.deltaTime * 10f);
		}
	}
    */
}
