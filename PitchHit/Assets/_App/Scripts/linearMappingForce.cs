﻿using UnityEngine;
using System.Collections;

public class linearMappingForce : MonoBehaviour {

	public Transform heightTransform;

	public Rigidbody adjustableRB;

	private float yDiff;
	private Vector3 adjustablePos;
    private bool isCalibrated;

	// Use this for initialization
	public void Calibrate () {
		yDiff = heightTransform.position.y - adjustableRB.position.y;
		adjustablePos = adjustableRB.position;
        isCalibrated = true;
	}

	void FixedUpdate(){
        if (isCalibrated)
        {
            adjustablePos.y = heightTransform.position.y - yDiff;
            adjustableRB.MovePosition(adjustablePos);
        }
	}
}
