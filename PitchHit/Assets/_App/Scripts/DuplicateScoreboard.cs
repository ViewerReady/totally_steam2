﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuplicateScoreboard : MonoBehaviour {
    public ScoreboardController sourceScoreboard;
    public Text totalScoreText;
    public Text[] partialScoreTexts;
    public GameObject homerunPanel;
    public Text statusText;

    private string previousSourceText = "nothing";
	// Update is called once per frame
	void Update () {
	    if(sourceScoreboard && previousSourceText.Equals(sourceScoreboard.scoreboardText.text)==false)
        {
            previousSourceText = sourceScoreboard.scoreboardText.text;
            if (totalScoreText)
            {
                totalScoreText.text = sourceScoreboard.scoreboardText.text;
            }
            else
            {
                if(partialScoreTexts.Length>0)
                {
                    foreach(Text t in partialScoreTexts)
                    {
                        t.text = "0";
                    }

                    for(int t=0; t< previousSourceText.Length;t++)
                    {
                        if(t<partialScoreTexts.Length)
                        {
                            partialScoreTexts[partialScoreTexts.Length - 1 - t].text = previousSourceText[previousSourceText.Length - 1 - t]+"";
                        }
                        else
                        {
                            partialScoreTexts[0].text = previousSourceText[previousSourceText.Length - 1 - t] + partialScoreTexts[0].text;
                        }
                        
                    }
                }
            }
            homerunPanel.SetActive(sourceScoreboard.homeRunBoard.activeSelf);
            statusText.text = sourceScoreboard.statusText.text;
        }	
	}
}
