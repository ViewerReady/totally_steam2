﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(HingeJoint))]
[RequireComponent(typeof(Interactable))]
public class RealLever : MonoBehaviour
{
    SteamVR_Action_Boolean grabObject;

    HingeJoint leverJoint;

    LeverState currentState;
    [SerializeField] LeverState defaultPosition;
    [SerializeField] float leverAngle;
    [SerializeField] bool shouldTriggerOnStart = false;
    [SerializeField] MeshRenderer leverLightMesh;
    [SerializeField] Material leverLightGreen;
    [SerializeField] Material leverLightRed;

    private bool beingHeldSteamVR;
    private bool beingHeldOculus;
    private Hand controllingHand;
    private OVRGrabber controllingGrabber;
    private Vector3 originalPosition;
    private Vector3 originalRotation;
    [SerializeField] float rotationSpeed = 200f;    //If you raise your arm 1 Unity unit, this is the amount of degrees we rotate.


    [SerializeField] UnityEvent onLeverUp;
    [SerializeField] UnityEvent onLeverDown;

    private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & (~Hand.AttachmentFlags.SnapOnAttach) & (~Hand.AttachmentFlags.DetachOthers);

    void Awake()
    {
        grabObject = SteamVR_Actions.baseballSet_GrabObject;

        currentState = defaultPosition;

        leverJoint = gameObject.GetComponent<HingeJoint>();
        JointSpring spring = leverJoint.spring;
        spring.targetPosition = (defaultPosition == LeverState.UP ? -leverAngle : leverAngle);
        leverJoint.spring = spring;
    }

    void Start()
    {
        
        if (leverLightMesh.material != null)
        {
            leverLightMesh.material = (currentState == LeverState.UP ? leverLightGreen : leverLightRed);
        }
        if (shouldTriggerOnStart && currentState == LeverState.UP)
        {
            onLeverUp.Invoke();
        }
        else if (shouldTriggerOnStart && currentState == LeverState.DOWN)
        {
            onLeverDown.Invoke();
        }
    }

    void Update()
    {
        if (currentState == LeverState.UP)
        {
            float currZRotation = transform.localRotation.eulerAngles.z;
            if (currZRotation >= (leverAngle / 2) && currZRotation <= 180)
            {
                DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("lever_pull", transform.position);
                currentState = LeverState.DOWN;
                JointSpring spring = leverJoint.spring;
                spring.targetPosition = leverAngle;
                leverJoint.spring = spring;
                leverLightMesh.material = leverLightRed;
                onLeverDown.Invoke();
            }
            
        }
        else if (currentState == LeverState.DOWN)
        {
            float currZRotation = transform.rotation.eulerAngles.z;
            if (currZRotation <= (360f - (leverAngle / 2)) && currZRotation > 180)
            {
                DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("lever_pull", transform.position);
                currentState = LeverState.UP;
                JointSpring spring = leverJoint.spring;
                spring.targetPosition = -leverAngle;
                leverJoint.spring = spring;
                leverLightMesh.material = leverLightGreen;
                onLeverUp.Invoke();
            }
        }

        if (beingHeldSteamVR)
        {
            float yDifference = originalPosition.y - controllingHand.transform.position.y;
            transform.localEulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + yDifference * rotationSpeed);
            if (!grabObject.GetLastState(controllingHand.handType))
            {
                beingHeldSteamVR = false;
                leverJoint.useSpring = true;
                //controllingHand.ShowHand();
                CustomHand customHand = (controllingHand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.ShowHand();
            }
        }
        else if (beingHeldOculus)
        {
            float yDifference = originalPosition.y - controllingGrabber.transform.position.y;
            transform.localEulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + yDifference * rotationSpeed);
        }

        if (currentState == LeverState.UP)
        {
            float currZRotation = transform.localRotation.eulerAngles.z;
            if (currZRotation > (leverAngle + (leverAngle / 2f)) && currZRotation < (360f - (leverAngle + (leverAngle / 2f))))
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, (360f - (leverAngle + (leverAngle / 2f))));
            }
        }
        else if (currentState == LeverState.DOWN)
        {
            float currZRotation = transform.localRotation.eulerAngles.z;
            if (currZRotation > (leverAngle + (leverAngle / 2f)) && currZRotation < (360f - (leverAngle + (leverAngle / 2f))))
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, (leverAngle + (leverAngle / 2f)));
            }
        }
    }

    //-------------------------------------------------
    // Called every Update() while a Hand is hovering over this object
    //-------------------------------------------------
    private void HandHoverUpdate(Hand hand)
    {
        if (grabObject.GetLastStateDown(hand.handType))
        {
            if (!beingHeldSteamVR)
            {
                beingHeldSteamVR = true;
                controllingHand = hand;
                //controllingHand.HideHand();
                CustomHand customHand = (hand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.HideHand();
                leverJoint.useSpring = false;
                originalPosition = hand.transform.position;
                originalRotation = transform.localEulerAngles;
            }
        }
    }

    public void CustomGrabbableGrabBegin(OVRGrabber grabber)
    {
        if (!beingHeldOculus)
        {
            beingHeldOculus = true;
            controllingGrabber = grabber;
            //controllingGrabber.HideHand();
            controllingGrabber.HideGrabber();
            leverJoint.useSpring = false;
            originalPosition = grabber.transform.position;
            originalRotation = transform.localEulerAngles;
        }
    }

    public void CustomGrabbableGrabEnd(OVRGrabber grabber)
    {
        beingHeldOculus = false;
        leverJoint.useSpring = true;
        controllingGrabber.ShowGrabber();
    }
}

enum LeverState
{
    UP,
    DOWN
}