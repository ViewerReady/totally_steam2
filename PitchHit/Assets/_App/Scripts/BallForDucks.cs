﻿using UnityEngine;
using System.Collections;
using ArchimedsLab;

public class BallForDucks : MonoBehaviour {
    public GameObject[] waterSplashPrefabs;
    public Material pitchMaterial;
    public hittable hittableComponent;
    private Rigidbody rigid;
    private Collider col;
    private bool pitched;
    private float thresholdToFinishPitch = 2.7f;
    private bool isUnderwater;

    private static Material sea;
    private static float maxWaveHeight;

    // Use this for initialization
	void Awake () {
        rigid = GetComponent<Rigidbody>();
        if (!sea)
        {
            sea = GameObject.Find("Sea").GetComponent<Renderer>().material;
            maxWaveHeight = sea.GetFloat("_Intensity") *.5f;
        }
    }

    void FixedUpdate()
    {
        float currentElevation = transform.position.y;
        if (!isUnderwater)
        {
            if (currentElevation < maxWaveHeight)
            {
                float waterLevel = CustomFloatingEntity.GetWaterHeight(transform.position);
                if (currentElevation < waterLevel)
                {
                    SplashIntoWater(waterLevel);
                    isUnderwater = true;
                }
            }
        }
        else
        {
            if (currentElevation > -maxWaveHeight)
            {
                if (currentElevation > CustomFloatingEntity.GetWaterHeight(transform.position) + .1f)
                {
                    isUnderwater = false;
                }
            }
        }
    } 

    
    public void SplashIntoWater(float waterElevation)
    {
        Vector3 waterSurfaceHere = new Vector3(transform.position.x, waterElevation, transform.position.z);
        int waterIndex = Mathf.Min(waterSplashPrefabs.Length-1,(int)(Mathf.Abs(rigid.velocity.magnitude) / 5f));
        GameObject temp = Instantiate(waterSplashPrefabs[waterIndex], waterSurfaceHere, Quaternion.LookRotation(Vector3.up)) as GameObject;

    }

    //this is simply used for keeping the ball's collider as a trigger 
    //so it will go through ducks until before it gets to the batter.
    IEnumerator Pitching()
    {
       while(pitched)
        {
            yield return null;

            if(transform.position.z< thresholdToFinishPitch)
            {
                FinishPitch();
            }

        }
    }
    
    public void GetPitched()
    {
        GetComponent<Renderer>().material = pitchMaterial;

        if (col == null)
        {
            col = GetComponent<Collider>();
        }
        col.isTrigger = true;
        pitched = true;

        StartCoroutine(Pitching());
    }
    
    public void FinishPitch()
    {
        col.isTrigger = false;
        pitched = false;
    }

}
