﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShatterWords : MonoBehaviour
{
    public Transform firstLetter;
    public Transform lastLetter;
    public Rigidbody[] allRigidBodies;
    public MeshFilter[] allMeshFilters;
    public Transform[] allColliderTransforms;
    public LayerMask avoidLayerMask;
    private bool shattered;
    private BoxCollider areaTrigger;
    public AudioSource sound;
    public AudioClip[] smashSounds;
    [HideInInspector]
    public HomeRunBall judger;
    public bool justDisappear;
   
    // Use this for initialization
    void Start()
    {
        //position a bit side to side to create some randomness
        float random = Random.value - .5f;
        float halfWordWidth = Mathf.Abs(firstLetter.localPosition.x);
        Vector3 randomOffset = transform.right * ((random * halfWordWidth) - (Mathf.Clamp(-transform.position.x,-.7f * halfWordWidth, .7f * halfWordWidth)));
        random = Random.value;

        randomOffset -= transform.up * random * firstLetter.localPosition.y;

        transform.position += randomOffset;
        areaTrigger = GetComponent<BoxCollider>();

        Vector3 checkCenter = transform.TransformPoint(areaTrigger.center);// + randomOffset;
        if (Physics.OverlapBox(checkCenter, areaTrigger.size * .5f, transform.rotation, avoidLayerMask).Length != 0)
        {
            //Debug.Log("--------------------------------------------------shatter words started in collision.");
            if (judger)
            {
                float sweepDistance = 50f;
                Vector3 direction = judger.GetDirectionOnImpact().normalized * sweepDistance;
                //Debug.Log("get sweep0000 "+direction);
                checkCenter -= direction;
                RaycastHit hit;
                Debug.DrawRay(checkCenter, direction, Color.cyan, 100f);

                ExtDebug.DrawBoxCastBox(checkCenter, areaTrigger.size * .5f, transform.rotation, direction, sweepDistance, Color.blue);
                if (Physics.BoxCast(checkCenter, areaTrigger.size, direction, out hit, transform.rotation, sweepDistance, avoidLayerMask))
                {
                    Debug.DrawRay(hit.point, Vector3.up * 20f, Color.green, 20f);
                    // Debug.Log("sweep true "+hit.distance+" "+hit.collider.name);
                    //transform.position = checkCenter + (direction.normalized * hit.distance);
                    transform.position -= direction.normalized * (sweepDistance - hit.distance);
                    Debug.Log("box cast for homerun letters hit "+hit.collider.name+" after "+hit.distance);
                }
                else
                {
                     Debug.Log("hit nothing.");
                    //transform.position -= direction;
                }

            }
            else
            {
                //transform.position += 10f * transform.forward;
            }
        }

        //OnShatter();
        StartCoroutine(FallAnywaysRoutine());
    }

    IEnumerator FallAnywaysRoutine()
    {
        float timeLeft = 10f;
        while(timeLeft>0 && !shattered)
        {
            yield return null;
            timeLeft -= Time.deltaTime;
        }
        if(timeLeft<=0)
        {
            OnShatter(false);
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (!shattered)
        {
            hittable ball = c.GetComponentInParent<hittable>();
            if (ball)
            {
                OnShatter();
            }  
        }
    }

    public void OnShatter(bool withSound = true)
    {
        shattered = true;
        foreach (Rigidbody r in allRigidBodies)
        {
            r.useGravity = true;
            //r.sleepThreshold = 50f;
            //Destroy(r.gameObject, Random.value * 14f);
        }
        //Destroy(gameObject, 15f);
        areaTrigger.enabled = false;

        //GetCombined();

        StartCoroutine(CheckForSleepRoutine());

        if(withSound && sound && smashSounds.Length>0)
        {
            sound.clip = smashSounds[Random.Range(0, smashSounds.Length)];
            if(sound.clip)
            {
                sound.Play();
            }
        }
    }

    public HomeRunDerbyController derbyController;
    private void GetCombined()
    {
        if(!derbyController)
        {
            derbyController = FindObjectOfType<HomeRunDerbyController>();
        }

        Transform combinedParent = derbyController.CombineMeshesOfBrokenWords(allMeshFilters);

        foreach(Transform t in allColliderTransforms)
        {
            t.SetParent(combinedParent);
        }

        Destroy(gameObject);

    }

    IEnumerator CheckForSleepRoutine()
    {
        bool anyAwake = true;
        int bodiesRemaining = allRigidBodies.Length - 1;
        yield return new WaitForSeconds(3f);
        while (anyAwake)
        {
            anyAwake = false;
            int lastSlotFilled = -1;
            for (int r = 0; r <= bodiesRemaining; r++)
            {

                Vector3 chunkPos = allRigidBodies[r].transform.position;
                //allRigidBodies[r].IsSleeping()
                if (allRigidBodies[r].velocity.sqrMagnitude < .1f || chunkPos.y<0 || (chunkPos.sqrMagnitude>50000f && chunkPos.y<19f))
                {
                    if (justDisappear)
                    {
                        Destroy(allRigidBodies[r].gameObject);
                    }
                    else
                    { 
                        allRigidBodies[r].isKinematic = true;
                        //allRigidBodies[r].gameObject.SetActive(false);
                        Debug.DrawRay(allRigidBodies[r].transform.position, Vector3.up * 30f, Color.red, 10f);
                    }
                    //allRigidBodies[r].gameObject.
                    //Destroy(allRigidBodies[r]);

                    //allRigidBodies[r] = null;

                }
                else
                {
                    Debug.DrawRay(allRigidBodies[r].transform.position, Vector3.up * 30f, Color.green);
                    lastSlotFilled++;

                    if (lastSlotFilled != r)
                    {
                        allRigidBodies[lastSlotFilled] = allRigidBodies[r];
                        //allRigidBodies[r] = null;
                    }

                    anyAwake = true;
                }

                yield return null;
            }

            bodiesRemaining = lastSlotFilled;
            yield return null;
        }
        if (!justDisappear)
        {
            int perFrame = 0;
            foreach (Rigidbody r in allRigidBodies)
            {
                if (r)
                {
                    //Destroy(r);
                    //Destroy(r.gameObject, Random.value * 5f);
                }
                if (perFrame > 20f)
                {
                    perFrame = 0;
                    yield return null;

                }
                else
                {
                    perFrame++;
                }
            }

            //Destroy(gameObject, 5f);
            //allRigidBodies = null;
            GetCombined();
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
