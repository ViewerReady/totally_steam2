﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfettiController : MonoBehaviour {
    public GameObject confettiPrefab;
    public Transform[] spawnPoints;

    public void LaunchAllConfetti()
    {
        for(int i=0; i<spawnPoints.Length;i++)
        {
            LaunchConfetti(i);
        }
    }

    public void LaunchConfetti(int i)
    {
        if(i<0)
        {
            i = 0;
        }
        else if(i>=spawnPoints.Length)
        {
            i = spawnPoints.Length - 1;
        }

        Destroy(Instantiate(confettiPrefab, spawnPoints[i].position, spawnPoints[i].rotation) as GameObject, 10f);
    }
}
