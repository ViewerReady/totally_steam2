﻿using UnityEngine;
using System.Collections;

public class ClamPitcher : pitchBall {
    public Transform pitchOrigin;
    //public Transform pitchTarget;
    public AudioClip pitchSound;
    public float pitchSoundTime = 1.4f;
    //private AudioSource sound;
    private Rigidbody ballHeld;
    private bool stillPitching;
    private GameObject ballPrefab;
    private Animator anim;
	// Use this for initialization
	void Awake () {
        sound = GetComponentInChildren<AudioSource>();
	}

    new void OnEnable()
    {
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        if (anim)
        {
            anim.Play("Idle", 0);
        }
        hasPitched = false;
        stillPitching = false;

        base.OnEnable();
    }

    new void OnDisable()
    {
        if(ballHeld)
        {
            Destroy(ballHeld.gameObject);
        }
        base.OnDisable();
    }

    public override void pitch(GameObject prefab)
    {
        ballPrefab = prefab;
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        if (anim && !stillPitching)
        {
            anim.SetTrigger("BeginPitch");
            stillPitching = true;
        }
    }

    void PitchSoon()
    {
        ballHeld = (Instantiate(ballPrefab,pitchOrigin.position,pitchOrigin.rotation,pitchOrigin)as GameObject).GetComponent<Rigidbody>();
        //Debug.Log("pitch soon "+ballHeld.transform.parent.name);
        ballHeld.isKinematic = true;
        ballHeld.GetComponent<BallForDucks>().GetPitched();
        hasPitched = false;
    }

    void PitchNow()
    {
       // Debug.Log("pitch now " + ballHeld.transform.parent.name);

        ballHeld.isKinematic = false;
        ballHeld.transform.SetParent(null);
        ballHeld.drag = 0f;
        if (pitchTarget)
        {
            //ballHeld.velocity = Trajectory.GetSlowestVelocityByLanding(pitchOrigin.position, pitchTarget.position);
            ballHeld.velocity = Trajectory.GetLaunchVelocityByLanding(ballHeld.position, pitchTarget.position,pitchingSpeed);
            //Vector3 direction = (pitchTarget.position - pitchOrigin.position);
            //ballHeld.velocity = direction.normalized * pitchingSpeed;
        }
        else
        {
            ballHeld.velocity = (transform.forward+(Vector3.up*.22f)).normalized * pitchingSpeed;
        }

        if(sound && pitchSound)
        {
            sound.clip = pitchSound;
            sound.time = pitchSoundTime;
            sound.Play();
        }
        //Debug.Log("Clam pitch now");
        hasPitched = true;
    }

    void FinishPitch()
    {
        stillPitching = false;
    }
}
