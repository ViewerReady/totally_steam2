﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInEffects : MonoBehaviour {
    public ParticleSystemRenderer[] particles;
    public string colorProperty = "_TintColor";
    public float fadeInTime = .5f;

    private float[] initialAlphas;
	// Use this for initialization
    void Awake()
    {
        initialAlphas = new float[particles.Length];
        for (int a = 0; a < initialAlphas.Length; a++)
        {
            initialAlphas[a] = particles[a].material.GetColor(colorProperty).a;
        }
    }

	void OnEnable () {
       
        StartCoroutine(FadeIn());
	}
	
    IEnumerator FadeIn()
    {
        float countDown = 0f;
        Color tempColor = Color.clear;
        while(countDown<fadeInTime)
        {
            countDown += Time.deltaTime;
            for(int p=0; p< particles.Length;p++)
            {
                tempColor = particles[p].material.GetColor(colorProperty);
                tempColor.a = (countDown / fadeInTime) * initialAlphas[p];
                particles[p].material.SetColor(colorProperty,tempColor);
            }
            yield return null;
        }

        for (int p = 0; p < particles.Length; p++)
        {
            tempColor = particles[p].material.GetColor(colorProperty);
            tempColor.a = initialAlphas[p];
            particles[p].material.SetColor(colorProperty, tempColor);
        }
    }
}
