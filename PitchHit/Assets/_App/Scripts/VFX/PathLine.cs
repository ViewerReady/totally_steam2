﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

[RequireComponent(typeof(LineRenderer))]
public class PathLine : MonoBehaviour {
    public enum LinePattern
    {
        SOLID,
        DOTTED,
        SQUARE_DOTTED,
    }

    private LinePattern currentPattern;

    protected LineRenderer line;
    public float[]  scrollSpeeds;

    private bool doubleSided = false;
    private LineRenderer otherSide;
/*
    public Material solidMat;
    public Material dottedMat;
    public Material squareDottedMat;
    */
    private Vector3[] _positions = new Vector3[0];

    void Awake()
    {
        line = GetComponent<LineRenderer>();
        _positions = new Vector3[line.positionCount];
        line.GetPositions(_positions);
    }

    void Start()
    {
        SetPattern(currentPattern);
        if (doubleSided)
        {
            GameObject other = new GameObject();
            other.transform.SetParent(this.transform);
            otherSide = other.AddComponent<LineRenderer>(line);
            otherSide.transform.localRotation = Quaternion.Euler(0, 180, 0);
            SetPattern(currentPattern);
            SetLineWidthMultiplier(line.widthMultiplier);
        }
    }


    void Update()
    {
        
        for(int m = 0; m <line.sharedMaterials.Length;m++)
        {
            float offset = (Time.time * scrollSpeeds[m%scrollSpeeds.Length]) % 1f;
            line.sharedMaterials[m].mainTextureOffset = new Vector2(-offset, 0);
        }
        /*
        if(scrollSpeed != 0)
        {
            float offset = (Time.time * scrollSpeed)%1f;
            line.sharedMaterial.mainTextureOffset = new Vector2(-offset, 0);
            if (otherSide)
            {
                otherSide.sharedMaterial.mainTextureOffset = new Vector2(-offset, 0);
            }
        }
        */
    }

    void OnDisable()
    {
        for (int m = 0; m < line.sharedMaterials.Length; m++)
        {
            line.sharedMaterials[m].mainTextureOffset = Vector2.zero;
        }
    }
    /*
    public void SetScrollSpeed(float speed)
    {
        scrollSpeed = speed;
    }
    */

    public void SetLineWidthMultiplier(float widthMultiplier)
    {
        widthMultiplier = transform.lossyScale.x/ widthMultiplier;
        line.widthMultiplier = widthMultiplier;
        if (otherSide)
        {
            otherSide.widthMultiplier = widthMultiplier;
        }
    }

	public void SetColor(Color c)
    {
        line.startColor = c;
        line.endColor = c;

        if (otherSide)
        {
            otherSide.startColor = c;
            otherSide.endColor = c;
        }
    }

    public void SetPattern(LinePattern pat)
    {
        //if (pat == currentPattern) return;
        /*
        switch (pat)
        {
            case LinePattern.SOLID:
                line.material = solidMat;
                if (otherSide)
                {
                    otherSide.material = solidMat;

                }
                break;
            case LinePattern.DOTTED:
                line.material = dottedMat;
                line.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);
                if (otherSide)
                {
                    otherSide.material = dottedMat;
                    otherSide.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);
                }
                break;
            case LinePattern.SQUARE_DOTTED:
                line.material = squareDottedMat;
                line.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);
                if (otherSide)
                {
                    otherSide.material = dottedMat;
                    otherSide.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);
                }
                break;

        }
        */
        currentPattern = pat;
    }

    public void ShowHide(bool isShowing)
    {
        line.gameObject.SetActive(isShowing);
        line.enabled = isShowing;
        if (otherSide)
        {
            otherSide.gameObject.SetActive(isShowing);
            otherSide.enabled = isShowing;
        }
    }
    public void SetPositions(Vector3[] positions)
    {
        //Debug.Log("Set trajectory positions "+positions.Length);
        _positions = positions;
        line.positionCount = positions.Length;
        line.SetPositions(positions);
        if (otherSide)
        {
            otherSide.positionCount = positions.Length;
            otherSide.SetPositions(positions);
        }
        switch (currentPattern)
        {
            case LinePattern.SOLID:
                break;
            case LinePattern.DOTTED:
                //line.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);
                if (otherSide)
                {
                    //otherSide.materials[0].mainTextureScale = new Vector3(GetLineDistance(), 1, 1);

                }
                break;
        }
    }

    protected float GetLineDistance()
    {
        float totalDist = 0;
        if (_positions.Length <= 1) return 0;
        for(int i =0; i<_positions.Length-1; i++)
        {
            totalDist += Vector3.Distance(_positions[i], _positions[i + 1]);
        }

        return totalDist;
    }

    
}

public static class ComponentCopy
{
    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }
}
