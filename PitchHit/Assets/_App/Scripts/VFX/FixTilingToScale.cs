﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTilingToScale : MonoBehaviour {
    public bool useSquareRoot = true;
    public float factor = 1f;
    private ParticleSystem particles;
    private ParticleSystemRenderer renderer;
	// Use this for initialization
	void Start () {
        particles = GetComponent<ParticleSystem>();
        renderer = GetComponent<ParticleSystemRenderer>();	
	}
	// Update is called once per frame
	void Update () {
        if (useSquareRoot)
        {
            renderer.material.mainTextureScale = new Vector3(Mathf.Sqrt(transform.lossyScale.x * factor), 1f, Mathf.Sqrt(transform.lossyScale.z * factor));
        }
        else
        {
            renderer.material.mainTextureScale = new Vector3(transform.lossyScale.x * factor, transform.lossyScale.y * factor, transform.lossyScale.z * factor);
        }
	}
}
