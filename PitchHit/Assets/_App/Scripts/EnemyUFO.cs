﻿using UnityEngine;
using System.Collections;

public class EnemyUFO : MonoBehaviour {
    [HideInInspector]
    public Transform target;//assigned by spawner
    [HideInInspector]
    public float speed = 5f;//assigned by spawner
    [HideInInspector]
    public float preferredElevation = 5f;//assignedBySpawner
    [HideInInspector]
    public float attackRange;//assignedBySpawner
    [HideInInspector]
    public float timeToFire = 7f;//assigned by spawner
    //[HideInInspector]
    //public float shootSpeed = 20f;// assigned by spawner
    [HideInInspector]
    public float acceleration = 50f;//assigned by spawner
    [HideInInspector]
    public float tailgateDistance = 2f;//assigned by spawner
    [HideInInspector]
    public float farAheadToPlan = 5f;//assigned by spawner
    [HideInInspector]
    public float timeAlive;

    private SpaceCannon cannon;
    private UFOHull hull;

    public Collider shipCollider;
    public int pointsForDestruction = 25;
    public int pointsForDamage = 5;
    public float totalHealth = 25f;
    public GameObject damageParticle;
    public AudioClip soundOfSpawn;
    public AudioClip soundOfIdle;
    public AudioClip soundOfDying;
    private AudioSource sound;

    private float currentHealth;
    
    
    public bool debugStates;
    private Renderer hullSkin;


    private Vector3 nextLocation;
    private Vector3 preferredVelocity;
    private Vector3 blockedSweepDirection;
    private Vector3 currentVelocity;
    private bool begun;
    private bool arrived;
    private bool arriving;
    private bool blocked;
    private Rigidbody rigid;
    private Rigidbody shipRigid;
    private float countDown;

    private static UFOSpawnController spawner;
	// Use this for initialization
	void Start () {
        sound = GetComponentInChildren<AudioSource>();
        cannon = GetComponentInChildren<SpaceCannon>();
        cannon.target = target;
        //cannon.projectileSpeed = shootSpeed;
        rigid = GetComponent<Rigidbody>();
        shipRigid = shipCollider.GetComponent<Rigidbody>();
        nextLocation = transform.position;
        hull = GetComponentInChildren<UFOHull>();
        hullSkin = GetComponentInChildren<Renderer>();
        currentHealth = totalHealth;

        if(!spawner)
        {
            spawner = FindObjectOfType<UFOSpawnController>();
        }

        if(!ScoreboardController.instance.scorable)
        {
            Destroy(cannon.gameObject);
        }

        StartCoroutine(SpawnSound());
    }

    IEnumerator SpawnSound()
    {
        if (sound)
        {
            if (soundOfSpawn)
            {
                sound.loop = false;
                sound.clip = soundOfSpawn;
                sound.Play();

                yield return new WaitForSeconds(soundOfSpawn.length);
            }
            if (soundOfIdle)
            {
                sound.loop = true;
                sound.clip = soundOfIdle;
                sound.Play();
            }
        }
    }

	public void SufferDamage(float damage, Collision c)
    {
        if (damageParticle)
        {
            //Debug.Log("do the damage particle!");
            ContactPoint impactPoint = c.contacts[0];
            float angleFromUpToImpact = Vector3.Angle(Vector3.up, impactPoint.normal);
            Vector3 axis = Vector3.Cross(Vector3.up, impactPoint.normal);
            GameObject smoke = Instantiate(damageParticle, impactPoint.point, Quaternion.AngleAxis(angleFromUpToImpact, axis),null) as GameObject;
            smoke.transform.SetParent(hull.transform);
        }

        if(currentHealth<=0)//no more sounds or points after it's dead.
        {
            return;
        }

        if (c.gameObject.GetComponent<EnemyProjectile>())
        {
            currentHealth = 0f;
            //Debug.Log("hit by own blast ball!");
        }
        else
        {
            // Debug.Log("hit by something else!");
            currentHealth -= damage;
        }

        if (currentHealth<=0)
        {
            //Debug.Log("dieeee "+currentHealth);
            arrived = true;
            begun = false;
            if (cannon)
            {
                Destroy(cannon.gameObject);
            }
            hull.BeginDestruction();
            if (soundOfDying && sound)
            {
                sound.clip = soundOfDying;
                sound.loop = false;
                sound.Play();
                Destroy(gameObject, soundOfDying.length+.1f);
            }
            else
            {
                Destroy(gameObject, 2f);
            }
            //UFOSpawnController.countUFO--;
            ScoreboardController.instance.AddScore(pointsForDestruction);
        }
        else
        {
            ScoreboardController.instance.AddScore(pointsForDamage);
        }

    }

	// Update is called once per frame
	void FixedUpdate () {
        if (begun)
        {
            if (rigid)
            {
                if(arriving || blocked || arrived)
                {
                    Vector3 toWaitingSpot = nextLocation - transform.position;
                    if (toWaitingSpot.magnitude > .1f)
                    {
                        preferredVelocity = toWaitingSpot;
                    }
                    currentVelocity = Vector3.MoveTowards(currentVelocity, preferredVelocity, acceleration * Time.deltaTime);
                    rigid.MovePosition(rigid.position + (currentVelocity * Time.deltaTime));
                    preferredVelocity = currentVelocity;

                    if (blocked)
                    {
                        if (debugStates)
                        {
                            Debug.DrawLine(transform.position, nextLocation, Color.red);
                            hullSkin.material.color = Color.red;
                        }
                    }
                    else if(arriving)
                    {
                        if (debugStates)
                        {
                            Debug.DrawLine(transform.position, nextLocation, Color.cyan);
                            hullSkin.material.color = Color.cyan;
                        }
                        if (toWaitingSpot.magnitude < 1f && currentVelocity.magnitude < .75f)
                        {
                            arrived = true;
                            arriving = false;
                        }
                    }
                    else if(arrived && ScoreboardController.instance.scorable)
                    {
                        if (debugStates)
                        {
                            hullSkin.material.color = Color.black;
                        }
                        countDown -= Time.deltaTime;
                        if (countDown < 0)
                        {
                            if (cannon)
                            {
                                cannon.Fire();
                                countDown = timeToFire;
                            }
                        }
                    }
                }
                else
                {
                    if (debugStates)
                    {
                        hullSkin.material.color = Color.white;
                        Debug.DrawLine(transform.position, nextLocation, Color.white);
                    }
                    
                    currentVelocity = Vector3.MoveTowards(currentVelocity, preferredVelocity, Time.deltaTime * acceleration);
                        
                    rigid.MovePosition(rigid.position + (currentVelocity * Time.deltaTime));
                }
            }
            
        }
        else
        {
            StartCoroutine(SteeringRoutine());
        }

        
    }

    void Update()
    {
        timeAlive += Time.deltaTime;

        //if (!blocked && Input.GetKey("k"))
        //{
        //    Destroy(gameObject);
        //}
    }

    IEnumerator SteeringRoutine()
    {
        begun = true;

        while (!arrived)
        {
            RaycastHit hit;

            if (!blocked)
            {
                if (!arriving)
                {

                    //difference to target
                    nextLocation = target.position - transform.position;

                    //flattened difference to target
                    nextLocation = new Vector3(nextLocation.x, 0f, nextLocation.z);
                    float remainingDistance = nextLocation.magnitude;
                    if (remainingDistance - farAheadToPlan < attackRange)
                    {
                        nextLocation = transform.position + (nextLocation.normalized * (remainingDistance - attackRange));
                        arriving = true;

                    }
                    else
                    {
                        nextLocation = transform.position + (nextLocation.normalized * farAheadToPlan);
                    }
                    Ray lookDown = new Ray(nextLocation + (Vector3.up * 10f), -Vector3.up);
                    Debug.DrawRay(lookDown.origin, lookDown.direction * 100f, Color.red,10f);
                    if (Physics.Raycast(lookDown, out hit, 100f, 1 << LayerMask.NameToLayer("Ground")))
                    {
                        Debug.Log("ufo elevation raycast finds "+ hit.collider.gameObject.name);
                        nextLocation = hit.point + (Vector3.up * preferredElevation);
                    }
                    else
                    {
                        Debug.Log("UFO elevation raycast finds nothing!");
                    }

                    blockedSweepDirection = nextLocation - transform.position;
                    if (rigid.SweepTest(blockedSweepDirection, out hit, blockedSweepDirection.magnitude))
                    {
                        EnemyUFO otherUFO = hit.transform.GetComponent<EnemyUFO>();
                        if (otherUFO)
                        {
                            if (otherUFO.timeAlive > timeAlive)
                            {
                                //just go halfway
                                nextLocation = transform.position + blockedSweepDirection.normalized * (hit.distance - tailgateDistance);
                                //nextLocation = nextLocation + ((transform.position - nextLocation).normalized * tailgateDistance);
                                blocked = true;
                            }
                            
                        }
                       
                    }
                    if (!blocked)
                    {
                        preferredVelocity = (nextLocation - transform.position).normalized * speed;
                        float travelTime = (nextLocation - transform.position).magnitude / speed;
                        yield return new WaitForSeconds(travelTime);
                    }
                    else
                    {
                        if(arriving)
                        {
                            arriving = false;
                        }
                        preferredVelocity = Vector3.zero;
                        
                        yield return new WaitForSeconds(2f);
                    }
                        
                }
                else
                {
                    //arriving
                    yield return new WaitForSeconds(4f);//no reason to not just return here, but whatever.
                }
            }
            else
            {
                blockedSweepDirection = nextLocation - transform.position;

                if (rigid.SweepTest(blockedSweepDirection, out hit, blockedSweepDirection.magnitude))
                {
                    EnemyUFO otherUFO = hit.transform.GetComponent<EnemyUFO>();
                    if(!otherUFO)
                    {
                        otherUFO = hit.transform.GetComponent<EnemyUFO>();
                        if(otherUFO)
                        {
                            Debug.DrawLine(transform.position,transform.position+(Vector3.up*5f),Color.red,10f);
                        }
                    }
                    if (otherUFO)
                    {
                        if (otherUFO.timeAlive > timeAlive)
                        {
                            blocked = true;//still blocked. this changes nothing.
                        }
                    }
                    else
                    {
                        blocked = false;
                        Debug.DrawLine(transform.position, transform.position + (Vector3.up * 5f), Color.yellow,10f);
                    }
                }
                else
                {
                    blocked = false;
                    Debug.DrawLine(transform.position, transform.position + (Vector3.up * 5f), Color.blue,10f);
                }
                if(!blocked)
                {
                    nextLocation = transform.position += blockedSweepDirection;
                    float travelTime = (nextLocation - transform.position).magnitude / speed;
                    yield return new WaitForSeconds(travelTime);
                }
                else
                {
                    yield return new WaitForSeconds(2f);
                }
            }
            
        }
    }

    private float additionalForceStrength = 50000f;
    void OnCollisionEnter(Collision c)
    {
        ContactPoint firstContact = c.contacts[0];
        hittable ball = c.collider.GetComponent<hittable>();
        Debug.Log("impact happened");
        if(ball)
        {
            Debug.Log("impacted by ball");
           shipRigid.AddForceAtPosition(c.rigidbody.mass * c.rigidbody.velocity * additionalForceStrength, firstContact.point, ForceMode.Impulse);

        }


            /*
        EnemyUFO otherUFO = c.gameObject.GetComponent<EnemyUFO>();
        if(otherUFO)
        {
            //blocked = true;
            //blockedSweepDirection = nextLocation - transform.position;

            if (otherUFO.timeAlive> timeAlive)
            {
                //yield
            }
            else
            {
                //they yield
            }
        }*/
    }

    void OnDestroy()
    {
        UFOSpawnController.countUFO--;
    }
}
