﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEventHandles : MonoBehaviour {

    private TotallyTutorial tutorial;

    public void Initialize(TotallyTutorial tut)
    {
        tutorial = tut;
    }
    public void SetAudioStartTime(float time)
    {
        tutorial.SetAudioStartTime(time);
    }

    public void StartAudioTrack(AudioClip clip)
    {
        tutorial.StartAudioTrack(clip);
    }

    public void SetTutorialReady()
    {
        tutorial.SetTutorialReady();
    }

    public void ShowBat()
    {
        tutorial.ShowBat();
    }
    public void HideBat()
    {
        tutorial.HideBat();
    }

    public void LoadBallIntoTutorialGuideHand()
    {
        tutorial.LoadBallIntoTutorialGuideHand();
    }

    public void TeachHitting()
    {
        tutorial.TeachHitting();
    }

    public void TeachRunning()
    {
        tutorial.TeachRunning();
    }

    public void TeachCatching()
    {
        tutorial.TeachCatching();
    }

    public void TeachThrowing()
    {
        tutorial.TeachThrowing();
    }

    public void TeachThrowingStrike()
    {
        tutorial.TeachThrowingStrike();
    }

    public void TeachManualThrowing()
    {
        tutorial.TeachManualThrowing();
    }

    public void ServeBall()
    {
        tutorial.ServeBall();
    }

    public void HighlightGrips()
    {
        tutorial.HighlightGrips();
    }

    public void HighlightRun()
    {
        tutorial.HighlightRun();
    }

    public void HighlightTrigger()
    {
        tutorial.HighlightTrigger();
    }
}
