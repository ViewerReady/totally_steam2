﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class DeviceChooser : MonoBehaviour {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////public SteamVR_Controller.DeviceRelation deviceRelation;
    public ETrackedDeviceClass deviceClass;
    public DeviceChooser otherDevice;
    public bool showRenderModel;
    //public int minimumOfClassPresent;

    private SteamVR_RenderModel renderModel;
    private SteamVR_TrackedObject trackedObject;
    private Renderer rend;
    private bool isChosen;
    public bool canCycleThrough;
    public bool chooseDeviceOnAwake = true;
    public bool keepTrying = true;
	// Use this for initialization
	void Start () {
        trackedObject = GetComponent<SteamVR_TrackedObject>();
        if (chooseDeviceOnAwake)
        {
            ChooseDevice();
        } 
	}

    public void ShowRenderModel(bool s)
    {
        if (isChosen && rend)
        {
            rend.enabled = s;
        }
        else
        {
            StartCoroutine(SetShowRenderModel(s));
        }
    }

    IEnumerator SetShowRenderModel(bool show)
    {
        yield return new WaitForSeconds(3f);
        if(!rend)
        {
            rend = GetComponent<Renderer>();
        }
        if (rend)
        {
            rend.enabled = false;
        }
        
        showRenderModel = show;

        if (!renderModel)
        {
            renderModel = GetComponent<SteamVR_RenderModel>();
        }
        if (!renderModel)
        {
            renderModel = gameObject.AddComponent<SteamVR_RenderModel>();
        }

        renderModel.index = trackedObject.index;
        if (renderModel.index != SteamVR_TrackedObject.EIndex.None)
        {
            renderModel.UpdateModel();
        }
        if (!show)
        {
            StartCoroutine(EliminateRendererRoutine());
        }
    }

    IEnumerator EliminateRendererRoutine()
    {
        //if(!rend)
        //{
        //    rend = GetComponent<Renderer>();
        //}
        while(rend == null)
        {
            yield return new WaitForSeconds(1f);
            rend = GetComponent<Renderer>();
        }

        rend.enabled = false;
    }

    public SteamVR_TrackedObject.EIndex GetIndex()
    {
        if(!trackedObject)
        {
            trackedObject = GetComponent<SteamVR_TrackedObject>();
            if(!trackedObject)
            {
                return SteamVR_TrackedObject.EIndex.None;
            }
        }

        return trackedObject.index;

    }
    

    public void ChooseDevice()
    {
        if(deviceClass == ETrackedDeviceClass.Invalid)
        {
            return;
        }

        var system = OpenVR.System;
        if (system == null)
        {
            return;
        }

        StartCoroutine(DeviceChoosingRoutine());
    }

    IEnumerator DeviceChoosingRoutine()
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*while (!isChosen)
        {
            SteamVR_TrackedObject.EIndex chosenIndex = (SteamVR_TrackedObject.EIndex)SteamVR_Controller.GetDeviceIndex(deviceRelation, deviceClass);
            //ETrackedDeviceClass.GenericTracker

            if (chosenIndex != SteamVR_TrackedObject.EIndex.None)
            {
                if (!otherDevice || otherDevice.GetIndex() != chosenIndex)
                {
                    trackedObject.index = chosenIndex;

                    isChosen = true;
                    //SteamVR_Events.HideRenderModels.Send(false);
                }
                StartCoroutine(SetShowRenderModel(showRenderModel));
            }
            yield return null;
        }*/
        yield return null;
    }
}
