﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ButtonPointer : MonoBehaviour {
    public Transform contactGraphic;
    protected LayerMask buttonLayer;
    protected LayerMask menuLayer;
    protected bool isOnButton;
    protected MonoBehaviour currentButton;
    protected float timeOnSameButton = 0f;

    // Use this for initialization
    protected void Start () {
        buttonLayer = LayerMask.GetMask("Button");
        menuLayer = LayerMask.GetMask("Menu");
    }

    void OnDisable()
    {
        NoHit();
    }

    protected virtual void NoHit()
    {
        timeOnSameButton = 0f;
        isOnButton = false;
        if (contactGraphic)
        {
            contactGraphic.gameObject.SetActive(false);
            contactGraphic.localPosition = Vector3.zero;
        }
    }

    protected void MoveContactGraphic(RaycastHit hit, Transform button)
    {
        contactGraphic.gameObject.SetActive(true);
        contactGraphic.position = hit.point + .0001f * hit.normal;
        contactGraphic.rotation = Quaternion.LookRotation(-hit.normal, button.up);
    }
}
