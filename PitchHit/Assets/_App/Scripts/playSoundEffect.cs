﻿using UnityEngine;
using System.Collections;

public class playSoundEffect : MonoBehaviour {

	public randomSoundEffect effectEngine;
    public bool listenForCollision;
    public bool invokePostSplit = true;
	private FracturedObject fractureEngine;
    private bool soundMade;

	// Use this for initialization
	void Start () {
		fractureEngine = GetComponent<FracturedObject> ();

        // may not exist
        if (fractureEngine)
        {
            fractureEngine.EventDetachedAnyCallGameObject = gameObject;
            fractureEngine.EventDetachedAnyCallMethod = "playEffect";
        }
    }

	public void playEffect(){
        Debug.Log("PLAY SOUND EFFECT" +gameObject.name);

        if (soundMade || Time.timeSinceLevelLoad<.1f)
        {
            return;
        }

        StartCoroutine(AllowNewSound());

        if (invokePostSplit)
        {
            pointsOnShatter shatter = GetComponent<pointsOnShatter>();
            if (shatter)
            {
                Debug.Log("call points on shatter.");
                shatter.PostSplit();
            }
        }

        SmashKillSubobjects smash = GetComponent<SmashKillSubobjects>();
        if (smash)
        {
            smash.SmashKill();
        }

        if (effectEngine)
        {
            effectEngine.playClipAt(transform.position);
        }
        
        
    }


    IEnumerator AllowNewSound()
    {
        soundMade = true;
        yield return null;
        soundMade = false;
    }
    // Update is called once per frame
    //void Update () {

    //}

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (listenForCollision)
        {
            if (collisionInfo.rigidbody.CompareTag("ball"))
            {
                playEffect();
            }
        }
    }
}
