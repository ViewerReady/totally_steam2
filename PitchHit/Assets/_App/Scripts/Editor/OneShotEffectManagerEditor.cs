﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

//Tells the Editor class that this will be the Editor for the WaveManager
[CustomEditor(typeof(OneShotEffectManager))]
public class OneShotEffectManagerEditor : Editor
{
    //The array property we will edit
    SerializedProperty effects;

    //The Reorderable list we will be working with
    ReorderableList list;

    private void OnEnable()
    {
        //Gets the wave property in WaveManager so we can access it. 
        effects = serializedObject.FindProperty("effectsList");

        //Initialises the ReorderableList. We are creating a Reorderable List from the "wave" property. 
        //In this, we want a ReorderableList that is draggable, with a display header, with add and remove buttons        
        list = new ReorderableList(serializedObject, effects, true, true, true, true);

        list.drawElementCallback = DrawListItems;
        list.drawHeaderCallback = DrawHeader;

    }

    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index); //The element in the list

        // Create a property field and label field for each property. 

        // The 'mobs' property. Since the enum is self-evident, I am not making a label field for it. 
        // The property field for mobs (width 100, height of a single line)
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("name"),
            GUIContent.none
        );

        //The property field for level. Since we do not need so much space in an int, width is set to 20, height of a single line.
        EditorGUI.PropertyField(
            new Rect(rect.x + 160, rect.y, 100, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("prefab"),
            GUIContent.none
        );


    }

    void DrawHeader(Rect rect)
    {
        string name = "Effects";
        EditorGUI.LabelField(rect, name);
    }

    //This is the function that makes the custom editor work
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}