﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(JerseyTextureCreator))]
public class JerseyTextureCreatorEditor : Editor {

    SerializedProperty _name;
    SerializedProperty _number;


    void OnEnable()
    {
        // Fetch the objects from the GameObject script to display in the inspector
        _name = serializedObject.FindProperty("nameText");
        _number = serializedObject.FindProperty("numberText");
    }


    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(_name);
        EditorGUILayout.PropertyField(_number);

        if (GUILayout.Button("Capture"))
        {
            ((JerseyTextureCreator)target).Capture("test", "01");
        }

        serializedObject.ApplyModifiedProperties();

    }
}
