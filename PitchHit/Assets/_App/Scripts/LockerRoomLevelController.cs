﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;
#if RIFT
using Oculus.Platform;
#endif

public class LockerRoomLevelController : MonoBehaviour {

    public bool demoMode;
    public bool allUnlocked;
    //public int demoLevel = 0;
    public static bool isDemo;
    private string demoLockText = "Buy the game\nAvailable now!";
    private string needsHandsText = "Requires Hand\nControllers";
    public RealButton[] levelButtons;
    public RealButton leftHandedButton;
    public RealButton fullBatterButton;
    public Text batGripText;
    public Text whichHandText;
    public RealButton[] fullBatterRadioButtons;
    public RealButton[] needHandsButtons;

    public GameObject swapOutIfDemo;
    public GameObject swapInIfDemo;
    public Transform ballBucket;

   // private Player thisPlayer;
    private TutorialController tutorial;
    private float timeToResetAllLevels = 3f;
    private float countdown;
#if RIFT
    //private Oculus.Platform.Message.Callback OnEntitlementCheckComplete;
#endif
    // Use this for initialization
    void Awake () {
        if (fullBatterButton)
        {
            fullBatterButton.gameObject.SetActive(false);
        }
        if (swapOutIfDemo)
        {
            swapOutIfDemo.SetActive(!demoMode);
        }
        if (swapInIfDemo)
        {
            swapInIfDemo.SetActive(demoMode);
        }

        if (demoMode)
        {
            isDemo = true;
            levelButtons[0].SetIsLocked(false);
           
            //bool demoLevelFound = false;
            for (int lev = 0; lev < levelButtons.Length; lev++)
            {
               // if(lev == demoLevel)
               // {
               //     demoLevelFound = true;
               //     levelButtons[lev].SetIsLocked(false);
               // }
                //else
                //{
                    levelButtons[lev].SetIsLocked(true);
                    levelButtons[lev].GetComponentInChildren<Text>(true).text = demoLockText;
                //}
            }
            //if(!demoLevelFound)
            //{
            //    levelButtons[0].SetIsLocked(true);
            //    levelButtons[0].GetComponentInChildren<Text>(true).text = demoLockText;
            //}

        }
        else
        {

            if(allUnlocked)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED",levelButtons.Length-1);
            }

            DetermineLevelsUnlocked();
        }

        //load if left handed
        string tempHand = PlayerPrefs.GetString("HAND", "RIGHT");
        switch (tempHand)
        {
            case "RIGHT":
                HandManager.dominantHand = HandManager.HandType.right;
                whichHandText.text = "Righty";
                leftHandedButton.isActivated = false;
                break;
            case "LEFT":
                whichHandText.text = "Lefty";
                HandManager.dominantHand = HandManager.HandType.left;
                leftHandedButton.isActivated = false;
                break;
            case "BOTH":
                whichHandText.text = "Ambidexterity";
                HandManager.dominantHand = HandManager.HandType.both;
                leftHandedButton.SetActivation(false);
                break;
        }

        leftHandedButton.RefreshColor();
        batGripText.text = hitThings.GetCurrentGripType().ToString();
    }
	
    void Start()
    {
        /*
        if(GameController.isTrueOculus)
        {
            //ballBucket.localPosition += Vector3.forward * .8f;
            ballBucket.localPosition += Vector3.up * .4f;
            //ballBucket.SetParent(ballBucketHolderForOculus);
            
            //ballBucket.localPosition = Vector3.zero;
            //ballBucket.localRotation = Quaternion.identity;
        }
        */
#if RIFT && !UNITY_EDITOR
        Entitlements.IsUserEntitledToApplication().OnComplete(
  (Message msg) =>
  {
      if (msg.IsError)
      {
          // User is NOT entitled.
          UnityEngine.Application.Quit();
      }
  }
);



#endif

        if (GameController.fullBatterMode ||ballController.instance.genericTrackerHasBeenSeen)
        {
            if (fullBatterButton)
            {
                fullBatterButton.gameObject.SetActive(true);
            }
            if (GameController.fullBatterMode && fullBatterButton)
            {
                fullBatterButton.SetActivation(true);
            }
        }
        
    }

    // Update is called once per frame
    void Update () {
#if RIFT
        Oculus.Platform.Request.RunCallbacks();
#endif
        //if(Input.GetKeyDown("b"))
       // {
         //   GameController.fullBatterMode = !GameController.fullBatterMode;
        //}

	    if(!allUnlocked && !isDemo)
        {
            if(Input.GetKey("k"))
            {
                if(countdown<=0)
                {
                    PlayerPrefs.SetString("HAND", "RIGHT");
                    PlayerPrefs.SetInt("LEVELSUNLOCKED", 0);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
                else
                {
                    countdown -= Time.deltaTime;
                }
            }
            else
            {
                countdown = timeToResetAllLevels;
            }

            //if(Input.GetKeyDown("p"))
            //{
            //    GameController.isPitchingMode = !GameController.isPitchingMode;
            //}
           
        }
        //Debug.Log("mixed bat button? " + ballController.instance.genericTrackerHasBeenSeen + " " + HandManager.genericTracker + " " + HandManager.genericTracker.IsValid());

        if (ballController.instance.genericTrackerHasBeenSeen && fullBatterButton)
        {
            fullBatterButton.gameObject.SetActive(true);
        }
    }

    private Coroutine ongoingTipCalibRoutine;
    public void StartAttemptingBatTipCalibration()
    {
        Debug.Log("0000000000000000000000000000000000000000000000000000000000000000");
        StopAttemptingBatTipCalibration();
        ongoingTipCalibRoutine = StartCoroutine(BatTipCalibRoutine());
    }

    public void StopAttemptingBatTipCalibration()
    {
        if (ongoingTipCalibRoutine != null)
        {
            Debug.Log("Stop the old one!");
            StopCoroutine(ongoingTipCalibRoutine);
        }
    }

    IEnumerator BatTipCalibRoutine()
    {
        Debug.Log("calib?");
        bool stillTrying = true;
        float timeAcceptable = 0f;
        float timeRequired = 2f;
        
        Transform tip = HandManager.genericTracker.transform;
        Vector3 previousPosition = Vector3.zero;
        while(stillTrying)
        {
            Debug.Log("still trtying.");
            if(tip.position.y>.1f && tip.position.y<.56)
            {
                if(Vector3.Angle(tip.up,Vector3.up)<7f)
                {
                    if((Vector3.Distance(tip.position,previousPosition)/Time.deltaTime)<.1f)
                    {
                        Debug.Log("acceptable "+timeAcceptable);
                        if (timeAcceptable > timeRequired)
                        {
                            HandManager.genericTracker.CalibrateTipLength();
                            stillTrying = false;
                        }
                        else
                        {
                            timeAcceptable += Time.deltaTime;
                        }
                    }
                    else
                    {
                        Debug.Log("too much movement "+ (Vector3.Distance(tip.position, previousPosition) / Time.deltaTime));
                        timeAcceptable = 0f;
                    }
                }
                else
                {
                    Debug.Log("wrong angle"+ Vector3.Angle(tip.up, Vector3.up));
                    timeAcceptable = 0f;
                }
            }
            else
            {
                Debug.Log("wrong elevation"+ tip.position.y);
                timeAcceptable = 0f;
            }
            previousPosition = tip.position;
            yield return null;
        }
    }

    public void DetermineLevelsUnlocked()
    {
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED", 0);
        for (int lev = 0; lev < levelButtons.Length; lev++)
        {
            if (lev <= levelsUnlocked)
            {
                levelButtons[lev].SetIsLocked(false);
            }
            else
            {
                levelButtons[lev].SetIsLocked(true);
            }
        }
        Debug.Log("levels unlocked. "+levelsUnlocked);
    }

    public void SetLeftHanded(bool lefty)
    {
        if(lefty)
        {
            Debug.Log("SetLefty "+Time.time);
            HandManager.SetDominantHand(HandManager.HandType.left);
            whichHandText.text = "Lefty";
            //HandManager.currentRightCustomHand.GetComponent<CustomHand>().showRenderModel = true;
        }
        else
        {
            Debug.Log("SetLefty " + Time.time);

            HandManager.SetDominantHand(HandManager.HandType.right);
            whichHandText.text = "Righty";
            //HandManager.currentLeftCustomHand.GetComponent<CustomHand>().showRenderModel = true;
        }

        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }
    }

    public void ToggleHandedness()
    {
        SetLeftHanded(HandManager.dominantHand != HandManager.HandType.left);
#if RIFT
        OVRInput.SetControllerVibration(0f, 0f, HandManager.secondaryOculusHand);
        StartCoroutine(OculusHapticOverride(.25f));
#else

        if(HandManager.currentLeftCustomHand)
        {
            //HandManager.currentLeftCustomHand.showRenderModel = true;
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if (HandManager.currentRightCustomHand)
        {
            //HandManager.currentRightCustomHand.showRenderModel = true;
            HandManager.currentRightCustomHand.RefreshHandState();
        }

        if (hitThings.instance.attached)
        {
             hitThings.instance.attachedCustomHand.showRenderModel = false;
        }
#endif
    }
#if RIFT
    IEnumerator OculusHapticOverride(float duration)
    {
        float countdown = duration;
        while(countdown>0)
        {
            OVRInput.SetControllerVibration(.2f, .2f, HandManager.OculusHand(hitThings.instance.attachedCustomHand));
            countdown -= Time.deltaTime;
            yield return null;
        }
        OVRInput.SetControllerVibration(0f, 0f, HandManager.dominantOculusHand);
    }
#endif
    public void SetNextBatGripStyle()
    {
        hitThings.gripType grip = hitThings.SelectNextGripType();
        if(grip != hitThings.gripType.always)
        {
            if(!tutorial)
            {
                tutorial = FindObjectOfType<TutorialController>();
            }
            StartCoroutine(ConsiderTeachingBatGrip());
        }
        batGripText.text = grip.ToString();

#if RIFT
        OVRInput.SetControllerVibration(0f, 0f, HandManager.OculusHand(hitThings.instance.attachedCustomHand));
#endif
    }

    IEnumerator ConsiderTeachingBatGrip()
    {
        yield return new WaitForSeconds(.1f);
        tutorial.TryBatTutorialAgain();
    }

    
    public void SetFullBatterMode(bool full)
    {
        foreach (RealButton b in fullBatterRadioButtons)
        {
            b.gameObject.SetActive(full);
        }
        GameController.fullBatterMode = full;

        if(full)
        {
            Debug.Log("already full.");
            //refresh full batter radio buttons
            switch(GameController.batType)
            {
                case GameController.FullBatType.TrackerAtBase:
                    Debug.Log("enter base.");
                    fullBatterRadioButtons[0].SetActivation(true);
                    //StopAttemptingBatTipCalibration();
                    break;
                case GameController.FullBatType.HTCRacquet:
                    Debug.Log("enter racquet");
                    fullBatterRadioButtons[1].SetActivation(true);
                    //StopAttemptingBatTipCalibration();
                    break;
                case GameController.FullBatType.TrackerAtTip:
                    Debug.Log("enter tip.");
                    fullBatterRadioButtons[2].SetActivation(true);
                    //StartAttemptingBatTipCalibration();
                    break;
            }

            //lockup levels that need hands
            foreach(RealButton b in needHandsButtons)
            {
                b.SetIsLocked(true);
                if(!demoMode)
                {
                    b.GetComponentInChildren<Text>(true).text = needsHandsText;
                }
            }

        }
        else
        {
            //assess levels that need hands
            DetermineLevelsUnlocked();
        }
    }

    public void UpdateBatType()
    {
        switch (GameController.batType)
        {
            case GameController.FullBatType.TrackerAtBase:
                Debug.Log("enter base.");
                StopAttemptingBatTipCalibration();
                break;
            case GameController.FullBatType.HTCRacquet:
                Debug.Log("enter racquet");
                StopAttemptingBatTipCalibration();
                break;
            case GameController.FullBatType.TrackerAtTip:
                Debug.Log("enter tip.");
                //StartAttemptingBatTipCalibration();
                break;
        }
    }

    public void ExitGame()
    {
        UnityEngine.Application.Quit();
    }

}
