﻿using UnityEngine;
using System.Collections;

public class soundOnShatter : MonoBehaviour {

	public randomSoundEffect soundSource;

	private ShatterToolkit.ShatterTool shatterTool;

	public void PostSplit(){
		if (soundSource != null) {
			soundSource.playClipAt (transform.position);
		}
	}

	// Use this for initialization
	void Awake () {
		shatterTool = GetComponent<ShatterToolkit.ShatterTool> ();
		if (shatterTool && shatterTool.IsLastGeneration) {
			soundSource = null;
		}
	}
	
	// Update is called once per frame
	//void Update () {
	
	//}
}
