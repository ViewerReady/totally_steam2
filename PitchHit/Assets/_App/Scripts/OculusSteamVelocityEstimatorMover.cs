﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusSteamVelocityEstimatorMover : MonoBehaviour {

    public Vector3 oculusInSteamPosition;
    void Awake()
    {
        if (GameController.isOculusInSteam)
        {
            transform.localPosition = oculusInSteamPosition;
        }
    }
}
