﻿using UnityEngine;
//using Valve.VR;

//using Valve.VR.InteractionSystem;
using System.Collections;

public class BallShooter : MonoBehaviour {
    private GameObject ballShootPrefab;

	//public OffenseCoordinator offense;
	//public DefenseCoordinator defense;
    public Vector2 shootSpeedRange;
    public bool editorOnly = true;
    private bool readyToAutoFire = false;
    private float countDown = 2.5f;
    public float autoFireRate = 1;

    private hitThings bat;
    public float horizontalSpread;
    public float verticalSpread;

	private Vector3 m_lastLaunch;

	private Vector3 lastLaunch{
		get{
			if (m_lastLaunch == Vector3.zero) {
				m_lastLaunch = new Vector3 (
					PlayerPrefs.GetFloat ("savedLaunch_x"),
					PlayerPrefs.GetFloat ("savedLaunch_y"),
					PlayerPrefs.GetFloat ("savedLaunch_z")
				);
			}
			return m_lastLaunch;
		}
		set{
			m_lastLaunch = value;
			PlayerPrefs.SetFloat ("savedLaunch_x", value.x);
			PlayerPrefs.SetFloat ("savedLaunch_y", value.y);
			PlayerPrefs.SetFloat ("savedLaunch_z", value.z);
		}
	}

    public bool forceVelocity = false;
    public Vector3 forceLaunchVelocity;
	//public WorldScaler worldScale;

    private bool repeatLastLaunch
    {
        get
        {
            return PlayerPrefs.GetInt("repeatSavedLaunch") == 1;
        }
        set
        {
            PlayerPrefs.SetInt("repeatSavedLaunch", value ? 1 : 0);
        }
    }


    public bool autoShoot = false;

    // Use this for initialization
    void Start () {
        ballShootPrefab = FindObjectOfType<ballController>().ballPrefab;
        bat = GetComponent<hitThings>();
    }

    void FixedUpdate()
    {
        if(!Application.isEditor && editorOnly)
        {
            return;
        }
        if (!readyToAutoFire)
        {
            if (countDown <= 0f)
			{
                countDown = autoFireRate;
                readyToAutoFire = true;
            }
            else
            {
                countDown -= Time.fixedDeltaTime;
            }
            return;
        }


#if RIFT
        if(OVRInput.GetDown(OVRInput.Button.Two,OVRInput.Controller.All))
        {
            ShootBall();
            //readyToShoot = false;
        }
#endif

		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			repeatLastLaunch = !repeatLastLaunch;
			Debug.Log ("Repeat Launch " + repeatLastLaunch + " " + lastLaunch);

		}



        else if (autoShoot && readyToAutoFire) {
            //if (FieldManager.Instance && FieldManager.Instance.ReadyForNewBall())
            //{
                ShootBall();
                readyToAutoFire = false;
           // }

		}
        else {
        }
    }

	public void ShootBall()
    {
        if(ballShootPrefab==null)
        {
            return;
        }

        Debug.Log("shoooooooooooooooooooooooooooooooooot ball now.");
		

		GameObject temp = Instantiate (ballShootPrefab, this.transform, false) as GameObject;
        temp.transform.localPosition = Vector3.forward;
        temp.transform.SetParent(this.transform.parent);
        if (forceVelocity)
        {
            lastLaunch = forceLaunchVelocity;
        }
		else if (!repeatLastLaunch) {
			Vector3 spread = new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), 0.0f);
			spread.y *= horizontalSpread;
			spread.x *= verticalSpread;
			Quaternion rotation = Quaternion.Euler (spread) * transform.rotation;

			temp.transform.rotation =(rotation);
			lastLaunch = Random.Range (shootSpeedRange.x, shootSpeedRange.y) * temp.transform.forward;
		}

        temp.GetComponent<Rigidbody>().velocity = lastLaunch;// * Mathf.Sqrt(WorldScaler.worldScale);
			
		

        if (bat)
        {
            bat.ForceBatHit(temp.GetComponent<hittable>());
        }
        else
        {
            temp.GetComponent<hittable>()._onBatHit(temp.transform.position);
        }

        temp.GetComponent<SphereCollider>().enabled = true;
    }

    private void ShootBallTargeted(Vector3 target, float airTime)
    {
        GameObject temp = Instantiate(ballShootPrefab, transform.position + (transform.forward * .9f), Quaternion.identity) as GameObject;
        temp.GetComponent<Rigidbody>().velocity = Trajectory.InitialVelocityNeededToHitPoint(temp.GetComponent<Rigidbody>(), target, airTime);
    }
}
