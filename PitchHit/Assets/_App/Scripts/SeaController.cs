﻿using UnityEngine;
using System.Collections;

public class SeaController : MonoBehaviour {
    private Material seaMaterial;
    public bool makeOqaque = false;
    // Use this for initialization
	void Start () {
        seaMaterial = GetComponent<Renderer>().material;

	    if(makeOqaque)
        {
            SetBaseColorAlpha(1f);
        }
	}
	
	// Update is called once per frame
	//void Update () {
	
	//}

    public void SetBaseColorAlpha(float a)
    {
        Color oldColor = seaMaterial.GetColor("_BaseColor");
        seaMaterial.SetColor("_BaseColor", new Color(oldColor.r, oldColor.g, oldColor.b, a));
    }

    public void SetBaseColor(Color c)
    {
        seaMaterial.SetColor("Base color", c);
    }

    public void SetReflectionColor(Color c)
    {
        seaMaterial.SetColor("Reflection color", c);
    }

    public void SetSpecularColor(Color c)
    {
        seaMaterial.SetColor("Specular color", c);
    }

    public void SetWaveLength(float f)
    {
        seaMaterial.SetFloat("Wave Length", f);
    }

    public void SetIntensity(float f)
    {
        seaMaterial.SetFloat("Intensity", f);
    }

    public void SetPeriode(float f)
    {
        seaMaterial.SetFloat("Periode", f);
    }
    
}
