﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Valve.VR.InteractionSystem;
using Valve.VR;

#if RIFT
using OVRTouchSample;
#elif STEAM_VR
using Valve.VR;
#endif

public class CustomHand : MonoBehaviour
{
    SteamVR_Action_Boolean turnRightAction;
    SteamVR_Action_Boolean turnLeftAction;
    SteamVR_Action_Boolean gripPressAction;
    SteamVR_Action_Boolean grabObjectAction;
    SteamVR_Action_Boolean runButtonAction;
    SteamVR_Action_Boolean reverseButtonAction;
    SteamVR_Action_Boolean menuButtonAction;
    SteamVR_Action_Single triggerDepthAction;
    SteamVR_Action_Vibration hapticsAction;

    public enum HandState { None, Empty, Ball, LargeBall, Pointing };
    public HandState currentHandState;

    private Renderer debugSkin;

    [HideInInspector]
    public bool renderModelsLoaded;

    public bool isOccupied;
    public bool isOculusRight;
    public Text leftDisplayText;
    public Text rightDisplayText;
    public CustomHand otherHand;
    public GameObject compass;
    public Animator handAnimator;
    private SteamVR_Input_Sources _handType = SteamVR_Input_Sources.Any;
    //public Transform grippedBallLocation;
    [HideInInspector]
    public bool joystickPressed = false;
    public bool previousJoystickPressed = false;

    public bool showRenderModel
    {
        get
        { return _showRenderModel; }
        set
        {
            //Debug.Log(this.name + " set render model "+ value);
            _showRenderModel = value;

            if (GameController.mixedRealityMode)
            {
                value = false;
            }
#if RIFT
            if (oculusRenderModel)
            {
                oculusRenderModel.SetActive(value);
                touchAnimator.enabled = value;

            }
#elif STEAM_VR
            ///////////////////////////////////////////////////////////////////////////////////////
            /*if (modelSpawner && !renderModel)
            {
                renderModel = modelSpawner.GetRenderModels()[0];
            }*/
            if (renderModel)
            {
                //renderModel.OnHideRenderModels(!value);
                renderModel.gameObject.SetActive(value);
            }
#endif
        }
    }
    [SerializeField]
    private bool _showRenderModel = true;

    SteamVR_Events.Action renderModelLoadedAction
    {
        get
        {
            if (_renderModelLoadedAction==null)
            {
                _renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(OnRenderModelLoaded);
            }
            return _renderModelLoadedAction;
        }
        set
        {
            if (_renderModelLoadedAction == null)
            {
                _renderModelLoadedAction = value;
            }
        }
    }

    private SteamVR_Events.Action _renderModelLoadedAction;

    //private bool controllerLoaded;
    private GameObject oculusRenderModel;
    private bool turnAroundHintShowing;
    private bool gripBatHintShowing;
    private bool spawnBallHintShowing;
    private bool grabHintShowing;
    private bool returnToBatHintShowing;
    private bool menuButtonHintShowing;

    private OVRGrabber grabber;

    [SerializeField] Transform offsetTransform;
    [SerializeField] Transform indexOffset;
    [SerializeField] Transform oculusSVROffset;

    public float GripValue
    {
        get; private set;
    }

#if RIFT
    private NewCustomTouchAnimator touchAnimator;
    [HideInInspector]

    //  public TrackedController trackedOculusTrackedController;
#elif GEAR_VR
    [HideInInspector]
    public Vector3 initialLocalPosition;
    //private Vector2 previousTouchPosition;
    private float timeSpentTouching;
    private float touchingTimeUntilSlide = 0;
    private bool sliding;
    
    public Transform teeForPivot;
#elif STEAM_VR
    [HideInInspector]
    public Hand actualHand;
    //private ControllerButtonHints steamVRButtonHints;
    //[HideInInspector]
    //public SteamVR_RenderModel renderModel;
    /////////////////////////////////////////////////////////////////////////////////////////////////public SpawnRenderModel modelSpawner;
    protected SteamVR_RenderModel renderModel;

#endif

    void Awake()
    {
#if STEAM_VR
        turnLeftAction = SteamVR_Actions.baseballSet_TurnLeft;
        turnRightAction = SteamVR_Actions.baseballSet_TurnRight;
        gripPressAction = SteamVR_Actions.baseballSet_GripPressed;
        grabObjectAction = SteamVR_Actions.baseballSet_GrabObject;
        runButtonAction = SteamVR_Actions.baseballSet_RunButton;
        reverseButtonAction = SteamVR_Actions.baseballSet_ReverseButton;
        menuButtonAction = SteamVR_Actions.baseballSet_MenuButton;
        triggerDepthAction = SteamVR_Actions.baseballSet_TriggerDepth;
        hapticsAction = SteamVR_Actions.baseballSet_Haptic;
        actualHand = GetComponent<Hand>();
        Debug.Log("actual hand " + actualHand + " found for " + name);
        renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(OnRenderModelLoaded);
        //showRenderModel = true;
#endif


    }

    void Start()
    {
        if (compass)
        {
            compass.SetActive(false);
        }
        debugSkin = GetComponentInChildren<Renderer>();

#if RIFT
       // trackedOculusTrackedController = GetComponent<TrackedController>();
        touchAnimator = GetComponentInChildren<NewCustomTouchAnimator>();
        renderModelsLoaded = touchAnimator;

        if (isOculusRight) {
            oculusRenderModel = GameObject.Find("right_touch_controller_model_skel");
        }
        else
        {
            oculusRenderModel = GameObject.Find("left_touch_controller_model_skel");
        }

        showRenderModel = false;
        grabber = GetComponentInChildren<OVRGrabber>();

        SetHandState(HandState.Empty);
#elif GEAR_VR
        if(hitThings.pivotAroundTee)
        {
            if(!teeForPivot)
            {
                if (ballController.instance && ballController.instance.teeController)
                {
                    teeForPivot = ballController.instance.teeController.transform;
                }
            }

        }
        initialLocalPosition = transform.localPosition;
#endif

#if STEAM_VR
        var system = OpenVR.System;
        //SteamVR_TrackedObject.EIndex index = SteamVR_TrackedObject.EIndex.Device3;
        var error = ETrackedPropertyError.TrackedProp_Success;
        for (int index = 0; index < 6; index++)
        {
            var capacity = system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, null, 0, ref error);
            if (capacity <= 1)
            {
                //Debug.LogError("<b>[SteamVR]</b> Failed to get render model name for tracked object " + index);
                //return;
            }
            var buffer = new System.Text.StringBuilder((int)capacity);
            system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, buffer, capacity, ref error);
            string s = buffer.ToString();
            //Debug.Log("HAND DEVICE IS: " + s);
            if (s.Contains("indexcontroller"))
            {
                //Debug.Log("Giving the mit the index offset");
                offsetTransform.localPosition = indexOffset.localPosition;
                offsetTransform.localRotation = indexOffset.localRotation;
                offsetTransform.localScale = indexOffset.localScale;
                break;
            }
            else if (s.Contains("oculus"))
            {
                offsetTransform.localPosition = oculusSVROffset.localPosition;
                offsetTransform.localRotation = oculusSVROffset.localRotation;
                offsetTransform.localScale = oculusSVROffset.localScale;
                break;
            }
        }
#endif

#if STEAM_VR || RIFT
        
#endif
    }
#if STEAM_VR
    void OnEnable()
    {
        renderModelLoadedAction.enabled = true;
    }

    void OnDisable()
    {
        renderModelLoadedAction.enabled = false;
    }
#endif

    private bool previousGrabbingOn;
    private bool grabbingOn;

    private bool grabAvailable; //MARC: needed as a way to not trigger grab the instant isOccupied is set to false if trigger is down

    private bool previousSqueezingOn;
    private bool squeezingOn;

    private bool previousUsingOn;
    private bool usingOn;

    private bool previousTouchpadOn;
    private bool touchpadOn;
    private bool finishedTurning = false;
    private Vector2 touchPosition;
    private Vector2 previousTouchPosition;

    private float gripStrength;
    private float indexGripStrength;
    private float deepestCurrentGripStrength;
    private float minimumGripStrength = .5f;
    private float gripStrengthMargin = .001f;
    private bool grabIsCancelled;
    //[HideInInspector]
    public bool gripWithWholeHand = true;

    // Update is called once per frame
    void Update()
    {

#if STEAM_VR || RIFT
        
#endif

#if RIFT
       
        if(isOculusRight)
        {
            bool connected = OVRInput.IsControllerConnected(OVRInput.Controller.RTouch);
            bool tracking = OVRInput.GetControllerPositionTracked(OVRInput.Controller.RTouch);
            Vector3 position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
//            Debug.Log("OVR RTouch " + "connected: " + connected + " tracking: " + tracking + " position: " + position);
            transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

            //if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
            ///{
            //    OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            //}
            
        }
        else
        {
            //Debug.Log(OVRInput.GetConnectedControllers().ToString());
            transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

            //if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.LTouch))
            //{
            //    OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            //}
        }

        if (GameController.canTurnAround)
        {
            if (touchPosition.x > .8f && previousTouchPosition.x <= .8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, 90f);
            }
            else
            if (touchPosition.x < -.8f && previousTouchPosition.x >= -.8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, -90f);
            }
            else
            if (touchPosition.y < -.8f && previousTouchPosition.y >= -.8f)
            {
                OVRManager.instance.transform.Rotate(Vector3.up, 180f);
            }
        }

#elif STEAM_VR
        if (GameController.isOculusInSteam)
        {
            if (actualHand != null)
            {
                if (GameController.canTurnAround)
                {
                    if (finishedTurning)
                    {
                        if (Mathf.Abs(touchPosition.x) < .5f && Mathf.Abs(touchPosition.y) < .5f)
                        {
                            finishedTurning = false;
                        }
                    }
                    else
                    {

                        if (turnRightAction.GetLastStateDown(actualHand.handType))
                        {
                            Player.instance.transform.Rotate(Vector3.up, 90f);
                            finishedTurning = true;
                        }
                        else if (turnLeftAction.GetLastStateDown(actualHand.handType))
                        {
                            Player.instance.transform.Rotate(Vector3.up, -90f);
                            finishedTurning = true;
                        }
                    }
                }
            }
            else
            {
                //Debug.Log("no actual hand in "+name);
            }
        }

#endif

        previousGrabbingOn = grabbingOn;
        previousSqueezingOn = squeezingOn;
        previousTouchpadOn = touchpadOn;
        previousUsingOn = usingOn;

#if RIFT

        previousJoystickPressed = joystickPressed;

        if (isOculusRight)
        {
            squeezingOn  = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch);
            if (gripWithWholeHand)
            {
                gripStrength = Mathf.Max(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch), OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch));
            }
            else
            {
                gripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
            }
            joystickPressed = OVRInput.Get(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.RTouch);
            indexGripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);

            //Debug.Log("right oculus grip:"+gripStrength +" squeeze:"+squeezingOn);
        }
        else
        {
            squeezingOn = OVRInput.Get(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch);
            if (gripWithWholeHand)
            {
                gripStrength = Mathf.Max(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch), OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch));
            }
            else
            {
                gripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
            }
            joystickPressed = OVRInput.Get(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch);
            indexGripStrength = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
        }

        touchpadOn = joystickPressed;
        previousTouchPosition = touchPosition;
        touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, isOculusRight? OVRInput.Controller.RTouch: OVRInput.Controller.LTouch);
        //squeezingOn = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0; ;


#elif GEAR_VR
        if (Application.isEditor || (OVRInput.GetConnectedControllers() & OVRInput.Controller.RTrackedRemote) != 0)
        {
            if (GameController.isAtBat && !GameController.isMenuPointing)
            {
                if (hitThings.pivotAroundTee)
                {
                    if (hitThings.doublePivot)
                    {
                        squeezingOn = Input.GetKey("k") || OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote);
                    }
                    if (hitThings.doublePivot && squeezingOn)
                    {
                        if (debugSkin && squeezingOn)
                        {
                            debugSkin.material.color *= .5f;
                        }
                        if (Application.isEditor)
                        {
                            transform.localRotation = Quaternion.AngleAxis(20f * Time.deltaTime, Vector3.up) * transform.localRotation;
                        }
                        else
                        {
                            transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                        }
                        if (!squeezingOn)
                        {
                            transform.position = teeForPivot.position - (transform.forward * .7f);
                        }

                    }
                    else
                    {
                        //this is where the pivoting around the tee happens.
                        transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                        transform.position = teeForPivot.position - (transform.forward * .7f);

                        if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote))
                        {
                            squeezingOn = true;
                            Vector2 touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);
                            transform.Rotate(Vector3.up, touchPosition.x * 30f);
                        }
                    }
                }
                else
                {
                    if (OVRInput.GetDown(OVRInput.Button.Back, OVRInput.Controller.RTrackedRemote))
                    {
                        transform.localPosition = initialLocalPosition;
                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.cyan;
                        }
                    }
                    else
                    {
                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.green;
                        }
                    }
                    transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                    //print("BOO " + OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote));
                    if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote))
                    {
                        if (!sliding && timeSpentTouching >= 0)
                        {
                            timeSpentTouching -= Time.deltaTime;
                        }
                        else
                        {
                            if (!sliding)
                            {
                                if (compass)
                                {
                                    compass.SetActive(true);
                                }
                                sliding = true;
                                previousTouchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);
                            }
                            else
                            {
                                Vector2 touchPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

                                transform.position += transform.forward * (touchPosition.y - previousTouchPosition.y) * .25f;
                                transform.position += transform.right * (touchPosition.x - previousTouchPosition.x) * .25f;

                                previousTouchPosition = touchPosition;
                            }
                        }

                        if (debugSkin)
                        {
                            debugSkin.material.color = Color.blue;
                        }
                    }
                    else
                    {
                        if (compass)
                        {
                            compass.SetActive(false);
                        }
                        sliding = false;
                        timeSpentTouching = touchingTimeUntilSlide;
                    }

                    squeezingOn = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTrackedRemote);
                    if (debugSkin && squeezingOn)
                    {
                        debugSkin.material.color *= .5f;
                    }
                }
            }
            else
            {
                transform.localPosition = (Vector3.forward+Vector3.up).normalized*.5f;
                squeezingOn = true;
                if (!Application.isEditor)
                {
                    transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
                }
                else
                {
                    transform.localRotation = Quaternion.AngleAxis(Input.GetAxis("Mouse X"), Vector3.up) * transform.localRotation;
                    transform.localRotation = Quaternion.AngleAxis(Input.GetAxis("Mouse Y"), Vector3.Cross(Vector3.up,transform.forward)) * transform.localRotation;
                }
            }
            
        }
        else
        {
            if (debugSkin)
            {
                debugSkin.material.color = Color.black;
            }
        }
       
        grabbingOn = false;
#elif STEAM_VR
        float squeeze = 0;
        if (actualHand != null)
        {
            /*touchpadOn = actualHand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
            gripStrength = actualHand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
            indexGripStrength = actualHand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
            squeezingOn = actualHand.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_Grip);*/

            touchpadOn = reverseButtonAction.GetLastState(actualHand.handType);
            gripStrength = triggerDepthAction.GetAxis(actualHand.handType);
            indexGripStrength = triggerDepthAction.GetAxis(actualHand.handType);
            squeezingOn = gripPressAction.GetLastState(actualHand.handType);

            if (GameController.isOculusInSteam)
            {
                squeeze = gripPressAction.GetLastState(actualHand.handType) ? 1f : 0f;
            }
            else
            {
                squeeze = gripPressAction.GetLastState(actualHand.handType) ? 1f : 0f;
            }

        }
        if (squeeze >= gripStrength) SetHandGripValue(squeeze, true);
        else SetHandGripValue(gripStrength, false);
#endif

#if STEAM_VR || RIFT


        // if (gripStrength > 0) grabbingOn = true;
        //  else grabbingOn = false;

        bool grabbingDeeper = false;
        if (gripStrength > deepestCurrentGripStrength + gripStrengthMargin)
        {
            grabbingDeeper = true;
            deepestCurrentGripStrength = gripStrength - gripStrengthMargin;
        }

        if (grabbingOn || grabIsCancelled)
        {
            if (gripStrength < deepestCurrentGripStrength - gripStrengthMargin)
            {
                grabIsCancelled = false;
                grabbingOn = false;
            }
        }
        else
        {
            if (!grabIsCancelled && gripStrength > minimumGripStrength && grabbingDeeper)
            {
                grabbingOn = true;
            }
        }

        if (gripStrength < minimumGripStrength)
        {
            deepestCurrentGripStrength = minimumGripStrength;


        }
        usingOn = (indexGripStrength > .5);

        //MARC: Noticed that setting isOccupied to false would immediately trigger GrabDown(). Ideal behavior is to wait until grips are released, then make grabbing possible again
        if (isOccupied) //hand is occupied, grab is not available
        {
            grabAvailable = false;
        }
        else if (!grabbingOn) //hand is not occupied and not grabbing, grab is available
        {
            grabAvailable = true;
        }

        grabbingOn = grabbingOn && grabAvailable;

#if RIFT

        if (grabber.grabbedObject == null)
        {
            
            if (grabber)
            {
                if(squeezingOn) SetHandGripValue(gripStrength / minimumGripStrength, true);
                else SetHandGripValue(gripStrength / minimumGripStrength, false);
            }

        }
#endif


        //Debug.Log(triggerIsPressed);

#endif
    }

    public Vector3 GetCurrentBatGripPosition()
    {
#if RIFT
        if(!grabber)
        {
            grabber = GetComponentInChildren<OVRGrabber>();
        }
        if(grabber)
        {
            return grabber.transform.position;
        }
#endif
        return transform.position;
    }

    public void StartPointing()
    {
        if (!grabber)
        {
            grabber = GetComponentInChildren<OVRGrabber>();
        }
        if (GetCurrentGrabbable() == null)
        {
            SetHandState(HandState.Pointing);
        }
    }

    public void StopPointing()
    {
        if (currentHandState != HandState.Pointing)
        {
            return;
        }

        //if (hitThings.instance && hitThings.instance.attached && hitThings.instance.attachedCustomHand == this)
        if (isOccupied)
        {
            SetHandState(HandState.None);
        }
        else
        {
            OVRGrabbable grabbed = GetCurrentGrabbable();
            if (grabbed)
            {
                if (grabbed.GetComponent<BallForDucks>())
                {
                    SetHandState(HandState.LargeBall);
                }
                else
                {
                    SetHandState(HandState.Ball);
                }
            }
            else
            {
                SetHandState(HandState.Empty);
            }
        }

    }

    public bool GetGrip()
    {
        return grabbingOn;
    }

    public bool GetGripDown()
    {
        //Debug.Log(name+" get grip down? "+grabbingOn+" "+previousGrabbingOn);
        return grabbingOn && !previousGrabbingOn;
    }


    public bool GetGripUp()
    {
        //return false;
        return !grabbingOn && previousGrabbingOn;
    }

    public bool GetSqueezeDown()
    {
        return squeezingOn && !previousSqueezingOn;
    }

    public bool GetSqueezeUp()
    {
        return !squeezingOn && previousSqueezingOn;
    }
    public bool GetSqueezing()
    {
        return squeezingOn;
    }


    public bool GetTouchpadDown()
    {
        return touchpadOn && !previousTouchpadOn;
    }

    public bool GetTouchpadUp()
    {
        return !touchpadOn && previousTouchpadOn;
    }
    public bool GetTouchpad()
    {
        return touchpadOn;
    }

    public bool GetUseDown()
    {
        return usingOn && !previousUsingOn;
    }

    public bool GetUseUp()
    {
        return !usingOn && previousUsingOn;
    }

    public bool GetUsing()
    {
        return usingOn;
    }

    public Vector2 GetTouchPosition()
    {
        return touchPosition;
    }

    public bool GetButtonOne()
    {
#if STEAM_VR
        return actualHand != null && reverseButtonAction.GetLastStateDown(actualHand.handType);
#elif RIFT
        //if (isOculusRight) Debug.Log("OVR+++++++++++++++++++++++++++++++++++++++++++++ "+ OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch));
        if (isOculusRight) return OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch);
        return OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.LTouch);
#elif GEAR_VR
        return false;
#endif
    }

    public bool GetMenuButtonDown()
    {
#if STEAM_VR
        return actualHand != null && menuButtonAction.GetLastStateDown(actualHand.handType);
#elif RIFT
        return OVRInput.GetDown(OVRInput.Button.Start,OVRInput.Controller.LTouch);
#elif GEAR_VR
        return false;
#else
        return false;
#endif
    }



   

#if RIFT || STEAM_VR
    public Transform grippedBallLocation;
    private float handFlickPushFactor = 250f;
    private float handFlickSpinFactor = 200f;
    private float maxGripSlide = .3f;
    private float gripRange = .75f;
    private float minimumHandFlick = 5f;
    private VelocityEstimator velocity;

    
#endif
    public void DetachObject(GameObject obj)
    {
#if RIFT

#elif STEAMVR

#endif
    }

    public void HoverLock(Interactable interact)
    {
#if STEAM_VR
        if (actualHand)
        {
            actualHand.HoverLock(interact);
        }
#endif
    }

    public void HoverUnlock(Interactable interact)
    {
#if STEAM_VR
        if (actualHand)
        {
            actualHand.HoverUnlock(interact);
        }
#endif
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void OnRenderModelLoaded(SteamVR_RenderModel renderModel, bool success)
    {
        Debug.Log("++++++++++++++++++++++++++++++++ OnRenderModelLoaded. " + name + " rm" + renderModel.name + " success: " + success);

        if (!renderModelsLoaded)
        {
            StartCoroutine(AssignRenderModel());

            renderModelsLoaded = success;
        }

    }

    IEnumerator AssignRenderModel()
    {
        yield return null;
#if STEAM_VR
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /* modelSpawner = GetComponentInChildren<SpawnRenderModel>();
         //Debug.Log("--------------------------------rendermodel spawner "+modelSpawner);

         if (modelSpawner)
         {
             //steamVRButtonHints = GetComponentInChildren<ControllerButtonHints>();

             //renderModel = renderModelSpawner.GetRenderModels()[0];
             //modelSpawner = renderModelSpawner.gameObject;
             //Debug.Log("found render model " + modelSpawner.name);

         }
         else
         {
             //Debug.Log("didn't find one in  " + name);
         }


         if (GameController.mixedRealityMode)
         {
             showRenderModel = showRenderModel;
         }*/

        //Debug.Log("///////////////////////////////////////////////////////////////////////CUSTOM HAND IS AWAKE " + GameController.mixedRealityMode + " " + showRenderModel);
#endif

    }

    public void SetGripBatHint(bool on)
    {
        gripBatHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            Debug.Log("oooooooooooooooooooooooooooooooooooooooooooooo activate the touch animator");
           touchAnimator.SetButtonGripHighlight(on);

            if(grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
        else
        {
            Debug.Log("SORRY NO TOUCH ANIMATOR!");
        }
#elif STEAM_VR
        if (on)
        {
            //ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), EVRButtonId.k_EButton_SteamVR_Trigger);
            Debug.Log("turn grip hint on. " + actualHand.name);
            ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), gripPressAction);
        }
        else
        {
            Debug.Log("Turn Grip hint offfffff.");
            ControllerButtonHints.HideButtonHint(GetComponent<Hand>(), gripPressAction);
        }
#endif

        RefreshHandState();
    }

    public void SetRunHint(bool on)
    {
        gripBatHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            //Debug.Log("oooooooooooooooooooooooooooooooooooooooooooooo activate the touch animator");
           touchAnimator.SetButtonGripHighlight(on);

            if(grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
        else
        {
            Debug.Log("SORRY NO TOUCH ANIMATOR!");
        }
#elif STEAM_VR
        if (on)
        {
            //ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), EVRButtonId.k_EButton_SteamVR_Trigger);
            Debug.Log("turn grip hint on. " + actualHand.name);
            ControllerButtonHints.ShowButtonHint(GetComponent<Hand>(), runButtonAction);
        }
        else
        {
            Debug.Log("Turn Grip hint offfffff.");
            ControllerButtonHints.HideButtonHint(GetComponent<Hand>(), runButtonAction);
        }
#endif

        RefreshHandState();
    }

    public void SetReverseHint(bool on)
    {
#if RIFT
        if (touchAnimator)
        {
            touchAnimator.SetButtonOneHighlight(on);
            if (grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (actualHand == null)
        {
            return;
        }

        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, reverseButtonAction);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, reverseButtonAction);
            }

        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, reverseButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, reverseButtonAction);
            }

        }
#endif
    }

    public void SetBallSpawnHint(bool on)
    {
        spawnBallHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            touchAnimator.SetButtonOneHighlight(on);
            if (grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (actualHand == null)
        {
            return;
        }

        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, reverseButtonAction);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, reverseButtonAction);
            }

        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, reverseButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, reverseButtonAction);
            }

        }
#endif
    }

    public void SetGrabHint(bool on)
    {
        grabHintShowing = on;
#if RIFT
        if(touchAnimator)
        {
            touchAnimator.SetButtonTriggerHighlight(on);
            if (grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }

        }
#elif STEAM_VR
        if (on)
        {
            ControllerButtonHints.ShowButtonHint(actualHand, grabObjectAction);
        }
        else
        {
            ControllerButtonHints.HideButtonHint(actualHand, grabObjectAction);
        }
#endif
    }

    public void SetReturnToBatHint(bool on)
    {
        returnToBatHintShowing = on;
#if RIFT
        if (touchAnimator)
        {
            touchAnimator.SetJosystickHighlight(on);
            if (grabber)
            {
                //SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }

        }
#endif
    }

    public void SetMenuButtonHint(bool on)
    {

#if RIFT
        if(isOculusRight)
        {
            return;
        }
        menuButtonHintShowing = on;
        if (touchAnimator)
        {
            touchAnimator.SetButtonThreeHighlight(on);
            if (grabber)
            {
                //SetHandState(HandState.None);
                if (on)
                {
                    showRenderModel = true;
                }
            }
        }
#elif STEAM_VR
        menuButtonHintShowing = on;
        if (on)
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
        }
        else
        {
            if (GameController.isOculusInSteam)
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }

        }
        if (GameController.isOculusInSteam && isOculusRight)
        {
            return;
        }
#endif
    }

    public void SetTurnAroundHint(bool on)
    {
        turnAroundHintShowing = on;
#if RIFT
        if(touchAnimator)
        {
            touchAnimator.SetJosystickHighlight(on);
            //touchAnimator.SetButtonTwoHighlight(on);
            if (grabber)
            {
                SetHandState(HandState.None);
                showRenderModel = true;
            }
        }
#elif STEAM_VR
        if (GameController.isOculusInSteam)
        {
            if (on)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }
        }
        else
        {
            if (on)
            {
                ControllerButtonHints.ShowButtonHint(actualHand, menuButtonAction);
            }
            else
            {
                ControllerButtonHints.HideButtonHint(actualHand, menuButtonAction);
            }
        }
#endif
    }

    public void HideHints()
    {
#if STEAM_VR
        ControllerButtonHints.HideAllButtonHints(actualHand);
#endif
        turnAroundHintShowing = false;
        gripBatHintShowing = false;
        spawnBallHintShowing = false;
        grabHintShowing = false;
        returnToBatHintShowing = false;
        menuButtonHintShowing = false;
        RefreshHandState();
    }

    public void DisplayText(string words)
    {

        if (HandManager.currentLeftCustomHand == this)
        {
            leftDisplayText.gameObject.SetActive(true);
            leftDisplayText.text = words;
            if (rightDisplayText)
            {
                rightDisplayText.gameObject.SetActive(false);
            }
        }
        else if (HandManager.currentRightCustomHand == this)
        {
            rightDisplayText.gameObject.SetActive(true);
            rightDisplayText.text = words;
            if (leftDisplayText)
            {
                leftDisplayText.gameObject.SetActive(false);
            }
        }

    }

    public void DisableText()
    {

        if (leftDisplayText)
        {
            leftDisplayText.gameObject.SetActive(false);
        }

        if (rightDisplayText)
        {
            rightDisplayText.gameObject.SetActive(false);
        }

        //displayText.enabled = false;
        //displayText.gameObject.SetActive(false); 

    }

    public void RefreshHandState(bool forceShowRenderModel = false)
    {
        if (AnyHintsShowing())
        {

            showRenderModel = true;
            SetHandState(HandState.None);

            /*
            if (turnAroundHintShowing)
            {
                SetTurnAroundHint(true);
            }

            if(gripBatHintShowing)
            {
                SetGripBatHint(true);
            }

            if(spawnBallHintShowing)
            {
                SetBallSpawnHint(true);
            }

            if(grabHintShowing)
            {
                SetGrabHint(true);
            }

            if(returnToBatHintShowing)
            {
                SetReturnToBatHint(true);
            }

            if(menuButtonHintShowing)
            {
                SetMenuButtonHint(true);
            }
            */

            return;
        }

        if (forceShowRenderModel == false)
        {
            showRenderModel = false;
        }
        else
        {
            showRenderModel = true;
            SetHandState(HandState.None);
            return;
        }

        //if (grabber)
        {
            if (GetCurrentGrabbable())
            {
                if (GetCurrentGrabbable().GetComponent<BallForDucks>())
                {
                    SetHandState(HandState.LargeBall);
                }
                else
                {
                    SetHandState(HandState.Ball);
                }
            }
            else
            {


                //bool occupied = ballController.instance.glove && ballController.instance.glove.attached && ((ballController.instance.glove.isOnRightHand && isOculusRight) || (!ballController.instance.glove.isOnRightHand && !isOculusRight));
                //if (!occupied && (hitThings.instance == null || hitThings.instance.attached == false || hitThings.instance.attachedCustomHand != this))

                //Debug.Log("_____________________________________________________ REFRESHING HAND "+name+" is occupied? "+IsOccupied());
                if (!IsOccupied())
                {
                    SetHandState(HandState.Empty);
                }
                else
                {
                    SetHandState(HandState.None);
                }
            }
        }
    }

    public bool IsOccupied()
    {
        if (GetCurrentGrabbable())
        {
            return true;
        }

        if (HandManager.instance.glove && HandManager.instance.glove.GetAttachedHand() == this)
        {
            return true;
        }

        if (HandManager.instance.cannon && HandManager.instance.cannon.GetAttachedHand() == this)
        {
            return true;
        }

        if (HandManager.instance.bat && HandManager.instance.bat.GetAttachedHand() == this)
        {
            return true;
        }
        
        return false;
    }

    public OVRGrabbable GetCurrentGrabbable()
    {
#if RIFT
        if(grabber)
        {
            return grabber.grabbedObject;
        }
#elif STEAM_VR
        if (actualHand)
        {
            if (actualHand.currentAttachedObject)
                return actualHand.currentAttachedObject.GetComponent<OVRGrabbable>();
        }
#endif
        return null;
        //return m_grabbedObj;
    }

    public void MakeHandInvisible(bool permanent = false)
    {
        //Debug.Log("MAKE HAND INVISIBLE!!!!! ");
        //if (grabber)
        {
            //Debug.Log("and it has a grabber.");
            SetHandState(HandState.None);
            if (permanent && handAnimator)
            {
                Destroy(handAnimator.gameObject);
            }
        }
    }


    private bool AnyHintsShowing()
    {
        return turnAroundHintShowing || gripBatHintShowing || spawnBallHintShowing || grabHintShowing || returnToBatHintShowing || menuButtonHintShowing;
    }

    public void SetHandGripValue(float value, bool isSqueeze)
    {
        //isSqueeze specifies that we're using the grip buttons, rather than the trigger.

        GripValue = value;
        if (handAnimator && handAnimator.gameObject.activeInHierarchy)
        {
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.BROADCAST)
            {
                if (isSqueeze) handAnimator.SetFloat("GripBlend", value);
                else handAnimator.SetFloat("GripBlend", -value);
            }
            else handAnimator.SetFloat("GripBlend", value);
        }
        //        Debug.Log("Set grip val " + value);
    }

    public void SetHandType(SteamVR_Input_Sources type)
    {
        if (_handType == type) return;
        _handType = type;
        if (_handType == SteamVR_Input_Sources.LeftHand)
        {
            grippedBallLocation.localScale = new Vector3(-1, 1, 1);
            handAnimator.name = "LeftAnim";
        }
        if (_handType == SteamVR_Input_Sources.RightHand)
        {
            grippedBallLocation.localScale = Vector3.one;
            handAnimator.name = "RightAnim";
        }
    }

    public void SetHandState(HandState state)
    {

        //Debug.Log(name+" okay change hand state to " + state+"?? ");
        if (!handAnimator)
        {
            //Debug.Log("it's already invisible so nevermind.");
            return;
        }

        if (state == currentHandState && state != HandState.None)
        {
            //Debug.Log("it's already in that state..");
            return;
        }


        if (state != HandState.None)
        {
            if (currentHandState == HandState.None)
            {
                handAnimator.gameObject.SetActive(true);
            }

            if (handAnimator)
            {
                switch (state)
                {
                    case HandState.Empty:
                        handAnimator.SetTrigger("BecomeEmpty");
                        break;
                    case HandState.Ball:
                        handAnimator.SetTrigger("HoldBall");
                        break;
                    case HandState.LargeBall:
                        handAnimator.SetTrigger("HoldLargeBall");
                        break;
                    case HandState.Pointing:
                        handAnimator.SetTrigger("BeginPointing");
                        break;
                }
            }

            if (grabber)
            {
                grabber.enabled = true;
            }
        }
        else
        {
            if (handAnimator)
            {
                handAnimator.gameObject.SetActive(false);

            }

            if (grabber)
            {
                grabber.enabled = false;
            }
        }

        currentHandState = state;
    }

    private void OnHandInitialized(int deviceIndex)
    {

        // Debug.Log("_____________________________________________CUSTOM HAND ON HAND INITIALIZED " + deviceIndex + " show render model " + showRenderModel);
        RefreshHandState(true);

        //SetHandState(HandState.None);
        //showRenderModel = true;

        StartCoroutine(RefreshHandsAfterDelay(2f));
    }

    IEnumerator RefreshHandsAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        RefreshHandState();
    }

    public void ShowHand()
    {
        handAnimator.gameObject.SetActive(true);
    }

    public void HideHand()
    {
        handAnimator.gameObject.SetActive(false);
    }
}
