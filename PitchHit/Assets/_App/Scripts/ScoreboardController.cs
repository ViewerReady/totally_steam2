﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ScoreboardController : MonoBehaviour {
    public static ScoreboardController instance;
    public UnityEvent OnScoreAdded;
    public Text scoreboardText;
    public GameObject homeRunBoard;
    //public Text homeRunText;
    public Text statusText;
	public LeaderboardManager leaderboard;
    //public bool shouldScoreHomeRuns;
    public bool scorable;

    public GameObject[] fireworksPrefabs;
    public Transform[] fireworksSources;

    private string defaultStatus;
    public int myScore;
    //public int ballHitCount;
    void Awake()
    {
        instance = this;
        defaultStatus = statusText.text;
        myScore = 0;

    }

    public void HomeRun()
    {
        Debug.Log("homerun routine");


        StartCoroutine(HomeRunEnter());
    }

    public void TriggerFireworks()
    {
        //Debug.Log("NOW MAKE FIREWORKS FOR THE HOMERUN");
        if (fireworksSources != null && fireworksPrefabs != null && fireworksPrefabs.Length > 0)
        {
            foreach (Transform t in fireworksSources)
            {
                //Debug.Log("pop");
                Instantiate(fireworksPrefabs[Random.Range(0, fireworksPrefabs.Length)], t.position, t.rotation);
            }
        }

    }

    IEnumerator HomeRunEnter()
    {
        yield return new WaitForSeconds(1f);

        homeRunBoard.SetActive(true);
        TriggerFireworks();
        //if(shouldScoreHomeRuns)
        //{
        //    AddScore();

        //}
        yield return new WaitForSeconds(3f);

        homeRunBoard.gameObject.SetActive(false);
    }
    
    public void GoodBall()
    {
        statusText.text = defaultStatus;
    }

	public void FoulBall(bool inFence)
    {
		TrajTester1 currentTraj = FindObjectOfType<TrajTester1>();
		currentTraj.enabled = false;

		if (!inFence) {
			currentTraj.CancelScoring ();
		}
        StartCoroutine(FoulBallEnter(inFence));
    }

	IEnumerator FoulBallEnter(bool inFence)
    {
        // scoreText.SetActive(false);
        //  speedText.SetActive(false);

        statusText.text = "FoulBall";
        yield return new WaitForSeconds(3f);

        //  //scoreText.SetActive(true);
        ////  speedText.SetActive(true);
        GameObject.FindObjectOfType<TrajTester1>().enabled = true;
        statusText.text = defaultStatus;
    }

    public void DisableScoring()
    {
        Debug.Log("disable scoring");
        scorable = false;
    }
    public void EnableScoring()
    {
        Debug.Log("scoring enabled");
        scorable = true;
    }

    public void AddScore()
    {
        AddScore(1);
    }

    public void AddScore(int points)
    {
        Debug.Log("add score?");

        if (scorable)
        {
            Debug.Log("scorable");
            myScore+=points;

            UpdateScoreboard();

            OnScoreAdded.Invoke();
        }
    }

    public void SetScore(int newScore)
    {
        if(scorable)
        {
            myScore = newScore;
            UpdateScoreboard();

            OnScoreAdded.Invoke();
        }
    }

    public void IncrementBallHitCount()
    {
        //ballHitCount++;
    }

    public void ClearScore()
    {
        if (scorable)
        {
            if (leaderboard)
            {
                leaderboard.CommitScoreToLeaderboard(myScore);
            }
            myScore = 0;
        }
        UpdateScoreboard();
    }

    public virtual void UpdateScoreboard()
    {
        scoreboardText.text = myScore.ToString();
    }

    private void setScoreVisuals()
    {
        //Debug.Log ("updating score text");
        if (scoreboardText)
        {
            scoreboardText.text = myScore.ToString();
        }
    }

    public void FinalizeScore()
    {
        if (!leaderboard)
        {
            leaderboard = FindObjectOfType<LeaderboardManager>();
        }
        if (leaderboard)
        {
            leaderboard.CommitScoreToLeaderboard(myScore);
        }

        Debug.Log("finalize scoring");
        scorable = false;
        if (statusText)
        {
            statusText.text = "FINAL";
        }
    }

    public void ResetScoring()
    {
        EnableScoring();
        myScore = 0;
        //ballHitCount = 0;
    }
}
