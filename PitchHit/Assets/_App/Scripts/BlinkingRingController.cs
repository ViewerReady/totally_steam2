﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkingRingController : MonoBehaviour {
    public Renderer[] renderers;
    private Color originalColor;
	// Use this for initialization
	void Start () {
		if(renderers.Length>0)
        {
            originalColor = renderers[0].material.color;
        }
	}
	
    public void BlinkOnForTime(float duration, Color c)
    {
        Debug.Log("blink on for time");
        StopAllCoroutines();
        StartCoroutine(BlinkRoutine(duration, c));
    }

    IEnumerator BlinkRoutine(float duration, Color c)
    {
        Debug.Log("blink on for time routine");
        SetBlinkOn(c);
        yield return new WaitForSeconds(duration);
        Debug.Log("blink on for time and off");
        SetBlinkOff();
    }

    public void SetBlinkOn(Color c)
    {
        //StopAllCoroutines();
        foreach (Renderer r in renderers)
        {
            r.material.color = c;
        }
    }

    public void SetBlinkOff()
    {
        StopAllCoroutines();
        foreach (Renderer r in renderers)
        {
            r.material.color = originalColor;
        }
    }



}
