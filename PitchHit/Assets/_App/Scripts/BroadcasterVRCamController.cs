﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BroadcasterVRCamController : MonoBehaviour
{
    CinemachineVirtualCamera cineCam;
    Camera vrCam;

	void Awake()
    {
        cineCam = gameObject.GetComponent<CinemachineVirtualCamera>();
    }
    
    void Update()
    {
        if(cineCam != null)
        {
            if(vrCam == null)
            {
                vrCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            }
            else
            {
                //cineCam.m_Lens.FieldOfView = vrCam.fieldOfView;
                cineCam.transform.position = vrCam.transform.position;
                cineCam.transform.rotation = vrCam.transform.rotation;
            }
        }
    }
}
