﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXDemonstrator : MonoBehaviour {
    public GameObject visualEffectContainer;

    public bool useToggleTimer = true;
    public float toggleTimer =3f;

    public bool useToggleKey = false;
    public KeyCode toggleKey;

    private float countdown;
	
	// Update is called once per frame
	void Update () {
		if(useToggleTimer)
        {
            if(countdown < toggleTimer)
            {
                countdown += Time.deltaTime;
            }
            else
            {
                countdown = 0;
                ToggleNow();
            }
        }

        if(useToggleKey)
        {
            if(Input.GetKeyDown(toggleKey))
            {
                ToggleNow();
            }
        }
	}

    public void ToggleNow()
    {
        if(visualEffectContainer)
        {
            visualEffectContainer.SetActive(!visualEffectContainer.activeSelf);
        }
    }

}
