﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class CustomInteractable : MonoBehaviour {

    public bool tomatoPresence;

    protected virtual void OnAttachedToHand(Hand hand)
    {
        if (tomatoPresence)
        {
            CustomHand customHand = (hand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
            if (customHand != null) customHand.HideHand();
        }
    }

    protected virtual void OnDetachedFromHand(Hand hand)
    {
        if (tomatoPresence)
        {
            CustomHand customHand = (hand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
            if (customHand != null) customHand.ShowHand();
        }
    }
}
