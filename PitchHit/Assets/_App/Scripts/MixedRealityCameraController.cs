﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class MixedRealityCameraController : MonoBehaviour {
    public SteamVR_ExternalCamera exCam;
    public LayerMask cameraMask;
    public bool canCycleThrough;

    private bool hasAppliedCameraMask;
    public SteamVR_TrackedObject trackedObject;

    // Use this for initialization
    void Start () {
        StartCoroutine(CameraMaskRoutine());
	}

    IEnumerator CameraMaskRoutine()
    {
        if (exCam)
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            /*while (!hasAppliedCameraMask)
            {
                Camera c = exCam.GetCam();
                if(c)
                {
                    c.cullingMask = cameraMask;
                    hasAppliedCameraMask = true;
                }
                yield return null;
            }*/
        }
        yield return null;
    }
	
	// Update is called once per frame
	void Update () {
        if (canCycleThrough)
        {
            if (Input.GetKeyDown(KeyCode.Period))
            {
                CycleThrough(true);
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Comma))
                {
                    CycleThrough(false);
                }
            }
        }
    }

    public void CycleThrough(bool up)
    {
        var system = OpenVR.System;
        if (system == null)
        {
            return;
        }

        /*
        for (int i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++)
        {
            if (i == relativeTo || system.GetTrackedDeviceClass((uint)i) != deviceClass)
                continue;
        */
        if (trackedObject.index == SteamVR_TrackedObject.EIndex.None)
        {
            trackedObject.index = SteamVR_TrackedObject.EIndex.Device1;
            Debug.Log("__________________________________________________________x " + trackedObject.index);
            return;
        }


        int currentIndex = (int)trackedObject.index;
        Debug.Log("__________________________________________________________x " + currentIndex);
        if (up)
        {
            currentIndex++;
            if (system.GetTrackedDeviceClass((uint)currentIndex) == ETrackedDeviceClass.Invalid)
            {
                currentIndex = 0;
            }
        }
        else
        {
            currentIndex--;
            if (currentIndex < 0)
            {
                currentIndex = (int)OpenVR.k_unMaxTrackedDeviceCount;
            }
        }

        Debug.Log("__________________________________________________________xx " + currentIndex);
        trackedObject.index = (SteamVR_TrackedObject.EIndex)currentIndex;
        Debug.Log("__________________________________________________________cycled through " + trackedObject.index);
    }
}
