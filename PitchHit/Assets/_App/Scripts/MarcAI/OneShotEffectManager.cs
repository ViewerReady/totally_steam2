﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable] public class StringToObjectDictionary : SerializableDictionary<string, GameObject> { }


public class OneShotEffectManager : MonoBehaviour
{
    [System.Serializable]
    public struct Effect
    {
        public string name;
        public GameObject prefab;
    }
    private static OneShotEffectManager _instance;
    public static OneShotEffectManager Instance { get { return _instance; } }

    private static Dictionary<string, GameObject> effectDictionary_static
    {
        get
        {
            if(_effectDictionary_static == null) _effectDictionary_static = new Dictionary<string, GameObject>();
            return _effectDictionary_static;
        }
        set
        {
            _effectDictionary_static = value;
        }
    }
    private static Dictionary<string, GameObject> _effectDictionary_static;
    [SerializeField] private List<Effect> effectsList;

    private Dictionary<string, GameObject> effectsCache
    {
        get
        {
            if (_effectsCache == null) _effectsCache = new Dictionary<string, GameObject>();
            return _effectsCache;
        }
        set
        {
            _effectsCache = value;
        }
    }
    private Dictionary<string, GameObject> _effectsCache;

    private void Awake()
    {
        //This implementation isn't quite how we'd normally do a singleton, but we're 
        //not sure how this is going to behave between scenes/restarts yet.

        if (_instance != null && _instance != this)
        {
            Destroy(_instance);
        }
        _instance = this;
        AddEffectsToStaticDictionary();
    }



    void AddEffectsToStaticDictionary()
    {
        foreach(Effect effect in effectsList)
        {
            if (!effectDictionary_static.ContainsKey(effect.name))
            {
                effectDictionary_static.Add(effect.name, effect.prefab);
            }
        }
    }

    public static void SpawnEffect(string effectKey, Vector3 position, Quaternion orientation, float lifetime = -1, bool spectatorOnly = false)
    {
        if (effectDictionary_static == null) return;
        if (_instance.effectsCache == null) return;
        GameObject prefab;
        GameObject effect;

        if (_instance.effectsCache.TryGetValue(effectKey, out prefab))
        {
            //Debug.Log("Spawning cached effect: " + effectKey);
            effect = prefab;
            effect.transform.position = position;
            effect.transform.rotation = orientation;
            effect.SetActive(true);
            _instance.effectsCache.Remove(effectKey);
        }
        else if (effectDictionary_static.TryGetValue(effectKey, out prefab))
        {
            //Debug.Log("No cached effect: " + effectKey + ". Spawning brand new one.");
            effect = GameObject.Instantiate(prefab, position, orientation);    
        }
        else
        {
            Debug.Log("Could not spawn effect: " + effectKey);
            return;
        }

        if (spectatorOnly)
        {
            GameController.SetLayerRecursively(effect, LayerMask.NameToLayer("SpectatorOnly"));
        }
        if (lifetime > 0)
        {
            _instance.StartCoroutine(_instance.CacheAfterLifetime(effect, effectKey, lifetime));
        }
    }

    IEnumerator CacheAfterLifetime(GameObject effect, string effectKey, float lifetime)
    {
        yield return new WaitForSeconds(lifetime);

        GameObject prefab;
        if (effectsCache.TryGetValue(effectKey, out prefab))    //If there's already a cached effect, we can just destroy this one and return;
        {
            //Debug.Log("Lifetime over. Already have cached effect: " + effectKey + ". Just destroying this one.");
            Destroy(effect);
            yield break;
        }
        else
        {
            //Debug.Log("Lifetime over for: " + effectKey + ". Cacheing.");
            effect.SetActive(false);
            effectsCache.Add(effectKey, effect);
        }
    }
}