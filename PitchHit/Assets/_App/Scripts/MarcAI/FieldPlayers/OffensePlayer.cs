﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;

public class OffensePlayer : AIFieldPlayer
{

    public Action<int, OffensePlayer> OnBaseReached;
    public Action<int, OffensePlayer> OnBaseLeft;
    public Action<OffensePlayer, OutType> OnOut;
    public Action<OffensePlayer> OnHome;

    public int baseAtPitch
    {
        get; protected set;
    }

    //[HideInInspector]
    public int baseLastTouched
    {
        get { return _baseLastTouched; }
        protected set { //Debug.Log("touch base " + value);
            _baseLastTouched = value; }
    }

    private int _baseLastTouched;

    public bool forcedRun
    {
        get; protected set;
    }

    public int baseTarget_Eventual { get; protected set; }
    public int baseTarget_Immediate { get; protected set; }
    /*
	private WorldScaler worldScale;

	public void SetWorldScaler(WorldScaler world)
	{
		worldScale = world;
	}
*/
    public override void InstantReset()
    {
        StayOnBase(baseLastTouched);
        position = FieldMonitor.Instance.GetBasePosition(baseLastTouched);
        RemoveForce();
    }

    public override void Initialize()
    {
        Initialize(-1);

    }

    public void Initialize(int baseNo)
    {
        if (isInitialized) return;
        ResetBaseHistory();
        baseLastTouched = baseNo;
        FieldMonitor.RegisterRunner(this, baseNo);
        base.Initialize();
        bodyControl.SetAnimatorEnabled(true);

    }

    protected override void GetDressed()
    {
        base.GetDressed();
        bodyControl.EquipGear(FieldPlayerGear.HELMET, true);

    }

    public override void SetPlayerInfo(TeamInfo tInfo, PlayerInfo pInfo)
    {
        base.SetPlayerInfo(tInfo, pInfo);
        runSpeed = pInfo.runSpeedOffense;
    }

    public void ResetBaseHistory()
    {
        baseTarget_Immediate = 0;
        baseTarget_Eventual = 0;
        baseLastTouched = -1;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        FieldMonitor.UnRegisterRunner(this);
        //if (OnDestroyRunner != null) OnDestroyRunner.Invoke(this);
    }

    #region AIFieldPlayer Extension

    public override void SetHumanPosession(bool isPosessed)
    {
        if (isTetheredToCamera == isPosessed)
        {
            return;
        }
        base.SetHumanPosession(isPosessed);
        if (isPosessed)
        {
            HumanGearManager.instance.ShowBatterGear();
        }
    }

    protected override void SetPlayerAction(FieldPlayerAction action)
    {
        if (state == FieldPlayerAction.OUT) return; // MARC Fail-Safe: Under no circumstances should a runner who is OUT change state
        base.SetPlayerAction(action);
    }

    #endregion

    public override void RunToBase(int baseNo)
    {
        //        Debug.Log("Run to base " + baseNo);
        if (!isInitialized) Initialize();

        //if(FieldManager.Instance.defense.runningPlay == FieldPlayType.RETURN)
        //{
        //    return;
        //}

        baseTarget_Eventual = baseNo;
        baseTarget_Immediate = GetNextImmediateBaseTarget();

        //Debug.Log(gameObject.name + " baseTarget_Eventual: " + baseTarget_Eventual);
        //Debug.Log(gameObject.name + " baseTarget_Immediate: " + baseTarget_Immediate);
        //Debug.Log(gameObject.name + " baseLastTouched: " + baseLastTouched);

        //Greg: I'm not sure what to do if either of these return false.
        if (baseTarget_Eventual < FieldMonitor.Instance.numBases)
        {
            FieldMonitor.Instance.GetBase(baseTarget_Eventual).AttemptToTakeControl(this);
        }
        if (baseTarget_Immediate < FieldMonitor.Instance.numBases)
        {
            FieldMonitor.Instance.GetBase(baseTarget_Immediate).AttemptToTakeControl(this);
        }
        base.RunToBase(baseTarget_Immediate);
    }

    public void RunToBaseForced(int baseNo)
    {
        forcedRun = true;
        RunToBase(baseNo);
    }

    /// <summary>
    /// If a human player has reached a base, the runner's state should be BASE if they are safe, RUN if they are forced.
    /// It will be changed to RUN when they leave the base
    /// </summary>
    /// <param name="b"></param>
    public void OnHumanBaseEntered(FieldBase b)
    {

        //Debug.Log("Human on base " + b.baseNum);
        if(b.baseNum>baseLastTouched+1 || b.baseNum<baseLastTouched)
        {
            return;//skipped a base? or touched homebase as leaving it?
        }
        // retreated to last base
        if (b.baseNum == baseLastTouched)
        {
            SetPlayerAction(FieldPlayerAction.BASE);
        }

        baseLastTouched = b.baseNum;

        if (baseLastTouched != baseTarget_Eventual)
        {
            //Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN INCOMPLETE COMPLETE");
            baseTarget_Immediate = GetNextImmediateBaseTarget();
            //SetMoveTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
            //SetLookTargetPosition(FieldManager.Instance.GetBasePosition(baseTarget_Immediate));
        }
     
        else//running forward
        {
            //Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN COMPLETE");
            if (forcedRun) RemoveForce();
            SetPlayerAction(FieldPlayerAction.BASE);
            baseTarget_Eventual = baseLastTouched + 1;
            baseTarget_Immediate = GetNextImmediateBaseTarget();
            if (OnBaseReached != null) OnBaseReached.Invoke(baseLastTouched, this);
        }

        HumanPosessionManager.SetProjectionRailPoints(FieldMonitor.Instance.GetBasePosition(baseLastTouched), FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate));

    }

    public void OnHumanBaseExited(FieldBase b)
    {
        if(state != FieldPlayerAction.OUT)
            SetPlayerAction(FieldPlayerAction.RUN);
    }

    public float GetTimeToRunBetweenBases(int startBase, int endBase)
    {
        startBase = Mathf.Clamp(startBase, 0, FieldMonitor.Instance.numBases);
        endBase = Mathf.Clamp(endBase, 0, FieldMonitor.Instance.numBases);
        float total = 0;
        int direction = startBase < endBase ? 1 : -1;
        for (int baseNo = startBase; baseNo != endBase; baseNo += direction)
        {
            total += Vector3.Distance(FieldMonitor.Instance.GetBasePosition(baseNo), FieldMonitor.Instance.GetBasePosition(baseNo + direction)) / runSpeed;
        }
        //        Debug.Log("Time to Base " + startBase + " " + endBase + " " + total);
        return total;
    }

    protected int GetNextImmediateBaseTarget()
    {
        int eventualGoal = baseTarget_Eventual;
        int lastTagged = baseLastTouched;
        if (eventualGoal == lastTagged) return eventualGoal;
        int direction = (baseTarget_Eventual > lastTagged ? 1 : -1);
        return (lastTagged + direction);
    }

    public void StayOnBase(int baseNo)
    {
        baseTarget_Eventual = baseNo +1;
        baseTarget_Immediate = baseNo +1;
        baseLastTouched = baseNo;
        SetPlayerAction(FieldPlayerAction.BASE);
    }

    public void EjectFromBase(FieldBase b)
    {
        Debug.Log("----------------------------EJECT "+name+" FROM BASE "+b.baseNum+"????????????");
        if((state == FieldPlayerAction.BASE && b.baseNum != baseLastTouched) || (state == FieldPlayerAction.RUN && b.baseNum != baseTarget_Eventual))
        {

            Debug.Log(state+" NO NEVERMIND THAT'S NOT MY BASE "+b.baseNum+"  "+baseTarget_Eventual+" "+baseTarget_Immediate+" "+baseLastTouched);
            return;//must be some sort of mistake. I'm not even going to that base.
        }

        if(!forcedRun)//that's ok. I can just keep running.
        {
            if(b.baseNum == baseLastTouched)
            {
                RunToBaseForced(baseLastTouched + 1);
            }
            else
            {
                RunToBase(baseLastTouched);//Greg: This isn't forced because I don't want players to accidentally get their own runners out simply by motioning to the next base.
            }
        }
    }

    public void RemoveForce()
    {
        forcedRun = false;
    }

    #region FieldManager Handles
    public void NotifyOfOut(OutType outCall)
    {
        if (state == FieldPlayerAction.OUT) return; //ALREADY OUT
                                                    //        Debug.Log(this.name + " OUT " + outCall);
        if (OnOut != null) OnOut.Invoke(this, outCall);
        SetPlayerAction(FieldPlayerAction.OUT);
    }

    public void NotifyOfRunCompletion()
    {
        //Debug.Log(this.name + " COMPLETED RUN");

        OneShotEffectManager.SpawnEffect("run_score", FieldMonitor.Instance.GetHomePlate().transform.position, Quaternion.identity, 3f, spectatorOnly: isTetheredToCamera);
        //FieldManager.Instance.callou
        if (OnHome != null) OnHome.Invoke(this);
        /*
        if (!gameObject.activeSelf) Destroy(this.gameObject);
        else
        StartCoroutine(DestroyNextFrame());
        */
    }

    public void RecordBaseAtPitch()
    {
        baseAtPitch = baseLastTouched;

    }
    #endregion

    public void AttemptTagOut(OutfielderGlove glove)
    {
        if (!FieldMonitor.Instance.RunnerIsOnBase(this) && glove.heldBall != null && FullGameDirector.Instance.offense.runningPlay != FieldPlayType.WALK)
        {
            NotifyOfOut(OutType.TAG);
        }
    }

    #region StateMachine Logic
    void RESET_Enter()
    {
        SetPlayerAction(FieldPlayerAction.IDLE);
    }

    void RUN_Enter()
    {
        SetLookMode(LookMode.FORWARD);
        SetMoveTargetPosition(FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate));
        SetLookTargetPoint(FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate));
    }

    void RUN_FixedUpdate()
    {
        MoveTowardsTargetPosition();
    }

    [HideInInspector]
    float distanceToNextBase = float.MaxValue;
    [HideInInspector]
    float lastDistanceToNextBase = 0;

    void RUN_Update()
    {
        //Debug.Log("++++++++++++++++++++++++++ "+gameObject.name+" RUN_UPDATE");
        Debug.DrawLine(position + (Vector3.up * 2f), FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate) + (Vector3.up * 2f), Color.cyan);

        //if this runner is player controlled, detect which base they're running towards and 
        if(isTetheredToCamera)
        {
            if (!forcedRun)
            {
                int lastBase = baseLastTouched;
                int nextBase = baseLastTouched + 1;

                distanceToNextBase = Vector3.Distance(position, FieldMonitor.Instance.GetBasePosition(nextBase));


                bool runningToNextBase = lastDistanceToNextBase > distanceToNextBase;
                //                Debug.Log(lastDistanceToNextBase + "   " + distanceToNextBase);

                int previous_baseTarget_Immediate = baseTarget_Immediate;
                baseTarget_Immediate = (runningToNextBase ? nextBase : lastBase);

                //Greg: There's been a change of heart!
                if(baseTarget_Immediate != previous_baseTarget_Immediate)
                {
                    //Greg: Uh Oh! you changed your mind to a base you cannot take!
                    if (FieldMonitor.Instance.GetBases()[baseTarget_Immediate].AttemptToTakeControl(this) == false)
                    {
                        if (baseTarget_Immediate == lastBase)
                        {
                            if(FieldMonitor.Instance.GetBases()[baseTarget_Immediate].offensivePlayers.Count>0)
                            {
                                //GREG: You REALLY can't go back because someone else is on tha base now!
                                baseTarget_Immediate = previous_baseTarget_Immediate;
                                forcedRun = true;
                            }
                        }
                    }
                }

                //only save this distance if its at least .25f different from last
                if (Mathf.Abs(distanceToNextBase - lastDistanceToNextBase) > .2f)
                {
                    lastDistanceToNextBase = distanceToNextBase;
                }
            }

            return;
        }

        //GREG
        //if you're running forward and forced and the base behind you becomes available
            //you are no longer forced and can turn back if you like. 
        if(forcedRun)
        {
            if (baseLastTouched != -1)
            {
                if (FieldMonitor.Instance.GetBases()[baseLastTouched].runnerWithControl == null && baseTarget_Eventual != baseAtPitch)
                {
                    RemoveForce();
                }
            }
        }

        if (Vector3.Distance(position, FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate)) < (.1f))
        {
            baseLastTouched = baseTarget_Immediate;
            if (baseLastTouched != baseTarget_Eventual)
            {
//                Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN TO END COMPLETE");
                baseTarget_Immediate = GetNextImmediateBaseTarget();
                SetMoveTargetPosition(FieldMonitor.Instance.GetBasePosition(baseTarget_Immediate));

            }
            else//running forward
            {
//                Debug.Log("++++++++++++++++++++++++++ " + gameObject.name + " RUN_UPDATE RAN INCREMENT COMPLETE");
                if (forcedRun) RemoveForce();
                SetPlayerAction(FieldPlayerAction.BASE);
                if (OnBaseReached != null) OnBaseReached.Invoke(baseTarget_Immediate, this);
            }
        }
    }

    void RUN_Exit()
    {

    }

    void BASE_Enter()
    {
        if (isTetheredToCamera)
        {
            HumanPosessionManager.SetReverseRailLocomotionEnabled(false);
        }
        SetLookTargetPoint(Vector3.zero);
        SetLookMode(LookMode.POINT);

    }

    void BASE_Update()
    {
        //GREG
        //if the base you're on is targetted by another runner behind you
            //you are forced to run to the next base.



        //if(FieldManager.Instance.GetBases()[baseLastTouched])
    }


    void BASE_Exit()
    {
        if (isTetheredToCamera)
        {
            HumanPosessionManager.SetReverseRailLocomotionEnabled(true);
        }
        if (OnBaseLeft != null) OnBaseLeft.Invoke(baseAtPitch, this);

    }

    void OUT_Enter()
    {
        DarkTonic.MasterAudio.MasterAudio.PlaySound("player_out_explode");
        OneShotEffectManager.SpawnEffect("player_out", this.transform.position, Quaternion.identity, 3f, spectatorOnly: isTetheredToCamera);
        ResetBaseHistory();
    }

    void OUT_Update()
    {
        /*
        if(!isTetheredToCamera)
        GameObject.Destroy(this.gameObject);
        */
    }
    #endregion
}
