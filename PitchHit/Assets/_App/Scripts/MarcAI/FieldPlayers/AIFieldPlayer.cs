﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

[RequireComponent(typeof(Rigidbody))]
public abstract class AIFieldPlayer : MonoBehaviour {

    public enum FieldPlayerAction
    {
        IDLE,
        CATCH,
        CHASE,
        RESET,
        RUN,
        BASE,
        OUT,
        BACKUP,
        TAG,
        THROW,
    }

    public enum LookMode
    {
        POINT, // will look towards lookTargetPosition
        FORWARD, // will look in direction of movement
        FREEZE, // will not change look direction
        TRANSFORM, // will follow a transform
    }

    public struct PlayerActionContainer
    {
        public PlayerActionContainer(FieldPlayerAction action, OffensePlayer off = null, DefensePlayer def = null, Vector3 pos = default(Vector3), int _baseNo = -1)
        {
            this.action = action;
            offensePlayer = off;
            defensePlayer = def;
            position = pos;
            baseNo = _baseNo;
        }
        public FieldPlayerAction action;
        public OffensePlayer offensePlayer;
        public DefensePlayer defensePlayer;
        public Vector3 position;
        public int baseNo;

    }

    public float chestHeight
    {
        get { return info.chestHeight; }
    }

    public PlayerInfo info { get { return m_info; } protected set { m_info = value; } }
    [SerializeField]
    private PlayerInfo m_info;
    [SerializeField]
    public TeamInfo team { get; protected set; }

    public new Rigidbody rigidbody { get; protected set; }
    
    protected FieldPlayerBodyControl bodyControl
    {
        get
        {
            if (!_bodyControl)
            {
//                Debug.Log(this.name + " make body");

                _bodyControl = GetComponentInChildren<FieldPlayerBodyControl>();
            
                if (!_bodyControl && bodyControlPrefab)
                {
                    _bodyControl = GameObject.Instantiate(bodyControlPrefab, this.transform);
                    _bodyControl.transform.localPosition = Vector3.zero;
                }

                if (_bodyControl)
                {
                    GetDressed();
                    if (animatorControl)
                    {
                        //Debug.Log("here body anim");
                        _bodyControl.SetAnimationController(animatorControl);
                    }
                }
            }
            return _bodyControl;
        }
    }
    private FieldPlayerBodyControl _bodyControl;
    [SerializeField] protected FieldPlayerBodyControl bodyControlPrefab;
    [SerializeField] protected RuntimeAnimatorController animatorControl;


    protected int numOfThrowAnimations = 9;
    protected int numOfRunAnimations = 3;

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
        set
        {
            if (bodyControl.isActiveAndEnabled)
                bodyControl.DisableParticlesForFrame();
            transform.position = value;
        }
    }
    protected string debugText;

    public FieldPlayerAction state
    {
        get { return fsm.State; }
    }

    public FieldPlayerAction editorState;
    protected StateMachine<FieldPlayerAction> fsm;
    protected bool isInitialized = false;
	public bool isTetheredToCamera
    {
        get { return HumanPosessionManager.currentlyTetheredPlayer == this; }
    }
    protected new Collider collider;

    /// <summary>
    /// Target movement position in local space
    /// </summary>
    public Vector3 moveTargetPosition { get
        {
            return _moveTargetPosition;
        }
        private set
        {
            _moveTargetPosition = value;
        }
    }
    private Vector3 _moveTargetPosition;

    /// <summary>
    /// Target look position in local space
    /// </summary>
    protected Vector3 lookTargetPosition
    {
        get
        {
            return _lookTargetPosition;
        }
        private set
        {
            if (!lookTargetLock)
            {
                _lookTargetPosition = value;
            }
        }
    }
    private Vector3 _lookTargetPosition;
    protected Transform lookTargetTransform
    {
        get
        {
            return _lookTargetTransform;
        }
        private set
        {
            if (!lookTargetLock)
            {
                _lookTargetTransform = value;
            }
        }
    }
    private Transform _lookTargetTransform;

    private LookMode lookMode = LookMode.FORWARD; //if false, player will look in the direction of movement. if true, player will look towards lookTargetPosition
    private bool lookTargetLock = false; //if true, looktTargetPosition cannot be changed, 


    private Vector3 lastPosition;

    public float runSpeed = 5f;
    public float walkSpeed = 2f;
    protected float turnSpeed = 0.1f;
    public float delta = 0.0f;

    private bool freezePosition = false;
	protected bool applyCatchPositionNoise = false;

    public bool isFadingOut
    {
        get;protected set;
    }


    public Action<AIFieldPlayer, FieldPlayerAction> OnFieldPlayerStateChange;
    public Action<AIFieldPlayer, PlayerActionContainer> OnHumanInstruction;
    public Action<AIFieldPlayer> OnHumanPosession;
    public Action<AIFieldPlayer> OnHumanDeposession;

    public SpriteRenderer destinationPointer;

    public abstract void InstantReset();

    public void Reset()
    {
        fsm.ChangeState(FieldPlayerAction.RESET);
    }

    public void Idle()
    {
        fsm.ChangeState(FieldPlayerAction.IDLE);
    }

    public void EquipGear(FieldPlayerGear gear, bool equip)
    {
        bodyControl.EquipGear(gear, equip);
    }

    protected virtual void GetDressed()
    {
        bodyControl.EquipGear(FieldPlayerGear.HEAD, true);
        bodyControl.EquipGear(FieldPlayerGear.JERSEY, true);
        bodyControl.SetHandVisibility(HandManager.HandType.both, true);
        bodyControl.SetFeetVisibility(false);
    }


    public virtual void Initialize() {
        if (isInitialized) return;
        collider = GetComponent<Collider>();
        if (collider)
        {
            collider.enabled = false;
            collider.enabled = true;
        }
        rigidbody = GetComponent<Rigidbody>();
        fsm = StateMachine<FieldPlayerAction>.Initialize(this);
        fsm.Changed += SetEditorVisibleState;
        fsm.ChangeState(FieldPlayerAction.RESET);
        fsm.Changed += InvokeStateChangeEvent;
        OnFieldPlayerStateChange += MoodChange;
        if (destinationPointer)
        {
            destinationPointer.enabled = false;
            destinationPointer.transform.SetParent(this.transform.parent);
        }
        lastPosition = transform.position;
        

            bodyControl.SetAnimationFloat("RunType", (int)UnityEngine.Random.Range(0, numOfRunAnimations));
        
        bodyControl.SetDitherTransparency(1);
        transform.LookAt(Vector3.zero);
        isInitialized = true;
    }

    public virtual void SetPlayerInfo(TeamInfo tInfo, PlayerInfo pInfo)
    {
        if(pInfo)
        info = pInfo;
        if(tInfo)
        team = tInfo;
        //bool isHome = tInfo == FieldManager.Instance.homeTeam;//Greg. This doesn't work when a team plays against a clone of itself.

        //Material[] jerseyMaterials = new Material[2];
        Texture front;
        Texture back;
        Color color;


        if (tInfo)
        {
            if (FullGameDirector.homeTeam == tInfo)
            {
                back = tInfo.jerseyHomeBack;
                front = tInfo.jerseyHomeFront;
                color = Color.white;
            }
            else
            {
                back = tInfo.jerseyAwayBack;
                front = tInfo.jerseyAwayFront;
                color = tInfo.awayColor;
            }
        }

        if (pInfo && tInfo)
        {
            bodyControl.DressAsPlayer(pInfo, tInfo);
        }
        if (pInfo)
        {
            bodyControl.armLength = info.armLength;
        }
        //Greg: Update jersey appearance and stats here.
    }

    protected virtual void OnEnable()
    {
        isFadingOut = false;
    }

    protected virtual void OnDisable()
    {

    }

    protected virtual void OnDestroy()
    {
        if(destinationPointer != null)
            GameObject.Destroy(destinationPointer.gameObject);
        if (isTetheredToCamera)
        {
            HumanPosessionManager.ReleasePosessedPlayer(this);
        }
    }

	protected IEnumerator DestroyNextFrame()
	{
		yield return null;
		GameObject.Destroy(this.gameObject);
	}

    protected void Start()
    {

        if (!isInitialized) Initialize();
    }

    protected void Update()
    {
        UpdateLook();
    }

    protected void FixedUpdate()
    {
        RecordMovementDelta(Time.fixedDeltaTime);

    }

#if UNITY_EDITOR
    protected virtual void OnDrawGizmosSelected()
    {
        Handles.Label(transform.position + Vector3.up * 4, debugText);
    }
#endif
    #region Movement
    /// <summary>
    /// Call in FixedUpdate() only
    /// </summary>
    protected void MoveTowardsTargetPosition()
    {
        MoveTowardsPosition(moveTargetPosition);
    }

    protected void MoveTowardsTargetPosition(Vector3 offset)
    {
        MoveTowardsPosition(moveTargetPosition + offset);
    }

    public bool HasReachedRunTarget()
    {
        return Vector3.Distance(position, moveTargetPosition) < .1f;
    }


    /// <summary>
    /// Move towards a position in local space
    /// </summary>
    /// <param name="target">target position in local space</param>
    private void MoveTowardsPosition(Vector3 target)
    {
        if (freezePosition || isTetheredToCamera) return;

        Vector3 worldTargetPosition = (target);

        float maxDist = runSpeed * Time.fixedDeltaTime;
        if (maxDist == 0) return;
        Vector3 movePosition = Vector3.MoveTowards(rigidbody.position, worldTargetPosition, maxDist);
        rigidbody.MovePosition(movePosition);
        Debug.DrawLine(worldTargetPosition, worldTargetPosition + Vector3.up * .2f, Color.white);
        //transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, runSpeed * Time.fixedDeltaTime);
    }


    private Vector3 turnVelocity = Vector3.zero;
    private void LookTowardsPosition(Vector3 lookPos)
    {
        if (Time.timeScale == 0) return;

        Vector3 currentForward = transform.forward;

        Vector3 worldTargetPosition = (lookPos);
        worldTargetPosition.y = rigidbody.position.y;

        Vector3 targetForward = (worldTargetPosition - rigidbody.position).normalized;
        if (Vector3.Distance(rigidbody.position, worldTargetPosition) > .1f)
        {
            
            Vector3 nextForward = Vector3.SmoothDamp(currentForward, targetForward, ref turnVelocity, turnSpeed).normalized;
            //if the player is looking exactly 180 degrees from target, nudge the rotation
            if (nextForward == currentForward)
            {
                transform.Rotate(Vector3.up);
            }
            else {
                transform.LookAt(position + nextForward);
            }
        }
    }

    public void DressLikeOtherPlayer(AIFieldPlayer other)
    {
        //bodyControl.DressAsPlayer(other.info);
    }

    public void FadeToDeactivate(float duration, float delay = 0)
    {
        isFadingOut = true;
        DitherFade(1, 0, duration, delay);
        DeactivateAfterSeconds(duration + delay);
    }

    public void DitherFade(float start, float end, float duration, float delay = 0)
    {
        bodyControl.DitherFadeOverTime(start, end, duration, delay);
    }

    public void DitherFade(float val)
    {
        bodyControl.SetDitherTransparency(val);
    }

    public void DeactivateAfterSeconds(float delay)
    {
        StartCoroutine(DeactivateDelay(delay));
    }

    protected IEnumerator DeactivateDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        yield return null;
        this.gameObject.SetActive(false);
    }


    protected void SetMoveTargetPosition(Vector3 pos, Space space = Space.Self)
    {
        switch (space)
        {
            case Space.Self:
                pos.y = 0;
                moveTargetPosition = pos;
                break;
            case Space.World:
                pos = (pos);
                pos.y = 0;
                moveTargetPosition = pos;
                break;

        }
    }

    protected void LockLookTarget(bool isLocked)
    {
        lookTargetLock = isLocked;
    }

    public void SetLookMode(LookMode mode)
    {
        lookMode = mode;
    }

    protected void SetLookTargetPoint(Vector3 pos, Space space = Space.Self)
    {
        switch (space)
        {
            case Space.Self:
                pos.y = 0;
                lookTargetPosition = pos;
                break;
            case Space.World:
                pos = (pos);
                pos.y = 0;
                lookTargetPosition = pos;
                break;
        }
    }
    protected void SetLookTargetTransform(Transform target)
    {
        lookTargetTransform = target;
    }

    private void UpdateLook()
    {
        switch (lookMode)
        {
            case LookMode.FORWARD:
                Vector3 forward = (position - lastPosition).normalized;
                if (forward.sqrMagnitude > 0.1f)
                {
                    LookTowardsPosition(position + forward.normalized);
                }
                break;
            case LookMode.POINT:
                LookTowardsPosition(lookTargetPosition);
                break;
            case LookMode.FREEZE:
                break;
            case LookMode.TRANSFORM:
                if (lookTargetTransform)
                {
                    LookTowardsPosition(lookTargetTransform.position);
                }
                break;
        }
    }

    public void Hesitate(float seconds)
    {
        StopCoroutine("FreezePositionForSeconds");
        StartCoroutine("FreezePositionForSeconds", seconds);
    }

    IEnumerator FreezePositionForSeconds(float delay)
    {
        freezePosition = true;
        yield return new WaitForSeconds(delay);
        freezePosition = false;
    }
    #endregion

    #region State Change
    protected virtual void SetPlayerAction(FieldPlayerAction action)
    {
        fsm.ChangeState(action);
    }


    void InvokeStateChangeEvent(FieldPlayerAction state)
    {
        if (OnFieldPlayerStateChange != null) OnFieldPlayerStateChange.Invoke(this, state);
    }

    public void MoodChange(AIFieldPlayer player, FieldPlayerAction action)
    {
        switch (action)
        {
            case FieldPlayerAction.IDLE:
                player.bodyControl.SetMoodIdle();
                break;
            case FieldPlayerAction.OUT:
                player.bodyControl.SetMoodSuprise();
                break;
            case FieldPlayerAction.RUN:
                player.bodyControl.SetMoodIntense();
                break;
            case FieldPlayerAction.CATCH:
                player.bodyControl.SetMoodSmile();
                break;
            case FieldPlayerAction.TAG:
                player.bodyControl.SetMoodSmile();
                break;
            case FieldPlayerAction.CHASE:
                player.bodyControl.SetMoodIntense();
                break;
            default:
                player.bodyControl.SetMoodIdle();
                break;
        }
    }

    /// <summary>
    /// Set the edistorState field for debugging with the Inspector
    /// </summary>
    /// <param name="state"></param>
    void SetEditorVisibleState(AIFieldPlayer.FieldPlayerAction state)
    {
#if UNITY_EDITOR
       // Debug.Log("<color=grey>"+this.name + " " + state+"</color>", this);
#endif
        editorState = state;

    }

    #endregion

    /// <summary>
    /// Set Gizmo text floating above this player
    /// </summary>
    /// <param name="text"></param>
    public void SetDebugText(string text)
    {
        debugText = text;
    }

    /// <summary>
    /// Set color of the Selection highlight
    /// </summary>
    /// <param name="c"></param>
    protected virtual void SetSelectableColor(Color c)
    {
        //highlight.color = c;
        //destinationPointer.color = c;
    }

    public void AnnounceHumanInstruction(PlayerActionContainer action)
    {
        if(OnHumanInstruction != null) OnHumanInstruction.Invoke(this, action);
    }


    public virtual void RunToBase(int baseNo)
    {
        RunToTarget(FieldMonitor.Instance.GetBasePosition(baseNo));
    }


    /// <summary>
    /// Run to local space position
    /// </summary>
    /// <param name="runToPosition"></param>
    public virtual void RunToTarget(Vector3 runToPosition)
    {
        if (!isInitialized) Initialize();
        SetMoveTargetPosition(runToPosition);
        fsm.ChangeState(FieldPlayerAction.RUN);
    }

    public virtual void RunToTarget(Transform trans)
    {
        RunToTarget((trans.position));
    }

    public void LookAtTarget(Vector3 lookPosition, Space space = Space.Self)
    {
        SetLookTargetPoint(lookPosition);
    }

    /// <summary>
    /// Get time to run to position
    /// </summary>
    /// <param name="runToPosition">target position in local space</param>
    /// <returns></returns>
    public virtual float GetTimeToRunToPosition(Vector3 runToPosition)
    {
        float tempRunSpeed = runSpeed; 

        if(isTetheredToCamera)
        {
            tempRunSpeed = ArmSwinger.armSwingMaxSpeed * .1f;
        }

        runToPosition.y = 0;
        return Vector3.Distance(this.position, runToPosition)/tempRunSpeed;
    }
    public virtual float GetTimeToRunToBase(int baseNo)
    {
        return GetTimeToRunToPosition(FieldMonitor.Instance.GetBasePosition(baseNo));

    }

    public void Teleport(Vector3 toPosition, float duration = 1)
    {
        Debug.Log(this.name + " TELEPORT TO " + toPosition);
        StartCoroutine(TeleportRoutine(toPosition, duration));
    }

    protected IEnumerator TeleportRoutine(Vector3 target, float duration)
    {
        DitherFade(1, 0, duration/2f);
        yield return new WaitForSeconds(duration/2f);
        position = target;
        DitherFade(0, 1, duration/2f);

    }


    public PlayerHighlight highlight;
    

    #region Animation
    public void PlayPitchAnimation()
    {
        bodyControl.SetAnimationTrigger("pitch");
    }

    public void StartWalkingAnimation()
    {
        bodyControl.SetAnimationFloat("Confidence", UnityEngine.Random.value>=.5? 0f: 1f);
        //animator.Play("Walk",0,0f);
        bodyControl.SetAnimationTrigger("walk");
    }

	private const float PERFECT_PITCH_SPEED_FOR_SWING_ANIMATION = 30f;
    public void PlaySwingAnimation(hittable ball, float swingSpeed = 1f)
    {
		//Debug.Log ("Play Swing Animation "+ball.name);
		if (ball) {
			//bodyControl.SetAnimationSpeed((ball.rb.velocity.magnitude / PERFECT_PITCH_SPEED_FOR_SWING_ANIMATION));
		}
        //bodyControl.SetAnimationSpeed(swingSpeed);
        bodyControl.SetAnimationFloat("swing_speed", swingSpeed);
        bodyControl.SetAnimationTrigger("bat");
    }

    public void PlaySwingSetupAnimation()
    {
        bodyControl.SetAnimationTrigger("bat_setup");
    }


    protected void RecordMovementDelta(float timeDelta)
    {
            delta = Vector3.Distance(lastPosition, transform.position) / timeDelta;
			bodyControl.SetAnimationFloat("moveDelta", delta);
            lastPosition = transform.position;
    }

    
    public virtual void SetHumanPosession(bool isPosessed)
    {
        //Debug.Log("set human posession " + isPosessed);
        if (isTetheredToCamera == isPosessed)
        {
            //            Debug.Log("REDUNDANT POSSESSION ASSIGNMENT. ABANDON TO PREVENT PROBLEMS.");
            return;
        }
        
        //isTetheredToCamera = isPosessed;
        if (isPosessed)
        {
            if (OnHumanPosession != null) OnHumanPosession.Invoke(this);


			if(bodyControl)
			{

                bodyControl.SetAnimatorEnabled(false);

                Debug.DrawRay(Camera.main.transform.position, Vector3.up, Color.green, 100f);
                bodyControl.acceptDeviceInput = true;
                
                bodyControl.EquipGear(FieldPlayerGear.CAP, false);
                bodyControl.EquipGear(FieldPlayerGear.HEAD, false);// headRoot.gameObject.SetActive (false);
                bodyControl.EquipGear(FieldPlayerGear.JERSEY, false);//.gameObject.SetActive (false);
                bodyControl.EquipGear(FieldPlayerGear.HELMET, false);//.gameObject.SetActive (false);

                bodyControl.SetHandVisibility (HandManager.HandType.both, false);
                bodyControl.SetFeetVisibility(false);

                HumanGearManager.instance.SetTorsoMaterials(bodyControl.GetJerseyMaterials());
                HumanGearManager.instance.DressAsPlayer(info, team);

            }


        }
        else
        {
//            Debug.Log("start reappearing body parst.");
            if (OnHumanDeposession != null) OnHumanDeposession.Invoke(this);

			
			if(bodyControl)
			{
                bodyControl.SetAnimatorEnabled(true);
                bodyControl.acceptDeviceInput = false;
                GetDressed();
                DitherFade(1);
			}

        }

    }

    /// <summary>
    /// Usually posessed players will stick to the camera position, this will set it to something else
    /// </summary>
    /// <param name="follow"></param>
    public void SetPosessedBodyFollow(Transform follow)
    {

        bodyControl.SetRootFollow(follow);
    }
    

    public Material[] GetJerseyMaterials()
    {
        if (bodyControl)
            return bodyControl.GetJerseyMaterials();
        else return new Material[0];
    }
    #endregion
}
