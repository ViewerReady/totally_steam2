﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatterPlayer : AIFieldPlayer {

    [SerializeField]
    protected BatController bat;
    [SerializeField]
    protected Transform batRoot;
    private bool autoSwingFlag = false;
    [SerializeField]
    protected AutoBatterBox autoBatter;

    [SerializeField]
    private bool forceAlwaysStrike = false;

    public bool finishedSetupAnimation = false;

    public static Action OnSwing;

    public override void Initialize()
    {
        if (isInitialized) return;
        //SetSelectableColor(FieldManager.offenseColor);
        base.Initialize();

        //MARC: we will never want the batterPlayer's bat to use physics. AI uses timing estimation and human uses a separate bat.
        bat.SetColliderActive(false);
        bodyControl.SetAnimationFloat("TeamRole", 1f);
        bodyControl.ParentToHand(batRoot, HandManager.HandType.left);
    }

    void OnEnable()
    {
        //PlaySwingSetupAnimation();
    }

    public override void InstantReset()
    {
        position = Vector3.left;
    }

    protected override void GetDressed()
    {
        base.GetDressed();
        bodyControl.SetHandVisibility(HandManager.HandType.left, false);
        bodyControl.EquipGear(FieldPlayerGear.HELMET, true);
    }

    public override void SetHumanPosession(bool isPosessed)
    {
        if (isTetheredToCamera == isPosessed)
        {
            return;
        }
        base.SetHumanPosession(isPosessed);

        hitThings.currentGripType = (isPosessed? hitThings.gripType.always : hitThings.gripType.holding);
        GameController.isAtBat = isPosessed;
        if (isPosessed)
        {
            HumanGearManager.instance.ShowBatterGear();
            HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, true);
            batRoot.gameObject.SetActive(false);
        }
        else
        {
            position = Vector3.left;
            transform.localRotation = Quaternion.identity;
            if (HandManager.instance)
            {
                HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.BAT, false);
            }
            if (batRoot)
            {
                batRoot.gameObject.SetActive(true);
            }
        }
    }

    public override void SetPlayerInfo(TeamInfo tInfo, PlayerInfo pInfo)
    {

        base.SetPlayerInfo(tInfo, pInfo);
        if (pInfo)
        {
            autoBatter.foulLikelihood = pInfo.foulChance;
            autoBatter.homerunLikelihood = pInfo.homeRunChance;
            autoBatter.shootSpeedRange = pInfo.powerRange;
        }
    }


    public void PrepareForAutoBat(BallGate strikeGate, hittable ball)
    {
        autoSwingFlag = false;
        StopAllCoroutines();
        StartCoroutine(SwingAfterDelay(strikeGate, ball));
        PlaySwingSetupAnimation();
    }

    void AutoSwingAtBall(hittable ball, float swingSpeed =1f, bool snapToContact = false)
    {
        if (OnSwing != null) OnSwing.Invoke();
        autoSwingFlag = true;
        PlaySwingAnimation(ball,swingSpeed);
        finishedSetupAnimation = false;
    }

    public bool AutoSwingDetected()
    {
        return autoSwingFlag;

        //return bat.SwingWasDetected();
    }

    //current swing anim takes quarter second to reach contact position
    const float SWING_ANIM_DURATION = .25f;

    IEnumerator SwingAfterDelay(BallGate strikeGate, hittable ball)
    {
        //Debug.Log("___________________________________ SWING AFTER DELAY 01");
        yield return new WaitForFixedUpdate();

        float t;
        bool isStrike;
        bool takeSwing;
        bool contactBall;

        if (BallWillPassThroughGate(strikeGate, ball, out isStrike, out t))
        {
            //Debug.Log("___________________________________ SWING AFTER DELAY 01A");
            //Debug.Log("IS STRIKE? " + isStrike);
            takeSwing = (UnityEngine.Random.Range(0f, 1f) < (isStrike ? info.swingAtStrikes : info.swingAtBalls));
            //Debug.Log("TAKE SWING? " + takeSwing);

            contactBall = takeSwing && (UnityEngine.Random.Range(0f, 1f) > info.missSwing);

            if (forceAlwaysStrike)
            {
                takeSwing = true;
                contactBall = false;
            }


            float expectedContactTime = Time.time + t; // t - .395f; //.5f is how long it takes for the swing to reach contact position

            //Debug.Log("ball will pass gate at " + expectedContactTime + " seconds");
            float swingStartTime = expectedContactTime - SWING_ANIM_DURATION;

            while(Time.time < swingStartTime)
            {
                //Debug.Log("___________________________________ SWING AFTER DELAY 02");
                //Debug.Log(Time.time);
                yield return new WaitForFixedUpdate();
            }
            //Debug.Log("___________________________________ SWING AFTER DELAY 03");
            if (takeSwing)
            {
                //Debug.Log("___________________________________ SWING AFTER DELAY 04 speed =("+expectedContactTime+"-"+Time.time+")/"+SWING_ANIM_DURATION);
                float timeToContact = (expectedContactTime - Time.time);
                float swingSpeed = 1;
                bool snapToContact = false;
                if (timeToContact > 0)
                {
                    swingSpeed = SWING_ANIM_DURATION / timeToContact;
                }
                else
                {
                    swingSpeed = 4;
                    snapToContact = true;
                }
                Debug.Log("ball arrives in " + timeToContact + " s   " + " swing will take " + SWING_ANIM_DURATION + " s    " + " use speed " + swingSpeed + "  snap to Contact " + snapToContact);
                
                AutoSwingAtBall(ball,swingSpeed, snapToContact);

            }

            while (Time.time < expectedContactTime)
            {
                //Debug.Log(Time.time);
                yield return new WaitForFixedUpdate();
            }

            //Debug.Log("CONTACT BALL BOOLEAN: " + contactBall);
            if (contactBall)
            {
                //Debug.Log("GONNA HIT BALL");
                autoBatter.HitBallInPlay();
            }

        }

        else
        {
            //Debug.Log("Expecting wild pitch, don't swing");
            //ball is way outside of strike gate, don't swing
        }

    }


    bool BallWillPassThroughGate(BallGate gate, hittable ball, out bool insideStrikeZone, out float t_pass)
    {
        TrajectoryInfo trajInfo = ball.GetTrajectory();

        t_pass = -1;

        insideStrikeZone = false;
        bool passed = false;
        float interval = Time.fixedDeltaTime;
        float maxTime = 5;
        float t = 0;
        float tIntersection = -1;
        while(!passed && t < maxTime)
        {
            Vector3 pos_1 = trajInfo.Position_at_T(t, Space.World) + Vector3.down * .088f;
            Vector3 pos_2 = trajInfo.Position_at_T(t+ interval, Space.World) + Vector3.down * .088f;
            if (gate.GateIsBetweenPoints(pos_1, pos_2, out tIntersection))
            {
                t_pass = t + interval*tIntersection;
                passed = true;
                insideStrikeZone = true;
            }
            else if (gate.GateIsBetweenPoints_Extended(pos_1, pos_2, out tIntersection))
            {
                t_pass = t + interval * tIntersection;
                passed = true;
            }
            t += interval;

        }

        return passed;

    }

    

    void RUN_Enter()
    {
        SetLookMode(LookMode.FORWARD);
    }

    void RUN_FixedUpdate()//Greg: The batter player really just uses this to approach the plate before batting.
    {
        if (isTetheredToCamera) return;

        Vector3 worldTargetPosition = (moveTargetPosition);

        float maxDist = walkSpeed * Time.fixedDeltaTime;
        if (maxDist == 0) return;

        Vector3 movePosition = Vector3.MoveTowards(rigidbody.position, worldTargetPosition, maxDist);
        rigidbody.MovePosition(movePosition);
        Debug.DrawLine(worldTargetPosition, worldTargetPosition + Vector3.up * .2f, Color.white);

        if (Vector3.Distance(transform.position,moveTargetPosition)<.01f)
        {
            fsm.ChangeState(FieldPlayerAction.IDLE);
        }
    }

    void RUN_Update()
    {


       
    }

    void IDLE_Enter()
    {
        SetLookMode(LookMode.POINT);
        SetLookTargetPoint(FieldMonitor.Instance.GetPitchersMoundPosition());
    }

}
