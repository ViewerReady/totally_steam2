﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MonsterLove.StateMachine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class DefensePlayer : AIFieldPlayer
{

    public DefenseFieldPosition fieldPosition
    {
        get; protected set;
    }
    private Vector3 fieldWorldPosition;

    const float backup_buffer = 10; //distance player should keep between them and the spot they are backing up

    public Transform gloveRoot;
    public OutfielderGlove glovePrefab;
    public OutfielderGlove glove
    {
        get
        {
            if (!m_glove) CreateGlove();
            return m_glove;
        }
    }
    private OutfielderGlove m_glove;

    public HandCannonController cannon;
    static HandCannonController theCannon;

    public CapsuleCollider personalSpaceCollider;

    public const float throwWindupDuration = 0.5f;
    public float throwSpeed
    {
        get {
            if (isTetheredToCamera)
            {
                return HandManager.instance.GetHumanThrowStrength();
            }
            else
            {
                return info.throwSpeed;
            }
        }
    }

    public float pitchSpeed
    {
        get { return info.pitchSpeed; }
    }

    public float maxVerticalReach
    {
        get {
            //Debug.Log("get players max vertical reach "+chestHeight+" "+info.armLength+" "+glove.GetMaxVerticalReach());
            return chestHeight + info.armLength + glove.GetMaxVerticalReach();
        }
    }
    public float maxHorizontalReach
    {
        get { return info.armLength; }
    }

    public float fumbleChance
    {
        get { return info.fumbleChance; }
    }

    public bool atPitchPosition = false;



    public bool inPosessionOfBall
    {
        get
        {
            /*
            //Debug.Log ("does "+gameObject.name+" have the ball?"+heldBall);
            if (isTetheredToCamera)
            {
                if (FieldManager.Instance.defense)
                {
                    if (FieldManager.Instance.defense.humanControlled && FieldManager.Instance.defense.playerGlove.hasBall)
                    {
                        return FieldManager.Instance.defense.playerGlove.GetCurrentBall().inGlove;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            */
            return heldBall != null;
        }
    }

    public bool needsInstructions
    {
        get
        {
            if(throwWindupInProgress)
            {
                return false;
            }
            return true;
        }
    }


    protected Transform backupTarget;
    protected Rigidbody trackedRigidbody;
    private Vector3 trackedRigidbody_lastWorldPos;

    public hittable trackedBall { get; protected set; }
    protected hittable heldBall
    {
        get
        {
            if (glove.heldBall) return glove.heldBall;
            else if (ballInHand) return ballInHand;
            return null;
        }
    }

    private hittable ballInHand;

    protected OffensePlayer trackedRunner;

    public Action<DefensePlayer> OnFumble;
    public Action<DefensePlayer> OnBallPosession;
    public Action<DefensePlayer> OnThrow;
    public Action<DefensePlayer, DefensePlayer, Vector3, hittable> OnPassThrow;
    public Action<DefensePlayer> OnMissedCatch;
    void Awake()
    {
        glove.OnBallCatch += (HandleCaughtBall);
        glove.OnFumbleCatch += (HandleFumble);
    }

    public override void InstantReset()
    {
        position = fieldWorldPosition;
        SetPlayerAction(FieldPlayerAction.RESET);
        CancelThrowAnimation();

        //if there's no fieldManager, this might be happening as a scen is unloaded
        if (!FullGameDirector.Instance) return;
        TeamInfo defendingTeam = FullGameDirector.currentDefensiveTeam;
        SetPlayerInfo(defendingTeam, defendingTeam.GetPlayerAssignedToPosition(fieldPosition));

        //DitherFade(0, 1, .5f);
    }

    public override void Initialize()
    {
        if (isInitialized) return;
        base.Initialize();
        bodyControl.PutGloveOnLeftHand(gloveRoot);
        bodyControl.SetAnimatorEnabled(true);
        CreateGlove();
    }

    protected override void GetDressed()
    {
        base.GetDressed();
        bodyControl.SetHandVisibility(HandManager.HandType.left, false);
        bodyControl.EquipGear(FieldPlayerGear.CAP, true);
    }

    private void CreateGlove()
    {
        if(!m_glove)
        m_glove = GameObject.Instantiate(glovePrefab, gloveRoot) as OutfielderGlove;
    }

    public override void SetPlayerInfo(TeamInfo tInfo, PlayerInfo pInfo)
    {
        base.SetPlayerInfo(tInfo, pInfo);
        runSpeed = pInfo.runSpeedDefense;
    }


#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Handles.color = Color.red;
        Handles.DrawLines(GetPointsForDirectionalWeights(30));
    }
#endif

    protected override void OnEnable()
    {
        base.OnEnable();
        CancelThrowAnimation();
    }
    protected override void OnDisable()
    {
        CancelThrowAnimation();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        StopTrackingRunner();
        StopTrackingBall();
        FieldMonitor.UnRegisterDefensePlayer(this);
    }

    #region AIFieldPlayer Extension

    public override void SetHumanPosession(bool isPosessed)
    {
        if (isTetheredToCamera == isPosessed)
        {
            //            Debug.Log("REDUNDANT POSSESSION ASSIGNMENT. ABANDON TO PREVENT PROBLEMS.");
            return;
        }
        base.SetHumanPosession(isPosessed);

        //Hook up glove to Human Glove
        if (isPosessed)
        {
            if (HandManager.instance)
            {
                HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, true);
                //HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.CANNON, true);
            }

            //if i have a ball, offer it to human glove before registering for its events to prevent a loop
            //if the human has a ball, 
            HumanGloveController humanGlove = HandManager.instance.glove;
            
            if (humanGlove)
            {

                if (glove.heldBall)
                {
                    Debug.Log("CatchBall held ball. C");
                    humanGlove.CatchBall(glove.heldBall);
                }
                else if (ballInHand)
                {
                    Debug.Log("CatchBall held ball. C");
                    humanGlove.CatchBall(ballInHand);
                    glove.ForceCatchBall(ballInHand);
                    ballInHand = null;
                }
                else
                {
                    HandManager.instance.HideCannonImmediately();
                }

                
            }

            HumanGloveController.onHumanBallCatch.AddListener(glove.ForceCatchBall);
            HumanGloveController.onHumanBallRelease.AddListener(glove.ForceReleaseBall);
            HumanGloveController.onHumanBallReleaseIntoHand.AddListener(OnHumanMovedBallFromGloveToHand);


            CustomThrowable.OnBallThrown += RemoveBallFromHand;

            glove.ShowHide(false);
            glove.SetCatchingEnabled(false);
        }
        else
        {
            if (HandManager.instance)
            {
                HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.GLOVE, false);
                //HandManager.instance.SetHandEquipmentActive(HandManager.HandGear.CANNON, false);
            }


            HumanGloveController.onHumanBallCatch.RemoveListener(glove.ForceCatchBall);
            HumanGloveController.onHumanBallRelease.RemoveListener(glove.ForceReleaseBall);
            HumanGloveController.onHumanBallReleaseIntoHand.RemoveListener(OnHumanMovedBallFromGloveToHand);

            CustomThrowable.OnBallThrown -= RemoveBallFromHand;

            //if I have a ball, force human glove to release it so it belongs to me only, and recenter it in my glove
            HumanGloveController humanGlove = HandManager.instance.glove;

            if (glove.heldBall && humanGlove)
            {
                humanGlove.ReleaseBall();
                glove.CenterHeldBallInGlove();
            }

            glove.ShowHide(true);
            glove.SetCatchingEnabled(true);
        }

        if (isPosessed)
        {
            if (fieldPosition == DefenseFieldPosition.CATCHER)
            {
                HumanGearManager.instance.ShowCatcherGear();
            }
            else
            {
                HumanGearManager.instance.ShowDefenderGear();
            }
        }
    }

    public void SetPersonalSpaceRadius(float radius)
    {
        personalSpaceCollider.radius = radius;
    }


    #endregion

    public void SetFieldPosition(DefenseFieldPosition pos, Vector3 worldPos)
    {


        DefenseFieldPosition old_pos = fieldPosition;

        switch (old_pos)
        {
            case DefenseFieldPosition.CATCHER:
                EquipGear(FieldPlayerGear.CATCHER_FACE, false);
                EquipGear(FieldPlayerGear.CATCHER_CHEST, false);
                EquipGear(FieldPlayerGear.BACKWARDS_CAP, false);
                break;
        }

        switch (pos)
        {
            case DefenseFieldPosition.CATCHER:
                EquipGear(FieldPlayerGear.CATCHER_FACE, true);
                EquipGear(FieldPlayerGear.CATCHER_CHEST, true);
                EquipGear(FieldPlayerGear.BACKWARDS_CAP, true);
                EquipGear(FieldPlayerGear.CAP, false);
                bodyControl.SetAnimationFloat("TeamRole", 3f);
                glove.catcherGlove = true;
                break;
            default:
                bodyControl.SetAnimationFloat("TeamRole", UnityEngine.Random.Range(1, 3));
                bodyControl.SetAnimationFloat("CycleOffset", UnityEngine.Random.value);
                glove.catcherGlove = false;
                break;
        }
        fieldPosition = pos;
        fieldWorldPosition = worldPos;
    }

    #region OutfieldCoordinator_Handles

    public void BackupTarget(Transform target)
    {
        if (backupTarget == this.transform) {
            Debug.LogWarning(this.name + " Is trying to back himself up");
            SetPlayerAction(FieldPlayerAction.IDLE);
        }
        backupTarget = target;
        SetLookTargetPoint(target.position);
        SetPlayerAction(FieldPlayerAction.BACKUP);
    }

    public void PursueBall(hittable ball)
    {
        if (ball)
        {
            trackedBall = ball;
            trackedRigidbody = ball.GetRigidbody();
        }
    }



    public void StopTrackingBall()
    {
        trackedBall = null;
        trackedRigidbody = null;
    }

    public void PursueRunner(OffensePlayer runner)
    {
        trackedRunner = runner;
        runner.OnOut += StopTrackingOutRunner;
    }

    void StopTrackingOutRunner(OffensePlayer runner, OutType outT)
    {
        StopTrackingRunner();
    }

    public void StopTrackingRunner()
    {
        if (trackedRunner == null) return;

        trackedRunner.OnOut -= StopTrackingOutRunner;
        trackedRunner = null;

    }

    public void CatchBallAtPosition(Vector3 catchPosition, hittable ball, bool uncertainPosition = false)
    {
        //Debug.Log("CatchBallAtPosition " + catchPosition);
        FieldMonitor.DrawDebugLine(catchPosition, catchPosition + Vector3.up * 10, Color.red, 5);

        PursueBall(ball);


        applyCatchPositionNoise = uncertainPosition;
        SetMoveTargetPosition(catchPosition);

        SetPlayerAction(FieldPlayerAction.CATCH);
        //FieldManager.DrawDebugLine(catchPosition, catchPosition + Vector3.up * 5, Color.yellow, 5);


    }

    public void CatchBallAtBase(int baseNo, hittable ball)
    {
        Debug.Log(this.name + " catch ball at base " + baseNo);
        CatchBallAtPosition(FieldMonitor.Instance.GetBaseDefensePosition(baseNo), ball);
    }

    public void ChaseGroundBall(hittable ball)
    {
        PursueBall(ball);
        SetPlayerAction(FieldPlayerAction.CHASE);
    }

    public void AttemptToTagRunner(OffensePlayer runner)
    {
        PursueRunner(runner);
        SetPlayerAction(FieldPlayerAction.TAG);

    }

    protected void FollowBallWithGlove(Vector3 ballPos)
    {
        if (bodyControl)
        {
            SetLookTargetPoint(ballPos);
            bodyControl.FollowBallWithGlove(ballPos);
        }
    }

    private BallGate overrideStrikeGate;
    public void OverrideStrikeGate(BallGate gate)
    {
        overrideStrikeGate = gate;
    }

    public void PitchBall(Vector3 pitchTarget)
    {
        ThrowBall(pitchTarget, isPitching:true);
        if (FieldMonitor.Instance != null)
        {
            FieldMonitor.Instance.AnnouncePitch();
        }
    }

    public void PitchBall()
    {

        Vector3 target = Vector3.zero;
        if (overrideStrikeGate != null)
        {
            target = overrideStrikeGate.GetValidStrikePosition();
        }

        else if (FieldMonitor.Instance)
        {
            target = FieldMonitor.Instance.GetPitchTarget(info.pitchStrikeChance);
        }
        PitchBall(target);
        

    }

    public void AIMoveBallFromGloveToHand()
    {
        if (glove.heldBall != null)
        {
            PutBallInRightHand(glove.ReleaseBall(true));
        }
    }

    public void PutBallInRightHand(hittable ball)
    {
            ballInHand = ball;
            ballInHand.GetRigidbody().isKinematic = true;
            ballInHand.transform.SetParent(bodyControl.rightHandRoot);
        // ballInHand.transform.position = bodyControl.rightHandRoot.position + (ballInHand.transform.position - bodyControl.rightHandRoot.position).normalized* 0f;
        ballInHand.transform.localPosition = Vector3.right * .05f;
    }

    protected void OnHumanMovedBallFromGloveToHand(hittable ball)
    {
        glove.ReleaseBallIntoHand(ball);
        ballInHand = ball;
    }

    public override void RunToBase(int baseNo)
    {
        RunToTarget(FieldMonitor.Instance.GetBaseDefensePosition(baseNo));
    }


    public override float GetTimeToRunToBase(int baseNo)
    {
        return GetTimeToRunToPosition(FieldMonitor.Instance.GetBaseDefensePosition(baseNo));
    }



    [HideInInspector]
    bool useSavedThrowTarget;

    private bool throwWindupInProgress;

    [HideInInspector]
    Vector3 savedThrowTarget;

    [HideInInspector]
    DefensePlayer savedThrowReciever;

    /// <summary>
    /// Used if we want the ball thrown with no designated receiver
    /// </summary>
    public void BeginThrow(Vector3 targetPosition, bool throwToChestHeight = true)
    {
        if (heldBall == null) return;
        if (throwToChestHeight) targetPosition.y = chestHeight;
        savedThrowTarget = targetPosition;
        useSavedThrowTarget = true;
        savedThrowReciever = null;
        BeginThrowAnimation(targetPosition, Vector3.Distance(position, targetPosition));

    }

    /// <summary>
    /// Used if we want the ball trajectory to be calculated at the moment of release to intercept receiver (eg. Pitcher returning to mound)
    /// </summary>
    /// <param name="receiver"></param>
    public void BeginThrow(DefensePlayer receiver)
    {
        if (heldBall == null) return;
        savedThrowReciever = receiver;
        useSavedThrowTarget = false;
        Vector3 lookTarget = receiver.position;
        lookTarget.y = chestHeight;
        BeginThrowAnimation(lookTarget, Vector3.Distance(position, receiver.position));
    }

    /// <summary>
    /// Used if we want the ball to be caught at a specific location by the reciver (eg. At a base)
    /// </summary>
    /// <param name="receiver"></param>
    /// <param name="targetPosition"></param>
    public void BeginThrow(DefensePlayer receiver, Vector3 targetPosition)
    {
//        Debug.Log("Throw " + receiver + " " + targetPosition);
        if (heldBall == null) return;
        targetPosition.y = chestHeight;
        savedThrowTarget = targetPosition;
        useSavedThrowTarget = true;
        savedThrowReciever = receiver;
        BeginThrowAnimation(targetPosition, Vector3.Distance(position, targetPosition));

    }

    
    /// <summary>
    /// Used to prevent more instructions
    /// </summary>
    public void BeginWindup()
    {
        throwWindupInProgress = true;
    }

    private void BeginThrowAnimation(Vector3 look, float distance)
    {
//        Debug.Log(this.name + " Begin Throw Animation ");

        throwWindupInProgress = true;
        bodyControl.SetAnimationFloat("ThrowType", distance < 10? 9 : UnityEngine.Random.Range(0, numOfThrowAnimations));
        bodyControl.SetAnimationTrigger("throw");
        SetLookTargetPoint(look);
        LockLookTarget(true);
        SetLookMode(LookMode.POINT);
    }

    public void CancelThrowAnimation()
    {
        if (throwWindupInProgress)
        {
            throwWindupInProgress = false;
            bodyControl.SetAnimationTrigger("walk");
            LockLookTarget(false);
            SetLookMode(LookMode.FORWARD);
        }

    }


    /// <summary>
    /// Find interception point within receiver's path of travel and starts a throw. Assumes calculations were already made to rule out other actions.
    /// </summary>
    /// <param name="to"></param>
    private void ThrowBall(DefensePlayer to)
    {
        //calculate interception point
        Vector3 target = to.position;

        AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception = FieldMonitor.Instance.FindPassingInterceptionPoint(this, to);
        float time_RecieverToTarget = to.GetTimeToRunToPosition(to.moveTargetPosition);
        if (interception.value > 0 && interception.value < time_RecieverToTarget)
        {
            target = interception.position;

            //Vector3 catchPosition = interception.position;
            //PassBetweenPlayers(from, to, catchPosition);
        }
        else
        {
            target = to.moveTargetPosition;
        }

        target.y = chestHeight;

        ThrowBall(to, target);
    }

    private void ThrowBall(DefensePlayer receiver, Vector3 target)
    {
        if (OnPassThrow != null) OnPassThrow.Invoke(this, receiver, target, ThrowBall(target));
        
    }

    private hittable ThrowBall(Vector3 targetPosition, bool isPitching = false)
    {
        hittable ballToThrow = heldBall;
        glove.ReleaseBall();
        ballInHand = null;
       
        if(ballToThrow == null)
        {
            Debug.Log("JUST TRIED TO THROW A BALL THAT DOESN'T EXIST.");
            return ballToThrow;
        }

        ballToThrow.SetHeldByPlayer(false);

        ballToThrow.hitGround = false;

        ballToThrow.transform.SetParent(transform.parent);

        Rigidbody body = ballToThrow.GetRigidbody();
        body.isKinematic = false;

        body.velocity = InitialVelocityToThrowToPoint(ballToThrow, (targetPosition),isPitching);
        SetPlayerAction(FieldPlayerAction.IDLE);
        //highlight.SetColor(FieldManager.defenseColor);
        LockLookTarget(false);
        if (OnThrow != null) OnThrow.Invoke(this);

        return ballToThrow;
    }


    /// <summary>
    /// Called by FieldPlayerAnimationEventHandler to trigger release of the ball in the middle of the throw animation
    /// Uses saved info to derive throwing intention (to player, to base, etc.)
    /// </summary>
    public void ThrowBall_AnimDelayed()
    {
        throwWindupInProgress = false;

        if (heldBall == null) return;

        if (useSavedThrowTarget && savedThrowReciever != null)
        {
            ThrowBall(savedThrowReciever, savedThrowTarget);
        }
        else if(!useSavedThrowTarget && savedThrowReciever != null)
        {
            ThrowBall(savedThrowReciever);
        }
        else if(useSavedThrowTarget && savedThrowReciever == null)
        {
            ThrowBall(savedThrowTarget);
        }
        else
        {
            //this should never happen
            //ThrowBall(Vector3.zero);
        }
    }

    protected void RemoveBallFromHand(hittable ball)
    {
        if(ballInHand != null && ballInHand == ball)
        {
            ballInHand = null;
            ball.SetHeldByPlayer(false);
        }
    }

    public void ShootBallFromCannonTo(DefensePlayer receiver, Vector3 pos)
    {
        //highlight.SetColor(FieldManager.defenseColor);

        StartCoroutine(ShootBallRoutine(receiver, pos));
    }

    IEnumerator ShootBallRoutine(DefensePlayer receiver, Vector3 pos)
    {
        //		Debug.Log ("ShootBallRoutine");
        if (cannon)
        {
            //			Debug.Log ("ShootBallRoutine02");
            cannon.enabled = true;
            cannon.PrepareForUse();


            Vector3 catchPosition = pos;

            hittable ball = glove.ReleaseBall();
            cannon.ForceLoadBall(ball);



            receiver.CatchBallAtPosition((catchPosition), ball);
            DefenseCoordinator.Instance.SetBallControlPlayer(receiver);


            while (!cannon.IsReadyToFire())
            {
                //				Debug.Log ("waiting for appearance.");
                yield return null;
            }
            cannon.ShootBallAtTarget(catchPosition, throwSpeed);




            gloveRoot.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Initial velocity need to throw a ball to a point
    /// </summary>
    /// <param name="ballToThrow"></param>
    /// <param name="targetPosition">Position in local space</param>
    /// <returns></returns>
    protected Vector3 InitialVelocityToThrowToPoint(hittable ballToThrow, Vector3 targetPosition, bool isPitching = false)
    {
        Rigidbody body = ballToThrow.GetRigidbody();
        float throwDistance = Vector3.Distance(targetPosition, this.position);
        return Trajectory.InitialVelocityNeededToHitPoint(body, (targetPosition), throwDistance / (isPitching? pitchSpeed: throwSpeed));
    }



    public float GetTimeToThrowToBase(int baseNo)
    {
        //Debug.DrawLine(this.transform.position,FieldManager.Instance.GetBasePosition(baseNo),Color.white);
        return GetTimeToThrowToPosition(FieldMonitor.Instance.GetBaseDefensePosition(baseNo));
    }

    public override void RunToTarget(Transform trans)
    {
        //        Debug.Log(this.name + " run to target " + trans.name + "   - "  +FieldManager.Instance.GetHomePlate().name);
        Vector3 localSpace_offset = Vector3.zero;

        //have the catcher stand just behind home plate instead of on it
        if (fieldPosition == DefenseFieldPosition.CATCHER && trans == FieldMonitor.Instance.GetHomePlate().transform)
        {
            localSpace_offset = Vector3.back * 1.5f;
        }

        RunToTarget((trans.position) + localSpace_offset);

    }

    public float GetTimeToThrowToPosition(Vector3 position)
    {
        return GetTimeToThrowToPosition(position, this.position);
    }

    public float GetTimeToThrowToPosition(Vector3 position, Vector3 start)
    {
        return Vector3.Distance(start, position) / throwSpeed + throwWindupDuration;
    }

    public hittable DropBall()
    {
        if (heldBall == null) return null;
        hittable eat = glove.ReleaseBall();
        //highlight.SetColor(FieldManager.defenseColor);
        return eat;
    }

    #endregion



    #region Catching Logic
    public void HandleCaughtBall(hittable ball)
    {
#if UNITY_EDITOR
        //        Debug.Log(this.name + " Handle Ball");
#endif
        throwWindupInProgress = false;
        StopTrackingBall();
        //highlight.SetColor(FieldManager.ballColor);
        if(state == FieldPlayerAction.CATCH || state == FieldPlayerAction.CHASE)
            SetPlayerAction(FieldPlayerAction.IDLE);

        //		Debug.Log (this.fieldPosition + " Handle Ball"); 
        if (OnBallPosession != null) OnBallPosession.Invoke(this);

        RaycastHit hit;
        if (Physics.Raycast(ball.transform.position + (Vector3.up * 0.1f), Vector3.down, out hit, 1f, LayerMask.GetMask("Ground")))
        {
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("ball_scoop", transform.position);
        }
    }

    void HandleFumble(hittable ball)
    {
        //TryToGetBall(ball);
        //Debug.Log("fumble " + state);
        if (OnFumble != null) OnFumble.Invoke(this);
        bodyControl.SetAnimationTrigger("fumble");

    }

    //A missed catch is not necessarily a fumble
    void HandleMissedCatch(hittable ball)
    {
        //Debug.Log("HANDLE MISSED CATCH");
        //Debug.Log(this.name + "MISSED a catch");
        //TryToGetBall(trackedBall);
        if (OnMissedCatch != null)
        {
            OnMissedCatch.Invoke(this);
        }
        //else SetPlayerAction(FieldPlayerAction.CHASE);
        SetPlayerAction(FieldPlayerAction.IDLE);
    }


    bool RigidbodyIsMovingCloserToPoint(Rigidbody body, Vector3 localPoint)
    {
        Vector3 currentPos = (body.position);
        currentPos.y = 0;// WorldScaler.elevation;
        Vector3 lastPos = (body.position - body.velocity * Time.fixedDeltaTime);
        lastPos.y = 0;// WorldScaler.elevation;

        //        Debug.Log(Vector3.Distance(localPoint, nextPos) + "  " + Vector3.Distance(localPoint, currentPos));

        if (Vector3.Distance(localPoint, lastPos) > Vector3.Distance(localPoint, currentPos))
        {
            return true;
        }
        return false;
    }


    public AnimationCurve catchingWeights;

    Vector3[] GetPointsForDirectionalWeights(int numPoints)
    {
        List<Vector3> points = new List<Vector3>();
        for (float r = 0f; r < 2 * Mathf.PI; r += (2 * Mathf.PI) / (float)numPoints) {
            Vector3 dirVector = new Vector3(Mathf.Cos(r), 0, Mathf.Sin(r));
            dirVector *= GetWeightInDirection(Vector3.zero, transform.position + dirVector) * 3f;
            Vector3 point = new Vector3(transform.position.x + dirVector.x, transform.position.y, transform.position.z + dirVector.z);
            points.Add(point);
            if (points.Count > 1) points.Add(point);
        }
        if (points.Count % 2 != 0) points.Add(points[points.Count - 1]);
        return points.ToArray();
    }


    /// <summary>
    /// Gets this player's weight for deciding who is best positioned to catch a ball
    /// Assumes running backwards is less desirable than running forwards
    /// </summary>
    /// <param name="ballOrigin">Position ball is coming from</param>
    /// <param name="interceptionPoint">Position this player would be able to intercept</param>
    /// <returns></returns>
    public float GetWeightInDirection(Vector3 ballOrigin, Vector3 interceptionPoint)
    {

        ballOrigin.y = 0;
        interceptionPoint.y = 0;
        Vector3 myPos = transform.position;
        myPos.y = 0;

        float angle = Vector3.Angle(ballOrigin-myPos, interceptionPoint-myPos);
        return catchingWeights.Evaluate(Mathf.Deg2Rad * angle / Mathf.PI);

    }

    protected void LookTowardsBallInPlay()
    {
        if (FieldMonitor.ballInPlay != null && !inPosessionOfBall)
        {
            SetLookTargetTransform(FieldMonitor.ballInPlay.transform);
            SetLookMode(LookMode.TRANSFORM);
        }
        else
        {
            SetLookTargetPoint(Vector3.zero);
            SetLookMode(LookMode.POINT);
        }
    }


    #endregion


    #region StateMachine Logic
    void IDLE_Enter()
    {
        //face home plate
        //transform.LookAt(FieldManager.LocalToWorldPosition(Vector3.zero));// FieldManager.Instance.GetHomePlatePosition()));
        
    }

    void IDLE_Update()
    {
        LookTowardsBallInPlay();
    }

    void IDLE_Exit()
    {
        atPitchPosition = false;
    }

    void CATCH_Enter()
    {
        SetLookMode(LookMode.POINT);
        glove.SetCatchingEnabled(true);
        
    }

    void CATCH_Update()
    {
        //LookTowardsTargetPosition();
    }

    void CATCH_FixedUpdate()
    {
        if (applyCatchPositionNoise)
        {
            Vector3 offset = new Vector3(Mathf.PerlinNoise(moveTargetPosition.x, Time.time * 2), 0, (Mathf.PerlinNoise(moveTargetPosition.y, Time.time * 2))) * 1;
            MoveTowardsTargetPosition(offset);
        }
        else
        {
            MoveTowardsTargetPosition();
        }

    }

    void CATCH_LateUpdate()
    {
        if (trackedRigidbody != null)
        {
            FollowBallWithGlove(trackedRigidbody.position);

            Vector3 ballPositionFlat = (trackedRigidbody.position); ballPositionFlat.y = 0;// WorldScaler.elevation;
            FieldMonitor.DrawDebugLine(ballPositionFlat, moveTargetPosition, Color.grey);
            FieldMonitor.DrawDebugLine(this.position, moveTargetPosition, Color.cyan);

            //If the ball is somehow moving away from this outfielder, chase it
            if (!RigidbodyIsMovingCloserToPoint(trackedRigidbody, moveTargetPosition) || trackedRigidbody.position.y < .06f)
            {
                //Debug.Log(this.name + " missed a catch!");

                HandleMissedCatch(trackedBall);

                //fsm.ChangeState(FieldPlayerState.CHASE);
            }
        }
        else
        {
            //fsm.ChangeState(FieldPlayerState.RESET);
        }
    }

    void CATCH_Exit()
    {
        glove.SetCatchingEnabled(false);

    }

    void CHASE_Enter()
    {
        SetLookMode(LookMode.POINT);
            glove.SetCatchingEnabled(true);
        
    }

    void CHASE_Update()
    {
        if (trackedRigidbody != null)
        {
            Vector3 ballPosition = (trackedRigidbody.position);
            SetLookTargetPoint(ballPosition);
            //LookTowardsTargetPosition();
        }
    }

    void CHASE_FixedUpdate()
    {
        if (trackedRigidbody != null)
        {
            Vector3 ballPosition = (trackedRigidbody.position);
            Vector3 ballVelocity = (trackedRigidbody.velocity);

            SetMoveTargetPosition(FieldMonitor.Instance.EstimatedGroundInterceptionPoint(ballPosition, ballVelocity, this));

            Vector3 closestPointOnBallPath = Vector3.Dot(position - ballPosition, ballVelocity.normalized) * ballVelocity.normalized + ballPosition;
            MoveTowardsTargetPosition();

            FieldMonitor.DrawDebugLine(new Vector3(ballPosition.x, 0, ballPosition.z), moveTargetPosition, Color.grey, Time.fixedDeltaTime);

        }
    }

    void CHASE_LateUpdate()
    {
        if (trackedRigidbody != null)
        {
            FollowBallWithGlove(trackedRigidbody.position);

            FieldMonitor.DrawDebugLine(position, moveTargetPosition, Color.magenta);
        }
        else
        {
            SetPlayerAction(FieldPlayerAction.RESET);
        }
    }

    void CHASE_Exit()
    {
        glove.SetCatchingEnabled(false);

    }

    void BACKUP_Enter()
    {
        SetLookMode(LookMode.POINT);
    }

    void BACKUP_Update()
    {
        //LookTowardsTargetPosition();

    }

    void BACKUP_FixedUpdate()
    {
        if (backupTarget != null && FieldMonitor.ballInPlay != null)
        {
            Vector3 backupTargetPosition = (backupTarget.position);
            Vector3 ballPosition = (FieldMonitor.ballInPlay.transform.position);

            Vector3 ballToBackup = (backupTargetPosition - ballPosition); ballToBackup.y = 0;

            if (ballToBackup.magnitude > .1f)
            {
                SetMoveTargetPosition(backupTargetPosition + (ballToBackup).normalized * backup_buffer);
            }


            MoveTowardsTargetPosition();

            FieldMonitor.DrawDebugLine(position, moveTargetPosition, Color.grey, Time.fixedDeltaTime);

        }
        else
        {
            SetPlayerAction(FieldPlayerAction.RESET);
        }
    }

    void BACKUP_Exit()
    {

    }


    void TAG_Enter() {
        SetLookMode(LookMode.FORWARD);
        glove.SetCatchingEnabled(true);
        
    }

    void TAG_Update()
    {
    }

    void TAG_FixedUpdate()
    {
        if (trackedRunner != null)
        {     
            Vector3 runnerVelocity = (FieldMonitor.Instance.GetBaseDefensePosition(trackedRunner.baseTarget_Immediate) - trackedRunner.position).normalized * trackedRunner.runSpeed;
            SetMoveTargetPosition(FieldMonitor.Instance.EstimatedGroundInterceptionPoint(trackedRunner.position, runnerVelocity, this));
            //Debug.Log("tag move toward target from " + position);
            MoveTowardsTargetPosition();
            //Debug.Log("tag move toward target at " + position);
        }

    }

    void TAG_LateUpdate()
    {
        if (trackedRunner != null)
        {
            FollowBallWithGlove(trackedRunner.transform.position);
        }
        else
        {
            SetPlayerAction(FieldPlayerAction.RESET);
        }
    }

    void TAG_Exit()
    {
        glove.SetCatchingEnabled(false);

    }

    void RUN_Enter()
    {
        SetLookMode(LookMode.FORWARD);
    }

    void RUN_FixedUpdate()
    {

        MoveTowardsTargetPosition();

    }

    void RUN_Update()
    {

        //LookTowardsTargetPosition();

        if (Vector3.Distance(position, moveTargetPosition) < (.1f))
        {
            //fsm.ChangeState(FieldPlayerAction.IDLE);
        }
    }

    void RUN_Exit()
    {

    }


    void RESET_Enter()
    {
        SetLookMode(LookMode.FORWARD);
        glove.SetCatchingEnabled(false);
        trackedRigidbody = null;
        SetMoveTargetPosition(fieldWorldPosition);
    }

    void RESET_FixedUpdate()
    {
        MoveTowardsTargetPosition();
    }

    void RESET_Update()
    {

        if (Vector3.Distance(position, moveTargetPosition) < (.5f))
        {
            atPitchPosition = true;
            SetPlayerAction(FieldPlayerAction.IDLE);
        }
        else
        {
            //			Debug.Log ("player name "+gameObject.name+" reset update "+Vector3.Distance(position, targetPosition));
            //			Debug.Log ("from "+position+" to "+targetPosition);
        }
    }





    #endregion



    /// <summary>
    /// Attempt to catch the ball if possible and chase otherwise
    /// </summary>
    /// <param name="ball"></param>
    /// <returns>If this player will be able to catch the ball in-air</returns>
    bool TryToGetBall(hittable ball)
    {
        if (ball != null)
        {
            AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception;
            if (FieldMonitor.Instance.CanCatchInAir(ball, this, out interception))
            {
                CatchBallAtPosition(interception.position, ball);
                return true;
            }
            else
            {
                ChaseGroundBall(ball);
            }
        }
        return false;
    }

    public bool CatchBallBehindHomePlate(hittable ball)
    {
        Vector3 catcherPos = Vector3.back*2;
        if(DefenseCoordinator.Instance)
            catcherPos = DefenseCoordinator.Instance.GetFieldPosition(DefenseFieldPosition.CATCHER);

        if (ball != null)
        {
            Vector3 catchPos;
            AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception;
            if (FieldMonitor.Instance.CanCatchInAir(ball, this, out interception))
            {
                catchPos = interception.position;
//                Debug.Log("Air catch " + catchPos);
            }
            else
            {
                TrajectoryInfo info = ball.GetTrajectory();
                Vector3 groundHitPos = info.groundHitPosition;
                float t;
                if(Trajectory.GetPointOfPassingTarget(ball.GetRigidbody(), ball.spotThrownFrom, catcherPos, out catchPos, out t))
                {
                    //the ball is going over the catcher's head
                    if (catchPos.y > 0) return false;
                    //the ball will take more than 3 seconds to reach catcher
                    if (t > 3) return false;
                    catchPos.y = 0;
                    Debug.Log("Pass catch " + catchPos);

                }
                else
                {
                    catchPos = groundHitPos;
                }

            }

            if (catchPos.z > catcherPos.z) catchPos.z = catcherPos.z;


//            Debug.Log("Catcher catch " + FieldMonitor.BallInPlay +" "+ Vector3.Distance(catchPos, catcherPos));

            if (Vector3.Distance(catchPos, catcherPos) < 5)
            {
                CatchBallAtPosition(catchPos, ball);
                return true;
            }
        }
        return false;
    }

    

}
