﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum FieldPlayerGear
{
    CATCHER_FACE,
    CATCHER_CHEST,
    BACKWARDS_CAP,
    HELMET,
    CAP,
    JERSEY,
    HEAD,
}

[RequireComponent(typeof(Animator))]
public class FieldPlayerBodyControl : MonoBehaviour {

    protected Animator animator
    {
        get
        {
            if (m_animator == null) m_animator = GetComponent<Animator>();
            return m_animator;
        }
    }
    private Animator m_animator;

    [Header("Transforms")]
    [SerializeField] protected Transform headRoot;
    [SerializeField] protected Transform neckPivot;
    [SerializeField] protected Transform lowerBodyRoot;

    [SerializeField] protected Transform jersey;
    [SerializeField] protected Transform leftShoulderRoot;
    [SerializeField] protected Transform rightShoulderRoot;
    [SerializeField] protected Transform leftShoulderRoot_Neutral;
    [SerializeField] protected Transform rightShoulderRoot_Neutral;

    [SerializeField] public Transform leftHandRoot;
    [SerializeField] public Transform rightHandRoot;
    [SerializeField] protected Transform torso;
    [SerializeField] protected Transform gloveRoot;

    [Header("GameObjects")]
    [SerializeField] protected GameObject head;
    [SerializeField] protected GameObject eyes;
    [SerializeField] protected GameObject mouth;
    [SerializeField] protected GameObject cap;
    [SerializeField] protected GameObject backwardsCap;
    [SerializeField] protected GameObject helmet;
    [SerializeField] protected GameObject hair;
    [SerializeField] protected GameObject leftHandVisual;
    [SerializeField] protected GameObject rightHandVisual;

    [Header("Animators")]
    [SerializeField] protected Animator eyeAnimator;
    [SerializeField] public Animator mouthAnimator;
    [SerializeField] protected Sprite restBoyMouth;
    [SerializeField] protected Sprite restBoyEyes;
    [SerializeField] protected Sprite restManMouth;
    [SerializeField] protected Sprite restManEyes;

    [Header("MeshRenderers")]
    [SerializeField] protected SpriteRenderer jerseyNameNumber;
    protected MeshRenderer jerseyRenderer;
    protected MeshRenderer leftSleeveRenderer;
    protected MeshRenderer rightSleeveRenderer;
    protected MeshRenderer capRenderer;
    protected MeshRenderer backwardsCapRenderer;
    protected MeshRenderer hairRenderer;
    protected MeshRenderer headRenderer;

    protected SpriteRenderer eyeSpriteRenderer;
    protected SpriteRenderer mouthSpriteRenderer;

    protected MeshFilter hairFilter;
    protected MeshFilter headFitler;

    protected bool isOneToOne;
    protected bool lockFeet;

    [Header("Prefabs")]
    public GameObject catcherMaskPrefab;
    public GameObject catcherVestPrefab;

    protected Renderer[] allRenderers;

    [Header("Options")]
    public float armLength = 1;

    protected Transform rootFollowTransform;//usually main camera
    protected Transform headFollowTransform;//usually main camera
    protected Transform rootTransform;

    public AvatarMeshFeature hairOptions;

    private MaterialPropertyBlock matPropertyBlock;


    public bool acceptDeviceInput
    {
        get
        {
            return _acceptDeviceInput;
        }
        set
        {
            _acceptDeviceInput = value;
        }
    }
    private bool _acceptDeviceInput;

    [Space(10)]
    protected Transform _controller;
    protected Camera _camera;

    private GameObject catchMask
    {
        get
        {
            if(m_catchMask == null)
            {
                m_catchMask = GameObject.Instantiate(catcherMaskPrefab);
                m_catchMask.transform.SetParent(headRoot, false);
                m_catchMask.transform.localPosition = Vector3.zero;
                m_catchMask.transform.localRotation = Quaternion.identity;
                m_catchMask.SetActive(false);
            }
            return m_catchMask;
        }
    }
    private GameObject m_catchMask;
    private GameObject catchVest
    {
        get
        {
            if(m_catchVest == null)
            {
                m_catchVest = GameObject.Instantiate(catcherVestPrefab);
                m_catchVest.transform.SetParent(torso, false);
                m_catchVest.transform.localPosition = Vector3.zero;
                m_catchVest.transform.localRotation = Quaternion.identity;
                m_catchVest.SetActive(false);
            }
            return m_catchVest;
        }
    }
    private GameObject m_catchVest;

    protected CustomHand leftHand;
    protected CustomHand rightHand;

    public Vector3 leftHandOffset;
    public Vector3 rightHandOffset;
#if STEAM_VR
    public Vector3 leftHandOffsetVive;
    public Vector3 rightHandOffsetVive;
#elif RIFT
    public Vector3 leftHandOffsetOculus;
    public Vector3 rightHandOffsetOculus;
#endif

    private ParticleSystem[] particleSystems;

    protected virtual void Awake()
    {


#if STEAM_VR

#elif RIFT
        leftHandOffset = leftHandOffsetOculus;
        rightHandOffset = rightHandOffsetOculus;
#endif

        rootTransform = transform.parent;
        if (rootTransform)
        {
            particleSystems = rootTransform.GetComponentsInChildren<ParticleSystem>();
        }
        GetAllRenderers();

        if (jersey)
        {
            jerseyRenderer = jersey.GetComponent<MeshRenderer>();
        }
        if (leftShoulderRoot)
        {
            leftSleeveRenderer = leftShoulderRoot.GetComponent<MeshRenderer>();
        }
        if (rightShoulderRoot)
        {
            rightSleeveRenderer = rightShoulderRoot.GetComponent<MeshRenderer>();
        }
        if (cap)
        {
            capRenderer = cap.GetComponent<MeshRenderer>();
        }
        if (backwardsCap)
        {
            backwardsCapRenderer = backwardsCap.GetComponent<MeshRenderer>();
        }
        if (head)
        {
            headRenderer = head.GetComponent<MeshRenderer>();
        }
        if (eyes)
        {
            eyeSpriteRenderer = eyes.GetComponent<SpriteRenderer>();
        }
        if (mouth)
        {
            mouthSpriteRenderer = mouth.GetComponent<SpriteRenderer>();
        }

        if (hair)
        {
            hairRenderer = hair.GetComponent<MeshRenderer>();
            hairFilter = hair.GetComponent<MeshFilter>();
        }
}


    void Start()
    {
        rootFollowTransform = Camera.main.transform;
        headFollowTransform = Camera.main.transform;

    }

    void OnEnable()
    {
        if(particleSystems!=null && particleSystems.Length>0)
        {
            SetParticlesEnabled(true);
        }
    }


    protected void LateUpdate()
    {
        if (acceptDeviceInput)
        {
            MatchPlayerMovement();
        }

        SimpleShoulderIK();
    }

    protected void GetAllRenderers()
    {
        allRenderers = GetComponentsInChildren<Renderer>();

    }

    public void SetRootFollow(Transform follow)
    {
        if (follow == null) rootFollowTransform = Camera.main.transform;
        else rootFollowTransform = follow;
    }

    public void SetHeadFollow(Transform follow)
    {
        if (follow == null) headFollowTransform = Camera.main.transform;
        else headFollowTransform = follow;
    }

    /// <summary>
    /// Used to prevent room-scale player movement from moving the body control. Used for locomotion B on rails
    /// </summary>
    /// <param name="locked"></param>
    public virtual void LockFeetAtCenter(bool locked)
    {
        lockFeet = locked;

    }

    protected virtual void MatchPlayerMovement()
    {
        MatchPositionToFollow();
        MatchHeadToCamera();
        MatchHandsToControllers();
        SimpleTorsoIK();
    }

    protected virtual void MatchHandsToControllers()
    {
        if (gloveRoot) { gloveRoot.localPosition = Vector3.zero; gloveRoot.localRotation = Quaternion.identity; }

        if (HandManager.currentLeftCustomHand)
        {
            leftHand = (HandManager.dominantHand == HandManager.HandType.right? HandManager.currentLeftCustomHand : HandManager.currentRightCustomHand);
            Vector3 tempOffset = HandManager.dominantHand == HandManager.HandType.right ? leftHandOffset : rightHandOffset;
            leftHandRoot.SetPositionAndRotation(leftHand.transform.position + leftHand.transform.TransformVector(tempOffset), leftHand.transform.rotation);
        }
        if (HandManager.currentRightCustomHand)
        {
            rightHand = (HandManager.dominantHand == HandManager.HandType.right ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
            Vector3 tempOffset = HandManager.dominantHand == HandManager.HandType.right ? rightHandOffset : leftHandOffset;
            rightHandRoot.SetPositionAndRotation(rightHand.transform.position + rightHand.transform.TransformVector(tempOffset), rightHand.transform.rotation);

        }

    }

    protected virtual void MatchPositionToFollow()
    {
            Vector3 deltaToFollow = rootFollowTransform.position - rootTransform.position;
            deltaToFollow.y = 0f;

            rootTransform.position += deltaToFollow;
    }

    protected virtual void MatchHeadToCamera()
    {
        if (headFollowTransform == null) SetHeadFollow(null);
        if (headFollowTransform)
        {
            headRoot.transform.SetPositionAndRotation(headFollowTransform.position, headFollowTransform.rotation * Quaternion.Euler(0, 180, 0));
        }
    }

    private void SimpleTorsoIK()
    {
        if (!neckPivot) return;
            torso.position = neckPivot.position - neckPivot.up * .1f;

            Vector3 torsoLookDirection = neckPivot.forward;
            if (torso.forward.y < -.5f) //if looking down, use up for forward
            {
                torsoLookDirection = transform.up;
            }
            torsoLookDirection.y = 0f;

            torso.rotation = Quaternion.LookRotation(torsoLookDirection, Vector3.up);
    }

    protected void SimpleShoulderIK()
    {
        SimpleShoulderIK(leftShoulderRoot, leftShoulderRoot_Neutral, leftHandRoot);
        SimpleShoulderIK(rightShoulderRoot, rightShoulderRoot_Neutral, rightHandRoot);
    }

    private void SimpleShoulderIK(Transform shoulderRoot, Transform shoulderRoot_neutral, Transform handRoot)
    {
        if (shoulderRoot && shoulderRoot_neutral && handRoot)
        {

            float extension = Vector3.Distance(shoulderRoot.position, handRoot.position);
            float follow = extension / armLength;
            follow = follow * follow;

            //Debug.DrawLine(leftShoulderRoot.position, leftHandRoot.position, Color.yellow);
            //Debug.DrawRay(leftShoulderRoot.position, leftShoulderRoot.forward.normalized * armLength, Color.magenta);
            shoulderRoot.rotation =
                Quaternion.Lerp(
                    shoulderRoot_neutral.rotation,
                    Quaternion.LookRotation(handRoot.position - shoulderRoot.position),
                    follow);
        }
    }


#region GEAR

    public void EquipGear(FieldPlayerGear gear, bool equip)
    {
        switch (gear)
        {
            case FieldPlayerGear.CATCHER_FACE:
                if (!equip && m_catchMask == null) break;
                catchMask.SetActive(equip);
                break;
            case FieldPlayerGear.CATCHER_CHEST:
                if (!equip && m_catchVest == null) break;
                catchVest.SetActive(equip);

                break;

            case FieldPlayerGear.BACKWARDS_CAP:
                if (backwardsCap)
                {
                    backwardsCap.SetActive(equip);
                }
                else
                {
                    Debug.LogWarning(gear + " gear is not present.");
                }
                break;
            case FieldPlayerGear.CAP:
                if (cap)
                {
                    cap.SetActive(equip);
                }
                else
                {
                    Debug.LogWarning(gear + " gear is not present.");
                }
                break;
            case FieldPlayerGear.HELMET:
                if (helmet)
                {
                    helmet.SetActive(equip);
                }
                else
                {
                    Debug.LogWarning(gear + " gear is not present.");
                }
                break;
            case FieldPlayerGear.JERSEY:
                if (torso)
                {
                    torso.gameObject.SetActive(equip);
                }
                else
                {
                    Debug.LogWarning(gear + " gear is not present.");
                }
                break;
            case FieldPlayerGear.HEAD:
                if (head)
                {
                    head.SetActive(equip);
                }
                else
                {
                    Debug.LogWarning(gear + " gear is not present.");
                }
                if (eyes)
                {
                    eyes.SetActive(equip);
                }
                if (mouth)
                {
                    mouth.SetActive(equip);
                }
                break;
        }
        
        if(hair && head)
            hair.SetActive(head.activeSelf);
    
        GetAllRenderers();
    }



#endregion

    IEnumerator MatchHeadRoutine()
	{
		float timeRemaining = .1f;
		while (timeRemaining > 0) {
			yield return null;
            Camera.main.transform.parent.position += headRoot.position - Camera.main.transform.position;
			timeRemaining -= Time.deltaTime;
		}
	}

    public void SetHandVisibility(HandManager.HandType hand, bool visible)
    {
        switch (hand)
        {
            case HandManager.HandType.left:
                if(leftHandVisual)
                    leftHandVisual.SetActive(visible);

                break;
            case HandManager.HandType.right:
                if(rightHandVisual)
                    rightHandVisual.SetActive(visible);

                break;
            case HandManager.HandType.both:
                if(leftHandVisual)
                    leftHandVisual.SetActive(visible);
                if(rightHandVisual)
                    rightHandVisual.SetActive(visible);

                break;
        }
    }

    public void SetFeetVisibility(bool visible)
    {
//        Debug.Log("Set feet visibility " + visible);
        if (lowerBodyRoot)
        {
            lowerBodyRoot.gameObject.SetActive(visible);
        }
    }

    public void ParentToHand(Transform obj, HandManager.HandType hand)
    {
        switch (hand)
        {
            case HandManager.HandType.left:
                obj.SetParent(leftHandRoot);
                obj.localPosition = Vector3.zero;
                obj.localRotation = Quaternion.identity;
                break;
            case HandManager.HandType.right:
                obj.SetParent(rightHandRoot);
                obj.localPosition = Vector3.zero;
                obj.localRotation = Quaternion.identity;
                break;
            
        }

    }

    public void PutGloveOnLeftHand(Transform glove)
    {
        glove.SetParent(gloveRoot);
        glove.localPosition = Vector3.zero;
        glove.localRotation = Quaternion.identity;
    }

    public void SetBodyVisible()
    {

    }

    public void FollowBallWithGlove(Vector3 ballPos)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Fumble")) return;
        if (gloveRoot) { gloveRoot.localPosition = Vector3.zero; gloveRoot.localRotation = Quaternion.identity; }
        leftHandRoot.position = leftShoulderRoot.position + (ballPos - leftShoulderRoot.position).normalized * armLength;
        leftHandRoot.LookAt(ballPos);

    }

    protected void LookTowards(Vector3 lookPos)
    {
        //too close to look;
        if (Vector3.Distance(lookPos, rootTransform.position) < .1f) return;
        lookPos.y = 0;
        rootTransform.LookAt(lookPos);
    }

    public void SetParticlesEnabled(bool emit)
    {
        if (particleSystems != null)
        {
            foreach (ParticleSystem sys in particleSystems)
            {
                var e = sys.emission;
                e.enabled = emit;
            }
        }
    }

    public void DisableParticlesForFrame()
    {
        StartCoroutine(DisableParticlesForFrameRoutine());
    }

    IEnumerator DisableParticlesForFrameRoutine()
    {
        SetParticlesEnabled(false);
        yield return null;
        SetParticlesEnabled(true);
    }

    public Material[] GetJerseyMaterials()
    {
        if (torso)
        {
            Renderer rend = torso.GetComponentInChildren<Renderer>();
            if (rend)
            {
                Material[] mat = rend.materials;
                return mat;
            }
        }
        return null;
    }

    public void SetJerseyMaterials(Material[] mats)
    {
        if (mats == null) return;
        if (torso)
        {
            Renderer rend = torso.GetComponentInChildren<Renderer>();
            if (rend)
            {
                rend.materials = mats;
            }
        }
        SetDitherTransparency(ditherVal);
    }


    public void SetGearSpectatorOnly(FieldPlayerGear gear)
    {
        int spectatorLayer = LayerMask.NameToLayer("SpectatorOnly");
        switch (gear)
        {
            case FieldPlayerGear.CAP:
                //CreateShadowCopy(cap);
                cap.gameObject.layer = spectatorLayer;
                break;
            case FieldPlayerGear.CATCHER_CHEST:
                foreach (Renderer rend in catchVest.GetComponentsInChildren<Renderer>())
                {
                    rend.gameObject.layer = spectatorLayer;
                }
                CreateShadowCopy(catchVest);
                break;
            case FieldPlayerGear.CATCHER_FACE:
                foreach (Renderer rend in catchMask.GetComponentsInChildren<Renderer>())
                {
                    rend.gameObject.layer = spectatorLayer;
                }
                CreateShadowCopy(catchVest);
                break;
            case FieldPlayerGear.HEAD:
                CreateShadowCopy(head);
                head.gameObject.layer = spectatorLayer;
                break;
            case FieldPlayerGear.HELMET:
                CreateShadowCopy(helmet);
                helmet.gameObject.layer = spectatorLayer;
                break;
            case FieldPlayerGear.JERSEY:
                CreateShadowCopy(jersey.gameObject);
                jersey.gameObject.layer = spectatorLayer;
                break;
        }
    }

    protected static void CreateShadowCopy(GameObject original)
    {
        if(original.transform.Find("ShadowOnly") != null) return;
        GameObject shadow = GameObject.Instantiate(original, original.transform);
        shadow.SetActive(true);
        shadow.transform.localPosition = Vector3.zero;
        shadow.transform.localRotation = Quaternion.identity;
        shadow.transform.localScale = Vector3.one;
        shadow.name = "ShadowOnly";
        foreach(Renderer rend in shadow.GetComponentsInChildren<Renderer>())
        {
            if(rend.shadowCastingMode == UnityEngine.Rendering.ShadowCastingMode.On)
                rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
            rend.gameObject.layer = 0;
        }
    }

    public void DressAsPlayer(PlayerInfo info)
    {
        if (info == null)
        {
            Debug.Log("Player info was null! Returning!");
            return;
        }

        SetSkinColor(info.skinColor);
        SetHairMesh(info.hairMesh);
        SetHairColor(info.hairColor);

        if (info.isManly)
        {
            if (eyeAnimator != null && mouthAnimator != null)
            {
                eyeAnimator.SetBool("Manly", true);
                mouthAnimator.SetBool("Manly", true);
                ForceManishFace();
            }
        }
        else
        {
            if (eyeAnimator != null && mouthAnimator != null)
            {
                eyeAnimator.SetBool("Manly", false);
                mouthAnimator.SetBool("Manly", false);
                ForceBoyishFace();
            }
        }

        SetDitherTransparency(ditherVal);
    }

    public void DressAsPlayer(PlayerInfo info, TeamInfo tInfo)
    {
        if(jerseyRenderer == null)
        {
            Awake();
        }
        DressAsPlayer(info);

        if (jerseyNameNumber == null)
        {
            Debug.Log("Jersey name number null. Returning.");
            return;
        }
        jerseyNameNumber.sprite = info.jerseyNameNumber;

        gameObject.name = info.jerseyName;

        if (capRenderer != null) capRenderer.material = tInfo.jerseyMaterial;
        if (backwardsCapRenderer != null) backwardsCapRenderer.material = tInfo.jerseyMaterial;
        if (jerseyRenderer != null) jerseyRenderer.material = tInfo.jerseyMaterial;
        if (leftSleeveRenderer != null) leftSleeveRenderer.material = tInfo.jerseyMaterial;
        if (rightSleeveRenderer != null) rightSleeveRenderer.material = tInfo.jerseyMaterial;
        //The material HAS to be set this way. Don't ask me why. Yell at the Unity people.
        if (helmet != null)
        {
            Material[] mats = helmet.GetComponent<MeshRenderer>().materials;
            mats[1] = tInfo.helmetMaterial;
            helmet.GetComponent<MeshRenderer>().materials = mats;
        }
    }

    #region Customization



    private void SetHairMesh(Mesh hairMesh)
    {
        if (hairFilter != null)
        {
            hairFilter.sharedMesh = hairMesh;
        }
    }

    private void SetHairColor(Color hairColor)
    {
        if (hairRenderer != null)
        {
            if (matPropertyBlock == null) matPropertyBlock = new MaterialPropertyBlock();
            hairRenderer.GetPropertyBlock(matPropertyBlock);
            hairColor.a = 1;
            matPropertyBlock.SetColor("_Color", hairColor);
            hairRenderer.SetPropertyBlock(matPropertyBlock);
        }
    }

    private void SetSkinColor(Color skinColor)
    {
        if(headRenderer != null)
        {
            if (matPropertyBlock == null) matPropertyBlock = new MaterialPropertyBlock();
            headRenderer.GetPropertyBlock(matPropertyBlock);
            skinColor.a = 1;
            matPropertyBlock.SetColor("_Color", skinColor);
            headRenderer.SetPropertyBlock(matPropertyBlock);
        }
    }


    #endregion

    public void ForceBoyishFace()
    {
        if (eyeSpriteRenderer) eyeSpriteRenderer.sprite = restBoyEyes;
        if (mouthSpriteRenderer) mouthSpriteRenderer.sprite = restBoyMouth;
    }

    public void ForceManishFace()
    {
        if (eyeSpriteRenderer) eyeSpriteRenderer.sprite = restManEyes;
        if (mouthSpriteRenderer) mouthSpriteRenderer.sprite = restManMouth;
    }

    private float ditherVal = 1;
    public void SetDitherTransparency(float fade)
    {
        if (matPropertyBlock == null) matPropertyBlock = new MaterialPropertyBlock();
        if (allRenderers == null) return;
        fade = Mathf.Clamp01(fade);
        foreach(Renderer rend in allRenderers)
        {
            if (rend == null) continue;
            rend.GetPropertyBlock(matPropertyBlock);
            matPropertyBlock.SetFloat("_DissolveValue", 1 - fade);
            rend.SetPropertyBlock(matPropertyBlock);
        }

        if (eyeSpriteRenderer) eyeSpriteRenderer.enabled = fade > .5f;
        if (mouthSpriteRenderer) mouthSpriteRenderer.enabled = fade > .5f;
        if (jerseyNameNumber) jerseyNameNumber.enabled = fade > .5f;

        ditherVal = fade;
    }

    public void DitherFadeOverTime(float start, float end, float duration, float delay = 0)
    {
        if (this.isActiveAndEnabled)
        {
            StartCoroutine(DitherFadeRoutine(start, end, duration, delay));
        }
    }

    private IEnumerator DitherFadeRoutine(float start, float end, float duration, float delay)
    {
        yield return null;
        GetAllRenderers();
        yield return new WaitForSeconds(delay);
        SetDitherTransparency(start);
        float t = 0;
        while(t< duration)
        {
            SetDitherTransparency(Mathf.Lerp(start, end, t / duration));
            t += Time.deltaTime;

            yield return null;
        }
        SetDitherTransparency(end);

    }

    public void SetAnimationController(RuntimeAnimatorController control)
    {
        animator.runtimeAnimatorController = control;
    }

    public void SetAnimationTrigger(string name)
    {
        animator.SetTrigger(name);
    }

    public void SetAnimationFloat(string name, float val)
    {
        animator.SetFloat(name, val);
    }

    public void SetAnimationInt(string name, int val)
    {
        animator.SetInteger(name, val);
    }

    public void SetAnimationBool(string name, bool val)
    {
        animator.SetBool(name, val);
    }

    public void SetAnimatorEnabled(bool isEnabled)
    {
        animator.enabled = isEnabled;
    }

    public void SetAnimationSpeed(float speed)
    {
        animator.speed = speed;
    }

    public void SetMoodIdle()
    {
        if (mouthAnimator == null || eyeAnimator == null) return;

        mouthAnimator.SetBool("Intense", false);
        mouthAnimator.SetBool("Suprise", false);
        mouthAnimator.SetBool("Smile", false);

        eyeAnimator.SetBool("Intense", false);
        eyeAnimator.SetBool("Suprise", false);
    }

    public void SetMoodSmile()
    {
        if (mouthAnimator == null || eyeAnimator == null) return;

        mouthAnimator.SetBool("Intense", false);
        mouthAnimator.SetBool("Suprise", false);
        mouthAnimator.SetBool("Smile", true);

        eyeAnimator.SetBool("Intense", false);
        eyeAnimator.SetBool("Suprise", false);
    }

    public void SetMoodIntense()
    {
        if (mouthAnimator == null || eyeAnimator == null) return;

        mouthAnimator.SetBool("Intense", true);
        mouthAnimator.SetBool("Suprise", false);
        mouthAnimator.SetBool("Smile", false);

        eyeAnimator.SetBool("Intense", true);
        eyeAnimator.SetBool("Suprise", false);
    }

    public void SetMoodSuprise()
    {
        if (mouthAnimator == null || eyeAnimator == null) return;

        mouthAnimator.SetBool("Intense", false);
        mouthAnimator.SetBool("Suprise", true);
        mouthAnimator.SetBool("Smile", false);

        eyeAnimator.SetBool("Intense", false);
        eyeAnimator.SetBool("Suprise", true);
    }

    public void HideGearFromLIV()
    {
        Debug.Log("HIDING GEAR FROM LIV");
        ChangeLayersRecursively(headRoot.transform, "FieldPlayer");
        ChangeLayersRecursively(torso.transform, "FieldPlayer");
        ChangeLayersRecursively(lowerBodyRoot.transform, "FieldPlayer");
    }

    public void UnhideGearFromLIV()
    {
        Debug.Log("UNHIDING GEAR FROM LIV");
        ChangeLayersRecursively(headRoot.transform, "Default");
        ChangeLayersRecursively(torso.transform, "Default");
        ChangeLayersRecursively(lowerBodyRoot.transform, "Default");
    }

    void ChangeLayersRecursively(Transform trans, string name)
    {    
        if(trans.gameObject.layer != LayerMask.NameToLayer("SpectatorOnly"))
        {
            trans.gameObject.layer = LayerMask.NameToLayer(name);
        }
        foreach (Transform child in trans)
        {
            if (child.gameObject.layer != LayerMask.NameToLayer("SpectatorOnly"))
            {
                child.gameObject.layer = LayerMask.NameToLayer(name);
            }          
            ChangeLayersRecursively(child, name);
        }
    }
}
