﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//TODO: finish splitting FieldPlayerBodyControl into skeleton and skin
//A Skeleton ONLY handles full body animations and IK. No renderers, materials, or textures.
[RequireComponent(typeof(Animator))]
public class FieldPlayerSkeleton : MonoBehaviour {

    protected Animator animator
    {
        get
        {
            if (m_animator == null) m_animator = GetComponent<Animator>();
            return m_animator;
        }
    }
    private Animator m_animator;

    [Header("Transforms")]
    [SerializeField] protected Transform headRoot;
    [SerializeField] protected Transform neckPivot;
    [SerializeField] protected Transform lowerBodyRoot;

    [SerializeField] protected Transform jersey;
    [SerializeField] protected Transform leftShoulderRoot;
    [SerializeField] protected Transform rightShoulderRoot;
    [SerializeField] protected Transform leftShoulderRoot_Neutral;
    [SerializeField] protected Transform rightShoulderRoot_Neutral;

    [SerializeField] public Transform leftHandRoot;
    [SerializeField] public Transform rightHandRoot;
    [SerializeField] protected Transform torso;
    [SerializeField] protected Transform gloveRoot;


    [Header("Options")]
    public float armLength = 1;





    
    [Space(10)]
    protected Transform _controller;
    protected Camera _camera;

   
    public Vector3 leftHandOffset;
    public Vector3 rightHandOffset;
#if STEAM_VR
    public Vector3 leftHandOffsetVive;
    public Vector3 rightHandOffsetVive;
#elif RIFT
    public Vector3 leftHandOffsetOculus;
    public Vector3 rightHandOffsetOculus;
#endif


    private ParticleSystem[] particleSystems;


    protected virtual void Awake()
    {


#if STEAM_VR

#elif RIFT
        leftHandOffset = leftHandOffsetOculus;
        rightHandOffset = rightHandOffsetOculus;
#endif

        
}


    protected void LateUpdate()
    { 
        SimpleShoulderIK();
        SimpleTorsoIK();
    }
   

    private void SimpleTorsoIK()
    {
        if (!neckPivot) return;
            torso.position = neckPivot.position - neckPivot.up * .1f;

            Vector3 torsoLookDirection = neckPivot.forward;
            if (torso.forward.y < -.5f) //if looking down, use up for forward
            {
                torsoLookDirection = transform.up;
            }
            torsoLookDirection.y = 0f;

            torso.rotation = Quaternion.LookRotation(torsoLookDirection, Vector3.up);
    }

    protected void SimpleShoulderIK()
    {
        SimpleShoulderIK(leftShoulderRoot, leftShoulderRoot_Neutral, leftHandRoot);
        SimpleShoulderIK(rightShoulderRoot, rightShoulderRoot_Neutral, rightHandRoot);
    }

    private void SimpleShoulderIK(Transform shoulderRoot, Transform shoulderRoot_neutral, Transform handRoot)
    {
        if (shoulderRoot && shoulderRoot_neutral && handRoot)
        {

            float extension = Vector3.Distance(shoulderRoot.position, handRoot.position);
            float follow = extension / armLength;
            follow = follow * follow;

            shoulderRoot.rotation =
                Quaternion.Lerp(
                    shoulderRoot_neutral.rotation,
                    Quaternion.LookRotation(handRoot.position - shoulderRoot.position),
                    follow);
        }
    }


    
    public void SetAnimationController(RuntimeAnimatorController control)
    {
        animator.runtimeAnimatorController = control;
    }

    public void SetAnimationTrigger(string name)
    {
        animator.SetTrigger(name);
    }

    public void SetAnimationFloat(string name, float val)
    {
        animator.SetFloat(name, val);
    }

    public void SetAnimationInt(string name, int val)
    {
        animator.SetInteger(name, val);
    }

    public void SetAnimatorEnabled(bool isEnabled)
    {
        animator.enabled = isEnabled;
    }

    public void SetAnimationSpeed(float speed)
    {
        animator.speed = speed;
    }
}
