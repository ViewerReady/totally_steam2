﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldPlayerAnimationEventHandler : MonoBehaviour {

    DefensePlayer def;
    OffensePlayer off;
    BatterPlayer bat;

    void Start()
    {
        def = GetComponentInParent<DefensePlayer>();
        off = GetComponentInParent<OffensePlayer>();
        bat = GetComponentInParent<BatterPlayer>();
    }

    public void PitchBall()
    {
        if (def != null) def.PitchBall();
    }

    public void ThrowBall()
    {
        if (def != null) def.ThrowBall_AnimDelayed();
    }

    public void ParentBallToRightHand()
    {
        if (def != null) def.AIMoveBallFromGloveToHand();
    }

    public void BatterBecomesReady()
    {
        if (bat != null) bat.finishedSetupAnimation = true;
    }
}
