﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatterBodyControl : FieldPlayerBodyControl {

//    public Transform rightHandRoot;
    public float gripTilt =0;
	public BatController theBat;

    // Update is called once per frame

    protected override void MatchPlayerMovement()
    {
        base.MatchPlayerMovement();
        MoveBat();
    }

    void MoveBat()
    {
        if (_controller == null)
        {
            //            Debug.Log("use mouse to move bat");
            rightHandRoot.transform.position += Vector3.up * Input.GetAxis("Mouse Y") * .01f;
            float elevation = rightHandRoot.position.y - headRoot.position.y;
            if (elevation > 1f)
            {
                rightHandRoot.position -= Vector3.up * (elevation - 1f);
            }
            else if (elevation < -1f)
            {
                rightHandRoot.position -= Vector3.up * (elevation + 1f);
            }
            rightHandRoot.transform.rotation = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * 1.2f, Vector3.up) * rightHandRoot.transform.rotation;
            return;
        }
        Quaternion camRot_Y = _camera.transform.rotation;
        camRot_Y = Quaternion.Euler(0, camRot_Y.eulerAngles.y, 0);
        transform.rotation = camRot_Y;
        Vector3 headPos = _camera.transform.localPosition;
        headRoot.rotation = _camera.transform.rotation;
        //      Quaternion relative = Quaternion.Inverse(_camera.transform.rotation) * _controller.Orientation;
        rightHandRoot.rotation = /*Quaternion.AngleAxis(-45, Vector3.right) **/ _controller.rotation;
        if (gripTilt != 0)
        {
            rightHandRoot.rotation = Quaternion.AngleAxis(gripTilt, rightHandRoot.transform.right) * rightHandRoot.rotation;
        }
        if (isOneToOne)
        {
			//rightHandRoot.localPosition = _controller.Position;

			rightHandRoot.position = Camera.main.transform.parent.TransformPoint(_controller.position);
        }
        else
        {
			rightHandRoot.position = headRoot.position + (_controller.position - headPos);
        }

		theBat.transform.localPosition = Vector3.zero;
		theBat.transform.localRotation = Quaternion.identity;
    }
}
