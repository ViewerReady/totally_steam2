﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectator : AIFieldPlayer {

    public SpectatorPosition spectatorPosition;

    public override void InstantReset()
    {
    }

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void SetHumanPosession(bool isPosessed)
    {
        if (isTetheredToCamera == isPosessed)
        {
            return;
        }

        base.SetHumanPosession(isPosessed);

        if (isPosessed)
        {
            if (spectatorPosition == SpectatorPosition.GIANT)
            {
                HumanGearManager.instance.HideAllGear();
            }
            else
            {
                //HumanGearManager.instance.ShowDefenderGear();
            }
        }
        else
        {
            

        }
    }
}
