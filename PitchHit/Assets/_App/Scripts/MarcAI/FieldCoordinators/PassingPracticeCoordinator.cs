﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Players on first and third pass back and forth to each other
public class PassingPracticeCoordinator : AIFieldCoordinator {

    public DefensePlayer playerPrefab;

    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
        SetPlayType(play);
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        if (isHuman)
        {
        }
    }
}
