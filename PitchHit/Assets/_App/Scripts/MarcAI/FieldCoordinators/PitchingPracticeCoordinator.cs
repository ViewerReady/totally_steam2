﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchingPracticeCoordinator : DefenseCoordinator {

    public DefensePlayer pitcherPrefab;

    public override void Initialize()
    {
        base.Initialize();
        SetPlayType(FieldPlayType.NONE);
    }

    new void Update()
    {
        if(FieldMonitor.ballInPlay == null)
        {
            SpawnBallInPitchersGlove();
        }
    }

    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
        SetPlayType(play);
    }

    protected override void SpawnDefensePlayers()
    {
        pitcher = CreateFielder(positions[0]);
    }

    protected override bool ShouldBeginPitch()
    {
        return true;
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);
        if (!isHuman)
        {
            pitcher.InstantReset();
        }
    }

    protected override void NONE_Enter()
    {

        FieldMonitor.Instance.ShowHideStrikeGate(true);
        isReadyToPitch = true;
    }

    protected override void NONE_Update()
    {
        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            FieldMonitor.Instance.ExplodeBall();
        }

        if(FieldMonitor.ballInPlay != null)
        {
            if (FieldMonitor.ballInPlay.hitGround)
            {
                FieldMonitor.Instance.ExplodeBall();
            }
        }
        if (ShouldBeginPitch())
        {
            if (!humanControlled)
            {
                if (isReadyToPitch)
                {
                    AutoPitch();
                }
            }
        }
        
    }

    protected override void NONE_Exit()
    {
        base.NONE_Exit();
    }

    protected override void PITCH_Enter()
    {
        base.PITCH_Enter();
    }
    protected override void PITCH_Update()
    {
//        Debug.Log("pitch update  ball outa here " + FieldMonitor.Instance.BallHasLeftPark());
        base.PITCH_Update();
    }
    protected override void PITCH_Exit()
    {
        base.PITCH_Exit();
    }
}
