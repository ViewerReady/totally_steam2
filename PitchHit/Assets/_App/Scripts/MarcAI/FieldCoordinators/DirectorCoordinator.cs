﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectorCoordinator : BroadcastCoordinator
{
    [SerializeField] GameObject[] directorOnlyObjects;

    new void Start()
    {
        base.Start();
    }

    new void Initialize()
    {
        base.Initialize();
    }

    public void ShowDirectorTools()
    {
        foreach (GameObject go in directorOnlyObjects)
        {
            go.SetActive(true);
        }
    }

    public void HideDirectorTools()
    {
        foreach (GameObject go in directorOnlyObjects)
        {
            go.SetActive(false);
        }
    }
}
