﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchingPracticeCoordinator : AIFieldCoordinator {

    public DefensePlayer playerPrefab;
    private DefensePlayer fielder;
    public Transform outfielderPosition;

    public override void Initialize()
    {
        base.Initialize();
        fielder = GameObject.Instantiate<DefensePlayer>(playerPrefab);
        fielder.SetFieldPosition(DefenseFieldPosition.CENTER_FIELDER, outfielderPosition.position);
        fielder.Initialize();
        FieldMonitor.RegisterDefensePlayer(fielder);
        fielder.InstantReset();

        SetPlayType(FieldPlayType.NONE);
    }
    //if not human controlled, do nothing
    //if human controlled, show ball landing marker
    //if player catches the ball, destroy it
    //if player does not catch the ball, explode it

    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
        SetPlayType(play);

    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        if (isHuman)
        {
            HumanGloveController.onHumanBallCatch.AddListener(OnBallCaughtByHuman);
            HumanGloveController.onHumanBallRelease.AddListener(OnBallReleasedByHuman);

            HumanPosessionManager.SetInitialPositionRotation(outfielderPosition.position, Quaternion.LookRotation(Vector3.back));
            HumanPosessionManager.PosessPlayer(fielder, resetPlayArea: true, fade: true);
        }
        else
        {
            HumanGloveController.onHumanBallCatch.RemoveListener(OnBallCaughtByHuman);
            HumanGloveController.onHumanBallRelease.RemoveListener(OnBallReleasedByHuman);
        }
    }

    void OnBallCaughtByHuman(hittable ball)
    {
        Debug.Log("OnBallCaughtByHuman");
        //MARC: Logic moved to HandManager
        //PreparePitcherCannon();
    }

    void OnBallReleasedByHuman(hittable ball)
    {

    }
}
