﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Linq;

public enum OutType
{
    STRIKEOUT,
    FOULS,
    CAUGHT,
    FORCED,
    DEFENDED,
    TAG,
    INNING_OVER,
    HIT_FOUL,
}

public class OffenseCoordinator : AIFieldCoordinator
{
    public bool humanRunning;

    //[HideInInspector]
    protected BatController humanBat
    {
        get
        {
            return HandManager.instance.bat;
        }
    }

    public OffensePlayer FieldPlayerPrefab;
    public BatterPlayer batterPrefab;
    protected BatterPlayer batter
    {
        get {
            if (_batter == null)
            {
                _batter = Instantiate(batterPrefab, this.transform.parent);
                _batter.transform.localPosition = Vector3.left;
                _batter.transform.localRotation = Quaternion.identity;
                _batter.gameObject.SetActive(false);
            }
            return _batter;
        }
    }
    private Queue<OffensePlayer> _runnerCache = new Queue<OffensePlayer>();

    public FieldDiagnosticsManager diagnosticsManager;
    public SetGateForPlayerHeight gateSetter;
    private BatterPlayer _batter;
    private OffensePlayer lastRunner;
    private int maxForcedBaseThisPlay = 0;
    private int runnerSerial = 0;

    private bool waitingForCatch = false;

    public Transform onDeckTransform;
    public Transform leftBatterBoxTransform;
    public Transform rightBatterBoxTransform;

    protected Condition PlayOverCondition;

    protected int batterIndex = 0;

    public static bool repeatLastLaunch
    {
        get
        {
            return PlayerPrefs.GetInt("repeatSavedLaunch") == 1;
        }
        protected set
        {
            PlayerPrefs.SetInt("repeatSavedLaunch", value ? 1 : 0);
        }
    }

    private static Vector3 m_savedLaunch;
    public static Vector3 savedLaunch
    {
        get
        {
            if (m_savedLaunch == Vector3.zero)
            {
                m_savedLaunch = new Vector3(
                    PlayerPrefs.GetFloat("savedLaunch_x"),
                    PlayerPrefs.GetFloat("savedLaunch_y"),
                    PlayerPrefs.GetFloat("savedLaunch_z")
                );
            }
            return m_savedLaunch;
        }
        set
        {
            m_savedLaunch = value;
            PlayerPrefs.SetFloat("savedLaunch_x", value.x);
            PlayerPrefs.SetFloat("savedLaunch_y", value.y);
            PlayerPrefs.SetFloat("savedLaunch_z", value.z);
        }
    }

    private static Vector3 m_savedLaunchOrigin;
    public static Vector3 savedLaunchOrigin
    {
        get
        {
            if (m_savedLaunchOrigin == Vector3.zero)
            {
                m_savedLaunchOrigin = new Vector3(
                    PlayerPrefs.GetFloat("savedLaunchOrigin_x"),
                    PlayerPrefs.GetFloat("savedLaunchOrigin_y"),
                    PlayerPrefs.GetFloat("savedLaunchOrigin_z")
                );
                
            }
            return m_savedLaunchOrigin;
        }
        set
        {
            m_savedLaunchOrigin = value;
            PlayerPrefs.SetFloat("savedLaunchOrigin_x", value.x);
            PlayerPrefs.SetFloat("savedLaunchOrigin_y", value.y);
            PlayerPrefs.SetFloat("savedLaunchOrigin_z", value.z);
        }
    }

    private bool atBatCompleted = true;

    public static Action<PlayerInfo> OnNewBatter;

    private Dictionary<TeamInfo, Queue<PlayerInfo>> battingQueues = new Dictionary<TeamInfo, Queue<PlayerInfo>>();
    public Action<Queue<PlayerInfo>> OnBattingOrderUpdated;
    private TeamInfo battingTeam;

    //public WorldScaler worldScale;

    /// <summary>
    /// Sets up critical fields, called in AIFieldPlayer.Start()
    /// </summary>
    public override void Initialize()
    {
        if (isInitialized) return;
        base.Initialize();

        CacheRunner(SpawnRunner());
        CacheRunner(SpawnRunner());
        CacheRunner(SpawnRunner());
        CacheRunner(SpawnRunner());

    }


    void Update()
    {
        CheckForCheatInput();

    }

#if UNITY_EDITOR
    
    void OnDrawGizmos()
    {
        if (repeatLastLaunch)
        {
            Handles.color = Color.magenta;
            Handles.DrawRectangle(0, Vector3.zero, Quaternion.LookRotation(Vector3.up), 2);

            TrajectoryInfo traj = new TrajectoryInfo();
            traj.SimulatePosition(new Vector3(0, 1, 1));

            traj.SimulateVelocity(savedLaunch);
            traj.SimulatePosition(savedLaunchOrigin);
            traj.DebugDrawTrajectory_Handles();
        }

    }
#endif

    public override void InstantReset()
    {
        base.InstantReset();
        batterIndex = 0;
        RemoveAllRunners();
        if(batter)
        batter.gameObject.SetActive(false);
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        GameController.isAtBat = isHuman;
        if (isHuman)
        {
            HumanPosessionManager.SetInitialPositionRotation(FieldMonitor.Instance.GetHumanBatterPosition(), Quaternion.LookRotation(Vector3.forward, Vector3.up));
            HumanPosessionManager.PosessPlayer(batter, resetPlayer: true, resetPlayArea: true, fade: true, slowMo: false, cancelIfAlreadyTethered: false);
            HumanPosessionManager.SetLocomotionEnabled(false);
            HumanPosessionManager.ConstrainProjectionToRail(true);
            gateSetter.SetForHMDHeight();


            HumanHUDManager.Instance.ShowHideOffscreenBallIndicator(true);

            //small strike gate for human batters
            FieldMonitor.Instance.ShowHideStrikeMarker(false);
            FieldMonitor.Instance.SetStrikeGateDimensions(Vector2.one * .8f, Vector2.one * .7f, new Vector2(1, .6f));
            diagnosticsManager.diagnosticsEnabled = true;
        }
        else
        {
            diagnosticsManager.diagnosticsEnabled = true;
            gateSetter.SetToDefaultHeight();

        }

        if (runningPlay == FieldPlayType.NONE)
        {
            NONE_Enter();
        }
        else SetPlayType(FieldPlayType.NONE);
        
        //RefreshState();
    }


    void CheckForCheatInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ForceLoadBase(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ForceLoadBase(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ForceLoadBase(2);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            repeatLastLaunch = !repeatLastLaunch;
            Debug.Log("Repeat Launch " + repeatLastLaunch + " " + savedLaunch);

        }
    }

    #region Batting

    public void SetBattingTeam(TeamInfo team)
    {
        if (!team)
        {
            Debug.LogError("no batting team");
        }
        battingTeam = team;
        //Debug.Log("Setting batting team to the " + team.teamName);
        Queue<PlayerInfo> teamQueue = GetBattingQueueForTeam(team);

        if (OnBattingOrderUpdated != null) OnBattingOrderUpdated.Invoke(teamQueue);
    }

    private Queue<PlayerInfo> GetBattingQueueForTeam(TeamInfo team)
    {
        if (!team)
        {
            Debug.LogError("no batting team");
            return null;
        }
        if (!battingQueues.ContainsKey(team))
        {
            Queue<PlayerInfo> initQueue = new Queue<PlayerInfo>();
            foreach (PlayerInfo info in team.battingOrder)
            {
                initQueue.Enqueue(info);
            }
            battingQueues.Add(team, initQueue);
            return initQueue;
        }
        return battingQueues[team];
    }

    private void AddToBattingQueue(PlayerInfo info)
    {
        if (!battingTeam)
        {
            Debug.LogError("no batting team");
            return;
        }
        //Debug.Log("Adding player " + info.jerseyName + " to the batting queue for team " + battingTeam.teamName);
        Queue<PlayerInfo> battingQueue = GetBattingQueueForTeam(battingTeam);
        battingQueue.Enqueue(info);
        
        if (OnBattingOrderUpdated != null) OnBattingOrderUpdated.Invoke(battingQueue);
    }

    private PlayerInfo GetNextBatter()
    {
        if (!battingTeam)
        {
            Debug.LogError("no batting team");
            return null;
        }
        PlayerInfo batter = battingTeam.GetRandomBatter(); //fallback in case queue is empty
        Queue<PlayerInfo> battingQueue = GetBattingQueueForTeam(battingTeam);

        if (battingQueue.Count == 0 ) {
            foreach (PlayerInfo info in battingTeam.battingOrder)
            {
                battingQueue.Enqueue(info);
            }
        }
        batter = battingQueue.Dequeue();
        //Debug.Log("Dequeueing batter " + batter.jerseyName + " from the batting queue for team " + battingTeam.teamName);

        if (OnBattingOrderUpdated != null) OnBattingOrderUpdated.Invoke(battingQueue);
        return batter;
    }
    #endregion


    /// <summary>
    /// Immediately place a runner onto a base
    /// </summary>
    /// <param name="baseNo">Base to put a runner on</param>
    void ForceLoadBase(int baseNo)
    {
        if (baseNo >= 0 && baseNo < FieldMonitor.Instance.numBases)
        {
            OffensePlayer newRunner = SpawnRunner(baseNo);
            newRunner.position = FieldMonitor.Instance.GetBasePosition(baseNo);

            newRunner.StayOnBase(baseNo);
        }
    }

    protected override void ExecutePlayWithBall(hittable hitBall, FieldPlayType playType)
    {
        if (hitBall != null)
        {
            hitBall.OnCaughtByDefender += CheckForOutOnCatch;
            waitingForCatch = FieldMonitor.Instance.AirInterceptionEstimates(hitBall).Count > 0;
        }
        if(playType == FieldPlayType.STANDARD || playType == FieldPlayType.WALK)
        {
            SendOutNewRunner();
        }
        SetPlayType(playType);
    }

    private void AdvanceAllRunnersSingleBase()
    {
        foreach (OffensePlayer runner in FieldMonitor.offensivePlayers)
        {
            runner.RunToBaseForced(runner.baseLastTouched + 1);
        }
    }

    private OffensePlayer GetCachedRunner()
    {
        if (_runnerCache.Count > 0)
        {
            OffensePlayer runner = _runnerCache.Dequeue();
            runner.gameObject.SetActive(true);
            runner.ResetBaseHistory();
            FieldMonitor.RegisterRunner(runner);
            return runner;
        }
        else return SpawnRunner();
    }

    private void CacheRunner(OffensePlayer runner)
    {
        //Debug.Log("cache runner " + runner.name);
        FieldMonitor.UnRegisterRunner(runner);
        runner.gameObject.SetActive(false);
        if (!_runnerCache.Contains(runner))
        {
            _runnerCache.Enqueue(runner);
        }
    }

    void SendOutNewRunner()
    {
        int maxForcedBase = FieldMonitor.Instance.MaxForcedBase();

        foreach (OffensePlayer runner in FieldMonitor.offensivePlayers)
        {
            int nextBase = runner.baseAtPitch + 1;
            if (nextBase <= maxForcedBase)
            {
                runner.RunToBaseForced(nextBase);
            }
            //if not forced, runners will make decisions about running in UpdateRunners()                
        }

        lastRunner = GetCachedRunner();
        TeamInfo tInfo = battingTeam;

        PlayerInfo pInfo;
        if (batter) pInfo = batter.info;
        else pInfo = tInfo.GetRandomBatter();

        lastRunner.SetPlayerInfo(tInfo, pInfo);
        lastRunner.runSpeed = pInfo.runSpeedOffense;
        lastRunner.position = batter.position;
        lastRunner.RunToBaseForced(0);

        if (humanControlled && humanRunning)
        {
            //Debug.Log("PLAYER HAS TO RUN WITH ARMS!");
            GameController.isAtBat = false;
            HumanPosessionManager.PosessPlayer(lastRunner, fade: false, slowMo: false, resetPlayer: false, resetPlayArea: false, snapToHumanPosition: true);
            HumanPosessionManager.SetLocomotionEnabled(true, .5f);
            HumanPosessionManager.SetProjectionRailPoints(FieldMonitor.Instance.GetHomePlatePosition(), FieldMonitor.Instance.GetBasePosition(0));
            HumanPosessionManager.SetReverseRailLocomotionEnabled(false);
        }

        if (!humanControlled)
            switcherooRoutine = StartCoroutine(BatterRunnerSwitcheroo(batter, lastRunner));
    }

    OffensePlayer[] runnersArray;
    void CheckForForcedOuts()
    {
        runnersArray = FieldMonitor.offensivePlayers.ToArray();
        foreach (OffensePlayer runner in runnersArray)
        {
            
            //if the runner has no choice, check to see if they're out by force
            if (runner.forcedRun)
            {
                //if their forced base is defended
                if (FieldMonitor.Instance.BaseIsDefended(runner.baseTarget_Eventual))
                {
                    //if they're currently on their forced base, continue to the next runner
                    int onBase;
                    if (FieldMonitor.Instance.RunnerIsOnBase(runner, out onBase))
                    {
                        if (onBase == runner.baseTarget_Eventual)
                            continue;
                    }
                    runner.NotifyOfOut(OutType.FORCED);
                }

                
                
            }
        }
    }

    public void ReactToBattingOut(OutType outType)
    {
        atBatCompleted = true;
        Debug.Log("Batted out!");
        AddToBattingQueue(batter.info);
        //Debug.Log("batter " + batter);
        //Debug.Log("find batter " + FindObjectOfType<BatterPlayer>());
        OneShotEffectManager.SpawnEffect("player_out", batter.transform.position, batter.transform.rotation, lifetime: 3f, spectatorOnly: batter.isTetheredToCamera);
        batter.gameObject.SetActive(false);
        //batter will reappear in none_enter 
        
    }


    /// <summary>
    /// Iterate through list of runners and make decisions about whether to run forward, run back, or stay put
    /// </summary>
    void UpdateRunnerTargetBases()
    {

        RankedFieldPlayer<DefensePlayer> posessionEstimate = new RankedFieldPlayer<DefensePlayer>(null, float.MaxValue, Vector3.zero);

        List<RankedFieldPlayer<DefensePlayer>> airInterception = FieldMonitor.Instance.AirInterceptionEstimates(FieldMonitor.ballInPlay);
        if (airInterception.Count > 0) posessionEstimate = airInterception[0];
        else
        {
            List<RankedFieldPlayer<DefensePlayer>> groundInterception = FieldMonitor.Instance.GroundInterceptionEstimates(FieldMonitor.ballInPlay);
            if (groundInterception.Count > 0) posessionEstimate = groundInterception[0];
        }

        foreach (OffensePlayer runner in FieldMonitor.offensivePlayers)
        {
            //Debug.Log("runner " + runner.name + " " + runner.state);
            //if this player is out, don't give them any instructions
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT)
            {

                continue;
            }

            //if the runner has no choice, no need to make calculations. If the runner is tethered, do not change it's state
            if (!runner.forcedRun && !runner.isTetheredToCamera)
            {
                int baseLastTagged = runner.baseLastTouched;
                int nextBase = baseLastTagged + 1;

                if (ShouldRunToNextBase(runner, baseLastTagged, nextBase, posessionEstimate))
                {
//                    Debug.Log(runner.name + " run to next base " + nextBase);
                    //if the runner isn't already running to the next base, tell it to
                    //if (runner.baseTarget_Immediate != nextBase)
                    runner.RunToBase(nextBase);
                }

                else
                {
                    //runner is already on base, no need to do anything
                    if (Vector3.Distance(runner.position, FieldMonitor.Instance.GetBasePosition(baseLastTagged)) < .1f)
                    {
                        runner.StayOnBase(baseLastTagged);
                    }

                    //if the runner isn't already running back to baseLastTouched, tell it to
                    else if (runner.baseTarget_Eventual != baseLastTagged)
                    {
                        runner.RunToBase(baseLastTagged);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Give directions to runner who has just reached a base.
    /// <param name="baseNo"></param>
    /// <param name="runner"></param>
    void GiveDirectionsToRunnerOnBase(int baseNo, OffensePlayer runner)
    {
        //if runner is currently being controlled, 
        if (runner.isTetheredToCamera)
        {

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="runner"></param>
    /// <param name="lastBase"></param>
    /// <param name="nextBase"></param>
    /// <returns>true means run to next base, false means run to last base</returns>
    bool ShouldRunToNextBase(OffensePlayer runner, int lastBase, int nextBase, AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> posession)
    {
        if (runningPlay == FieldPlayType.HOMERUN) return true;
        if (runningPlay == FieldPlayType.FOUL) return false;
        if (runningPlay == FieldPlayType.WALK) return false;


        if (waitingForCatch) return false;

        //if there's no estimation of who will have the ball next, the ball might be destroyed. so go for it.
        if (posession.fieldPlayer == null) return true;

        FieldMonitor.FieldState state = FieldMonitor.Instance.fieldState;
        OffensePlayer ahead = FieldMonitor.Instance.RunnerAheadOf(runner);
        if (ahead != null && ahead.baseTarget_Immediate == nextBase) return false;
        //if(state.baseOccupied[nextBase]) return false;

        Vector3 pos_nextBase = FieldMonitor.Instance.GetBasePosition(nextBase);
        Vector3 pos_lastBase = FieldMonitor.Instance.GetBasePosition(lastBase);
        float time_runToNextBase = runner.GetTimeToRunToPosition(pos_nextBase);// Vector3.Distance(runner.transform.position, pos_nextBase) / runner.runSpeed;
        float time_runToLastBase = runner.GetTimeToRunToPosition(pos_lastBase);// Vector3.Distance(runner.transform.position, pos_lastBase) / runner.runSpeed;
        float time_ballToNextBase = posession.value + Vector3.Distance(posession.position, pos_nextBase) / posession.fieldPlayer.throwSpeed;
        float time_ballToLastBase = posession.value + Vector3.Distance(posession.position, pos_lastBase) / posession.fieldPlayer.throwSpeed;

        bool safeToNext = time_runToNextBase < time_ballToNextBase;
        bool safeToLast = time_runToLastBase < time_ballToLastBase;

        runner.SetDebugText(time_runToNextBase.ToString("0.0") + " " + time_ballToNextBase.ToString("0.0"));

        //        Debug.Log("Should Run To Next " + lastBase + " " + nextBase + " " + time_runToNextBase + " " + time_ballToNextBase + " " + posession + " " + Vector3.Distance(posession.position, pos_nextBase) / posession.fieldPlayer.throwSpeed);
        if (safeToNext) return true;
        else if (safeToLast) return false;
        else return true;
    }

    /// <summary>
    /// Called whenever the playBall is caught. Checks to see if the ball ever touched the ground and gets the last runner out if not.
    /// </summary>
    /// <param name="ball"></param>
    /// <param name="catcher"></param>
    void CheckForOutOnCatch(hittable ball)
    {
        if (runningPlay == FieldPlayType.PITCH
            || runningPlay == FieldPlayType.HOMERUN
            || runningPlay == FieldPlayType.WALK)
        {
            //do nothing
        }

        else if (!ball.hasEverHitGround)
        {
            if (lastRunner)
            {
                lastRunner.NotifyOfOut(OutType.CAUGHT);
            }
            ForceReturnRunnersToBaseAtPitch();
        }

        ball.OnCaughtByDefender -= CheckForOutOnCatch;
        waitingForCatch = false;
    }

    void ForceReturnRunnersToBaseAtPitch()
    {
        foreach (OffensePlayer runner in FieldMonitor.offensivePlayers)
        {
            //players that are out need to stay out
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue;
            if (runner == lastRunner) continue;
            runner.RunToBaseForced(runner.baseAtPitch);
        }
    }

    OffensePlayer SpawnRunner(int baseNo = -1)
    {
        OffensePlayer runner = Instantiate(FieldPlayerPrefab, this.transform.parent, false) as OffensePlayer;
        runner.Initialize(baseNo);
        //Debug.Log("spawn runner at base:"+baseNo+" at "+ FieldManager.Instance.GetBasePosition(baseNo));

        if (baseNo == -1)
        {
            if (humanControlled)//Greg: Sometimes the player was jerking to the batter's forced position. So I skipped the middle man.
            {
                Vector3 playerPosition = Camera.main.transform.position;
                playerPosition.y = 0f;
                runner.position = playerPosition;
            }
            else
            {
                runner.position = batter.position;
            }
        }
        else
        {
            runner.position = FieldMonitor.Instance.GetBasePosition(baseNo);
        }
        //			runner.SetWorldScaler (worldScale);



        runner.OnHome += ReactToRunCompleted;
        runner.OnOut += ReactToPlayerOut;
        runner.OnBaseReached += GiveDirectionsToRunnerOnBase;
        runner.OnFieldPlayerStateChange += ReactToPlayerStateChange;
        runner.OnHumanInstruction += ReactToPlayerReceivingHumanInstructions;
        runner.OnHumanPosession += ReactToHumanPosession;
        runner.OnHumanDeposession += ReactToHumanDeposession;
        runner.name = "Runner_" + runnerSerial;
        runnerSerial++;
        return runner;
    }

    private Coroutine switcherooRoutine;
    IEnumerator BatterRunnerSwitcheroo(BatterPlayer bat, OffensePlayer run)
    {
        run.gameObject.SetActive(false);
        yield return new WaitForSeconds(.5f);
        if(run != null) run.gameObject.SetActive(true);
        if(bat != null) bat.gameObject.SetActive(false);
        if (FullGameDirector.OnHideBatter != null) FullGameDirector.OnHideBatter();
        //FieldManager.Instance.UnRegisterBatterFromMinimap();
        //yield return new WaitForSeconds(2);
    }

    Coroutine newBatterWalkupRoutine;
    protected void PrepareBatter(float delay = 0, bool startAtPlate = false)
    {
        Debug.Log("Checking how many outs there currently are in the prepare batter function: " + FullGameDirector.currentOuts);
        Debug.Log("Checking if atBatCompleted in the prepare batter function: " + atBatCompleted);
        if (atBatCompleted && FullGameDirector.currentOuts < 3)
        {
            TeamInfo team = battingTeam;
            PlayerInfo newBatterInfo = GetNextBatter();
            batter.SetPlayerInfo(team, newBatterInfo);
            if (OnNewBatter != null)
            {
                Debug.Log("Invoking on new batter");
                OnNewBatter.Invoke(newBatterInfo);
            }
        }

        atBatCompleted = false;
        if (FullGameDirector.OnShowBatter != null) FullGameDirector.OnShowBatter(batter.transform, batter.info);

        if (humanControlled)
        {
            HumanPosessionManager.SetInitialPositionRotation(FieldMonitor.Instance.GetHumanBatterPosition(), Quaternion.LookRotation(Vector3.forward, Vector3.up));
            HumanPosessionManager.PosessPlayer(batter, resetPlayArea: true, fade: true, cancelIfAlreadyTethered: true);
            HumanPosessionManager.SetLocomotionEnabled(false);
            gateSetter.SetForHMDHeight();

        }
        else if (batter && !batter.isActiveAndEnabled)
        {
            StartCoroutine(SendOutBatterRoutine(delay, startAtPlate));
        }

      
    }

    IEnumerator SendOutBatterRoutine(float delay, bool startAtPlate)
    {
        batter.gameObject.SetActive(true);
        batter.DitherFade(0);

        if (delay > 0)
            yield return new WaitForSeconds(delay);

        if (startAtPlate)
        {
            batter.transform.localPosition = Vector3.left;
            batter.transform.localRotation = Quaternion.identity;
        }
        else
        {
            batter.position = onDeckTransform.position;
            batter.transform.rotation = onDeckTransform.rotation;
        }
        batter.gameObject.SetActive(true);
        batter.DitherFade(0, 1, .5f);
        if (FullGameDirector.OnBatterWalkToPlate != null) FullGameDirector.OnBatterWalkToPlate(batter.transform, batter.info);
        batter.StartWalkingAnimation();
        batter.RunToTarget(leftBatterBoxTransform);
    }

    void RemoveAllRunners()
    {
        OffensePlayer[] runners = FieldMonitor.offensivePlayers.ToArray();
        foreach (OffensePlayer runner in runners)
        {
            if (runner.state != AIFieldPlayer.FieldPlayerAction.OUT)
                runner.NotifyOfOut(OutType.INNING_OVER);
        }
    }

    public void EndPlay()
    {
        //Debug.Log("EndPlay for Offense");
        if (_state == FieldPlayType.NONE)//Greg: In case we have the EndPlay functions calling each other we don't want a stack overflow.
        {
            return;
        }

//        Debug.Log("This ought to be the one setting the state to NONE");
        SetPlayType(FieldPlayType.NONE);

        if (humanControlled)
        {


        }
    }

    void ReactToHumanPosession(AIFieldPlayer player)
    {

    }

    void ReactToHumanDeposession(AIFieldPlayer player)
    {
        //if the runner is on a base, make sure they center themself on that base
        if (player.state == AIFieldPlayer.FieldPlayerAction.BASE) player.InstantReset();
    }


    public bool SwingDetected()
    {
        if (humanControlled) return humanBat.SwingWasDetected();
        return batter.AutoSwingDetected();
    }

    #region Conditions

    bool NoActiveRunners()
    {
        return (FieldMonitor.offensivePlayers.Count == 0);

    }

    bool AllRunnersOnBase()
    {
        return FieldMonitor.Instance.AllRunnersOnBase();
    }

    bool BallIsNoLongerAirborne()
    {
        if (    FieldMonitor.ballInPlay == null 
            ||  FieldMonitor.ballInPlay.hitGround
            ||  FieldMonitor.ballInPlay.hasBeenCaught
            ||  FieldMonitor.ballInPlay.transform.position.y < -1)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// AI pitchers need to wait for an animation queue, human pitchers do not.
    /// </summary>
    /// <param name="humanPitcher"></param>
    /// <returns></returns>
    public bool ReadyForPitch(bool humanPitcher)
    {
        return humanPitcher ? BatterIsReadyForHumanPitch() : BatterIsReadyForAIPitch();
    }

    private bool BatterIsReadyForHumanPitch()
    {
        if (batter.isTetheredToCamera) return true;
        return (batter.isActiveAndEnabled && batter.state == AIFieldPlayer.FieldPlayerAction.IDLE );
    }

    private bool BatterIsReadyForAIPitch()
    {
        if (batter.isTetheredToCamera) return true;
        return BatterIsReadyForHumanPitch() && batter.finishedSetupAnimation;
    }

    #endregion


    #region PlayerState_Machine

    void ReactToRunCompleted(AIFieldPlayer player)
    {
        AddToBattingQueue(player.info);
        OffensePlayer runner = player as OffensePlayer;
        CacheRunner(runner);
    }

    void ReactToPlayerOut(AIFieldPlayer player, OutType outType)
    {
        //Debug.Log("React to player out: " + outType);
        //anytime a posessed runner gets tagged out, make sure posession transfers to the batter

        //NOTE: We don't always want to posess the batter when a runner gets out. if it's the end of the inning
        //No guarantee FieldManager will update state 
        if (player.isTetheredToCamera)
        {
            StartCoroutine(PrepareBatterAfterFrame());
        }

        if(outType != OutType.HIT_FOUL)
        {
            AddToBattingQueue(player.info);
        }

        OffensePlayer runner = player as OffensePlayer;
        CacheRunner(runner);

    }

    IEnumerator PrepareBatterAfterFrame()
    {
        yield return null;
        PrepareBatter();

    }
    void ReactToPlayerStateChange(AIFieldPlayer player, AIFieldPlayer.FieldPlayerAction newState)
    {

    }

    void ReactToPlayerReceivingHumanInstructions(AIFieldPlayer instructedPlayer, AIFieldPlayer.PlayerActionContainer instructedAction)
    {

        OffensePlayer instructedDefender = instructedPlayer as OffensePlayer;
        if (instructedDefender == null) return;

        switch (instructedAction.action)
        {

            case AIFieldPlayer.FieldPlayerAction.BASE:
                break;
            case AIFieldPlayer.FieldPlayerAction.RUN:
                break;

        }
    }
    #endregion

    #region STATE_MACHINE
    void NONE_Enter()
    {
        //Debug.Log("Entering NONE, calling prepare batter.");
        PrepareBatter();
    }

    //MARC: NONE seems dangerous 1/17/19
    /*
    void NONE_Update()
    {
        UpdateRunnerTargetBases();

        //someone has calculated that they should run, go back to STANDARD
        if (!AllRunnersOnBase())
        {
            SetPlayType(FieldPlayType.STANDARD);
        }
    }
    */

    void NONE_Exit()
    {

    }

    void PITCH_Enter()
    {
        PlayOverCondition = BallIsNoLongerAirborne;
        lastRunner = null;

        foreach (OffensePlayer runner in FieldMonitor.offensivePlayers)
        {
            runner.RecordBaseAtPitch();
        }

        if (!humanControlled)
        {
            //FieldMonitor.ballInPlay.GetComponent<Pitchable>().OnPitchComplete += PrepareAutoBatter;
            batter.PrepareForAutoBat(FieldMonitor.Instance.strikeGate, FieldMonitor.ballInPlay);
        }

        if (humanControlled)
        {
            humanBat.ResetSwingFlag();
            if (humanControlled)
            {
                humanBat.SetColliderActive(true);
            }
            else
            {
                humanBat.SetColliderActive(false);
            }
        }
    }

    /*void PrepareAutoBatter()
    {
        Debug.Log("PREPARE FOR AUTOBATTER OC");
        FieldMonitor.ballInPlay.GetComponent<Pitchable>().OnPitchComplete -= PrepareAutoBatter;
        batter.PrepareForAutoBat(FieldMonitor.Instance.strikeGate, FieldMonitor.ballInPlay);
    }*/

    void PITCH_Update()
    {
        if (PlayOverCondition())
        {
            EndPlay();
        }
    }

    void PITCH_Exit()
    {
        if (humanControlled)
        {
            humanBat.SetColliderActive(false);
        }
    }

    void STANDARD_Enter()
    {
        PlayOverCondition = NoActiveRunners;
        atBatCompleted = true;

        //SendOutNewRunner(); MARC: moved this to ExecutePlayWithBall

        //hide the batter so he doesn't appear in the player's face suddenly when not posessed
        if (humanControlled)
        {
            batter.gameObject.SetActive(false);
        }
    }

    void STANDARD_Update()
    {
        if (waitingForCatch && FieldMonitor.ballInPlay.hasEverHitGround) waitingForCatch = false;
        CheckForForcedOuts();
        UpdateRunnerTargetBases();

        //the human player probably chucked it out of the field, no need to let the runners go all the way home.
        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            AdvanceAllRunnersSingleBase();
            SetPlayType(FieldPlayType.WALK);
        }

        if (humanControlled)
        {
            //MARC: this may be causing player to get kicked out of a runner prematurely
            // Debug.Log(FieldManager.Instance.PitcherOnMoundWithBall() + "   " + FieldManager.Instance.AllRunnersOnBase());

            if (FieldMonitor.Instance.PitcherOnMoundWithBall() && FieldMonitor.Instance.AllRunnersOnBase())
            {
                EndPlay();
            }

        }
        else
        {
            if (FieldMonitor.Instance.AllRunnersOnBase())
            {
                //MARC: NONE seems dangerous 1/17/19
                EndPlay();
            }
        }
        if (PlayOverCondition()) EndPlay();


    }
    void STANDARD_Exit()
    {
        // StartCoroutine(BatterWalkUpRoutine());
    }

    void HOMERUN_Enter()
    {
        atBatCompleted = true;
        PlayOverCondition = NoActiveRunners;
        if(HomerunSettingUI.abridgeHomerun) AbridgeHomeRun();
    }
    void HOMERUN_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();

    }

    void AbridgeHomeRun()
    {
        StartCoroutine("HOMERUN_Abridge");
    }

    IEnumerator HOMERUN_Abridge()
    {
        while (FieldMonitor.offensivePlayers.Count > 0)
        {
            LinkedListNode<OffensePlayer> run = FieldMonitor.offensivePlayers.Last;
            while (run != null)
            {
                if (run.Value.state != AIFieldPlayer.FieldPlayerAction.OUT)
                {
                    run.Value.NotifyOfRunCompletion();
                    break;
                }
                run = run.Previous;
            }

            yield return new WaitForSeconds(1);
        }
    }

    void HOMERUN_Exit()
    {
        StopCoroutine("HOMERUN_Abridge");
    }

    void FOUL_Enter()
    {
        FieldMonitor.Instance.ExplodeBall(1);

        PlayOverCondition = AllRunnersOnBase;
        atBatCompleted = false;

        //reset last batter
        Debug.Log("FOUL  offense ");

        if (switcherooRoutine != null)
        {
            StopCoroutine(switcherooRoutine);
        }

        if (lastRunner != null)
        {
            Debug.Log("deal with hit foul");
            lastRunner.gameObject.SetActive(true);
            lastRunner.NotifyOfOut(OutType.HIT_FOUL);
        }

        ForceReturnRunnersToBaseAtPitch();
    }

    void FOUL_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();
    }

    void FOUL_Exit()
    {

    }

    void RETURN_Enter()
    {
        PlayOverCondition = NoActiveRunners;
    }

    void RETURN_Update()
    {
        if (PlayOverCondition()) EndPlay();
    }

    void RETURN_Exit() { }

    void SWITCH_Enter()
    {
        atBatCompleted = true;
        InstantReset();
    }

    void SWITCH_Update()
    {

    }
    void SWITCH_Exit()
    {

    }

    void WALK_Enter()
    {
        atBatCompleted = true;
        PlayOverCondition = AllRunnersOnBase;
        //SendOutNewRunner(); MARC: moved this to ExecutePlayWithBall so we can enter WALK when ball goes out of play, without creating a new runner

    }

    void WALK_Update()
    {
        UpdateRunnerTargetBases();
        if (PlayOverCondition()) EndPlay();
    }

    void WALK_Exit()
    {

    }

    void OPENNING_Enter()
    {

    }

    void OPENNING_Update()
    {

    }

    void OPENNING_Exit()
    {
    }

    void CLOSING_Enter()
    {
        RemoveAllRunners();
        batter.gameObject.SetActive(false);
    }

    void CLOSING_Update()
    {
        if (batter.gameObject.activeSelf) batter.gameObject.SetActive(false);
    }

    void CLOSING_Exit()
    {
    }

    void DISABLED_Enter()
    {

    }

    void DISABLED_Exit()
    {

    }


    #endregion

    public void RefreshState()
    {
        switch (_state)
        {
            case FieldPlayType.STANDARD:
                STANDARD_Enter();
                break;
            case FieldPlayType.FOUL:
                FOUL_Enter();
                break;
            case FieldPlayType.HOMERUN:
                HOMERUN_Enter();
                break;
            case FieldPlayType.NONE:
                NONE_Enter();
                break;
            case FieldPlayType.PITCH:
                PITCH_Enter();
                break;
            case FieldPlayType.RETURN:
                RETURN_Enter();
                break;
        }
    }
}