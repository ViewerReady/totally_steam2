﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattingPracticeCoordinator : OffenseCoordinator {



    public override void Initialize()
    {
        base.Initialize();
        batter.transform.position = Vector3.left;
        batter.SetLookMode(AIFieldPlayer.LookMode.POINT);
        batter.LookAtTarget(Vector3.forward * 28);
        SetPlayType(FieldPlayType.NONE);
    }


    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
        SetPlayType(play);
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);
        if (isHuman)
        {
            HumanPosessionManager.SetLocomotionEnabled(true);
        }
        else
        {
            batter.InstantReset();
        }
    }

    void PITCH_Enter()
    {

        if (!humanControlled)
        {
            batter.PrepareForAutoBat(FieldMonitor.Instance.strikeGate, FieldMonitor.ballInPlay);
        }

        if (humanControlled)
        {
            humanBat.ResetSwingFlag();
            if (humanControlled)
            {
                humanBat.SetColliderActive(true);
            }
            else
            {
                humanBat.SetColliderActive(false);
            }
        }
    }

    void PITCH_Update()
    {

    }

    void PITCH_Exit()
    {
        if (humanControlled)
        {
            humanBat.SetColliderActive(false);
        }
    }




}
