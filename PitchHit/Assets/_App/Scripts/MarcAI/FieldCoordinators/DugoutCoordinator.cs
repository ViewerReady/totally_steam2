﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif



public enum DugoutRole
{
    SIT,
    STAND,
}

/// <summary>
/// This class listens for events from a "hitThings" and gives directions to all Outfielders to execute plays
/// </summary>
public class DugoutCoordinator : AIFieldCoordinator
{
    //public WorldScaler worldScale;
    public static DugoutCoordinator Instance;
    public DugoutPlayer FieldPlayerPrefab;


    public Transform[] homeBenches;
    public Transform home_left, home_right;
    public Transform[] awayBenches;
    public Transform away_left, away_right;

    protected DugoutPlayer[] homeDougs;
    protected DugoutPlayer[] awayDougs;

    protected int numPlayersPerDugout = 3;


    void Awake()
    {
        if (Instance == null) Instance = this;
        else GameObject.Destroy(this);
    }
    

    #region Initialization

    /// <summary>
    /// Sets up critical fields, called in AIFieldPlayer.Start()
    /// </summary>
    public override void Initialize()
    {
        if (isInitialized) return;

        base.Initialize();

        SpawnDugoutPlayers();
       
    }

    #endregion

    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
        SetPlayType(play);
    }

    DugoutPlayer[] CreateDugoutPlayersForTeam(TeamInfo team)
    {
        DugoutPlayer[] players = new DugoutPlayer[numPlayersPerDugout];
        for(int i = 0; i<numPlayersPerDugout; i++)
        {
            DugoutPlayer doug = GameObject.Instantiate(FieldPlayerPrefab);

            doug.SetPlayerInfo(team, team.GetRandomBatter());
            players[i] = doug;
            doug.gameObject.SetActive(false);
        }

        return players;
    }

    void SpawnDugoutPlayers()
    {
        homeDougs = CreateDugoutPlayersForTeam(FullGameDirector.homeTeam);
        awayDougs = CreateDugoutPlayersForTeam(FullGameDirector.awayTeam);
    }

    void PlaceDugoutDwellers(Transform[] benches, Transform left, Transform right, DugoutPlayer[] dougs)
    {
        
        Transform chosenBench = benches[UnityEngine.Random.Range(0, benches.Length)];
        Transform seat_a = benches[UnityEngine.Random.Range(0, 1)].GetChild(UnityEngine.Random.Range(0, 1));
        Transform seat_b = benches[UnityEngine.Random.Range(2, 3)].GetChild(UnityEngine.Random.Range(0, 1));


        for (int i = 0; i < numPlayersPerDugout; i++)
        {
            dougs[i].gameObject.SetActive(true);
            dougs[i].DitherFade(0, 1, 1);
        }

        dougs[0].SitOnBenchSeat(seat_a, true);
        dougs[1].SitOnBenchSeat(seat_b, false);
        dougs[2].PaceBetweenPoints(left, right);


    }

    void RefreshDugoutPlayers()
    {
        PlaceDugoutDwellers(homeBenches, home_left, home_right, homeDougs);
        PlaceDugoutDwellers(awayBenches, away_left, away_right, awayDougs);
    }


    void OPENNING_Enter()
    {
        RefreshDugoutPlayers();
    }

    void OPENNING_Update()
    {
       
    }

    void OPENNING_Exit()
    {
    }


    void SWITCH_Enter()
    {
        RefreshDugoutPlayers();
    }

    void SWITCH_Update()
    {

    }
    void SWITCH_Exit()
    {

    }

}