﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CloudFine.ThrowLab;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Linq;

#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

public enum DefenseFieldPosition
{
    PITCHER,
    FIRST_BASE,
    SECOND_BASE,
    THIRD_BASE,
    SHORTSTOP,
    LEFT_FIELDER,
    CENTER_FIELDER,
    RIGHT_FIELDER,
    CATCHER
}

/// <summary>
/// This class listens for events from a "hitThings" and gives directions to all Outfielders to execute plays
/// </summary>
/// 
public class DefenseCoordinator : AIFieldCoordinator
{
    //public WorldScaler worldScale;
    public static DefenseCoordinator Instance;
    public DefensePlayer FieldPlayerPrefab;

    //POSITIONS
    public OutfieldPositionContainer[] positions;
    protected Dictionary<DefenseFieldPosition, Vector3> positionDict; //stores field positions in local space
    protected Dictionary<DefenseFieldPosition, DefensePlayer> positionToOutfielderDict = new Dictionary<DefenseFieldPosition, DefensePlayer>();

    //FLAGS
    protected bool baseCoverage_dirty = false;
    protected bool backupCoverage_dirty = false;
    protected bool ballChasers_dirty = false;

    private bool witholdBallHolderDirections = false;
    protected bool isReadyToPitch;
    public bool aIpassInProgress
    {
        get; private set;
    }
    private bool autoSwitchPosession;
    private bool finishedTeleportingForThisPlay;

    private ThrowTarget _catcherThrowTarget;

    //ROLES
    protected List<DefensePlayer> infielders = new List<DefensePlayer>();
    protected List<DefensePlayer> outfielders = new List<DefensePlayer>();

    protected DefensePlayer[] baseCoverage;
    protected DefensePlayer[] backupCoverage;
    protected Transform[] backupTargetsWithBallController;

    protected DefensePlayer playerWithBallControl;
    protected List<DefensePlayer> baseDefenders = new List<DefensePlayer>(); //players who are covering bases
    protected List<DefensePlayer> ballChasers = new List<DefensePlayer>(); //players who are trying to get the ball
    protected List<DefensePlayer> backupPlayers = new List<DefensePlayer>(); //players who are backing up bases
    private List<RankedFieldPlayer<DefensePlayer>> airInterceptors;

    public DefensePlayer pitcher { get; protected set; }
    public DefensePlayer catcher { get; protected set; }

    private DefensePlayer lastThrower;
    private DefensePlayer lastReceiver;

    //DOPPELGANGERS
    protected Dictionary<DefensePlayer, DefensePlayer> doppelgangerDict = new Dictionary<DefensePlayer, DefensePlayer>();
    protected List<DefensePlayer> doppelgangers = new List<DefensePlayer>(); //players who are backing up bases

    //DELEGATES

    protected delegate float PositionEvaluation(List<RankedFieldPlayer<DefensePlayer>> candidates, Vector3 position, int baseNo = -1);
    protected delegate float PlayerEvaluation(DefensePlayer candidate, Vector3 position, int baseNo = -1);
    protected delegate void Instructions(DefensePlayer fieldPlayer);
    protected delegate void DirectedInstructions(DefensePlayer fieldPlayer, Transform transform);

    protected Instructions BallHolderInstructions;
    protected Instructions BallThrowerInstructions;
    protected Instructions BasePlayerInstructions;
    protected Instructions BackupPlayerInstructions;
    protected Instructions BallChaserInstructions;

    protected Instructions OnPosessionInstructions;
    protected Instructions OnDeposessionInstruction;

    protected Condition PlayOverCondition;

    //EQUIPMENT
    private HandCannonController pitcherCannon
    {
        get
        {
            return HandManager.instance.cannon;
        }
    }
    private float cannonStrength;
    [HideInInspector] public HumanGloveController playerGlove
    {
        get {
            if (HandManager.instance == null)
            {
                return null;
            }
            return HandManager.instance.glove;
        }
    }

    //OPTIONS
    private struct PassData
    {
        public DefensePlayer sender;
        public DefensePlayer receiver;
        public Vector3 targetCatchPosition;
        public PassPriority priority;
        public float delay;
        public bool useCutoffMan;
    }
    private enum PassPriority
    {
        PLAYER, //goal is to get ball to other player
        POSITION //goal is to get ball to place on field, doesn't matter who
    }

    //MAGIC NUMBERS
    private const float typicalCatchHeight = 1.5f;

    //DEBUG
    public string debug_baseCoverage
    {
        get
        {
            string str = "";
            for(int i = 0; i<baseCoverage.Length; i++)
            {
                str += (baseCoverage[i] ? baseCoverage[i].fieldPosition.ToString() : "---") + "\n";
            }
            return str;
        }
    }
    public string debug_baseCoveragePool
    {
        get
        {
            string str = "";
            for (int i = 0; i < baseDefenders.Count; i++)
            {
                str += (baseDefenders[i].fieldPosition.ToString() ) + "\n";
            }
            return str;
        }
    }
    public string debug_ballChasers
    {
        get
        {
            string str = "";
            for (int i = 0; i < ballChasers.Count; i++)
            {
                str += (ballChasers[i].fieldPosition.ToString()) + "\n";
            }
            return str;
        }
    }
    public string debug_backupPlayers
    {
        get
        {
            string str = "";
            for (int i = 0; i < backupCoverage.Length; i++)
            {
                str += (backupCoverage[i] ? backupCoverage[i].fieldPosition.ToString() : "---") + "\n";
            }
            return str;
        }
    }
    public string debug_backupPool
    {
        get
        {
            string str = "";
            for (int i = 0; i < backupPlayers.Count; i++)
            {
                str += (backupPlayers[i].fieldPosition.ToString()) + "\n";
            }
            return str;
        }
    }
    public string debug_ballControl
    {
        get
        {
            if (!playerWithBallControl) return "---";
            return playerWithBallControl.fieldPosition.ToString();
        }
    }

    public BallGate activeStrikeGate
    {
        get
        {
            return FieldMonitor.Instance.strikeGate;
        }
    }

    [System.Serializable]
    public struct OutfieldPositionContainer
    {
        public DefenseFieldPosition positionName;
        public Transform positionObject;
    }

    void Awake()
    {
        if (Instance == null) Instance = this;
        else GameObject.Destroy(this);
    }



    //TODO: should clear roles, reset players, etc.
    public override void InstantReset()
    {
        base.InstantReset();
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        if (humanControlled)
        {

            //pitcherCannon.OnBallFired += ReactToBallLaunchedByHuman;
            HandCannonController.OnBallFired += ReactToBallLaunchedByHuman;
            CustomThrowable.OnBallThrown += ReactToBallLaunchedByHuman;


            HumanGloveController.onHumanBallCatch.AddListener(OnBallCaughtByHuman);
            HumanGloveController.onHumanBallRelease.AddListener(OnBallReleasedByHuman);

            HumanPosessionManager.ConstrainProjectionToRail(false);
            HumanPosessionManager.SetInitialPositionRotation(GetFieldPosition(DefenseFieldPosition.PITCHER), Quaternion.LookRotation(Vector3.back, Vector3.up));
            HumanPosessionManager.PosessPlayer(pitcher, resetPlayer: true, resetPlayArea: true, fade: true, slowMo: false, cancelIfAlreadyTethered: false);

            //generous strike gate for human pitchers
            FieldMonitor.Instance.SetStrikeGateDimensions(Vector2.one * 1.5f, Vector2.zero, Vector2.one * 2f);
            FieldMonitor.Instance.ShowHideStrikeMarker(true);

        }
        else
        {
            //pitcherCannon.OnBallFired -= ReactToBallLaunchedByHuman;
            HandCannonController.OnBallFired -= ReactToBallLaunchedByHuman;

            CustomThrowable.OnBallThrown -= ReactToBallLaunchedByHuman;

            HumanGloveController.onHumanBallCatch.RemoveListener(OnBallCaughtByHuman);
            HumanGloveController.onHumanBallRelease.RemoveListener(OnBallReleasedByHuman);

        }

        RefreshState();
    }

    void Update()
    {
        //this fail-safe should guarantee that if the ballInPlay is destroyed for any reason, the pitcher will pull out a fresh ball and play can continue
        if (FullGameDirector.Instance.ReadyForNewBall())
        {
//            Debug.Log("DefenseCoordinator Ready for New Ball Spawn BallIn PitcherGlove");
            SpawnBallInPitchersGlove();
        }

        if (humanControlled)
        {
            bool showOffscreenBallArrow = true;
            if (BallIsInPosession() && playerWithBallControl == HumanPosessionManager.currentlyTetheredPlayer) showOffscreenBallArrow = false;
            HumanHUDManager.Instance.ShowHideOffscreenBallIndicator(showOffscreenBallArrow);
        }

        if (ballChasers_dirty)
        {
            RefreshBallChasers();
        }
        if (baseCoverage_dirty)
        {
            RefreshBaseCoverage();
        }
        if (backupCoverage_dirty)
        {
            RefreshBackupCoverage();
        }

    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (playerWithBallControl != null)
        {
            Handles.color = Color.cyan;
            Handles.DrawWireDisc(playerWithBallControl.transform.position, Vector3.up, 1);
        }
    }
#endif



    #region Initialization

    /// <summary>
    /// Sets up critical fields, called in AIFieldPlayer.Start()
    /// </summary>
    public override void Initialize()
    {
        if (isInitialized) return;

        baseCoverage = new DefensePlayer[FieldMonitor.Instance.numBases];
        backupCoverage = new DefensePlayer[FieldMonitor.Instance.numBases + 1];

        PlayOverCondition = PlayersAtPitchPosition_PitcherHasBall;
        BallHolderInstructions = InitiateReturnToPitchPositions;
        BallThrowerInstructions = ReportForBaseDuty;
        BasePlayerInstructions = FindBaseAssignment;

        if (positionDict == null)
        {
            BuildPositionDictionary();
        }

        SpawnDefensePlayers();

        backupTargetsWithBallController = new Transform[FieldMonitor.Instance.numBases + 1];
        for (int i = 0; i < FieldMonitor.Instance.numBases; i++)
        {
            backupTargetsWithBallController[i] = FieldMonitor.Instance.basePlacements[i];
        }

        if (pitcher)
            backupTargetsWithBallController[backupTargetsWithBallController.Length - 1] = pitcher.transform;



        base.Initialize();

    }


    protected DefensePlayer CreateFielder(OutfieldPositionContainer positionContainer)
    {
        //Debug.LogError("Creating new fielder.");
        DefensePlayer fielder = Instantiate(FieldPlayerPrefab, transform.parent, false);

        fielder.position = positionContainer.positionObject.transform.localPosition;
        fielder.transform.rotation = Quaternion.identity;

        fielder.OnBallPosession += ReactToPlayerGainingPosessionOfBall;
        fielder.OnThrow += GiveDirectionsToBallThrower;
        fielder.OnPassThrow += HandlePassBetweenPlayers;
        fielder.OnMissedCatch += ReactToFumble;
        fielder.OnFumble += ReactToFumble;
        fielder.OnFieldPlayerStateChange += ReactToPlayerStateChange;
        fielder.OnHumanPosession += ReactToHumanPosession;
        fielder.OnHumanDeposession += ReactToHumanDeposession;

        fielder.SetFieldPosition(positionContainer.positionName, positionContainer.positionObject.position);
        //fieldPlayers[i] = fielder;
        positionToOutfielderDict.Add(positionContainer.positionName, fielder);
        FieldMonitor.RegisterDefensePlayer(fielder);
        fielder.Initialize();
        fielder.name = "Defense_" + positionContainer.positionName;
        return fielder;
    }

    /// <summary>
    /// Spawn an instance of Outfielder at each position
    /// </summary>
    protected virtual void SpawnDefensePlayers()
    {
        //Greg: This only makes sense since we're spawning them in position.
        //If we were spawning them at dugout and having them walkout, they'd be in Middle.
        FullGameDirector.currentInningFrame = FullGameDirector.InningFrame.Top;

        positionToOutfielderDict.Clear();

        TeamInfo team = FullGameDirector.homeTeam;
        PlayerInfo player;

        //fieldPlayers = new DefensePlayer[positions.Length];
        for (int i = 0; i < positions.Length; i++)
        {
            OutfieldPositionContainer positionContainer = positions[i];
            DefensePlayer fielder = CreateFielder(positionContainer);

            fielder.SetPlayerInfo(team, team.GetPlayerAssignedToPosition(positionContainer.positionName));
            //DressPlayerForTeam(fielder, team, true);

            switch (fielder.fieldPosition)
            {
                case DefenseFieldPosition.CATCHER:
                    infielders.Add(fielder);
                    break;
                case DefenseFieldPosition.CENTER_FIELDER:
                    outfielders.Add(fielder);
                    break;
                case DefenseFieldPosition.FIRST_BASE:
                    infielders.Add(fielder);
                    break;
                case DefenseFieldPosition.LEFT_FIELDER:
                    outfielders.Add(fielder);
                    break;
                case DefenseFieldPosition.PITCHER:
                    infielders.Add(fielder);
                    break;
                case DefenseFieldPosition.RIGHT_FIELDER:
                    outfielders.Add(fielder);
                    break;
                case DefenseFieldPosition.SECOND_BASE:
                    infielders.Add(fielder);
                    break;
                case DefenseFieldPosition.SHORTSTOP:
                    infielders.Add(fielder);
                    break;
                case DefenseFieldPosition.THIRD_BASE:
                    infielders.Add(fielder);
                    break;
            }
            fielder.InstantReset();

            DefensePlayer doppel = Instantiate(fielder);
            doppel.Initialize();
            doppel.SetFieldPosition(fielder.fieldPosition, GetFieldPosition(fielder.fieldPosition));
            doppel.name = fielder.name + "_doppelganger";
            doppelgangerDict.Add(fielder, doppel);
            doppelgangers.Add(doppel);
            doppel.gameObject.SetActive(false);

        }
        catcher = GetDefensePlayerForPosition(DefenseFieldPosition.CATCHER);
        if (catcher)
        {
            _catcherThrowTarget = catcher.GetComponentInChildren<ThrowTarget>();
            if (_catcherThrowTarget)
            {
                _catcherThrowTarget.enabled = false;
            }
        }
        pitcher = GetDefensePlayerForPosition(DefenseFieldPosition.PITCHER);

        //pitcher.fumbleChance = 0;
        //catcher.fumbleChance = 0f;
        //Greg:This is temp and should really just apply for when receiving pitches.

    }

    /// <summary>
    /// Assembles a dictionary for getting outfielder positions in local space
    /// </summary>
    void BuildPositionDictionary()
    {
        positionDict = new Dictionary<DefenseFieldPosition, Vector3>();
        foreach (OutfieldPositionContainer posContainer in positions)
        {
            positionDict.Add(posContainer.positionName, posContainer.positionObject.localPosition);
        }
    }


    #endregion

    #region GetInfo
    /// <summary>
    /// Gets a field position in local space
    /// </summary>
    /// <param name="positionName">The name of the position</param>
    /// <returns>World-space position for the given field position</returns>
    public Vector3 GetFieldPosition(DefenseFieldPosition positionName)
    {
        if (positionDict == null)
        {
            BuildPositionDictionary();
        }

        Vector3 pos;
        if (positionDict.TryGetValue(positionName, out pos))
        {
            return pos;
        }
        //Debug.LogWarning("Could not find field position " + positionName);
        return Vector3.zero;
    }

    /// <summary>
    /// Get the outfielder assigned to a given field position
    /// </summary>
    /// <param name="positionName"></param>
    /// <returns></returns>
    public DefensePlayer GetDefensePlayerForPosition(DefenseFieldPosition positionName)
    {
        DefensePlayer o;
        if (positionToOutfielderDict.TryGetValue(positionName, out o))
        {
            return o;
        }
        return null;
    }

    public DefensePlayer GetPlayerWithBallControl()
    {
        return playerWithBallControl;
    }

    #endregion

    #region List Sorting
    List<FullGameDirector.RankedPosition> HighestToLowest(List<FullGameDirector.RankedPosition> positionList)
    {
        return positionList.OrderByDescending(x => x.value).ToList();
    }

    List<FullGameDirector.RankedPosition> LowestToHighest(List<FullGameDirector.RankedPosition> positionList)
    {
        return positionList.OrderBy(x => x.value).ToList();
    }

    float TimeToReachPosition(AIFieldPlayer player, Vector3 postition, int baseNo = -1)
    {
        return Vector3.Distance(player.position, postition) / player.runSpeed;
    }


    float TimeToReachBackUpPosition(AIFieldPlayer player, Vector3 position, int baseNo = -1)
    {
        Vector3 ballPos = FieldMonitor.ballInPlay.transform.position;
        ballPos.y = 0;
        Vector3 posToBall = (ballPos - position).normalized;

        float backupDistance = 10f;
        Vector3 backupPosition = position - posToBall * backupDistance;
        //Debug.DrawLine(position, backupPosition, Color.blue, 5);

        return TimeToReachPosition(player, backupPosition, baseNo);
    }


    float MinimumTimeToReach(List<RankedFieldPlayer<DefensePlayer>> playerPool, Vector3 pos, int baseNo = -1)
    {
        return playerPool[0].value;
    }

    /// <summary>
    /// Estimates the time until the backup will be useful at this position
    /// </summary>
    /// <param name="playerPool"></param>
    /// <param name="pos"></param>
    /// <param name="baseNo"></param>
    /// <returns></returns>
    float MostLikelyToRecieveBall(List<RankedFieldPlayer<DefensePlayer>> playerPool, Vector3 pos, int baseNo = -1)
    {
        //assume the catcher will never need backup
        if (baseNo == FieldMonitor.Instance.numBases - 1) return -1;

        //a number higher than the number of bases means that playerWithBallControl is being evaluated
        if (baseNo == FieldMonitor.Instance.numBases)
        {

            if (playerWithBallControl != null && (baseDefenders.Contains(playerWithBallControl)))
            {
                //ball control player iw covering a base, so we can assume a backup player will be covering his position by covering his base.
                return -1;
            }
            return 0;

        }

        bool forced;
        float timeToRunner = FieldMonitor.Instance.GetTimeToRunnerOnBase(baseNo, out forced);
        if (timeToRunner == float.MaxValue) return -1;
        //return distance to the position
        return timeToRunner;
    }
    #endregion

    #region Player Instructions

    /// <summary>
    /// A DefensePlayer put on base duty will be added to the basePlayers list.
    /// The basePlayers list is filtered using FindOptimalBaseAssignment, which tells any spare DefensePlayers without a base to cover to ReportForBackupDuty
    /// </summary>
    /// <param name="defPlayer"></param>
    public void ReportForBaseDuty(DefensePlayer defPlayer)
    {
        BasePlayerInstructions(defPlayer);
    }

    /// <summary>
    /// Tell DefensePlayer who just threw the ball what to do now
    /// </summary>
    /// <param name="thrower"></param>
    private void GiveDirectionsToBallThrower(DefensePlayer thrower)
    {
        BallThrowerInstructions(thrower);

    }


    /// <summary>
    /// Gets called when a player has just come in posession of a ball
    /// </summary>
    /// <param name="ballHolder"></param>
    private void ReactToPlayerGainingPosessionOfBall(DefensePlayer ballHolder)
    {
//        Debug.Log("REACT TO PLAYER GAINING POSSESSION OF BALL " + ballHolder.name);

        //if this was a successful pass, make sure passInProgress is set to false
        aIpassInProgress = false;

        //having a player in posession of the ball will change backup priorities
        backupCoverage_dirty = true;

        //everyone except the player who caught the ball should be put on base duty
        ballChasers_dirty = true;

        //the player who caught the ball was not the expected catcher, this shouldn't happen
        if (playerWithBallControl != null && ballHolder != playerWithBallControl)
        {
            ReportForBaseDuty(playerWithBallControl);
        }
        SetBallControlPlayer(ballHolder);


        //make sure player is not still chasing the ball


        if (!ballHolder.isTetheredToCamera)
        {
            ballHolder.Idle();
            StartCoroutine(WitholdInstructionsForFrameRoutine(ballHolder));
        }

        if(ballHolder.isTetheredToCamera && humanControlled)
        {
            HumanHUDManager.Instance.ShowHideOffscreenBallIndicator(false);
        }
    }

    /// <summary>
    /// Tell DefensePlayer who just acquried the ball what to do with it after an update.
    /// This gives OffenseCoordinator an opportunity to call outs and lets FieldManager update the FieldState    
    /// /// </summary>
    /// <param name="ballHolder"></param>
    /// <returns></returns>
    IEnumerator WitholdInstructionsForFrameRoutine(DefensePlayer ballHolder)
    {
        witholdBallHolderDirections = true;
        yield return null;
        witholdBallHolderDirections = false;
    }

    /// <summary>
    /// Tells all defense players to return to pitch positions
    /// </summary>
    /// <param name="ballHolder"></param>
    private void InitiateReturnToPitchPositions(DefensePlayer ballHolder)
    {
        ReturnToPitchPositions();
    }

    /// <summary>
    /// Used as a BallThrowerInstruction
    /// </summary>
    private void DoNothing(DefensePlayer def)
    {

    }


    /// <summary>
    /// Chill out, have a snack
    /// </summary>
    /// <param name="ballHolder"></param>
    private void DoNothingWithBall(DefensePlayer ballHolder)
    {
        //nothing
        //eat ball?
    }

    private void PassBallToRandomOtherDefender(DefensePlayer ballHolder)
    {
        if (ballHolder.state != AIFieldPlayer.FieldPlayerAction.RESET)
            ballHolder.Reset();

        List<DefensePlayer> passTargets = new List<DefensePlayer>();
        foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
        {

            if (def != ballHolder)
            {
                if (def == null) Debug.LogError("Def was null");
                float dist = Vector3.Distance(ballHolder.position, def.position);
                if (dist > 10 && dist < 50)
                    passTargets.Add(def);
            }
        }

        if (passTargets.Count == 0) return;

        PassData data = new PassData();
        data.sender = ballHolder;
        data.receiver = passTargets[UnityEngine.Random.Range(0, passTargets.Count)];
        data.priority = PassPriority.PLAYER;

        PassBall(data);
    }



    /// <summary>
    /// Look for best way to defend bases with the ball
    /// </summary>
    /// <param name="ballHolder"></param>
    private void DefendBasesWithBall(DefensePlayer ballHolder)
    {
        if (witholdBallHolderDirections) return;

        //identify highest priority base for defending
        int targetBase = PickBaseToPassTo(ballHolder);


        //There's a target base, but no one is running to it yet. Pass the ball to the pitcher
        if (targetBase == -1)//!FieldManager.Instance.RunnerIsTargetingBase(targetBase))
        {
            RemoveFromBaseOrBackup(pitcher);
            pitcher.Reset();
            ReturnBallToPitcher(ballHolder);
            return;
        }

        AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> tagEstimate;
        if (CanTagARunnerOut(ballHolder, out tagEstimate))
        {
            //if it will be quicker to tag a runner than pass to a base, go for the tag
            if (tagEstimate.value < TimeToGetBallToBase(ballHolder, targetBase))
            {

                ballHolder.AttemptToTagRunner(tagEstimate.fieldPlayer);
                return;
            }
        }




        DefensePlayer targetReceiver = baseCoverage[targetBase];
        Vector3 targetCatchPosition = FieldMonitor.Instance.GetBaseDefensePosition(targetBase);
        float time_ReceiverInPosition = TimeUntilBaseIsCovered(targetBase);
        float time_ThrowToPosition = ballHolder.GetTimeToThrowToPosition(targetCatchPosition, ballHolder.glove.heldBall.transform.position);
        float time_RunToBase = ballHolder.GetTimeToRunToPosition(targetCatchPosition);

        //if the ball holder is already supposed to be covering the base
        //or if it will be faster to run
        //run to the base
        if ((baseCoverage[targetBase] != null && baseCoverage[targetBase] == ballHolder)
            || (time_RunToBase < time_ReceiverInPosition))
        {
            ReportForBaseDuty(ballHolder);
        }

        //otherwise pass the ball
        else
        {
            PassData pass = new PassData();
            pass.sender = ballHolder;
            pass.receiver = targetReceiver;
            pass.targetCatchPosition = targetCatchPosition;
            pass.delay = .1f;
            pass.useCutoffMan = true;
            pass.priority = PassPriority.POSITION;
            PassBall(pass);
        }
    }

    /// <summary>
    /// Would this ball holder be able to get any runners out with either a tag or base defense?
    /// </summary>
    /// <param name="ballHolder"></param>
    /// <returns></returns>
    private bool CanGetARunnerOutWithBall(DefensePlayer ballHolder)
    {
        //NEED WORK
        if (ballHolder == null) return false;
        Debug.Log(CanTagARunnerOut(ballHolder) + " " + CanDefendABase(ballHolder));
        return (CanTagARunnerOut(ballHolder) || CanDefendABase(ballHolder));
    }

    /// <summary>
    /// Would this ball holder be able to successfuly defend a base that is being targeted by a runner
    /// </summary>
    /// <param name="ballHolder"></param>
    /// <returns></returns>
    private bool CanDefendABase(DefensePlayer ballHolder)
    {
        if (ballHolder == null) return false;

        return QuickestBasesToDefend(ballHolder).Count > 0;

        //if value is <0, there is no way this player will be able to get the ball to the base before the runner
        //FullGameDirector.RankedBase urgentBase = QuickestBasesToDefend(ballHolder)[0];
        //Debug.Log("uregent base  " + urgentBase.baseNo + " " + urgentBase.value);
        //return urgentBase.value > 0;// && FieldManager.Instance.RunnerIsTargetingBase(urgentBase.baseNo);
    }

    /// <summary>
    /// Would this ball holder be able to tag a runner out? also spits out the tag estimate
    /// </summary>
    /// <param name="ballHolder"></param>
    /// <param name="tagEstimate"></param>
    /// <returns></returns>
    private bool CanTagARunnerOut(DefensePlayer ballHolder, out AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> tagEstimate)
    {
        List<AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>> tagEstimates = FieldMonitor.Instance.RunnerInterceptionEstimates(ballHolder);
        if (tagEstimates.Count > 0)
        {
            tagEstimate = tagEstimates[0];
            return (tagEstimate.value < float.MaxValue);
        }
        tagEstimate = new AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>(null, float.MaxValue, Vector3.zero);
        return false;
    }

    /// <summary>
    /// Would this ballholder be able to tag a runner out?
    /// </summary>
    /// <param name="ballHolder"></param>
    /// <returns></returns>
    private bool CanTagARunnerOut(DefensePlayer ballHolder)
    {
        AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> tagEstimate;
        return CanTagARunnerOut(ballHolder, out tagEstimate);
    }

    /// <summary>
    /// Get the ball to the pitcher
    /// </summary>
    /// <param name="ballHolder"></param>
    private void ReturnBallToPitcher(DefensePlayer ballHolder)
    {
        if (witholdBallHolderDirections) return;

        PassData data = new PassData();
        data.sender = ballHolder;
        data.receiver = pitcher;

        //if the pitcher is holding the ball, make sure they're resetting to pitch position
        if (ballHolder == pitcher)
        {
            if (!pitcher.atPitchPosition)
            {
                ballHolder.Reset();
            }
            return;
        }

        else if (!pitcher.isTetheredToCamera)
        {
            data.priority = PassPriority.PLAYER;
            PassBall(data);
        }
        else
        {
            data.targetCatchPosition = pitcher.position;
            data.priority = PassPriority.POSITION; //humans will not move in straight line, throw to where they are right now, with slight delay
            data.delay = .2f;
            PassBall(data);
        }


    }

    /// <summary>
    /// Get the ball to the pitcher
    /// </summary>
    /// <param name="ballHolder"></param>
    private void DestroyHeldBall(DefensePlayer ballHolder)
    {
        Debug.Log("Destroy Held ball.");
        SpawnBallInPitchersGlove();
        EndPlay();
    }

    /// <summary>
    /// React to a defenseplayer failing to get ahold of the ball
    /// </summary>
    /// <param name="fumbler"></param>
    void ReactToFumble(DefensePlayer fumbler)
    {
        //Im using out for this, because that's the field action that causes the WHOA face. There is no field action for fumbles.
        fumbler.MoodChange(fumbler, AIFieldPlayer.FieldPlayerAction.OUT);

        //if a human pitcher missed an AI pass, and there's no action on the bases, just explode it and spawn a fresh one for them
        if ((FieldMonitor.Instance.AllRunnersOnBase() || runningPlay == FieldPlayType.WALK) 
            && fumbler == pitcher 
            && fumbler.isTetheredToCamera
            && aIpassInProgress
            && (FieldMonitor.ballInPlay && FieldMonitor.ballInPlay.isHeld == false)
            )
        {

            FieldMonitor.Instance.ExplodeBall();
        }


        //if this was a failed pass, make sure passInProgress is set to false
        aIpassInProgress = false;

        if (fumbler == playerWithBallControl)
        {
            SetBallControlPlayer(null);
        }
        AddBallChaser(fumbler);


        //Debug.Log("++++++++++++++++++++++++REACT TO FUMBLE 02 " + fumbler.name + " " + runningPlay + " " + fumbler.isTetheredToCamera);
        


        //MARC:
        //counting on UpdateBallChasers to give fumbler new instruction. But if this happens to be called in a state where ball chasers are not refreshed, fumbler gets stuck.
    }

    #endregion

    #region DirectedInstructions
    public void DirectPlayerToRunToTarget(DefensePlayer def, Transform t)
    {
        FieldMonitor.DrawDebugLine(def.position, (t.position), Color.yellow, 1);
        def.RunToTarget(t);
    }

    public void DirectPlayerToBase(DefensePlayer def, int baseNo)
    {
        Vector3 pos = FieldMonitor.Instance.GetBaseDefensePosition(baseNo);
        FieldMonitor.DrawDebugLine(def.position, (pos), Color.yellow, 1);
        def.RunToBase(baseNo);
    }

    public void DirectPlayerToBackupTransform(DefensePlayer def, Transform t)
    {
        def.BackupTarget(t);
    }


    #endregion


    #region Posession


    void ReactToHumanPosession(AIFieldPlayer player)
    {
        //MARC: Removing this because it should never happen
        /*
        if (playerGlove.hasBall && playerGlove.GetCurrentBall().inGlove)
        {
            DefensePlayer def = player as DefensePlayer;
            def.glove.ForceCatchBall(playerGlove.GetCurrentBall());
        }
        */

    }

    void ReactToHumanDeposession(AIFieldPlayer player)
    {
        if (runningPlay != FieldPlayType.STANDARD) player.InstantReset();
    }



    #endregion

    #region FieldPlay Control
    /// <summary>
    /// Begin a play with a ball that was just hit
    /// </summary>
    /// <param name="ball">Ball that was just hit.</param>
    /// <param name="playType">Indicates if this ball is a home run or foul. Otherwise it will be normal.</param>
    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType playType)
    {
        ClearRoles();

        if (ball && ball.visualAssist)
        {
            ball.visualAssist.SetActive(true);
        }

        if (playType == FieldPlayType.STANDARD)
        {
            foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
            {
                //def.Hesitate(UnityEngine.Random.Range((.2f), (.6f)));
            }
        }

        SetPlayType(playType);




    }

    public void EndPlay()
    {

//        Debug.Log("Defense END PLAY");
        isReadyToPitch = false;

        //reset to pitcher's mound as well
        /*
        if (playerLocomotion)
        {
            playerLocomotion.armSwingNavigation = false;
        }
        */
        if (humanControlled)
        {
            //MARC: Coordinators ideally shouldn't tell eachother what to do
            //FieldManager.Instance.offense.EndPlay();
            //HumanPosessionManager.SetArmLocomotionEnabled(false);

        }

        SetPlayType(FieldPlayType.NONE);

        //playArea.transform.position = initialVRPosition;
        //playArea.transform.rotation = initialVRRotation;
    }

    public void ReturnToPitchPositions()
    {
        //Debug.Log("Reset to Pitch Positions");
        SetPlayType(FieldPlayType.RETURN);
    }

    public hittable SpawnBallInPitchersGlove()
    {
        if (pitcher == null) { return null; }
        else if (pitcher.glove.heldBall != null) { return pitcher.glove.heldBall; }
        hittable ball = FieldMonitor.Instance.SpawnFreshBall();

        if (humanControlled && pitcher.isTetheredToCamera)
        {
            playerGlove.CatchBall(ball);
        }
        else
        {
            pitcher.glove.ForceCatchBall(ball);
        }
        return ball;
    }

    /*
    IEnumerator EnforceBallToPlayerGlove(hittable ball)
    {
        Debug.Log("make sure it's ripe for the player's glove to take control.");
        TrajTester1 ballTraj = ball.GetComponentInChildren<TrajTester1>();
        playerGlove.trajToWatch = ballTraj;
        while (ballTraj.isCaught == false)
        {
            Debug.Log("still trying to get the ball to the player glove.");
            ball.GetRigidbody().isKinematic = true;
            ball.transform.position = playerGlove.catchPosition.position;
            yield return null;
        }
    }
    */
    #endregion


    #region Conditions

    bool PlayersAtPitchPosition()
    {
        return (FieldMonitor.Instance.AllDefendersAtPitchPosition());
    }

    bool PlayersAtPitchPosition_PitcherHasBall()
    {
        return PlayersAtPitchPosition() && PitcherHasTheBall();
    }

    bool PitcherOnMoundWithBall()
    {
        return FieldMonitor.Instance.PitcherOnMoundWithBall();
    }

    bool PitcherOnMound()
    {
        return FieldMonitor.Instance.PitcherOnMound();
    }

    bool HomerunComplete()
    {
        return FieldMonitor.offensivePlayers.Count == 0 && PlayersAtPitchPosition();
    }

    bool PitcherHasTheBall()
    {
        if (pitcher.isTetheredToCamera && pitcher.inPosessionOfBall)//Greg: probably inelligant but I added this. it may not even need to check if tethered to camera??
        {
            return true;
        }

        if (playerWithBallControl != null
            && playerWithBallControl.inPosessionOfBall
            && playerWithBallControl == pitcher)
        {
            return true;
        }
        return false;
    }

    

    public bool BallIsInPosession()
    {
        return (playerWithBallControl != null && playerWithBallControl.inPosessionOfBall);
    }

    public bool Never()
    {
        return false;
    }
    #endregion


    #region Player Roles

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ball"></param>
    /// <returns></returns>
    bool FindPlayerToCatchBall(hittable ball, out float timeToCatch, bool prioritizeBallControlPlayer = false, bool prioritizeBaseCoveragePlayers = false)
    {
        timeToCatch = 0;
        airInterceptors = FieldMonitor.Instance.AirInterceptionEstimates(ball);
        AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> best;
        //        Debug.Log("Find Player TO Catch Ball " + prioritizeBallControlPlayer + "  " + prioritizeBaseCoveragePlayers);
        //found someone to catch the ball!!!!

        if (airInterceptors.Count > 0)
        {
            best = airInterceptors[0];

            AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> priority;

            if (prioritizeBallControlPlayer
                && playerWithBallControl != null)
            {
                priority = airInterceptors.Find(x => (x.fieldPlayer == playerWithBallControl));
                if (priority!=null && priority.fieldPlayer != null)
                {
                    best = priority;
                }
            }

            if (prioritizeBaseCoveragePlayers)
            {
                //Debug.Log("_____________________________________________________________" + best);
                DefensePlayer ballThrower = playerWithBallControl;
                if (humanControlled)
                {
                    ballThrower = HumanPosessionManager.currentlyTetheredPlayer as DefensePlayer;//Greg: without this it seemed like ball thrower was null.
                }
                foreach (FullGameDirector.RankedBase b in QuickestBasesToDefend(ballThrower, ball.GetVelocity().magnitude))
                {
                    priority = airInterceptors.Find(x => (x.fieldPlayer == baseCoverage[b.baseNo] && !x.fieldPlayer.isTetheredToCamera));
                    if (priority != null && priority.fieldPlayer)
                    {
                        //Debug.Log("best is found. " + best.fieldPlayer);
                        best = priority;
                        break;
                    }
                }
            }
            //Debug.Log("final best pick " + best + " time:" + best.value);
            RemoveFromBaseOrBackup(best.fieldPlayer);
            best.fieldPlayer.CatchBallAtPosition(best.position, ball);
            timeToCatch = best.value;
            SetBallControlPlayer(best.fieldPlayer);
            return true;


        }
        return false;
    }


    /// <summary>
    /// Look for someone to catch the ball, 
    /// </summary>
    public void AssignInitialRoles()
    {
        hittable ball = FieldMonitor.ballInPlay;

        float timeToCatch = 0;
        //found someone to catch the ball!!!!
        if (BallIsInPosession() || FindPlayerToCatchBall(ball, out timeToCatch, prioritizeBallControlPlayer: true))
        {
            //Debug.LogError("setting all on base except  " + playerWithBallControl);

            //put all other players on base duty
            foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
            {
                if (player != playerWithBallControl)
                {
                    ReportForBaseDuty(player);
                }
            }
        }


        //Will need players to chase the ball
        //ballPlayers list will be pruned in UpdateBallPlayers()
        else
        {
            //            Debug.LogError("setting all on chase");
            //if no one has the ball or can catch the ball, ballcontrolPlayer should be null
            SetBallControlPlayer(null);
            foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
            {
                AddBallChaser(def);
            }

        }
    }

    void ClearRoles()
    {
        //SetBallControlPlayer(null); Setting this to null here created problems when a player was currently holding the ball
        baseCoverage = new DefensePlayer[FieldMonitor.Instance.numBases];
        baseDefenders = new List<DefensePlayer>();
        ballChasers = new List<DefensePlayer>();
        backupPlayers = new List<DefensePlayer>();
        backupCoverage = new DefensePlayer[FieldMonitor.Instance.numBases + 1];
    }


    private void RemoveFromBaseOrBackup(DefensePlayer player)
    {
        if (baseDefenders.Contains(player))
        {
            //Debug.Log("remove from base or back up " + player.name);
            baseDefenders.Remove(player);
            for (int i = 0; i < baseCoverage.Length; i++)
            {
                if (baseCoverage != null && baseCoverage[i] == player)
                    baseCoverage[i] = null;
            }
            baseCoverage_dirty = true;
        }

        if (backupPlayers.Contains(player))
        {
            //            Debug.Log("remove from base or back up " + player.name);
            backupPlayers.Remove(player);
            for (int i = 0; i < backupCoverage.Length; i++)
            {
                if (backupCoverage != null && backupCoverage[i] == player)
                    backupCoverage[i] = null;
            }
            backupCoverage_dirty = true;
        }

        //MARC: if there are less than enough base players to cover all bases, draw from backup players
        if (baseDefenders.Count < FieldMonitor.Instance.numBases)
        {
            foreach (DefensePlayer def in backupPlayers)
            {
                ReportForBaseDuty(def);
            }
            baseCoverage_dirty = true;
            backupPlayers.Clear();
            backupCoverage_dirty = true;
        }
    }


    #region Base Coverage
    void AssignInitialBaseCoverage()
    {
        foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
        {
            baseDefenders.Add(def);
        }
        baseCoverage_dirty = true;
    }

    void FindBaseAssignment(DefensePlayer defPlayer)
    {
        baseDefenders.Add(defPlayer);
        baseCoverage_dirty = true;
    }

    void RefreshBaseCoverage()
    {
        baseDefenders = FindOptimalPositionCoverage(
            baseDefenders,
            FieldMonitor.Instance.GetBaseDefensePositions(),
            //FieldManager.Instance.GetBaseTransforms(),
            baseCoverage,
            TimeToReachPosition,
            MinimumTimeToReach,
            HighestToLowest,         //order the positions by minimum time to get covered, descending
            DirectPlayerToRunToTarget,
            ReportForBackupDuty
            );
        baseCoverage_dirty = false;
    }

    /// <summary>
    /// Takes a list of DefensePlayers and assigns them to bases in a way that minimizes uncovered time for each base
    /// </summary>
    /// <param name="playerPool">Returns DefensePlayers who are still on base duty after filtering</param>
    List<DefensePlayer> FindOptimalPositionCoverage(
        List<DefensePlayer> playerPool,
        Transform[] positions,
        DefensePlayer[] positionCoverage,
        PlayerEvaluation playerEval,
        PositionEvaluation positionEval,
        RankedPositionSort prioritySort,
        DirectedInstructions coverageInstruction,
        Instructions surplusInstruction)
    {

        // Debug.Log("optimize base assignments");
        List<FullGameDirector.RankedPosition> rankedPositions = new List<FullGameDirector.RankedPosition>();
        List<DefensePlayer> claimedPlayers = new List<DefensePlayer>();
        Dictionary<int, List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>>> playersByDistanceToPosition = new Dictionary<int, List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>>>();

        for (int i = 0; i < positions.Length; i++)
        {
            Vector3 pos = (positions[i].position);

            List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> playersByDistance = new List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>>();
            foreach (DefensePlayer defPlayer in playerPool)
            {
                float playerValue = playerEval(defPlayer, pos, i);
                playersByDistance.Add(new AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>(defPlayer, playerValue, pos));

            }
            if (FieldMonitor.ballInPlay)
            {
                playersByDistance = playersByDistance
                    .OrderBy(x => x.value, new ApproxCompare(.1f))
                    .ThenByDescending(x => Vector3.Distance(x.fieldPlayer.transform.position, FieldMonitor.ballInPlay.transform.position))
                    .ToList();

                playersByDistanceToPosition.Add(i, playersByDistance);
                if (playersByDistance.Count > 0)
                {
                    FullGameDirector.RankedPosition rankedPos = new FullGameDirector.RankedPosition(pos, positionEval(playersByDistance, pos, i), i);
                    //if the eval returns a negative, prune out this position
                    if (rankedPos.value >= 0)
                        rankedPositions.Add(rankedPos);
                }
            }

        }
        rankedPositions = prioritySort(rankedPositions);


        foreach (FullGameDirector.RankedPosition rankedPos in rankedPositions)
        {
            //pick the closest available player that isn't already claimed
            int posIndex = rankedPos.index;
            List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> playersByDistance = playersByDistanceToPosition[posIndex];
            for (int i = 0; i < playersByDistance.Count; i++)
            {
                DefensePlayer def = playersByDistance[i].fieldPlayer;
                //if this player has not already been claimed for base duty
                if (!claimedPlayers.Contains(def))
                {
                    //base is not currently covered
                    if (positionCoverage[posIndex] == null)
                    {
                        positionCoverage[posIndex] = def;
                        coverageInstruction(def, positions[posIndex]);
                    }
                    //selected Player is already covering the base
                    else if (positionCoverage[posIndex] == def)
                    {
                        //Don't change state of player who is trying to catch the ball
                        if (positionCoverage[posIndex] != playerWithBallControl)
                        {
                            coverageInstruction(def, positions[posIndex]);
                        }

                    }
                    //someone else is covering the base and should be reset
                    else
                    {
                        positionCoverage[posIndex] = def;
                        coverageInstruction(def, positions[posIndex]);
                    }
                    claimedPlayers.Add(def);
                    i = playersByDistance.Count;
                }
            }
        }

        foreach (DefensePlayer def in playerPool)
        {
            if (!claimedPlayers.Contains(def))
            {
                surplusInstruction(def);
            }
        }

        return claimedPlayers;

    }

    public bool PlayerShouldBeRecruitedForBaseDuty(DefensePlayer def)
    {
        for (int i = 0; i < FieldMonitor.Instance.numBases; i++)
        {
            if (PlayerShouldBeConsideredToCoverBase(def, i))
            {
                //Debug.Log(def.name + " is better to cover " + i + " than " + baseCoverage[i]);
                return true;
            }
        }
        return false;
    }

    public bool PlayerShouldBeConsideredToCoverBase(DefensePlayer def, int baseNo)
    {

        if (baseCoverage[baseNo] == def) return true;
        if (TimeUntilBaseIsCovered(baseNo) > def.GetTimeToRunToBase(baseNo)) return true;

        return false;

    }

    public float TimeUntilBaseIsCovered(int baseNo)
    {
        if (baseCoverage[baseNo] == null) return float.MaxValue;
        return (Vector3.Distance(baseCoverage[baseNo].position, FieldMonitor.Instance.GetBaseDefensePosition(baseNo)) / baseCoverage[baseNo].runSpeed);
    }

    #endregion

    #region Backup Duty

    public void ReportForBackupDuty(DefensePlayer defPlayer)
    {
        BackupPlayerInstructions(defPlayer);
    }

    public void FindBackupAssignment(DefensePlayer backup)
    {
        //        Debug.Log(backup.name + " REPORT for backup duty");
        if (!backupPlayers.Contains(backup))
            backupPlayers.Add(backup);
        backupCoverage_dirty = true;
    }


    void RefreshBackupCoverage()
    {
        bool coverBallControl = false;
        if (playerWithBallControl != null && !BallIsInPosession())
        {
            backupTargetsWithBallController[backupTargetsWithBallController.Length - 1] = playerWithBallControl.transform;
            //            Debug.Log("backup refresh " + backupTargetsWithBallController[0]);
            coverBallControl = true;
        }
        FindOptimalPositionCoverage(
            backupPlayers, //player pool
            coverBallControl ? backupTargetsWithBallController : FieldMonitor.Instance.basePlacements, //positions
            backupCoverage, //position coverage
            TimeToReachBackUpPosition, //
            MostLikelyToRecieveBall, //eval
            LowestToHighest, //priority sort
            DirectPlayerToBackupTransform, //backup instruction
            ReturnToPitchPosition //surplus instruction
            );

        backupCoverage_dirty = false;
        //ball catcher
        //first base
        //second base
        //third base
    }

    #endregion

    #region Ball Chasers
    private List<DefensePlayer> newChaserSelection_cached = new List<DefensePlayer>();
    private List<DefensePlayer> chasersToRemove_cached = new List<DefensePlayer>();
    /// <summary>
    /// Looks at entire list of DefensePlayers and decides who should be going after the ball right now
    /// </summary>
    public void RefreshBallChasers()
    {
        bool someoneHasBallControl = (playerWithBallControl != null);
        chasersToRemove_cached.Clear();

        //If someone's going to catch the ball, or its no longer in the park, everyone else on the ballChasers list should be bumped down to base duty
        if (someoneHasBallControl || FieldMonitor.Instance.BallHasLeftPark())
        {

            foreach (DefensePlayer def in ballChasers)
            {
                    chasersToRemove_cached.Add(def);
            }
        }

        //if no one is going to catch the ball, keep 2 ball chasers active until it's acquired
        else if (FieldMonitor.Instance)
        {
            int numDesiredChasers = 2;
            int numFoundChasers = 0;
            newChaserSelection_cached.Clear();

            List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> rankedChasers = FieldMonitor.Instance.GroundInterceptionEstimates(FieldMonitor.ballInPlay);
            //look for nearest outfielders who are either available or already chasing the ball
            foreach (AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> rankedChaser in rankedChasers)
            {

                //if the human player is currently posessing a player standing on a base, do not take them off base coverage unless they step off the base
                if (rankedChaser.fieldPlayer.isTetheredToCamera)
                {
                    int baseNo = -1;
                    if (FieldMonitor.Instance.DefenderIsOnBase(rankedChaser.fieldPlayer, out baseNo))
                    {
                        if (rankedChaser.fieldPlayer != baseCoverage[baseNo])
                            ReportForBaseDuty(rankedChaser.fieldPlayer);
                        continue;

                    }
                }

                //if there's already a more promising chaser and this player is better used on a base, put him on a base
                if (numFoundChasers >= 1 && PlayerShouldBeRecruitedForBaseDuty(rankedChaser.fieldPlayer))
                {
                    continue;
                }

                RemoveFromBaseOrBackup(rankedChaser.fieldPlayer);
                if (!ballChasers.Contains(rankedChaser.fieldPlayer))
                {
                    ballChasers.Add(rankedChaser.fieldPlayer);
                }
                newChaserSelection_cached.Add(rankedChaser.fieldPlayer);
                numFoundChasers++;

                if (numFoundChasers == numDesiredChasers) break;

            }

            foreach (DefensePlayer oldChaser in ballChasers)
            {
                if (!newChaserSelection_cached.Contains(oldChaser))
                {
                    chasersToRemove_cached.Add(oldChaser);
                }
            }
        }

        foreach (DefensePlayer rem in chasersToRemove_cached)
        {
            ballChasers.Remove(rem);
            if (someoneHasBallControl && rem == playerWithBallControl)
            {

            }
            else
            {
                ReportForBaseDuty(rem);
            }
        }

        foreach(DefensePlayer chaser in ballChasers)
        {
            BallChaserInstructions(chaser);
        }

        ballChasers_dirty = false;
    }

    private void AddBallChaser(DefensePlayer bPlayer)
    {
        ballChasers.Add(bPlayer);
        ballChasers_dirty = true;
    }


    //Ball chaser INSTRUCTION
    private void ChaseBall(DefensePlayer def)
    {
        def.ChaseGroundBall(FieldMonitor.ballInPlay);

    }


    #endregion

    #region Ball Control
    public void UpdatePlayerWithBallControl()
    {
//        Debug.Log("update ball control player  " + playerWithBallControl);
//        if (playerWithBallControl) Debug.Log(" needs instructions? " + playerWithBallControl.needsInstructions);
        if (playerWithBallControl == null) return;
        else if (playerWithBallControl.isTetheredToCamera) return;
        else if (!playerWithBallControl.inPosessionOfBall) return;
        else if (witholdBallHolderDirections) return;
        else if (!playerWithBallControl.needsInstructions) return;
        else
        {
            BallHolderInstructions(playerWithBallControl);
        }

    }

    public void SetBallControlPlayer(DefensePlayer control)
    {
#if UNITY_EDITOR
//        Debug.Log("Set ball control player " + control);
#endif

        //be sure to cancel any throws in progress
        if(playerWithBallControl != null)
        {
            playerWithBallControl.CancelThrowAnimation();
        }
        if (backupPlayers.Contains(control))
        {
            backupPlayers.Remove(control);
            backupCoverage_dirty = true;
        }
        playerWithBallControl = control;
        ballChasers_dirty = true;
    }

    #endregion

    #endregion


    #region Pass_Logic


    private void PassBall(PassData pass)
    {
        if (pass.priority == PassPriority.POSITION && pass.useCutoffMan)
        {
            DefensePlayer cutoff;
            Vector3 cutoffPosition;
            //Debug.Log("look for cutoff man");

            if (TryToFindCutoffManForPass(pass, out cutoff, out cutoffPosition))
            {
                //Debug.Log("use cutoff man " + cutoff + "  " + cutoffPosition);
                pass.receiver = cutoff;
                pass.targetCatchPosition = cutoffPosition;
                cutoff.RunToTarget(cutoffPosition);
                pass.priority = PassPriority.PLAYER;
            }
        }
        StartCoroutine(PassBallRoutine(pass));
    }

    private IEnumerator PassBallRoutine(PassData pass)
    {
        switch (pass.priority)
        {
            //priority is the player, should never have to wait unless there's a delay
            case PassPriority.PLAYER:

                AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception = FieldMonitor.Instance.FindPassingInterceptionPoint(pass.sender, pass.receiver);
                float time_RecieverToTarget = pass.receiver.GetTimeToRunToPosition(pass.receiver.moveTargetPosition);
                //if thrown now, will the ball be able to intercept receiver on their path of travel?
                if (interception.value > 0 && interception.value < time_RecieverToTarget)
                {
                    pass.sender.BeginWindup();

                    //will re-calculate interception point after animation has played, this was just an estimate
                    yield return new WaitForSeconds(pass.delay);
                    pass.sender.BeginThrow(pass.receiver);
                }
                //otherwise throw to their destination
                else
                {
                    pass.sender.BeginWindup();

                    yield return new WaitForSeconds(pass.delay);
                    pass.sender.BeginThrow(pass.receiver, pass.receiver.moveTargetPosition);
                }

                break;


            //priority is the position, wait until receiver will be able to catch at target position by the time it arrives
            case PassPriority.POSITION:

                SetBallControlPlayer(pass.receiver);
                pass.receiver.RunToTarget(pass.targetCatchPosition);

                float time_ThrowToPosition = 0;
                float time_ReceiverToCatchPosition = 0;
                float time_ThrowerToCatchPosition = 1;


                //while the reciever is not close enough to catch 
                while (time_ThrowToPosition <= time_ReceiverToCatchPosition
                    && time_ThrowerToCatchPosition > time_ReceiverToCatchPosition)
                {
                    yield return null;
                    time_ThrowToPosition = pass.sender.GetTimeToThrowToPosition(pass.targetCatchPosition);
                    time_ReceiverToCatchPosition = pass.receiver.GetTimeToRunToPosition(pass.targetCatchPosition);
                    time_ThrowerToCatchPosition = pass.sender.GetTimeToRunToPosition(pass.targetCatchPosition);
                }

                if (time_ThrowerToCatchPosition < time_ReceiverToCatchPosition)
                {
                    SetBallControlPlayer(pass.sender);
                }
                else
                {
                    pass.sender.BeginWindup();

                    yield return new WaitForSeconds(pass.delay);
                    pass.sender.BeginThrow(pass.receiver, pass.targetCatchPosition);
                }


                break;
        }
    }

    /// <summary>
    /// Gets called when the ball leaves the hand of the thrower
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    private void HandlePassBetweenPlayers(DefensePlayer from, DefensePlayer to, Vector3 catchPosition, hittable ball)
    {
        aIpassInProgress = true;
        FieldMonitor.DrawDebugLine(catchPosition, catchPosition + Vector3.up * 6, Color.green, 5);
        SetBallControlPlayer(to);
        to.CatchBallAtPosition((catchPosition), ball);
    }

    private bool TryToFindCutoffManForPass(PassData pass, out DefensePlayer cutoff, out Vector3 cutoffPoint)
    {
        //guidelines
        //base defenders should not leave base to be a cutoff man
        //cutoff man should move 

        cutoff = null;
        cutoffPoint = Vector3.zero;
        Vector3 passPath = pass.targetCatchPosition - pass.sender.position;

        //no cutoff man needed
        float homeToMound = Vector3.Distance(FieldMonitor.Instance.GetPitchersMoundPosition(), FieldMonitor.Instance.GetHomePlatePosition());
        if (passPath.magnitude <= homeToMound * 2f + 1) return false;

        float minDistanceOffPassPath = float.MaxValue;
        Vector3 toOther;
        Vector3 pointOnPath;
        float dot;

        foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
        {
            if (def == pass.sender || def == pass.receiver) continue;

            //avoid passes that are shorter than the distance from mound to plate
            if (Vector3.Distance(def.position, pass.sender.position) < homeToMound) continue;
            if (Vector3.Distance(def.position, pass.receiver.position) < homeToMound) continue;


            toOther = (def.position - pass.sender.position);
            dot = Vector3.Dot(toOther, passPath.normalized);

            if (dot < 0 || dot > passPath.magnitude) continue;

            pointOnPath = pass.sender.position + passPath.normalized * dot;
            Debug.DrawRay(pointOnPath, Vector3.up * 3, Color.yellow, 2);
            Debug.DrawLine(pointOnPath, def.position, Color.yellow, 2);
            float distanceOffPassPath = 
                Vector3.Distance(
                    pointOnPath, 
                    def.position
                    );

            if(distanceOffPassPath < minDistanceOffPassPath)
            {
                minDistanceOffPassPath = distanceOffPassPath;
                cutoffPoint = pointOnPath;
                cutoff = def; 
            }
        }

        if(minDistanceOffPassPath <= passPath.magnitude/2f)
        {
            return true;
        }
        return false;
    }


    

    protected void AutoPitch()
    {
        //                Debug.Log("auto pitch " + pitcher.inPosessionOfBall);

        if (!pitcher.inPosessionOfBall) return;
        pitcher.gloveRoot.gameObject.SetActive(true);
        pitcher.PlayPitchAnimation();
        isReadyToPitch = false;

    }


    /// <summary>
    /// Returns the time in seconds it would take to get the ball from ballHolder to a target base
    /// </summary>
    /// <param name="ballHolder">current ball holder</param>
    /// <param name="baseNo">target base</param>
    /// <returns></returns>
    public float TimeToGetBallToBase(DefensePlayer ballHolder, int baseNo)
    {
        float time_runToBase = ballHolder.GetTimeToRunToBase(baseNo);

        float time_throwToBase = ballHolder.GetTimeToThrowToBase(baseNo);
        if (baseCoverage[baseNo] == ballHolder) time_throwToBase = 0;

        float time_toBaseCoverage = TimeUntilBaseIsCovered(baseNo);

        //get faster method, running or passing
        return Math.Min(
            time_runToBase, //running
            Mathf.Max(time_throwToBase, time_toBaseCoverage) //passing
            );
    }

    public List<FullGameDirector.RankedBase> QuickestBasesToDefend(DefensePlayer ballHolder, float ballSpeed = 0f)
    {
        //guideline notes
        //if the runner is forced, the base can be resolved quickly
        //if the runner is not forced, it will take longer to resolve
        //you want to resolve anything that you can quickly get out of the way
        //you want to prioritize closer margins


        //basesByUrgency is the 
        List<FullGameDirector.RankedBase> basesByTimeToResolve = new List<FullGameDirector.RankedBase>();
        //FieldManager.FieldState state = FieldManager.Instance.fieldState;
        if (ballSpeed == 0)
        {
            ballSpeed = ballHolder ? ballHolder.throwSpeed : FieldMonitor.ballInPlay.GetVelocity().magnitude;
        }
        bool forcedRun = false;
        //int maxForcedBase = FieldManager.Instance.MaxForcedBase();

        for (int baseNo = 0; baseNo < FieldMonitor.Instance.numBases; baseNo++)
        {
            //bool forcedRun = baseNo <= maxForcedBase;
            //bool baseOccupied = state.baseOccupied[baseNo];

            float time_runnerToBase = FieldMonitor.Instance.GetTimeToRunnerOnBase(baseNo, out forcedRun);
            float time_ballToBase = TimeToGetBallToBase(ballHolder, baseNo);



            float time_defenseMargin = time_runnerToBase - time_ballToBase;
            float noise = 0; //add noise?
            bool defendable = time_defenseMargin + noise > 0; 
            //if t < 0, no chance of making it
            //if t > 0, can make it before runner

            float time_minToResolve = 0;
            if (forcedRun)
            {
                //if forced, can resolve immediately when the ball arrives
                time_minToResolve = time_ballToBase;
            }
            else
            {
                time_minToResolve = time_runnerToBase;
            }

            //float urgency = UrgencyOfDefendingBase(time_ballToBase, time_runnerToBase, forcedRun);
            if (defendable)
            {
                basesByTimeToResolve.Add(new FullGameDirector.RankedBase(baseNo, time_minToResolve));
            }


        }

        //order the bases by which are possible to reach in time, From quickest to slowest to resolve
        basesByTimeToResolve = basesByTimeToResolve
            .OrderBy(x =>x.value)
            //.OrderBy(x => x.value < 0) //will put values > 0 in front
            //.ThenBy(x => Mathf.Abs(x.value)) //will order by absolute values ascending, positive first, then negative (1,2,3,-1,-2,-3)
                                             //.ThenBy(x=>FieldManager.Instance.numBases - x.baseNo)
            .ToList();

        string basePrint = "----PASS OPTIONS----";
        foreach (FullGameDirector.RankedBase rBase in basesByTimeToResolve)
        {
            basePrint += "{" + rBase.baseNo + ", " + rBase.value + "} ";
        }

        return basesByTimeToResolve;
    }

    public int PickBaseToPassTo(DefensePlayer ballHolder)
    {
        List<FullGameDirector.RankedBase> priorities = QuickestBasesToDefend(ballHolder);
        int choice = -1;
        //bool activeRunners = !FieldMonitor.Instance.AllRunnersOnBase();
        for (int i = 0; i < priorities.Count; i++)
        {

            //targeted? includes base behind in case the ball is way in the outfield and we might want to cut off a runner ahead of the base they're currently running to
            bool targeted = (
                FieldMonitor.Instance.RunnerIsTargetingBase(priorities[i].baseNo)
                || FieldMonitor.Instance.RunnerIsTargetingBase(priorities[i].baseNo -1)

                );

            if (targeted)
            {
                choice = i;
                break;
            }

            /*
            if (targeted)
            {
                //expecting to get to base less than 2 seconds after runner
                if (priorities[i].value > -closeCallOptimisticMargin)
                {
                    //Debug.Log("close call defend " + i);
                    choice = i;
                    break;
                }
            }
            else if (!FieldMonitor.Instance.BaseIsOccupiedByRunner(priorities[i].baseNo))
            {
                //expecting to cut off runner by less than 1 seconds
                if (priorities[i].value > 0 && priorities[i].value < cutoffOptimisticMargin)
                {
                    //Debug.Log("cut off ahead " + i);
                    choice = i;
                    break;
                }
            }
            */
        }
        if (choice == -1) return -1; //could be that no bases are defendable
        return priorities[choice].baseNo;
    }


    public const float closeCallOptimisticMargin = 1.5f; //prevents players from completely ignoring a base that they expect to be .00001 seconds too late to defend
    public const float cutoffOptimisticMargin = 1; //only attempt to cut off a runner if expecting ball to reach base less than this amount of time before the runner, otherwise assume pitcher will have time to get it to the base
    public const float closeTagOptimisticMargin = .5f;







    #endregion

    #region RESET


    void ResetAllDefenders(bool excludeBallControlPlayer = false)
    {
        //        Debug.Log("Reset all Defenders,  exclude ball control :: " + excludeBallControlPlayer);
        baseDefenders.Clear();
        backupPlayers.Clear();
        backupCoverage_dirty = false;
        baseCoverage_dirty = false;
        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            if (excludeBallControlPlayer && player == playerWithBallControl) continue;
            ReturnToPitchPosition(player);
        }
    }

    void ClearBaseCoverage()
    {
        for (int b = 0; b < baseCoverage.Length; b++)
        {
            baseCoverage[b] = null;
        }
    }

    void ReturnToPitchPosition(DefensePlayer player)
    {
        player.Reset();
    }
    #endregion

    #region PlayerState_Machine
    void ReactToPlayerStateChange(AIFieldPlayer player, AIFieldPlayer.FieldPlayerAction newState)
    {

    }

    protected virtual bool ShouldBeginPitch()
    {
        return FullGameDirector.Instance.BothTeamsReadyForPitch();
    }

    bool DetermineHumanThrowIsPitch(hittable thrownBall)
    {
        //must be human, on mound, throwing roughly towards home plate
        if (!humanControlled)
        {
            return false;
        }
        if (!pitcher.isTetheredToCamera)
        {
            return false;
        }
        if (!PitcherOnMound())
        {
            return false;
        }

        Vector3 toPlate = (FieldMonitor.Instance.GetHomePlatePosition() - thrownBall.transform.position);
        toPlate.y = 0;
        Vector3 direction = thrownBall.GetVelocity();
        direction.y = 0;
        float angleOffHomePlate = Vector3.Angle(toPlate, direction);
        Debug.Log("ANGLE OFF HOME PLATE: " + angleOffHomePlate);
        if (angleOffHomePlate > 15)
        {
            return false;
        }
        return true;
    }

    void ReactToBallLaunchedByHuman(hittable ball)
    {
        HumanHUDManager.Instance.ShowHideOffscreenBallIndicator(true);

        //Debug.Log("REACT TO BALL LAUNCHED BY HUMAN");
        trailParticleManager tempTrail = ball.GetComponent<trailParticleManager>();
        if (tempTrail)
        {
            tempTrail.OnHit();
        }

        if (ball.visualAssist)
        {
            ball.visualAssist.SetActive(true);
        }

        SetBallControlPlayer(null);
        if (DetermineHumanThrowIsPitch(ball))
        {
            FieldMonitor.Instance.AnnouncePitch();
        }
        else
        {
            float timeToCatch = 0;
            if (FindPlayerToCatchBall(ball, out timeToCatch, prioritizeBaseCoveragePlayers: true))
            {
                Debug.Log("posess cather?    " + timeToCatch);
                float minimumReactionTime = .2f;
                if (timeToCatch > minimumReactionTime)
                {
                    if (FindBestDefenderToPosess())
                    {
                        //HumanPosessionManager.playArea.Rotate(Vector3.up * 180f);
                        //                        FieldManager.Instance.EnterSlowMotion();
                    }
                }
            }
            //numPosessionsThisPlay++;
            finishedTeleportingForThisPlay = true;//Greg: You should really only be making one pass during a play.
        }

    }

    /// <summary>
    /// Will look for best player to posess and switch human posession
    /// </summary>
    bool FindBestDefenderToPosess(bool ballIsFromBat = false)
    {
        Debug.Log("_______________________________ FIND BEST DEFENDER TO POSESS");
        //if (numPosessionsThisPlay >= maxPosessionsPerPlay) return false;
        if (finishedTeleportingForThisPlay) return false;
        if (!autoSwitchPosession) return false;

        DefensePlayer best = null;
        if (playerWithBallControl != null)
        {
            Debug.Log("BEST candidate is player With Ball Control.");

            best = playerWithBallControl;//Greg: At this point, if the ball is coming from a cannon. the playerWithBallControl will be on a base if possible.
        }
        else
        {
            List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> rankedChasers;

            rankedChasers = FieldMonitor.Instance.AirInterceptionEstimates(FieldMonitor.ballInPlay);

            if (rankedChasers.Count > 0)
            {
                best = rankedChasers[0].fieldPlayer;
            }

            else
            {
                //Debug.Log("look for Best candidate in ranked chasers");
                rankedChasers = FieldMonitor.Instance.GroundInterceptionEstimates(FieldMonitor.ballInPlay);

                if (rankedChasers.Count > 0)
                {
                    //Debug.Log("BEST candidate is the first ranked chaser.");

                    best = rankedChasers[0].fieldPlayer;
                }
                else
                {
                    Debug.Log("There is no best chaser.");
                }
            }
        }


        if (best != null)
        {
            if (best.fieldPosition == DefenseFieldPosition.CENTER_FIELDER || best.fieldPosition == DefenseFieldPosition.LEFT_FIELDER || best.fieldPosition == DefenseFieldPosition.RIGHT_FIELDER)//if you get it in the outfield you won't be anyone else.
            {
                finishedTeleportingForThisPlay = true;
                //numPosessionsThisPlay++;
            }
            if (ballIsFromBat)
            {
                //look towards home plate
                HumanPosessionManager.SetInitialPositionRotation(best.position, Quaternion.LookRotation(FieldMonitor.Instance.GetHomePlatePosition() - best.position));
                HumanPosessionManager.PosessPlayer(
                best,
                cancelIfAlreadyTethered: false, //still want slo mo, even if not changing player
                fade: !best.isTetheredToCamera,
                slowMo: true,
                turnAround: false,
                resetPlayer: false,
                resetPlayArea: !best.isTetheredToCamera
                );
            }
            else
            {
                HumanPosessionManager.PosessPlayer(
                best,
                cancelIfAlreadyTethered: true, //if you toss up, to yourself, transition should not happen
                fade: !best.isTetheredToCamera,
                slowMo: true,
                turnAround: true,
                resetPlayer: false
                );
            }
            
            return true;
        }
        return false;
    }



    void OnBallCaughtByHuman(hittable ball)
    {
        Debug.Log("OnBallCaughtByHuman");
        //MARC: Logic moved to HandManager
        //PreparePitcherCannon();
    }

    void OnBallReleasedByHuman(hittable ball)
    {

    }

   

    private bool CanAttemptToGetRunnerOut()
    {
        return (!FieldMonitor.Instance.AllRunnersOnBase()
            && (playerWithBallControl != null && CanGetARunnerOutWithBall(playerWithBallControl))) ;
    }

    #endregion


    #region STATE_MACHINE

    protected virtual void NONE_Enter()
    {
        BallHolderInstructions = ReturnBallToPitcher;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ChaseBall;
        
        if (humanControlled)
        {
            //catcher.Reset();
            //if the pitcher is still posessed and hasn't used locomotion to move the play area, no need to use posession
            //Greg: I commented this out since I think it should just always happen and I saw some bugs where the player was stuck off the mound and expected to pitch.
            if (!(HumanPosessionManager.PlayAreaIsAtInitialPositionRotation() && HumanPosessionManager.currentlyTetheredPlayer == pitcher))
            {
                bool startOfGame = Time.timeSinceLevelLoad < 1f;//Greg: This is gross but I think it will work for now. Oculus' fade looks bad at the start of levels.
                HumanPosessionManager.SetInitialPositionRotation(GetFieldPosition(DefenseFieldPosition.PITCHER), Quaternion.LookRotation(Vector3.back, Vector3.up));
                HumanPosessionManager.PosessPlayer(pitcher, resetPlayer: true, resetPlayArea: true, cancelIfAlreadyTethered: false, fade: !startOfGame);
            }

            HumanPosessionManager.SetLocomotionEnabled(true);

            //MARC: logic moved to HandManager
            //PreparePitcherCannon();

            //FieldManager.Instance.ShowHideStrikeGate(true);// !useCannon);

        }

        if (!humanControlled)
        {
            isReadyToPitch = true;
            HandCannonController.predictionIsAllowed = false;
        }
        else
        {
            HandCannonController.predictionIsAllowed = true;
        }

        FieldMonitor.Instance.ShowStrikeGateForSeconds(2f);
        ResetAllDefenders(true);
    }



    protected virtual void NONE_Update()
    {
        UpdatePlayerWithBallControl();
        RefreshBallChasers();

#if UNITY_EDITOR
        if (debugCheats)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                isReadyToPitch = true;
            }
        }
#endif

        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            FieldMonitor.Instance.ExplodeBall();
        }
        //                Debug.Log ("NONE UPDATE " + FieldManager.Instance.BothTeamsReadyForPitch());

        //MARC: NONE seems dangerous 1/17/19
        /*
        if (CanAttemptToGetRunnerOut())
        {
            Debug.Log("might be able to get someone out, go back to STANDARD");
            SetPlayType(FieldPlayType.STANDARD);
        }
        */

        if (ShouldBeginPitch())
        {
            if (!humanControlled)
            {
                if (isReadyToPitch)
                {
                    AutoPitch();


                }
            }
        }

       
    }

    protected virtual void NONE_Exit()
    {
        if (humanControlled)
        {
            //TempHomePlateMover.SetUsable(false);
        }


    }

    protected virtual void PITCH_Enter()
    {
        finishedTeleportingForThisPlay = false;

        BallHolderInstructions = InitiateReturnToPitchPositions;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ReturnToPitchPosition;

        if (catcher)
        {
            if (catcher.CatchBallBehindHomePlate(FieldMonitor.ballInPlay))
            {
                SetBallControlPlayer(catcher);
            }
        }
        groundedPitchTimer = 0;
    }

    protected float groundedPitchTimer;
    protected virtual void PITCH_Update()
    {
        UpdatePlayerWithBallControl();

        //give the ball a couple seconds to roll to the catcher, but don't wait too long
        if (FieldMonitor.ballInPlay.hitGround){
            groundedPitchTimer += Time.deltaTime;
        }

        if (FieldMonitor.Instance.BallHasLeftPark() || (groundedPitchTimer>1f))
        {
            //Debug.Log("--------------------- EXPLODE PITCHED BALL");
            //ResetAllDefenders(true);
            FieldMonitor.Instance.ExplodeBall();
            groundedPitchTimer = 0;
            ReturnToPitchPositions();
            //FieldManager.CalloutBall();
        }
    }

    protected virtual void PITCH_Exit()
    {
        if (humanControlled)
        {
            //			Debug.Log ("disappear pitching cannon 02");
            //pitcherCannon.gameObject.SetActive (false);

        }

        FieldMonitor.Instance.ShowHideStrikeGate(false);
    }

    void STANDARD_Enter()
    {
        //      SetBallControlPlayer(null);
//        Debug.Log("Standard enter " + humanControlled);

        //MARC: never go from STANDARD to NONE, always use RETURN as an intermediate
        PlayOverCondition = Never;

        HandCannonController.predictionIsAllowed = true;

        BallHolderInstructions = DefendBasesWithBall;
        BallThrowerInstructions = ReportForBaseDuty;
        BasePlayerInstructions = FindBaseAssignment;
        BackupPlayerInstructions = FindBackupAssignment;
        BallChaserInstructions = ChaseBall;

        AssignInitialRoles();


        if (humanControlled)
        {
            //pitcher.TetherToCamera(false);
            //AssignInitialBaseCoverage();
            //PlayOverCondition = PitcherOnMoundWithBall;
            autoSwitchPosession = true;

            //Greg: Possess whoever is the most responsible for going after the ball. If there is an AI pass in progress, let the AI complete it
            if (!aIpassInProgress)
                FindBestDefenderToPosess(true);
            HumanPosessionManager.SetLocomotionEnabled(true);

            if (_catcherThrowTarget)
            {
                _catcherThrowTarget.enabled = true;
            }
        }


        waitForRunnersToSettle = 0;
    }


    private float waitForRunnersToSettle = 0;
    void STANDARD_Update()
    {

        UpdatePlayerWithBallControl();
        RefreshBallChasers();

        if (FieldMonitor.Instance.AllRunnersOnBase())
        {
            waitForRunnersToSettle += Time.deltaTime;
            if (BallIsInPosession() && waitForRunnersToSettle > 1)
            {
                //maybe take this out. it doesnt make sense for players to return to their posts before the player has a chance to pass a caught ball
                ReturnToPitchPositions();
            }
        }
        else
        {
            waitForRunnersToSettle = 0;
        }
        

        //if the ball has left the park for any reason, defense should just wait until all runners have walked one base
        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            SetPlayType(FieldPlayType.WALK);
        }


    }
    void STANDARD_Exit()
    {
        autoSwitchPosession = false;

        if (_catcherThrowTarget)
        {
            _catcherThrowTarget.enabled = false;
        }
    }

    void HOMERUN_Enter()
    {

        BallHolderInstructions = InitiateReturnToPitchPositions;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ReturnToPitchPosition;

        ClearBaseCoverage();

        ResetAllDefenders(false);

        //It's important that the pitcher not be given a new ball before Offense is done running bases
        PlayOverCondition = HomerunComplete;
    }

    void HOMERUN_Update()
    {
        if (PlayOverCondition())
        {
            EndPlay();
        }

    }
    void HOMERUN_Exit()
    {
        Debug.Log("Defense Coordinator HOMERUN_Exit SpawnBallInPitchersGlove");
        SpawnBallInPitchersGlove();
    }

    void FOUL_Enter()
    {
        Debug.Log("Foul enter");

        BallHolderInstructions = InitiateReturnToPitchPositions;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ChaseBall;

        if (humanControlled)
        {
            pitcher.highlight.ShowArrow(true);
            PlayOverCondition = PitcherHasTheBall;
        }
        else
        {
            PlayOverCondition = PlayersAtPitchPosition_PitcherHasBall;
        }

        ClearBaseCoverage();

        ResetAllDefenders(true);
    }

    void FOUL_Update()
    {


        UpdatePlayerWithBallControl();
        RefreshBallChasers();

        if (PlayOverCondition())
        {
            EndPlay();
        }

        //if the ball leaves the park for any reason, explode it
        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            Debug.Log("foul has left the park.");
            FieldMonitor.Instance.ExplodeBall();
        }

    }

    void FOUL_Exit()
    {
        pitcher.highlight.ShowArrow(false);

    }

    void RETURN_Enter()
    {

        BallHolderInstructions = ReturnBallToPitcher;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ChaseBall;

        if (humanControlled)
        {
            pitcher.highlight.ShowArrow(true);
            PlayOverCondition = PitcherHasTheBall;
        }
        else
        {
            PlayOverCondition = PlayersAtPitchPosition_PitcherHasBall;
        }


        //MARC: removed this so that players can still make estimates about how long it will take to pass to a base to catch stealers
        //ClearBaseCoverage();

        ResetAllDefenders(true);
    }

    void RETURN_Update()
    {
        UpdatePlayerWithBallControl();
        RefreshBallChasers();

        if (CanAttemptToGetRunnerOut())
        {
            Debug.Log("might be able to get someone out, go back to STANDARD");
            SetPlayType(FieldPlayType.STANDARD);
        }
        else if (PlayOverCondition() && FieldMonitor.Instance.AllRunnersOnBase())
        {
            EndPlay();
        }

        if (FieldMonitor.Instance.BallHasLeftPark())
        {
            Debug.Log("ball being returned has left the park.");
            SetPlayType(FieldPlayType.WALK);
            //FieldManager.Instance.ExplodeBall();
        }
    }

    void RETURN_Exit()
    {
        pitcher.highlight.ShowArrow(false);

    }


    List<AIFieldPlayer> remainingDoppelgangers;
    void SWITCH_Enter()
    {
        StopAllCoroutines();
        if (playerWithBallControl != null) playerWithBallControl.CancelThrowAnimation();
       
        baseDefenders.Clear();
        for (int b = 0; b < baseCoverage.Length; b++)
        {
            baseCoverage[b] = null;
        }
        backupPlayers.Clear();

        remainingDoppelgangers = new List<AIFieldPlayer>();
        doppelgangersToRemove = new List<AIFieldPlayer>();

        TeamInfo defendingTeam = (FullGameDirector.previousInningFrame == FullGameDirector.InningFrame.Top ? FullGameDirector.awayTeam : FullGameDirector.homeTeam);

        DefensePlayer doppel;
        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            //OneShotEffectManager.SpawnEffect("explosion", player.transform.position, Quaternion.identity);
            /*
            if (doppelgangerDict.TryGetValue(player, out doppel))
            {
                remainingDoppelgangers.Add(doppel);
                doppel.gameObject.SetActive(true);
                doppel.DitherFade(1);
                doppel.position = player.position;

                doppel.SetPlayerInfo(player.team, player.info);
                doppel.RunToTarget(FullGameDirector.Instance.GetDugoutForTeam(player.team));
            }
            */

            //Debug.Log("+++++++++++++++++++++++++++++++++++++++++++++ SWITCH_ENTER FROM FRAME " + FieldManager.currentInningFrame);

            player.SetPlayerInfo(defendingTeam, defendingTeam.GetPlayerAssignedToPosition(player.fieldPosition));
            player.position = FullGameDirector.Instance.GetDugoutForTeam(player.team);

            player.DitherFade(0, 1, .5f);
            player.Reset();
        }

        BallHolderInstructions = ReturnBallToPitcher;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ReturnToPitchPosition;

    }

    List<AIFieldPlayer> doppelgangersToRemove;
    void SWITCH_Update()
    {
        UpdatePlayerWithBallControl();


        foreach (AIFieldPlayer doppel in remainingDoppelgangers)
        {
                if (!doppel.isFadingOut && Vector3.Distance(doppel.moveTargetPosition, doppel.position) <5)
                {
                    doppel.FadeToDeactivate(.5f);
                    doppelgangersToRemove.Add(doppel);
                }
        }
        foreach(AIFieldPlayer doppel in doppelgangersToRemove)
        {
            remainingDoppelgangers.Remove(doppel);
        }
        doppelgangersToRemove.Clear();
    }

    void SWITCH_Exit()
    {
        //hide everyones doppelgangers
        DefensePlayer doppel;
        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            if (doppelgangerDict.TryGetValue(player, out doppel))
            {
                doppel.gameObject.SetActive(false);
            }
        }

    }

    void WALK_Enter()
    {
        //Debug.Log("WALK Enter");
        BallHolderInstructions = ReturnBallToPitcher;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ChaseBall;

        PlayOverCondition = FieldMonitor.Instance.AllRunnersOnBase;
    }

    void WALK_Update()
    {
        // we still want the catcher to go grab the pitched ball if possible
        UpdatePlayerWithBallControl();
        RefreshBallChasers();

        if (PlayOverCondition()) EndPlay();
    }
    void WALK_Exit()
    {


    }

    void OPENNING_Enter()
    {
        BallHolderInstructions = PassBallToRandomOtherDefender;
        BallThrowerInstructions = ReturnToPitchPosition;
        BasePlayerInstructions = ReturnToPitchPosition;
        BackupPlayerInstructions = ReturnToPitchPosition;
        BallChaserInstructions = ChaseBall;

        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            player.position = FullGameDirector.Instance.GetDugoutForTeam(player.team);
            player.DitherFade(0, 1, 2f);
            player.Reset();
        }
    }

    void OPENNING_Update()
    {
        UpdatePlayerWithBallControl();
        RefreshBallChasers();
    }

    void OPENNING_Exit()
    {
    }

    void CLOSING_Enter()
    {
        StartCoroutine(HandshakeRoutine());

    }

    void CLOSING_Update()
    {
    }

    void CLOSING_Exit()
    {
    }

    void DISABLED_Enter()
    {

    }

    void DISABLED_Exit()
    {

    }


    IEnumerator HandshakeRoutine()
    {
        Vector3 meetPoint = ((FieldMonitor.Instance.GetHomePlatePosition() + FieldMonitor.Instance.GetPitchersMoundPosition()) / 2);
        Ray defLine = new Ray(
            meetPoint + Vector3.back * .5f + Vector3.right * 5,
            Vector3.right);
        Ray offLine = new Ray(
            meetPoint + Vector3.forward * .5f + Vector3.left * 5,
            Vector3.left);

        int i = 0;
        float spacing = 1;

        DefensePlayer doppel;
        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            if (doppelgangerDict.TryGetValue(player, out doppel))
            {
                doppel.gameObject.SetActive(true);
                doppel.SetPlayerInfo(FullGameDirector.otherTeam(player.team), FullGameDirector.otherTeam(player.team).GetPlayerAssignedToPosition(doppel.fieldPosition));
                doppel.DitherFade(0, 1, .5f);
                doppel.position = FullGameDirector.Instance.GetDugoutForTeam(doppel.team);

                doppel.RunToTarget(offLine.GetPoint(i * spacing));
                doppel.SetPersonalSpaceRadius(.3f);
            }
            player.SetPersonalSpaceRadius(.3f);
            player.RunToTarget(defLine.GetPoint(i * spacing));
            i++;
        }

        bool everyoneLinedUp = false;
        while (!everyoneLinedUp)
        {
            yield return new WaitForSeconds(1);
            everyoneLinedUp = true;
            foreach (DefensePlayer def in FieldMonitor.defensivePlayers)
            {
                if (!def.HasReachedRunTarget()) everyoneLinedUp = false;
                if(doppelgangerDict.TryGetValue(def, out doppel))
                {
                    if (!doppel.HasReachedRunTarget()) everyoneLinedUp = false;
                }
            }

        }

        foreach (DefensePlayer player in FieldMonitor.defensivePlayers)
        {
            if (doppelgangerDict.TryGetValue(player, out doppel))
            {
                doppel.FadeToDeactivate(.5f, Vector3.Distance(offLine.GetPoint(-15), doppel.position) / doppel.runSpeed);
                doppel.RunToTarget(offLine.GetPoint(-100));
            }
            player.FadeToDeactivate(.5f, Vector3.Distance(defLine.GetPoint(-15), player.position)/player.runSpeed);
            player.RunToTarget(defLine.GetPoint(-100));
        }
    }

    public void RefreshState()
    {
        switch (_state)
        {
            case FieldPlayType.STANDARD:
                STANDARD_Enter();
                break;
            case FieldPlayType.FOUL:
                FOUL_Enter();
                break;
            case FieldPlayType.HOMERUN:
                HOMERUN_Enter();
                break;
            case FieldPlayType.NONE:
                NONE_Enter();
                break;
            case FieldPlayType.PITCH:
                PITCH_Enter();
                break;
            case FieldPlayType.RETURN:
                RETURN_Enter();
                break;
        }
    }
    #endregion
}
