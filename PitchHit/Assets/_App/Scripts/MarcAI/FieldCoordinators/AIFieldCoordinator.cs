﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

public enum FieldPlayType
{
    NONE, //setting up for next pitch
    STANDARD, //get runs, get outs, the usual
    HOMERUN, //home run
    FOUL, //foul ball
    RETURN, //return the ball to the pitcher
    PITCH, //ball is travelling from the pitcher to the batter
    SWITCH, //teams are switching sides
    WALK, // 4 balls = walk for batter
    OPENNING, //openning ceremony
    CLOSING, //closing ceremony
    DISABLED, //do nothing
}

/// <summary>
/// Comes with a state machine, generic containers for evaluating player positions, and option to set human controlled
/// </summary>
public abstract class AIFieldCoordinator : MonoBehaviour
{
    public class RankedFieldPlayer<T> where T : AIFieldPlayer
    {
        public RankedFieldPlayer(T o, float v, Vector3 p)
        {
            fieldPlayer = o; value = v; position = p;
        }
        public T fieldPlayer;
        public float value;
        public Vector3 position;
        public override string ToString()
        {
            return "{PLAYER: " + fieldPlayer.ToString() + " VAL: " + value + " POS: " + position + "}";
        }
    }

    public FieldPlayType _state;
    public FieldPlayType runningPlay
    {
        get
        {
            if (fsm !=null)
            {
                return fsm.State;
            }
            else
            {
                return FieldPlayType.NONE;
            }
        }
    }
    private StateMachine<FieldPlayType> fsm;
    protected bool isInitialized = false;

    protected delegate bool Condition();

    protected delegate List<FullGameDirector.RankedPosition> RankedPositionSort(List<FullGameDirector.RankedPosition> positionList);



    //protected T[] fieldPlayers;
    public bool debugCheats = false;
    public bool humanControlled { get; protected set; }
    private bool isResponsiveToFieldEvents = true;


    public virtual void Initialize()
    {
        if (isInitialized) return;
        fsm = StateMachine<FieldPlayType>.Initialize(this, FieldPlayType.DISABLED);
        fsm.Changed += SetEditorVisibleState;
        isInitialized = true;
    }


    protected virtual void Start()
    {
        if (!isInitialized) Initialize();
    }


    public void RespondToFieldEvent(hittable ball, FieldPlayType play)
    {
        if (isResponsiveToFieldEvents) ExecutePlayWithBall(ball, play);   
    }

    protected abstract void ExecutePlayWithBall(hittable ball, FieldPlayType play);

    protected void SetPlayType(FieldPlayType play)
    {
#if UNITY_EDITOR
        //        Debug.Log("<color=white>" + this.GetType().ToString() + " : " + play + "</color>");
#endif
        if (fsm!=null)
        {
            fsm.ChangeState(play);
        }
    }


    private void SetEditorVisibleState(FieldPlayType state)
    {
        _state = state;
    }

    public virtual void SetHumanControlled(bool isHuman)
    {
        if (!isInitialized) Initialize();
        humanControlled = isHuman;
    }

    public void SetResponsiveToFieldEvents(bool isResponsive)
    {
        isResponsiveToFieldEvents = isResponsive;
    }

    public virtual void InstantReset()
    {

    }
}