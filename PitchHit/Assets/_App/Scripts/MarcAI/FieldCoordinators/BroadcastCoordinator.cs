﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SpectatorPosition
{
    HOME,
    TOWER,
    GIANT,
    BOOTH,
}

public class BroadcastCoordinator : AIFieldCoordinator
{
    Spectator giant;
    Spectator booth;

    public Spectator spectatorPrefab;

    public Transform boothPosition;
    public Animator spectatorAnimController;

    public SpectatorCameraController spectatorCameraController;
    protected bool pov = false;
    public bool defaultPOV = false;

    private BoothFeatureControl _boothControl;

    new void Start()
    {
        base.Start();
        _boothControl = FindObjectOfType<BoothFeatureControl>();
        SetPovMode(defaultPOV);
    }

    void Update()
    {
        if (debugCheats)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                TogglePovOnlyMode();
            }
        }

        if (FullGameDirector.Instance)
        {
            spectatorAnimController.SetBool("pitcherOnMoundWithBall", FieldMonitor.Instance.PitcherOnMoundWithBall());

            spectatorAnimController.SetBool("runnerOnFirst", FieldMonitor.Instance.BaseIsOccupiedByRunner((int)BaseID.FIRST));
            spectatorAnimController.SetBool("runnerOnSecond", FieldMonitor.Instance.BaseIsOccupiedByRunner((int)BaseID.SECOND));
            spectatorAnimController.SetBool("runnerOnThird", FieldMonitor.Instance.BaseIsOccupiedByRunner((int)BaseID.THIRD));

            hittable ball = FieldMonitor.ballInPlay;
            Vector3 ballVelocity = Vector3.zero;
            bool isHeld = false;
            bool untouched = true;
            if (ball)
            {
                ballVelocity = ball.GetVelocity();
                isHeld = ball.isHeld;
                untouched = !ball.hasTouchedDefender;
            }
            Vector3 ballVelocity_flat = ballVelocity; ballVelocity_flat.y = 0;
            spectatorAnimController.SetFloat("ballVelocity_Y", ballVelocity.y);
            spectatorAnimController.SetFloat("ballGroundSpeed", ballVelocity_flat.magnitude);

            spectatorAnimController.SetBool("ballIsHeld", isHeld);
            spectatorAnimController.SetBool("ballUntouched", untouched);

            spectatorAnimController.SetBool("humanOffense", FullGameDirector.Instance.offense.humanControlled);
            spectatorAnimController.SetBool("humanDefense", FullGameDirector.Instance.defense.humanControlled);

        }
    }

    public override void SetHumanControlled(bool isHuman)
    {
        base.SetHumanControlled(isHuman);

        if (isHuman)
        {
            /*
            if(runningPlay == FieldPlayType.OPENNING || runningPlay == FieldPlayType.CLOSING)
                GiantView();
            else
            */
            BoothView();
        }
}

    protected override void ExecutePlayWithBall(hittable ball, FieldPlayType play)
    {
//        Debug.Log("Spectator controller exceut e with play " +play);


        SetPlayType(play);

        spectatorAnimController.SetBool("humanOffense", FullGameDirector.Instance.offense.humanControlled);
        spectatorAnimController.SetBool("humanDefense", FullGameDirector.Instance.defense.humanControlled);

        spectatorAnimController.SetTrigger(play.ToString());
        switch(play)
        {
            case FieldPlayType.NONE:
               

                break;
            default:
                //spectatorCameraController.SetAngleOptions(null);
                break;
        }
    }


    public void TogglePovOnlyMode()
    {
        pov = !pov;
        SetPovMode(pov);
    }

    protected void SetPovMode(bool enabled)
    {
        if (spectatorCameraController)
        {
            spectatorCameraController.SetCinemachineEnabled(!enabled);
        }
        pov = enabled;
    }

    #region Initialization

    public override void Initialize()
    {
        if (isInitialized) return;

        giant = GameObject.Instantiate(spectatorPrefab, Vector3.zero, Quaternion.identity);
        giant.transform.localScale = Vector3.one * 30;
        giant.name = "GiantSpectator";
        giant.spectatorPosition = SpectatorPosition.GIANT;

        booth = GameObject.Instantiate(spectatorPrefab, boothPosition.position, boothPosition.rotation);
        booth.name = "BoothSpectator";
        booth.spectatorPosition = SpectatorPosition.BOOTH;
        if (FullGameDirector.isBroadcastMode)
        {
            if(_boothControl == null)
            {
                _boothControl = FindObjectOfType<BoothFeatureControl>();
            }
            if (_boothControl)
            {
                _boothControl.SetBroadcastOnlyObjectsActive(true);
            }

        }

        base.Initialize();
    }

    protected void GiantView()
    {
        HumanPosessionManager.SetInitialPositionRotation(FieldMonitor.Instance.GetPitchersMoundPosition(), Quaternion.identity);
        HumanPosessionManager.PosessPlayer(giant, fade: true, matchScale: true, resetPlayArea: true);
        HumanPosessionManager.SetLocomotionEnabled(true);
        HumanGearManager.instance.HideAllGear();
    }

    protected void BoothView()
    {
        //If we're already in the booth, we shouldn't fade into it.
        bool fadeView = (FullGameDirector.previousHumanPlayerRole != FullGameDirector.HumanPlayerRole.BROADCAST);
        //Debug.Log("FADE == " + fadeView.ToString());
        HumanPosessionManager.SetInitialPositionRotation(boothPosition.position, boothPosition.rotation);
        HumanPosessionManager.PosessPlayer(booth, fade: fadeView, resetPlayArea: true);
        HumanPosessionManager.SetLocomotionEnabled(true);
        if (spectatorCameraController)
        {
            spectatorCameraController.SetCamToBooth();
        }
        //HumanGearManager.instance.HideAllGear();
    }

    #endregion
}