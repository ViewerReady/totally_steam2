﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Palette", menuName = "ColorPalette", order = 1)]
public class ColorPalette : ScriptableObject {

    public string paletteName = "Default";
    public Color color_1 = Color.cyan;
    public Color color_2 = Color.magenta;
    public Color color_3 = Color.yellow;
    public Color color_4 = Color.white;
    public Color color_5 = Color.gray;
    public Color color_6 = Color.black;

    public static ColorPalette defaultPalette
    {
        get
        {
            if(_defaultPalette == null){
                _defaultPalette = ScriptableObject.CreateInstance<ColorPalette>() as ColorPalette;
            }
            return _defaultPalette;
        }
    }
    private static ColorPalette _defaultPalette;
}
