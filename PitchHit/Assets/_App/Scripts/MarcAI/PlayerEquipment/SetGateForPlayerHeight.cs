﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallGate))]
public class SetGateForPlayerHeight : MonoBehaviour {

    Transform head;
    BallGate gate;

    [Range(0f, 1f)]
    public float bottomPercent = .3f;
    [Range(0f, 1f)]
    public float topPercent = .7f;

    protected Vector3 defaultPos;

    private void Start()
    {
        gate = GetComponent<BallGate>();
        head = Camera.main.transform;
        Debug.Log("Camera main is: " + Camera.main.gameObject.name);
        defaultPos = transform.localPosition;
    }

    public void SetToDefaultHeight()
    {
        transform.localPosition = defaultPos;
    }

	public void SetForHMDHeight()
    {
        if (!head)
        {
            SetToDefaultHeight();
            return;
        }
        float headHeight = head.localPosition.y;
        float bottom = headHeight * bottomPercent;
        float top = headHeight * topPercent;
        float gateYPos = (top+bottom)/ 2f;
        float geteHeight = (top - bottom);

        transform.localPosition = new Vector3(transform.localPosition.x, gateYPos, transform.localPosition.z);
        gate.size = new Vector2(gate.size.x, top-bottom);
    }
}
