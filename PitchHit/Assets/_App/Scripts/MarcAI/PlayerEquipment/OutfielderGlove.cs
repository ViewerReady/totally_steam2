﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OutfielderGlove : MonoBehaviour {

    public Action<hittable> OnBallCatch;
    public Action<hittable> OnFumbleCatch;
    public Transform ballPosition;

    public hittable heldBall;// { get; protected set; }
    private DefensePlayer owner;

    private float maxVerticalReach;
    protected SphereCollider gloveCollider;
    public Renderer meshRenderer;

    public AudioClip[] catchSounds;

    bool ignoreTrigger = false;
    bool canAttemptCatch = true;
    bool isShowing = true;

    public bool catcherGlove = false;

    List<OffensePlayer> runnersInTrigger = new List<OffensePlayer>();
    List<hittable> ballsInTrigger = new List<hittable>();

    void Awake()
    {
        gloveCollider = GetComponent<SphereCollider>();
        owner = GetComponentInParent<DefensePlayer>();
    }

    void Start()
    {
        maxVerticalReach = gloveCollider.center.z + gloveCollider.radius + ballController.ballRadius;
        if (owner)
        {
			Physics.IgnoreCollision (gloveCollider, owner.GetComponent<Collider> ());
		}
    }

    void OnDestroy()
    {
        foreach(hittable ball in ballsInTrigger)
        {
            ball.OnBallDestroy -= RemoveBallInTrigger;
        }
    }

    void OnEnable()
    {
        ignoreTrigger = false;
    }

    public float GetMaxVerticalReach()
    {
        return maxVerticalReach;
    }

    public void ShowHide(bool show)
    {
        if (show == isShowing) return;
        isShowing = show;
        meshRenderer.enabled = isShowing;  
    }

    void OnTriggerEnter(Collider other)
    {
        hittable ball = other.GetComponent<hittable>();
        if (ball != null)
            AddBallInTrigger(ball);

        OffensePlayer runner = other.GetComponent<OffensePlayer>();
        if (runner != null)
            AddRunnerInTrigger(runner);
    }

    void OnTriggerExit(Collider other)
    {
        hittable ball = other.GetComponent<hittable>();
        RemoveBallInTrigger(ball);

        OffensePlayer runner = other.GetComponent<OffensePlayer>();
        RemoveRunnerInTrigger(runner);
    }

    void AddBallInTrigger(hittable ball)
    {
        if (!ballsInTrigger.Contains(ball))
        {
//            Debug.Log(" Add ball to "+owner.name);
            ballsInTrigger.Add(ball);
            ball.OnBallDestroy += RemoveBallInTrigger;
            RefreshObjectsInTrigger();

        }
    }
    void RemoveBallInTrigger(hittable ball)
    {
        //Debug.Log(" Remove ball to " + owner.name);
        if (ballsInTrigger.Contains(ball))
            ballsInTrigger.Remove(ball);
        RefreshObjectsInTrigger();
    }

    void AddRunnerInTrigger(OffensePlayer runner)
    {
        if (!runnersInTrigger.Contains(runner))
        {
            //Debug.Log(" Add Runner to " + owner.name);
            runnersInTrigger.Add(runner);
            runner.OnOut += RemoveRunnerInTrigger;
            RefreshObjectsInTrigger();
        }
    }

    void RemoveRunnerInTrigger(OffensePlayer runner, OutType o = OutType.CAUGHT)
    {
        if (runnersInTrigger.Contains(runner))
        {
            runnersInTrigger.Remove(runner);
        }
        RefreshObjectsInTrigger();
    }


    void RefreshObjectsInTrigger()
    {
        if (ignoreTrigger) return;


        hittable[] balls = ballsInTrigger.ToArray();
        foreach (hittable ball in balls)
        {
            if (owner && owner.isTetheredToCamera) continue;

            if (canAttemptCatch && ball != null && ball != heldBall && !ball.isHeld)
            {
                if (owner)
                {
                    //Debug.Log("AttemptTo Catch ball by owner "+owner.name);
                    AttemptToCatchBall(ball, owner.fumbleChance);//Greg: Sometimes this produces NullReferenceExceptions?
                }
                else
                {
                    //Debug.Log("Attempt to catch ball with no owner." + name);
                    AttemptToCatchBall(ball);
                }
            }
        }

        OffensePlayer[] runners = runnersInTrigger.ToArray();
        foreach (OffensePlayer runner in runners)
        {
            if (runner != null)
            {
                if (heldBall != null)
                {
                    runner.AttemptTagOut(this);
                }
            }
        }
    }



    public void ForceCatchBall(hittable ball)
    {
//        Debug.Log("FORCE CATCH BALL "+owner.name);
        AttemptToCatchBall(ball, 0);
    }

    public void ForceReleaseBall(hittable ball)
    {
        ReleaseBall(ball);
    }

    void AttemptToCatchBall(hittable ball, float fumbleChance = 0)
    {

        float fumbleRoll = UnityEngine.Random.Range(0f, 1f);
        bool fumble = fumbleRoll < fumbleChance;

        if (fumble)
        {
            Rigidbody ballBody = ball.GetRigidbody();
            Vector3 ballVelocity = ballBody.velocity;
            Vector3 awayFromBody = UnityEngine.Random.insideUnitSphere; awayFromBody.y = 0; awayFromBody.Normalize();
			ballBody.velocity = awayFromBody * (owner? (owner.runSpeed * 1.1f):1.1f);// Mathf.Max(3,ballVelocity.magnitude * .5f);
            ballBody.AddForce(UnityEngine.Random.Range(0f,1f)*Vector3.up, ForceMode.Impulse);
            IgnoreTrigger(1);
            ball.hasTouchedDefender = true;
            if (OnFumbleCatch != null) OnFumbleCatch.Invoke(ball);
        }
        else
        {
            heldBall = ball;
            CenterHeldBallInGlove();
            ball.OnBallDestroy += ReleaseDestroyedBall;
            ball.SetHeldByPlayer(true);
            //ball.inGlove = true;
            //ball.hasBeenCaught = true;
            if(catcherGlove) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("mit_catcher", transform);
            else DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("player_catch", transform);
            if (OnBallCatch != null) OnBallCatch.Invoke(ball);
            if (ball.OnCaughtByDefender != null) ball.OnCaughtByDefender.Invoke(ball);
            
        }

    }

    public void ReleaseBallIntoHand(hittable ball)
    {
        if (heldBall == ball)
        {
            ReleaseBall(grabbedByHand: true);
        }
    }

    void ReleaseBall(hittable ball)
    {
        if (ball == heldBall)
        {
            ReleaseBall();
        }
    }

    void ReleaseDestroyedBall(hittable ball)
    {
        if(ball == heldBall)
        {
            ReleaseBall(beingDestroyed: true);
        }
    }

    public hittable ReleaseBall(bool grabbedByHand = false, bool beingDestroyed = false)
	{
//		Debug.Log ("release ball");
        if (heldBall == null) return null;

        /*
        TrajTester1 trajTester = heldBall.GetComponent<TrajTester1>();
        if (trajTester)
        {
            trajTester.OnReleased();
        }
        */

       
        if (!grabbedByHand)
        {
            heldBall.SetHeldByPlayer(false);
            heldBall.GetRigidbody().isKinematic = false;
            if (!beingDestroyed)
            {
                heldBall.transform.SetParent(owner ? owner.transform.parent : null);
            }
        }
        hittable releasedBall = heldBall;
        heldBall = null;
        //StartCoroutine(IgnoreOtherColliderForSeconds(releasedBall.GetComponent<Collider>(), 1));
        RemoveBallInTrigger(releasedBall);


        if (this)
        IgnoreTrigger(.5f);


        return releasedBall;
    }

    public void CenterHeldBallInGlove()
    {
        if (heldBall)
        {
            heldBall.transform.SetParent(ballPosition);
            heldBall.transform.localPosition = Vector3.zero;
            heldBall.GetRigidbody().isKinematic = true;
        }
    }

    public void SetCatchingEnabled(bool isEnabled)
    {
        canAttemptCatch = isEnabled;

        //if(gloveCollider == null) gloveCollider = GetComponentInChildren<Collider>();
        //gloveCollider.enabled = isEnabled;
    }

    public void IgnoreTrigger(float seconds)
    {
        if (isActiveAndEnabled)
        {
            StopAllCoroutines();
            StartCoroutine("IgnoreTriggerForSeconds", seconds);
        }
    }

    IEnumerator IgnoreTriggerForSeconds(float seconds)
    {
//        Debug.Log(" Ignore trigger for seconds. " + owner.name);
        ignoreTrigger = true;
        yield return new WaitForSeconds(seconds);
        ignoreTrigger = false;

        RefreshObjectsInTrigger();
    }

    IEnumerator IgnoreOtherColliderForSeconds(Collider other, float seconds)
    {
        Physics.IgnoreCollision(gloveCollider, other);
        yield return new WaitForSeconds(seconds);
        if(other != null)
        {
            Physics.IgnoreCollision(gloveCollider, other, false);
        }
    }
}
