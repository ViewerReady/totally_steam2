﻿// %BANNER_BEGIN%
// ---------------------------------------------------------------------
// %COPYRIGHT_BEGIN%
//
// Copyright (c) 2018 Magic Leap, Inc. All Rights Reserved.
// Use of this file is governed by the Creator Agreement, located
// here: https://id.magicleap.com/creator-terms
//
// %COPYRIGHT_END%
// ---------------------------------------------------------------------
// %BANNER_END%

using System;
using System.Collections.Generic;
using UnityEngine;

#if LUMIN
using UnityEngine.XR.MagicLeap;

   
    /// <summary>
    /// Component used to communicate with the MLHands API and manage
    /// which KeyPoses are currently being tracked by each hand.
    /// KeyPoses can be added and removed from the tracker during runtime.
    /// </summary>
    public class GloveHandTracking : MonoBehaviour
    {

        public GameObject glove;



        [SerializeField]
        private MLKeyPointFilterLevel _keyPointFilterLevel = MLKeyPointFilterLevel.ExtraSmoothed;

        [SerializeField]
        private MLPoseFilterLevel _PoseFilterLevel = MLPoseFilterLevel.ExtraRobust;


    MLHandKeyPose[] poses ={ MLHandKeyPose.OpenHandBack};

    private bool gloveFlag = false;
	public OutfielderGlove glovePart;

/// <summary>
/// Initializes and finds references to all relevant components in the
/// scene and registers required events.
/// </summary>
void OnEnable()
        {
            MLResult result = MLHands.Start();
            if (!result.IsOk)
            {
                Debug.LogErrorFormat("Error: HandTracking failed starting MLHands, disabling script. Reason: {0}", result);
                enabled = false;
                return;
            }
        else
        {
            Debug.Log("MLHands started " + result);
        }


            MLHands.KeyPoseManager.SetKeyPointsFilterLevel(_keyPointFilterLevel);
            MLHands.KeyPoseManager.SetPoseFilterLevel(_PoseFilterLevel);

        MLHands.KeyPoseManager.OnHandKeyPoseBegin += HandleKeyPoseEnter;
        MLHands.KeyPoseManager.OnHandKeyPoseEnd += HandleKeyPoseExit;


        bool status = MLHands.KeyPoseManager.EnableKeyPoses(poses, true);
        if (!status)
        {
            Debug.LogError("Error: HandTracking failed enabling tracked KeyPoses, disabling script.");
            enabled = false;
            return;
        }

        glovePart.OnBallCatch += OnBallCatch;
        SetGloveActive(false);
        gloveFlag = false;
    }

        /// <summary>
        /// Stops the communication to the MLHands API and unregisters required events.
        /// </summary>
        void OnDisable()
        {

        MLHands.KeyPoseManager.OnHandKeyPoseBegin -= HandleKeyPoseEnter;
        MLHands.KeyPoseManager.OnHandKeyPoseEnd -= HandleKeyPoseExit;

        if (MLHands.IsStarted)
            {
            bool status = MLHands.KeyPoseManager.EnableKeyPoses(poses, false);

            // Disable all KeyPoses if MLHands was started
            // and is about to stop
            MLHands.Stop();
            }

        glovePart.OnBallCatch -= OnBallCatch;
        SetGloveActive(false);
        gloveFlag = false;
    }

        /// <summary>
        /// Update KeyPoses tracked if enum value changed.
        /// </summary>

        void Update()
    {
      
        MLHand gloveHand = (HandednessOptions.dominantHand == MLInput.Hand.Left ? MLHands.Right : MLHands.Left);
        bool lastFlag = gloveFlag;
        gloveFlag = (GetKeyPoseConfidence(gloveHand) >0);
        if(gloveFlag != lastFlag)
        {
            SetGloveActive(gloveFlag);
        }

        if (gloveFlag)
        {
            //orient glove position
            Vector3 handCenter = gloveHand.Center;
            glove.transform.localPosition = handCenter;
            Vector3 flipScale = Vector3.one; flipScale.x = (HandednessOptions.dominantHand == MLInput.Hand.Right ? 1 : -1);
            glove.transform.localScale = flipScale;

            //orient glove rotation
            if (!FieldManager.Instance) return;
			hittable ballToCatch = FieldMonitor.BallInPlay;
			if (
                 FieldManager.Instance.currentlyTetheredPlayer ==null 
                || CameraScaler.isZooming 
                || (DefenseCoordinator.Instance.runningPlay != FieldPlayType.STANDARD && DefenseCoordinator.Instance.runningPlay != FieldPlayType.RETURN)

                //|| DefenseCoordinator.Instance.BallIsInPosession()

                ) {
				ballToCatch = null;
			}

			Quaternion targetRotation = Quaternion.identity;
            if (ballToCatch && !ballToCatch.hasBeenCaught) {
                //Debug.Log("ball to catch, has not been caught");
                glovePart.SetColliderEnabled(true);
                glovePart.ShowHide(true);

                if (ballToCatch.inGlove && glovePart.heldBall == ballToCatch) {
                    targetRotation = Quaternion.LookRotation(Vector3.up, Camera.main.transform.forward);
                } else {
                    targetRotation = Quaternion.LookRotation(ballToCatch.transform.position - glove.transform.position, Camera.main.transform.up);// (Camera.main.transform.TransformPoint(handCenter) - Camera.main.transform.position));
                }
                if (!glove.activeSelf) {
                    glove.transform.rotation = targetRotation;
                }
                //glove.transform.LookAt (ballToCatch.transform.position);
            } else if((ballToCatch))
            { //the player is in posession of the ball
                //Debug.Log("ball to catch, is held by player");

                glovePart.SetColliderEnabled(false);
                targetRotation = Quaternion.LookRotation(Camera.main.transform.forward, Vector3.up);
                glove.transform.rotation = targetRotation;


            }
            else {
                //Debug.Log("else else else " + ballToCatch + " -- " + glovePart.heldBall + " -- ");
                //glove.transform.rotation = Camera.main.transform.rotation;
                glovePart.SetColliderEnabled(false);
                glovePart.ShowHide(false);
                targetRotation = Camera.main.transform.rotation;

			}

			glove.transform.rotation = Quaternion.Lerp (glove.transform.rotation, targetRotation, .7f);
        }
    }
        void HandleKeyPoseEnter(MLHandKeyPose pose, MLHandType hand)
    {
        MLHandType gloveHand = (HandednessOptions.dominantHand == MLInput.Hand.Left? MLHandType.Right : MLHandType.Left);
        if (hand == gloveHand && pose == MLHandKeyPose.OpenHandBack)
        {
            SetGloveActive(true);
        }

    }
    void HandleKeyPoseExit(MLHandKeyPose pose, MLHandType hand)
    {
        MLHandType gloveHand = (HandednessOptions.dominantHand == MLInput.Hand.Left ? MLHandType.Right : MLHandType.Left);
        if (hand == gloveHand && pose == MLHandKeyPose.OpenHandBack)
        {
            SetGloveActive(false);
        }
    }


    void SetGloveActive(bool isActive)
    {
        glove.SetActive(isActive);
    }

#region Private Methods
    /// <summary>
    /// Get the confidence value for the hand being tracked.
    /// </summary>
    /// <param name="hand">Hand to check the confidence value on. </param>
    /// <returns></returns>
    private float GetKeyPoseConfidence(MLHand hand)
    {
        if (hand != null)
        {
            if (hand.KeyPose == MLHandKeyPose.OpenHandBack)
            {
                return hand.KeyPoseConfidence;
            }
        }
        return 0.0f;
    }
#endregion

    void OnBallCatch(hittable ball)
    {

    }
}
#endif
