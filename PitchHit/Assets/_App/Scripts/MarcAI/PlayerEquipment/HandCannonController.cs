﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Valve.VR;

#if LUMIN
using UnityEngine.XR.MagicLeap;
#endif

public class HandCannonController : MonoBehaviour
{
    SteamVR_Action_Boolean cannonFireButtonAction;

    public PathLine linePredictor;
    public ParticleSystem pathParticles;
    public Transform strikeZoneTarget;
    public Transform muzzle;
    public float strength = 30f;
    public HandCannonAnimationHandler handCannonAnimationHandler;
    public bool matchControllerPosition;
    public bool appearOnAwake;
    public bool isAttached;

    private new AudioSource audio;

    private hittable chamberedBall;
    private CustomHand attachedHand;

    public static bool predictionIsAllowed = true;
    public static Action<hittable> OnBallFired;

    public Transform cannonRootTransform;
    public Transform indexCannonPosition;
    public Transform oculusSVRCannonPosition;

    public bool isActivated
    {
        get;
        private set;
    }

    void Awake()
    {
        cannonFireButtonAction = SteamVR_Actions.baseballSet_ShootCannon;
        handCannonAnimationHandler.gameObject.SetActive(false);
#if STEAM_VR
        //Time to decide if we're on Index controller so we can apply the offset.
        var system = OpenVR.System;
        //SteamVR_TrackedObject.EIndex index = SteamVR_TrackedObject.EIndex.Device3;
        var error = ETrackedPropertyError.TrackedProp_Success;
        for (int index = 0; index < 6; index++)
        {
            var capacity = system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, null, 0, ref error);
            if (capacity <= 1)
            {
                //Debug.LogError("<b>[SteamVR]</b> Failed to get render model name for tracked object " + index);
                //return;
            }
            var buffer = new System.Text.StringBuilder((int)capacity);
            system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, buffer, capacity, ref error);
            string s = buffer.ToString();
            //Debug.Log("HAND DEVICE IS: " + s);
            if (s.Contains("indexcontroller"))
            {
                cannonRootTransform.localPosition = indexCannonPosition.localPosition;
                cannonRootTransform.localRotation = indexCannonPosition.localRotation;
                cannonRootTransform.localScale = indexCannonPosition.localScale;
                break;
            }
            else if (s.Contains("oculus"))
            {
                cannonRootTransform.localPosition = oculusSVRCannonPosition.localPosition;
                cannonRootTransform.localRotation = oculusSVRCannonPosition.localRotation;
                cannonRootTransform.localScale = oculusSVRCannonPosition.localScale;
                break;
            }
        }
#endif
    }

#if LUMIN

	private MLInputController _controller;
#endif
    void OnEnable()
    {
#if LUMIN
        matchControllerPosition = CameraScaler.instance.playOneToOne;

        MLInput.OnControllerButtonDown += OnControllerBumperDown;
#endif
        if (handCannonAnimationHandler && appearOnAwake)
        {
            PrepareForUse();
        }
    }

    void OnDisable()
    {
        isActivated = false;
        isAttached = false;
        if (attachedHand)
        {
            attachedHand.isOccupied = false;
            attachedHand = null;
        }
        Disappear_Immediate();
#if LUMIN
        MLInput.OnControllerButtonDown -= OnControllerBumperDown;
#endif
    }

    void Start()
    {
        //handCannonAnimationHandler = GetComponentInChildren<HandCannonAnimationHandler>();
#if LUMIN

		if (MLInput.IsStarted)
		{
			_controller = MLInput.GetController(MLInput.Hand.Left);
		}
#endif
        if (linePredictor)
        {
            linePredictor.SetPattern(PathLine.LinePattern.DOTTED);
        }
        audio = GetComponent<AudioSource>();
        if (handCannonAnimationHandler)
        {
            handCannonAnimationHandler.OnFinishIn += OnCannonFinishAppear;
            handCannonAnimationHandler.OnFinishOut += OnCannonFinishDisappear;
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveCannon();
        UpdatePrediction();
    }

    public CustomHand GetAttachedHand()
    {
        if(!isAttached)
        {
            return null;
        }
        return attachedHand;
    }
    /*
    public void RefreshHand()
    {
        //if (attachedHand) attachedHand.RefreshHandState();
    }
    */

    void MoveCannon()
    {
        attachedHand = HandManager.GetAvailableDominantHand();
        if (attachedHand)
        {
            transform.position = attachedHand.transform.position;
            transform.rotation = attachedHand.transform.rotation;
            //Debug.Log("cannon moved.");
#if RIFT
            if (attachedHand.GetUseDown())
            {
                // Debug.Log("grip down on cannon.");
                ShootBall();
            }
#elif STEAM_VR
            if (cannonFireButtonAction.GetLastStateDown(attachedHand.actualHand.handType))
            {
               // Debug.Log("grip down on cannon.");
                ShootBall();
            }
#endif
        }
    }

    void UpdatePrediction()
    {
        if (linePredictor != null)
        {
            linePredictor.ShowHide(predictionIsAllowed);
        }
        if(pathParticles != null)
        {
            pathParticles.gameObject.SetActive(predictionIsAllowed);
        }
        if (predictionIsAllowed && (handCannonAnimationHandler.IsFullyAppeared()) && GetBallToLaunch())
        {
            if (linePredictor)
            {
                linePredictor.ShowHide(true);
            }
            if (pathParticles != null)
            {
                pathParticles.gameObject.SetActive(true);
            }
            ProjectCurveBall(strength, Physics.gravity);
        }
        else
        {
            if (linePredictor)
            {
                linePredictor.ShowHide(false);
            }
            if (pathParticles != null)
            {
                pathParticles.gameObject.SetActive(false);
            }
        }
    }

    public void ForceLinePrediction()
    {
        ProjectCurveBall(strength, Physics.gravity);
    }

    private ParticleSystem.Particle[] particleBuffer = new ParticleSystem.Particle[60];
    private TrajectoryInfo particleTraj = new TrajectoryInfo();

    public void ProjectCurveBall(float initialSpeed, Vector3 accel)
    {
        if (linePredictor)
        {
            linePredictor.ShowHide(true);
        }

        particleTraj.SimulatePosition(muzzle.position);
        particleTraj.SimulateVelocity(muzzle.forward * initialSpeed);

        int numParts = pathParticles.GetParticles(particleBuffer);
        for (int i = 0; i < numParts; i++)
        {
            float t = 1f - particleBuffer[i].remainingLifetime / particleBuffer[i].startLifetime;
            particleBuffer[i].position = particleTraj.Position_at_T(t*t*t*particleBuffer[i].startLifetime);
        }
        pathParticles.SetParticles(particleBuffer, numParts);





        /*
        float timeBetweenPoints = .05f;

        Vector3[] predictionPoints = new Vector3[30];
        predictionPoints[0] = muzzle.position;
        float timePredicted = 0f;
        //Vector3 curveAcceleration = (curveStrength * Vector3.Cross(muzzle.forward.normalized, Vector3.up)) + Physics.gravity;
        for (int x = 1; x < predictionPoints.Length; x++)
        {
            timePredicted += timeBetweenPoints;
            predictionPoints[x] = predictionPoints[0] + (muzzle.forward * initialSpeed * timePredicted) + (.5f * accel * timePredicted * timePredicted);
        }

        if (linePredictor)
        {
            linePredictor.SetPositions(predictionPoints);
            //linePredictor.SetScrollSpeed (initialSpeed / 10f);
        }

        if (pathParticles)
        {
            pathParticles.transform.position = predictionPoints[0];

            Vector3 localGravity = pathParticles.transform.InverseTransformDirection(Physics.gravity);
            localGravity *= Mathf.Pow(pathParticles.velocityOverLifetime.z.constant / strength, 2f) * 2f;   //Laser line would actually match the ball's real path unless I added the *2 in there.

            ParticleSystem.ForceOverLifetimeModule force = pathParticles.forceOverLifetime;
            force.enabled = true;
            force.x = localGravity.x;
            force.y = localGravity.y;
            force.z = localGravity.z;

            //Greg: This is the only way I could think that would result in the particle path immediately addapting to the change in gravity entirely.
            pathParticles.Simulate(Time.time + (pathParticles.emission.rateOverTime.constant * 100f));
        }
    */
    }
#if LUMIN

    public void OnControllerBumperDown(byte controllerId, MLInputControllerButton button)
    {
        if (button == MLInputControllerButton.Bumper)
        {
            ShootBall();
        }
    }
#endif

    public void PrepareForUse()
    {
        isActivated = true;
        handCannonAnimationHandler.gameObject.SetActive(true);
        StartCoroutine("StartAppearingAfterHandAvailable");

    }

    IEnumerator StartAppearingAfterHandAvailable()
    {
        //Debug.Log("xxxxxxxx STARTAPPEARINGAFTERHANDAVAILABLE 1");
        while (attachedHand == null)
        {
            attachedHand = HandManager.GetAvailableDominantHand();
            yield return null;
        }
        //Debug.Log("xxxxxxxx STARTAPPEARINGAFTERHANDAVAILABLE 2");

        isAttached = true;
        handCannonAnimationHandler.gameObject.SetActive(true);
        attachedHand.isOccupied = true;

        //attachedHand.RefreshHandState();

        attachedHand.MakeHandInvisible();
        handCannonAnimationHandler.StartAppearing();
    }
    public void FiringVisualSequence(float delay)
    {
        handCannonAnimationHandler.Fire();
    }

    IEnumerator DisappearDelayRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        Disappear();
    }
    public void Disappear()
    {
        //Debug.Log("disappear");
        isActivated = false;
        handCannonAnimationHandler.StartDisappearing();
        SetAttached(false);
    }

    public void Disappear_Immediate()
    {
        isActivated = false;
        SetAttached(false);
        handCannonAnimationHandler.Disappear_Immediate();
    }

    void SetAttached(bool attached)
    {
        isAttached = attached;
        if (attachedHand)
        {
            attachedHand.isOccupied = attached;
            attachedHand.RefreshHandState();
        }
    }

    void OnCannonFinishAppear()
    {
        SetAttached(true);

    }

    void OnCannonFinishDisappear()
    {
        handCannonAnimationHandler.gameObject.SetActive(false);
        SetAttached(false);
    }



    public void ShootBall()
    {
        //Debug.Log("SHOOOOT ball");
        if (!IsReadyToFire()) return;
        hittable thisBall = GetBallToLaunch(true);
        chamberedBall = null;

        if (thisBall != null)
        {
            thisBall.hitGround = false;

            ShootBallWithVelocity(thisBall, muzzle.forward * strength);
            return;
        }
        else
        {
            Debug.Log("No ball to shoot");

        }

    }

    hittable ShootBallWithVelocity(hittable ball, Vector3 velocity)
    {
        Debug.Log("shoot ball with velocity " + velocity);
        HandManager.VibrateController(0, .5f, 120, .4f, attachedHand);

        ball.hitGround = false;
        ball.transform.SetParent(null);
        ball.transform.position = muzzle.position;
        Rigidbody ballBody = ball.GetRigidbody();
        ballBody.isKinematic = false;
        ballBody.useGravity = true;
        ballBody.velocity = velocity;
        if(ball == chamberedBall)
        {
            chamberedBall = null;
            //forceLoadedBall = null;
        }
        if (OnBallFired != null)
        {
            OnBallFired(ball);
        }
        else
        {
            Debug.Log("no ball fired listeners");
        }

        //audio.Play();

        //Disappear();
        FiringVisualSequence(1.5f);
        return ball;
    }



    public hittable ShootBallAtTarget(Vector3 target, float speed, DefensePlayer receiver = null)
    {
        if (!IsReadyToFire())
        {
            return null;
        }
        hittable thisBall = GetBallToLaunch(true);
        chamberedBall = null;
        if (thisBall == null)
        {
            Debug.Log("no ball to launch.");
            return null;
        }

        thisBall.hitGround = false;

        Rigidbody body = thisBall.GetComponent<Rigidbody>();
        float throwDistance = Vector3.Distance(target, (this.transform.position));
        Vector3 launch = Trajectory.InitialVelocityNeededToHitPoint(body, (target), throwDistance / speed);
        
        return ShootBallWithVelocity(thisBall, launch);// Trajectory.InitialVelocityNeededToHitPoint(ballToThrow.rb, target, Vector3.Distance(ballToThrow.transform.position, target) / strength));



    }

    hittable GetBallToLaunch(bool releaseBall = false)
    {
        HumanGloveController humanGlove = HandManager.instance.glove;
        if (chamberedBall == null)
        {
            if (humanGlove)
            {
                if (releaseBall)
                {
                    chamberedBall = humanGlove.ReleaseBall();
                }
                else
                {
                    chamberedBall = humanGlove.GetCurrentBall();
                }
            }
        }

        if (HumanPosessionManager.currentlyTetheredPlayer)
        {
            if(releaseBall && humanGlove.GetCurrentBall())
            {
                chamberedBall = humanGlove.ReleaseBall();
            }
        }

       return chamberedBall;
    }

    public bool IsReadyToFire(float delay = 0f)
    {
        if (AppearingMenu.isVisible)
        {
            return false;
        }

        if (handCannonAnimationHandler)
        {
            return handCannonAnimationHandler.IsFullyAppeared(delay);
        }

        return true;
    }

    public void ForceLoadBall(hittable ball)
    {
        chamberedBall = ball;
        chamberedBall.transform.SetParent(muzzle.transform);
        chamberedBall.transform.localPosition = Vector3.zero;
        chamberedBall.GetRigidbody().isKinematic = true;
        chamberedBall.GetRigidbody().useGravity = false;
    }
}
