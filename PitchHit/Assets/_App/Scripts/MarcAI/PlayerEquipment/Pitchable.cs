﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;
//using Valve.VR.InteractionSystem;

public class Pitchable : MonoBehaviour {
    public hittable hitComponent;
    public Rigidbody rb;
    public trailParticleManager trail;
    public CustomThrowable customThrowableComponent;
    public bool cantBeThrown;
    public GameObject cantBeThrownPuffPrefab;


    //public static Action<hittable> OnBallThrown;
    private Valve.VR.InteractionSystem.VelocityEstimator velocityEstimator;

   

    //private CustomHand currentCustomHand;
    private bool isHeld;

	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody>();
        if (rb)
        {
            rb.maxAngularVelocity = 21f;//Mathf.Infinity;
        }//Debug.Log("max ang: "+rb.maxAngularVelocity);

        //hitComponent = GetComponent<hittable>();
        velocityEstimator = GetComponent<Valve.VR.InteractionSystem.VelocityEstimator>();
    }

    public void OnGrabbed()
    {
        isHeld = true;
#if RIFT
        velocityEstimator.BeginEstimatingVelocity();
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
#endif
    }

    public void OnReleased()
    {
        isHeld = false;
#if RIFT
        //Logic copied from Throwable, which will not be executed without Steam_VR
        rb.isKinematic = false;

        velocityEstimator.FinishEstimatingVelocity();
            Vector3 angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
            angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
            Vector3 velocity = velocityEstimator.GetVelocityEstimate();
			Vector3 position = velocityEstimator.transform.position;
            Vector3 r = transform.TransformPoint( rb.centerOfMass ) - position;
            rb.velocity = velocity + (Vector3.Cross( angularVelocity, r ) * Pitchable.flickDirectionFactor);
            rb.angularVelocity = angularVelocity;

			// Make the object travel at the release velocity for the amount
			// of time it will take until the next fixed update, at which
			// point Unity physics will take over
			float timeUntilFixedUpdate = ( Time.fixedDeltaTime + Time.fixedTime ) - Time.time;
			transform.position += timeUntilFixedUpdate * velocity;
			float angle = Mathf.Rad2Deg * angularVelocity.magnitude;
			Vector3 axis = angularVelocity.normalized;
			transform.rotation *= Quaternion.AngleAxis( angle * timeUntilFixedUpdate, axis );
#endif
        OnPitched(customThrowableComponent.GetAttachedHand());
    }
	
    public void OnPitched(CustomHand throwingHand)
    {
#if RIFT || STEAM_VR
        //Debug.Log("Pitchable On Pitched");
        //throwingHand.OnPitched(GetComponent<Rigidbody>());
        //StartCoroutine(ThrowingRoutine(throwingHand));
        hitComponent.spotThrownFrom = transform.position;

        DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("player_throw_ball", transform);
        
        //StartCoroutine(MagnusRoutine());
        if(trail)
        {
            trail.OnHit();
        }

        if(cantBeThrown)
        {
            StartCoroutine(InvalidPitchRoutine());
        }

#endif
    }
    

    IEnumerator InvalidPitchRoutine()
    {
        Vector3 initialPosition = transform.position;
        while(Vector3.Distance(transform.position,initialPosition)<3f)
        {
            if(hitComponent.GetWasHitByBat() || isHeld || (hitComponent.hitGround && rb.velocity.magnitude<1f))
            {
                yield break;
            }

            yield return null;
        }

        Instantiate(cantBeThrownPuffPrefab, transform.position, transform.rotation, null);
        Destroy(gameObject);
    }


#if STEAM_VR
    public static float flickDirectionFactor = 0;//3.0f;
    public static float throwSpeedMultiplier = .005f;//.005f;
#elif RIFT
    public static float throwSpeedMultiplier = .007f;
    public static float flickDirectionFactor = 1f;
#elif GEAR_VR
    public static float flickDirectionFactor = 1f;
    public static float throwSpeedMultiplier = .007f;
#else
    public static float flickDirectionFactor = 3.0f;
    public static float throwSpeedMultiplier = .005f;
#endif
#if RIFT || STEAM_VR

    public Renderer debugRenderer;


   

    const string globalLinearVelocityPrefsKey = "THROW_SPEED_MOD";
    public static float globalLinearVelocityMod
    {
        get
        {
            if (_globalLinearVelocityMod < 0)
            {
                _globalLinearVelocityMod = Mathf.Max(1, PlayerPrefs.GetFloat(globalLinearVelocityPrefsKey, 1));
            }
            return _globalLinearVelocityMod;
        }
        set
        {
            _globalLinearVelocityMod = value;
            PlayerPrefs.SetFloat(globalLinearVelocityPrefsKey, value);
        }
    }
    private static float _globalLinearVelocityMod = -1;
    /// <summary>
    /// Greg Tomargo's throwing algorithm. Uses wrist rotation
    /// Runs in parallel with CustomHand.ThrowingRoutine
    /// </summary>
    /// <param name="throwingHand"></param>
    /// <returns></returns>
    

    public void DebugColor(bool on)
    {
        if (debugRenderer)
        {
            debugRenderer.material.color = on ? Color.red : Color.white;
        }
    }

    const string assistPrefsKey = "ASSIST_WEIGHT";
    public static float assistWeight
    {
        get {
            if (_assistWeight < 0)
            {
                _assistWeight = PlayerPrefs.GetFloat(assistPrefsKey, 1);
            }
            return _assistWeight;
        }
        set {
            _assistWeight = Mathf.Clamp01(value);
            PlayerPrefs.SetFloat(assistPrefsKey, _assistWeight);
        }
    }
    private static float _assistWeight = -1;





#endif
        }
