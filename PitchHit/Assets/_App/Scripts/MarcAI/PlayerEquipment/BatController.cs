﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.Events;


//using UnityEngine.XR.MagicLeap;

public class BatEvent : UnityEvent<hittable> { }

public class BatController : MonoBehaviour {

    public Transform Grip {
        get { return _grip; }
    }
    public Transform Tip {
        get { return _tip; }
    }


    [SerializeField] private Transform _grip;
    [SerializeField] private Transform _tip;

    public static BatEvent onBatHit = new BatEvent();

    public static Action onSwing;

    Transform sourceController;
    public Rigidbody rb;
    public float strengthMultiplier = 1f;
    //public Transform tipPoint;
    // public Transform gripPoint;
    [HideInInspector]
    public Vector3 currentHitDirection;
    public float reflectionDamp = .5f;

    private new AudioSource audio;

    public ParticleSystem[] hitParticlePrefabs;
    public AudioClip[] hitSounds;
    public AudioClip[] strongHitSounds;
    protected Collider[] cols;

    private List<MathUtility.StraightLine> history = new List<MathUtility.StraightLine>();
    private int maxHistoryEntries = 100;
    [HideInInspector]
    public Vector3 movingAverage = Vector3.zero;
    private Vector3 initialFlatForward;

    MeshRenderer meshRenderer;

    private bool swingFlag;
    public bool isSwinging;
    public float swingDetectionThreshold = 10;
    public bool alwaysUseStrongSounds;

    [Range(0, 1)]
    public float missedSwingChance;

    private hitThings hit;

    public Transform batRootTransform;
    public Transform oculusSVRBatPosition;
    public Transform indexBatPosition;

    void Awake()
    {
        if (!hit)
        {
            hit = GetComponent<hitThings>();
        }

        audio = GetComponent<AudioSource>();
        cols = GetComponentsInChildren<Collider>(true);

        meshRenderer = GetComponentInChildren<MeshRenderer>();

#if STEAM_VR
        //Time to decide if we're on Index controller so we can apply the offset.
        var system = OpenVR.System;
        //SteamVR_TrackedObject.EIndex index = SteamVR_TrackedObject.EIndex.Device3;
        var error = ETrackedPropertyError.TrackedProp_Success;
        for (int index = 0; index < 6; index++)
        {
            var capacity = system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, null, 0, ref error);
            if (capacity <= 1)
            {
                //Debug.LogError("<b>[SteamVR]</b> Failed to get render model name for tracked object " + index);
                //return;
            }
            var buffer = new System.Text.StringBuilder((int)capacity);
            system.GetStringTrackedDeviceProperty((uint)index, ETrackedDeviceProperty.Prop_RenderModelName_String, buffer, capacity, ref error);
            string s = buffer.ToString();
            //Debug.Log("HAND DEVICE IS: " + s);
            if (s.Contains("indexcontroller"))
            {
                //Debug.Log("Giving the mit the index offset");
                if(indexBatPosition != null)
                {
                    batRootTransform.localPosition = indexBatPosition.localPosition;
                    batRootTransform.localRotation = indexBatPosition.localRotation;
                    batRootTransform.localScale = indexBatPosition.localScale;
                }
                break;
            }
            else if (s.Contains("oculus"))
            {
                if (oculusSVRBatPosition)
                {
                    batRootTransform.localPosition = oculusSVRBatPosition.localPosition;
                    batRootTransform.localRotation = oculusSVRBatPosition.localRotation;
                    batRootTransform.localScale = oculusSVRBatPosition.localScale;
                }
                break;
            }
        }
#endif
    }
    // Use this for initialization
    void Start() {
        initialFlatForward = transform.forward; initialFlatForward.y = 0f;
    }

    Vector3 previousForward;
    void FixedUpdate()
    {
        RecordHistory();
        CheckForSwing();

        if (isSwinging)
        {
            SweepForBalls();
        }
        //rb.MoveRotation(controller rotation.)
    }

    public void SetColliderActive(bool active)
    {
//        Debug.Log("Set bat colliders active " + active + " " + this.name);
        foreach (Collider col in cols)
        {
            if (col)
            {
                col.enabled = active;
            }
        }
    }

    public void HitRigidbody(Rigidbody ballBody, Collision collision)
    {
        ContactPoint hit = collision.contacts[0];
        StartCoroutine(HitRigidbodyNextFixedUpdate(ballBody, hit.point, hit.normal, movingAverage));
    }

    public void HitRigidbody(Rigidbody ballBody, RaycastHit hit, float forceDir_x = 0, float forceDir_y = 0, float forceDir_z = 0)
    {
        bool saved = false;
        Vector3 forceDirection = new Vector3(forceDir_x, forceDir_y, forceDir_z);
        Vector3 normal = hit.normal;
        if (forceDirection == Vector3.zero)
        {
            forceDirection = movingAverage;
        }
        else
        {
            saved = true;
            normal = forceDirection.normalized;
        }

        StartCoroutine(HitRigidbodyNextFixedUpdate(ballBody, hit.point, normal, forceDirection));
    }


    private IEnumerator HitRigidbodyNextFixedUpdate(Rigidbody ballBody, Vector3 hitPoint, Vector3 hitNormal, Vector3 forceDirection)
    {
        SetColliderActive(false);
        hittable ballHit = ballBody.GetComponent<hittable>();
        ballHit.velocityBeforeBat = ballBody.velocity;
        ballBody.position = hitPoint;

        forceDirection = forceDirection * strengthMultiplier;

        //        Debug.Log(forceDirection.magnitude + " saved? " + saved);

        Vector3 nextBallPosition = ballBody.position + ballBody.velocity * Time.fixedDeltaTime;
        Vector3 reflectedVelocity = MathUtility.ReflectedVector(ballBody.velocity, hitNormal);
        reflectedVelocity *= (1 - reflectionDamp);
        ballBody.velocity = reflectedVelocity;// + forceDirection;

        float glanceFactor = Mathf.Clamp01(1 - (Vector3.Angle(hitNormal, movingAverage.normalized) / 90f));
        ballBody.AddForceAtPosition(forceDirection * glanceFactor, hitPoint);

        yield return new WaitForFixedUpdate();

        PlayHitEffects(ballBody, hitPoint, ballBody.transform.position, forceDirection);


        if (ballHit) {
            ballHit._onBatHit(hitPoint);

            if (onBatHit != null)
            {
                onBatHit.Invoke(ballHit);
            }
        }

        if (!hit)
        {
            hit = GetComponent<hitThings>();
        }
        if (hit)
        {
            hit.VibrateBat();
        }

        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();

        SetColliderActive(true);
    }

    public void PlayHitEffects(Rigidbody ballRB, Vector3 contactPoint, Vector3 ballPosition, Vector3 force)
    {
        TrajectoryInfo trajectory = new TrajectoryInfo(ballRB);
        Vector3 groundPos = trajectory.groundHitPosition;

        if (isSwinging && groundPos.magnitude > 50f)
        {
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_hit", transform);
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", transform);
        }
        else if (isSwinging)
        {
            if(UnityEngine.Random.Range(0, 4) == 3) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_sweetener_linedrive", transform);
            else DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_hit", transform);
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", transform);
        }
        else
        {
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_bunt", transform);
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", transform);
        }

        //Debug.Log("spawn from hit particle Prefabs. "+hitParticlePrefabs.Length+" ");
        GameObject temp = Instantiate(hitParticlePrefabs[UnityEngine.Random.Range(0, hitParticlePrefabs.Length)].gameObject, contactPoint, Quaternion.LookRotation(force.normalized));
        if(hit && hit.held)
        {
            GameController.SetLayerRecursively(temp, LayerMask.NameToLayer("SpectatorOnly"));
            //temp.layer = LayerMask.NameToLayer("SpectatorOnly");
        }
        Destroy(temp, 3f);
        
        OneShotEffectManager.SpawnEffect("hit_pow", contactPoint, Quaternion.identity, 3f);

    }

    void RecordHistory()
    {
        /*
        history.Add(new MathUtility.StraightLine(gripPoint.position, tipPoint.position));
        while(history.Count > maxHistoryEntries)
        {
            history.RemoveAt(0);
        }
        */

        Vector3 swing = Vector3.Cross(previousForward, transform.forward) / Time.fixedDeltaTime;

        currentHitDirection = Vector3.Cross(swing, transform.forward).normalized * (swing.magnitude);

        previousForward = transform.forward;

        movingAverage = (movingAverage + currentHitDirection) / 2f;

       // Debug.DrawLine(rb.position, rb.position + movingAverage * 1000, Color.yellow, Time.fixedDeltaTime);

    }

    public void ResetSwingFlag()
    {
        swingFlag = false;
    }
    public bool SwingWasDetected()
    {
        return swingFlag;
    }

    private void CheckForSwing()
    {
        isSwinging = movingAverage.magnitude > swingDetectionThreshold;
        if (isSwinging && !swingFlag)
        {
            if (onSwing != null) onSwing.Invoke();
            swingFlag = true;
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("sfx_bat_swing_whoosh", transform);
        }
        else if((movingAverage.magnitude < (swingDetectionThreshold / 8f)) && swingFlag)
        {
            swingFlag = false;
        }

        if (isSwinging)
        {
            //meshRenderer.material.color = Color.red;
        }
        else
        {
            meshRenderer.material.color = Color.white;
        }
//        Debug.Log("Swing " + movingAverage.magnitude + " / " + swingDetectionThreshold);


    }

    /// <summary>
    /// Not currently used
    /// </summary>
    void SweepForBalls()
    {
        RaycastHit hit;
        if (rb.SweepTest(currentHitDirection, out hit,rb.velocity.magnitude * Time.fixedDeltaTime))
        {
            hittable ball = (hit.collider.GetComponent<hittable>());
            if(ball != null)
            {
                Debug.Log("<color=blue>Swinging Bat Sweep hit ball</color>");
                //HitRigidbodyNextFixedUpdate(ball.GetRigidbody(), hit);
            }
        }
    }

    public CustomHand GetAttachedHand()
    {
        if (!hit)
        {
            hit = GetComponent<hitThings>();
        }
        if (hit)
        {
            if(hit.attached == false)
            {
                return null;
            }

            return hit.attachedCustomHand;
        }
        return null;
    }

    public void SetHoldable(bool isHoldable)
    {
        if (!hit)
        {
            hit = GetComponent<hitThings>();
        }
        if (hit)
        {
            hit.holdable = isHoldable;
            if (!isHoldable)
            {
                hit.onRelease();
            }
        }
    }
}
