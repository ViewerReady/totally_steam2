﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BallCastAhead : MonoBehaviour {
    public float fastCastRadius;
    public float slowCastRadius = 1;

    public float fastCastThreshold = 10;
    
    public Vector2 safetyCastWindow_bat;
    public Vector2 safetyCastWindow_glove;

    public new Rigidbody rigidbody;
    private hittable _ball
    {
        get
        {
            if (m_ball == null) m_ball = GetComponent<hittable>();
            return m_ball;
        }
    }
    private hittable m_ball;

    private bool fastCasting = false;

    private LayerMask batCastMask;
    private LayerMask gloveCastMask;

    public bool safetyHitFlag { get; private set; }

    TrajectoryInfo info;

    private Vector3 safetyWindowStart_bat{
        get
        {
            return rigidbody.position + rigidbody.velocity * safetyCastWindow_bat.x;
        }
    }
    private Vector3 safetyWindowEnd_bat
    {
        get
        {
            return rigidbody.position + rigidbody.velocity * safetyCastWindow_bat.y;
        }
    }

    private Vector3 safetyWindowStart_glove
    {
        get
        {
            return rigidbody.position + rigidbody.velocity * safetyCastWindow_glove.x;
        }
    }
    private Vector3 safetyWindowEnd_glove
    {
        get
        {
            return rigidbody.position + rigidbody.velocity * safetyCastWindow_glove.y;
        }
    }

    RaycastHit savedHit;
    float savedHitTime = -1;
    float savedHitOffset;
    BatController savedBat;
    Vector3 savedBatDirection;


    HumanGloveController savedGlove;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.sleepThreshold = -1f;
        info = new TrajectoryInfo(rigidbody);

    }

    void Awake()
    {
        batCastMask = LayerMask.GetMask("Bat");
        gloveCastMask = LayerMask.GetMask("Glove");
        _ball.OnCaughtByDefender += ReactToCatch;
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Color c;
        if (fastCasting)
        {
            if (!_ball.GetWasHitByBat())
            {
                c = Color.cyan;
                c.a = .25f;
                Handles.color = c;
                Handles.DrawSphere(0, safetyWindowStart_bat, Quaternion.identity, fastCastRadius * 2);
                Handles.DrawSphere(0, safetyWindowEnd_bat, Quaternion.identity, fastCastRadius * 2);
            }   
        }
        if (!_ball.isHeld)
        {
            c = Color.red;
            c.a = .25f;
            Handles.color = c;
            Handles.DrawSphere(0, safetyWindowStart_glove, Quaternion.identity, gloveCastRadius * 2);
            Handles.DrawSphere(0, safetyWindowEnd_glove, Quaternion.identity, gloveCastRadius * 2);
        }

        c = Color.cyan;
        c.a = .25f;
        Handles.color = c;
        //Handles.DrawSphere(0, rigidbody.position + rigidbody.velocity * (Time.fixedDeltaTime), Quaternion.identity, fastCastRadius * 2);
        //Handles.DrawLine(safetyWindowStart_bat, safetyWindowEnd_bat);
       // Handles.color = Color.red;


        if (savedHitTime >0)
        {
           c = Color.yellow;
            c.a = .25f;
            Handles.color = c;
            Handles.DrawSphere(0, savedHit.point, Quaternion.identity, fastCastRadius * 2);

        }
    }
#endif
    void ReactToCatch(hittable ball)
    {
        ClearSavedHit();
    }
    // Update is called once per frame
    void FixedUpdate () {

        fastCasting = (rigidbody.velocity.magnitude >= fastCastThreshold);
           
        if(fastCasting)
        {
            if (!_ball.GetWasHitByBat())
            {
                safetyHitFlag = false;
                CastAheadForBat();
            }
        }

        if (!_ball.isHeld)
            CastAheadForGlove();
        else
        {
            if (savedGlove != null)
            {
                savedGlove.CloseCatchWindow();
                savedGlove = null;
                //end catch window
            }
        }

        if (!_ball.GetWasHitByBat() && savedHitTime > 0 && Time.time >= savedHitTime)
        {
            UseSavedBatHit();
        }
        DrawProjectedPath();
    }


    RaycastHit batHit;
    BatController bat;

    void CastAheadForBat()
    {


        //immediate hit
        if(Physics.SphereCast(rigidbody.position, fastCastRadius, rigidbody.velocity, out batHit, (rigidbody.velocity * (Time.fixedDeltaTime)).magnitude, batCastMask))
        {
            if (batHit.rigidbody != null)
            {
                bat = batHit.rigidbody.GetComponent<BatController>();
                if (bat != null)
                {
                    Debug.Log("<color=blue>Ball cast immediate hit bat</color>");
                    bat.HitRigidbody(rigidbody, batHit);
                    _ball._onBatHit(batHit.point);
                    ClearSavedHit();
                }
            }
        }

        //future/past hit
        else if (!_ball.GetWasHitByBat() && Physics.SphereCast(safetyWindowStart_bat, fastCastRadius, rigidbody.velocity, out batHit, (rigidbody.velocity * (safetyCastWindow_bat.y - safetyCastWindow_bat.x)).magnitude, batCastMask))
        {
//            Debug.Log("Safety HIT");
            if(batHit.rigidbody != null)
            {
//                Debug.Log("Rigidbody is there");
                bat = batHit.rigidbody.GetComponent<BatController>();
                if (bat != null)
                {
                    savedBat = bat;
                    savedHit = batHit;
                    savedHitOffset = safetyCastWindow_bat.x + batHit.distance / rigidbody.velocity.magnitude;
                    savedHitTime = Time.time + savedHitOffset;

                    savedBatDirection = bat.movingAverage;
//                    Debug.Log("Saved hit time " + savedHitTime + " TIME " + Time.time + " dist " + hit.distance / rigidbody.velocity.magnitude);
                }
            }
            else
            {
//                Debug.Log("Rigidbody is NOT there");
            }
        }
    }


    private Ray gloveCastRay;
    private RaycastHit[] gloveHits;
    private float gloveCastRadius = .5f;
    HumanGloveController humanGlove;

    void CastAheadForGlove()
    {
         gloveCastRadius = _ball.hitGround? slowCastRadius : fastCastRadius;

        //Debug.DrawRay(safetyWindowStart_glove, rigidbody.velocity.normalized * (rigidbody.velocity * (safetyCastWindow_glove.y - safetyCastWindow_glove.x)).magnitude, Color.red);
        //future/past hit
        gloveCastRay.origin = safetyWindowStart_glove;
        gloveCastRay.direction = rigidbody.velocity;


        gloveHits = Physics.SphereCastAll(
            gloveCastRay,
            gloveCastRadius,
            (rigidbody.velocity * (safetyCastWindow_glove.y - safetyCastWindow_glove.x)).magnitude,
            gloveCastMask
            );

        if(gloveHits.Length > 0)
        {
            foreach(RaycastHit gloveHit in gloveHits)
            {
                humanGlove = gloveHit.rigidbody.GetComponent<HumanGloveController>();
                if (humanGlove != null && humanGlove != ignoredGlove)
                {
//                    Debug.Log("found glove control");
                    if (savedGlove != null && humanGlove == savedGlove)
                    {
                        //already in catch window
                    }
                    else
                    {
                        savedGlove = humanGlove;
                        savedGlove.OpenCatchWindow(_ball);
                        //start catch window
                    }
                    break;
                }
            }
        }

        else
        {
            if(savedGlove != null)
            {
                savedGlove.CloseCatchWindow();
                savedGlove = null;
                //end catch window
            }
        }
    }


    void UseSavedBatHit()
    {
        if (savedBat != null)
        {
            Debug.Log("<color=blue>Ball cast ahead hit bat</color>");
            safetyHitFlag = true;
            savedBat.HitRigidbody(rigidbody, savedHit, savedBatDirection.x, savedBatDirection.y, savedBatDirection.z);
            _ball._onBatHit(savedHit.point);
            ClearSavedHit();
        }
        
    }

    void ClearSavedHit()
    {
        savedHitTime = -1;
        savedBat = null;
    }

    void DrawProjectedPath()
    {
#if UNITY_EDITOR
        info.DebugDrawTrajectory(.01f, 10, Color.cyan);
#endif
    }

    HumanGloveController ignoredGlove;
    public void IgnoreGloveForSeconds(HumanGloveController glove, float seconds)
    {
        StartCoroutine(IgnoreGloveForSecondsRoutine(glove, seconds));
    }

    private IEnumerator IgnoreGloveForSecondsRoutine(HumanGloveController glove, float seconds)
    {
        ignoredGlove = glove;
        yield return new WaitForFixedUpdate();
        yield return new WaitForSeconds(seconds);
        ignoredGlove = null;
    }
}
