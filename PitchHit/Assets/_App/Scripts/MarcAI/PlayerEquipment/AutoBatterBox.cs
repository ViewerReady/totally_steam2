﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;


public enum HitType
{
    FOUL,
    HOMERUN,
    GROUNDER,
    POPFLY
}

public class AutoBatterBox : MonoBehaviour {
	public bool hitToOutfieldersOnly;
    public float horizontalSpread = 45f;
    public float verticalSpread = 45f;
    public Vector2 shootSpeedRange = new Vector2(25f, 40f);
    public float homerunShootSpeed = 50f;
    public Transform launchTransform;

    [Range(0f,1f)]
    public float foulLikelihood = .3f;
    [Range(0f, 1f)]
    public float fairBallLikelihood = .7f;
    [Range(0f, 1f)]
    public float homerunLikelihood = .1f;
    [Range(0f, 1f)]
    public float grounderLikelihood = .3f;
    [Range(0f, 1f)]
    public float flyballLikelihood = .7f;

    public Vector2 minMaxGrounderAngle;
    public Vector2 minMaxFlyAngle;
   

    private float totalFairnessProbability;
    private float totalLiftProbability;

    public Action<hittable> OnBallDetected;

    public bool hasDetectedPitch
    {
        get { return lastDetectedPitch != null; }
    }
    private hittable lastDetectedPitch;

    public BatController bat;



    public bool forceVelocity = false;
    public Vector3 forceLaunchVelocity;
    //public WorldScaler worldScale;

    

    [Range(0,1)]
	public float strikeLikelyhood = .5f;

    void Start()
    {
        totalFairnessProbability = foulLikelihood + homerunLikelihood + fairBallLikelihood;
        totalLiftProbability = grounderLikelihood + flyballLikelihood;

        if(!bat)
        {
            bat = FindObjectOfType<BatController>();
        }
    }

    void OnTriggerEnter(Collider other)
    {
//		Debug.Log ("AUTO BATTER BOX ON ENTER "+other.name);
        hittable ball = other.GetComponent<hittable>();
        if (ball != null && !ball.hasBeenCaught)
        {
//			Debug.Log ("THAT'S THE BALL!");
            lastDetectedPitch = ball;
            if(OnBallDetected != null) OnBallDetected(ball);
            
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        TrajectoryInfo traj = new TrajectoryInfo();
        traj.SimulatePosition(new Vector3(0,1,1));

        Handles.color = Color.blue;
        traj.SimulateVelocity(GetLaunchVelocity(HitType.GROUNDER,.5f, 0f, 0f));
        traj.DebugDrawTrajectory_Handles();

        traj.SimulateVelocity(GetLaunchVelocity(HitType.GROUNDER,.5f, 1f, 1f));
        traj.DebugDrawTrajectory_Handles();

        Handles.color = Color.yellow;
        traj.SimulateVelocity(GetLaunchVelocity(HitType.POPFLY, .5f, 0f, 0f));
        traj.DebugDrawTrajectory_Handles();

        traj.SimulateVelocity(GetLaunchVelocity(HitType.POPFLY, .5f, 1f, 1f));
        traj.DebugDrawTrajectory_Handles();

        Handles.color = Color.red;
        traj.SimulateVelocity(forceLaunchVelocity);
        traj.DebugDrawTrajectory_Handles();

        

    }

#endif



    //Give the old ball a frame to call OnDestroy
	IEnumerator FireBallAfterFrame(hittable ball)
    {
        yield return null;

			ShootBall (ball);
		
        
    }

	public void ShootBall(hittable ball)
	{


        //Quaternion rotation = Quaternion.Euler (spread) * launchTransform.rotation;

        Vector3 launchVelocity = Vector3.zero;

        if (forceVelocity)
        {
            launchVelocity = forceLaunchVelocity;
        }
        else if (OffenseCoordinator.repeatLastLaunch)
        {
            launchVelocity = OffenseCoordinator.savedLaunch;
            ball.transform.position = OffenseCoordinator.savedLaunchOrigin;
        }
        else
        {
            launchVelocity = GetCurratedLaunchVelocity();
            OffenseCoordinator.savedLaunch = launchVelocity;
            OffenseCoordinator.savedLaunchOrigin = ball.transform.position;
        }


        ball.SetVelocity(launchVelocity);

        if (bat != null)
            bat.PlayHitEffects(ball.GetRigidbody(), ball.transform.position, ball.transform.position, launchVelocity);

        //Debug.Log("Shoot ball with velocity :: " + launchVelocity);
        ball._onBatHit(ball.transform.position);
        BatController.onBatHit.Invoke(ball);
        
	}

    private Vector3 GetRandomLaunchVelocity()
    {
        Vector3 launchVelocity = Vector3.zero;
        Vector3 spread = new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), 0.0f);
        spread.y *= horizontalSpread;
        spread.x *= verticalSpread;
        if(launchTransform) launchVelocity = Quaternion.AngleAxis(spread.y, launchTransform.right) * Quaternion.AngleAxis(spread.x, launchTransform.up) * launchTransform.forward;
        else launchVelocity = Quaternion.AngleAxis(spread.y, Vector3.right) * Quaternion.AngleAxis(spread.x, Vector3.up) * Vector3.forward;
        launchVelocity = launchVelocity.normalized * UnityEngine.Random.Range(shootSpeedRange.x, shootSpeedRange.y);
        return launchVelocity;
    }

    private Vector3 GetLaunchVelocity(HitType type, float latLerp = -1f, float liftLerp = -1f, float speedLerp = -1f)
    {
        if (latLerp < 0) latLerp = UnityEngine.Random.value;
        if (liftLerp < 0) liftLerp = UnityEngine.Random.value;
        if (speedLerp < 0) speedLerp = UnityEngine.Random.value;

        float lateralAngle = 0;
        float liftAngle = 0;
        float speed = 0;

        switch (type)
        {
            case HitType.FOUL:
                if (latLerp <= .5f)
                    lateralAngle = Mathf.Lerp(-90f, 0, latLerp);
                else
                    lateralAngle = Mathf.Lerp(0, 90f, latLerp);
                liftAngle = Mathf.Lerp(minMaxGrounderAngle.x, minMaxFlyAngle.y, liftLerp);
                speed = Mathf.Lerp(shootSpeedRange.x, shootSpeedRange.y, speedLerp);

                break;
            case HitType.HOMERUN:
                lateralAngle = Mathf.Lerp(-40f, 40f, latLerp);
                liftAngle = Mathf.Lerp(35f, 50f, liftLerp);
                speed = homerunShootSpeed;
                break;
            case HitType.GROUNDER:
                lateralAngle = Mathf.Lerp(-45f, 45f, latLerp);
                liftAngle = Mathf.Lerp(minMaxGrounderAngle.x, minMaxGrounderAngle.y, liftLerp);
                speed = Mathf.Lerp(shootSpeedRange.x, shootSpeedRange.y, speedLerp);
                break;
            case HitType.POPFLY:
                lateralAngle = Mathf.Lerp(-45f, 45f, latLerp);
                liftAngle = Mathf.Lerp(minMaxFlyAngle.x, minMaxFlyAngle.y, liftLerp);
                speed = Mathf.Lerp(shootSpeedRange.x, shootSpeedRange.y, speedLerp);
                break;
        }

        return AssembleLaunchVelocity(lateralAngle, liftAngle, speed);
    }

    private Vector3 AssembleLaunchVelocity(float lateralAngle, float liftAngle, float speed)
    {
        Vector3 launchVelocity = Vector3.zero;
        if (launchTransform) launchVelocity = Quaternion.AngleAxis(-liftAngle, launchTransform.right) * launchTransform.forward;//tilt it.
        launchVelocity = Quaternion.AngleAxis(-liftAngle, Vector3.right) * Vector3.forward;//tilt it.
        launchVelocity = Quaternion.AngleAxis(lateralAngle, Vector3.up) * launchVelocity;//yaw it
        launchVelocity = launchVelocity.normalized * speed;//set magnitude
        return launchVelocity;
    }

    
    private Vector3 GetCurratedLaunchVelocity()
    {
        HitType hit = HitType.GROUNDER;
        float rand = UnityEngine.Random.value * totalFairnessProbability;
        //hit foul
        if (rand < foulLikelihood)
        {
            hit = HitType.FOUL;
        }
        //hit fair
        else if (rand < foulLikelihood + fairBallLikelihood)
        {
            if (UnityEngine.Random.value * totalLiftProbability < grounderLikelihood)
                hit = HitType.GROUNDER;
            else
                hit = HitType.POPFLY;
        }
        //hit homerun
        else
        {
            //MARC: ENOUGH WITH THE HOMERUNS!!! figure this out later
            hit = HitType.POPFLY;// HitType.HOMERUN;
        }
        Vector3 launch = GetLaunchVelocity(hit);
        //Debug.Log("CPU Batter hit:: " + hit + "  " + launch);

        return launch;

    }
    /*
    private Vector3 GetFoulVelocity()
    {
        float lateralAngle = UnityEngine.Random.Range(-45f, 45f);
        if(lateralAngle<0)
        {
            lateralAngle -= 45f; //make it a foul to the left.
        }
        else
        {
            lateralAngle += 45f;//make it a foul to the right.
        }

        
        Vector3 launchVelocity = Quaternion.AngleAxis(GetCuratedLiftAngle(), transform.right) * transform.forward;//tilt it.
        launchVelocity = Quaternion.AngleAxis(lateralAngle, Vector3.up) * launchVelocity;//yaw it
        launchVelocity = launchVelocity.normalized * UnityEngine.Random.Range(shootSpeedRange.x, shootSpeedRange.y);//set magnitude
        return launchVelocity;
    }


    private Vector3 GetFairVelocity(float latLerp = -1f, float liftLerp = -1f, float speedLerp = -1f)
    {
        if (latLerp < 0) latLerp = UnityEngine.Random.value;
        if (liftLerp < 0) liftLerp = UnityEngine.Random.value;
        if (speedLerp < 0) speedLerp = UnityEngine.Random.value;

        float lateralAngle = Mathf.Lerp(-45f, 45f, latLerp);
        
        Vector3 launchVelocity = Quaternion.AngleAxis(GetCuratedLiftAngle(), transform.right) * transform.forward;//tilt it.
        launchVelocity = Quaternion.AngleAxis(lateralAngle, Vector3.up) * launchVelocity;//yaw it
        launchVelocity = launchVelocity.normalized * Mathf.Lerp(shootSpeedRange.x, shootSpeedRange.y, speedLerp);//set magnitude

        Debug.Log("------------------------ set launch velocity of ball " + launchVelocity.magnitude + " at angle " + lateralAngle);

                return launchVelocity;
    }

    private Vector3 GetHomerunVelocity()
    {
        Debug.Log("it's a homerun.");
        float lateralAngle = UnityEngine.Random.Range(-40f, 40f);
        float liftAngle = UnityEngine.Random.Range(35f, 50f);

        Vector3 launchVelocity = Quaternion.AngleAxis(liftAngle, -transform.right) * transform.forward;//tilt it. (not sure why this one uses the lift angle negatively.)
        launchVelocity = Quaternion.AngleAxis(lateralAngle, Vector3.up) * launchVelocity;//yaw it
        launchVelocity = launchVelocity.normalized * UnityEngine.Random.Range(shootSpeedRange.y,homerunShootSpeed);//set magnitude

        return launchVelocity;
    }

    private float GetCuratedLiftAngle()
    {
        float rand = UnityEngine.Random.value * totalLiftProbability;
        if(rand< grounderLikelihood)
        {
            Debug.Log("It's a grounder.");
            return GetGrounderAngle();

        }
        else
        {
            Debug.Log("it's a fly ball.");
            return GetPopFlyAngle();
        }
    }

    private float GetGrounderAngle(float lerp = -1f)
    {
        if (lerp < 0) lerp = UnityEngine.Random.value;
        return Mathf.Lerp(minMaxGrounderAngle.x, minMaxGrounderAngle.y, lerp);
    }

    private float GetPopFlyAngle(float lerp = -1f)
    {
        if (lerp < 0) lerp = UnityEngine.Random.value;
        return Mathf.Lerp(minMaxFlyAngle.x, minMaxFlyAngle.y, lerp);
    }
    */


    public void ShootBallAtTarget(hittable ball, Vector3 landingSpot)
	{
//		Debug.Log ("AUTOBATTER BOX SHOOT BALL AT TARGET.");

		float speed = UnityEngine.Random.Range (shootSpeedRange.x, shootSpeedRange.y);
        float extraHangTime = 4f;
		Vector3 launchVelocity = Trajectory.InitialVelocityNeededToHitPoint (ball.GetRigidbody(), landingSpot, (Vector3.Distance (ball.transform.position, landingSpot) / speed));

		ball.GetRigidbody().velocity = launchVelocity;


		if (bat != null)
			bat.PlayHitEffects(ball.GetRigidbody(), ball.transform.position, ball.transform.position, launchVelocity);


		BatController.onBatHit.Invoke(ball);
	}

    public void HitLastDetectedPitch()
    {
        if (lastDetectedPitch == null) return;
        if (!FullGameDirector.Instance.offense.humanControlled)
        {
            if (UnityEngine.Random.value >= strikeLikelyhood)
            {
                if (FullGameDirector.Instance.PitchInProgress())
                {
					if (hitToOutfieldersOnly) {
						ShootBallAtTarget (lastDetectedPitch, DefenseCoordinator.Instance.positions [UnityEngine.Random.Range (5, 8)].positionObject.position + Vector3.up * 1.5f);//outfielders exist in positions of 5,6,7 (Range(int,int) is max exclusive.)
					} else {
						ShootBall (lastDetectedPitch);
					}
                }
            }
        }
    }

    public void HitBallInPlay()
    {
        if (!FieldMonitor.Instance || !FieldMonitor.ballInPlay) return;
        hittable ball = FieldMonitor.ballInPlay;
        if (UnityEngine.Random.value >= strikeLikelyhood)
        {
            ShootBall(ball);
        }
    }

    public void ClearLastDetectedPitch()
    {
        lastDetectedPitch = null;
    }
}
