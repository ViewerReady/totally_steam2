﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
#if STEAM_VR
using Valve.VR;
using Valve.VR.InteractionSystem;
#endif

public class BallEvent : UnityEvent<hittable> { }

//[RequireComponent(typeof(Interactable))] 
[RequireComponent(typeof(Rigidbody))]
public class hitThings : MonoBehaviour
{
    public enum gripType
    {
        holding, toggle, always
    }

    //private static gripType[] allGripTypes;

    public static hitThings instance;
    public static gripType currentGripType
    {
        get
        {
            //if backing val is <0, it needs to be updated from playerprefs
            if (_currentGripType<0) _currentGripType = PlayerPrefs.GetInt("GRIPTYPE", 0);
            return (gripType)_currentGripType;
        }
        set
        {
            _currentGripType = (int)value;
            PlayerPrefs.SetInt("GRIPTYPE", _currentGripType);
        }
    }
    private static int _currentGripType = -1;
    public static bool pivotAroundTee = true;
    public static bool doublePivot = true;
    public bool holdable = true;
    public bool held = false;
    public bool attached = false;
    public bool debugMode;
    public bool ghostBat;
    //private GameObject ghostBatContainer;
    public AnimationCurve vibrateFade;
    //[HideInInspector]
#if STEAM_VR
    public Hand attachedHand;
    private Player thisPlayer;
#elif RIFT
    public float gripTilt = -45f;//-61
    public Vector3 gripOffset;//0 -.015 0

    //public float gripTiltAdjust = 65f;
    public Transform oculusGripPosition;

#endif
    public CustomHand attachedCustomHand;
    private OVRGrabber attachedGrabber;

    //public BallEvent onBallHit = new BallEvent();

    public AudioClip[] batHits;

    public Transform gripTransform;
    public Collider batCollider;
    public bool useFastBatColliders = true;
    public Transform fastBatContainer;
    public Collider[] fastBatColliders;
    private Vector3 previousFastBatPosition;
    private float fastBatSpeed = 6f;
    public ButtonPointer pointer
    {
        get
        {
            if (_pointer == null)
            {
                _pointer = GetComponentInChildren<ButtonPointer>(true);
            }
            return _pointer;
        }
    }
    private ButtonPointer _pointer;
    //public spawnBall newBallSpawner;

    //private ballController ballCtrl;
    //private difficultyController difficultyCtrl;

    private static bool toggleOn;
    public float cooldown = 0.5f;
    public float minVelocityForHit = 1f;
    private bool cooling = false;
    public Renderer batSkin;
    public float startDelay = 0f;
    private bool controllerLoaded;
    private Quaternion previousVRRotation;
    [HideInInspector]
    public bool matchingMixedRealityBat;
    //needs to be in the order:

    //public UnityEvent onHit;

    //grip rot
    //grip pos
    //baseball bat w/collider

    public Rigidbody rb;

    public AudioSource hitSound;

    private int batLayer;
    private int generalLayer;

    /*
    private static void InitializeGripType()
    {
        if (allGripTypes == null)
        {
            allGripTypes = (gripType[])Enum.GetValues(typeof(gripType));

#if GEAR_VR
            currentGripType = 0;
#else
            currentGripType = (gripType)PlayerPrefs.GetInt("GRIPTYPE", 0);
#endif
        }
    }
    */

    public static gripType SelectNextGripType()
    {
        //Debug.Log(currentGripType + " old grip type " +allGripTypes[currentGripType]);
        /*
        InitializeGripType();

        int curGrip = (int)currentGripType;

        currentGripType++;
        if ((int)currentGripType >= (allGripTypes.Length))
        {
            currentGripType = 0;

        }
        if (currentGripType == gripType.toggle)
        {
            toggleOn = true;
        }
        //Debug.Log(currentGripType+" new grip type " + allGripTypes[currentGripType]);
        PlayerPrefs.SetInt("GRIPTYPE", (int)currentGripType);
        */

        currentGripType = (gripType)(((int)currentGripType + 1) % (Enum.GetValues(typeof(gripType)).Length));

        if (currentGripType == gripType.toggle)
        {
            toggleOn = true;
        }

        return currentGripType;
    }

    public static gripType GetCurrentGripType()
    {
        //InitializeGripType();

        return currentGripType;

    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //rigidbody = GetComponent<Rigidbody>();
        //hitSound = GetComponent<AudioSource>();
        batLayer = LayerMask.NameToLayer("Bat");
        generalLayer = LayerMask.NameToLayer("Default");

        //if (ghostBat)
        //{
        //    StartCoroutine(GhostBatCreationRoutine());
        //}
        //if (batCollider)
        //{
        //    batSkin = batCollider.GetComponentInChildren<Renderer>();
        // }

        //put this stuff back in when ready.
        /*
         * GameObject gameController = GameObject.Find("GameController");
        if (gameController)
        {
            statsCtrl = gameController.GetComponent<StatsController>();
        }
        */

#if RIFT
        previousVRRotation = OVRManager.instance.transform.rotation;
        if (gripTransform && oculusGripPosition)
        {
            gripTransform.localPosition = oculusGripPosition.localPosition;
            gripTransform.localRotation = oculusGripPosition.localRotation;
            gripTransform.localScale = oculusGripPosition.localScale;
            //gripTransform.localPosition = new Vector3(0, -.02f, 0);
            //gripTransform.localRotation = Quaternion.Euler(new Vector3(0, 90, -1f * gripTiltAdjust));//was -50
        }
        //transform.rotation = Quaternion.Euler(transform.right*gripTiltAdjust) * transform.rotation;

#elif STEAM_VR
        if (Player.instance)
        {
            previousVRRotation = Player.instance.transform.rotation;
        }
        //gripTransform.localPosition = new Vector3(0f, 0f, 0f);
        //gripTransform.localRotation = Quaternion.Euler(new Vector3(0, 90f, 0));
        thisPlayer = FindObjectOfType<Player>();

#endif

        toggleOn = false;

        DetermineAttachedHand();

        if (GameController.isPitchingMode)
        {
            holdable = false;
            gameObject.SetActive(false);
        }
    }
    /*
    IEnumerator GhostBatCreationRoutine()
    {
        int maxIteration = 0;
        int iteration = 0;
        while (iteration <= maxIteration)
        {
            GameObject ghostBatGameObject = (Instantiate(batCollider.gameObject, batCollider.transform.parent) as GameObject);
            ghostBatGameObject.GetComponent<Collider>().isTrigger = true;
            ghostBatGameObject.name = "Ghost Bat " + iteration;
            ghostBatGameObject.GetComponentInChildren<Renderer>().material.color = new Color(1, 1, 1, 1f / (iteration + 2f));

            ghostBatGameObject.transform.localPosition = batCollider.transform.localPosition;

            if (iteration == 0)
            {
                ghostBatContainer = ghostBatGameObject;
                ghostBatContainer.transform.localRotation = batCollider.transform.localRotation;
                ghostBatContainer.SetActive(false);
            }
            else
            {
                ghostBatGameObject.transform.SetParent(ghostBatContainer.transform);
                ghostBatGameObject.transform.localRotation = Quaternion.AngleAxis(iteration * 10, Vector3.up);
            }
            iteration++;
            yield return null;
        }
    }
    */
    void playBatSound()
    {
        /*Debug.Log("PLAYBAT SOUND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if (!hitSound) return;
        hitSound.clip = batHits[UnityEngine.Random.Range(0, batHits.Length)];
        Debug.Log("hit sound clip chosen.... "+hitSound.clip.name);
        hitSound.Play();*/
        /*BatController batControl = gameObject.GetComponent<BatController>();
        if(batControl != null)
        {
            if (batControl.isSwinging) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_hit", transform);
            else DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("bat_bunt", transform);
        }*/
    }

    // Update is called once per frame
    // private int knownNumberOfHands = 0;
    private int numberOfHands = 0;
    private float timerSinceHolding;
    private float minTimeSinceHolding = .2f;
    private bool invisibleBat;
    void Update()
    {
        /*
        if(Input.GetKeyDown("b"))
        {
            invisibleBat = !invisibleBat;
            batSkin.enabled = !invisibleBat;
        }
        */
        if (GetCurrentGripType() == gripType.always)
        {
            bool leftHandActive = HandManager.IsHandValid(HandManager.HandType.left);
            bool rightHandActive = HandManager.IsHandValid(HandManager.HandType.right);

            int temp = 0;
            if (leftHandActive)
            {
                temp++;
            }
            if (rightHandActive)
            {
                temp++;
            }
            if (temp != numberOfHands)
            {
                DetermineAttachedHand();
                numberOfHands = temp;
            }

            //Debug.Log("number of hands "+numberOfHands+" startdelay "+startDelay);
            if (startDelay > 0)
            {

                if (GetCurrentGripType() != gripType.always || (leftHandActive && rightHandActive))
                {
                    startDelay = 0f;
                }
                else
                {
                    startDelay -= Time.deltaTime;
                }
                DetermineAttachedHand();
                //Debug.Log("a return");
                return;
            }
            //DetermineAttachedHand();

        }
        else
        {
            if(!attached)
            {
                DetermineAttachedHand();
            }

            DetermineIfSomethingHeld();
        }

        matchingMixedRealityBat = false;
        // 
        //        if (attachedHand != null && (attachedHand.controller != null || GameController.isTrueOculus))
        if (GameController.fullBatterMode && HandManager.genericTracker && HandManager.genericTracker.IsValid())
        {
            MatchMixedRealityBat();
        }
        else
        {
            if (attachedCustomHand != null)
            {
                switch (GetCurrentGripType())
                {
                    case gripType.holding:
                        {
#if GEAR_VR
                            //Checks to see of there is a ball held in hand allready

                            held = attachedCustomHand.GetSqueezing();
#elif RIFT
                            held = attachedCustomHand.GetSqueezing();// || attachedCustomHand.GetGrip();
                            // Debug.Log("bat held by oculus? "+held);
#elif STEAM_VR
                            held = attachedCustomHand.GetSqueezing();
                            if (attachedHand.currentAttachedObject != null && attachedHand.currentAttachedObject.GetComponent<Interactable>())
                            {
                                //Debug.Log("holding " + attachedHand.currentAttachedObject.name);
                                timerSinceHolding = 0f;
                            }
                            else
                            {
                                if (timerSinceHolding < minTimeSinceHolding)
                                {
                                    timerSinceHolding += Time.deltaTime;
                                }
                            }
                            Debug.DrawRay(attachedHand.transform.position, Vector3.up, Color.blue);
                            //held = attachedHand && attachedHand.controller != null && attachedHand.controller.GetPress(EVRButtonId.k_EButton_Grip);
                            held = attachedCustomHand.GetSqueezing();
#endif

                            //Debug.Log("held "+held+" holdable "+holdable+" timesince "+timerSinceHolding);
                            if (held && holdable)
                            {
                                if (timerSinceHolding >= minTimeSinceHolding)
                                {
                                    onGrab();
                                }
                                //  onGrab();
                            }
                            else
                            {
                                //if (!holdable && ghostBat)
                                //{
                                //    ghostBatContainer.SetActive(false);
                                //}
                                onRelease();
                                DetermineAttachedHand();
                            }
                            break;
                        }
                    case gripType.toggle:
                        {
#if GEAR_VR
                            //held = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0;
                            held = attachedCustomHand.GetSqueezeDown();
#elif RIFT
                            held = attachedCustomHand.GetSqueezeDown();// || attachedCustomHand.GetGripDown();
#elif STEAM_VR
                            //held = attachedHand.controller.GetPressDown(EVRButtonId.k_EButton_Grip);
                            held = attachedCustomHand.GetSqueezeDown();
#endif

                            if (held && holdable)
                            {
                                if (timerSinceHolding >= minTimeSinceHolding)
                                {
                                    toggleOn = !toggleOn;
                                }
                            }
                            //if (held && holdable && toggleOn)
                            //{
                                //Debug.DrawRay(attachedCustomHand.transform.position, Vector3.right, Color.cyan);
                            //}
                            if (holdable && toggleOn)
                            {
                                onGrab();
                            }
                            else
                            {
                                onRelease();
                                DetermineAttachedHand();
                            }
                            break;
                        }
                    case gripType.always:
                        {
                            if (holdable && startDelay <= 0)
                            {
                                held = true;
                                onGrab();
                            }
                            else
                            {
                                held = false;
                                onRelease();
                                DetermineAttachedHand();
                            }
                            break;
                        }
                }
            }
            else
            {
                DetermineAttachedHand();

                if (attachedCustomHand == null && HandManager.genericTracker && HandManager.genericTracker.IsValid() && Time.timeSinceLevelLoad>3f)
                {
                    //no hands. only tracker
                    MatchMixedRealityBat();
                }
            }
        }
#if RIFT
        previousVRRotation = OVRManager.instance.transform.rotation;
#elif STEAM_VR
        if (Player.instance)
        {
            previousVRRotation = Player.instance.transform.rotation;
        }
#endif

    }

    //this should be called if a button changes the left and right hand.
    public void RefreshOnHandChange()
    {
#if STEAM_VR
        if (attachedHand)
        {
            attachedHand.HoverUnlock(null);
        }
#endif
        DetermineAttachedHand();
#if STEAM_VR
        if(attachedCustomHand && attachedCustomHand.GetSqueezing())
        //if (attachedHand && attachedHand.controller.GetPress(EVRButtonId.k_EButton_Grip))
        {
            if (holdable)
            {
                attached = false;
                onGrab();
            }
        }
        else
        {
            attached = true;
            onRelease();
        }
#endif
    }

    private bool useEitherHand = false;
    private void DetermineAttachedHand()
    {
        if (Time.time < 2f)
        {
            return;
        }
        attachedCustomHand = HandManager.GetAvailableBattingHand();

        if (useEitherHand)
        {
            if (attachedCustomHand)
            {
                if (attachedCustomHand.otherHand)
                {
                    if (attachedCustomHand.otherHand.GetSqueezing() && currentGripType != gripType.always)
                    {
                        attachedCustomHand = attachedCustomHand.otherHand;
                        if (!DetermineIfSomethingHeld())
                        {
                            timerSinceHolding = minTimeSinceHolding;
                        }
                        Debug.DrawRay(attachedCustomHand.transform.position, Vector3.up, Color.green);
                    }
                }
            }
        }
        else
        {
            if (attachedCustomHand)
            {
                //attachedCustomHand.gripWithWholeHand = false;
                if (attachedCustomHand.otherHand)
                {
                    attachedCustomHand.otherHand.gripWithWholeHand = true;
                }
            }
        }
#if STEAM_VR
        if (attachedCustomHand)
        {
            attachedHand = attachedCustomHand.actualHand;
        }
        else
        {
            attachedHand = null;
        }
#elif RIFT
        //Debug.Log("find the grabber.");
        if (attachedCustomHand)
        {
            attachedGrabber = attachedCustomHand.GetComponentInChildren<OVRGrabber>();
        }
        //Debug.Log("attached grabber found " + attachedGrabber);
#endif


    }

    private bool DetermineIfSomethingHeld()
    {
        if (attachedCustomHand)
        {
#if RIFT
            OVRGrabbable somethingHeld = null;
            if (attachedGrabber)
            {
                somethingHeld = attachedGrabber.grabbedObject;
            }
#elif GEAR_VR
                GameObject somethingHeld = null;
#elif STEAM_VR
            GameObject somethingHeld = attachedCustomHand.actualHand.currentAttachedObject;
            if (somethingHeld && somethingHeld.GetComponent<Interactable>() == null)
            {
                somethingHeld = null;
            }
#endif

#if RIFT || GEAR_VR || STEAM_VR
            if (somethingHeld != null)
            {
                //Debug.Log("something held "+somethingHeld.name);
                timerSinceHolding = 0f;
                return true;
                //Debug.DrawRay(attachedCustomHand.transform.position, Vector3.right, Color.magenta);
            }
            else
            {
                if (timerSinceHolding < minTimeSinceHolding)
                {
                    timerSinceHolding += Time.deltaTime;
                }
            }
#endif
        }

        return false;
    }

    public void MatchMixedRealityBat()
    {
        matchingMixedRealityBat = true;
        if(GameController.batType == GameController.FullBatType.None)
        {
            return;
        }

        if(GameController.isAtBat == false)
        {
            attached = false;
            transform.position = -Vector3.up * 10f;
            transform.rotation = HandManager.genericTracker.GetCurrentBatGripRotation();
            return;
        }

        if (!attached)
        {
            transform.position = HandManager.genericTracker.GetCurrentBatGripPosition();
            transform.rotation = HandManager.genericTracker.GetCurrentBatGripRotation();
            

            //bool alreadyOverlappyBall = false;

            if (batCollider)
            {
                batCollider.isTrigger = true;
                batCollider.enabled = true;
            }
            if (useFastBatColliders)
            {
                if (fastBatContainer)
                {
                    previousFastBatPosition = fastBatContainer.position;
                }
                if (fastBatColliders.Length > 0)
                {
                    foreach (Collider c in fastBatColliders)
                    {
                        c.isTrigger = true;
                    }
                }
            }
            if (batSkin)
            {
                batSkin.enabled = !invisibleBat;
            }
            //if (ghostBatContainer)
           // {
            //    ghostBatContainer.SetActive(false);
           // }

            attached = true;
            rb.isKinematic = true;
        }
        else
        {
            if (batCollider && batCollider.isTrigger)
            {
                batCollider.isTrigger = false;
            }

            if (useFastBatColliders && fastBatContainer)
            {
                Vector3 swingDelta = fastBatContainer.position - previousFastBatPosition;
                bool isFast = swingDelta.magnitude / Time.deltaTime > fastBatSpeed;
                fastBatContainer.gameObject.SetActive(isFast);
                fastBatContainer.Rotate(Vector3.right,GameController.AngleAroundAxis(fastBatContainer.forward, swingDelta, fastBatContainer.right),Space.Self);

                if (fastBatColliders.Length > 0)
                {
                    foreach (Collider c in fastBatColliders)
                    {
                        c.isTrigger = !isFast;
                    }
                }
                previousFastBatPosition = fastBatContainer.position;
            }
#if STEAM_VR
            if (Player.instance.transform.rotation == previousVRRotation)
#else
            if(true)
#endif
            {
                rb.MovePosition(HandManager.genericTracker.GetCurrentBatGripPosition());
                rb.MoveRotation(HandManager.genericTracker.GetCurrentBatGripRotation());
            }
            else
            {
                transform.position = HandManager.genericTracker.GetCurrentBatGripPosition();
                transform.rotation = HandManager.genericTracker.GetCurrentBatGripRotation();
            }
        }
        HandManager.genericTracker.OnBatMatching();
    }

    public void onGrab()
    {
        if(GameController.isAtBat == false)
        {
            onRelease();
            return;
        }
        if (!attached)
        {
            if (!attachedCustomHand)
            {
                return;
            }


            transform.position = attachedCustomHand.GetCurrentBatGripPosition();
            transform.rotation = attachedCustomHand.transform.rotation;

            if (useFastBatColliders && fastBatContainer)
            {
                previousFastBatPosition = fastBatContainer.position;
            }
            //bool alreadyOverlappyBall = false;

            if (batCollider)
            {
                batCollider.isTrigger = true;
                batCollider.enabled = true;
            }
            if (batSkin)
            {
                batSkin.enabled = !invisibleBat;
            }
            //if (ghostBatContainer)
            //{
            //    ghostBatContainer.SetActive(false);
           // }

            attached = true;
            rb.isKinematic = true;


#if RIFT || STEAM_VR
            attachedCustomHand.isOccupied = true;
            //pointer.enabled = true;
            attachedCustomHand.MakeHandInvisible();
#if STEAM_VR
            attachedHand.HoverLock(null);
            //if (GameController.isOculusInSteam)
            //{
            //pointer.enabled = true;
            //}
#endif
#endif

        }
        else
        {

            if (batCollider && batCollider.isTrigger)
            {
#if GEAR_VR
                if (ballController.instance.currentServiceType == ballController.ballServiceType.tee)
                {
                    Collider[] cols = Physics.OverlapSphere(ballController.instance.teeController.transform.position, .1f, gameObject.layer);
                    if (cols == null || cols.Length == 0)
                    {
                        batCollider.isTrigger = false;
                    }
                }
                else
                {
                    batCollider.isTrigger = false;
                }
#else
                batCollider.isTrigger = false;
#endif
            }

#if RIFT
            if (OVRManager.instance.transform.rotation == previousVRRotation)
            {
                rb.MovePosition(attachedCustomHand.GetCurrentBatGripPosition());
                rb.MoveRotation(Quaternion.AngleAxis(gripTilt,attachedCustomHand.transform.right) * attachedCustomHand.transform.rotation);
            }
            else
            {
                transform.position = attachedCustomHand.GetCurrentBatGripPosition();
                transform.rotation = Quaternion.AngleAxis(gripTilt, attachedCustomHand.transform.right) * attachedCustomHand.transform.rotation;
            }
#elif GEAR_VR
            rb.MovePosition(attachedCustomHand.transform.position);
            rb.MoveRotation(attachedCustomHand.transform.rotation);
#elif STEAM_VR

            /*if (controllerLoaded || (attachedCustomHand.modelSpawner && attachedCustomHand.modelSpawner.transform.childCount > 0))
            {

                controllerLoaded = true;
            }*/

            if  (Player.instance.transform.rotation == previousVRRotation)
            {

                rb.MovePosition(attachedHand.transform.position);
               rb.MoveRotation(attachedHand.transform.rotation);
            }
            else
            {

                transform.position = attachedHand.transform.position;
                transform.rotation = attachedHand.transform.rotation;
            }

            if (useFastBatColliders && fastBatContainer)
            {
                Vector3 swingDelta = fastBatContainer.position - previousFastBatPosition;
                bool isFast = swingDelta.magnitude / Time.deltaTime > fastBatSpeed;
                fastBatContainer.gameObject.SetActive(isFast);
                fastBatContainer.Rotate(Vector3.right, GameController.AngleAroundAxis(fastBatContainer.forward, swingDelta, fastBatContainer.right), Space.Self);

                if (fastBatColliders.Length > 0)
                {
                    foreach (Collider c in fastBatColliders)
                    {
                        c.isTrigger = !isFast;
                    }
                }
                previousFastBatPosition = fastBatContainer.position;
            }

#endif
            attachedCustomHand.showRenderModel = false;

        }

    }

    public void onRelease()
    {
        if (GameController.isMenuPointing || matchingMixedRealityBat)
        {
            return;
        }
        //
        if (attached)
        {
            transform.position = Vector3.up * -50f;
            //print("ON RELEASE");
            //Debug.Log("detach.");
            

            if (batSkin)
            {
                batSkin.enabled = false;
            }

            attached = false;
            //rb.isKinematic = false;
            //rb.velocity = Vector3.zero;
            //rb.angularVelocity = Vector3.zero;

            attachedCustomHand.isOccupied = false;
            attachedCustomHand.RefreshHandState();

#if RIFT
            //OVRInput.SetControllerVibration(0f, 0f, HandManager.OculusHand(attachedCustomHand));
            //pointer.enabled = false;
#elif STEAM_VR
            if (attachedHand)
            {
                attachedHand.HoverUnlock(null);
                //if(GameController.isOculusInSteam)
                //{
                //    pointer.enabled = false;
                //}
                //attachedCustomHand.showRenderModel = true;
            }
#endif
            //gameObject.SetActive(false);
        }
        else
        {
            if (batCollider)
            {
                batCollider.isTrigger = true;
                batCollider.enabled = false;
            }

            if (ghostBat)
            {
                //if (ghostBatContainer)
                //{
                //    ghostBatContainer.SetActive(true);
                //}
                transform.position = attachedCustomHand.transform.position;
                transform.rotation = attachedCustomHand.transform.rotation;

                if (pivotAroundTee)
                {
                    //transform.Rotate(Vector3.up, -30f);
                }
            }
        }
        

    }

    

    public void ForceBatHit(hittable ball)
    {
        ball._onBatHit(ball.transform.position);
//        Debug.Log("Hit a ball");
        //onBallHit.Invoke(ball);
        startDelay = 0;
        playBatSound();
        VibrateBat();
        //if (ScoreboardController.instance)
        //{
        //    ScoreboardController.instance.IncrementBallHitCount();
        //}
    }

    // float highestImpulse;
    //float highestFriction;
    //float highestOther;
    void OnCollisionEnter(Collision collision)
    {
        hittable hitEventController = collision.gameObject.GetComponent<hittable>();
        if (hitEventController)
        {
            // Debug.Log("enter bat");
            if (hitEventController.OnBatEnter())
            {
                OnDeterminedHit(hitEventController);
            }
        }
    }

    bool reloading = false;
    // Text debugText;
    void OnCollisionStay(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > minVelocityForHit && collision.rigidbody != null && collision.rigidbody.gameObject.tag == "ball")
        {

            //if(collision.frictionForceSum)
            hittable hitEventController = collision.rigidbody.gameObject.GetComponent<hittable>();
            if (!hitEventController.alreadyBeenHit)
            {
                //Debug.Log("hit??????? vel mag: "+ collision.rigidbody.velocity.magnitude +"stay? "+ hitEventController.OnBatStay());

                // if (/*collision.contacts[0].separation > .1f ||*/ collision.impulse.sqrMagnitude>0 || hitEventController.IsGettingHit())
                if ((collision.contacts[0].separation < .05f || collision.rigidbody.velocity.magnitude > 1f) && hitEventController.OnBatStay())
                {
                    OnDeterminedHit(hitEventController);
                }
            }
        }

    }

    void OnCollisionExit(Collision collision)
    {
        hittable hitEventController = collision.gameObject.GetComponent<hittable>();
        if (hitEventController)
        {
            if (!hitEventController.alreadyBeenHit && hitEventController.OnBatExit())
            {
                OnDeterminedHit(hitEventController);
            }
        }
    }

    private void OnDeterminedHit(hittable ball)
    {
        /*
        ball.velocityBeforeBat = rb.velocity;
        startDelay = 0;


        BatController batControl  = GetComponent<BatController>();
        batControl.HitRigidbodyNextFixedUpdate(ball.GetRigidbody(), col);
        */
        
        playBatSound();

        BatController.onBatHit.Invoke(ball);
        ball._onBatHit(ball.transform.position);
        startDelay = 0;

        VibrateBat();
        ball.velocityBeforeBat = rb.velocity;
    }

    public void VibrateBat()
    {
        //onBallHit.Invoke(ball);
        HandManager.VibrateController(0, .1f, 130, 1, attachedCustomHand);
    }
}