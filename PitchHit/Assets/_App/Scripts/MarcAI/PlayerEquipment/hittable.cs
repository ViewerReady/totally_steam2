﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public class hittable : MonoBehaviour {

    private trailManager trails;
    private TrailRenderer trailRend;
    public bool useTrails = false;

    public Action<hittable> OnBatHit;
    public Action<hittable> OnCaughtByDefender;
    //public Action<hittable> OnCaughtByCatcher;
    public Action<hittable> OnBallDestroy;
    public Action<hittable> OnHitGround;
    public Action<hittable> OnHandGrabbed;

    public GameObject[] groundSoftHitParticles;
    public GameObject[] groundHardHitParticles;
    public GameObject visualAssist;
    public float minParticleScale = .1f;
    public float maxParticleScale = 1f;
    public float particleScaleCoefficient = .007f;

    public float distance = 0f;
    public Vector3 positionWhenHit;

    DarkTonic.MasterAudio.PlaySoundResult currentSound;

    [HideInInspector]
    public Vector3 spotThrownFrom;


    //FLAGS
    public bool isHeld
    {
        get; protected set;
    }
    public bool isActive;
    private bool goingUp;
    bool onBat;
    bool wasHitByBat;
    public bool alreadyBeenHit;
    public bool hitGround = false;
    public bool hasEverHitGround
    {
        set
        {
            if (value && !hasEverHitGround)
            {
                if (OnHitGround != null)
                {
                    OnHitGround.Invoke(this);
                }
            }
            _hasEverHitGround = value;
        }
        get
        {
            return _hasEverHitGround;
        }
    }
    private bool _hasEverHitGround = false;
    public bool hasTouchedDefender;
    public bool hasBeenCaught;
    public bool isStoppedOnGround
    {
        get
        {
            return hitGround && !isHeld && rb.velocity.magnitude < .1f;
        }
    }
    public bool isScored = false;


    public Vector3 initialVelocity;


    [HideInInspector]
    private Rigidbody rb;


    private float colliderRadius;

    private BallLifetime lifeTime;
    private Vector3 previousPosition;
    private Transform spawnTransform;
    private float distanceToBecomeActive = 1f;
    private RaycastHit hit;
    private LayerMask batLayer;
    //public static ballController theBallController;
    private ballController.ballServiceType serviceOrigin;
    //public float velocityAngleChangeThreshold = 10f;
    public float velocityDeltaThreshold = 2f;
    public float minSpeedThreshold = 4f;
    [HideInInspector]
    public Vector3 velocityBeforeBat;
    Vector3 previouslVelocityDuringBat;



    private float timeSinceLastBatHit;

    const float MINTIMEBETWEENBATHITS = .15f;


    public float GetRadius()
    {
        return colliderRadius;
    }

    public bool GetWasHitByBat()
    {
        return wasHitByBat;
    }

    private void SetWasHitByBat(bool hit)
    {
        wasHitByBat = hit;
    }

    public void SetHeldByPlayer(bool isHeld)
    {
        this.isHeld = isHeld;
        hasBeenCaught = hasBeenCaught || isHeld;
        hasTouchedDefender = hasTouchedDefender || isHeld;
        if(rb)
        {
            if(isHeld)
            {
                rb.interpolation = RigidbodyInterpolation.None;
            }
            else
            {
                rb.interpolation = RigidbodyInterpolation.Interpolate;
            }
        }
    }

    public void OnGrabbed()
    {
        if (OnHandGrabbed != null) OnHandGrabbed.Invoke(this);
        SetHeldByPlayer(true);
    }

    public void OnUngrabbed()
    {
        SetHeldByPlayer(false);
    }
    
    public void SetOrigin(ballController.ballServiceType origin, Transform spawner)
    {
        serviceOrigin = origin;
        spawnTransform = spawner;

        if(origin == ballController.ballServiceType.pitcher)
        {
            //rb.drag = .3f;
        }
        //if(origin == ballController.ballServiceType.self)
        //{
            //transform.SetParent(spawner);
        //}
    }


    public ballController.ballServiceType GetOrigin()
    {
        return serviceOrigin;
    }


	// Use this for initialization
	void Awake () {
        timeSinceLastBatHit = MINTIMEBETWEENBATHITS;

        trails = GetComponent<trailManager> ();
        trailRend = GetComponent<TrailRenderer>();
        lifeTime = GetComponent<BallLifetime>();
        rb = GetComponent<Rigidbody>();
        SphereCollider sphere = GetComponentInChildren<SphereCollider>();
        if (sphere)
        {
            float biggestScaleAxis = sphere.transform.lossyScale.x;
            if(sphere.transform.lossyScale.y > biggestScaleAxis)
            {
                biggestScaleAxis = sphere.transform.lossyScale.y;
            }
            if (sphere.transform.lossyScale.z > biggestScaleAxis)
            {
                biggestScaleAxis = sphere.transform.lossyScale.z;
            }

            colliderRadius = sphere.radius * biggestScaleAxis;
        }
        else
        {
            colliderRadius = .05f;
        }

        batLayer = 1 << LayerMask.NameToLayer("Bat");
        previousPosition = transform.position;

    }

	void OnEnable(){
		//distance = 0f;
		hitGround = false;
        hasTouchedDefender = false;
    }

    void OnDestroy()
    {
        if (OnBallDestroy != null) OnBallDestroy.Invoke(this);
    }

    void FixedUpdate()
    {
        //if(!wasHitByBat && serviceOrigin== ballController.ballServiceType.pitcher)
        //{
        //    rb.AddForce(Physics.gravity * -.15f);

        //}

        if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
        {
            timeSinceLastBatHit += Time.deltaTime;
            //GetComponentInChildren<Renderer>().material.color = Color.Lerp(Color.red,Color.green,timeSinceLastBatHit/MINTIMEBETWEENBATHITS);
        }
        if (!isActive)
        {
            switch(serviceOrigin)
            {
                case ballController.ballServiceType.pitcher:
                    BecomeActive();
                    break;
                case ballController.ballServiceType.tee:
               
                   // print("spawnTransform " + spawnTransform.position);
                   if (spawnTransform == null)
                    {
                        BecomeActive();
                        break;
                    }
                    if ((transform.position-spawnTransform.position).sqrMagnitude > .75f)
                    {
                       // Debug.DrawRay(transform.position, (transform.position - spawnPosition), Color.blue,10f);
                        //Debug.Log((transform.position - spawnPosition).sqrMagnitude);
                        BecomeActive();
                    }
                    break;
                case ballController.ballServiceType.self:
                    if ((transform.position - spawnTransform.position).sqrMagnitude > 2f)
                    {
                        BecomeActive();
                    }
                    break;
                    
            }
        }
        else
        {
            if(rb.velocity.y>.1f)
            {
                goingUp = true;
            }
            else
            {
                if (rb.velocity.y < -.1f)
                {
                    if (goingUp)
                    {
                        alreadyBeenHit = false;
                    }
                    goingUp = false;
                }
            }
        }
        if (!onBat)
        {
            velocityBeforeBat = rb.velocity;
            previouslVelocityDuringBat = velocityBeforeBat;
            //timeOfBat = Time.timeSinceLevelLoad;
        }

        if (currentSound == null && (transform.position - previousPosition).magnitude >= 0.3f)
        {
            //Debug.Log("STARTING TO PLAY WHOOSH SOUND");
            currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound3DFollowTransform("ball_whooshing", transform);
        }
        else if (currentSound != null && (transform.position - previousPosition).magnitude < 0.3f)
        {
            //Debug.Log("STOPPING WHOOSH SOUND");
            DarkTonic.MasterAudio.MasterAudio.StopAllOfSound("ball_whooshing");
            currentSound = null;
        }
        //Debug.Log("BALL MOVEMENT DELTA: " + (transform.position - previousPosition).magnitude);
        previousPosition = transform.position;
    }

    public void ResetFlags()
    {
        hasTouchedDefender = false;
        SetWasHitByBat(false);
        hasEverHitGround = false;
        hitGround = false;
        //OnCaughtByDefender = null;
        hasBeenCaught = false;
        //OnBallDestroy = null;

    }

    public void BecomeActive()
    {
        /*
         switch (serviceOrigin)
         {
             case ballController.ballServiceType.pitcher: 
                 GetComponentInChildren<Renderer>().material.color = Color.red;
                 break;
             case ballController.ballServiceType.tee:
                 GetComponentInChildren<Renderer>().material.color = Color.blue;
                 break;
             case ballController.ballServiceType.self:
                 GetComponentInChildren<Renderer>().material.color = Color.green;
                 break;

         }
         */
        // Debug.Log("from "+serviceOrigin+" to active");
        if (!isActive)
        {
            //if(serviceOrigin == ballController.ballServiceType.self && transform.parent == spawnTransform)
            //{
            //    transform.SetParent(null);
            //}

			if (lifeTime) {
				lifeTime.StartKillCount ();
			}
			//ballController.AddToActiveBalls(this);
            isActive = true;
        }
    }

    public void _onBatHit(Vector3 contactPoint){
        //Debug.Log("hittable._onBatHit()");
        positionWhenHit = contactPoint;// GetRigidbody().position;


        
        {
            hasTouchedDefender = false;
            wasHitByBat = true;
            hitGround = false;
            //Debug.Log("_____________________________________________invoke ON BAT HIT if it exists_______________");
            if (OnBatHit != null)
            {
                foreach(Delegate d in OnBatHit.GetInvocationList())
                {
                    //Debug.Log("OnBatHit.... "+d.ToString());
                }
                OnBatHit.Invoke(this);
            }
            else
            {
                //Debug.Log("ON BAT HIT IS NULL");
            }
        }

        if (useTrails)
        {
            if (trails != null)
            {
                if (serviceOrigin == ballController.ballServiceType.pitcher)
                {
                    trails.ClearAll();
                }
                trails.trackVelocity = true;
            }
            else
            {
                /*if (trailRend)
                {
                    if (serviceOrigin == ballController.ballServiceType.pitcher)
                    {
                        trailRend.Clear();
                    }
                    trailRend.enabled = true;
                }*/
            }
        }
        //isActive = true;
        BecomeActive();
        
        
        alreadyBeenHit = true;
        //StartCoroutine(FindInitialVelocity());

    }

    IEnumerator FindInitialVelocity()
    {
        yield return new WaitForSeconds(.1f);
        alreadyBeenHit = false;
        
        //while (initialVelocity == Vector3.zero)
        //{
        initialVelocity = rb.velocity;
         //   yield return null;
        //}
    }


    void OnCollisionEnter(Collision collisionInfo){
        if (collisionInfo.collider.tag == "Defense")
        {
            //rb.velocity /= 2f;//the ball should lose a lot of energy when it hits the player.
            hasTouchedDefender = true;
            //Greg: It should probably also announce that its trajectory has changed.
        }

		if (!hitGround && collisionInfo.gameObject.layer == LayerMask.NameToLayer("Ground")) {
			//Vector3 diff = transform.position - positionWhenHit;
			//distance = diff.magnitude;
			hitGround = true;
            hasEverHitGround = true;
            Debug.DrawRay(collisionInfo.contacts[0].point, Vector3.up, Color.green, 5f);

            ContactPoint firstContact = collisionInfo.contacts[0];
            int terrainTextureIndex = TerrainSampler.GetMainTexture(firstContact.point);

            float minimumHardImpactVelocity = 0f;
            if (rb.velocity.magnitude > minimumHardImpactVelocity)
            {
                if (groundHardHitParticles.Length > 0)
                {
                    terrainTextureIndex = Mathf.Clamp(terrainTextureIndex, 0, groundHardHitParticles.Length - 1);
                    GameObject temp = Instantiate(groundHardHitParticles[terrainTextureIndex], firstContact.point, Quaternion.identity);
                    if (terrainTextureIndex == 0) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("ball_impact_dirt", transform.position);
                    else if (terrainTextureIndex == 1) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("ball_impact_grass", transform.position);
                    Destroy(temp, 3f);
                }
            }
            else
            {
                if (groundSoftHitParticles.Length > 0)
                {
                    terrainTextureIndex = Mathf.Clamp(terrainTextureIndex, 0, groundSoftHitParticles.Length - 1);
                    GameObject temp = Instantiate(groundSoftHitParticles[terrainTextureIndex], firstContact.point, Quaternion.identity);
                    Destroy(temp, 3f);
                }
            }
            /*
            if(groundHitParticle)
            {
                
                GameObject temp = Instantiate(groundHitParticle, firstContact.point, Quaternion.LookRotation(-Vector3.up)) as GameObject;
                ParticleScaler scaler = groundHitParticle.GetComponent<ParticleScaler>();
                temp.SetActive(true);

                if (scaler)
                {
                    float strength = Mathf.Cos(Vector3.Angle(rb.velocity, firstContact.normal) * Mathf.Deg2Rad *2f) *.5f +.5f;
                    Debug.Log(collisionInfo.gameObject.name+"ang: "+ Vector3.Angle(rb.velocity, firstContact.normal) + "cos - "+ strength+ "----- "+ (collisionInfo.relativeVelocity.magnitude * strength));
                    strength = Mathf.Min(minParticleScale +(collisionInfo.relativeVelocity.magnitude * strength * particleScaleCoefficient),maxParticleScale);
                    //strength = .001f;
                    
                    scaler.MultiplyParticleScale(strength);
                }
                float angleToNormal = Vector3.Angle(Vector3.up, firstContact.normal);
                if (angleToNormal > 0.001)
                {
                    Vector3 axis = Vector3.Cross(Vector3.up, firstContact.normal);
                    //temp.transform.Rotate(axis, angleToNormal);
                }
                Destroy(temp, 5f);
            }
            */
		}
        else
        {
           // if(collisionInfo.collider.gameObject.layer != LayerMask.NameToLayer("Bat"))
            //{
            //    velocityBeforeBat = rb.velocity;
            //}
        }
	}

    void OnCollisionStay(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "ground")
        {
            //distance = diff.magnitude;
            hitGround = true;
            hasEverHitGround = true;

        }
    }

    void OnCollisionExit(Collision c)
    {
        //if (hitGround && c.gameObject.tag == "ground")
        //{
       //     hitGround = false;
        //}
    }

    public bool OnBatEnter()
    {
        
        // if (!ballRend)
        // {
        //     ballRend = GetComponentInChildren<Renderer>();
        // }

        //check for difference in horizontal movement (makes sense because there's no drag.)
        if (Mathf.Abs(rb.velocity.z - velocityBeforeBat.z) > .05f || Mathf.Abs(rb.velocity.x - velocityBeforeBat.x) > .05f)
        {
            //Debug.Log("z "+ Mathf.Abs(rb.velocity.z - previouslVelocityDuringBat.z)+"   x"+ Mathf.Abs(rb.velocity.x - previouslVelocityDuringBat.x));
            //GetComponentInChildren<Renderer>().material.color = Color.red;
            if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
            {
                return false;
            }
            timeSinceLastBatHit = 0;
            return true;
        }

        if (((velocityBeforeBat - rb.velocity).sqrMagnitude > 200f) && rb.velocity.sqrMagnitude > 200f)
        {
            //Debug.Log("delta "+ (velocityBeforeBat - rb.velocity).sqrMagnitude + " mag "+ rb.velocity.sqrMagnitude);
            //ballRend.material.color = Color.cyan;
            alreadyBeenHit = true;
            //GetComponentInChildren<Renderer>().material.color = Color.red;
            if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
            {
                return false;
            }
            timeSinceLastBatHit = 0;
            return true;
           
        }
        alreadyBeenHit = false;
        onBat = true;
        velocityBeforeBat = rb.velocity;
        previouslVelocityDuringBat = velocityBeforeBat;
        //timeOfBat = Time.timeSinceLevelLoad;
        return false;// JudgeAffectOfBat();

    }

    public bool OnBatStay()//check if the bat has altered the trajectory
    {
        if (hitGround)
        {
           // return false;
        }
        //GetComponentInChildren<Renderer>().material.color = Color.yellow;

        return JudgeAffectOfBat();

    }

    public bool OnBatExit()
    {
        //Debug.Log("on bat exit");
        onBat = false;

        if (hitGround)
        {
           // return false;
        }

        bool temp = false;

       
        if (Mathf.Abs(rb.velocity.z - velocityBeforeBat.z) > .05f || Mathf.Abs(rb.velocity.x - velocityBeforeBat.x) > .05f)
        {
            //Debug.Log("z "+ Mathf.Abs(rb.velocity.z - previouslVelocityDuringBat.z)+"   x"+ Mathf.Abs(rb.velocity.x - previouslVelocityDuringBat.x));
            
            temp = true;
        }
        /*
        if (!temp)
        {
            Vector3 acceleratedVelocityBeforeBat = velocityBeforeBat + (Physics.gravity * (Time.timeSinceLevelLoad - timeOfBat));
            if (((acceleratedVelocityBeforeBat - rb.velocity).sqrMagnitude > (velocityDeltaThreshold * 2f)) && rb.velocity.sqrMagnitude > (minSpeedThreshold * 1.5f))
            {
                // Debug.Log("YES should: " + acceleratedVelocityBeforeBat + " is: " + rb.velocity + " after: " + (Time.timeSinceLevelLoad - timeOfBat));

                //velocityBeforeBat = rb.velocity;
                //ballRend.material.color = Color.blue;
                temp = true;
            }
        }*/
        if (!temp)
        {
            //temp = JudgeAffectOfBat();
        }
       // if(temp)
       // {
            //GetComponentInChildren<Renderer>().material.color = Color.green;

       // }

        alreadyBeenHit = false;
        if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
        {
            return false;
        }
        if (temp)
        {
            timeSinceLastBatHit = 0;
        }
        return temp;
    }
    //private float timeOfBat;
   // private Renderer ballRend;
    private bool JudgeAffectOfBat()
    {
        //if(!ballRend)
       // {
       //     ballRend = GetComponentInChildren<Renderer>();
       //}
        //Debug.Log("judgey "+alreadyBeenHit);
        if(/*rb.velocity.y>.01f && */(rb.velocity.y - previouslVelocityDuringBat.y )>0.05f)
        {
            //   ballRend.material.color = Color.red;
            //ballRend.material.color = Color.green;
            //Debug.Log("juggle "+Time.time);
            //GetComponentInChildren<Renderer>().material.color = Color.blue;
            hasEverHitGround = false;
            if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
            {
                return false;
            }
            timeSinceLastBatHit = 0;
            return true; 
        }

        if(Mathf.Abs(rb.velocity.z-previouslVelocityDuringBat.z)>.01f || Mathf.Abs(rb.velocity.x - previouslVelocityDuringBat.x)>.01f)
        {
            //Debug.Log("z "+ Mathf.Abs(rb.velocity.z - previouslVelocityDuringBat.z)+"   x"+ Mathf.Abs(rb.velocity.x - previouslVelocityDuringBat.x));
            //  ballRend.material.color = Color.blue;
            //GetComponentInChildren<Renderer>().material.color = Color.blue;
            if (timeSinceLastBatHit < MINTIMEBETWEENBATHITS)
            {

                return false;
            }
            timeSinceLastBatHit = 0;
            return true;
        }
        else
        {
            previouslVelocityDuringBat = rb.velocity;
        //    ballRend.material.color = Color.yellow;
            return false;

        }

        /*
        //velocityBeforeBat += Physics.gravity * Time.deltaTime;
        Vector3 acceleratedVelocityBeforeBat = velocityBeforeBat + (Physics.gravity * (Time.timeSinceLevelLoad - timeOfBat));
        if (((acceleratedVelocityBeforeBat - rb.velocity).sqrMagnitude > (velocityDeltaThreshold * 2f)) && rb.velocity.sqrMagnitude > (minSpeedThreshold * 1.5f))
        {
           // Debug.Log("YES should: " + acceleratedVelocityBeforeBat + " is: " + rb.velocity + " after: " + (Time.timeSinceLevelLoad - timeOfBat));

            velocityBeforeBat = rb.velocity;
            //ballRend.material.color = Color.blue;
            return true;
        }
        else
        {
            //Debug.Log("NO should: " + acceleratedVelocityBeforeBat + " is: " + rb.velocity + " after: " + (Time.timeSinceLevelLoad - timeOfBat));

            //ballRend.material.color = Color.red;
            previouslVelocityDuringBat = rb.velocity;
            return false;
        }
        */
    }

    public Rigidbody GetRigidbody()
    {
        //Debug.Log("get rigidbody");
        return rb;
    }

    public Vector3 GetVelocity()
    {
        return rb.velocity;
    }

    public void SetVelocity(Vector3 velocity)
    {
        Debug.Log("set hittable velocity " + velocity);
        rb.velocity = velocity;
    }

    public TrajectoryInfo GetTrajectory()
    {
        return new TrajectoryInfo(rb);
    }


    public void SetMass(float m)
    {
        rb.mass = m;
        if (GetOrigin() == ballController.ballServiceType.pitcher)
        {
            rb.drag = .0f;
        }
    }

}
