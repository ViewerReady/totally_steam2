﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FieldControlButton : MonoBehaviour {

    public SpriteRenderer icon;

	public Color activeColor = Color.green;
    public Color litColor;
    protected Color unlitColor;

    protected bool _highlit;
    protected Material buttonRenderer;
	protected bool isActive;

    // Use this for initialization
    protected void Start()
    {
        if (buttonRenderer == null)
        {
            buttonRenderer = GetComponent<MeshRenderer>().material;
        }
        unlitColor = GetDesaturatedColor(litColor);
    }

    protected Color GetDesaturatedColor(Color original)
    {
        float h; float s; float v;
        Color.RGBToHSV(original, out h, out s, out v);
        return Color.HSVToRGB(h, s / 2f, v);
    }

    protected void SetColor(Color c)
    {
        if(buttonRenderer==null)
        {
            buttonRenderer = GetComponent<MeshRenderer>().material;
        }

      buttonRenderer.color = c;
    }

    protected abstract void OnButtonPress();
    

    
}
