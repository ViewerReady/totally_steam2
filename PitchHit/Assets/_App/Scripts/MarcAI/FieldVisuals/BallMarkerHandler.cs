﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMarkerHandler : MonoBehaviour
{

    public BallLandingSpotMarker groundedMarker;

    void Awake()
    {
        HideMarker();
        FieldMonitor.HitBallJudgement += DropMarkerForBall;
        //FullGameDirector.OnFieldEvent += DropMarkerForBall;
    }

    void OnDestroy()
    {
        FieldMonitor.HitBallJudgement -= DropMarkerForBall;
        //FullGameDirector.OnFieldEvent -= DropMarkerForBall;
    }

    // Use this for initialization
    void DropMarkerForBall(hittable ball, UmpireCall play, bool conclusive)
    {
        if (ball == null || (play != UmpireCall.FAIR)) return;
        TrajectoryInfo trajectory = ball.GetTrajectory();
        groundedMarker.ShowTrajectory(ball, trajectory);
        Vector3 groundPos = trajectory.groundHitPosition;
        groundedMarker.transform.localPosition = groundPos;
        AddListeners(ball);
    }

    void BallCaught(hittable ball)
    {
        RemoveListeners(ball);
    }

    void BallHitGround(hittable ball)
    {

    }

    void AddListeners(hittable ball)
    {
        ball.OnCaughtByDefender += BallCaught;
        //ball.OnHitGround += BallHitGround;
        ball.OnBallDestroy += RemoveListeners;
    }

    void RemoveListeners(hittable ball)
    {
        HideMarker();

        ball.OnCaughtByDefender -= BallCaught;
        //ball.OnHitGround -= BallHitGround;
        ball.OnBallDestroy -= RemoveListeners;
    }

    // Update is called once per frame
    void HideMarker()
    {
        groundedMarker.ShowHide(false);
    }


}
