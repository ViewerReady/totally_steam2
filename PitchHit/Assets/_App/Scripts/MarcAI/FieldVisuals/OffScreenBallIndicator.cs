﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Greyman;

[RequireComponent(typeof(OffScreenIndicator))]
public class OffScreenBallIndicator : MonoBehaviour {


    private OffScreenIndicator indicator;
    private ArrowIndicator curArrow;



    void Awake()
    {
        indicator = GetComponent<OffScreenIndicator>();
    }

    void OnEnable()
    {

    }

    void OnDisable()
    {
        if (curArrow != null)
        {
            indicator.RemoveIndicator(curArrow);
        }
        curArrow = null;
    }

    void Update () {

            if (FieldMonitor.Instance)
            {
                hittable ball = FieldMonitor.ballInPlay;

                if (curArrow == null && ball)
                {
                    curArrow = indicator.AddIndicator(FieldMonitor.ballInPlay.transform, 0);
                }
                else if (curArrow != null && !ball)
                {
                    indicator.RemoveIndicator(curArrow);
                    curArrow = null;
                }

            }
        
	}
}
