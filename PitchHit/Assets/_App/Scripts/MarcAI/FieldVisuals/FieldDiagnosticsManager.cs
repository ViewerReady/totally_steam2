﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldDiagnosticsManager : MonoBehaviour {

    public TrailRenderer ballTrailPrefab;
    private TrailRenderer m_ballTrail;
    private TrailRenderer ballTrail
    {
        get
        {
            if (m_ballTrail == null) m_ballTrail = GameObject.Instantiate(ballTrailPrefab);
            return m_ballTrail;
        }
    }

    public MeleeWeaponTrail batTrailPrefab;
    private MeleeWeaponTrail m_batTrail;
    private MeleeWeaponTrail batTrail
    {
        get
        {
            if(m_batTrail == null) m_batTrail = GameObject.Instantiate(batTrailPrefab);
            return m_batTrail;
        }
    }

    public GameObject ballMarkerPrefab;
    private GameObject m_ballMarker;
    private GameObject ballMarker
    {
        get
        {
            if (m_ballMarker == null) m_ballMarker = GameObject.Instantiate(ballMarkerPrefab);
            return m_ballMarker;
        }
    }

    public GameObject ghostBatPrefab;
    private GameObject m_ghostBat;
    private GameObject ghostBat
    {
        get
        {
            if (m_ghostBat == null) m_ghostBat = GameObject.Instantiate(ghostBatPrefab);
            return m_ghostBat;
        }
    }

    public float lifespan = 3f;

    public bool diagnosticsEnabled
    {
        get { return m_diagnosticsEnabled; }
        set
        {
            m_diagnosticsEnabled = value;
            if (!value)
            {
                HideDiagnostics();
            }
        }
    }
    private bool m_diagnosticsEnabled = false;


    void Start()
    {
        batTrail.Tip = HandManager.instance.bat.Tip;
        batTrail.Base = HandManager.instance.bat.Grip;
        batTrail.enabled = false;

        m_ballTrail = GameObject.Instantiate(ballTrailPrefab);

        m_ghostBat = GameObject.Instantiate(ghostBatPrefab);
        ghostBat.SetActive(false);

        m_ballMarker = GameObject.Instantiate(ballMarkerPrefab);
        ballMarker.transform.localScale = Vector3.one * .1f;
        ballMarker.SetActive(false);

    }

    void OnEnable()
    {
        FieldMonitor.OnBallPitched += RespondToPitch;
        FieldMonitor.PitchJudgement += RespondToPitchJudgement;
    }

    void OnDestroy()
    {
        FieldMonitor.OnBallPitched -= RespondToPitch;
        FieldMonitor.PitchJudgement -= RespondToPitchJudgement;
    }

    void ListenToBall(hittable ball)
    {

        ball.OnBatHit += MarkBallHit;
        ball.OnCaughtByDefender += StopBatTrail;
        ball.OnHitGround += StopBatTrail;
        ball.OnBallDestroy += StopBatTrail;
    }

    void RespondToPitch(hittable pitch)
    {
        if (diagnosticsEnabled)
        {
            ListenToBall(pitch);
            StartBatTrail();
        }
    }

    void RespondToPitchJudgement(hittable ball, UmpireCall call)
    {
        StopBatTrail(ball);
    }

    private IEnumerator HideDiagnosticsAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(lifespan);

        HideDiagnostics();
    }

    private void HideDiagnostics()
    {
        batTrail.enabled = false;
        ghostBat.SetActive(false);
        ballMarker.SetActive(false);
        ballTrail.enabled = false;
    }


    private void StartBatTrail()
    {
        batTrail.enabled = true;
        batTrail.Use = true;
    }


    private void StopBatTrail(hittable ball)
    {
        batTrail.Use = false;
        if (this && isActiveAndEnabled)
        {
            StartCoroutine("HideDiagnosticsAfterSeconds", lifespan);
        }

    }


    private void MarkBallHit(hittable ball)
    {
        //Debug.Log("Mark Hit");

        //MARKER
        ballMarker.SetActive(true);
        ballMarker.transform.position = ball.positionWhenHit;
        bool safetyHit = ball.GetComponent<BallCastAhead>().safetyHitFlag;
        ballMarker.GetComponent<MeshRenderer>().material.color = Color.white;// safetyHit? Color.magenta : Color.white;


        //BAT GHOST
        ghostBat.SetActive(true);
        ghostBat.transform.position = batTrail.Base.position;
        ghostBat.transform.LookAt(batTrail.Tip.position, Vector3.up);


        //BAT TRAIL
        batTrail.Use = false;
        MeleeWeaponTrail.Point p = new MeleeWeaponTrail.Point();
        p.basePosition = batTrail.Base.position;
        p.tipPosition = batTrail.Tip.position;
        batTrail.ForceAddPoint(p);


        //BALL TRAIL
        StartCoroutine("TrailBall", ball);
        
    }

    private IEnumerator TrailBall(hittable ball)
    {
        ballTrail.enabled = false;
        ballTrail.time = lifespan;
        ballTrail.transform.position = ball.positionWhenHit;
        ballTrail.transform.SetParent(ball.transform);
        //ballTrail.transform.localPosition = Vector3.zero;
        ballTrail.enabled = true;

        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();

        ballTrail.transform.SetParent(null);
    }
}
