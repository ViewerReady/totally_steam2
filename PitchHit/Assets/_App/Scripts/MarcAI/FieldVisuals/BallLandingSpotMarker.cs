﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLandingSpotMarker : MonoBehaviour
{

    public GameObject visualRoot;
    public GameObject impactVisual;
    public Transform afterImpactIndicator;
    public AnimationCurve shrinkCurve;
    public AnimationCurve trajectoryCurve;
    public float largestRadius;
    public float smallerRadius;
    public float smallerElevation;

    private hittable ball;
    private TrajectoryInfo trajectory;

    void Start()
    {

    }

    void OnDestory()
    {
    }

    public void SetColor(Color c)
    {
        foreach (SpriteRenderer sprite in visualRoot.GetComponentsInChildren<SpriteRenderer>())
        {
            sprite.color = c;
        }

    }

    public void ShowHide(bool isShowing)
    {
        if (visualRoot != null)
        {
            if (isShowing) visualRoot.transform.localScale = Vector3.one;
            visualRoot.SetActive(isShowing);
        }

        if (isShowing && impactVisual)//this resets the impact marker when the ball launches so that it will play its animation when the ball hits
        {
            impactVisual.SetActive(false);
        }

    }

    public void ShowTrajectory(hittable ball, TrajectoryInfo info)
    {
        StartCoroutine(TrajectoryRoutine(ball, info));


    }

    public void ShrinkToHide()
    {
        StartCoroutine(ShrinkRoutine(.3f));
    }

    protected IEnumerator ShrinkRoutine(float duration)
    {
        visualRoot.SetActive(true);
        visualRoot.transform.localScale = Vector3.one;

        float t = 0;
        while (t < duration)
        {
            t += Time.deltaTime;
            visualRoot.transform.localScale = Vector3.one * shrinkCurve.Evaluate(t / duration);
            yield return null;
        }
        visualRoot.SetActive(false);

    }

    IEnumerator TrajectoryRoutine(hittable ball, TrajectoryInfo info)
    {
        if (impactVisual)
        {
            impactVisual.SetActive(false);
        }

        if(afterImpactIndicator)
        {
            afterImpactIndicator.gameObject.SetActive(false);
        }

        visualRoot.gameObject.SetActive(true);

        transform.position = info.groundHitPosition;
        visualRoot.transform.localScale = Vector3.one * largestRadius;
        float timeUntilPeak = info.T_Apex;
        float timeUntilImpact = info.T_Ground;
        float timeUntilChestHeight = timeUntilImpact - .5f;

        float timePassed = 0f;


        float progress = 0;
        float targetSize = largestRadius;
        while(timePassed < timeUntilPeak)//ass the ball approaches its peak, shrink down towards its smaller size.
        {
            Debug.DrawRay(visualRoot.transform.position, Vector3.up, Color.red);
            progress = timePassed / timeUntilPeak;
            targetSize = Mathf.Lerp(largestRadius, smallerRadius, trajectoryCurve.Evaluate(progress));

            visualRoot.transform.localScale = Vector3.one * targetSize;

            yield return null;
            timePassed += Time.deltaTime;

        }

        while(timePassed < timeUntilChestHeight)
        {
            Debug.DrawRay(visualRoot.transform.position, Vector3.up, Color.yellow);
            //visualRoot.transform.localScale = Vector3.Lerp(visualRoot.transform.localScale, Vector3.one * smallerRadius * .9f, Time.deltaTime);

            yield return null;
            timePassed += Time.deltaTime;
        }

        Vector3 finalScale = visualRoot.transform.localScale;

        while(timePassed < timeUntilImpact)
        {
            progress = ((timeUntilImpact - timePassed) / (timeUntilImpact - timeUntilChestHeight));
            Debug.DrawRay(visualRoot.transform.position, Vector3.up, Color.green);
            visualRoot.transform.localScale = finalScale * trajectoryCurve.Evaluate(progress);
            yield return null;
            timePassed += Time.deltaTime;
        }

        visualRoot.gameObject.SetActive(false);

        if (impactVisual && ball && ball.isHeld == false)
        {
            impactVisual.transform.position = ball.transform.position;
            impactVisual.transform.position += Vector3.up * (.1f-impactVisual.transform.position.y);
            //yield return null;
            impactVisual.SetActive(true);
        }

        if(afterImpactIndicator)
        {
            afterImpactIndicator.gameObject.SetActive(true);

            while(ball && ball.isHeld==false)
            {
                afterImpactIndicator.position = ball.transform.position;
                yield return new WaitForEndOfFrame();
            }
            afterImpactIndicator.gameObject.SetActive(false);
        }

    }
    /*
    public void ShowImpact(Vector3 spot)
    {
        if (impactVisual)
        {
            spot.y = impactVisual.transform.position.y;
            impactVisual.transform.position = spot;
            impactVisual.SetActive(true);
        }
    }
    */
}
