﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CalloutManager : MonoBehaviour
{
    public GameObject vfxOutPrefab;
    public GameObject vfxScorePrefab;
    public TextMesh billboardMessagePrefab;
    public Text popMessageText;
    public Transform popMessage;

    [SerializeField] Transform[] fireworksSpawnPositions;
    [SerializeField] GameObject fireworksPrefab;
    [SerializeField] UIScoreboard uiScoreboard;

    public Transform popMessageRoot;
    public AnimationCurve popMessageCurve;
    private float popHeight = 3;
    private float popDuration = 1.5f;

    public SpriteMessageBillboard callAnimator;
    public SpriteMessageBillboard farCallAnimator;
    //public Animator closeCallAnimator;
    protected SpriteMessageBillboard callAnimator_mobile;

    public AudioClip homerunOrgan;
    public AudioClip foulOrgan;
    public AudioClip ballOrgan;
    public AudioClip outOrgan;
    public AudioClip strikeOrgan;

    public new AudioSource audio;
    public AudioSource voiceOver;

    public AudioClip[] outVo;
    public AudioClip[] foulVo;
    public AudioClip[] strikeVo;
    public AudioClip[] ballVo;
    public AudioClip[] homerunVo;

    public AudioClip[] openingGameVo;
    public AudioClip[] middleFrameVo;
    public AudioClip[] endFirstInningVo;
    public AudioClip[] newInningVo;
    public AudioClip[] closingGameVo;

    public static DarkTonic.MasterAudio.PlaySoundResult currentVOClip;

    public static bool isVOPlaying
    {
        get
        {
            if(currentVOClip != null)
            {
                return currentVOClip.ActingVariation.IsPlaying;
            }
            else
            {
                return false;
            }
        }
    }

    public ParticleSystem[] fireworks;

    // Use this for initialization
    void Start()
    {
        if (popMessage && popMessageRoot)
        {
            popMessage.position = popMessageRoot.position;
        }
        if (callAnimator)
        {
            callAnimator_mobile = Instantiate(callAnimator);
        }
    }

    void OnEnable()
    {
        FullGameDirector.OnScoreChange += CalloutRunAnimation;

    }

    void OnDisable()
    {
        FullGameDirector.OnScoreChange -= CalloutRunAnimation;

    }

    public void Spawn3DMessage(string text)
    {
        popMessageText.text = text;
        StartCoroutine(PopMessageRoutine(popMessage));
    }

    public void SpawnAnimatedMessage(UmpireCall call, bool far = false)
    {

        if (far && farCallAnimator)
        {
            farCallAnimator.SetAnimationTrigger(CallToTriggerString(call));
            if(call == UmpireCall.HOMERUN)
            {
                StartCoroutine(FireworksRoutine(5f));
            }
        }
        else
        {
            callAnimator.SetAnimationTrigger(CallToTriggerString(call));
        }

        PlaySoundForCall(call);
    }

    public void SpawnAnimatedMessage(UmpireCall call, Vector3 position)
    {

        callAnimator_mobile.SetPosition(position);

        callAnimator_mobile.SetAnimationTrigger(CallToTriggerString(call));
        PlaySoundForCall(call);
    }

    public void ClearAnimatedMessages()
    {
        farCallAnimator.SetAnimationTrigger("empty");
    }

    public void PlaySoundForCall(UmpireCall call)
    {
//        Debug.Log("Play sound for call " + call);
        switch (call)
        {
            case UmpireCall.BALL:
                DarkTonic.MasterAudio.MasterAudio.PlaySound("ball_organ");
                string ballOption;
                if (FullGameDirector.currentBalls == 0) ballOption = "Ball_04";
                else ballOption = "Ball_0" + (FullGameDirector.currentBalls);
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound(ballOption);
                break;
            case UmpireCall.FOUL:
                Debug.Log("Plating foul sound");
                DarkTonic.MasterAudio.MasterAudio.PlaySound("foul_organ");
                string foulOption = "vo_foul" + (FullGameDirector.currentFouls);
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_foul");
                break;
            case UmpireCall.OUT:
                DarkTonic.MasterAudio.MasterAudio.PlaySound("out_organ");
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound("TheyOut");
                break;
            case UmpireCall.STRIKE:
                DarkTonic.MasterAudio.MasterAudio.PlaySound("strike_organ");
                string strikeOption = "Strike_0" + (FullGameDirector.currentStrikes);
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound(strikeOption);
                break;
            case UmpireCall.HOMERUN:
                DarkTonic.MasterAudio.MasterAudio.PlaySound("homerun_organ");
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound("HOMERUN");
                break;
        }
    }

    private string CallToTriggerString(UmpireCall call)
    {
        switch (call)
        {
            case UmpireCall.STRIKE:
                return ("strike");
            case UmpireCall.BALL:
                return ("ball");
            case UmpireCall.FOUL:
                return ("foul");
            case UmpireCall.OUT:
                return ("out");
            case UmpireCall.HOMERUN:
                return ("homerun");
            case UmpireCall.FAIR:
                if (FullGameDirector.currentBalls >= FullGameDirector.maxBallsPerWalk)//Greg: I did this since there isn't an umpire call for WALK yet.
                {
                    return ("walk");
                }
                return ("run");
        }
        return "";
    }

    public void SpawnBillboardMessage(string text, Vector3 position)
    {
        TextMesh message = Instantiate(billboardMessagePrefab, position, Quaternion.identity) as TextMesh;
        message.text = text;
        message.transform.localScale = Vector3.one;// * WorldScaler.worldScale;
        StartCoroutine(BillboardMessageRoutine(message));

    }

    // Update is called once per frame
    IEnumerator BillboardMessageRoutine(TextMesh message)
    {
        float hold = 1f;
        yield return new WaitForSeconds(hold);
        GameObject.Destroy(message.gameObject);
    }

    IEnumerator PopMessageRoutine(Transform message)
    {

        float t = 0;
        while (t < popDuration)
        {
            message.position = popMessageRoot.position + Vector3.up * popMessageCurve.Evaluate(t / popDuration) * popHeight;
            t += Time.deltaTime;
            yield return null;
        }

        message.position = popMessageRoot.position;
    }

    IEnumerator FireworksRoutine(float seconds)
    {
        foreach (ParticleSystem p in fireworks)
        {
            p.gameObject.SetActive(true);
            var e = p.emission;
            e.enabled = false;
        }

        while (seconds>0)
        {
            seconds -= Time.deltaTime;
            yield return null;
        }

        foreach(ParticleSystem p in fireworks)
        {
            var e = p.emission;
            e.enabled = false;
        }
    }

    public void PlaySoundForFieldPlayType(FieldPlayType type)
    {
        switch (type)
        {
            case FieldPlayType.OPENNING:
                string sceneName = SceneManager.GetActiveScene().name;
                if (sceneName == "AI_LAB")
                {
                    if (FullGameDirector.awayTeam.teamName == "Sloths") DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Daytime_Sloths");
                    else DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Daytime_Generic");
                }
                else if (sceneName == "AI_LAB_NIGHT")
                {
                    if (FullGameDirector.awayTeam.teamName == "Robins") DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Night_Robins");
                    else DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Night_Generic");
                }
                if (sceneName == "FULL_GAME_PRO")
                {
                    if (FullGameDirector.awayTeam.teamName == "Phants") DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Stadium_Phants");
                    else DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_start_game", 1f, null, 0f, "Stadium_Generic");
                }
                break;
            case FieldPlayType.SWITCH:
                if (FullGameDirector.previousInningFrame == FullGameDirector.InningFrame.Top)//still the same inning.
                {
                    /*if (middleFrameVo.Length > 0)
                    {
                        voiceOver.clip = middleFrameVo[Random.Range(0, middleFrameVo.Length)];
                    }*/
                    //DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_middle_frame");
                }
                else//switching to a new inning.
                {
                    if (FullGameDirector.currentInning == 1)//first inning is numbered 0. value is increased before start of switch routine.
                    {
                        if (endFirstInningVo.Length > 0)
                        {

                        }
                    }
                    else
                    {
                        if (newInningVo.Length > 0)
                        {

                        }
                    }
                }
                break;
            case FieldPlayType.CLOSING:
                /*if (closingGameVo.Length > 0)
                {
                    voiceOver.clip = closingGameVo[Random.Range(0, closingGameVo.Length)];
                }*/
                Debug.Log("CALLOUT CLOSING");
                if (uiScoreboard != null) Debug.Log("GONNA PLAY ENDING ANIMATION");  uiScoreboard.PlayEndGameAnimation();
                currentVOClip = DarkTonic.MasterAudio.MasterAudio.PlaySound("vo_end_game");
                break;
            default:
                voiceOver.clip = null;
                break;
        }

        //if (voiceOver.clip)
        //{
        //    voiceOver.Play();
        //}
    }

    public void CalloutHomeRun()
    {
        //Debug.Log("Calling out home run");
        DarkTonic.MasterAudio.MasterAudio.PlaySound("reaction_homerun");
        if (uiScoreboard != null) uiScoreboard.PlayHomerunAnimation();
        //SpawnAnimatedMessage(UmpireCall.HOMERUN, true);
        if (fireworksSpawnPositions.Length > 0)
        {
            //Debug.Log("There was at least one thingy in the list");
            for (int i = 0; i < fireworksSpawnPositions.Length; i++)
            {
                //Debug.Log("Spawning in fireworks to " + fireworksSpawnPositions[i].gameObject.name);
                Instantiate(fireworksPrefab, fireworksSpawnPositions[i]);
            }
            for (int i = 0; i < 9; i++)
            {
                //Debug.Log("Spawning fireworks noise in " + (0.5f * (float)i) + "s");
                DarkTonic.MasterAudio.MasterAudio.PlaySound("Fireworks" + (i % 3).ToString(), 1f, null, (0.5f * (float)i));
            }
        }
    }

    public void CalloutRunAnimation(int teamAtBat, int currentInning, int runsThisFrame)
    {
        if (runsThisFrame == 0) return;     //We don't need to play any animations if it's just the score being reset.
        //Debug.Log("callout run");
        string teamName = FullGameDirector.currentOffensiveTeam.teamName;

        switch (teamName)
        {
            case "Foxes":
                if (uiScoreboard != null) uiScoreboard.PlayFoxScoreAnimation();
                break;
            case "Sloths":
                if (uiScoreboard != null) uiScoreboard.PlaySlothScoreAnimation();
                break;
            case "Robins":
                if (uiScoreboard != null) uiScoreboard.PlayRobinScoreAnimation();
                break;
            case "Phants":
                if (uiScoreboard != null) uiScoreboard.PlayPhantScoreAnimation();
                break;
        }
    }

    public void CalloutEndAnimation()
    {
        if (uiScoreboard != null) uiScoreboard.PlayEndGameAnimation();
    }
}