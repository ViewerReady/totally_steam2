﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

/// <summary>
/// This class contains information about the field that AI coordinators can reference
/// </summary>
[RequireComponent(typeof(FieldMonitor))]
[RequireComponent(typeof(CalloutManager))]
public class FullGameDirector : BaseGameDirector
{
    public static bool isTutorialLab;

    public static TeamInfo homeTeam
    {
        get; private set;
    }
    public static TeamInfo awayTeam
    {
        get; private set;
    }

    public static TeamInfo otherTeam(TeamInfo mine)
    {
        if (mine == homeTeam) return awayTeam;
        if (mine == awayTeam) return homeTeam;
        return null;
    }    


    
    public OffenseCoordinator offense;
    public DefenseCoordinator defense;
    public BroadcastCoordinator broadcast;
    public DugoutCoordinator dugout;

    float maxAnnouncerBoothTime = 17f;


    protected static CalloutManager callout;
    protected static FieldMonitor monitor;
    

    public const int maxOutsPerFrame = 3;
    private const int maxStrikesPerOut = 3;
    public const int maxBallsPerWalk = 4;

    private const int numTeams = 2;
    public static int numInningsPerGame
    {
        get
        {
            if (_numInningsPerGame == 0) return 6;
            return _numInningsPerGame;
        }
        private set
        {
            _numInningsPerGame = value;
        }
    }
    private static int _numInningsPerGame;

    public enum InningFrame { Top, Middle, Bottom };
    public static InningFrame currentInningFrame;
    public static InningFrame previousInningFrame = InningFrame.Middle;


    public static TeamInfo currentDefensiveTeam
    {
        get { switch (currentInningFrame) { case InningFrame.Top: return homeTeam; case InningFrame.Bottom: return awayTeam; } return homeTeam; }
    }
    public static TeamInfo currentOffensiveTeam
    {
        get { switch (currentInningFrame) { case InningFrame.Top: return awayTeam; case InningFrame.Bottom: return homeTeam; } return awayTeam; }
    }

    public Color homeTeamColor { get { if (!homeTeam) return Color.white; return homeTeam.homeColor; } }
    public Color awayTeamColor { get { if (!awayTeam) return Color.white; return awayTeam.awayColor; } }


    public Color defenseColor
    {
        get
        {
            if (currentInningFrame == InningFrame.Top) return homeTeamColor;
            if (currentInningFrame == InningFrame.Bottom) return awayTeamColor;
            return Color.white;
        }
    }
    public Color offenseColor
    {
        get
        {
            if (currentInningFrame == InningFrame.Bottom) return homeTeamColor;
            if (currentInningFrame == InningFrame.Top) return awayTeamColor;
            return Color.white;
        }
    }

    public static int homeTeamRuns
    {
        get { return m_homeTeamRuns; }
        private set
        {
            m_homeTeamRuns = value;
            if (currentInning >= (numInningsPerGame - 1) && homeTeamRuns > m_awayTeamRuns && currentInningFrame == InningFrame.Bottom)
            {
                //Debug.Log("End of game check 1.");
                CallEndOfGame();
            }
        }
    }
    private static int m_homeTeamRuns = 0;

    public static int awayTeamRuns
    {
        get { return m_awayTeamRuns; }
        private set
        {
            m_awayTeamRuns = value;          
        }
    }
    private static int m_awayTeamRuns = 0;

    public static int currentInning
    {
        get { return m_inning; }
        private set
        {
            m_inning = value;
        }
    }
    private static int m_inning = 0;

    public static int teamAtBat
    {
        get { return m_teamAtBat; }
        private set
        {
            m_teamAtBat = value;
            if (m_teamAtBat >= numTeams)
            {
                m_teamAtBat = 0;
                currentInning++;
                //Debug.Log("INNING IS NOW: " + currentInning);
            }
        }
    }
    private static int m_teamAtBat = 0;

    public static int runsThisFrame
    {
        get { return m_runsThisFrame; }
        private set
        {
            if (value > m_runsThisFrame)
            {
                if (teamAtBat % 2 == 0) awayTeamRuns++;
                else homeTeamRuns++;
            }
            m_runsThisFrame = value;
            if (OnScoreChange != null) OnScoreChange(teamAtBat, currentInning, m_runsThisFrame);
        }
    }
    private static int m_runsThisFrame;

    public static int currentStrikes
    {
        get { return m_strikes; }
        private set
        {
            m_strikes = value;
            if (OnStrikesChange != null) OnStrikesChange.Invoke(m_strikes);
            if (m_strikes >= maxStrikesPerOut)
            {
                if (Instance && Instance.offense)
                {
                    //FieldManager.Instance.announcerMinimap.DestroyMiniBatter();
                    Instance.offense.ReactToBattingOut(OutType.STRIKEOUT);
                }
                CalloutOut(null, OutType.STRIKEOUT);
            }
        }
    }
    private static int m_strikes = 0;
    public static int currentFouls
    {
        get { return m_fouls; }
        private set
        {
            m_fouls = value; if (OnFoulsChange != null) OnFoulsChange.Invoke(m_fouls);
            if (m_fouls == 0) return;
        }
    }
    private static int m_fouls = 0;
    public static int currentBalls
    {
        get { return m_balls; }
        private set
        {
            m_balls = value;
            if (OnBallsChange != null) OnBallsChange.Invoke(m_balls);
            if (m_balls >= maxBallsPerWalk) CalloutWalk();
        }
    }
    private static int m_balls = 0;
    public static int currentOuts
    {
        get { return m_outs; }
        private set
        {
            Debug.Log("Setting current outs to " + value);
            m_outs = value; if (OnOutsChange != null) OnOutsChange.Invoke(m_outs);
            Debug.Log("Checking if current outs " + m_outs + " is greater than or equal to the outs required for this inning " + maxOutsPerFrame);
            if (m_outs >= maxOutsPerFrame)
            {
                Debug.Log("Calling Switch sides");
                CallSwitchSides();
            }
        }
    }
    private static int m_outs = 0;

    public static Action<int> OnStrikesChange;
    public static Action<int> OnFoulsChange;
    public static Action<int> OnBallsChange;
    public static Action<int> OnOutsChange;
    public static Action<int, int, int> OnScoreChange;
    public static Action<int, bool> OnChangeover;

    public static Action<Transform, PlayerInfo> OnShowBatter;
    public static Action OnHideBatter;
    public static Action<Queue<PlayerInfo>> OnBattingOrderUpdated;
    public static Action<Transform, PlayerInfo> OnBatterWalkToPlate;

    protected FieldBase[] baseObjects;

    private static FullGameDirector m_Instance;
    public static FullGameDirector Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<FullGameDirector>();
            }
            return m_Instance;
        }

        private set
        {
            m_Instance = value;
        }
    }

    public struct RankedBase
    {
        public int baseNo;
        public float value;
        public RankedBase(int b, float v)
        {
            baseNo = b; value = v;
        }
    }

    public struct RankedPosition
    {
        public Vector3 position;
        public int index;
        public float value;
        public RankedPosition(Vector3 p, float v, int i = 0)
        {
            position = p; value = v; index = i;
        }
    }

    private bool _unjudgedPitchInProgress;

    void Awake()
    {
        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
        Application.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);
        if (Instance == null || Instance == this)
        {
            Instance = this;
        }
        else
        {
            GameObject.Destroy(this);
        }
    }

    void OnEnable()
    {
        FieldMonitor.OnRegisterRunner += RegisterRunner;
        FieldMonitor.OnUnRegisterRunner += UnRegisterRunner;
        FieldMonitor.OnRegisterDefensePlayer += RegisterDefensePlayer;
        FieldMonitor.OnUnRegisterDefensePlayer += UnRegisterDefensePlayer;

        FieldMonitor.HitBallJudgement += ReactToHitBallJudgement;
        FieldMonitor.PitchJudgement += ReactToPitchJudgement;
        FieldMonitor.OnBallPitched += ReactToPitch;
        OffenseCoordinator.OnNewBatter += ResetStatsForNewBatter;

        OnFieldEvent += PrintFieldEvent;
    }

    void OnDisable()
    {
        FieldMonitor.OnRegisterRunner -= RegisterRunner;
        FieldMonitor.OnUnRegisterRunner -= UnRegisterRunner;
        FieldMonitor.OnRegisterDefensePlayer -= RegisterDefensePlayer;
        FieldMonitor.OnUnRegisterDefensePlayer -= UnRegisterDefensePlayer;

        FieldMonitor.HitBallJudgement -= ReactToHitBallJudgement;
        FieldMonitor.PitchJudgement -= ReactToPitchJudgement;
        FieldMonitor.OnBallPitched -= ReactToPitch;
        OffenseCoordinator.OnNewBatter -= ResetStatsForNewBatter;

        OnFieldEvent -= PrintFieldEvent;
    }

    void Start()
    {
        this.transform.localPosition = Vector3.zero;
        callout = GetComponent<CalloutManager>();
        monitor = GetComponent<FieldMonitor>();



        InitializeGameStats();

        //Debug.Log("FULL GAME DIRECTOR START CALLED");
        if (offense)
        {
            offense.Initialize();
            offense.OnBattingOrderUpdated += BattingOrderChanged;
            //Debug.Log("CALLING SET BATTING TEAM FROM START");
            offense.SetBattingTeam(currentOffensiveTeam);
        }
        if (defense) defense.Initialize();
        if (broadcast) broadcast.Initialize();
        if (dugout) dugout.Initialize();

        foreach(AIFieldCoordinator coordinator in GetComponentsInChildren<AIFieldCoordinator>())
        {
            OnFieldEvent += coordinator.RespondToFieldEvent;
        }

        //use static role by default, then instance role
        StartOpenningCeremony();
    }

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            InstantResetAll();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            currentOuts++;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            skipCeremony = true;
        }

        //if a ball manages to get below the ground, destroy it
        hittable ballInPlay = FieldMonitor.ballInPlay;
        if (ballInPlay)
        {
            if (((ballInPlay.transform.position).y < -1 && ballInPlay.isHeld == false)
                || (ballInPlay.transform.position.magnitude > 500))
            {

                monitor.ForceDestroyBallInPlay();
            }
        }
    }


    public void InstantResetAll()
    {
        //remove all runners, hide the batter
        offense.InstantReset();
        defense.InstantReset();

        //make sure players are dressed for defense team. Drop at pitch positions
        foreach (AIFieldPlayer p in FieldMonitor.defensivePlayers)
        {
            p.InstantReset();
        }
        
        //MARC: shouldn't be any runners left, this is redundant but I'm leaving it in just in case
        foreach (AIFieldPlayer p in FieldMonitor.offensivePlayers)
        {
            p.InstantReset();
        }

        monitor.ForceDestroyBallInPlay();

        //will apply proper jersey and gear
        //HumanPosessionManager.RefreshJersey(); //Is this Necessary???? Bradley 9-20-19
        //Debug.Log("INSTANT RESET ALL MAKING FIELD PLAY TYPE NONE.");
        if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.NONE);      
    }


#region Ball Management

    void PrintFieldEvent(hittable ball, FieldPlayType play)
    {
#if UNITY_EDITOR
        //Debug.Log("<color=yellow>FIELD PLAY  : " + play.ToString() + "</color>");
#endif
    }

    public bool ReadyForNewBall()
    {
        if (offense && offense.runningPlay == FieldPlayType.HOMERUN)
        {
            return false;
        }

        if (defense && defense.runningPlay == FieldPlayType.HOMERUN)
        {
            return false;
        }

        return FieldMonitor.ballInPlay == null;
    }

    void ReactToHitBallJudgement(hittable ball, UmpireCall call, bool conclusive)
    {
        switch (call)
        {
            case UmpireCall.FAIR:
                //CalloutFairBall();
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.STANDARD);
                break;
            case UmpireCall.FOUL:
                if (conclusive)
                {
                    CalloutFoul();
                }
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.FOUL);
                break;
            case UmpireCall.HOMERUN:
                if (conclusive)
                {
                    CalloutHomeRun();
                }
                if (SpectatorCameraController.Instance) SpectatorCameraController.Instance.SetCamToHomerun();
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.HOMERUN);
                break;
        }
        _unjudgedPitchInProgress = false;
    }

    void ReactToPitchJudgement(hittable ball, UmpireCall call)
    {
        if (!PitchInProgress()) return;
        switch (call)
        {
            case UmpireCall.NONE:
                //failed pitch, 
                if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.NONE);
                break;
            case UmpireCall.STRIKE:
                CalloutStrike(ball);
                break;
            case UmpireCall.BALL:
                CalloutBall();
                break;
        }
        _unjudgedPitchInProgress = false;
    }

    void ReactToPitch(hittable ball)
    {
        //if offense isn't ready, don't consider it a pitch
        if (offense.ReadyForPitch(defense.humanControlled))
        {
            _unjudgedPitchInProgress = true;
            if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.PITCH);
        }
    }

    #endregion

    #region Game Settings
    public static void SetHomeTeam(TeamInfo home)
    {
        Debug.Log("Setting home team to: " + home.teamName);
        homeTeam = home;
    }

    public static void SetAwayTeam(TeamInfo away)
    {
        Debug.Log("Setting away team to: " + away.teamName);
        awayTeam = away;
    }

    public static void SetNumInnings(int innings)
    {
        numInningsPerGame = innings;
    }
    #endregion

    #region Team Events
    private static void BattingOrderChanged(Queue<PlayerInfo> battingQueue)
    {
            if (OnBattingOrderUpdated != null) OnBattingOrderUpdated.Invoke(battingQueue);
    }

    #endregion

    #region AI Player Tracking

   
    private void RegisterRunner(OffensePlayer fielder)
    {   
            fielder.OnOut += CalloutOut;
            fielder.OnHome += CalloutCompletedRun;   
    }

    private void UnRegisterRunner(OffensePlayer fielder)
    {
            fielder.OnOut -= CalloutOut;
            fielder.OnHome -= CalloutCompletedRun;
    }

    private void RegisterDefensePlayer(DefensePlayer fielder)
    {
            fielder.OnFumble += CalloutFumble;
    }

    private void UnRegisterDefensePlayer(DefensePlayer fielder)
    {
            fielder.OnFumble -= CalloutFumble;
    }

    #endregion

    public bool PitchInProgress()
    {
        return _unjudgedPitchInProgress;
    }

    public bool DefenseReadyForNewBatter()
    {
        return defense._state == FieldPlayType.NONE || (defense._state == FieldPlayType.RETURN && monitor.PitcherOnMoundWithBall());
    }

    public bool BothTeamsReadyForPitch()
    {
        return monitor.AllRunnersOnBase() && monitor.PitcherOnMoundWithBall() && offense.ReadyForPitch(defense.humanControlled);
        //return (offense.runningPlay == FieldPlayType.NONE) && defense.runningPlay == FieldPlayType.NONE;
    }

   

#region Callouts

    private static void InitializeGameStats()
    {
        teamAtBat = 0;
        currentInning = 0;
        currentInningFrame = InningFrame.Top;
        homeTeamRuns = 0;
        awayTeamRuns = 0;
        runsThisFrame = 0;     

        currentOuts = 0;

        currentStrikes = 0;
        currentBalls = 0;
        currentFouls = 0;
    }



    private static void ResetStatsForNewBatter(PlayerInfo batter)
    {
        Debug.Log("Restting stats for new Batter: " + batter.jerseyName);
        currentStrikes = 0;
        currentBalls = 0;
        currentFouls = 0;
    }


    private static void CalloutPlayBall()
    {

    }

    private static void CalloutWalk()
    {
        callout.SpawnAnimatedMessage(UmpireCall.FAIR);
        if(Instance) Instance.OnFieldEvent(null, FieldPlayType.WALK);

    }

    private static void CalloutFairBall()
    {
        Debug.Log("-----------------------------------------------------------------CALLOUT FAIR BALL");
        callout.SpawnAnimatedMessage(UmpireCall.FAIR);

    }

    private void CalloutHomeRun()
    {
        callout.CalloutHomeRun();
    }

    public void ClearCallouts()
    {
        callout.ClearAnimatedMessages();
    }

    private static void CalloutCompletedRun(OffensePlayer runner)
    {
        //spawn a fun message over home plate
        runsThisFrame++;
    }

    public void CalloutStrike(hittable pitch)
    {
        currentStrikes++;
        callout.SpawnAnimatedMessage(UmpireCall.STRIKE);
    }

    private void CalloutFoul()
    {
        currentFouls++;
        if (currentStrikes < (maxStrikesPerOut - 1))
        {
            currentStrikes++;
        }
        callout.SpawnAnimatedMessage(UmpireCall.FOUL);

        //less than two strikes - foul counts as strike
    }

    public static void CalloutBall()//OffensePlayer batter)
    {
        //Debug.Log("CALLOUT BALL");
        currentBalls++;
        callout.SpawnAnimatedMessage(UmpireCall.BALL);
    }

    private static void CalloutFumble(DefensePlayer defender)
    {
        //callout.SpawnBillboardMessage("FUMBLE", defender.transform.position + Vector3.up);// * WorldScaler.worldScale);
    }

    private static void CalloutOut(AIFieldPlayer runner, OutType outType)
    {
        if (outType == OutType.INNING_OVER || outType == OutType.HIT_FOUL) return;

        
        if (runner)
        {
            //runner will spawn their own explosion effect, just need the sound
            DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", runner.transform);
            if (outType != OutType.STRIKEOUT) callout.SpawnAnimatedMessage(UmpireCall.OUT);
        }
        else
        {
            if(outType != OutType.STRIKEOUT) callout.SpawnAnimatedMessage(UmpireCall.OUT);
        }
        //DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtTransform("reaction_happy", runner.transform);
        currentOuts++;
    }

    private static void CallSwitchSides()
    {
        Debug.Log("SWITCH SIDES");
        //currentOuts = 0;

        teamAtBat++;
        //game may be determined to be over at this point

        Debug.Log("Current inning: " + m_inning);
        Debug.Log("Num innings per game: " + numInningsPerGame);

        //if (Instance && currentInning <= (numInningsPerGame-1))
        if (Instance)
        {
            Debug.Log("First check: " + (currentInning <= (numInningsPerGame - 1)));
            Debug.Log("Second check: " + (currentInning >= (numInningsPerGame - 1) && (homeTeamRuns == awayTeamRuns && currentInningFrame == InningFrame.Bottom)));
            Debug.Log("Third check: " + (currentInning >= (numInningsPerGame - 1) && (homeTeamRuns != awayTeamRuns && currentInningFrame == InningFrame.Top)));

            if (currentInning > (numInningsPerGame - 1) && homeTeamRuns < awayTeamRuns && currentInningFrame == InningFrame.Bottom)
            {
                Debug.Log("End of game check 2.");
                CallEndOfGame();
            }
            else if (currentInning >= (numInningsPerGame - 1) && homeTeamRuns > awayTeamRuns && currentInningFrame == InningFrame.Top)
            {
                Debug.Log("End of game check 3.");
                CallEndOfGame();
            }
            else if((currentInning <= (numInningsPerGame - 1)) || (currentInning >= (numInningsPerGame - 1) && (homeTeamRuns == awayTeamRuns && currentInningFrame == InningFrame.Bottom)) || (currentInning >= (numInningsPerGame - 1) && (currentInningFrame == InningFrame.Top)))
            {
                Debug.Log("Start Changeover Routine");
                runsThisFrame = 0;
                OnChangeover.Invoke(currentInning + 1, currentInningFrame != InningFrame.Top);
                Instance.StartCoroutine(Instance.ChangeOverRoutine());
            }
        }
    }


    public static bool skipCeremony = false;

    IEnumerator ChangeOverRoutine()
    {
        skipCeremony = false;

        Debug.Log("Changeover routine started");
        //offense.SetBattingTeam(currentDefensiveTeam);
        previousInningFrame = currentInningFrame;

        if (doInningChangeOver)
        {
            currentInningFrame = InningFrame.Middle;

            HumanPlayerRole targetRole = currentHumanPlayerRole;
            if (currentHumanPlayerRole == HumanPlayerRole.DEFENSE)
            {
                targetRole = (HumanPlayerRole.OFFENSE);
                ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }
            else if (currentHumanPlayerRole == HumanPlayerRole.OFFENSE)
            {
                targetRole = (HumanPlayerRole.DEFENSE);
                ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }
            yield return null;

            //wait for posession routine to finish
            while (HumanPosessionManager.transferRoutineInProgress)
            {
                yield return null;
            }
            monitor.ForceDestroyBallInPlay();
            yield return null; //wait for ball to respawn in pitcher's glove
            OnFieldEvent(null, FieldPlayType.SWITCH);
            callout.PlaySoundForFieldPlayType(FieldPlayType.SWITCH);

            yield return new WaitForFixedUpdate(); //wait for the pitcher with the ball to be removed from the mound trigger

            //while (CalloutManager.currentVOClip.ActingVariation.IsPlaying)
            while (CalloutManager.isVOPlaying && !skipCeremony)
            {
                yield return null;
            }
            float timeInBooth = 0f;

            if (!skipCeremony) { 
                yield return new WaitForSeconds(1f);
                timeInBooth += 1f;
            }


            DarkTonic.MasterAudio.PlaySoundResult currentSound;

            int voIndex = 0;
            string[] voClipNames = new string[]
            {
                (previousInningFrame == InningFrame.Top)? "Mid_Inning" : "End_Inning",
                "Sponsor",
                "Back_to_game",
            };

            while (voIndex < voClipNames.Length && !skipCeremony)
            {
                currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound(voClipNames[voIndex]);

                if (currentSound == null)
                {
                    Debug.Log("Current sound is null");
                    voIndex++;
                    continue;
                }
                else if (currentSound.ActingVariation == null)
                {
                    Debug.Log("Acting variation is null");
                    voIndex++;
                    continue;
                }

                CalloutManager.currentVOClip = currentSound;
                while (currentSound != null && currentSound.ActingVariation.IsPlaying && !skipCeremony)
                {
                    timeInBooth += Time.deltaTime;
                    yield return null;
                }

                float endPauseTime = timeInBooth + 1;
                while (timeInBooth < endPauseTime && !skipCeremony)
                {
                    timeInBooth += Time.deltaTime;
                    yield return null;
                }
                voIndex++;
            }


           

            //while ((!PitcherOnMoundWithBall() || timeInBooth < maxAnnouncerBoothTime) && !skipChangeover)
            //while (!PitcherOnMoundWithBall() && !skipChangeover)
            while ((!monitor.PitcherOnMoundWithBall() || timeInBooth < maxAnnouncerBoothTime) && !skipCeremony)
            {
                timeInBooth += Time.deltaTime;
                //Debug.Log("ChangeOver Wait " + PitcherOnMoundWithBall() + " " + skipChangeover);
                yield return null;
            }
            yield return new WaitForSeconds(1);
            if (previousInningFrame == InningFrame.Top)
            {
                currentInningFrame = InningFrame.Bottom;
            }
            else
            {
                currentInningFrame = InningFrame.Top;
            }
            previousInningFrame = InningFrame.Middle;

            Debug.Log("SETTING THE BATTING TEAM IN THE GAME DIRECTOR'S CHANGEOVER ROUTINE");
            currentOuts = 0;
            offense.SetBattingTeam(currentOffensiveTeam);

            while (!monitor.AllDefendersAtPitchPosition()
                && !skipCeremony)
            {
                //            Debug.Log("Waiting to reach positions " + AllDefendersAtPitchPosition());
                yield return null;
            }

            InstantResetAll();
            if (currentHumanPlayerRole != targetRole) ChangePlayerRole(targetRole);
        }
        else
        {
            InstantResetAll();
        }
    }

    private static void StartOpenningCeremony()
    {
        if (Instance)
        {
            Instance.StartCoroutine(Instance.OpenningCeremonyRoutine());
        }
    }

    IEnumerator OpenningCeremonyRoutine()
    {
        HumanPlayerRole startingRole;
        startingRole = currentHumanPlayerRole;

        skipCeremony = false;

        if (doOpenningCeremony)
        {
            Debug.Log("Starting opening Ceremony");
            if (SpectatorCameraController.Instance) SpectatorCameraController.Instance.SetCamToCeremony();
            callout.PlaySoundForFieldPlayType(FieldPlayType.OPENNING);
            if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.OPENNING);


           if(startingRole != HumanPlayerRole.NONE)
            {
                ChangePlayerRole(HumanPlayerRole.BROADCAST, true);
            }
            if (SpectatorCameraController.Instance) SpectatorCameraController.Instance.SetCamToCeremony();
            float t = 0;
            while (!skipCeremony && t<15) {
                t += Time.deltaTime;
                yield return null;
            }
        }
        ChangePlayerRole(startingRole, true);
        InstantResetAll();
        yield return null;
    }

    private static void StartClosingCeremony()
    {
        if (Instance)
        {
            Instance.StartCoroutine(Instance.ClosingCeremonyRoutine());
        }
    }

    IEnumerator ClosingCeremonyRoutine()
    {
        skipCeremony = false;

        if (doClosingCeremony)
        {
            DarkTonic.MasterAudio.PlaySoundResult currentSound;
            DarkTonic.MasterAudio.MasterAudio.StopAllOfSound("field-ambience");
            DarkTonic.MasterAudio.MasterAudio.PlaySound("take_me_out_organ");
            if (awayTeamRuns > homeTeamRuns) callout.CalloutRunAnimation(0, currentInning, 1);
            else callout.CalloutRunAnimation(1, currentInning, 1);

            if (currentHumanPlayerRole != HumanPlayerRole.NONE)
            {
                Instance.ChangePlayerRole(HumanPlayerRole.BROADCAST);
            }
            if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.CLOSING);

            yield return new WaitForSeconds(2f);
            if (SpectatorCameraController.Instance) SpectatorCameraController.Instance.SetCamToCeremony();
            currentSound = DarkTonic.MasterAudio.MasterAudio.PlaySound("EndGameVO");

            //callout.CalloutRunAnimation(teamAtBat, currentInning, m_runsThisFrame);
            callout.CalloutEndAnimation();
            //if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.CLOSING);
            

            while (!skipCeremony && currentSound.ActingVariation.IsPlaying)
            {
                yield return null;
            }
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene("TOTALLY_LOCKERS");
            
    }

    public Vector3 GetDugoutForTeam(TeamInfo team)
    {
        if (team == homeTeam) return monitor.homeDugout.position;
        if (team == awayTeam) return monitor.awayDugout.position;

        return Vector3.zero;
    }

    private static void CallEndOfGame()
    {
        Debug.Log("END OF GAME");
        StartClosingCeremony();
    }

#endregion


    private static bool doInningChangeOver = true;
    private static bool doOpenningCeremony = true;
    private static bool doClosingCeremony = true;


    

    public static bool isBroadcastMode;

    public void ChangePlayerRole_Defense() { ChangePlayerRole(HumanPlayerRole.DEFENSE); }
    public void ChangePlayerRole_Offense() { ChangePlayerRole(HumanPlayerRole.OFFENSE); }
    public void ChangePlayerRole_Broadcast() { ChangePlayerRole(HumanPlayerRole.BROADCAST); }
    public void ChangePlayerRole_None() { ChangePlayerRole(HumanPlayerRole.NONE); }
    public void ChangePlayerRole_Tutorial() { ChangePlayerRole(HumanPlayerRole.TUTORIAL); }



    public static void PrepForFullGame(bool startBatting = false)
    {
        if (startBatting)
        {
            currentHumanPlayerRole = HumanPlayerRole.OFFENSE;
        }
        else
        {
            currentHumanPlayerRole = HumanPlayerRole.DEFENSE;
        }
        doOpenningCeremony = true;
        doClosingCeremony = true;
        doInningChangeOver = true;
        isBroadcastMode = false;
    }

    public static void PrepForOffenseOnly()
    {
        currentHumanPlayerRole = HumanPlayerRole.OFFENSE;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
        isBroadcastMode = false;
    }

    public static void PrepForDefenseOnly()
    {
        currentHumanPlayerRole = HumanPlayerRole.DEFENSE;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
        isBroadcastMode = false;
    }

    public static void PrepForBroadcast()
    {
        currentHumanPlayerRole = HumanPlayerRole.BROADCAST;
        doOpenningCeremony = true;
        doClosingCeremony = true;
        doInningChangeOver = true;
        isBroadcastMode = true;
    }

    public static void PrepForTutorial()
    {
        currentHumanPlayerRole = HumanPlayerRole.TUTORIAL;
        doOpenningCeremony = false;
        doClosingCeremony = false;
        doInningChangeOver = false;
        isBroadcastMode = false;
    }

    public override void ChangePlayerRole(HumanPlayerRole role, bool refresh = false)
    {
        if (currentHumanPlayerRole == role && !refresh) return;

        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.NONE:
                break;
            case HumanPlayerRole.DEFENSE:
                defense.SetHumanControlled(false);
                break;
            case HumanPlayerRole.OFFENSE:
                offense.SetHumanControlled(false);
                break;
            case HumanPlayerRole.BROADCAST:
                broadcast.SetHumanControlled(false);
                break;
            case HumanPlayerRole.TUTORIAL:
                defense.SetResponsiveToFieldEvents(true);
                offense.SetResponsiveToFieldEvents(true);
                broadcast.SetResponsiveToFieldEvents(true);

                defense.enabled = true;
                offense.enabled = true;
                //spectator.enabled = false;
                if(dugout) dugout.enabled = true;
                break;
        }

        previousHumanPlayerRole = currentHumanPlayerRole;
        currentHumanPlayerRole = role;

        switch (role)
        {
            case HumanPlayerRole.NONE:
                HumanPosessionManager.SetLocomotionEnabled(true);
                break;
            case HumanPlayerRole.DEFENSE:
                defense.SetHumanControlled(true);
                break;
            case HumanPlayerRole.OFFENSE:
                offense.SetHumanControlled(true);
                break;
            case HumanPlayerRole.BROADCAST:
                broadcast.SetHumanControlled(true);
                break;
            case HumanPlayerRole.TUTORIAL:
                defense.SetResponsiveToFieldEvents(false);
                offense.SetResponsiveToFieldEvents(false);
                broadcast.SetResponsiveToFieldEvents(false);
                defense.enabled = false;
                offense.enabled = false;
                //spectator.enabled = false;
                if(dugout) dugout.enabled = false;
                break;
        }

        base.ChangePlayerRole(role, refresh);
    }
}