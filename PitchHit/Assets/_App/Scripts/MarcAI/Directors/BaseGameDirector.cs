﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class BaseGameDirector : MonoBehaviour {

    public enum HumanPlayerRole
    {
        NONE,
        DEFENSE,
        OFFENSE,
        BROADCAST,
        TUTORIAL,
        TUTORIAL_BATTER,
        TUTORIAL_PITCHER,
        TUTORIAL_CATCH,
        TUTORIAL_PASS,
    }

    public static HumanPlayerRole currentHumanPlayerRole { get; protected set; }
    public static HumanPlayerRole previousHumanPlayerRole { get; protected set; }
    public Action OnRoleChanged;

    public Action<hittable, FieldPlayType> OnFieldEvent;

    public virtual void ChangePlayerRole(HumanPlayerRole role, bool refresh = false)
    {
#if UNITY_EDITOR
        Debug.Log("<color=cyan>CHANGE PLAYER ROLE  " +  previousHumanPlayerRole +  "  to   " + role + "</color>");
#endif
        if (HumanPosessionManager.Instance)
        {
            HumanPosessionManager.Instance.RefreshLocomotionType();
        }
        if (HumanGearManager.instance)
        {
            HumanGearManager.instance.RefreshColliderStatus();
        }


        if (OnRoleChanged != null) OnRoleChanged.Invoke();

    }
}
