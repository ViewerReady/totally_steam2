﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDirector : BaseGameDirector {

    //public TutorialZone tutorialZonePrefab;
    [SerializeField]
    private TutorialZone battingZone;
    [SerializeField]
    private TutorialZone pitchingZone;
    [SerializeField]
    private TutorialZone catchingZone;
    [SerializeField]
    private TutorialZone passingZoneA;
    [SerializeField]
    private TutorialZone passingZoneB;


    public BattingPracticeCoordinator battingCoordinator;
    public PitchingPracticeCoordinator pitchingCoordinator;
    public CatchingPracticeCoordinator catchingCoordinator;
    public PassingPracticeCoordinator passingCoordinator;

    public AIFieldPlayer defaultHumanBody;



	// Use this for initialization
	void Start () {
        foreach (AIFieldCoordinator coordinator in GetComponentsInChildren<AIFieldCoordinator>())
        {
            OnFieldEvent += coordinator.RespondToFieldEvent;
        }
        CreateTutorialZones();
        ChangePlayerRole(HumanPlayerRole.NONE, refresh: true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        FieldMonitor.HitBallJudgement += ReactToHitBallJudgement;
        FieldMonitor.PitchJudgement += ReactToPitchJudgement;
        FieldMonitor.OnBallPitched += ReactToPitch;
    }

    void OnDisable()
    {
        FieldMonitor.HitBallJudgement -= ReactToHitBallJudgement;
        FieldMonitor.PitchJudgement -= ReactToPitchJudgement;
        FieldMonitor.OnBallPitched -= ReactToPitch;
    }

    void ReactToHitBallJudgement(hittable ball, UmpireCall call, bool final)
    {
        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.TUTORIAL_BATTER:
                FieldMonitor.Instance.ExplodeBall(2);
                break;
            case HumanPlayerRole.TUTORIAL_PITCHER:
                FieldMonitor.Instance.ExplodeBall(1);
                break;
            case HumanPlayerRole.TUTORIAL_PASS:
                break;
            case HumanPlayerRole.TUTORIAL_CATCH:
                break;
        }
        if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.NONE);
        return;
    }

    void ReactToPitchJudgement(hittable ball, UmpireCall call)
    {
        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.TUTORIAL_BATTER:
                FieldMonitor.Instance.ExplodeBall(1);
                break;
            case HumanPlayerRole.TUTORIAL_PITCHER:
                FieldMonitor.Instance.ExplodeBall(1);
                break;
            case HumanPlayerRole.TUTORIAL_PASS:
                break;
            case HumanPlayerRole.TUTORIAL_CATCH:
                break;
        }
        if (OnFieldEvent != null) OnFieldEvent.Invoke(null, FieldPlayType.NONE);
    }

    void ReactToPitch(hittable ball)
    {
         if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.PITCH);
    }

    void CreateTutorialZones()
    {
        battingZone.OnHumanExitZone += EnterNone;
        battingZone.OnHumanEnterZone += EnterBattingPractice;

        pitchingZone.OnHumanExitZone += EnterNone;
        pitchingZone.OnHumanEnterZone += EnterPitchingPractice;

        catchingZone.OnHumanExitZone += EnterNone;
        catchingZone.OnHumanEnterZone += EnterCatchingPractice;

        passingZoneA.OnHumanExitZone += EnterNone;
        passingZoneA.OnHumanEnterZone += EnterPassingPractice;

        passingZoneB.OnHumanExitZone += EnterNone;
        passingZoneB.OnHumanEnterZone += EnterPassingPractice;
    }

    public override void ChangePlayerRole(HumanPlayerRole role, bool refresh = false)
    {
        if (currentHumanPlayerRole == role && !refresh) return;

        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.NONE:
                HumanPosessionManager.ReleasePosessedPlayer();
                SetZonePartilesPlaying(false);
                break;
            case HumanPlayerRole.TUTORIAL_BATTER:
                battingCoordinator.SetHumanControlled(false);
                break;
            case HumanPlayerRole.TUTORIAL_CATCH:
                catchingCoordinator.SetHumanControlled(false);
                break;
            case HumanPlayerRole.TUTORIAL_PITCHER:
                pitchingCoordinator.SetHumanControlled(false);
                break;
            case HumanPlayerRole.TUTORIAL_PASS:
                passingCoordinator.SetHumanControlled(false);
                break;
           
        }

        previousHumanPlayerRole = currentHumanPlayerRole;
        currentHumanPlayerRole = role;

        switch (currentHumanPlayerRole)
        {
            case HumanPlayerRole.NONE:
                HumanPosessionManager.PosessPlayer(defaultHumanBody, fade: true, snapToHumanPosition: true);
                SetZonePartilesPlaying(true);
                break;
            case HumanPlayerRole.TUTORIAL_BATTER:
                battingCoordinator.SetHumanControlled(true);
                break;
            case HumanPlayerRole.TUTORIAL_CATCH:
                catchingCoordinator.SetHumanControlled(true);
                break;
            case HumanPlayerRole.TUTORIAL_PITCHER:
                pitchingCoordinator.SetHumanControlled(true);
                break;
            case HumanPlayerRole.TUTORIAL_PASS:
                passingCoordinator.SetHumanControlled(true);
                break;

        }

        base.ChangePlayerRole(role, refresh);
    }

    private void SetZonePartilesPlaying(bool play)
    {
        battingZone.ShowHideParticles(play);
        catchingZone.ShowHideParticles(play);
        pitchingZone.ShowHideParticles(play);
        passingZoneA.ShowHideParticles(play);
        passingZoneB.ShowHideParticles(play);
    }


    void EnterNone()
    {
        Debug.Log("Enter none");
        ChangePlayerRole(HumanPlayerRole.NONE);
    }

    void EnterBattingPractice()
    {
        Debug.Log("Enter batting practice");
        ChangePlayerRole(HumanPlayerRole.TUTORIAL_BATTER);
    }

    void EnterPitchingPractice()
    {
        Debug.Log("Enter pitching practice");
        ChangePlayerRole(HumanPlayerRole.TUTORIAL_PITCHER);
    }

    void EnterCatchingPractice()
    {
        Debug.Log("Enter catching practice");
        ChangePlayerRole(HumanPlayerRole.TUTORIAL_CATCH);
    }

    void EnterPassingPractice()
    {
        Debug.Log("Enter passing practice");
        ChangePlayerRole(HumanPlayerRole.TUTORIAL_PASS);
    }

   
}
