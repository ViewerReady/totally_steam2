﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PitchProgressCheckpoint : MonoBehaviour {

    protected SpriteRenderer ring;
    public AudioClip passSound;

    public Action<hittable> OnGatePassThrough;

	// Use this for initialization
	void Start () {
        ring = GetComponentInChildren<SpriteRenderer>();
        Reset();
	}
	

    public void Reset()
    {
		if (ring) {
			ring.transform.localScale = Vector3.zero;
		}
    }

    void OnTriggerEnter(Collider other)
    {
        hittable pitch = other.GetComponent<hittable>();
        if (pitch != null && !pitch.GetWasHitByBat() && !pitch.hasBeenCaught)
        {
            OnGatePass(pitch);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("hit checkpoint");
    }

    void OnGatePass(hittable pitch)
    {
        Vector3 otherPos_local = transform.InverseTransformPoint(pitch.transform.position);
        ring.transform.localPosition = new Vector3(otherPos_local.x, otherPos_local.y, 0);
        if (passSound != null)
        {
            AudioSource.PlayClipAtPoint(passSound, transform.position);
        }
        if (OnGatePassThrough != null) OnGatePassThrough.Invoke(pitch);
        StartCoroutine("CloseAfterSeconds", 1);

    }

    IEnumerator CloseAfterSeconds(float seconds)
    {
        float openSpeed = 5;
        float closeSpeed = 2;
        float t = 0;
        while (t< 1)
        {
            t += Time.deltaTime* openSpeed;
            t = Mathf.Clamp01(t);
            ring.transform.localScale = Vector3.one* (t);
            yield return null;
        }
yield return new WaitForSeconds(seconds);
        while (t> 0)
        {
            t -= Time.deltaTime* closeSpeed;
            t = Mathf.Clamp01(t);
            ring.transform.localScale = Vector3.one* (t);
            yield return null;
        }
    }


}
