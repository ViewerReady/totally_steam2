﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTeamLoader : MonoBehaviour {

    public enum HumanPlayerMode
    {
        NONE,
        FULL,
        DEFENSE_ONLY,
        OFFENSE_ONLY,
        BROADCAST,
        TUTORIAL,
    }

    static public bool forceSettings = true;
    public TeamInfo homeTeam;
    public TeamInfo awayTeam;
    public int numInnings = 6;
    public HumanPlayerMode mode;

    void Awake()
    {
        /*if(FieldManager.homeTeam != null && FieldManager.awayTeam != null)
        {
            Debug.Log("DEBUG: TEAMS ALREADY SET. RETURNING.");
            return;
        }*/
        if (forceSettings)
        {
            FullGameDirector.SetHomeTeam(homeTeam);
            FullGameDirector.SetAwayTeam(awayTeam);
            FullGameDirector.SetNumInnings(numInnings);
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.NONE)
            {
                switch (mode)
                {
                    case HumanPlayerMode.FULL:
                        FullGameDirector.PrepForFullGame();
                        break;
                    case HumanPlayerMode.OFFENSE_ONLY:
                        FullGameDirector.PrepForOffenseOnly();
                        break;
                    case HumanPlayerMode.DEFENSE_ONLY:
                        FullGameDirector.PrepForDefenseOnly();
                        break;
                    case HumanPlayerMode.BROADCAST:
                        FullGameDirector.PrepForBroadcast();
                        break;
                    case HumanPlayerMode.TUTORIAL:
                        FullGameDirector.PrepForTutorial();
                        break;
                }
            }
        }
    }
}
