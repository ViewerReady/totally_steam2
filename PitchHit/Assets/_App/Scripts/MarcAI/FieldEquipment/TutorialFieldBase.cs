﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialFieldBase : MonoBehaviour {

    public List<AIFieldPlayer> players { get; private set; }
    private List<TutorialFieldBase> bases;     //This is so we can control the particle effects of the other bases on enter and exit.
    public ParticleSystem particles;
    protected new BoxCollider collider;
    [SerializeField] UnityEvent OnBaseEntered;
    [SerializeField] UnityEvent OnBaseLeft;

    void Awake()
    {
        players = new List<AIFieldPlayer>();
        bases = new List<TutorialFieldBase>();
        collider = GetComponent<BoxCollider>();
    }
	
    void Start()
    {
        bases = new List<TutorialFieldBase>(FindObjectsOfType<TutorialFieldBase>());
        collider.enabled = true;
        particles.Play();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("BASE ENTERED");
        AIFieldPlayer player = other.GetComponent<AIFieldPlayer>();
        if (player != null)
        {
            players.Add(player);
        }
        //particles.Stop();
        OnBaseEntered.Invoke();
        foreach (TutorialFieldBase b in bases) b.particles.Stop();
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("BASE EXITED");
        AIFieldPlayer player = other.GetComponent<AIFieldPlayer>();
        if (player != null)
        {
            players.Add(player);
        }
        //particles.Play();
        OnBaseLeft.Invoke();
        foreach (TutorialFieldBase b in bases) b.particles.Play();
    }
}