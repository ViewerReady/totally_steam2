﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FieldBaseRunThrough : MonoBehaviour {


    public List<OffensePlayer> offensivePlayers { get; private set; }


    public Action<OffensePlayer> OnPlayerExitTrigger;

    void OnTriggerEnter(Collider other)
    {
        OffensePlayer o = other.GetComponent<OffensePlayer>();
        if (o != null)
        {
            AddOffensePlayer(o);

        }
    }

    void OnTriggerExit(Collider other)
    {

        OffensePlayer o = other.GetComponent<OffensePlayer>();
        if (o != null)
        {
            RemoveOffensePlayer(o);
        }
    }


    void AddOffensePlayer(OffensePlayer o)
    {
        if (o.state == AIFieldPlayer.FieldPlayerAction.OUT) return;

        if (!offensivePlayers.Contains(o))
        {
            offensivePlayers.Add(o);
        }
    }

    void RemoveOffensePlayer(OffensePlayer o)
    {
        offensivePlayers.Remove(o);
        if (OnPlayerExitTrigger != null) OnPlayerExitTrigger.Invoke(o); 
    }
}
