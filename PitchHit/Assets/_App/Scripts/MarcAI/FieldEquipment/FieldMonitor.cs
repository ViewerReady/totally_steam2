﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;


public enum UmpireCall
{
    NONE = -1,
    STRIKE,
    BALL,
    WILD,
    OUT,
    FOUL,
    HOMERUN,
    FAIR,
}

/// <summary>
/// This class contains information about the field that AI coordinators can reference
/// </summary>
public class FieldMonitor : MonoBehaviour
{

    private static FieldMonitor m_Instance;
    public static FieldMonitor Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<FieldMonitor>();
            }
            return m_Instance;
        }

        private set
        {
            m_Instance = value;
        }
    }

    [SerializeField]
    protected FieldPitchersMound pitchersMound;
    //public FieldTransitionEffectManager transition;
    [SerializeField]
    public BallGate strikeGate;
    public void ShowHideStrikeGate(bool show) {
        strikeGate.ShowHide_Box(show);
    }
    public void ShowStrikeGateForSeconds(float seconds)
    {
        StopCoroutine("ShowStrikeGateForSecondsRoutine");
        StartCoroutine("ShowStrikeGateForSecondsRoutine",seconds);
    }
    IEnumerator ShowStrikeGateForSecondsRoutine(float seconds)
    {
        ShowHideStrikeGate(true);
        yield return new WaitForSeconds(seconds);
        ShowHideStrikeGate(false);
    }
    public void ShowHideStrikeMarker(bool show)
    {
        strikeGate.ShowHide_Marker(show);
    }

    public void SetStrikeGateDimensions(Vector2 size, Vector2 badPitchSpacer, Vector2 batPitchMargin)
    {
        strikeGate.size = size;
        strikeGate.badPitchSpacer = badPitchSpacer;
        strikeGate.badPitchMargin = batPitchMargin;
    }
    public FieldBase basePrefab;
    public Transform[] basePlacements;
    public Transform[] fenceCorners;//make sure homeplate is first and that they are arranged in a circle;
    private Vector2[] polyPoints;


    public int numBases
    {
        get
        {
            if (basePlacements == null) return 0;
            return basePlacements.Length;
        }
    }

    public Transform homeDugout;
    public Transform awayDugout;

    public static Action<hittable, UmpireCall> PitchJudgement;
    public static Action<hittable, UmpireCall, bool> HitBallJudgement;

    public static Action<hittable> OnBatHit;
    public static Action<hittable> OnBallPitched;

    public static Action<OffensePlayer> OnRegisterRunner;
    public static Action<OffensePlayer> OnUnRegisterRunner;
    public static Action<DefensePlayer> OnRegisterDefensePlayer;
    public static Action<DefensePlayer> OnUnRegisterDefensePlayer;
    public static Action<Transform> OnBallCreated;
    public static Action OnBallDestroyed;

    protected FieldBase[] baseObjects;

    public hittable ballPrefab;

   

    public struct RankedBase
    {
        public int baseNo;
        public float value;
        public RankedBase(int b, float v)
        {
            baseNo = b; value = v;
        }
    }

    public struct RankedPosition
    {
        public Vector3 position;
        public int index;
        public float value;
        public RankedPosition(Vector3 p, float v, int i = 0)
        {
            position = p; value = v; index = i;
        }
    }


    void OnEnable()
    {
        BatController.onBatHit.AddListener(AnnounceBallHit);
        BatController.onSwing+= RecordSwing;
        BatterPlayer.OnSwing += RecordSwing;
    }

    void OnDisable()
    {
        BatController.onBatHit.RemoveListener(AnnounceBallHit);
        BatController.onSwing-= RecordSwing;
        BatterPlayer.OnSwing -= RecordSwing;
    }

    void Awake()
    {
        if (Instance == null || Instance == this)
        {
            Instance = this;
        }
        else
        {
            GameObject.Destroy(this);
        }
    }

    void Start()
    {
        this.transform.localPosition = Vector3.zero;

        if (baseObjects == null) CreateFieldBases();
        SetupInsideOutsideFieldDetection();
    }

    

    void LateUpdate()
    {
        UpdateStateModel();
    }

#region State Model Management
    [System.Serializable]
    public struct FieldState
    {
        public FieldState(float[] runners, bool[] basesOcc, bool[] basesDef, bool defReady, bool offready)
        {
            runnerTimeToBase = runners;
            baseOccupied = basesOcc;
            baseDefended = basesDef;
            defenseReady = defReady;
            offenseReady = offready;
        }
        public float[] runnerTimeToBase { get; private set; }
        public bool[] baseOccupied { get; private set; }
        public bool[] baseDefended { get; private set; }


        public bool offenseReady;
        public bool defenseReady;
    }
    public FieldState fieldState { get; protected set; }


    void UpdateStateModel()
    {
        bool[] baseOccupation = new bool[numBases];
        float[] runnerTimes = new float[numBases];
        bool[] baseDefense = new bool[numBases];
        for (int i = 0; i < numBases; i++)
        {
            baseOccupation[i] = GetBase(i).occupiedByRunner;
            runnerTimes[i] = -1;
        }
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state != AIFieldPlayer.FieldPlayerAction.RUN) continue;

            int advanceBase = runner.baseLastTouched + 1;
            float t_Advance = runner.GetTimeToRunToBase(advanceBase);

            if (runnerTimes[advanceBase] < 0 || runnerTimes[advanceBase] > t_Advance)
                runnerTimes[advanceBase] = t_Advance;
            if (!runner.forcedRun)
            {
                int retreatBase = runner.baseLastTouched;
                float t_Retreat = runner.GetTimeToRunToBase(retreatBase);
                if (runnerTimes[retreatBase] < 0 || runnerTimes[retreatBase] > t_Retreat)
                    runnerTimes[retreatBase] = t_Retreat;
            }
        }

        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.defendedWithBall)
            {
                baseDefense[fBase.baseNum] = true;
            }
        }

        fieldState = new FieldState(
            runnerTimes,
            baseOccupation,
            baseDefense,
            AllDefendersAtPitchPosition(),
            AllRunnersOnBase()
            );

    }
#endregion



#region Base management
    /// <summary>
    /// Gets a base position in local space.
    /// </summary>
    /// <param name="baseNo">first base is 0, second is 1, third is 2, home is 3</param>
    /// <returns>Local-space position for the given base</returns>
    public Vector3 GetBasePosition(int baseNo)
    {
        if (baseNo >= basePlacements.Length || baseNo < 0) baseNo = numBases - 1;

        if (basePlacements[baseNo] == null)
        {
            Debug.LogWarning("Could not find base " + baseNo);
            return Vector3.zero;
        }
        //Debug.Log("get base position for base "+baseNo+" at "+basePlacements[baseNo].position);
        return basePlacements[baseNo].localPosition;
    }

    public Vector3 GetBaseDefensePosition(int baseNo)
    {
        if (baseNo >= basePlacements.Length || baseNo < 0) baseNo = numBases - 1;

        if (basePlacements[baseNo] == null)
        {
            Debug.LogWarning("Could not find base " + baseNo);
            return Vector3.zero;
        }
        //Debug.Log("get base position for base "+baseNo+" at "+basePlacements[baseNo].position);
        Transform baseTransform = basePlacements[baseNo];
        Vector3 offset = Vector3.Lerp(baseTransform.forward, -baseTransform.right, .5f) * 1.5f;

        if (baseNo == numBases - 1) offset = Vector3.back * 1.5f;

        return baseTransform.localPosition + offset;
    }

    public Vector3 GetHomePlatePosition()
    {

        return GetBasePosition(numBases - 1);
    }

    public Vector3 GetHumanBatterPosition()
    {
        Vector3 batterOffset = Vector3.zero;
        switch (HandManager.dominantHand)
        {
            case HandManager.HandType.left:
                batterOffset = Vector3.right;
                break;
            case HandManager.HandType.right:
                batterOffset = Vector3.left;
                break;
            case HandManager.HandType.both:
                break;
        }
        return GetHomePlatePosition() + batterOffset;
    }

    public FieldBase GetHomePlate()
    {
        return GetBase(numBases - 1);
    }

    public FieldBase GetBase(int i)
    {
        if (baseObjects == null) CreateFieldBases();
        if (i < 0 || i >= numBases) return null;
        return baseObjects[i];
    }

    public FieldBase[] GetBases()
    {
        return baseObjects;
    }

    public Transform[] GetBaseTransforms()
    {
        return baseObjectTransforms;
    }

    Transform[] baseDefenseTransforms;
    public Transform[] GetBaseDefensePositions()
    {
        if (baseDefenseTransforms == null)
        {
            baseDefenseTransforms = new Transform[numBases];
            for (int i = 0; i < numBases; i++)
            {
                GameObject def = new GameObject("Base_" + i + "_defense");
                def.transform.SetParent(GetBase(i).transform);
                def.transform.position = GetBaseDefensePosition(i);
                baseDefenseTransforms[i] = def.transform;
            }
        }
        return baseDefenseTransforms;
    }

    Vector3[] basePositions;
    public Vector3[] GetBasePositions()
    {
        if (basePositions == null)
        {
            basePositions = new Vector3[numBases];
            for (int i = 0; i < numBases; i++)
            {
                basePositions[i] = GetBasePosition(i);
            }
        }
        return basePositions;
    }

    public Vector3 GetPitchersMoundPosition()
    {
        return pitchersMound.transform.localPosition;
    }

    public Vector3 GetPitchTarget(float strikeChance)
    {
        strikeChance = Mathf.Clamp01(strikeChance);
        bool isStrike = UnityEngine.Random.Range(0f, 1f) < strikeChance;
        Vector3 strikePos = isStrike ? strikeGate.GetValidStrikePosition() : strikeGate.GetInvalidStrikePosition();
        return strikePos;
    }

    private Transform[] baseObjectTransforms;
    /// <summary>
    /// Instantiates FieldBase objects where base markers were placed
    /// </summary>
    void CreateFieldBases()
    {
        baseObjects = new FieldBase[numBases];
        baseObjectTransforms = new Transform[numBases];
        for (int i = 0; i < numBases; i++)
        {
            if (basePlacements[i] != null)
            {
                FieldBase newBase = GameObject.Instantiate(basePrefab, basePlacements[i].position, basePlacements[i].rotation, this.transform.parent);


                //newBase.GetComponent<Collider> ().enabled = true;
                newBase.SetBaseNumber(i, (i == numBases - 1));
                newBase.OnRunnerTouchBase += RecordRunnerOnBase;
                baseObjects[i] = newBase;
                baseObjectTransforms[i] = newBase.transform;
            }
        }
    }

    void SetupInsideOutsideFieldDetection()
    {
        polyPoints = new Vector2[fenceCorners.Length];
        for (int c = 0; c < fenceCorners.Length; c++)
        {
            polyPoints[c] = new Vector2(fenceCorners[c].position.x, fenceCorners[c].position.z);
        }
    }



    void RecordRunnerOnBase(int baseNo, FieldBase fBase, OffensePlayer runner)
    {
        if (fBase.defendedWithBall) runner.NotifyOfOut(OutType.DEFENDED);
        else if (baseNo == numBases - 1 && runner.baseTarget_Immediate == baseNo) runner.NotifyOfRunCompletion();
    }


    #endregion

    public static hittable ballInPlay
    {
        get { return m_ballInPlay; }
        private set { m_ballInPlay = value; }
    }
    protected static hittable m_ballInPlay;


    protected void BroadcastHitBallJudgement(hittable ball, UmpireCall judgement, bool conclusive)
    {
        if (HitBallJudgement != null) HitBallJudgement.Invoke(ball, judgement, conclusive);
    }

    protected void BroadcastPitchJudgement(hittable ball, UmpireCall judgement)
    {
        //Debug.Log("pitch judgement " + judgement);
        if (PitchJudgement != null) PitchJudgement.Invoke(ball, judgement);
    }

    protected void BroadcastPitch(hittable ball)
    {
        if (OnBallPitched != null) OnBallPitched.Invoke(ball);
    }



    /// <summary>
    /// Waits before announcing new ball to AI Coordinators to give ball velocity a few frames to settle.
    /// Also checks for home runs and foul balls and informs AI Coordinators
    /// </summary>
    /// <param name="ball"> new hittable in play</param>
    /// <param name="delay"> how long to wait before firing OnBallHit event</param>
    /// <returns></returns>
    IEnumerator AnnounceBallAfterFixedUpdate(hittable ball)
    {
//        Debug.Log("========================================= announce ball after delay.");
        yield return new WaitForFixedUpdate();
        ball.hasEverHitGround = false;

        bool ballCanBeJudged = false;
        UmpireCall ballJudgement = UmpireCall.FAIR;
        float timeAtHit = Time.time;


        //if the ball is obviously going to be foul, call it out
        TrajectoryInfo trajectory = ball.GetTrajectory();
        Vector3 landingPos = trajectory.groundHitPosition;
        if (IsPointFoulBall(landingPos))
        {
            ballJudgement = UmpireCall.FOUL;
            ballCanBeJudged = true;
        }

        //if the ball wasn't immediately judged to be a foul, declare a fair play. It can still be judged foul or homerun later in this coroutine
        if (true) //!ballCanBeJudged)
        {
            BroadcastHitBallJudgement(ball, ballJudgement, false);
            //if (OnFieldEvent != null) OnFieldEvent.Invoke(ball, FieldPlayType.STANDARD);
        }

        while (ball != null && !ballCanBeJudged)
        {
            Vector3 ballGroundPos = ball.transform.position;
            ballGroundPos.y = 0;
            bool pastFirstThird = (Vector3.Distance(ballGroundPos, GetHomePlatePosition()) > Vector3.Distance(GetBasePosition(1), GetHomePlatePosition()));

            //ball is in foul territory, but not necessarily judged conclusively 
            if (IsPointFoulBall(ball.transform.position))
            {
                ballJudgement = UmpireCall.FOUL;
            }
            else
            {
                ballJudgement = UmpireCall.FAIR;
            }

            //ball has touched a defender, can be judged immediately
            if (ball.hasTouchedDefender)
            {
                ballCanBeJudged = true;
            }

            //ball has passed first/third base, can be judged when hit ground
            if (pastFirstThird )
            {
                if (ball.hitGround)
                {
                    ballCanBeJudged = true;
                }
            }
            //ball has not passed first/third, cannot be judged until it comes to a stop
            else
            {
                if(ball.GetVelocity().sqrMagnitude < .01f)
                {
                    ballCanBeJudged = true;
                }
            }

            //ball was caught without hitting ground, can be judged fair
            if (!ball.hitGround && ball.hasBeenCaught)
            {
                ballJudgement = UmpireCall.FAIR;
                ballCanBeJudged = true;
            }

            if (IsHomeRun(ball.transform.position))
            {
                ballJudgement = UmpireCall.HOMERUN;
                ballCanBeJudged = true;
            }

            yield return null;
            if (Time.time - timeAtHit > 20)
            {
                Debug.LogWarning("Ball is taking a long time to be judged, breaking loop");
                break;
            }
        }

        yield return null;

        switch (ballJudgement)
        {
            case UmpireCall.FAIR:
                break;
            case UmpireCall.FOUL:
                BroadcastHitBallJudgement(ball, UmpireCall.FOUL, true);
                break;
            case UmpireCall.HOMERUN:
                BroadcastHitBallJudgement(ball, UmpireCall.HOMERUN, true);
                break;
        }
    }

    public void AnnouncePitch()
    {
        //Debug.Log("ANNOUNCE PITCH");
        m_ballInPlay.ResetFlags();
        strikeGate.Reset();
        swingFlag = false;

        //when do we need to evaluate the pitch?
        //caught by catcher
        //hit ground
        //left field
        //explodes
        //hit by bat


        m_ballInPlay.OnHitGround += JudgePitch;
        m_ballInPlay.OnCaughtByDefender += JudgePitch;
        //m_ballInPlay.OnCaughtByCatcher += JudgePitch;
        m_ballInPlay.OnBatHit += JudgePitch;
        m_ballInPlay.OnBallDestroy += JudgePitch;

        BroadcastPitch(m_ballInPlay);

        trailParticleManager tempTrail = m_ballInPlay.GetComponent<trailParticleManager>();
        if (tempTrail)
        {
            tempTrail.OnHit();
        }
    }

    void JudgePitch(hittable pitch)
    {
        //if ball never passed

        //if the ball didn't make it halfway to home plate before needing to be judged, just ignore it
        float noPitchThreshold = GetPitchersMoundPosition().z / 2f;
        if(pitch.transform.position.z > noPitchThreshold)
        {
            BroadcastPitchJudgement(pitch, UmpireCall.NONE);
            return;
        }

        bool strike = false;
        bool ball = false;
        //Debug.Log("JUDGING PITCH");
        //Debug.Log("CHECK FOR STRIKE " + defense.humanControlled + " " + strikeGate.IsPassed + "  " + strikeGate.strikeDetected);

        if (pitch.GetWasHitByBat())
        {
            //Debug.Log("JUDGED AS HIT BY BAT");
            // the ball was hit, no need to judge the pitch
        }
        else if (strikeGate.strikeDetected)
        {
            //ball was not hit, and passed through the strike gate, this is a strike
            //Debug.Log("JUDGED AS STRIKE DETECTED BY STRIKEGATE");
            strike = true;
        }
        else if (SwingDetected())
        {
            //ball was not hit and did not pass through strike gate, but a swing was detected, this is a strike
            //Debug.Log("JUDGED AS SWING DETECTED");
            strike = true;
        }
        else
        {
            // the ball did not pass the gate, was not hit, and the batter did not swing, this is a ball
            //Debug.Log("JUDGED AS A BALL");
            ball = true;
        }

        if (strike)
        {
            BroadcastPitchJudgement(pitch, UmpireCall.STRIKE);

        }
        else if (ball)
        {
            BroadcastPitchJudgement(pitch, UmpireCall.BALL);
        }


        pitch.OnHitGround -= JudgePitch;
        pitch.OnCaughtByDefender -= JudgePitch;
        //pitch.OnCaughtByCatcher -= JudgePitch;
        pitch.OnBatHit -= JudgePitch;
        pitch.OnBallDestroy -= JudgePitch;

        //Debug.LogError("Strike  gate: " + strikeGate.strikeDetected + "  swing: " + SwingDetected());
    }

    /*
    void CancelPitch(hittable pitch)
    {
        //Debug.Log("CANCELING PITCH");
        pitch.OnHitGround -= JudgePitch;
        pitch.OnCaughtByDefender -= JudgePitch;
        pitch.OnCaughtByCatcher -= JudgePitch;
        pitch.OnBatHit -= JudgePitch;
        pitch.OnBallDestroy -= JudgePitch;
    }
    */

    void AnnounceBallHit(hittable ball)
    {
        //Debug.Log("BALL HIT " + ball);
        if (ball == null) return;
        m_ballInPlay = ball;
        ball.OnBallDestroy += RemoveBallFromPlay;
        StartCoroutine(AnnounceBallAfterFixedUpdate(ball));
    }

    #region Ball Management

    public hittable SpawnFreshBall(bool destroyExisting = true)
    {
        hittable ball = GameObject.Instantiate(ballPrefab);
        if(destroyExisting) DestroyBallInPlay();
        m_ballInPlay = ball;
        ball.OnBallDestroy += RemoveBallFromPlay;
        ball.GetComponent<SphereCollider>().enabled = true;
        if (OnBallCreated != null) OnBallCreated(ball.transform);
        return ball;
    }

    

    
    void RemoveBallFromPlay(hittable ball)
    {
        if (ball == null) return;
        ball.OnBallDestroy -= RemoveBallFromPlay;
        if (m_ballInPlay == ball) m_ballInPlay = null;
        if(OnBallDestroyed != null) OnBallDestroyed();
    }

    void DestroyBallInPlay()
    {
        if (m_ballInPlay != null)
        {
            if (OnBallDestroyed != null) OnBallDestroyed();
            GameObject.Destroy(m_ballInPlay.gameObject);
        }
    }

    public void ForceDestroyBallInPlay()
    {
        //Debug.Log("ForceDestroyBallInPlay");
        DestroyBallInPlay();
    }

    public void ExplodeBall(float delay = 0)
    {
        Debug.Log("explode ball  " + delay);
        StopCoroutine("DetonateBallRoutine");
        // Debug.Log("Explode Ball.");
        if (delay > 0)
            StartCoroutine("DetonateBallRoutine",delay);
        else
            ExplodeBallImmediate();
    }

    private void ExplodeBallImmediate()
    {
        //Debug.Log("ExplodeBallImmediate.");
        if (m_ballInPlay != null)
        {
            DarkTonic.MasterAudio.MasterAudio.PlaySound("ball_explode");
            OneShotEffectManager.SpawnEffect("explosion", m_ballInPlay.transform.position, Quaternion.identity, 3f);
            ForceDestroyBallInPlay();
        }
    }

    private IEnumerator DetonateBallRoutine(float delay)
    {
        float t = 0;
        while (t < delay && m_ballInPlay != null && !(m_ballInPlay.hasBeenCaught || m_ballInPlay.isHeld))
        {
            t += Time.deltaTime;
            //            Debug.Log("explode routine " + t + " / " + delay);
            yield return null;
        }

        if (t >= delay) { 
            ExplodeBallImmediate();
        }
    }

    #endregion

    #region State Detection

    private bool swingFlag;

    private void RecordSwing()
    {
        swingFlag = true;
    }

    public bool SwingDetected()
    {
        return swingFlag;
    }

    public bool PitcherOnMoundWithBall()
    {
        if (pitchersMound == null)
        {
            return false;
        }
        return pitchersMound.PitcherOnMoundWithBall;

    }

    public bool PitcherOnMound()
    {
        if (pitchersMound == null)
        {
            return false;
        }
        return pitchersMound.PitcherOnMound;

    }

    public bool BallIsHeld()
    {
        if (!m_ballInPlay) return false;
        return m_ballInPlay.isHeld;
    }

    public bool BallHasLeftPark()
    {
        if (m_ballInPlay)
        {
            return ((IsOutsideFence(m_ballInPlay.transform.position)));
        }
        return false;
    }

    public bool IsPointFoulBall(Vector3 point)
    {
        Vector3 homePlatePoint = GetHomePlatePosition();
        homePlatePoint.y = 0;
        Vector3 thirdBasePoint = GetBasePosition(2);
        thirdBasePoint.y = 0;
        Vector3 firstBasePoint = GetBasePosition(0);
        firstBasePoint.y = 0;

        point.y = 0;

        float angleLeft = Vector3.Angle(thirdBasePoint - homePlatePoint, point - homePlatePoint);
        float angleRight = Vector3.Angle(firstBasePoint - homePlatePoint, point - homePlatePoint);
        float angleBoth = Vector3.Angle(thirdBasePoint - homePlatePoint, firstBasePoint - homePlatePoint);
        return Mathf.Abs(angleBoth - (angleLeft + angleRight)) > .1f;
    }

    public bool IsHomeRun(Vector3 point)
    {
        if (IsPointFoulBall(point))
        {
            return false;
        }
        if (FieldContainsPoint(point))
        {
            return false;
        }
        return true;
    }

    public bool IsOutsideFence(Vector3 point)
    {
        if (FieldContainsPoint(point))
        {
            return false;
        }
        return true;
    }

    protected bool FieldContainsPoint(Vector3 s)
    {
        Vector2 p = new Vector2(s.x, s.z);

        int j = polyPoints.Length - 1;
        bool inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            if (((polyPoints[i].y <= p.y && p.y < polyPoints[j].y) ||
                    (polyPoints[j].y <= p.y && p.y < polyPoints[i].y)) &&
             (p.x < (polyPoints[j].x - polyPoints[i].x) * (p.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x))
                inside = !inside;
        }
        return inside;
    }

    #endregion

    #region AI Player Tracking

    public static LinkedList<OffensePlayer> offensivePlayers
    {
        get
        {
            if (m_offensivePlayers == null) m_offensivePlayers = new LinkedList<OffensePlayer>();
            return m_offensivePlayers;
        }
        protected set
        {
            m_offensivePlayers = value;
        }
    }
    private static LinkedList<OffensePlayer> m_offensivePlayers;

    public static List<DefensePlayer> defensivePlayers
    {
        get
        {
            if (m_defensivePlayers == null) m_defensivePlayers = new List<DefensePlayer>();
            else m_defensivePlayers.RemoveAll(item => item == null);
            return m_defensivePlayers;
        }
        protected set
        {
            m_defensivePlayers = value;
        }
    }
    private static List<DefensePlayer> m_defensivePlayers;


    public static void RegisterRunner(OffensePlayer fielder, int startBase = -1)
    {
        if (!offensivePlayers.Contains(fielder))
        {
            bool foundSpot = false;
            if (offensivePlayers.Count == 0)
            {
                offensivePlayers.AddFirst(fielder);
                foundSpot = true;
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                return;
            }

            LinkedListNode<OffensePlayer> cur = offensivePlayers.First;
            if (cur.Value.baseLastTouched >= startBase)
            {
                offensivePlayers.AddFirst(fielder);
                foundSpot = true;
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                return;
            }
            while (cur.Next != null && !foundSpot)
            {
                int curBase = cur.Value.baseLastTouched;
                int nextBase = cur.Next.Value.baseLastTouched;
                if (startBase >= curBase && startBase <= nextBase)
                {
                    offensivePlayers.AddAfter(cur, fielder);
                    foundSpot = true;
                    if (OnRegisterRunner != null) OnRegisterRunner(fielder);
                }
                cur = cur.Next;
            }
            if (!foundSpot)
            {
                offensivePlayers.AddLast(fielder);
                if (OnRegisterRunner != null) OnRegisterRunner(fielder);
            }
        }
    }

    public static void UnRegisterRunner(OffensePlayer fielder)
    {
        if (offensivePlayers.Contains(fielder))
        {
            if(OnUnRegisterRunner != null) OnUnRegisterRunner(fielder);
            offensivePlayers.Remove(fielder);
        }
    }

    public static void RegisterDefensePlayer(DefensePlayer fielder)
    {
        //Debug.LogError("About to register defense player.");
        //if (fielder == null) Debug.LogError("We tried to add a null fielder to the defensive players list??");
        if (!defensivePlayers.Contains(fielder))
        {
            //Debug.Log("Adding player " + fielder.gameObject.name + " to the list of defensive players.");
            defensivePlayers.Add(fielder);
            if (OnRegisterDefensePlayer != null) OnRegisterDefensePlayer(fielder);
        }
        else
        {
            //Debug.Log("Tried to add player " + fielder.gameObject.name + " to the list of defensive players, but they were already in there.");
        }
    }

    public static void UnRegisterDefensePlayer(DefensePlayer fielder)
    {
        //Debug.LogError("About to unregister defense player.");
        if (defensivePlayers.Contains(fielder))
        {
            //Debug.LogError("Removing player " + fielder.gameObject.name + " from the list of defense players.");
            defensivePlayers.Remove(fielder);
            if(OnUnRegisterDefensePlayer != null) OnUnRegisterDefensePlayer(fielder);
        }
    }

    public OffensePlayer RunnerAheadOf(OffensePlayer runner)
    {
        LinkedListNode<OffensePlayer> node = offensivePlayers.Find(runner);
        if (node == null) return null;
        if (node.Next == null) return null;
        return node.Next.Value;
    }

    public OffensePlayer RunnerBehind(OffensePlayer runner)
    {
        LinkedListNode<OffensePlayer> node = offensivePlayers.Find(runner);
        if (node == null) return null;
        if (node.Previous == null) return null;
        return node.Previous.Value;
    }

    public bool AllDefendersAtPitchPosition()
    {
        foreach (DefensePlayer fielder in defensivePlayers)
        {
            if (!fielder.atPitchPosition && !fielder.isTetheredToCamera)
            {
                return false;
            }
        }
        return true;
    }

    public bool AllRunnersOnBase()
    {
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue; //Don't count players that are out
            if (runner.state != AIFieldPlayer.FieldPlayerAction.BASE)
            {
                return false;
            }
        }
        return true;
    }

    public bool RunnerIsTargetingBase(int baseNo)
    {
        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (runner.state == AIFieldPlayer.FieldPlayerAction.OUT) continue;
            if (runner.baseTarget_Immediate == baseNo && runner.state == AIFieldPlayer.FieldPlayerAction.RUN)

            {
                return true;
            }
        }
        return false;
    }

    #endregion

    #region Bases
    public int MaxForcedBase()
    {
        int maxForcedBase = 0;
        for (int i = 0; i < fieldState.baseOccupied.Length; i++)
        {
            if (fieldState.baseOccupied[i])
            {
                maxForcedBase = i + 1;
            }
            else
            {
                break;
            }
        }
        return maxForcedBase;
    }

    public bool BaseIsDefended(int baseNo)
    {
        if (baseNo < 0 || baseNo >= fieldState.baseDefended.Length) return false;
        return fieldState.baseDefended[baseNo];
    }

    public bool RunnerIsOnBase(OffensePlayer runner, out int baseNo)
    {
        baseNo = -1;
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.offensivePlayers.Contains(runner))
            {
                baseNo = fBase.baseNum;
                return true;
            }

        }
        return false;
    }

    public bool RunnerIsOnBase(OffensePlayer runner)
    {
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.offensivePlayers.Contains(runner))
            {
                return true;
            }
        }
        return false;
    }

    public bool BaseIsOccupiedByRunner(int baseNo)
    {
        return GetBase(baseNo).occupiedByRunner;
    }

    public bool DefenderIsOnBase(DefensePlayer player, out int baseNo)
    {
        baseNo = -1;
        foreach (FieldBase fBase in baseObjects)
        {
            if (fBase.defensivePlayers.Contains(player))
            {
                baseNo = fBase.baseNum;
                return true;
            }

        }
        return false;
    }
    #endregion

    #region Estimation

    /// <summary>
    /// If all runners were to run to their current target base, then continue forward, what is the soonest a runner would arrive at this base?
    /// if there are no runners, returns float.MaxValue
    /// </summary>
    /// <param name="baseNo"></param>
    /// <returns></returns>
    public float GetTimeToRunnerOnBase(int baseNo, out bool forced)
    {
        forced = false;
        float minTime = float.MaxValue;
        // Debug.Log("GET TIME TO RUNNER ON BASE "+offensivePlayers.Count);
        foreach (OffensePlayer runner in offensivePlayers)
        {
            //if isBeyondBase, assume they will never run back to it
            bool isBeyondBase = runner.baseLastTouched > baseNo;
            if (isBeyondBase) continue;

            bool isForced = runner.forcedRun && baseNo <= runner.baseTarget_Eventual;

            float estRunTime = 0;


            //get the time it will take them to get to their current target

            //if this player is being forced, take time to get to forced base into account
            if (isForced)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Immediate, runner.baseTarget_Eventual);
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Eventual, baseNo);
            }

            //if the base in question is farther than the runners target
            else if (baseNo > runner.baseTarget_Immediate)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));
                estRunTime += runner.GetTimeToRunBetweenBases(runner.baseTarget_Immediate, baseNo);

            }

            //if the base in question is the runner's target
            else if (baseNo == runner.baseTarget_Immediate)
            {
                estRunTime += runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));
            }


            if (estRunTime < minTime)
            {
                minTime = estRunTime;
                forced = isForced;
            }
        }
        //Debug.Log("GOT TIME TO RUNNER ON BASE " + minTime);

        return minTime;
    }



    private bool FindTimesToIntercept(Vector3 targetPosition, Vector3 targetVelocity, Vector3 interceptorPosition, float interceptorSpeed, out float t1, out float t2, float targetRadius = 0, float interceptorRadius = 0)
    {
        // |a|^2 = |b|^2 + |c|^2 -2|b||c| cosA
        //where
        //a is interceptor to interceptionPoint
        //b is target to interceptionPiont
        //c is target to interceptor
        //A is angle between b and c
        /*
                  interception
                /\
               /  \
        a     /    \  b
             /      \
            /_______A\ target
                 c
        */

        targetPosition.y = 0;// WorldScaler.elevation;
        Vector3 targetToInterceptor = interceptorPosition - targetPosition;
        targetToInterceptor.y = 0;// WorldScaler.elevation;

        //Debug.DrawLine(outfielderPosition, ballPosGround, Color.yellow, 5);
        targetVelocity.y = 0;// WorldScaler.elevation;

        float targetSpeed = targetVelocity.magnitude;
        float A = Vector3.Angle(targetToInterceptor, targetVelocity);

        //q is -2 * c * cosA
        //p is c ^2

        float targetToInterceptorMag_adjusted = targetToInterceptor.magnitude - targetRadius - interceptorRadius;

        float q = -2f * targetToInterceptor.magnitude * Mathf.Cos(Mathf.Deg2Rad * A);
        float p = targetToInterceptorMag_adjusted * targetToInterceptorMag_adjusted;

        //replace b with t * targetSpeed v
        //replace a with t * interceptorSpeed s

        //0 = (v ^ 2 - s ^2) t ^2 + (q x v)t + p
        //where
        //v is magnitude of targetVelocity
        //s is interceptor speed
        //t is time to intercept

        float a = (targetSpeed * targetSpeed - interceptorSpeed * interceptorSpeed);
        float b = q * targetSpeed;
        float c = p;

        t1 = 0;
        t2 = 0;

        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (MathUtility.SolveQuadratic(a, b, c, out t1, out t2))
        {
            return true;
        }
        return false;
    }

    public AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> FindPassingInterceptionPoint(DefensePlayer from, DefensePlayer to)
    {
        AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception = new AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>(to, float.MaxValue, Vector3.zero);
        interception.fieldPlayer = to;

        Vector3 targetVelocity = (to.moveTargetPosition - to.position).normalized * to.runSpeed;
        targetVelocity.y = 0;


        float t1, t2;
        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(to.position, targetVelocity, from.position, from.throwSpeed, out t1, out t2))
        {
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);
            interception.value = t_firstPathIntercept;
            interception.position = to.position + targetVelocity * t_firstPathIntercept;

        }

        else
        {
            interception.value = float.MaxValue;
        }

        return interception;
    }

    public List<AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>> RunnerInterceptionEstimates(DefensePlayer ballHolder)
    {
        List<AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>> interceptionEstimates = new List<AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>>();

        foreach (OffensePlayer runner in offensivePlayers)
        {
            if (FieldMonitor.Instance.RunnerIsOnBase(runner)) continue;
            AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> interception = EstimatedInterceptionOfRunner(ballHolder, runner);
            //Debug.Log("interception estimate " + runner.name + " " + interception.value);

            //interception.value += tolerance;
            if (interception.value >= 0)
            {
                interceptionEstimates.Add(interception);
            }
        }

        interceptionEstimates = interceptionEstimates.OrderBy(x => x.value).ToList();

        return interceptionEstimates;
    }


    /// <summary>
    /// Calculate expected interception of a Runner by a Defender
    /// if interception is not possible, estimation value will be float.Max
    /// </summary>
    /// <param name="defender"></param>
    /// <param name="runner"></param>
    /// <returns></returns>
    public AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> EstimatedInterceptionOfRunner(DefensePlayer defender, OffensePlayer runner)
    {
        AIFieldCoordinator.RankedFieldPlayer<OffensePlayer> interception = new AIFieldCoordinator.RankedFieldPlayer<OffensePlayer>(runner, float.MaxValue, Vector3.zero);

        Vector3 runnerVelocity = (GetBasePosition(runner.baseTarget_Immediate) - runner.position).normalized * runner.runSpeed;
        runnerVelocity.y = 0;// WorldScaler.elevation;

        float t1, t2;
        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(runner.position, runnerVelocity, defender.position, defender.runSpeed, out t1, out t2))
        {
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);

            float timeToBase = runner.GetTimeToRunToPosition(GetBasePosition(runner.baseTarget_Immediate));

            //will not be able to intercept the runner before they reach their target base
            if (t_firstPathIntercept > timeToBase)
            {
                interception.value = float.MaxValue;
            }
            else
            {
                interception.value = t_firstPathIntercept;
                interception.position = runner.position + runnerVelocity * t_firstPathIntercept;
            }

        }
        else
        {
            interception.value = float.MaxValue;
        }
        return interception;
    }

    public List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> GroundInterceptionEstimates(hittable ball)
    {
        List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> fastestGroundChasers = new List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>>();
        if (ball == null)
        {
            Debug.Log("no ball to judge groundInterception estimates with...");
            return fastestGroundChasers;
        }

        TrajectoryInfo info = ball.GetTrajectory();

        foreach (DefensePlayer outfielder in defensivePlayers)
        {
            float t = EstimatedTimeToInterceptGroundBall(info, outfielder);
            Vector3 pos_at_t = info.Position_at_T(t, Space.Self);
            pos_at_t.y = 0;// WorldScaler.elevation;
            fastestGroundChasers.Add(new AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>(outfielder, t, pos_at_t));
        }
        fastestGroundChasers = fastestGroundChasers.OrderBy(x => x.value).ToList();
        /*
        Debug.Log("=================================Finished Estimating ground chasers");
        foreach(AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer r in fastestGroundChasers)
        {
            Debug.Log("player:"+r.fieldPlayer.name+" "+r.value);

        }
        */
        return fastestGroundChasers;
    }

    /// <summary>
    /// Calculates an estimated time to intercept a ground ball
    /// </summary>
    /// <param name="trajectory"></param>
    /// <param name="outfielder"></param>
    /// <returns></returns>
    public float EstimatedTimeToInterceptGroundBall(TrajectoryInfo trajectory, DefensePlayer outfielder)
    {
        Vector3 targetPosition = EstimatedGroundInterceptionPoint(trajectory.Position_at_T(0, Space.Self), trajectory.Velocity_at_T(0, Space.Self), outfielder);
        float t_intercept = outfielder.GetTimeToRunToPosition(targetPosition);
        outfielder.SetDebugText(t_intercept.ToString("0.0"));// t_outfielderAtRendezVous.ToString("0.0") + " + " + t_ballAtRendezVous.ToString("0.0"));

        return t_intercept;



    }


    /// <summary>
    /// Gets estimated intercept point in local space
    /// </summary>
    /// <param name="targetPosition">target position in local space</param>
    /// <param name="targetVelocity">target velocity in local space</param>
    /// <param name="defender"></param>
    /// <returns></returns>
    public Vector3 EstimatedGroundInterceptionPoint(Vector3 targetPosition, Vector3 targetVelocity, DefensePlayer defender)
    {
        Vector3 targetPositionFlat = targetPosition;
        Vector3 targetVelocityFlat = targetVelocity;


        TrajectoryInfo info = new TrajectoryInfo();
        info.SimulatePosition(targetPosition, Space.Self);
        info.SimulateVelocity(targetVelocity, Space.Self);

        if (info.T_EnterDescendingCatchZone(defender.maxVerticalReach) > 0)
        {
            targetPositionFlat = info.groundHitPosition;
            //            Debug.Log("-0----" +targetPositionFlat + "  " + targetVelocityFlat);
            ///targetPositionFlat is getting set to the wrong value in this block

        }

        targetPositionFlat.y = 0;// WorldScaler.elevation;
        targetVelocityFlat.y = 0;// WorldScaler.elevation;
        float distanceToTarget = Vector3.Distance(targetPositionFlat, defender.position);
        Vector3 closestPointOnTargetPath = Vector3.Dot(defender.position - targetPositionFlat, targetVelocityFlat.normalized) * targetVelocityFlat.normalized + targetPositionFlat;
        //DrawDebugLine(defender.position, closestPointOnTargetPath, Color.yellow, Time.deltaTime);

        Vector3 combinedVelocities = targetVelocity + (targetPosition - defender.position).normalized * defender.runSpeed;
        float combo = combinedVelocities.magnitude;

        float T = distanceToTarget / Mathf.Abs(targetVelocityFlat.magnitude + defender.runSpeed);


        Vector3 goalPosition = targetPositionFlat + targetVelocityFlat * T;


        goalPosition.y = 0;// WorldScaler.elevation;
        return goalPosition;
    }

    public List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> AirInterceptionEstimates(hittable ball)
    {
        List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>> fastestCatchers = new List<AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>>();
        if (ball == null) return fastestCatchers;

        TrajectoryInfo info = ball.GetTrajectory();


        foreach (DefensePlayer outfielder in defensivePlayers)
        {
            AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> t_airIntercept;
            if (CanCatchInAir(info, outfielder, out t_airIntercept))
            {
                /*
                Vector3 catchPos = info.Position_at_T(t_airIntercept);
                catchPos.y = 0;// WorldScaler.elevation;
				float t_toCatchPosition = Vector3.Distance(outfielder.position, catchPos) / outfielder.runSpeed;
                fastestCatchers.Add(new AIFieldCoordinator<DefensePlayer>.RankedFieldPlayer(outfielder, t_toCatchPosition, catchPos));
                */
                t_airIntercept.value *= (1 / t_airIntercept.fieldPlayer.GetWeightInDirection(info.currentPosition, t_airIntercept.position));
                fastestCatchers.Add(t_airIntercept);
            }
        }
        fastestCatchers = fastestCatchers
            .OrderBy(x => x.value)
            .ThenBy(x => Vector3.Distance(x.fieldPlayer.position, x.position))
            .ToList();

        foreach (AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> rank in fastestCatchers)
        {
            //            Debug.Log(rank.fieldPlayer + " " + rank.value);
        }
        return fastestCatchers;
    }

    /// <summary>
    /// Will this player be able to catch the ball before it reaches the ground
    /// </summary>
    /// <param name="trajectory"></param>
    /// <param name="fieldPlayer"></param>
    /// <param name="timeToIntercept"></param>
    /// <returns>Outfielder is able to enter ball's path before it touches the ground</returns>
    public bool CanCatchInAir(TrajectoryInfo trajectory, DefensePlayer fieldPlayer, out AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception)
    {

        Vector3 ballPosGround = trajectory.currentPosition;
        ballPosGround.y = 0;

        Vector3 ballVelocity = trajectory.currentVelocity;
        ballVelocity.y = 0;

        bool canCatch = false;
        float t1, t2;
        float timeToIntercept = 0;

        Vector3 catchPosGround = trajectory.currentPosition;
        float t_toCatchPosition = float.MaxValue;

        //Debug.Log(square + " a: " + a + " b: " + b + " c: " +c + " ballSpeed: " + ballSpeed);
        if (FindTimesToIntercept(ballPosGround, ballVelocity, fieldPlayer.position, fieldPlayer.runSpeed, out t1, out t2))
        {
            //I think t2 will always be the answer but whatever. 
            //This will check both halves of the quadratic equation.



            //if the ball is slower than the outfielder, one of the solutions will be negative because the ball would never pass the outfielder 
            if (t1 < 0) t1 = float.MaxValue;
            if (t2 < 0) t2 = float.MaxValue;

            float t_firstPathIntercept = Mathf.Min(t1, t2);
            float t_lastPathIntercept = Mathf.Max(t1, t2);

            Vector3 earlyCatchPoint = trajectory.Position_at_T(t_firstPathIntercept);
            earlyCatchPoint.y = 0;// WorldScaler.elevation;
            Vector3 lastCatchPoint = trajectory.Position_at_T(t_lastPathIntercept);
            lastCatchPoint.y = 0;// WorldScaler.elevation;
            //Debug.DrawLine(outfielderPosition, earlyCatchPoint, Color.magenta, 5);
            //Debug.DrawLine(outfielderPosition, lastCatchPoint, Color.magenta, 5);

            float t_comingDown = trajectory.T_EnterDescendingCatchZone(fieldPlayer.maxVerticalReach);
            float t_goingUp = trajectory.T_ExitAscendingCatchZone(fieldPlayer.maxVerticalReach);
            float t_ground = trajectory.T_Ground;

            //Debug.Log(t_goingUp + "  " + t_comingDown + "  " + t_ground);
            //if it will be possible to catch the ball before it has risen above catchable altitude
            if (t_firstPathIntercept > 0 && t_firstPathIntercept <= t_goingUp)
            {
                //Debug.Log("here1");
                timeToIntercept = t_firstPathIntercept;
                canCatch = true;
            }


            //if ball is too high to catch when this outfielder would be able to intercept its path of travel
            else if (t_firstPathIntercept > t_goingUp && t_lastPathIntercept < t_comingDown)
            {
                //Debug.Log("here2");

                timeToIntercept = 0;
                canCatch = false;
            }



            //if it will be possible to catch the ball coming down, before it hits the ground
            else if (t_firstPathIntercept > t_comingDown && t_firstPathIntercept < t_ground)
            {
                //Debug.Log("here3");

                timeToIntercept = t_firstPathIntercept;
                canCatch = true;
            }

            //if it will be possible to catch the ball the moment it enters catchable altitude
            else if (t_firstPathIntercept < t_comingDown && t_lastPathIntercept > t_comingDown)
            {
                //Debug.Log("here4");

                timeToIntercept = t_comingDown;
                canCatch = true;
            }

            else
            {
                //Debug.Log("here5");

                canCatch = false;

            }

            catchPosGround = trajectory.Position_at_T(timeToIntercept);
            catchPosGround.y = 0;// WorldScaler.elevation;
            t_toCatchPosition = fieldPlayer.GetTimeToRunToPosition(catchPosGround);// Vector3.Distance(outfielder.position, catchPos) / fieldp.runSpeed;

        }

        //won't be in the path of the ball, but may be able to catch it with an outstretched arm
        else
        {
            catchPosGround = MathUtility.FindClosestPointOnLineToPoint(new Ray(ballPosGround, ballVelocity), fieldPlayer.position);
            catchPosGround.y = 0;// WorldScaler.elevation;

            if (Vector3.Dot(ballVelocity, catchPosGround - ballPosGround) < 0)
            {
                canCatch = false;
            }

            else
            {
                t_toCatchPosition = Vector3.Distance(catchPosGround, fieldPlayer.position) / fieldPlayer.runSpeed;
                float t_ballPassingPlayer = Vector3.Distance(ballPosGround, catchPosGround) / ballVelocity.magnitude;
                Vector3 playerChestPosWhenBallPasses = fieldPlayer.position + (catchPosGround - fieldPlayer.position).normalized * t_ballPassingPlayer + Vector3.up * fieldPlayer.chestHeight;

                canCatch = (Vector3.Distance(playerChestPosWhenBallPasses, trajectory.Position_at_T(t_ballPassingPlayer)) <= fieldPlayer.maxHorizontalReach);
            }
        }



        interception = new AIFieldCoordinator.RankedFieldPlayer<DefensePlayer>(fieldPlayer, t_toCatchPosition, catchPosGround);
        //Debug.Log(interception.ToString());

        return canCatch;

    }

    public bool CanCatchInAir(hittable ball, DefensePlayer fieldPlayer, out AIFieldCoordinator.RankedFieldPlayer<DefensePlayer> interception)
    {
        return CanCatchInAir(ball.GetTrajectory(), fieldPlayer, out interception);
    }
    #endregion


    public static void DrawDebugLine(Vector3 p1, Vector3 p2, Color color, float duration = -1f)
    {
        if (duration < 0)
        {
            Debug.DrawLine(p1, p2, color);
        }
        else
        {
            Debug.DrawLine(p1, p2, color, duration);
        }
    }
    
}