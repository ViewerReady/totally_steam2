﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum BaseID
{
    NONE = -1,
    FIRST = 0,
    SECOND = 1,
    THIRD = 2,
    HOME = 3,
}

[RequireComponent(typeof(BoxCollider))]
public class FieldBase : MonoBehaviour {

    protected new BoxCollider collider;
    //public MeshRenderer renderer;
   // public MeshFilter filter;
    //public Mesh homePlateMesh;
    public List<OffensePlayer> offensivePlayers { get; private set; }
    public List<DefensePlayer> defensivePlayers { get; private set;}
    public OffensePlayer runnerWithControl;

    public GameObject homePlate;
    public GameObject squareBase;
    public ParticleSystem safeParticles;
    public ParticleSystem defendedParticles;

    public FieldBaseFence safeFence;
    public FieldBaseFence defendedFence;

    public Vector3 baseColliderSize;
    public Vector3 baseColliderOffset;
    public Vector3 homeColliderSize;
    public Vector3 runThroughColliderSize;
    public Vector3 runThroughColliderOffset;

    public AudioSource soundMaker;
    public AudioClip safeOnBaseSound;

    public Action<int, FieldBase, OffensePlayer> OnRunnerTouchBase;
    public int baseNum;// { get; protected set; }

    bool offenseWalking; //This will get set by on field events. It's so we don't list the base as defended during a walk.
                         //I'm doing this because you were able to get offense players out by running onto the base with a ball while they were walking.

    public bool occupiedByRunner
    {
        get
        {
            return offensivePlayers.Count > 0;
        }
    }
    public bool defendedWithBall
    {
        get
        {
            if (defensivePlayers.Count == 0) return false;
            foreach(DefensePlayer d in defensivePlayers)
            {
                if (d.inPosessionOfBall && !offenseWalking)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public void Awake()
    {
         offensivePlayers = new List<OffensePlayer>();
    defensivePlayers = new List<DefensePlayer>();
        collider = GetComponent<BoxCollider>();
        collider.enabled = false;
}
    void Start()
    {
        ShowHideDefendedIndicator(false);
        ShowHideSafeIndicator(false);
        collider.enabled = true;

        if(FullGameDirector.Instance != null)
        {
            FullGameDirector.Instance.OnFieldEvent += HandleFieldEvent;
        }
    }

    void OnDestroy()
    {
    }

    void SetColliderDimensions(Vector3 colSize, Vector3 colOff)
    {
        if (collider)
        {
            collider.size = colSize;
            collider.center = colOff;
        }
    }

    public void SetBaseNumber(int n, bool isHome = false)
    {
        baseNum = n;
        if (isHome)
        {
            homePlate.SetActive(true);
            squareBase.SetActive(false);
            Vector3 colSize = collider.size;
            colSize.z = 3;
            SetColliderDimensions(homeColliderSize, baseColliderOffset);
        }
        else
        {
            SetColliderDimensions(baseColliderSize, baseColliderOffset);

            homePlate.SetActive(false);
            squareBase.SetActive(true);
        }
    }

    void AddDefensePlayer(DefensePlayer d)
    {
        if (!defensivePlayers.Contains(d))
        {
            d.OnBallPosession += OnDefenderOnBaseCatchesBall;
            defensivePlayers.Add(d);

        }
    }

    void RemoveDefensePlayer(DefensePlayer d)
    {
        if (defensivePlayers.Contains(d))
        {
            defensivePlayers.Remove(d);
            d.OnBallPosession -= OnDefenderOnBaseCatchesBall;
        }
    }

    public void OnDefenderOnBaseCatchesBall(DefensePlayer d)
    {
        UpdateColor();
    }

    void AddOffensePlayer(OffensePlayer o)
    {
        if (o.state == AIFieldPlayer.FieldPlayerAction.OUT) return;

        if (!offensivePlayers.Contains(o))
        {
            if (offensivePlayers.Count > 0)//Greg: There's already someone on this base and no more than one are allowed!
            {
                if(offensivePlayers[0].baseLastTouched < o.baseLastTouched)//Greg: We need a better way of deducing seniority in case both of them touched the same base last (player can stop on/off)
                {
                    o.NotifyOfOut(OutType.FORCED);
                }
                else
                {
                    offensivePlayers[0].NotifyOfOut(OutType.FORCED);
                }
            }

            offensivePlayers.Add(o);
            if (OnRunnerTouchBase != null)
            {
                OnRunnerTouchBase.Invoke(baseNum, this, o);
            }

            o.OnHome += RemoveDestroyedRunner;
            o.OnOut += RemoveOutRunner;

            if((o.baseLastTouched==baseNum || (o.baseLastTouched==baseNum-1)) && !defendedWithBall)
            {
                ShowHideSafeIndicator(true);
            }

            if (o.isTetheredToCamera)
            {
                if (squareBase.activeSelf)
                {
                    PlaySafeOnBaseSound();
                }
            }

            if (o.isTetheredToCamera)
            {
                o.OnHumanBaseEntered(this);
            }

            

        }
        if (baseNum == 0 && offensivePlayers.Count > 0)
        {
            SetColliderDimensions(runThroughColliderSize, runThroughColliderOffset);
        }
    }

    void RemoveOffensePlayer(OffensePlayer o)
    {
        if (offensivePlayers.Contains(o))
        {
            offensivePlayers.Remove(o);
            o.OnHome -= RemoveDestroyedRunner;
            o.OnOut -= RemoveOutRunner;

            ShowHideSafeIndicator(false);


            if (o.isTetheredToCamera)
            {
                o.OnHumanBaseExited(this);
            }

            if (baseNum == 0 && offensivePlayers.Count == 0)
            {
                SetColliderDimensions(baseColliderSize, baseColliderOffset);
            }
        }
    }

    public bool AttemptToTakeControl(OffensePlayer runner)
    {
        if(runnerWithControl == null)
        {
            runnerWithControl = runner;
            return true;
        }

        if(runner == runnerWithControl)
        {
            return true;
        }

        if((runnerWithControl.baseLastTouched<runner.baseLastTouched) && runner.forcedRun)//you can't run back because someone else is coming up to it and they're forced.
        {
            return false;
        }

        if(runner.isTetheredToCamera || (runner.forcedRun && (runner.baseLastTouched<runnerWithControl.baseLastTouched)))
        {
            runnerWithControl.EjectFromBase(this);

            runnerWithControl = runner;
            return true;
        }

        return false;
    }

    public bool RelinquishControl(OffensePlayer runner)
    {
        if(runner == runnerWithControl)
        {
            runnerWithControl = null;
            return true;
        }

        return false;
    }

    void RemoveDestroyedRunner(OffensePlayer runner)
    {
        RemoveOffensePlayer(runner);
    }
    void RemoveOutRunner(OffensePlayer off, OutType outType)
    {
        RemoveOffensePlayer(off);
    }

    void OnTriggerEnter(Collider other)
    {
        DefensePlayer d = other.GetComponent<DefensePlayer>();
        if(d != null)
        {
            AddDefensePlayer(d);
        }

        OffensePlayer o = other.GetComponent<OffensePlayer>();
        if(o!= null)
        {
            AddOffensePlayer(o);

        }
        UpdateColor();
    }

    void OnTriggerExit(Collider other)
    {
        DefensePlayer d = other.GetComponent<DefensePlayer>();
        if (d != null)
        {
            RemoveDefensePlayer(d);
        }

        OffensePlayer o = other.GetComponent<OffensePlayer>();
        if (o != null)// && !runThrough.offensivePlayers.Contains(o)) //hold onto any players that are still in the run-through
        {
            RemoveOffensePlayer(o);
        }
        UpdateColor();
    }

    void UpdateColor()
    {
        if (defendedWithBall)
        {
            ShowHideDefendedIndicator(true);
        }
        else
        {
            ShowHideDefendedIndicator(false);
            if (defensivePlayers.Count > 0)
            {
                SetColor(Color.blue);
            }
            else if (occupiedByRunner)
            {
                SetColor(Color.yellow);
            }
            else
            {
                SetColor(Color.white);
            }
        }
    }

    void SetColor(Color c)
    {
        //renderer.material.color = (c);
    }

    void SetSafeIndicatorColor(Color c)
    {
        foreach(ParticleSystem sys in safeParticles.GetComponentsInChildren<ParticleSystem>())
        {
            var main = sys.main;
            main.startColor = new Color(c.r, c.g, c.b, main.startColor.color.a);
        }
    }

    void SetDefendedIndicatorColor(Color c)
    {
        foreach (ParticleSystem sys in defendedParticles.GetComponentsInChildren<ParticleSystem>())
        {
            var main = sys.main;
            main.startColor = new Color(c.r, c.g, c.b, main.startColor.color.a);
        }
    }

    void ShowHideSafeIndicator(bool isShowing)
    {
        if (safeFence)
        {
            safeFence.gameObject.SetActive(isShowing);
        }
        if (safeParticles)
        {
            if (!isShowing)
            {
                safeParticles.Stop(true);
            }
            else
            {
                if (baseNum == 3) return; // We don't wanna show this for home base. After scoring runs, it wouldn't go away.
                //SetSafeIndicatorColor(FullGameDirector.Instance.offenseColor);
                safeParticles.Play(true);
            }
        }
    }

    void ShowHideDefendedIndicator(bool isShowing)
    {
        if (defendedFence)
        {
            defendedFence.gameObject.SetActive(isShowing);
        }
        if (defendedParticles)
        {
            if (!isShowing) defendedParticles.Stop(true);
            else
            {
                //SetDefendedIndicatorColor(FullGameDirector.Instance.defenseColor);
                defendedParticles.Play(true);
            }
        }
    }

    public void PlaySafeOnBaseSound()
    {
        if (soundMaker && safeOnBaseSound)
        {
            //soundMaker.clip = safeOnBaseSound;
            //soundMaker.Play();
            DarkTonic.MasterAudio.MasterAudio.PlaySound("on_base_safe");
        }
    }

    void HandleFieldEvent(hittable hit, FieldPlayType type)
    {
        Debug.Log("Field Base is handling field play type: " + type.ToString());
        if(type == FieldPlayType.WALK)
        {
            offenseWalking = true;
        }
        else
        {
            offenseWalking = false;
        }
    }
}