﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using UnityEngine.UI;

public class BallGate : MonoBehaviour {

    protected RectTransform visualRect
    {
        get
        {
            if (_visualRect == null) _visualRect = GetComponentInChildren<Canvas>().gameObject.GetComponent<RectTransform>(); ;
            return _visualRect;
        }
    }
    private RectTransform _visualRect;

    protected Animator animator
    {
        get
        {
            if (_animator == null) _animator = GetComponent<Animator>();
            return _animator;
        }
    }
    private Animator _animator;

    private GameObject passMarker
    {
        get { if (_passMarker == null)
                {
                    _passMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                _passMarker.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Color");
                Destroy(_passMarker.GetComponent<Collider>());
                _passMarker.transform.localScale = Vector3.one * .25f;
                    _passMarker.transform.SetParent(this.transform);
                }
            return _passMarker;
        }
    }
    private GameObject _passMarker;
    public bool drawGizmo = true;
    public Color gizmoColor = Color.red;
    public bool drawLine;
    [SerializeField]
    private Vector2 _size = Vector2.one;

    public Vector2 size { 
    
        get { return _size; }
        set
        {
            _size = value;
            if (visualRect) visualRect.sizeDelta = _size;
        }
    }
    
    public Vector3 center
    {
        get { return transform.position; }
    }
    public Vector2 badPitchSpacer = Vector2.one;//space between valid and wild pitch
    public Vector2 badPitchMargin = Vector2.one;

    protected Vector2 wildOffset
    {
        get
        {
            return new Vector2(size.x / 2f + badPitchSpacer.x / 2f, size.y / 2f + badPitchSpacer.y / 2f);
        }
    }

    private Vector3 lastBallPos;
    private bool markPassPoint;

    private bool passed;
    public bool strikeDetected { get; private set; }
    public Action OnPassThrough;

    private Vector3 _intersectionPoint;
    private bool _insidePass;
    private bool _extendedPass;
    

#if UNITY_EDITOR
    [ExecuteInEditMode]
    void OnDrawGizmosSelected()
    {
        if(!drawGizmo) return;
            Color strikeColor = gizmoColor;
            strikeColor.a = gizmoColor.a *.1f;
            Vector3[] verts = new Vector3[]
        {
            center +    transform.right * size.x/2f +  transform.up * size.y/2f, //top right
            center +    transform.right * size.x/2f -  transform.up * size.y/2f, //bottom right
            center -    transform.right * size.x/2f -  transform.up * size.y/2f, //bottom left
            center -    transform.right * size.x/2f +  transform.up * size.y/2f, //top left
        };
            Handles.DrawSolidRectangleWithOutline(verts, strikeColor, gizmoColor);
        strikeColor.a = 0;

        verts[0] = verts[0] + transform.right * badPitchSpacer.x / 2f + transform.up * badPitchSpacer.y / 2f;
        verts[1] = verts[1] + transform.right * badPitchSpacer.x / 2f - transform.up * badPitchSpacer.y / 2f;
        verts[2] = verts[2] - transform.right * badPitchSpacer.x / 2f - transform.up * badPitchSpacer.y / 2f;
        verts[3] = verts[3] - transform.right * badPitchSpacer.x / 2f + transform.up * badPitchSpacer.y / 2f;
        Handles.DrawSolidRectangleWithOutline(verts, strikeColor, gizmoColor);

        verts[0] = verts[0] + transform.right * badPitchMargin.x / 2f + transform.up * badPitchMargin.y / 2f;
        verts[1] = verts[1] + transform.right * badPitchMargin.x / 2f - transform.up * badPitchMargin.y / 2f;
        verts[2] = verts[2] - transform.right * badPitchMargin.x / 2f - transform.up * badPitchMargin.y / 2f;
        verts[3] = verts[3] - transform.right * badPitchMargin.x / 2f + transform.up * badPitchMargin.y / 2f;
        Handles.DrawSolidRectangleWithOutline(verts, strikeColor, gizmoColor);

    }
#endif

    private const float validSafetyMargin = .2f;

    /// <summary>
    /// Returns position inside gate in World Space
    /// </summary>
    /// <returns></returns>
    public Vector3 GetValidStrikePosition()
    {
        return transform.position 
            + transform.up      * (size.y- validSafetyMargin)    * UnityEngine.Random.Range(-.5f, .5f) 
            + transform.right   * (size.x- validSafetyMargin)    * UnityEngine.Random.Range(-.5f, .5f);
    }

    public Vector3 GetInvalidStrikePosition()
    {
        float rand_x = UnityEngine.Random.Range(-.5f, .5f);
        float rand_y = UnityEngine.Random.Range(-.5f, .5f);

        return transform.position
            + transform.up * (badPitchMargin.y * rand_y + Mathf.Sign(rand_y) * wildOffset.y)
            + transform.right * (badPitchMargin.x * rand_x + Mathf.Sign(rand_x) * wildOffset.x);
    }

    public void Reset()
    {
        passed = false;
        strikeDetected = false;
        if (FieldMonitor.Instance && FieldMonitor.ballInPlay)
        {
            lastBallPos = FieldMonitor.ballInPlay.transform.position;
        }

    }

    public bool IsPassed
    {
        get
        {
            return passed;
        }
    }

    [ExecuteInEditMode]
    void OnValidate()
    {
        size = _size;
    }

    public void ShowHide_Box(bool showGate)
    {
        //if (visual) visual.enabled = showGate;
        if (animator)
        {
            animator.SetBool("visible", showGate);
        }
    }

    public void ShowHide_Marker(bool showMarker)
    {
//        Debug.Log("Show marker " + showMarker);
        markPassPoint = showMarker;
        if (!showMarker)
        {
            StopCoroutine("MarkPassPointRoutine");
            passMarker.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (passed) return;
        if(FieldMonitor.Instance != null)
        {

            if (FieldMonitor.ballInPlay != null && FieldMonitor.ballInPlay.GetRigidbody().isKinematic==false)
            {
                //Debug.Log("Strike gate watching " + FieldMonitor.BallInPlay);
                
                Vector3 ballPos = FieldMonitor.ballInPlay.transform.position;
                //Debug.DrawLine(transform.position, ballPos, Color.red, 100f);
                //Debug.DrawLine(ballPos, lastBallPos, Color.blue, 100f);
                //Debug.DrawRay(FieldMonitor.BallInPlay.transform.position, Vector3.up, Color.cyan, 100f);
                float t;
                bool justPassedPlane = GatePlaneIsBetweenPoints(ballPos, lastBallPos, out _insidePass, out _extendedPass, out _intersectionPoint, out t);
                bool wild = !_insidePass && !_extendedPass;

                //Debug.Log("gate " + passed + " " + justPassedPlane + " " + ballPos + " " + lastBallPos + " " + _intersectionPoint + " " + _insidePass + " " + _extendedPass + " " + wild);
                if(!passed && justPassedPlane && !wild)
                {
                    if (_insidePass && markPassPoint)
                    {
                        //Debug.DrawLine(_intersectionPoint, ballPos, Color.red, 100f);
                        //Debug.DrawRay(ballPos, Vector3.up, Color.magenta, 100f);
                        StartCoroutine("MarkPassPointRoutine");
                    }
                    if (OnPassThrough != null) OnPassThrough.Invoke();
                    passed = true;
                    if (_insidePass) strikeDetected = true;
                }
                lastBallPos = ballPos;
            }
        }
    }


    /// <summary>
    /// Checks if gate is between two world space positions
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    protected bool GatePlaneIsBetweenPoints(Vector3 p1, Vector3 p2, out bool insideStrikeZone, out bool insideExtendedZone, out Vector3 intersectionPoint, out float t)
    {
        intersectionPoint = Vector3.zero;
        Vector3 local_p1 = transform.InverseTransformPoint(p1);
        Vector3 local_p2 = transform.InverseTransformPoint(p2);
        Vector3 origin = local_p1;
        Vector3 direction = (local_p2 - local_p1);
        float length = direction.magnitude;
        t = -1;
        insideExtendedZone = false; insideStrikeZone = false;

        //Debug.DrawLine(p1, p2, Color.black, 1);
        
        Ray ray = new Ray(origin, direction);
        float intersectDist;
        if(MathUtility.FindPlaneLineIntersection(ray, Vector3.forward, Vector3.zero, out intersectDist))
        {
            if (intersectDist >0 && intersectDist <= length)
            {
                t = intersectDist / length;
                Vector3 local_intersectionPoint = ray.GetPoint(intersectDist);
                Vector3 world_intersectionPoint = transform.TransformPoint(local_intersectionPoint);
                intersectionPoint = world_intersectionPoint;
                //                Debug.Log("predicting intersection " + transform.TransformPoint(local_intersectionPoint));

                insideStrikeZone = (
                    local_intersectionPoint.x < size.x/2f
                    && local_intersectionPoint.x > -size.x/2f 
                    && local_intersectionPoint.y < size.y/2f
                    && local_intersectionPoint.y > -size.y/2f
                    );

                Vector2 extendedSize = size + badPitchSpacer + badPitchMargin;
                insideExtendedZone = (
                    local_intersectionPoint.x < extendedSize.x / 2f
                    && local_intersectionPoint.x > -extendedSize.x / 2f
                    && local_intersectionPoint.y < extendedSize.y / 2f
                    && local_intersectionPoint.y > -extendedSize.y / 2f
                    );

                if (drawLine)
                {
                    Debug.DrawLine(transform.TransformPoint(local_intersectionPoint), transform.TransformPoint(local_intersectionPoint) + transform.forward, gizmoColor, 2);
                }
                return true;

            }
        }

        return false;
    }

    public bool GateIsBetweenPoints(Vector3 p1, Vector3 p2, out float t)
    {
        GatePlaneIsBetweenPoints(p1, p2, out _insidePass, out _extendedPass, out _intersectionPoint, out t);
        return _insidePass;
    }

    public bool GateIsBetweenPoints_Extended(Vector3 p1, Vector3 p2, out float t)
    {
        GatePlaneIsBetweenPoints(p1, p2, out _insidePass, out _extendedPass, out _intersectionPoint, out t);
        return _insidePass || _extendedPass;
    }

    IEnumerator MarkPassPointRoutine()
    {
        passMarker.gameObject.SetActive(true);
        passMarker.transform.position = _intersectionPoint;
        passMarker.GetComponent<Renderer>().material.color = Color.red;

        yield return new WaitForSeconds(2);
        passMarker.gameObject.SetActive(false);

    }

}
