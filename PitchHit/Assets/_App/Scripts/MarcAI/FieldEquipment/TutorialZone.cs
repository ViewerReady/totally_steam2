﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TutorialZone : MonoBehaviour {

    public Action OnHumanEnterZone;
    public Action OnHumanExitZone;
    public ParticleSystem particles;
    public GameObject label;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("onTrigger enter " + other);
        AIFieldPlayer player = other.GetComponent<AIFieldPlayer>();
        if(player != null)
        {
            if (player.isTetheredToCamera)
            {
                if (OnHumanEnterZone != null) OnHumanEnterZone.Invoke();

            }
        }  
    }

    private void OnTriggerExit(Collider other)
    {
        AIFieldPlayer player = other.GetComponent<AIFieldPlayer>();
        if (player != null)
        {
            if (player.isTetheredToCamera)
            {
                if (OnHumanExitZone != null) OnHumanExitZone.Invoke();

            }
        }
    }

    public void ShowHideParticles(bool show)
    {
        if (show) particles.Play();
        else particles.Stop();

        label.SetActive(show);
    }
}
