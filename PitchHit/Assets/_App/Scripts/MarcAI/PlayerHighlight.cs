﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHighlight : MonoBehaviour {

    public SpriteRenderer primarySprite;
    public SpriteRenderer secondarySprite;
    public SpriteRenderer arrowSprite;
    public AudioClip hoverSound;
    public AudioClip grabSound;
    public AudioClip releaseSound;

    const float availableSize = 6;
    const float unavailalbeSize = 0;
    const float highlightedSize = 4;

    public float highlightedZRotation = 180;
    public float unhighlightedZRotation = 0;


    public Color color;
    const float availableAlpha = .1f;

    

    void Start()
    {
        primarySprite.enabled = false;
        secondarySprite.enabled = false;
        if(arrowSprite)
        arrowSprite.enabled = false;
        SetColor(color);
        if(hoverSound == null)
            hoverSound = Resources.Load<AudioClip>("Audio/UI/sfx_sounds_button8");

        if (grabSound == null)
            grabSound = Resources.Load<AudioClip>("Audio/UI/sfx_sounds_button10");

    }

    public void SetColor(Color c)
    {
        primarySprite.color = c;
        secondarySprite.color = c;
        if(arrowSprite)
        arrowSprite.color = c;
        color = c;
    }
	public void PlayHighlightSound()
    {
        if (hoverSound != null) AudioSource.PlayClipAtPoint(hoverSound, Camera.main.transform.position, .1f);
    }

    public void PlayPressSound()
    {
        if (grabSound != null) AudioSource.PlayClipAtPoint(grabSound, Camera.main.transform.position, .1f);

    }

    public void ShowPrimary(bool isShowing)
    {
       primarySprite.enabled = isShowing;
    }
    public void ShowSecondary(bool isShowing)
    {
        secondarySprite.enabled = isShowing;
    }
    public void ShowArrow(bool isShowing)
    {
        if(arrowSprite)
        arrowSprite.enabled = isShowing;
    }

    

    IEnumerator ActivateRoutine(float endScale, float endAlpha, float endZrot)
    {
        //only needs to rotate going in-out of highlight
        primarySprite.enabled = true;

        float startZrot = primarySprite.transform.localRotation.eulerAngles.z;
        //if (endZrot < startZrot) endZrot = 360 + endZrot;
        float startScale = primarySprite.transform.localScale.x;

        Color startColor = primarySprite.color;
        Color endColor =color;
        endColor.a = endAlpha;

        float time = 0;
        float t = 0;
        float duration = .25f;
        while (time < duration)
        {
            yield return null;
            time += Time.deltaTime;
            t = time / duration;
            t = Mathf.Clamp01(t);
            primarySprite.transform.localScale = Vector3.one * Mathf.Lerp(startScale, endScale, t );
            primarySprite.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startZrot, endZrot, t ));
            primarySprite.color = Color.Lerp(startColor, endColor, t);
        }

        primarySprite.transform.localScale = Vector3.one * endScale;
        primarySprite.transform.localRotation = Quaternion.Euler(0, 0, endZrot);
        primarySprite.color = endColor;



    }

    IEnumerator DeactivateRoutine()
    {
        primarySprite.enabled = true;

        float startZrot = 45f;
        float endZrot = 180f;
        float startScale = 4;
        float endScale = 6;

        Color startColor = color;
        Color endColor = color;
        endColor.a = 0;

        float time = 0;
        float t = 0;
        float duration = .25f;
        while (time < duration)
        {
            yield return null;

            time += Time.deltaTime;
            t = time / duration;
            t = Mathf.Clamp01(t);
            primarySprite.transform.localScale = Vector3.one * Mathf.Lerp(startScale, endScale, t);
            primarySprite.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(startZrot, endZrot, t));
            primarySprite.color = Color.Lerp(startColor, endColor, t);
        }

        primarySprite.enabled = false;

    }


}
