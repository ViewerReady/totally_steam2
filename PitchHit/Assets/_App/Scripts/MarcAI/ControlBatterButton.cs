﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlBatterButton : FieldControlButton {
    public static bool shouldControlBatter = true;
    
    new void Start()
    {
        //FieldManager.Instance.offense.playerControlled = shouldControlBatter;
        Debug.Log("is batter player controlled at start?"+FullGameDirector.Instance.offense.humanControlled+" "+shouldControlBatter);
        

        base.Start();

		if (shouldControlBatter)
		{
			isActive = true;
			SetColor(activeColor);
		}
    }


    void ToggleControlingBatter()
    {
        shouldControlBatter = !shouldControlBatter;
		isActive = shouldControlBatter;
        //if(shouldControlBatter)
        //{
        //    ControlDefenseButton.shouldControlDefense = false;
        //}
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		Debug.Log("Toggle control of batter to "+shouldControlBatter);

        if (shouldControlBatter)
        {
            FullGameDirector.Instance.ChangePlayerRole(FullGameDirector.HumanPlayerRole.OFFENSE);
        }
        /*
		FieldManager.Instance.offense.humanControlled = shouldControlBatter;
		FieldManager.Instance.offense.RefreshState();
        */

		if (shouldControlBatter)
		{
			SetColor(shouldControlBatter? activeColor:unlitColor);
		}
    }

    protected override void OnButtonPress()
    {
		
        ToggleControlingBatter();
    }

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.B)) {
			OnButtonPress ();
		}
	}

}
