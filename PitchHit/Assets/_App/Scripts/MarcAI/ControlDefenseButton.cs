﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlDefenseButton : FieldControlButton {
    public static bool shouldControlDefense = false;
    
    new void Start()
    {
        //FieldManager.Instance.defense.playerControlled = shouldControlDefense;

        

        base.Start();
		if (shouldControlDefense)
		{
			SetColor(activeColor);
			isActive = true;
		}
    }


    void ToggleControlingDefense()
    {
        shouldControlDefense = !shouldControlDefense;
		isActive = shouldControlDefense;
        if (shouldControlDefense)
        {
            ControlBatterButton.shouldControlBatter = false;
        }
		Debug.Log ("toggle control of pitcher to "+shouldControlDefense);

        if(shouldControlDefense)
            FullGameDirector.Instance.ChangePlayerRole(FullGameDirector.HumanPlayerRole.DEFENSE);

        /*
		FieldManager.Instance.defense.humanControlled = shouldControlDefense;
		FieldManager.Instance.defense.RefreshState ();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        */

		if (shouldControlDefense)
		{
			SetColor(shouldControlDefense? activeColor:unlitColor);
		}

    }

    protected override void OnButtonPress()
    {
        ToggleControlingDefense();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            OnButtonPress();
        }
    }


}
