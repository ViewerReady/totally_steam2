﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

/// <summary>
/// There must be an Instance to run coroutines, but otherwise it can function as a static class
/// </summary>
public class HumanPosessionManager : MonoBehaviour
{
    public enum LocomotionVariant
    {
        STANDARD,
        SMOOTH,
    }

    public static HumanPosessionManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else GameObject.Destroy(this);
    }

    void Start()
    {
        if (!initialized)
        {
            Initialize();
        }
    }

    public static Transform playArea
    {
        get
        {
            if (_playArea == null)
            {
#if STEAM_VR
                _playArea = Player.instance.transform;
#elif RIFT
                _playArea = OVRManager.instance.transform;
#endif
            }
            return _playArea;
        }
        set
        {
            _playArea = value;
        }
    }
    private static Transform _playArea;
    protected static Vector3 initialVRPosition;
    protected static Quaternion initialVRRotation;
    protected static ArmSwinger armSwinger;

    private static LocomotionVariant locomotionType_current;
    private static bool locomotionProjection_current;

    #region SavedSettings

    private const string locomotionPrefsKey = "LOCOMOTION";

    public static LocomotionVariant locomotionType_saved
    {
        get { return (LocomotionVariant)PlayerPrefs.GetInt(locomotionPrefsKey); }
        private set { PlayerPrefs.SetInt(locomotionPrefsKey, (int)value); }
    }

    private const string locomotionProjectionPrefsKey = "LOCO_PROJECTION";
    public static bool locomotionProjection_saved
    {
        get { return PlayerPrefs.GetInt(locomotionProjectionPrefsKey) == 1; }
        private set { PlayerPrefs.SetInt(locomotionProjectionPrefsKey, value ? 1 : 0); }
    }

    private const string slowmoPrefsKey = "SLOWMO_ENABLED";
    public static bool slowmoEnabled{
        get
        {
            if (!PlayerPrefs.HasKey(slowmoPrefsKey)) slowmoEnabled = true;
            return (PlayerPrefs.GetInt(slowmoPrefsKey) >0);
        }
        set
        {
            PlayerPrefs.SetInt(slowmoPrefsKey, value? 1 : 0);
        }
    }

    private const string vignettePrefsKey = "VIGNETTE_ENABLED";
    public static bool locomotionVignetteEnabled
    {
        get
        {
            if (!PlayerPrefs.HasKey(vignettePrefsKey)) locomotionVignetteEnabled = true;
            return (PlayerPrefs.GetInt(vignettePrefsKey) > 0);
        }
        set
        {
            PlayerPrefs.SetInt(vignettePrefsKey, value ? 1 : 0);
        }
    }


    #endregion

    public static Action<AIFieldPlayer> OnPlayerPosessed;
    public static Action<AIFieldPlayer> OnPlayerUnposessed;

    private AIFieldPlayer currentlyTetheredPlayer_editor; //this field only exists to make currentlyTetheredPlayer visible in the inspector for debugging

    public FieldPlayerBodyControl projectionPrefab;
    private FieldPlayerBodyControl projectionPuppet;

    private bool currentlyProjectingTeleportLocation;

    public static AIFieldPlayer currentlyTetheredPlayer
    {
        get { return _currentlyTetheredPlayer; }
        private set { if (Instance) Instance.currentlyTetheredPlayer_editor = value; _currentlyTetheredPlayer = value; }
    }
    private static AIFieldPlayer _currentlyTetheredPlayer;
    private static Coroutine currentPossesRoutine;

    private static bool fadedOut = false;
    private static AIFieldPlayer waitingToPosess;

    private static bool projectOnRail = false;
    private static Vector3 railStart = Vector3.zero;
    private static Vector3 railEnd = Vector3.forward;

    private const float normalizedScale = 1.3f;

    private bool initialized = false;
    private static bool locomotionEnabled;

    public static bool transferRoutineInProgress{
        get;
        private set;
    }


    void Initialize()
    {
        StartCoroutine(InitializeRoutine());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SetLocomtionProjection(!locomotionProjection_saved);
        }
    }

    IEnumerator InitializeRoutine()
    {
        if (initialized)
        {
            yield break;
        }

        armSwinger = playArea.GetComponentInChildren<ArmSwinger>();
        armSwinger.OnBeginLocomotion += OnBeginLocomotion;
        armSwinger.OnEndLocomotion += OnEndLocomotion;
        projectionPuppet = GameObject.Instantiate<FieldPlayerBodyControl>(projectionPrefab);// locomotionProjection.GetComponentInChildren<ProjectionPuppet>();
        projectionPuppet.gameObject.SetActive(false);

        CameraScaler.instance.scale = normalizedScale;
        initialized = true;

        while (fadedOut)
        {
            yield return null;
        }
        initialVRPosition = playArea.position;
        initialVRRotation = playArea.rotation;
        SetupForLocomotionType(locomotionType_saved, locomotionProjection_saved);
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (projectOnRail)
        {
            Handles.color = Color.black;
            Handles.DrawLine(railStart, railEnd);
        }
        if(currentlyTetheredPlayer != null)
        {
            Handles.color = Color.yellow;
            Handles.DrawWireDisc(currentlyTetheredPlayer.transform.position, Vector3.up, 1.25f);
        }
    }
#endif


    public static void ResetPlayAreaToInitialPositionRotation()
    {
        playArea.transform.position = initialVRPosition;
        playArea.transform.rotation = initialVRRotation;
    }

    private static bool AlmostEqualsVector3(Vector3 a, Vector3 b, float precision)
    {
        if ((a - b).magnitude <= precision) return true;
        else return false;
    }

    public static bool PlayAreaIsAtInitialPositionRotation()
    {
        //return playArea.transform.position.AlmostEquals(initialVRPosition, .01f) && playArea.transform.rotation.AlmostEquals(initialVRRotation, .01f);
        return AlmostEqualsVector3(playArea.transform.position, initialVRPosition, 0.01f) && (Quaternion.Angle(playArea.transform.rotation, initialVRRotation) < 0.01f);
    }

    public static void SetInitialPositionRotation(Vector3 position, Quaternion rotation)
    {
        initialVRPosition = position;
        initialVRRotation = rotation;
        //ResetPlayAreaToInitialPositionRotation();
    }

    private void SetSnapTurnEnabled(bool snapTurn)
    {
        GameController.canTurnAround = snapTurn;
#if STEAM_VR
        SnapTurn snap = GetComponentInChildren<SnapTurn>();
        if(snap) snap.enabled = snapTurn;
#endif
    }

    Coroutine locomotionDelay;
    public static void SetLocomotionEnabled(bool isOn, float delay = 0)
    {
        locomotionEnabled = isOn;

        if(Instance.locomotionDelay != null)
        {
            Instance.StopCoroutine(Instance.locomotionDelay);
        }

        if (delay == 0)
        {
            RefreshLocomotion();
        }
        else
        {
            Instance.StartCoroutine(Instance.DelayedLocomotionActivation(delay));
        }
        
    }

    private static void RefreshLocomotion()
    {
        if (Instance &&
            ((locomotionType_saved != locomotionType_current)
                || (locomotionProjection_saved != locomotionProjection_current))
            )
        {
            Instance.SetupForLocomotionType(locomotionType_saved, locomotionProjection_saved);
        }
        if (armSwinger)
        {
            armSwinger.latestArtificialMovement = 0f;
            armSwinger.CleanHistory();
            armSwinger.armSwingNavigation = locomotionEnabled;
        }
    }

    IEnumerator DelayedLocomotionActivation(float delay)
    {
        float countdown = delay;
        while(countdown > 0 && (armSwinger.rightButtonPressed || armSwinger.leftButtonPressed))
        {
            yield return null;
            countdown -= Time.deltaTime;
        }
        RefreshLocomotion();
    }

    public static void ConstrainProjectionToRail(bool onRail)
    {
        projectOnRail = onRail;
    }

    public static void SetProjectionRailPoints(Vector3 start, Vector3 end, bool snapToStart = true)
    {
        railStart = start; railStart.y = 0;
        railEnd = end; railEnd.y = 0;
        //as long as useRotationOverride == false, it doesn't matter if we set the override rotation when not using rails
        Quaternion forwardLook = Quaternion.LookRotation((end - start), Vector3.up);
        armSwinger.overrideMoveRotation = forwardLook;

        if (projectOnRail)
        {
            if (snapToStart)
            {
                Instance.projectionPuppet.transform.position = railStart;
            }
            Instance.projectionPuppet.transform.rotation = forwardLook;

        }
    }

    public static void SetReverseRailLocomotionEnabled(bool reverse)
    {
        if(projectOnRail && locomotionProjection_current)
            ArmSwinger.canReverse = reverse;
        else
        {
            ArmSwinger.canReverse = true;
        }
       //Debug.Log("<color=red>REverse on rail " + reverse  + "  on rail now? " + projectOnRail + "</color>");
    }

    protected void SetupForLocomotionType(LocomotionVariant var, bool useProjection)
    {
        if (!initialized)
        {
            Debug.Log("Set up for locomotion");
            Initialize();
        }

        locomotionType_current = var;
        locomotionProjection_current = useProjection;

        switch (var)
        {
            
            case LocomotionVariant.STANDARD:
                SetSnapTurnEnabled(true);
                ArmSwinger.canReverse = true;
                armSwinger.overrideMoveRigidbody = null;
                armSwinger.useRotationOverride = false;
                armSwinger.smoothMovementOverride = false;
                break;
            case LocomotionVariant.SMOOTH:
                SetSnapTurnEnabled(false);
                armSwinger.overrideMoveRigidbody = null;
                armSwinger.useRotationOverride = false;
                armSwinger.smoothMovementOverride = true;

                break;
        }

        if (useProjection)
        {
            if (projectOnRail)
            {
                SetProjectionRailPoints(railStart, railEnd);
            }
            projectionPuppet.LockFeetAtCenter(projectOnRail);
            armSwinger.useRotationOverride = projectOnRail;
            armSwinger.overrideMoveRigidbody = projectionPuppet.GetComponent<Rigidbody>();
        }
        else
        {
            armSwinger.overrideMoveRigidbody = null;
            armSwinger.useRotationOverride = false;
        }
       // Debug.Log("<color=red>Setup Locomotion " + locomotionType_saved+"</color>");

    }

    public void RefreshLocomotionType()
    {
        StartCoroutine(WaitForFadeToRefreshRoutine());
    }

    IEnumerator WaitForFadeToRefreshRoutine()
    {
        while (fadedOut)
        {
            yield return null;
        }
        SetupForLocomotionType(locomotionType_saved, locomotionProjection_saved);
    }


    public static void SetLocomotionType(LocomotionVariant variant)
    {
        locomotionType_saved = variant;
        RefreshLocomotion();
    }

    public static void SetLocomtionProjection(bool useProjection)
    {
        locomotionProjection_saved = useProjection;
        SetLocomotionEnabled(false);
        Instance.StartCoroutine("TurnOnLocoAfterFame");
    }

    IEnumerator TurnOnLocoAfterFame()
    {
        yield return null;
            SetLocomotionEnabled(true);  
    }

    protected void OnBeginLocomotion()
    {
        if (locomotionProjection_current)
        {
            if (currentlyProjectingTeleportLocation) return;

            projectionPuppet.gameObject.SetActive(true);
            if (!projectOnRail)
            {
                projectionPuppet.transform.SetPositionAndRotation(armSwinger.transform.position, armSwinger.transform.rotation);
            }
            currentlyProjectingTeleportLocation = true;

            if (currentlyTetheredPlayer != null)
                currentlyTetheredPlayer.SetPosessedBodyFollow(projectionPuppet.transform);
        }
    }

    protected void OnEndLocomotion()
    {
        if (locomotionProjection_current)
        {
            if (!currentlyProjectingTeleportLocation) return;

                projectionPuppet.gameObject.SetActive(false);
            if (!projectOnRail)
                armSwinger.transform.position = projectionPuppet.transform.position;
            else
            {
                Vector3 delta = Camera.main.transform.localPosition; delta.y = 0;
                armSwinger.transform.position = projectionPuppet.transform.position - delta;

            }
            currentlyProjectingTeleportLocation = false;
            if (currentlyTetheredPlayer != null)
            {
                currentlyTetheredPlayer.SetPosessedBodyFollow(null);
            }

        }
    }

    public static void ReleasePosessedPlayer(AIFieldPlayer player)
    {
        if (currentlyTetheredPlayer != null && currentlyTetheredPlayer == player) ReleasePosessedPlayer();
    }

    public static void ReleasePosessedPlayer()
    {
        //Debug.Log("ReleasePlayer()");
        if (currentlyTetheredPlayer == null) return;
        //Debug.Log("<color=cyan>****************************** RELEASE " + currentlyTetheredPlayer.name + "</color>");
        _ReleasePosessedPlayer();
    }

    private static void _ReleasePosessedPlayer()
    {
        if (currentlyTetheredPlayer == null) return;
        //Debug.Log("_RELEASE POSESSION --------- " + currentlyTetheredPlayer.name);

        currentlyTetheredPlayer.SetHumanPosession(false);
        AIFieldPlayer last = currentlyTetheredPlayer;
        currentlyTetheredPlayer = null;
        if(Instance)Instance.OnEndLocomotion();
        if (OnPlayerUnposessed != null) OnPlayerUnposessed.Invoke(last);
    }


    public static void PosessPlayer(AIFieldPlayer toPosess,
                                    bool cancelIfAlreadyTethered = true,
                                    bool resetPlayer = false,
                                    bool resetPlayArea = false,
                                    bool slowMo = false,
                                    bool fade = false,
                                    bool turnAround = false,
                                    bool matchScale = false,
                                    bool snapToHumanPosition = false)
    {
        
        //Debug.Log("PosessPlayer() " + toPosess);
        if (toPosess == null) return;

        //there's already a coroutine to posess this player, assume the first one takes priority
        if (toPosess == waitingToPosess)
        {
            //Debug.Log("THIS POSSESSION IS ALREADY HAPPENING!");
            //return;
        }

        //bool needsToResetPlayArea = resetPlayer && !PlayAreaIsAtInitialPositionRotation();
        if (toPosess == currentlyTetheredPlayer && cancelIfAlreadyTethered)// && !needsToResetPlayArea && !resetPlayer)
        {
            //Debug.Log("CANCEL BECAUSE ALREADY TETHERED");
            return;
        }
        //Debug.Log("<color=cyan>****************************** POSESS " + toPosess.name + "</color>");


        if (Instance)
        {
            if (currentPossesRoutine != null)
            {
                //Debug.Log("<color=blue>****************************** Stopping Old Routine </color> ");
                Instance.StopCoroutine(currentPossesRoutine);
            }
            //Debug.Log("Starting possession routine and passing fade == " + fade.ToString());
            currentPossesRoutine = Instance.StartCoroutine(PosessionRoutine(toPosess, cancelIfAlreadyTethered, resetPlayer, resetPlayArea, slowMo, fade, turnAround, matchScale, snapToHumanPosition));
        }
        else
        {
            //Debug.Log("Calling transfer possession immediate");
            TransferPosession_Immediate(toPosess);
        }
    }

    private static void _PosessPlayer(AIFieldPlayer toPosess)
    {
        if (toPosess == null) return;
        if (toPosess == currentlyTetheredPlayer) return;
        //Debug.Log("_START POSESSION --------- " + toPosess.name);

        Vector3 playAreaPos = playArea.position + (toPosess.transform.position - Camera.main.transform.position);
        playAreaPos.y = toPosess.transform.position.y;
        playArea.position = playAreaPos;

        toPosess.SetHumanPosession(true);
        currentlyTetheredPlayer = toPosess;
        waitingToPosess = null;
        if (armSwinger)
            armSwinger.CleanHistory();
        if (OnPlayerPosessed != null) OnPlayerPosessed.Invoke(currentlyTetheredPlayer);
    }

    private static IEnumerator PosessionRoutine(AIFieldPlayer nextPosess,
                                    bool cancelIfAlreadyTethered,
                                    bool resetPlayer,
                                    bool resetPlayArea,
                                    bool slowMo,
                                    bool fade,
                                    bool turnAround,
                                    bool matchScale,
                                    bool snapToHumanPosition)
    {

        if (cancelIfAlreadyTethered && nextPosess.isTetheredToCamera)//Greg: not sure if this is necessary.
        {
            yield break;
        }


        transferRoutineInProgress = true;

        //Debug.Log("start posession routine " + nextPosess.name);
        waitingToPosess = nextPosess;
        if (slowMo && slowmoEnabled) FieldTransitionManager.Instance.EnterSlowMotion();
        if (fade)
        {
//            Debug.Log("DOING THE FADE");
            fadedOut = true;
            FieldTransitionManager.Instance.FadeOut(.5f);
            yield return new WaitForSecondsRealtime(.6f);
        }

        if (armSwinger)
        {
            armSwinger.HaltArmSwingerMotion();
        }

        if (resetPlayer /*&& !nextPosess.isTetheredToCamera*/)
        {
            nextPosess.InstantReset();
        }
        if (snapToHumanPosition)
        {
            Vector3 playerPosition = Camera.main.transform.position;
            playerPosition.y = 0f;
            nextPosess.position = playerPosition;
        }

        if (CameraScaler.instance)
        {

            Vector3 pivotPos = Vector3.zero;// nextPosess.transform.position; //pivotPos.y = 0;
            CameraScaler.instance.SetPivotPosition(pivotPos, adjustForCamOffset: false);

            if (matchScale)
            {
                CameraScaler.instance.scale = nextPosess.transform.localScale.y * normalizedScale;
            }
            else
            {
                CameraScaler.instance.scale = normalizedScale;
            }
        }

        TransferPosession_Immediate(nextPosess);

        if (turnAround)
        {
            //Debug.Log("TURN AROUND++++++++++++++++++++++++++++++++++++++++++++++++XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            playArea.RotateAround(Camera.main.transform.position, Vector3.up, 180f);
        }
        else
        {
            //this must mean that it's coming from the bat.
            // if (FieldManager.Instance && FieldManager.Instance.defense && FieldManager.Instance.defense.catcher == nextPosess)
            if (FullGameDirector.Instance && FullGameDirector.Instance.defense && nextPosess == FullGameDirector.Instance.defense.catcher)
            {
                //Debug.Log("it's the catcher so turn around!");
                HumanPosessionManager.playArea.Rotate(Vector3.up * 180f);
            }


        }
        if (resetPlayArea)
        {
            ResetPlayAreaToInitialPositionRotation();

            //MARC: this is gross, should not have posessionmanager need to reference fullgamedirector in any way. should be totally agnostic to what's going on in the scene
            if (FullGameDirector.Instance && FullGameDirector.Instance.offense.humanControlled)
            {
                GameController.isAtBat = true;
            }
        }

        //HumanGearManager.instance.RefreshHatStatus();

        yield return null;
        if (fadedOut)
        {
            FieldTransitionManager.Instance.FadeIn(.5f);
            fadedOut = false;
        }

        transferRoutineInProgress = false;
        //Debug.Log("end posession routine ");
    }



    private static void TransferPosession_Immediate(AIFieldPlayer nextPosess)
    {
        if (nextPosess.isTetheredToCamera && FullGameDirector.Instance.defense.runningPlay != FieldPlayType.STANDARD)
        {
            return;
        }

        _ReleasePosessedPlayer();

        HandManager.instance.DropAllHeldObjects();

        if (nextPosess != null)
        {
            _PosessPlayer(nextPosess);
        }

        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
            Debug.DrawRay(HandManager.currentLeftCustomHand.transform.position, Vector3.up * 5f, Color.magenta, 100f);
        }

        if(HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
            Debug.DrawRay(HandManager.currentRightCustomHand.transform.position, Vector3.up * 5f, Color.magenta, 100f);
        }
    }

    public static void RefreshJersey()
    {
        //Debug.Log("refresh Posession " + currentlyTetheredPlayer);
        if(currentlyTetheredPlayer != null)
        {
            HumanGearManager.instance.SetTorsoMaterials(currentlyTetheredPlayer.GetJerseyMaterials());
        }
    }
}
