﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
#if STEAM_VR
using Valve.VR.InteractionSystem;
#endif

/// <summary>
/// There must be an Instance to run coroutines, but otherwise it can function as a static class
/// </summary>
public class HumanHUDManager : MonoBehaviour
{
    public static HumanHUDManager Instance;
    public OffScreenBallIndicator ballIndicator;


    private const string offscreenBallPrefsKey = "OFFSCREEN_BALL";
    public static bool indicatorEnabled_saved
    {
        get
        {
            return PlayerPrefs.GetInt(offscreenBallPrefsKey) > 0;
        }
        set
        {
            PlayerPrefs.SetInt(offscreenBallPrefsKey, value ? 1 : 0);
            indicatorEnabled_cached = value;
            if (Instance) Instance.ShowHideOffscreenBallIndicator(Instance.indicatorShowing);
        }
    }
    private static bool indicatorEnabled_cached; 
    private bool indicatorShowing = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            indicatorEnabled_cached = indicatorEnabled_saved;
        }

        else GameObject.Destroy(this);
    }

    void Start()
    {
        //ShowHideOffscreenBallIndicator(true);
    }



    //public handle for turning ball indicator on and off
    //indicator must be enabled and showing to appear, otherwise it will be hidden
    public void ShowHideOffscreenBallIndicator(bool show)
    {
        indicatorShowing = show;
        if (ballIndicator)
        {
            ballIndicator.enabled = indicatorEnabled_cached && indicatorShowing;
        }
    }
}
