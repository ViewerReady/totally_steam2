﻿using UnityEngine;
using System.Collections;

public class HumanGearManager : MonoBehaviour {
    private static HumanGearManager _instance;
    public static HumanGearManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<HumanGearManager>();
            }
            return _instance;
        }
        private set
        {
            _instance = value;
        }
    }

    public Collider playerHeadCollider;
    public Collider playerTorsoCollider;
    protected bool lockHatAsCap;
    // Use this for initialization

    public FieldPlayerBodyControl bodyPrefab;
    protected FieldPlayerBodyControl body
    {
        get
        {
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.BROADCAST && !bodyIsAnnouncer)
            {
                if (_body != null) Destroy(_body.gameObject);
                _body = CreateBody();
                return _body;
            }
            else if(FullGameDirector.currentHumanPlayerRole != FullGameDirector.HumanPlayerRole.BROADCAST && bodyIsAnnouncer)
            {
                if (_body != null) Destroy(_body.gameObject);
                _body = CreateBody();
                return _body;
            }
            else
            {
                if (_body == null) _body = CreateBody();
                return _body;
            }
        }
    }
    private FieldPlayerBodyControl _body;

    public FieldPlayerBodyControl announcerBodyPrefab;
    protected FieldPlayerBodyControl announcerBody
    {
        get
        {
            if (_announcerBody == null) _announcerBody = CreateBody();
            return _announcerBody;
        }
    }
    private FieldPlayerBodyControl _announcerBody;

    private bool bodyIsAnnouncer;
    public bool livEnabled;

    void Start () {
        instance = this;

        //body = GameObject.Instantiate(bodyPrefab, this.transform);
        //body.transform.localPosition = Vector3.zero;


        RefreshHatStatus();

        foreach(Collider c in GetComponentsInChildren<Collider>(true))
        {
            if(c.name == "Head Collider")
            {
                playerHeadCollider = c;
            }
            else if (c.name == "Torso Collider")
            {
                playerTorsoCollider = c;
            }
        }

        RefreshColliderStatus();

    }

    private FieldPlayerBodyControl CreateBody()
    {
        GameObject bodyRoot = new GameObject("bodyRoot");
        bodyRoot.transform.SetParent(this.transform);
        FieldPlayerBodyControl myBody;
        if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.BROADCAST)
        {
            myBody = GameObject.Instantiate(announcerBodyPrefab, bodyRoot.transform);
            bodyIsAnnouncer = true;
        }
        else
        {
            myBody = GameObject.Instantiate(bodyPrefab, bodyRoot.transform);
            bodyIsAnnouncer = false;
            myBody.SetGearSpectatorOnly(FieldPlayerGear.CAP);
            //myBody.SetGearSpectatorOnly(FieldPlayerGear.HELMET);
            //myBody.SetGearSpectatorOnly(FieldPlayerGear.JERSEY);
            //myBody.SetGearSpectatorOnly(FieldPlayerGear.CATCHER_CHEST);
            //myBody.SetGearSpectatorOnly(FieldPlayerGear.CATCHER_FACE);
        }
        myBody.transform.localPosition = Vector3.zero;
        myBody.SetAnimatorEnabled(false);
        myBody.SetHandVisibility(HandManager.HandType.both, false);
        myBody.acceptDeviceInput = true;
        myBody.SetFeetVisibility(false);

        if (livEnabled)
        {
            myBody.HideGearFromLIV();
        }
        else
        {
            myBody.UnhideGearFromLIV();
        }

        //myBody.SetHeadFollow();
        return myBody;
    }

    public void RefreshColliderStatus()
    {
        if (FullGameDirector.Instance)
        {
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.DEFENSE)
            {
                if (playerHeadCollider)
                {
                    playerHeadCollider.gameObject.tag = "Defense";
                }
                if (playerTorsoCollider)
                {
                    playerTorsoCollider.gameObject.tag = "Defense";
                }
            }
            else if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.OFFENSE)
            {
                if (playerHeadCollider)
                {
                    playerHeadCollider.gameObject.tag = "Offense";
                    playerHeadCollider.enabled = false;
                }
                if (playerTorsoCollider)
                {
                    playerTorsoCollider.gameObject.tag = "Offense";
                    playerTorsoCollider.enabled = false;
                }
                
            }
        }
    }

    protected void RefreshHatStatus()
    {
        if(GameController.mixedRealityMode)
        {
            Debug.Log("mixed reality.");
            ShowHideGear(FieldPlayerGear.HELMET, false);
            ShowHideGear(FieldPlayerGear.CAP, false);
            ShowHideGear(FieldPlayerGear.CATCHER_FACE, false);
            ShowHideGear(FieldPlayerGear.CATCHER_CHEST, false);
            ShowHideGear(FieldPlayerGear.JERSEY, false);
            
            return;
        }

        if (FullGameDirector.Instance)
        {
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.BROADCAST) return;
        }

        if (FullGameDirector.Instance)
        {
            if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.DEFENSE)
            {
                ShowDefenderGear();
            }
            else
            {
                ShowBatterGear();
            }
            return;
        }

        if(lockHatAsCap)
        {
            ShowHideGear(FieldPlayerGear.CATCHER_FACE, false);
            ShowHideGear(FieldPlayerGear.HELMET, false);
            ShowHideGear(FieldPlayerGear.CAP, true);
            return;
        }

        ShowHideGear(FieldPlayerGear.HELMET, GameController.isAtBat);
        ShowHideGear(FieldPlayerGear.CAP, !GameController.isAtBat);
        ShowHideGear(FieldPlayerGear.CATCHER_FACE,
            (FullGameDirector.Instance && FullGameDirector.Instance.defense && FullGameDirector.Instance.defense.catcher && FullGameDirector.Instance.defense.catcher.isTetheredToCamera)
            );
        ShowHideGear(FieldPlayerGear.JERSEY,
            HumanPosessionManager.currentlyTetheredPlayer == null
            );

    }

    public void ShowHideGear(FieldPlayerGear gear, bool isOn)
    {
        body.EquipGear(gear, isOn);
       
    }

    public void HideAllGear()
    {
        ShowHideGear(FieldPlayerGear.HELMET, false);
        ShowHideGear(FieldPlayerGear.CAP, false);
        ShowHideGear(FieldPlayerGear.JERSEY, false);
        ShowHideGear(FieldPlayerGear.CATCHER_FACE, false);
        ShowHideGear(FieldPlayerGear.HEAD, false);

        if(HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.RefreshHandState();
        }

        if(HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.RefreshHandState();
        }
    }

    public void ShowBatterGear()
    {
        //Debug.Log("____________________________SHOW BATTER GEAR");
        ShowHideGear(FieldPlayerGear.HELMET, true);
        ShowHideGear(FieldPlayerGear.CAP, false);
        ShowHideGear(FieldPlayerGear.JERSEY, true);
        ShowHideGear(FieldPlayerGear.CATCHER_FACE, false);
        ShowHideGear(FieldPlayerGear.HEAD, true);

    }

    public void ShowCatcherGear()
    {
        //Debug.Log("____________________________SHOW Catcher GEAR");
        ShowHideGear(FieldPlayerGear.HELMET, false);
        ShowHideGear(FieldPlayerGear.CAP, true);
        ShowHideGear(FieldPlayerGear.JERSEY, true);
        ShowHideGear(FieldPlayerGear.CATCHER_FACE, true);
        ShowHideGear(FieldPlayerGear.HEAD, true);

    }

    public void ShowDefenderGear()
    {
        //Debug.Log("____________________________SHOW DEFENDER GEAR");
        ShowHideGear(FieldPlayerGear.HELMET, false);
        ShowHideGear(FieldPlayerGear.CAP, true);
        ShowHideGear(FieldPlayerGear.JERSEY, true);
        ShowHideGear(FieldPlayerGear.CATCHER_FACE, false);
        ShowHideGear(FieldPlayerGear.HEAD, true);

    }

    public void SetTorsoMaterials(Material[] mat)
    {
        body.SetJerseyMaterials(mat);
    }

    public void DressAsPlayer(PlayerInfo pInfo, TeamInfo tInfo)
    {
        if (pInfo == null || tInfo == null) return;
        body.DressAsPlayer(pInfo, tInfo);
    }

    public void RefreshLIVGear()
    {
        if (livEnabled) HideGearFromLIV();
        else UnhideGearFromLIV();
    }

    public void EnableLIVGear()
    {
        livEnabled = true;
        HideGearFromLIV();
    }

    public void DisableLIVGear()
    {
        livEnabled = false;
        UnhideGearFromLIV();
    }

    public void HideGearFromLIV()
    {
        body.HideGearFromLIV();
    }

    public void UnhideGearFromLIV()
    {
        body.UnhideGearFromLIV();
    }
}
