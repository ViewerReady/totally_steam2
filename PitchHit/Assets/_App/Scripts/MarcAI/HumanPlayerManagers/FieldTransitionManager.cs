﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class FieldTransitionManager : MonoBehaviour {

    public bool slowMotionEnabled = true;
    public bool fadeEnabled = true;

    public static FieldTransitionManager Instance;

    private static OVRScreenFade OculusFade
    {
        get
        {
            if(_oculusFade == null)
            {
                _oculusFade = FindObjectOfType<OVRScreenFade>();
            }
            return _oculusFade;
        }
    }
    private static OVRScreenFade _oculusFade;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void OnEnable()
    {
#if RIFT
        Debug.Log("Registering pause events");
        OVRManager.HMDMounted += UnpauseGameplay;
        OVRManager.HMDUnmounted += PauseGameplay;
#endif
    }

    void OnDisable()
    {
#if RIFT
        Debug.Log("Unregistering pause events");
        OVRManager.HMDMounted -= UnpauseGameplay;
        OVRManager.HMDUnmounted -= PauseGameplay;
#endif
    }

    #region Fade
    public void FadeOut(float seconds)
    {
        if (!fadeEnabled) return;

#if STEAM_VR
        SteamVR_Fade.View(Color.black, seconds);
#elif RIFT
        if (OculusFade)
        {
            OculusFade.fadeTime = seconds;
            OculusFade.FadeOut();
        }
#endif
    }

    public void FadeIn(float seconds)
    {
        if (!fadeEnabled) return;

        //        Debug.Log("FADE IN SCREEN!");
#if STEAM_VR
        SteamVR_Fade.View(Color.clear, seconds);
#elif RIFT
        if (OculusFade)
        {
            OculusFade.fadeTime = seconds;
            OculusFade.FadeIn();
        }
#endif
    }

    private static Coroutine currentFade;
    public void FadeInOut(float inOutDuration, float waitDuration)
    {
        if (!fadeEnabled) return;

        if (currentFade != null)
        {
            StopCoroutine(currentFade);
        }
        currentFade = StartCoroutine(FadeRoutine(inOutDuration, waitDuration));
    }

    protected IEnumerator FadeRoutine(float inoutDuration, float waitDuration)
    {
        FadeOut(inoutDuration);
        yield return new WaitForSeconds(waitDuration + inoutDuration);
        FadeIn(inoutDuration);
    }
#endregion

#region Time
    public AnimationCurve sloMoCurve;

    private static Coroutine currentSlowMo;

    public void EnterSlowMotion()
    {
        if (!slowMotionEnabled) return;

        Debug.Log("SLOW MO");
        if (currentSlowMo != null)
        {
            StopCoroutine(currentSlowMo);
        }
        currentSlowMo = StartCoroutine(SlowMotionRoutine(3f));
    }

    const float initialFixedDeltaTime = .0167f;
    IEnumerator SlowMotionRoutine(float duration)
    {
        


        float t = 0;
        while (t < 1f)
        {
            if (gameIsPaused)
            {
                yield return null;
                continue;
            }
            Time.timeScale = sloMoCurve.Evaluate(t);
            Time.fixedDeltaTime = Time.timeScale * initialFixedDeltaTime;

            t += Time.unscaledDeltaTime / duration;
            yield return null;
        }
        Time.timeScale = 1;


        Time.fixedDeltaTime = initialFixedDeltaTime;
    }

    bool gameIsPaused;

    void PauseGameplay()
    {
        Debug.Log("Pausing game");
        DarkTonic.MasterAudio.MasterAudio.PauseEverything();
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    void UnpauseGameplay()
    {
        Debug.Log("Unpausing game");
        DarkTonic.MasterAudio.MasterAudio.UnpauseEverything();
        Time.timeScale = 1f;
        gameIsPaused = false;
    }
#endregion

}
