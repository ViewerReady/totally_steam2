﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandednessSettingUI : MonoBehaviour {

    [SerializeField] private Button[] buttons;

    private void OnEnable()
    {
        RefreshButtons();
    }

    void RefreshButtons()
    {
        int cur = (int)HandManager.dominantHand;
        for (int i = 0; i<buttons.Length; i++)
        {
            if (i == cur) buttons[i].interactable = false;
            else buttons[i].interactable = true;
        }
    }

    public void SetHandedness(int type)
    {

        HandManager.SetDominantHand((HandManager.HandType)type);
        RefreshButtons();
    }
}