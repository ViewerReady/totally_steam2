﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocomotionSettingUI : MonoBehaviour {

    [SerializeField] private Button[] buttons;

    private void OnEnable()
    {
        RefreshButtons();
    }

    void RefreshButtons()
    {
        int cur = (int)HumanPosessionManager.locomotionType_saved;
        for (int i = 0; i<buttons.Length; i++)
        {
            if (i == cur) buttons[i].interactable = false;
            else buttons[i].interactable = true;
        }
    }

    public void SetLocomotionType(int type)
    {
        HumanPosessionManager.SetLocomotionType((HumanPosessionManager.LocomotionVariant)type);
        RefreshButtons();
    }
}
