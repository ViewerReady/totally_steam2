﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ThrowSpeedUI : MonoBehaviour {

    public float min = 1;
    public float max = 3;

    public Slider slider;

    void Awake()
    {
        slider.value = Pitchable.globalLinearVelocityMod;
    }
    public void SetThrowSpeed(Single speed)
    {
        Pitchable.globalLinearVelocityMod = speed;
    }
}
