﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ThrowAssistUI : MonoBehaviour {

    public Slider slider;

    void Awake()
    {
        slider.value = Pitchable.assistWeight;
    }

    public void SetThrowAssist(Single assist)
    {
        Pitchable.assistWeight = assist;
    }
}
