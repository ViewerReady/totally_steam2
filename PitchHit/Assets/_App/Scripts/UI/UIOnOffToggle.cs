﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOnOffToggle : MonoBehaviour {

    public Button onButton;
    public Button offButton;

    public void Refresh(bool isOn)
    {
        onButton.interactable = !isOn;
        offButton.interactable = isOn;
    }
}
