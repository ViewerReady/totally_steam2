﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnnouncerMouthController : MonoBehaviour {

    Animator mouthAnimator;
    bool isAnimating;

    void Awake()
    {
        mouthAnimator = gameObject.GetComponent<Animator>();
    }

	// Update is called once per frame
	void Update ()
    {
		if(mouthAnimator != null && CalloutManager.isVOPlaying && !isAnimating)
        {
            mouthAnimator.SetFloat("Speed", 1f);
            isAnimating = true;
        }
        else if (mouthAnimator != null && !CalloutManager.isVOPlaying && isAnimating)
        {
            mouthAnimator.SetFloat("Speed", 0f);
            mouthAnimator.Play("AnnouncerMouth", 0, 0);
            isAnimating = false;
        }
    }
}
