﻿using UnityEngine;

public class ParticleScaler : MonoBehaviour {
    public bool scaleAtStart;
    public float scaleToDo;
    public AudioSource audio;
    void Awake()
    {
        if(scaleAtStart)
        {
            MultiplyParticleScale(scaleToDo);
        }

        if(audio)
        {
            audio.transform.SetParent(null);
            Destroy(audio.gameObject,audio.clip.length+1f);
        }
    }

    /*
     * //THIS IS THE WAY MARC SET IT UP FOR THE FULL GAME (MAYBE MAGIC LEAP?)
	public void MultiplyParticleScale(float scale)
    {
        //scale *= WorldScaler.worldScale;
        ParticleSystem particles = GetComponent<ParticleSystem>();
        if(!particles)
        {
            return;
        }
        
                //ParticleSystem.MainModule psMain = particles.main;
                //ParticleSystem.MinMaxCurve tempCurve = psMain.startSpeed;
                //tempCurve.curveMultiplier = scale;
                //psMain.startSpeed = tempCurve;

                //tempCurve = psMain.startSize;
                //tempCurve.curveMultiplier = scale;
                //psMain.startSize = tempCurve;

                //tempCurve = psMain.startLifetime;
                //tempCurve.curveMultiplier = scale;
                //psMain.startLifetime = tempCurve;

                //particles.Emit(1);

        ParticleSystem.MainModule newMain = particles.main;
        newMain.startSpeedMultiplier = scale;
        newMain.startSizeMultiplier = scale;

        ParticleSystem.ForceOverLifetimeModule newForce = particles.forceOverLifetime;
        newForce.xMultiplier = scale;
        newForce.yMultiplier = scale;
        newForce.zMultiplier = scale;
        //newMain.multipl

        particles.startSize    *= scale;
        //particles.startLifetime*= scale;//Mathf.Sqrt(scale);
        transform.localScale   *= scale;
    }
    */

    //Greg: This is the way that worked in Pitch-Hit 
    public void MultiplyParticleScale(float scale)
    {
        ParticleSystem particles = GetComponent<ParticleSystem>();
        if (!particles)
        {
            return;
        }
        particles.startSpeed *= scale;
        particles.startSize *= scale;
        particles.startLifetime *= Mathf.Sqrt(scale);
        transform.localScale *= scale;
    }
}
