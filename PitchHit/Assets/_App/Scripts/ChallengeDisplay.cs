﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChallengeDisplay : MonoBehaviour {
    private TextMeshPro textMesh;

    private GameController gameController;
    private ballController ballController;

    void Awake()
    {
        textMesh = GetComponent<TextMeshPro>();
        textMesh.enabled = false;
    }

    void Start () {
        
        if (LockerRoomLevelController.isDemo)
        {
            Destroy(gameObject);
            return;
        }

        gameController = FindObjectOfType<GameController>();

        if (gameController)
        {
            int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");
            Debug.Log("levels unlocked:"+levelsUnlocked+" == "+gameController.levelNumber);
            if (levelsUnlocked == gameController.levelNumber)
            {
                Debug.Log("good.");
                textMesh.enabled = true;
            }


            if (!textMesh.enabled)
            {
                Debug.Log("bad.");
                Destroy(gameObject);
            }
            else
            {
                ballController = gameController.GetComponent<ballController>();
            }
        }
        else
        {
            Debug.Log("no game controller.");
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
		if(ballController.activeBalls!=null && ballController.activeBalls.Count>0)
        {
            Destroy(gameObject);
        }
	}
    
}
