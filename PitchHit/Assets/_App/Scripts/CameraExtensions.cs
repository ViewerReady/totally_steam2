﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraExtensions {

    static private Camera m_cachedMainCamera;
    static public Camera mainCamera
    {
        get
        {
            if(m_cachedMainCamera == null)
            {
                m_cachedMainCamera = Camera.main;
            }
            return m_cachedMainCamera;
        }
        private set { }
    }
}
