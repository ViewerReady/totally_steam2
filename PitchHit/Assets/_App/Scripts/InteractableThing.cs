﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Demonstrates how to create a simple interactable object
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace Valve.VR.InteractionSystem
{
	//-------------------------------------------------------------------------
	[RequireComponent( typeof( Interactable ) )]
	public class InteractableThing : MonoBehaviour
	{
        SteamVR_Action_Boolean grabObject;
        private Vector3 oldPosition;
		private Quaternion oldRotation;
        private bool beingHeld;
		private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & ( ~Hand.AttachmentFlags.SnapOnAttach ) & ( ~Hand.AttachmentFlags.DetachOthers );
        
        void Awake()
        {
            grabObject = SteamVR_Actions.baseballSet_GrabObject;
        }

		//-------------------------------------------------
		// Called every Update() while a Hand is hovering over this object
		//-------------------------------------------------
		private void HandHoverUpdate( Hand hand )
		{
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            if (grabObject.GetLastStateDown(hand.handType))
			{
				if ( hand.currentAttachedObject != gameObject )
				{
                    beingHeld = true;
					// Save our position/rotation so that we can restore it when we detach
					oldPosition = transform.position;
					oldRotation = transform.rotation;

					// Call this to continue receiving HandHoverUpdate messages,
					// and prevent the hand from hovering over anything else
					hand.HoverLock(GetComponent<Interactable>());

					// Attach this object to the hand
					hand.AttachObject( gameObject, GrabTypes.Trigger,attachmentFlags );
				}
			}
            else if(!grabObject.GetLastState(hand.handType))
            {
                beingHeld = false;
                // Detach this object from the hand
                hand.DetachObject(gameObject);

                // Call this to undo HoverLock
                hand.HoverUnlock(GetComponent<Interactable>());

                // Restore position/rotation
                //transform.position = oldPosition;
                //transform.rotation = oldRotation;
            }
        }
        
        void Update()
        {

            if (beingHeld)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    transform.localScale *= .94f;
                }
                if(Input.GetKeyDown(KeyCode.RightArrow))
                {
                    transform.localScale *= 1.1f;
                }
            }
        }
	}
}
