﻿using UnityEngine;
using System.Collections;

public class DuckLevelController : MonoBehaviour
{

    public int levelNumber = 1;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredPoints = 200;
    public AppearingMenu appearingMenu;


    public void CheckIfChallengePassed()
    {
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if (levelsUnlocked == levelNumber)
        {
            if (ScoreboardController.instance.myScore >= requiredPoints)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (sound && successClip)
                    {
                        sound.clip = successClip;
                        sound.Play();
                    }
                }
            }
        }
    }
}
