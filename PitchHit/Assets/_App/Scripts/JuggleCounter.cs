﻿//Hikmat
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JuggleCounter : MonoBehaviour
{
    public Text Best;
    public Text Current;
    private int bestJuggle = 0;
    public  JuggleCount currentJuggleBall;

    public int levelNumber = 2;
    public AppearingMenu appearingMenu;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredPoints = 10;

    public GameObject teeContainer;
    public GameObject teeBody;
    //public GameObject teeGrip;
    public spawnBall teeSpawner;

    private LeaderboardManager leaderboard;
    // Use this for initialization
    void Start()
    {
        leaderboard = FindObjectOfType<LeaderboardManager>();

        if(teeBody)
        {
            if(GameController.fullBatterMode)
            {
                teeContainer.SetActive(true);
                teeBody.SetActive(true);
                StartCoroutine(MaintainTeeRoutine());
                if(teeSpawner)
                {
                    teeSpawner.StartAutomaticSpawning(ballController.instance.ballPrefab,ballController.ballServiceType.tee);
                }
            }
            else
            {
                teeBody.SetActive(false);
                teeContainer.SetActive(false);

            }
        }

    }

    IEnumerator MaintainTeeRoutine()
    {
        while(true)
        {
            yield return null;
            teeContainer.SetActive(true);
            teeBody.SetActive(true);
        }
    }

    public void BeginJuggling(JuggleCount ball)
    {
        currentJuggleBall = ball;
    }

    public void FinalizeJuggle()
    {
        if (currentJuggleBall)
        {
            if (currentJuggleBall.numOfJuggling >= bestJuggle)
            {
                bestJuggle = currentJuggleBall.numOfJuggling;
                currentJuggleBall.numOfJuggling = 0;              //after hitting to land becomes 0
                ScoreboardController.instance.SetScore(bestJuggle);
                if (leaderboard && bestJuggle > 4)
                {
                    leaderboard.CommitScoreToLeaderboard(ScoreboardController.instance.myScore);
                }
                CheckIfChallengePassed();
            }
            currentJuggleBall = null;
        }
        Best.text = bestJuggle.ToString();

    }
    ///public void ObjectReference()
    //{
    //    juggle = GameObject.FindGameObjectWithTag("ball_in_use").GetComponent<JuggleCount>();
    //}
    public void ShowJuggle()
    {
        Current.text = currentJuggleBall.numOfJuggling.ToString();
    }

    public void CheckIfChallengePassed()
    {
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");
        Debug.Log("check if passed. " + levelsUnlocked +" == "+levelNumber);
        if (levelsUnlocked == levelNumber)
        {
            //Debug.Log("score: "+ScoreboardController.instance.score+" > "+requiredPoints);
            if (ScoreboardController.instance.myScore >= requiredPoints)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (sound && successClip)
                    {
                        sound.clip = successClip;
                        sound.Play();
                    }
                }
            }
        }
    }
}
