﻿#if REFERENCE
using UnityEngine;
using System.Collections;

public class VRBasketBall : VRThrowable {
    public Rigidbody rb;
    //[HideInInspector]
    //public Vector3 StartingPositon; //For storing it's intial position Michael Taylor
    [HideInInspector]
	public float BallReturnTimer; //For deciding when the ball will comeback
    private float BallReturnLimit = 5f; //once the BallReturnTimer excedes this it will return the ball
    private bool IsOutOfBounds; //Addition to the ReturnBasketBall() #Michael Taylor
    private bool IsHitOutOfBounds; //If hit ground while out of bounds this will activate #Michael Taylor
    private bool HoopInRange;
    [HideInInspector]
    public bool hasBeenShot;
    public bool HasScored;
    public static SoundController soundController;
    public bool hasBeenGrabbed;
    public float throwSpeedMultiplier = 1f;
    public bool discoverable = true;
    public bool isBuzzerBeater;

    public int numberOfRimHits;
    public bool HasHitBackBoard;

    public static int consecutiveShotsMade;
    private static int shotsNeededForFire = 2;

    [HideInInspector]
    public bool IsOnFire; //#michael taylor varible for on fire text

    public GameObject FireParticle; //To make the ball look like it's on fire. #michael taylor
    private Quaternion initialFireRotation;

    private Renderer debugRenderer;
    private static TimerController timeController;
    public static HoopController theHoop;
    private SphereCollider col;
	private BallTracker tracker;

	public static float playAreaMinX = Mathf.Infinity;// = -11.5f;
	public static float playAreaMaxX = -Mathf.Infinity;// = -8f;
	public static float playAreaMinZ = Mathf.Infinity;// = -1.3f;
	public static float playAreaMaxZ = -Mathf.Infinity;// = 1.25f;
	private static float playAreaMaxZBuffer = .4f;
	private static float offsetZ;
    public static Vector3 waitingPosition;
    public static Vector3 opponentWaitingPosition;

    [HideInInspector]
    public bool initiallyMine = false;
    [HideInInspector]
    public bool mostRecentlyMine = false;
    [HideInInspector]
    public bool isInWaiting;

	public static void SetOffset(float offset)
	{
		offsetZ = offset;
	}

    // Use this for initialization
    void Start()
    {
        debugRenderer = GetComponentInChildren<Renderer>();
        if (!soundController)
        {
            soundController = FindObjectOfType<SoundController>();
        }
        
        rb = GetComponent<Rigidbody>();
        //StartingPositon = transform.position; //gets the position
        if (!timeController)
        {
            timeController = FindObjectOfType<TimerController>(); //gets hold of the timer for scoring purposes #Michael Taylor
        }

        FireParticle = GetComponentInChildren<ParticleSystem>(true).gameObject;
        initialFireRotation = FireParticle.transform.rotation;

        //FireParticle = gameObject.transform.FindChild("CFXM4 Flamme Soft").gameObject; //gets the particle child as the variable #Michael Taylor
        FireParticle.SetActive(false); //turned off for presentation and design purposes #Michael Taylor

        col = GetComponent<SphereCollider>();

		tracker = GetComponent<BallTracker> ();
        if (tracker==null || tracker.photonView.isMine)
        {
            initiallyMine = true;
            mostRecentlyMine = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tracker == null || tracker.photon.isMine)
        {
            ReturnBasketBall();
        }

        if(FireParticle.activeSelf)
        {
            FireParticle.transform.rotation = initialFireRotation;
        }
		//if (tracker.photonView.isMine) {
			//This is only for the text to pop up (Temporary solution) #michaeltaylor
		//	if (consecutiveShotsMade == shotsNeededForFire - 1) {
		//		IsOnFire = true;
		//	}
		//}
    }

    void OnAttachedToHand(BasketBallHand hand)
    {
		if (tracker && !tracker.photonView.isMine) {
			return;
		}

		if (tracker) {
			tracker.isHeld = true;
			if (hand.currentHandType == VRHand.HandType.Right) {
				tracker.isHeldByRightHand = true;
			} else {
				tracker.isHeldByRightHand = false;
			}
		}

        mostRecentlyMine = true;
        hasBeenGrabbed = true;
        attached = true;
        attachedHand = hand;
        //hand.HoverLock(null);
        IsHitOutOfBounds = false; // added to make sure that the ball does not dissapear abruptly when it does goe out of bounds. #MichaelTaylor
        HasScored = false; //So that the player can score multiple times with the same ball
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.interpolation = RigidbodyInterpolation.None;

        velocityEstimator.BeginEstimatingVelocity();

        Collider[] otherBalls = Physics.OverlapSphere(transform.position, col.radius);//,hand.ballsLayer.value);
        //Debug.Log("just grabbed ball overlapping with "+otherBalls.Length);
        foreach (Collider c in otherBalls)
        {
            //Debug.Log(c.name);
            if (c.gameObject != gameObject)
            {
                VRBasketBall otherBall = c.GetComponent<VRBasketBall>();
                if (otherBall)
                {
                    //Debug.Log("it's a ball");
                    if (otherBall.attached)
                    {
                        //Debug.Log("it's attached!");
                        (otherBall.attachedHand as BasketBallHand).CancelGrip();
                        otherBall.attachedHand.DetachObject(otherBall.gameObject);
                        //otherBall.LateDetach(otherBall.attachedHand);
                    }
                }
            }
        }

    }

    public bool IsAttached()
    {
        return attached;
    }

    void OnDetachedFromHand(BasketBallHand hand)
    {
        if (tracker)
        {
            if (!tracker.photonView.isMine && !mostRecentlyMine)
            {
                return;
            }
        }

        if (timeController.hasStarted && !timeController.hasFinished)
            isBuzzerBeater = true;
        if (tracker) {
			tracker.isHeld = false;
		}
        //Debug.Log("detach basket ball.");
        attached = false;
        attachedHand = null;
        hand.HoverUnlock(null);

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.interpolation = RigidbodyInterpolation.Interpolate;

        velocityEstimator.FinishEstimatingVelocity();
        //Debug.Log("my velo:" + velocityEstimator.GetVelocityEstimate() + " hand velo:" + hand.GetVelocity());

        Vector3 velocity = velocityEstimator.GetVelocityEstimate();
        Vector3 angularVelocity = velocityEstimator.GetAngularVelocityEstimate();
        rb.velocity = velocity * throwSpeedMultiplier;
        rb.angularVelocity = angularVelocity;

        // Make the object travel at the release velocity for the amount
        // of time it will take until the next fixed update, at which
        // point Unity physics will take over
        /*
        float timeUntilFixedUpdate = (Time.fixedDeltaTime + Time.fixedTime) - Time.time;
        transform.position += timeUntilFixedUpdate * velocity;
        float angle = Mathf.Rad2Deg * angularVelocity.magnitude;
        Vector3 axis = angularVelocity.normalized;
        transform.rotation *= Quaternion.AngleAxis(angle * timeUntilFixedUpdate, axis);
        */
        StartCoroutine(ThrowingRoutine(hand));

    }

    IEnumerator ThrowingRoutine(BasketBallHand throwingHand)
    {
        if (tracker)
        {
            if (!tracker.photonView.isMine && !mostRecentlyMine)
            {
                yield break;
            }
        }
        discoverable = false;
        if (debugRenderer)
        {
            //debugRenderer.material.color = Color.green;
        }
        //Debug.Log("start throwing routine.");
        Rigidbody rb = GetComponent<Rigidbody>();
        bool stillPushing = true;

        while (stillPushing)
        {
            yield return null;
            Vector3 currentHandPosition = throwingHand.grippedBallLocation.position;
            Vector3 currentHandForward = throwingHand.grippedBallLocation.forward;
            //Vector3 gripVelocity = (currentHandPosition - previousHandPosition)/Time.deltaTime;
            Vector3 gripVelocity = throwingHand.GetVelocity() * throwSpeedMultiplier;

            //Debug.Log("still pushing "+gripVelocity.ToString("F5") + " - "+rb.velocity);

            float angle = Vector3.Angle(gripVelocity, rb.velocity);
            if (angle < 90)
            {
                //gripVelocity = gripVelocity.normalized * (gripVelocity.magnitude * Mathf.Cos(angle));
                //Vector3 gripAngularVelocity = Quaternion.FromToRotation(previousHandForward, currentHandForward).eulerAngles *Time.deltaTime;
                Vector3 gripAngularVelocity = throwingHand.GetAngularVelocity();
                // gripAngularVelocity = gripAngularVelocity.normalized * (gripAngularVelocity.magnitude * Mathf.Cos(Vector3.Angle(gripAngularVelocity, throwingHand.grippedBallLocation.right)));

                if (gripVelocity.sqrMagnitude < rb.velocity.sqrMagnitude)
                {
                    //Debug.Log("too slow. not pushing anymore "+ gripVelocity.sqrMagnitude +" < "+rb.velocity.sqrMagnitude);
                    stillPushing = false;
                }
                else
                {
                    rb.velocity = rb.velocity.normalized * gripVelocity.magnitude;

                    //if(gripAngularVelocity.sqrMagnitude>rb.angularVelocity.sqrMagnitude)
                    {
                        rb.angularVelocity = -20f * gripAngularVelocity;
                    }
                }
            }
            else
            {
                //Debug.Log("going the wrong direction. not pushing anymore. " + angle);
                stillPushing = false;
            }
        }
        if (debugRenderer)
        {
            //debugRenderer.material.color = Color.red;
        }

        if (rb.velocity.sqrMagnitude > .5f && rb.velocity.y > 0)
        {
            hasBeenShot = true;
            if (!tracker || tracker.photonView.isMine)
            {
                if (consecutiveShotsMade >= shotsNeededForFire)
                {
                    IsOnFire = true;
                    FireParticle.SetActive(true);
                }
            }
            else
            {
                FireParticle.SetActive(IsOnFire);
            }
            //fire
        }
        yield return new WaitForSeconds(.5f);

        discoverable = true;

        if (debugRenderer)
        {
            //debugRenderer.material.color = Color.white;
        }
    }

    void HandAttachedUpdate(BasketBallHand hand)
    {
        if (tracker)
        {
            if (!tracker.photonView.isMine && !mostRecentlyMine)
            {
                return;
            }
        }
        //Debug.Log("my velo:" + velocityEstimator.GetVelocityEstimate() + " hand velo:" + hand.GetVelocity());

        //Trigger got released
        //if (hand.GetStandardInteractionButtonUp())
        if (!hand.GetStandardInteractionButton())
        {
            // Detach ourselves late in the frame.
            // This is so that any vehicles the player is attached to
            // have a chance to finish updating themselves.
            // If we detach now, our position could be behind what it
            // will be at the end of the frame, and the object may appear
            // to teleport behind the hand when the player releases it.
            StartCoroutine(LateDetach(hand));
        }
        else
        {


            Collider[] otherBalls = Physics.OverlapSphere(transform.position, col.radius);//,hand.ballsLayer.value);

            foreach (Collider c in otherBalls)
            {
                //Debug.Log(c.name);
                if (c.gameObject != gameObject)
                {
                    VRBasketBall otherBall = c.GetComponent<VRBasketBall>();
                    if (otherBall)
                    {
                        if (otherBall.attached)
                        {
                            (attachedHand as BasketBallHand).CancelGrip();
                            (otherBall.attachedHand as BasketBallHand).CancelGrip();
                            LateDetach(attachedHand);
                            otherBall.LateDetach(otherBall.attachedHand);
                        }
                    }
                }
            }






            

        }
    }
    //Michael Taylor function
    private bool primedToScore;
    void OnTriggerEnter(Collider other)
    {
        if (tracker)
        {
            if (!tracker.photonView.isMine && !mostRecentlyMine)
            {
                return;
            }
        }
        if (HasScored)
        {
            return;
        }

        //Will check if the hoop is in range
        if (other.gameObject.tag == "ScoreDistance")
        {
            hasBeenShot = true;
            HoopInRange = true;
            return;
        }

        if (other.gameObject == theHoop.gameObject && transform.position.y > other.transform.position.y)
        {
            primedToScore = true;
        }


        /*
        //Will ONLY score when it hits the collider and it's falling down. And can only score when the timer is still going
        HoopController hoopController = other.gameObject.GetComponent<HoopController>();
        if (hoopController && GetComponent<Rigidbody>().velocity.y < -1f && timeController.IsTicking == true)
        {
            consecutiveShotsMade++;
            hoopController.ScoreBall(this,(consecutiveShotsMade==shotsNeededForFire));
            //other.gameObject.GetComponentInChildren<ScoreKeeper>().AddScore(1);
            //GetComponent<Collider>().enabled = false; //turns off the collider
            //Invoke("ResetCollider", 0.25f); //the function to turn on the collider
            HoopInRange = false; //this will make sure that the negative words come up
            HasScored = true;
        }
        */

    }

    void OnTriggerExit(Collider other)
    {
        if (tracker)
        {
            if (!tracker.photonView.isMine && !mostRecentlyMine)
            {
                return;
            }
        }
        if(!mostRecentlyMine)
        {
            return;
        }

        if(HasScored || timeController.hasFinished || !timeController.hasStarted || !mostRecentlyMine) 
        {
            //Debug.Log("OnTriggerExit return because: " + (HasScored? "has scored, ": "") + (timeController.hasFinished ? "time has finished, " : "") + (!timeController.hasStarted ? "time has not started, " : "") + (!mostRecentlyMine ? "ball was not yours " : ""));
            return;
        }

        //Will ONLY score when it hits the collider and it's falling down. And can only score when the timer is still going
        if (primedToScore)
        {
            if (other.gameObject == theHoop.gameObject && transform.position.y < other.transform.position.y && GetComponent<Rigidbody>().velocity.y < -1f && ((timeController.hasStarted && !timeController.hasFinished) && isBuzzerBeater))
            {
                consecutiveShotsMade++;
                theHoop.ScoreBall(this, (consecutiveShotsMade == shotsNeededForFire));
                //other.gameObject.GetComponentInChildren<ScoreKeeper>().AddScore(1);
                //GetComponent<Collider>().enabled = false; //turns off the collider
                //Invoke("ResetCollider", 0.25f); //the function to turn on the collider
                HoopInRange = false; //this will make sure that the negative words come up
                HasScored = true;
                primedToScore = false;
            }
        }

        //Debug.Log("--------------------------------------------------------------------------exited something "+other.name);
        //will only activate if it fails to score #michael taylor
        if (other.gameObject.tag == "ScoreDistance")
        {
            //Debug.Log("exited hoop range");
            HasScored = true;
            HoopInRange = false;
            //other.gameObject.transform.parent.GetComponentInChildren<HoopController>().SpawnNegativeWords();
            consecutiveShotsMade = 0;
            FireParticle.SetActive(false);
            IsOnFire = false;

            if (!HasHitBackBoard)
            {
               // Debug.Log("--------------------------------------------------------no backboard hit "+numberOfRimHits);
                if (numberOfRimHits == 1)
                {
                    //Debug.Log("brick");
                    theHoop.BrickWord();
                }
                else if (numberOfRimHits == 0)
                {
                    //Debug.Log("airball");
                    theHoop.AirballWord();
                }
                else
                {
                   // Debug.Log("some other number of rim hits.");
                    theHoop.SpawnNegativeWords();

                }
            }
            else
            {
                //Debug.Log("hit the backboard. regular miss.");
                theHoop.SpawnNegativeWords();
            }

            return;       
        }

        
    }
    //Resets the capsule collider
    void ResetCollider()
    {
        GetComponent<Collider>().enabled = true;
    }

	//Michael Taylor function
    public void ReturnBasketBall()
    {

		//if (shouldDebug) {
			//Debug.DrawRay (new Vector3(playAreaMinX, 1.0f, playAreaMinZ), Vector3.up*10f,Color.blue);
			//Debug.DrawRay (new Vector3(playAreaMinX, 1.0f, playAreaMaxZ), Vector3.up*10f,Color.yellow);
			//Debug.DrawRay (new Vector3(playAreaMaxX, 1.0f, playAreaMinZ), Vector3.up*10f,Color.green);
			//Debug.DrawRay (new Vector3(playAreaMaxX, 1.0f, playAreaMaxZ), Vector3.up*10f,Color.cyan);
        //}

		//if the ball steps out of any of these boundries it will start the timer. the last condition is the floor #maic
		if (transform.position.x < playAreaMinX || transform.position.x > playAreaMaxX || transform.position.z > (playAreaMaxZ) || transform.position.z < playAreaMinZ)
		{

			IsOutOfBounds = true;
			//destroy immediately x< -20 or x>20 or z> 10 or z< -10

			if(transform.position.y<2f)
			{
				if (transform.position.x < -20f || transform.position.x > 20f || transform.position.z > 10f || transform.position.z < -10f || transform.position.y<-1f)
				{
					StartCoroutine(ReturningRoutine());
					return;
				}
			}
		}
		else //if not it will stay at zero
		{ 
			IsOutOfBounds = false;
			BallReturnTimer = BallReturnLimit;
		}

        if (tracker)
        {
            if (!tracker.photonView.isMine && tracker.isHeld)
            {
                BallReturnTimer = BallReturnLimit;
            }
        }
        /*
        if (IsOutOfBounds || IsHitOutOfBounds)
        {
            if (IsOutOfBounds && IsHitOutOfBounds)
            {
                debugRenderer.material.color = Color.red;
            }
            else
            {
                if (IsOutOfBounds)
                {
                    debugRenderer.material.color = Color.cyan;
                }
                else
                {
                    debugRenderer.material.color = Color.blue;
                }
            }
        }
        else
        {
            debugRenderer.material.color = Color.green;
        }
        */
   
        if (IsOutOfBounds && IsHitOutOfBounds)
        {
            BallReturnTimer -= Time.deltaTime;
        }

        //Once it exedes this it will return the ball
        if (BallReturnTimer <=0)
        {
            StartCoroutine(ReturningRoutine());
        }
    }

    public void OnRespawned()
    {
        if (!rb)
        {
            rb = GetComponent<Rigidbody>();
        }
        BallReturnTimer = BallReturnLimit;
        primedToScore = false;
        numberOfRimHits = 0;
        FireParticle.SetActive(false);
        IsOnFire = false;
        HasScored = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = false;
        isInWaiting = false;
        IsHitOutOfBounds = false;

        StartCoroutine(LateUpdateTracker());
    }

    //to be used after the ball is put back on the rack to make sure the tracker represents this to the server.
    IEnumerator LateUpdateTracker()
    {
        yield return null;

        if (tracker)
        {
            tracker.isHeld = false;
            tracker.ForcePhotonUpdate();
        }
    }

    //this needs to be tweaked so that if the ball passes through the net when it straight line teleports back to the rack, it doesn't freak out the net.
    public IEnumerator ReturningRoutine()
	{
        if(!initiallyMine)
        {
            while (tracker.photon.isMine)
            {
                BallReturnTimer = 0;
                transform.position = opponentWaitingPosition;
                yield return null;
            }
            yield break;
        }

        if (tracker)
        {
            while (tracker.photonView.isMine == false)
            {
                yield return null;
            }
        }

		FireParticle.SetActive(false);
        IsOnFire = false;
		HasHitBackBoard = false;
		numberOfRimHits = 0;
		BallReturnTimer = BallReturnLimit;

		HasScored = false;
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;
        rb.isKinematic = true;
        primedToScore = false;
        transform.position = waitingPosition;

        if (tracker)
        {
            yield return new WaitForSeconds(.1f);
        }
        isInWaiting = true;
        
        //gameObject.SetActive(false);
    }

    private bool shouldIgnoreThisSound;
    public void IgnoreNextSound()
    {
        shouldIgnoreThisSound = true;
    }

    void OnCollisionStay(Collision c)
    {
        if(!IsHitOutOfBounds)
        {
            if (c.gameObject.tag == "Ground")
            {
                IsHitOutOfBounds = true; //Will trigger, but won't do anything until out of bounds, and resets to false in the OnAttachedToHand function;
            }
        }

    }

    void OnCollisionEnter(Collision c)
    {
        if(isInWaiting || transform.position.y<0)
        {
            return;
        }
        
        //Debug.Log("collided "+c.gameObject.tag);
        if (shouldIgnoreThisSound)
        {
            shouldIgnoreThisSound = false;
            return;
        }

        if (c.gameObject.tag == "Ground")
        {
            if (HoopInRange)
            {
                HasHitBackBoard = true;
            }
            //Debug.Log("collision with ground of speed "+c.relativeVelocity.magnitude);
            //Debug.Log("collided with ground.");
            soundController.PlayBallToGroundSoundAt(c.contacts[0].point,1.1f* c.relativeVelocity.magnitude / 10f);
            IsHitOutOfBounds = true; //Will trigger, but won't do anything until out of bounds, and resets to false in the OnAttachedToHand function;          
        }
        else if (c.gameObject.tag == "Hoop")
        {
            //Debug.Log("collision with hoop of speed " + c.relativeVelocity.magnitude);
            if (soundController.PlayRimSoundAt(c.contacts[0].point, 1.1f * c.relativeVelocity.magnitude / 10f))
            {
                numberOfRimHits++;
            }
        }
        else if (c.gameObject.tag == "Backboard")
        {
            //if (HoopInRange)
            //{
                HasHitBackBoard = true;
            //}
            // Debug.Log("collision with backboard of speed " + c.relativeVelocity.magnitude);
            soundController.PlayBackboardSoundAt(c.contacts[0].point, 1.1f * c.relativeVelocity.magnitude / 10f);
            //HasHitBackBoard = true;
        }
        else
        {
            VRBasketBall tempBall = c.gameObject.GetComponent<VRBasketBall>();
            if (tempBall && !tempBall.isInWaiting)
            {
                //Debug.Log("collision with other ball of speed " + c.relativeVelocity.magnitude);
                tempBall.IgnoreNextSound();
                if (c.contacts != null && c.contacts.Length > 0)
                {
                    soundController.PlayBallToBallSoundAt(c.contacts[0].point, 1.1f * c.relativeVelocity.magnitude / 10f);
                }
            }
        }
    }
}
#endif