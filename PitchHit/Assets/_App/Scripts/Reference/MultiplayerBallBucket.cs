﻿using UnityEngine;
using System.Collections;

public class MultiplayerBallBucket : MonoBehaviour {

    public float radius;
    public float returnRate;
    public Transform[] spawnPoints;
    public hittable[] ballPool;
    public GameObject ballRackStructure;
    //public LayerMask mask;

    // WORK IN PROGRESS
    // Use this for initialization
    void Start ()
    {
		
        //ballRackStructure.SetActive(false);
        //BallCheckTimer = new float[spawnPoints.Length];
	}
	
    public Transform[] GetSpawnPoints()
    {
        return spawnPoints;
    }

    public void SetBallPool(hittable[] newPool)
    {
        ballPool = newPool;
    }
    /*
    public void StartReplacementRoutine()
    {
        Debug.Log("start replacement reoutine");
        if (!replacementStarted)
        {
            StartCoroutine(BallReplacementRoutine());
            replacementStarted = true;
        }
    }

	IEnumerator BallReplacementRoutine()
    {
        VRBasketBall.waitingPosition = (transform.position - Vector3.up * 2f);
        VRBasketBall.opponentWaitingPosition = new Vector3(-VRBasketBall.waitingPosition.x, VRBasketBall.waitingPosition.y, -VRBasketBall.waitingPosition.z);
        Debug.Log("started ball replacement routine ."+VRBasketBall.waitingPosition+"        op:"+VRBasketBall.opponentWaitingPosition);
        yield return null;
        Debug.DrawRay(SpawnPoints[0].position, Vector3.forward, Color.red,10f);
        Debug.DrawRay(SpawnPoints[0].position, Vector3.up, Color.blue,10f);
        Debug.Log ("start ball replacement routine.");
        ballRackStructure.SetActive(true);
        TimerController time = FindObjectOfType<TimerController>();
        yield return null;

        while (!time.hasFinished)
        {
            for(int t=0;t<SpawnPoints.Length;t++)
            {
				Collider[] temp = Physics.OverlapSphere (SpawnPoints [t].position, Radius); 
				if (temp.Length == 0) {
                    BallCheckTimer[t] += 1f * Time.deltaTime;

                    if (BallCheckTimer[t] >= ReturnRate)
                    {
                        ReplaceBall(t);
                        BallCheckTimer[t] = 0;
                    }

                } else {
                    BallCheckTimer[t] = 0;
                    //Debug.DrawRay (SpawnPoints [t].position, Vector3.forward, Color.red);
					//Debug.DrawRay (SpawnPoints [t].position, Vector3.up, Color.blue);
				}
                yield return null;

            }
        }
    }

    void ReplaceBall(int ColliderIdentifier)
    {
        for(int i = 0; i < BasketballPool.Length; i++)
        {
            if ((!BasketballPool[i].gameObject.activeSelf) || (BasketballPool[i].isInWaiting && BasketballPool[i].transform.position.y<0))
            {
                BallCheckTimer[ColliderIdentifier] = 0;

                BasketballPool[i].transform.position = SpawnPoints[ColliderIdentifier].transform.position;
                BasketballPool[i].gameObject.SetActive(true);
                //temp.StartingPositon = SpawnPoints[ColliderIdentifier].localPosition;
                BasketballPool[i].isInWaiting = false;
                BasketballPool[i].OnRespawned();
                //Debug.Log(i);
                break;
            }
        }
    }
    */
}
