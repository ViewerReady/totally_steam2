﻿#if REFERENCE
using UnityEngine;
using System.Collections;

public class TorsoTracker : MonoBehaviour {

    public Transform myParent;
    private float newX;
    private float newY;
    private float newZ;
    public int lookThreshold;
    public bool isMine;
    public Material homeJersey;
    public Material awayJersey;
    public Renderer jerseySkin;

    //public Material homeHeadBand;
    //public Material awayHeadBand;
    public Renderer headBandSkin;

    void Start()
    {
        if(SideController.playerNumber==1)
        {
            if(isMine)
            {
                jerseySkin.material = homeJersey;
                headBandSkin.material = homeJersey;
            }
            else
            {
                jerseySkin.material = awayJersey;
                headBandSkin.material = awayJersey;
            }
        }
        else
        {
            if (isMine)
            {
                jerseySkin.material = awayJersey;
                headBandSkin.material = awayJersey;
            }
            else
            {
                jerseySkin.material = homeJersey;
                headBandSkin.material = homeJersey;
            }
        }
    }

    void Update () {
        if (isMine)
        {
            UpdateOrientation();
        }

        /*
        transform.localRotation = Quaternion.identity;

        newX = AngleAroundAxis(Vector3.up, myParent.transform.up, myParent.transform.right);

        if (newX < lookThreshold)
        {
            transform.Rotate(Vector3.right, -newX);
        }
        else if (newX >= lookThreshold && newX < 90)
        {
            transform.Rotate(Vector3.right, -lookThreshold);
        }
        else if (newX >= 90)
        {
            transform.Rotate(Vector3.right, -newX - (360- lookThreshold));
        }



        Vector3 tempGuideZ = transform.up;
        if(newX > 0)
        {
            tempGuideZ -= transform.forward;
        }
        else
        {
            tempGuideZ += transform.forward;
        }

        newZ = AngleAroundAxis(Vector3.up, tempGuideZ, transform.forward);

        //Debug.DrawRay(transform.position, tempGuideZ, Color.red);
        //Debug.Log((int)newZ);
        transform.Rotate(Vector3.forward, -newZ);
        */

    }

    public void UpdateOrientation()
    {
        float headTilt = AngleAroundAxis(Vector3.up, myParent.up, myParent.right);
        float targetTilt = Mathf.Clamp(headTilt - lookThreshold, 0, 45f);
        float torsoTilt = AngleAroundAxis(Vector3.up, transform.up, transform.right);
        transform.Rotate(transform.right, targetTilt - torsoTilt,Space.World);

        //targetRoll is zero
        float torsoRoll = AngleAroundAxis(Vector3.up, transform.up, transform.forward);
        transform.Rotate(transform.forward, -torsoRoll,Space.World);

        /*
        Vector3 faceForwardForYaw = myParent.forward;
        Vector3 theUp = Vector3.up;

        Vector3.OrthoNormalize(ref faceForwardForYaw, ref theUp);

        float yawToFaceForward = AngleAroundAxis(transform.forward, faceForwardForYaw, transform.up);
        transform.Rotate(Vector3.up, yawToFaceForward);
        */
        //transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 0f, transform.localEulerAngles.z);
    }

    // The angle between dirA and dirB around axis
    public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
    {
        // Project A and B onto the plane orthogonal target axis
        dirA = dirA - Vector3.Project(dirA, axis);
        dirB = dirB - Vector3.Project(dirB, axis);

        // Find (positive) angle between A and B
        float angle = Vector3.Angle(dirA, dirB);

        // Return angle multiplied with 1 or -1
        return angle * (Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) < 0 ? -1 : 1);
    }

   
}
#endif