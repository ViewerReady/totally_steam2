﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MenuButton : MonoBehaviour {
    [SerializeField]
    Image image;
    public bool isActivated = false;
    Color offColor = Color.black;
    Color onColor = Color.green;
    public UnityEvent onHover;
    public UnityEvent onClick;
    public UnityEvent onLeave;

    public UnityEvent onActivated;
    public UnityEvent onDeactivated;
    [SerializeField]
    float onTime;
    [SerializeField]
    float canHoverTime = 1.5f;

    bool canHover = true;

    [SerializeField]
    Image offImage;
    [SerializeField]
    Image onImage;
    [SerializeField]
    Image[] hoverImages;
    [SerializeField]
    float hoverPlaySpeed;

    public List<MenuButton> otherRadioButtons;
    public enum ButtonType
    {
        Radio,
        OnOff,
        OnSetTime,
        OnForever
    }
    public ButtonType buttonType;

	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update() {

	}

    public void Click()
    {
        Debug.Log("Click");
        if (!isActivated)
        {
            Activate();
        }
        else if (isActivated && buttonType == ButtonType.OnOff)
        {
            Deactivate();
        }

        onClick.Invoke();
    }

    public void Hover()
    {
        if (canHover)
        {
            if (buttonType == ButtonType.OnOff || !isActivated)
            {
                //Debug.Log("Hover");
                image.color = Color.yellow;
                onHover.Invoke();
            }     
        }      
    }

    public void Leave()
    {    
        canHover = true;
        //Debug.Log("Leave");
        if (isActivated)
            image.color = onColor;
        else
            image.color = offColor;
        onLeave.Invoke();
    }

    public void Activate()
    {
        Debug.Log("Activate");
        if (buttonType == ButtonType.Radio)
        {
            foreach (MenuButton button in otherRadioButtons)
            {
                button.Deactivate();
            }
        }

        image.color = onColor;
        canHover = false;
        isActivated = true;

        if (buttonType == ButtonType.OnSetTime)
        {
            Invoke("Deactivate", onTime);
            CancelInvoke("CanHover");
            Invoke("CanHover", onTime + canHoverTime);
        }
        else if (buttonType == ButtonType.OnOff)
        {
            CancelInvoke("CanHover");
            Invoke("CanHover", canHoverTime);
        }
        onActivated.Invoke();
    }

    public void StartActivated()
    {
        image.color = onColor;
        canHover = false;
        isActivated = true;

        if (buttonType == ButtonType.Radio)
        {
            foreach (MenuButton button in otherRadioButtons)
            {
                button.Deactivate();
            }
        }
    }

    public void Deactivate()
    {
        Debug.Log("Deactivate");
        image.color = offColor;
        if (buttonType == ButtonType.OnOff)
        {
            canHover = false;
            CancelInvoke("CanHover");
            Invoke("CanHover", canHoverTime);
        }
        isActivated = false;
        onDeactivated.Invoke();
    }

    void CanHover()
    {
        if (!isActivated || buttonType == ButtonType.OnOff)
            canHover = true;
    }
}
