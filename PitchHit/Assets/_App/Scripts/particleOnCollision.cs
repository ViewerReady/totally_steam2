﻿using UnityEngine;
using System.Collections;

public class particleOnCollision : MonoBehaviour {

	public ParticleSystem particleEffect;

	public float threshold = 9;

	// Use this for initialization
	//void Start () {
		
	//}
	
	// Update is called once per frame
	//void Update () {
	
	//}

	void OnCollisionEnter(Collision collisionInfo){
		if (collisionInfo.relativeVelocity.sqrMagnitude > threshold) {
			particleEffect.Play ();
            if (GetComponent<pointsOnShatter>() != null)
            {
                GetComponent<pointsOnShatter>().PostSplit();
            }
		}
	}

}
