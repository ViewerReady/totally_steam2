﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffectsSceneController : MonoBehaviour {
    public HandCannonController handCannon;
	// Use this for initialization
	void Start () {
		if(handCannon)
        {
            handCannon.ForceLinePrediction();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
