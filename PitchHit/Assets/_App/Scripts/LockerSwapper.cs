﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerSwapper : MonoBehaviour {
    private Animator theAnimator;
    public GameObject[] lockerGroups;
    public SlideBackForward[] buttonSliders;
    public RealButton buttonUp;
    public RealButton buttonDown;
    public AudioSource swappingSound;

    public static int currentLockerGroup;

	// Use this for initialization
	void Start () {
        theAnimator = GetComponent<Animator>();

        if (currentLockerGroup >= lockerGroups.Length)
        {
            currentLockerGroup = 0;
        }
        else if (currentLockerGroup < 0)
        {
            currentLockerGroup = lockerGroups.Length - 1;
        }

        SwapLockers();


        foreach (SlideBackForward s in buttonSliders)
        {
            s.SnapBack();
            if(s.gameObject.activeInHierarchy)
            {
                s.transform.localPosition = s.initialLocalPosition;
            }
        }



        //if(PlayerPrefs.GetInt("LEVELSUNLOCKED")<6)
        //{
        //    buttonUp.isLocked   = true;
        //    buttonDown.isLocked = true;
        //}
	}
    
    void Update()
    {
        if(Input.GetKeyDown("i"))
        {
            SwapUp();
        }
        else if(Input.GetKeyDown("o"))
        {
            SwapDown();
        }
    }
	
	public void SwapUp()
    {
        Debug.Log("swap up......................................");
        currentLockerGroup++;
        if (currentLockerGroup >= lockerGroups.Length)
        {
            currentLockerGroup = 0;
        }
        PerformSwap();
    }

    public void SwapDown()
    {
        Debug.Log("swap down......................................");
        currentLockerGroup--;
        if (currentLockerGroup < 0)
        {
            currentLockerGroup = lockerGroups.Length-1;
        }
        PerformSwap();
    }

    public void PerformSwap()
    {
        theAnimator.SetTrigger("GoUpAndDown");
        if(swappingSound && swappingSound.clip)
        {
            swappingSound.Play();
        }
    }

    public void SwapLockers()
    {
        for (int g = 0; g < lockerGroups.Length; g++)
        {
            lockerGroups[g].SetActive(g == currentLockerGroup);
        }
    }

    public void RetractButtons()
    {
        foreach(SlideBackForward s in buttonSliders)
        {
            s.BeginSlideBack();
        }
    }

    public void ExtendButtons()
    {
        foreach (SlideBackForward s in buttonSliders)
        {
            s.BeginSlideForward();
        }
    }
}
