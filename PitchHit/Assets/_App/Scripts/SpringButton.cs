﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class SpringButton : MonoBehaviour {

    ConfigurableJoint configJoint;

    bool buttonActive;
    [SerializeField] ButtonState currentState;
    [SerializeField] ButtonState defaultPosition;
    [SerializeField] float buttonPushThreshold;
    [SerializeField] bool shouldTriggerOnStart = false;
    [SerializeField] MeshRenderer buttonMesh;
    [SerializeField] Material upMaterial;
    [SerializeField] Material downMaterial;

    private bool beingHeld;
    private Hand controllingHand;
    private Vector3 originalPosition;
    private Vector3 originalRotation;
    [SerializeField] float rotationSpeed = 200f;    //If you raise your arm 1 Unity unit, this is the amount of degrees we rotate.

    [SerializeField] UnityEvent onButtonUp;
    [SerializeField] UnityEvent onButtonDown;

    private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & (~Hand.AttachmentFlags.SnapOnAttach) & (~Hand.AttachmentFlags.DetachOthers);

    void Awake()
    {
        buttonActive = true;
        currentState = defaultPosition;
        configJoint = gameObject.GetComponent<ConfigurableJoint>();
        SoftJointLimit softJointLimit = configJoint.linearLimit;
        softJointLimit.limit = buttonPushThreshold;
        configJoint.linearLimit = softJointLimit;
        originalPosition = transform.localPosition;
    }

    void Start()
    {
        if(defaultPosition == ButtonState.DOWN)
        {
            currentState = ButtonState.DOWN;
            configJoint.anchor = new Vector3(0f, 0f, buttonPushThreshold);
            SoftJointLimit softJointLimit = configJoint.linearLimit;
            softJointLimit.limit = 0f;
            configJoint.linearLimit = softJointLimit;
        }
        else if (defaultPosition == ButtonState.UP)
        {
            currentState = ButtonState.UP;
            configJoint.anchor = new Vector3(0f, 0f, -buttonPushThreshold);
            SoftJointLimit softJointLimit = configJoint.linearLimit;
            softJointLimit.limit = buttonPushThreshold * 2f;
            configJoint.linearLimit = softJointLimit;
        }
        if (buttonMesh.material != null)
        {
            buttonMesh.material = (currentState == ButtonState.UP ? upMaterial : downMaterial);
        }
        if (shouldTriggerOnStart && currentState == ButtonState.UP)
        {
            onButtonUp.Invoke();
        }
        else if (shouldTriggerOnStart && currentState == ButtonState.DOWN)
        {
            onButtonDown.Invoke();
        }
    }

    void Update()
    {
        if (currentState == ButtonState.UP && buttonActive)
        {
            float distancePushed = Vector3.Distance(originalPosition, transform.localPosition);
            if(distancePushed >= buttonPushThreshold && originalPosition.z > transform.localPosition.z)
            {
                currentState = ButtonState.DOWN;
                configJoint.anchor = new Vector3(0f, 0f, buttonPushThreshold);
                SoftJointLimit softJointLimit = configJoint.linearLimit;
                softJointLimit.limit = 0f;
                configJoint.linearLimit = softJointLimit;
                if (buttonMesh.material != null)
                {
                    buttonMesh.material = (currentState == ButtonState.UP ? upMaterial : downMaterial);
                }
                onButtonDown.Invoke();
            }
        }
    }

    public void SetButtonUp()
    {
        Debug.Log("SETTING " + gameObject.name + " TO UP");
        currentState = ButtonState.UP;
        configJoint.anchor = new Vector3(0f, 0f, -buttonPushThreshold);
        SoftJointLimit softJointLimit = configJoint.linearLimit;
        softJointLimit.limit = buttonPushThreshold * 2f;
        configJoint.linearLimit = softJointLimit;
        transform.localPosition = new Vector3(0f, 0f, buttonPushThreshold);
        if (buttonMesh.material != null)
        {
            buttonMesh.material = (currentState == ButtonState.UP ? upMaterial : downMaterial);
        }
        onButtonUp.Invoke();
    }

    public void SetButtonDown()
    {
        Debug.Log("SETTING " + gameObject.name + " TO DOWN");
        currentState = ButtonState.DOWN;
        configJoint.anchor = new Vector3(0f, 0f, buttonPushThreshold);
        SoftJointLimit softJointLimit = configJoint.linearLimit;
        softJointLimit.limit = 0f;
        configJoint.linearLimit = softJointLimit;
        transform.localPosition = new Vector3(0f, 0f, -buttonPushThreshold);
        if (buttonMesh.material != null)
        {
            buttonMesh.material = (currentState == ButtonState.UP ? upMaterial : downMaterial);
        }
        onButtonDown.Invoke();
    }

    public void MakeButtonActive()
    {
        SetButtonUp();
        buttonActive = true;
    }

    public void MakeButtonInactive()
    {
        SetButtonDown();
        buttonActive = false;
    }
}

enum ButtonState
{
    UP,
    DOWN
}