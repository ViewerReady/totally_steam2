﻿using UnityEngine;
using System.Collections;

public class EnemyProjectile : MonoBehaviour {

    private static SpaceLevelController spaceLevel;
    public Transform target;
    public bool isHomingProjectile = true;
    public Color slowestColor = Color.green;
    public Color fastestColor = Color.red;
    private float homingAgility = 10f;

    public AudioClip impactSound;
    public GameObject impactParticle;

    public Rigidbody rigid;
    public AudioSource sound;
    public GameObject travelingEffects;
    public MeshRenderer skin;
    private bool isBlownUp;
	// Use this for initialization
	void Awake () {

        if(spaceLevel==null)
        {
            spaceLevel = FindObjectOfType<SpaceLevelController>();
        }
        
        if(isHomingProjectile)
        {
            StartCoroutine(HomingRoutine());
        }
	}

    public void SetTarget(Transform targ, float angerPercentage)
    {
        //Debug.Log("projectile set target with anger "+angerPercentage);
        target = targ;
        //change color here.
        skin.material.color = Color.Lerp(slowestColor, fastestColor, angerPercentage);
    }

    IEnumerator HomingRoutine()
    {
        yield return null;

        while (isHomingProjectile && target)
        {
            float angleToSteer = Vector3.Angle(rigid.velocity, target.position - transform.position);
            if(angleToSteer>1 && angleToSteer< homingAgility)
            {
                //Debug.Log(angleToSteer);
                angleToSteer = Mathf.Min(Time.deltaTime*angleToSteer*(homingAgility),angleToSteer);
                Vector3 axisToSteerAround = Vector3.Cross(rigid.velocity, target.position - transform.position);
                rigid.velocity = Quaternion.AngleAxis(angleToSteer, axisToSteerAround) * rigid.velocity;
            }

            yield return null;
        }
    }

    private void OnHitPlayer(Collider playerCollider)
    {
        //jostle the helmet if you can
        Rigidbody helmetRigid = playerCollider.GetComponent<Rigidbody>();
        if (helmetRigid)
        {
            helmetRigid.AddForceAtPosition(rigid.velocity, transform.position);
        }

        BlowUp();

        spaceLevel.TakeDamage();
    }

    private void BlowUp()
    {
        if(isBlownUp)
        {
            return;
        }
        isBlownUp = true;

        isHomingProjectile = false;

        //disappear
        Destroy(travelingEffects);
        skin.enabled = false;

        //stopmoving
        rigid.velocity = Vector3.zero;

        //make particles
        if (impactParticle)
        {
            Instantiate(impactParticle, transform.position, transform.rotation);
        }

        //make sound
        if (impactSound)
        {
            sound.clip = impactSound;
            sound.Play();
        }

        //destroy self
        Destroy(gameObject, 3f);
    }

    void OnTriggerEnter(Collider c)
    {
        switch(c.gameObject.tag)
        {
            case "helmet":
                OnHitPlayer(c);
                break;
            case "bat":
                isHomingProjectile = false;
                break;
        }
        
    }

    void OnCollisionEnter(Collision c)
    {
        switch (c.gameObject.tag)
        {
            //case "ground":
            //    BlowUp();
            //    break;
            case "Hoop":
                BlowUp();
                break;
        }
    }
}
