﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScoreboard : MonoBehaviour {

    public TextMeshProUGUI strikesText;
    public TextMeshProUGUI ballsText;
    public TextMeshProUGUI outsText;
    public TextMeshProUGUI[] runs_0;
    public TextMeshProUGUI[] runs_1;
    public TextMeshProUGUI totalRuns0;
    public TextMeshProUGUI totalRuns1;
    public Image frameHighlight;

    [SerializeField] Animator scoreboardAnimator;
    [SerializeField] GameObject scoreboardAnimationTransform;
    bool animationPlaying = false;

    void Awake()
    {
        FullGameDirector.OnStrikesChange += SetStrikes;
        FullGameDirector.OnFoulsChange += SetFouls;
        FullGameDirector.OnOutsChange += SetOuts;
        FullGameDirector.OnBallsChange += SetBalls;
        FullGameDirector.OnScoreChange += SetRuns;

        for(int i = 0; i<runs_0.Length; i++)
        {
            runs_0[i].text = "";
            runs_1[i].text = "";
        }
    }
	
	public void SetRuns(int team, int inning, int runs)
    {
        TextMeshProUGUI runText = GetRunText(team, inning);
        if(runText != null)
        {
            runText.text = runs.ToString();
            //HighlightFrame(team, inning);
        }
        if (team % 2 == 0)
        {
            if(FullGameDirector.awayTeamRuns < 10) totalRuns0.text = "0" + FullGameDirector.awayTeamRuns.ToString();
            else totalRuns0.text = FullGameDirector.awayTeamRuns.ToString();
        }
        if (team % 2 == 1)
        {
            if (FullGameDirector.homeTeamRuns < 10) totalRuns1.text = "0" + FullGameDirector.homeTeamRuns.ToString();
            else totalRuns1.text = FullGameDirector.homeTeamRuns.ToString();
        }
    }

    private TextMeshProUGUI GetRunText(int team, int inning)
    {
        if(team == 0)
        {
            if (inning < runs_0.Length) return runs_0[inning];
        }
        if(team == 1)
        {
            if (inning < runs_1.Length) return runs_1[inning];
        }
        return null;
    }

    private void HighlightFrame(int team, int inning)
    {
        if (team == 0 && inning == 0) return;
        TextMeshProUGUI runText = GetRunText(team, inning);
        if (runText != null)
        {
            frameHighlight.transform.position = runText.transform.position;
//            Debug.Log(runText.transform.position);
        }
    }

    public void SetTime(int time) {
        //timeText.text = time.ToString();
    }

    public void SetStrikes(int strikes) {
        strikesText.text = strikes.ToString();
    }

    public void SetFouls(int fouls)
    {
        //foulsText.text = fouls.ToString();
    }

    public void SetBalls(int balls) {
        ballsText.text = balls.ToString();
    }

    public void SetOuts(int outs) {
        outsText.text = outs.ToString();
    }

    void OnDestroy()
    {
        FullGameDirector.OnStrikesChange -= SetStrikes;
        FullGameDirector.OnFoulsChange   -= SetFouls;
        FullGameDirector.OnOutsChange    -= SetOuts;
        FullGameDirector.OnBallsChange   -= SetBalls;
        FullGameDirector.OnScoreChange   -= SetRuns;
    }

    public void PlayHomerunAnimation()
    {
        QueueScoreboardAnimation("HomerunUIAnimation");
    }

    public void PlayEndGameAnimation()
    {
        QueueScoreboardAnimation("EndGameUIAnimation");
    }

    public void PlayFoxScoreAnimation()
    {
        QueueScoreboardAnimation("FoxScoreUIAnimation");
    }

    public void PlaySlothScoreAnimation()
    {
        QueueScoreboardAnimation("SlothScoreUIAnimation");
    }

    public void PlayRobinScoreAnimation()
    {
        QueueScoreboardAnimation("RobinScoreUIAnimation");
    }

    public void PlayPhantScoreAnimation()
    {
        QueueScoreboardAnimation("PhantScoreUIAnimation");
    }

    public void QueueScoreboardAnimation(string animationName)
    {
        StartCoroutine(WaitForScoreboard(animationName));
    }

    IEnumerator WaitForScoreboard(string animationName)
    {
        //Debug.Log("Threw " + animationName + " into the queue.");
        while (animationPlaying)
        {
            //Debug.Log(animationName + " is waiting for the current scoreboard animation to end.");
            yield return null;
        }
        StartCoroutine(ScoreboardAnimationRoutine(animationName));
    }

    IEnumerator ScoreboardAnimationRoutine(string animationStateName)
    {
        //Debug.Log("Starting scoreboard animation!");
        animationPlaying = true;
        //If there's already a scoreboard animation playing, just fuhgeddaboutit.
        if (scoreboardAnimationTransform.activeInHierarchy) yield break;

        scoreboardAnimationTransform.SetActive(true);
        scoreboardAnimator.Play(animationStateName);
        yield return new WaitForSeconds(1f);    //Apparently Unity takes a bit to update the animation state.
        while (!scoreboardAnimator.GetCurrentAnimatorStateInfo(0).IsName("AnimationOver"))
        {
            //Debug.Log("Waiting for scoreboard animation to end");
            yield return null;
        }
        scoreboardAnimationTransform.SetActive(false);
        animationPlaying = false;
        yield return null;
    }
}