﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PitcherMenu : MonoBehaviour {
    public Transform muzzle;
    public HandCannonAnimationHandler handCannon;

    public LineRenderer linePredictor;
    public float curveBallSpeed = 40f;
    public float slowBallSpeed = 40f;
    public float fastBallSpeed = 60f;
    public float curveStrength = 10f;
    public float slowStrength = 15f;
    private bool isBusy;

    [SerializeField]
    ScrollRect scrollRect;
    [SerializeField]
    float yFromHand;
    [SerializeField]
    float zFromHand;
    [SerializeField]
    float scrollAmount = 1000;
    [SerializeField]
    float scrollReleaseAmount = 1000;
    [SerializeField]
    float velocityStartTrackButtons = 10;
    Vector3 distanceFromHand;
    Vector2 currentVelocity;
    Vector2 previousVelocity = new Vector2(0, 0);
    CircularBuffer<float> touchpadYPoints;
    float closestButtonPosition;
    int closestButton = 1;
    [SerializeField]
    int homeButton = 1;
    float[] buttonPositions;
    //float[] buttonPositions = new float[] { -105, -75, -45, -15 };
    Button[] buttons;
    [SerializeField]
    PitcherMenuFrame buttonFrame;
    [SerializeField]
    GameObject backingCube;
    [SerializeField]
    int numEntriesToShow;
    [SerializeField]
    Canvas canvas;
    [SerializeField]
    float buttonWidth;
    [SerializeField]
    float buttonHeight;
    bool isActivated = false;

    private CustomHand currentHand;
	// Use this for initialization
	void Start () {
        Invoke("InitList", .01f);
        TurnOff();

        distanceFromHand = new Vector3(0.0f, yFromHand, zFromHand);
        touchpadYPoints = new CircularBuffer<float>(5, new float[] {0, 0, 0, 0, 0});
        
    }
	
	// Update is called once per frame
	void Update () {
        if (!isActivated || currentHand==null)
        {
            currentHand = HandManager.GetAvailableDominantHand();
        }

        if(!isActivated)
        {
            if(Input.GetKeyDown("h"))
            {
                Debug.Log("try and turn on.");
                TurnOn();
            }
        }
        else
        {
            if (Input.GetKeyDown("h"))
            {
                Debug.Log("try and turn off.");
                TurnOff();
            }
        }

        if (currentHand)
        {
            if (!isActivated)
            {
                if (currentHand.GetTouchpadDown())
                {
                    TurnOn();
                }
            }
            else if (currentHand.GetTouchpadDown())
            {
                TurnOff();
            }
            else
            {
                transform.position = currentHand.transform.TransformPoint(distanceFromHand);
                transform.rotation = currentHand.transform.rotation;
#if STEAM_VR
                //touchpadYPoints.PushFront(currentHand.actualHand.controller.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad).y);
#elif RIFT
                //touchpadYPoints.PushFront(currentHand.GetTouchPosition().y);
#endif
                //get closest button
                float minDistance = Mathf.Infinity;
                float listY = scrollRect.content.anchoredPosition.y;
                closestButtonPosition = 0;
                for (int i = 0; i < buttonPositions.Length; i++)
                {
                    float buttonPosition = buttonPositions[i];
                    float distance = Mathf.Abs(buttonPosition - listY);
                    if (distance < minDistance)
                    {
                        closestButtonPosition = buttonPosition;
                        closestButton = i;
                        minDistance = distance;
                    }
                }

                linePredictor.enabled = true;
                switch (buttons[closestButton].name)
                {
                    case "SlowBall":
                        ProjectCurveBall(slowBallSpeed, (slowStrength * Vector3.Cross(muzzle.right, -Vector3.up)) + Physics.gravity);
                        break;
                    case "CurveBall":
                        ProjectCurveBall(curveBallSpeed, (curveStrength * Vector3.Cross(muzzle.forward.normalized, Vector3.up).normalized) + Physics.gravity);
                        break;
                    case "FastBall":
                        ProjectCurveBall(fastBallSpeed, Physics.gravity);
                        break;
                    default:
                        linePredictor.enabled = false;
                        break;
                }


                //Debug.Log(touchpadYPoints.ToString());

#if RIFT
                if (Mathf.Abs(currentHand.GetTouchPosition().y) > .1f)
                {
                    currentVelocity = currentHand.GetTouchPosition()*400f;
                    scrollRect.velocity = currentVelocity;

                }
                //track towards the button
                else if (Mathf.Abs(scrollRect.velocity.y) < velocityStartTrackButtons)
                {
                    scrollRect.velocity += new Vector2(0, (closestButtonPosition - listY));
                }

                //check click
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.All))
                {
                    buttons[closestButton].onClick.Invoke();
                    //Debug.Log("Closest button = " + closestButton);
                }
#elif STEAM_VR
                if (touchpadYPoints[1] != 0 && touchpadYPoints[2] != 0 && touchpadYPoints[3] != 0)
                {
                    //scrolling
                    if (touchpadYPoints[0] != 0)
                    {
                        float difference = touchpadYPoints[0] - touchpadYPoints[1];
                        currentVelocity = GetVelocity(difference, scrollAmount);

                        scrollRect.velocity = currentVelocity;

                    }

                    //scroll release
                    else
                    {
                        float velocity = 0;
                        int count = 0;
                        for (int i = 1; i < touchpadYPoints.Capacity - 1; i++)
                        {
                            if (touchpadYPoints[i] != 0 && touchpadYPoints[i + 1] != 0)
                            {
                                //Debug.Log(touchpadYPoints[i] - touchpadYPoints[i + 1]);
                                velocity += touchpadYPoints[i] - touchpadYPoints[i + 1];
                                count++;
                            }
                        }
                        currentVelocity = GetVelocity(velocity, scrollReleaseAmount);
                        scrollRect.velocity = currentVelocity;
                    }

                }
                //track towards the button
                else if (touchpadYPoints[0] == 0 && Mathf.Abs(scrollRect.velocity.y) < velocityStartTrackButtons)
                {
                    scrollRect.velocity += new Vector2(0, (closestButtonPosition - listY));
                }
                //check click
                if (currentHand.GetSqueezeDown())
                {
                    buttons[closestButton].onClick.Invoke();
                    //Debug.Log("Closest button = " + closestButton);
                }
#endif
            }
        }
        else
        {
            if(isActivated)
            {
                //TurnOff();
            }
        }
    }

    void LateUpdate()
    {
        //keep in bounds because of ScrollRect bug
        if (scrollRect.normalizedPosition.y < 0)
            scrollRect.normalizedPosition = new Vector2(scrollRect.normalizedPosition.x, .001f);
        else if (scrollRect.normalizedPosition.y > 1)
            scrollRect.normalizedPosition = new Vector2(scrollRect.normalizedPosition.x, .999f);
        
        //direct track towards button when close
        if (Mathf.Abs(scrollRect.content.anchoredPosition.y - closestButtonPosition) < .2f && scrollRect.velocity.y < 1f && previousVelocity.y < 1f)
        {
            int sign;
            if (scrollRect.content.anchoredPosition.y > 0)
                sign = 1;
            else
                sign = -1;

            scrollRect.content.anchoredPosition += new Vector2(0, .1f * sign);
            if ((sign == 1 && scrollRect.content.anchoredPosition.y > closestButtonPosition) || scrollRect.content.anchoredPosition.y < closestButtonPosition)
            {
                scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, closestButtonPosition);
            }
        }

        previousVelocity = scrollRect.velocity;
    }

    Vector2 GetVelocity(float difference, float multiplier)
    {
        int sign = difference > 0 ? 1 : -1;
        return new Vector2(0, (Mathf.Abs(difference) < .2f ? difference : sign * .2f) * multiplier);
    }

    public void TurnOn()
    {
		if (handCannon) {
            Debug.Log("turn on hand cannon.");
            handCannon.StartAppearing();
            isBusy = true;
		}
        else
        {
            OnFinishAppearing();
        }

        GameController.canTurnAround = false;
        isActivated = true;
        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, buttonPositions[homeButton]);
    }

    public void OnFinishAppearing()
    {
        isBusy = false;
    }

    public void TurnOff()
    {
        if (handCannon)
        {
            Debug.Log("turn off hand cannon.");
            handCannon.StartDisappearing();
            isBusy = true;
        }
        else
        {
            OnFinishDisappearing();
        }

        isActivated = false;

    }

    public void OnFinishDisappearing()
    {
        Debug.Log("finished disappearing.");
        isBusy = false;

        GameController.canTurnAround = true;
        transform.position = new Vector3(0, -1000, 0);
        linePredictor.enabled = false;
        //handCannon.gameObject.SetActive(false);
    }

    void InitList()
    {
        GameObject dataList = scrollRect.content.gameObject;
        dataList.GetComponent<RectTransform>().sizeDelta = new Vector2(buttonWidth, dataList.GetComponent<RectTransform>().sizeDelta.y);
        buttons = dataList.GetComponentsInChildren<Button>();
        buttonPositions = new float[buttons.Length];
        float halfButtonHeight = buttonHeight / 2;
        buttonFrame.Init(buttonWidth, buttonHeight);
        buttonFrame.GetComponent<RectTransform>().anchoredPosition = new Vector2(buttonWidth / 2, -(halfButtonHeight + homeButton * buttonHeight));

        float topPadding = 0;
        float bottomPadding = 0;
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i < homeButton)
            {
                topPadding += buttonHeight;
            }
            else if (i > homeButton)
            {
                bottomPadding += buttonHeight;
            }
        }
        bottomPadding -= Mathf.Max(0, (buttons.Length - numEntriesToShow) * buttonHeight);


        float newButtonPosition = -(buttons.Length * buttonHeight + topPadding + bottomPadding) / 2;
        for (int i = 0; i < buttons.Length; i++)
        {
            Button button = buttons[i];
            button.GetComponent<RectTransform>().sizeDelta = new Vector2(buttons[i].GetComponent<RectTransform>().sizeDelta.x, buttonHeight);

            buttonPositions[i] = newButtonPosition;
            newButtonPosition += buttonHeight;
        }


        

        VerticalLayoutGroup layoutGroup = dataList.GetComponent<VerticalLayoutGroup>();
        layoutGroup.padding.top = (int)topPadding;
        layoutGroup.padding.bottom = (int)bottomPadding;

        scrollRect.content.anchoredPosition = new Vector2(buttonWidth / 2, buttonPositions[homeButton]);
        backingCube.transform.localScale = new Vector3(buttonWidth, buttonHeight * numEntriesToShow, backingCube.transform.localScale.z);
        canvas.GetComponent<RectTransform>().sizeDelta = new Vector2(buttonWidth, buttonHeight * numEntriesToShow);
        scrollRect.GetComponent<RectTransform>().sizeDelta = new Vector2(buttonWidth, buttonHeight * numEntriesToShow);

        closestButtonPosition = buttonPositions[homeButton];
    }

    public void PitchFastBall()
    {
        hittable ball = (Instantiate(ballController.instance.ballPrefab, muzzle.transform.position + transform.forward, transform.rotation, null) as GameObject).GetComponent<hittable>();

        ball.SetOrigin(ballController.ballServiceType.pitcher, transform);
        ball.BecomeActive();
        StrikeZoneController.OnBallPitched(ball);

        ball.SetVelocity( transform.forward * fastBallSpeed);
        TurnOff();
    }

    public void PitchSlowBall()
    {
        hittable ball = (Instantiate(ballController.instance.ballPrefab, muzzle.transform.position + transform.forward, transform.rotation, null) as GameObject).GetComponent<hittable>();

        ball.SetOrigin(ballController.ballServiceType.pitcher, transform);
        ball.BecomeActive();

        ball.SetVelocity(transform.forward * slowBallSpeed);
        StrikeZoneController.OnBallPitched(ball);

        StartCoroutine(CurveballRoutine(ball, (slowStrength * Vector3.Cross(muzzle.right, -Vector3.up))));
        TurnOff();
    }

    public void ProjectCurveBall(float initialSpeed, Vector3 accel)
    {
        float timeBetweenPoints = .05f;

        Vector3[] predictionPoints = new Vector3[30];
        linePredictor.positionCount = 30;
        predictionPoints[0] = muzzle.position;
        float timePredicted = 0f;
        //Vector3 curveAcceleration = (curveStrength * Vector3.Cross(muzzle.forward.normalized, Vector3.up)) + Physics.gravity;
        for (int x=1;x<predictionPoints.Length;x++)
        {
            timePredicted += timeBetweenPoints;

            //Vector3 previousDirection = currentDirection;
            //currentDirection = /*Quaternion.AngleAxis(curveAngle * timeBetweenPoints, Vector3.up) * */(currentDirection + (.5f* Physics.gravity * timeBetweenPoints * timeBetweenPoints));
            
            predictionPoints[x] = predictionPoints[0] + (muzzle.forward*initialSpeed*timePredicted) +(.5f * accel * timePredicted * timePredicted);
            

        }

        linePredictor.SetPositions(predictionPoints);
    }
    
    public void PitchCurveBall()
    {
        hittable ball = (Instantiate(ballController.instance.ballPrefab, muzzle.position + transform.forward, transform.rotation, null) as GameObject).GetComponent<hittable>();

        ball.SetOrigin(ballController.ballServiceType.pitcher, transform);
        ball.BecomeActive();

        ball.SetVelocity(transform.forward * curveBallSpeed);
        StartCoroutine(CurveballRoutine(ball,curveStrength * Vector3.Cross(muzzle.forward,Vector3.up).normalized));
        StrikeZoneController.OnBallPitched(ball);
        TurnOff();
    }

    
    IEnumerator CurveballRoutine(hittable ball,Vector3 curveAccel)
    {
        Rigidbody rb = ball.GetRigidbody();
        while(rb && !ball.hitGround && !ball.alreadyBeenHit)
        {
            //rb.velocity = Quaternion.AngleAxis(curveAccel * Time.deltaTime, Vector3.up) * rb.velocity;
            rb.velocity += (curveAccel*Time.deltaTime);

            yield return null;
        }
    }

    public void PitchManual()
    {
        hittable ball = (Instantiate(ballController.instance.ballPrefab, currentHand.transform.position, transform.rotation, null) as GameObject).GetComponent<hittable>();

        ball.SetOrigin(ballController.ballServiceType.pitcher,transform);
        ball.BecomeActive();

        //ball.GetRigidbody().velocity = transform.forward;
        TurnOff();
    }

}
