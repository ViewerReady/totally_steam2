﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Valve.VR.InteractionSystem;

public class ScoreDisplay : MonoBehaviour {
    private static Transform playerHead;
    public CanvasGroup displayGroup;
    public Text displayText;
    public Transform messageObject;
    public Number3DController hundreds3D;
    public Number3DController tens3D;
    public Number3DController singles3D;
    public float sweepDistance = 2f;

    private bool displaying = false;
    private float duration = 3f;
    private float countdown;
    private float floatUpSpeed = .5f;
    // Use this for initialization
    //void Start () {
	
	//}
	
	// Update is called once per frame
	void Update () {
	    if(displaying)
        {
            transform.position += Vector3.up * floatUpSpeed * Time.deltaTime * transform.lossyScale.y;
            //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(playerHead.position - transform.position, Vector3.up), Time.deltaTime);
            transform.LookAt(playerHead, Vector3.up);

            if(messageObject)
            {
                messageObject.localRotation = Quaternion.Lerp(messageObject.localRotation, Quaternion.identity, Time.deltaTime);
            }

            if (hundreds3D)
            {
                hundreds3D.transform.localRotation = Quaternion.Lerp(hundreds3D.transform.localRotation, Quaternion.identity, Time.deltaTime);
            }
            if (tens3D)
            {
                tens3D.transform.localRotation = Quaternion.Lerp(tens3D.transform.localRotation, Quaternion.identity, Time.deltaTime);
            }
            if (singles3D)
            {
                singles3D.transform.localRotation = Quaternion.Lerp(singles3D.transform.localRotation, Quaternion.identity, Time.deltaTime);
            }

            if (countdown>0)
            {
                countdown -= Time.deltaTime;
            }
            else
            {
                if(displayGroup.alpha>0)
                {
                    displayGroup.alpha -= Time.deltaTime;
                }
                else
                {
                   Destroy(gameObject,.1f);
                }
            }
        }
	}

    public void JustAppear()
    {
        if (playerHead == null)
        {
#if RIFT
            playerHead = Camera.main.transform;
#elif STEAM_VR
            playerHead = Player.instance.hmdTransform;
#endif
        }
        countdown = duration;
        displaying = true;


        transform.LookAt(playerHead, Vector3.up);
        RectTransform rect = GetComponent<RectTransform>();
        Vector3 halfExtents = new Vector3(rect.sizeDelta.x * rect.localScale.x, rect.sizeDelta.y * rect.localScale.y, .1f);
        Vector3 direction = -transform.forward * sweepDistance;
        RaycastHit hit;
        if (Physics.BoxCast(transform.position - direction, halfExtents, direction, out hit, transform.rotation, direction.magnitude, 1 << LayerMask.NameToLayer("Floaties")))
        {
            transform.position += transform.forward * (sweepDistance - hit.distance);
        }
    }

    public void BeginDisplay(int score)
    {
        //Debug.Log("begin display of "+message);
        if(playerHead == null)
        {
#if RIFT
            playerHead = Camera.main.transform;
#elif STEAM_VR
            playerHead = Player.instance.hmdTransform;
#endif

            /*
            if (GameController.isTrueOculus)
            {
                playerHead = Camera.main.transform;
            }
            else
            {
                SteamVR_TrackedObject[] trackedObjects = FindObjectsOfType<SteamVR_TrackedObject>(); 
                foreach (SteamVR_TrackedObject t in trackedObjects)
                {
                    if (t.index == SteamVR_TrackedObject.EIndex.Hmd)
                    {
                        playerHead = t.transform;
                        break;
                    }
                }
            }

            if (playerHead == null)
            {
                return;
            }*/
        }

        countdown = duration;
        displaying = true;

        displayGroup.alpha = 1f;

        if (displayText)
        {
            displayText.text = score.ToString()+"pts";
        }
        else
        {
            int temp = 0;
            if(score<100)
            {
                hundreds3D.gameObject.SetActive(false);
            }
            else
            {
                temp = score / 100;
                hundreds3D.DisplayNumber(temp);
                score -= temp * 100;
            }

            if(score<10)
            {
                tens3D.DisplayNumber(0);
            }
            else
            {
                temp = score / 10;
                tens3D.DisplayNumber(temp);
                score -= temp * 10;
            }

            singles3D.DisplayNumber(score);
        }


        //transform.LookAt(playerHead, Vector3.up);

        BoxCollider col = GetComponent<BoxCollider>();
        Vector3 halfExtents;
        if(col)
        {
            halfExtents = new Vector3(col.size.x * transform.localScale.x, col.size.y * transform.localScale.y, col.size.z * transform.localScale.z);
        }
        else
        {
            RectTransform rect = GetComponent<RectTransform>();

            halfExtents = new Vector3(rect.sizeDelta.x * rect.localScale.x, rect.sizeDelta.y * rect.localScale.y, .1f);

        }
        Vector3 direction = -transform.forward * sweepDistance;
        RaycastHit hit;
        if(Physics.BoxCast(transform.position-direction,halfExtents,direction,out hit,transform.rotation,direction.magnitude,(1<<LayerMask.NameToLayer("Floaties")) | (1<<LayerMask.NameToLayer("UI")),QueryTriggerInteraction.Collide))
        {
            transform.position += transform.forward * (sweepDistance - hit.distance);
        }
    }

    public void EndDisplay()
    {
        countdown = 0f;
    }

}
