﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeRunDerbyController : MonoBehaviour {
    public HomeRunTarget[] targets;
    public GameObject farHomeRunTextPrefab;
    public GameObject standsHomeRunTextPrefab;
    public Transform leftOutfieldPopUpSpot;
    public Transform rightOutfieldPopUpSpot;
    //public Animator frontCenterHomeRunDisplay;
    public Animator spriteAnimator;
    public ScoreboardController scoreController;
    public hitThings theBat;
    public GameObject photoFlashes;
    //public GameObject landingMarkerPrefab;
    public GameObject fireworksPrefab;
    public GameObject sparksPrefab;
    public GameObject dustPrefab;
    public GameObject shatterableHomeRunPrefab;
    public AudioSource photoSound;
    public AudioSource targetAppearSound;

    public Text currentDistanceText;
    public Text bestScoreText;
    public Text bestDistanceText;
    public bool constantFireworks;
    public bool useTimer;
    private HomeRunLevelController homeRunDetector;
    public TimeController time;

    public int levelNumber = 1;
    public AppearingMenu appearingMenu;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredPoints = 30;

    private List<hittable> activeBalls;
	// Use this for initialization
	void Start () {
        homeRunDetector = GetComponent<HomeRunLevelController>();
        activeBalls = new List<hittable>();

        //StartCoroutine(DisplayResultsRoutine(300f * (Vector3.forward + Vector3.right), 2f)); 
        //StartCoroutine(DisplayResultsRoutine(300f * (Vector3.forward - Vector3.right), 4f));
        if(constantFireworks)
        {
            StartCoroutine(ConstantFireworksRoutine());
        }

        GameObject temp = Instantiate(shatterableHomeRunPrefab, Vector3.up * -10f, Quaternion.identity, null) as GameObject;
        temp.SetActive(true);
        Destroy(temp,1f);
    }

    void OnEnable()
    {
        BatController.onBatHit.AddListener(OnBallHit);
        //theBat.onBallHit.AddListener(OnBallHit);
    }

    void OnDisable()
    {
        BatController.onBatHit.RemoveListener(OnBallHit);
        //theBat.onBallHit.RemoveListener(OnBallHit);
    }

    private bool targetsAreUp = false;
    void LateUpdate()
    {
        if (GameController.levelIsBegun && targets.Length > 0)
        {
            //Debug.Log(time.currentTimeSec + "  <? "+ ((time.totalTimeSec) / 2f));
            if (time.currentTimeSec < (time.totalTimeSec) / 2f)
            {
                if(!targetsAreUp)
                {
                    targetsAreUp = true;
                    if (targetAppearSound)
                    {
                        targetAppearSound.Play();
                    }
                    foreach (HomeRunTarget t in targets)
                    {
                        if(t)
                        {
                            t.GoUp();
                        }
                    }
                }

            }
        }
    }

    IEnumerator ConstantFireworksRoutine(int pops = -1)
    {
        Vector3 center = transform.position + (transform.forward * 100f) + (transform.up * 50f);
        photoFlashes.SetActive(true);
        int remainingPops = pops;
        while(pops<0 || remainingPops>0)
        {
            if(remainingPops > 0)
            {
                remainingPops--;
            }
            yield return new WaitForSeconds(Random.value * .6f);

            Vector3 spawnPoint = (Random.insideUnitSphere * 50f) + (Vector3.right*(Random.value-.5f)*100f);

            Instantiate(fireworksPrefab, center + spawnPoint, Quaternion.identity);

        }
    }

    private hittable lastBallStarted;
    public void OnBallHit(hittable ball)
    {
        if(lastBallStarted && lastBallStarted==ball)
        {
            return;
        }
        lastBallStarted = ball;
        //Debug.Log("Home run derby on ball hit called.");
        StartCoroutine(ResultsRoutine(ball.GetComponent<Rigidbody>()));
    }

    IEnumerator ResultsRoutine(Rigidbody ballBody)
    {
        HomeRunBall judger = ballBody.GetComponent<HomeRunBall>();
        if(!judger)
        {
            yield break;
        }
        judger.DetermineParticleTrail(ballBody.velocity.magnitude);
        
        yield return new WaitForSeconds(.2f);
        Vector3 impactPosition = Trajectory.GetLandingSpot(ballBody);
        bool isHomeRun = homeRunDetector.isHomeRun(impactPosition);
        
        StartCoroutine(CurrentDistanceRoutine(judger));

        if (isHomeRun)//make sure!
        {

            if(judger)
            {
                judger.BeginJudgement();
                while(judger.isJudging())
                {
                    yield return null;
                }
                isHomeRun = judger.isTrueHomeRun();
                impactPosition = judger.GetCollisionPosition();
                /*
                if (isHomeRun)
                {
                    Renderer marker = (Instantiate(landingMarkerPrefab, judger.GetCollisionPosition(), Quaternion.LookRotation(judger.GetCollisionNormal())) as GameObject).GetComponent<Renderer>();
                    marker.material.color = Color.blue;
                    marker.transform.localScale = Vector3.one + (Vector3.forward * Vector3.Distance(impactPosition, homeRunDetector.homePlate.position) * .4f);
                    marker.transform.position += marker.transform.forward * marker.transform.localScale.z * .5f;
                }
                */

                if (isHomeRun)
                {
                    if (judger.GetLandingType() == HomeRunBall.LandingType.Far)
                    {
                        
                        PopUpDisplay popUp = null;
                        if (GameController.AngleAroundAxis(homeRunDetector.homePlate.forward, impactPosition - homeRunDetector.homePlate.position, Vector3.up) < 0)
                        {
                            //popUp = (Instantiate(farHomeRunTextPrefab, leftOutfieldPopUpSpot.position, leftOutfieldPopUpSpot.rotation) as GameObject).GetComponent<PopUpDisplay>();
                        }
                        else
                        {
                            //popUp = (Instantiate(farHomeRunTextPrefab, rightOutfieldPopUpSpot.position, rightOutfieldPopUpSpot.rotation) as GameObject).GetComponent<PopUpDisplay>();
                        }

                        if (popUp)
                        {
                            popUp.SetText((new Vector3(impactPosition.x,0,impactPosition.z)).magnitude.ToString("F1") + "m");
                        }
                    }
                    else if(judger.GetLandingType() == HomeRunBall.LandingType.Lights)
                    {
                        HomeRunShatterable(judger);

                    }
                }
            }

        }
        else
        {
            
            judger.SetLandingTime(Trajectory.timeOfPreviousLandingCalculation);
            judger.SetLandingType(HomeRunBall.LandingType.Short);
        }

        float impactDistance = new Vector3(impactPosition.x,0f,impactPosition.z).magnitude;
        bool goingShort = false;
        switch (judger.GetLandingType())
        {
            case HomeRunBall.LandingType.Far:
                yield return new WaitForSeconds(1f);
                //particles
                Instantiate(fireworksPrefab, judger.transform.position,Quaternion.identity);
                //HomeRunPopUp();
                //ShatterablePopUp(judger);
                HomeRunShatterable(judger,10,true,true);
                break;
            case HomeRunBall.LandingType.Lights:
                if((judger.GetLandingTime() - Time.time)<=1f)
                {
                    yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                    Instantiate(sparksPrefab, judger.transform.position, Quaternion.identity);
                    //HomeRunPopUp();
                    HomeRunShatterable(judger);
                }
                else
                {
                    yield return new WaitForSeconds(1f);
                    //HomeRunPopUp();
                    HomeRunShatterable(judger);
                    yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                    //particles
                    Instantiate(sparksPrefab, judger.transform.position, Quaternion.identity);
                }
                
                break;
            case HomeRunBall.LandingType.Sign:
                if ((judger.GetLandingTime() - Time.time) <= 1f)
                {
                    yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                    Instantiate(sparksPrefab, judger.transform.position, Quaternion.identity);
                    //HomeRunPopUp();
                    HomeRunShatterable(judger);
                }
                else
                {
                    yield return new WaitForSeconds(1f);
                    //HomeRunPopUp();
                    HomeRunShatterable(judger);
                    yield return new WaitForSeconds(judger.GetLandingTime() - (Time.time + 3f));
                    //particles
                    Instantiate(sparksPrefab, judger.transform.position, Quaternion.identity);
                }
                break;
            case HomeRunBall.LandingType.Stands:
                yield return new WaitForSeconds((judger.GetLandingTime() - Time.time) * .5f);
                //HomeRunPopUp();
                HomeRunShatterable(judger);
                yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                //particles
                Instantiate(fireworksPrefab, judger.transform.position, Quaternion.identity);
                break;
            case HomeRunBall.LandingType.Target:
                yield return new WaitForSeconds((judger.GetLandingTime() - Time.time) * .5f);
                //HomeRunPopUp();
                HomeRunShatterable(judger,25,false);
                yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                //particles
                Instantiate(fireworksPrefab, judger.transform.position, Quaternion.identity);
                if(judger.GetHomeRunTarget())
                {
                    judger.GetHomeRunTarget().StartBlinking();
                }
                break;
            case HomeRunBall.LandingType.Barely:
                yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                //particles
                Instantiate(sparksPrefab, judger.transform.position, Quaternion.identity);
                break;
            case HomeRunBall.LandingType.WallShort:
                yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                //particles
                Transform temp = (Instantiate(dustPrefab, judger.transform.position, Quaternion.LookRotation(judger.GetCollisionNormal())) as GameObject).transform;
                //temp.Rotate(Vector3.right, -90f, Space.Self);
                break;
            case HomeRunBall.LandingType.Short:
                //yield return new WaitForSeconds(judger.GetLandingTime() - Time.time);
                //particles
                if (impactDistance > 20f)
                {
                    if (judger)
                    {
                        goingShort = true;
                        //Transform temp2 = (Instantiate(dustPrefab, judger.transform.position, Quaternion.LookRotation(Vector3.up, Vector3.forward)) as GameObject).transform;
                        //temp2.Rotate(Vector3.right, -90f, Space.Self);
                    }
                }
                if (!useTimer)
                {
                    FinalizeScore();
                }
                break;

        }

        if(!isHomeRun && !useTimer)
        {
            FinalizeScore();
        }


        keepTracking = false;
        UpdateCurrentDistanceText(impactDistance);
        if(impactDistance > maxDistance)
        {
            maxDistance = impactDistance;
            bestDistanceText.text = Mathf.Floor(maxDistance * 3.28084f) + "ft";
        }
        if (goingShort)
        {
            hittable hittableComponent = judger.GetHittableComponent();
            if (hittableComponent)
            {
                while (hittableComponent.hitGround == false)
                {
                    yield return null;
                }
                Instantiate(dustPrefab, judger.transform.position, Quaternion.LookRotation(Vector3.up, Vector3.forward));
            }
        }
    }

    private bool keepTracking;
    IEnumerator CurrentDistanceRoutine(HomeRunBall judger)
    {
        keepTracking = true;
        while(keepTracking)
        {

            if (judger)
            {
                UpdateCurrentDistanceText(new Vector3(judger.transform.position.x, 0, judger.transform.position.z).magnitude);
            }
            yield return null;
        }
    }

    private void UpdateCurrentDistanceText(float distance)
    {
        currentDistanceText.text = (Mathf.Floor(distance * 3.28084f) + "ft");
    }

    private void UpdateBestScoreAndDistance()
    {
        bestDistanceText.text = currentDistanceText.text;
        bestScoreText.text = scoreController.myScore.ToString();
    }
    /*
    private void HomeRunPopUp()
    {
        if (scoreController)
        {
            scoreController.AddScore(10);
            StartCoroutine(ConstantFireworksRoutine(5));
            //scoreController.TriggerFireworks();
        }

        //StartPhotoFlashes();
        //frontCenterHomeRunDisplay.SetTrigger("PerformPopUp");
        //homeRunLetterAnimator.SetTrigger("WiggleLetters");
    }
    */
    private int spriteAnimIndex = 1;
    private void HomeRunShatterable(HomeRunBall ball, int points = 10, bool useShatterable = true, bool atPeak = false)
    {
        if(ball.displayed)
        {
            return;
        }

        if (scoreController)
        {
            scoreController.AddScore(points);
            //scoreController.TriggerFireworks();
            StartCoroutine(ConstantFireworksRoutine(5));
        }
        ball.displayed = true;

        StartPhotoFlashes();
        if (useShatterable)
        {
            ball.ClobberingTime();
            Vector3 spot = atPeak ? Trajectory.GetSpotInFuture(ball.GetComponent<Rigidbody>(), 1.5f) : ball.GetCollisionPosition();
            GameObject temp = Instantiate(shatterableHomeRunPrefab, spot, Quaternion.LookRotation(TButt.TBCameraRig.instance.GetActiveCameraObject().transform.position - spot)) as GameObject;
            temp.GetComponent<ShatterWords>().judger = ball;
            temp.SetActive(true);
        }
        /*
        int randomIndex = Random.Range(1, 4);
        if(Random.value>.6f)
        {
            randomIndex = 3;
        }
                Debug.Log("random index for sprite animation... "+randomIndex);
        */

        spriteAnimator.SetTrigger("Anim_"+spriteAnimIndex);
        spriteAnimIndex++;
        if(spriteAnimIndex>3)
        {
            spriteAnimIndex = 1;
        }
    }

    IEnumerator HomeRunBullsEye(float delay, Vector3 impact, Animator theBullsEye)
    {
        theBullsEye.StopPlayback();

        yield return new WaitForSeconds(delay);

        //make something happen at the actual impact site
        //make sound start

        yield return new WaitForSeconds(.5f);

        if (scoreController)
        {
            scoreController.AddScore(25);
            //scoreController.TriggerFireworks();
            StartCoroutine(ConstantFireworksRoutine(5));
        }

        //theBullsEye.StartPlayback();//this is where the bullseye goes away.
    }

    private int bestScore = 0;
    private float maxDistance = 0f;
    public void FinalizeScore()
    {
        if(scoreController)
        {
            if(scoreController.myScore>0)
            {
                if(scoreController.myScore > bestScore)
                {
                    bestScore = scoreController.myScore;
                    bestScoreText.text = bestScore.ToString();
                }
                scoreController.ClearScore();
            }
        }
    }

    private Coroutine flashRoutine;
    public void StartPhotoFlashes()
    {
        if(flashRoutine != null)
        {
            StopCoroutine(flashRoutine);
        }

        StartCoroutine(PhotoFlashRoutine());
    }

    IEnumerator PhotoFlashRoutine()
    {
        photoSound.Play();
        photoFlashes.SetActive(true);
        yield return new WaitForSeconds(3f);
        photoFlashes.SetActive(false);
    }

    //private MeshFilter previousCombineFilter;
    //private MeshRenderer previousCombineRenderer;
    public GameObject combinePrefab;
    private bool alreadyCombined;
    public Transform CombineMeshesOfBrokenWords(MeshFilter[] meshFilters)
    {
        GameObject temp = Instantiate(combinePrefab, Vector3.zero, Quaternion.identity, transform);
        MeshFilter currentCombineFilter = temp.GetComponent<MeshFilter>();
        MeshRenderer currentCombineRenderer = temp.GetComponent<MeshRenderer>();

        CombineInstance[] combine = new CombineInstance[meshFilters.Length+ (alreadyCombined?1:0)];
        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].mesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            i++;
        }

        //if (alreadyCombined)
        //{
        //    combine[i].mesh = previousCombineFilter.mesh;
        //    combine[i].transform = previousCombineRenderer.transform.localToWorldMatrix;
        //}

        Mesh tempMesh = new Mesh();
        
        tempMesh.CombineMeshes(combine);
        currentCombineFilter.mesh = tempMesh;
        temp.SetActive(true);
        return temp.transform;
        //alreadyCombined = true;

        //if (previousCombineRenderer)
        //{
            //Destroy(previousCombineRenderer.gameObject);
        //}
        //previousCombineFilter = currentCombineFilter;
        //previousCombineRenderer = currentCombineRenderer;
    }
    
    public void CheckIfChallengePassed()
    {
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if (levelsUnlocked == levelNumber)
        {
            if (ScoreboardController.instance.myScore >= requiredPoints)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (sound && successClip)
                    {
                        sound.clip = successClip;
                        sound.Play();
                    }
                }
            }
        }
    }
}
