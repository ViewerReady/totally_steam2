﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoSphere : MonoBehaviour
{
    #region Public Variables
    public Color defaultColor = Color.red;
    public Color selectedColor = Color.green;
    public float size = 1;
    #endregion

    #region Public Methods
    public void OnDrawGizmos()
    {
#if UNITY_EDITOR
        if (UnityEditor.Selection.activeGameObject == gameObject)
        {
            Gizmos.color = selectedColor;
            Gizmos.DrawSphere(transform.position, size);
        }
        else
        {
            Gizmos.color = defaultColor;
            Gizmos.DrawSphere(transform.position, size);
        }
#endif
    }
    #endregion
}
