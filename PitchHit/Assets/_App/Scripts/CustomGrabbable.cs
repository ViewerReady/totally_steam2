﻿/********************************************************************************//**
\file      Grabbable.cs
\brief     Component that allows objects to be grabbed by the hand.
\copyright Copyright 2015 Oculus VR, LLC All Rights reserved.
************************************************************************************/

using System;
using UnityEngine;
using System.Collections;

// Which object had its grabbed state changed, and whether the grab is starting or ending.
public class CustomGrabbable : OVRGrabbable
{

    // public bool SnapToHand = true;
    public bool tomatoPresence = false;
    public bool rotationFrozen  = false;
    public bool draggable = true;
    private Quaternion m_fixedRotation;
    //private Collider m_grabbedCollider = null;
    private OVRGrabber m_grabber = null;
    public bool forceKinematic;
    private Rigidbody rigid;
    private Collider[] m_grabbedColliders;
    public bool IsGrabbed
    {
        get { return m_grabber != null; }
    }

    public OVRGrabber GrabbedHand
    {
        get { return m_grabber; }
    }

    //public Transform GrabbedTransform
    //{
    //    get { return m_grabbedCollider.transform; }
    //}
    //public Collider GrabbedCollider
    //{
    //    get { return m_grabbedCollider; }
    //}
    [HideInInspector]
    public Rigidbody GrabbedRigidbody;


    public bool RotationFrozen
    {

        //   get { return GrabbedRigidbody.constraints == RigidbodyConstraints.FreezeRotation; }
        get { return rotationFrozen; }
    }
    public Quaternion FixedRotation
    {
        get { return m_fixedRotation; }
    }
    public override void GrabBegin(OVRGrabber grabber, Collider grabPoint)
    {
        m_grabber = grabber;
        SendMessage("CustomGrabbableGrabBegin", m_grabber, SendMessageOptions.DontRequireReceiver);

        if (tomatoPresence) grabber.HideGrabber();

        // m_grabbedCollider = grabPoint;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    
        m_fixedRotation = transform.rotation;

    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        if (tomatoPresence) m_grabber.ShowGrabber();

        SendMessage("CustomGrabbableGrabEnd", m_grabber, SendMessageOptions.DontRequireReceiver);
        m_grabber = null;

        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = m_grabbedKinematic;

        if (m_grabbedKinematic)
        {
            //Debug.Log("Nevermind. it's kinemetic");
            return;//dont add velocity to kinematic objects
        }
        //print(" linearVelocity " + linearVelocity.magnitude);
        //rb.velocity = linearVelocity;
        rb.angularVelocity = angularVelocity;
        Debug.DrawRay(transform.position, linearVelocity, Color.red, 5f);
        rb.velocity = Quaternion.AngleAxis(angularVelocity.magnitude,angularVelocity) * linearVelocity;
        Debug.DrawRay(transform.position, rb.velocity, Color.green, 5f);
        // m_grabbedCollider = null;
        //StartCoroutine(ReportVelocity(rb));
    }

    IEnumerator ReportVelocity(Rigidbody rb)
    {
        int count = 5;
        while(count>0)
        {
            Debug.Log(rb.velocity.magnitude+" is the velocity. #"+count);
            count--;
            yield return null;
        }
    }

    void Update()
    {
        /*
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        if (rb.velocity.magnitude > 1)
        {
            print("rb.velocity " + rb.velocity);
        }
        */
        if (!IsGrabbed) return;
        SendMessage("CustomGrabbing", m_grabber, SendMessageOptions.DontRequireReceiver);

        if(forceKinematic)
        {
            if(!rigid)
            {
                rigid = GetComponent<Rigidbody>();

            }
            if(rigid)
            {
                rigid.isKinematic = true;
            }
        }

    }
    virtual protected void Awake()
    {
        //if (m_grabPoints.Length == 0)
        {
            // Get the collider from the grabbable
            //m_grabbedCollider = this.GetComponent<Collider>();
           GrabbedRigidbody = GetComponent<Rigidbody>();
            //if (m_grabbedCollider == null)
            //{
            //    throw new ArgumentException("Grabbable: Grabbables cannot have zero grab points and no collider -- please add a grab point or collider.");
            //}

            // Create a default grab point
            // m_grabPoints = new Collider[1] { collider };
        }
    }

    private void Start()
    {
        m_grabbedKinematic = GetComponent<Rigidbody>().isKinematic;
    
     
    }

    private void OnDestroy()
    {
        if (m_grabber != null)
        {
            // Notify the hand to release destroyed grabbables
            m_grabber.ForceRelease(this);
        }
    }

    private void SendMsg(string msgName, object msg)
    {
        this.transform.SendMessage(msgName, msg, SendMessageOptions.DontRequireReceiver);
    }
}
