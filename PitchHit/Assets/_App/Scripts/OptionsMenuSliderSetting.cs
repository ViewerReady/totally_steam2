﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuSliderSetting : MonoBehaviour {

    [SerializeField] Slider slider;
    [SerializeField] string settingKey;

    void OnEnable()
    {
        if (PlayerPrefs.HasKey(settingKey))
        {
            slider.value = PlayerPrefs.GetFloat(settingKey);
            slider.onValueChanged.Invoke(slider.value);
        }
        else
        {
            slider.value = 0.07f;
            slider.onValueChanged.Invoke(slider.value);
        }
    }

    public void SaveSliderChoice(float value)
    {
        PlayerPrefs.SetFloat(settingKey, value);
    }
}
