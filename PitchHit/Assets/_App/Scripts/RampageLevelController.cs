﻿using UnityEngine;
using System.Collections;

public class RampageLevelController : MonoBehaviour {

    public int levelNumber = 5;
    public AppearingMenu appearingMenu;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredWatermelons = 1;
    private int currentWatermelonKillCount;

    public void CheckIfChallengePassed()
    {
        currentWatermelonKillCount++;
        if(currentWatermelonKillCount>requiredWatermelons)
        {
            return;//we only need to do this once.
        }

        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if (levelsUnlocked == levelNumber)
        {
            if (currentWatermelonKillCount >= requiredWatermelons)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (GameController.appearingMenuMode)
                {
                    if (appearingMenu == null)
                    {
                        appearingMenu = FindObjectOfType<AppearingMenu>();
                    }
                    if (appearingMenu)
                    {
                        //appearingMenu.OnChallengedPassed();
                    }
                }
                else
                {
                    if (sound && successClip)
                    {
                        sound.clip = successClip;
                        sound.Play();
                    }
                }
            }
        }
    }
}
