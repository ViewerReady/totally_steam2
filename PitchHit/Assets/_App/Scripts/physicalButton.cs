﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class physicalButton : MonoBehaviour {

	public ParticleSystem particleOnPress;

	public UnityEvent onButtonPress;
	// TODO: public UnityEvent onButtonRelease;

	public Animation buttonPressAnimation;

	public bool pressMe = false;

	public void press(){
		if (buttonPressAnimation != null && !buttonPressAnimation.isPlaying) {
			buttonPressAnimation.Play ();
		

			onButtonPress.Invoke ();
            if (particleOnPress)
            {
                particleOnPress.transform.position = transform.position;
                particleOnPress.Play();
            }
		}
	}

	void OnCollisionEnter(Collision col){
        // Do not press on ball collision
        if (col.gameObject.layer != LayerMask.NameToLayer("Bat"))
        {
            return;
        }
		press (); 
	}
    
	void OnTriggerEnter(Collider collider){
        // Do not press on ball collision
        if (collider.gameObject.layer != LayerMask.NameToLayer("Bat"))
        {
            return;
        }
        press ();
	}

	// Use this for initialization
	void Awake () {
		buttonPressAnimation = GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (pressMe == true) {
			press ();
			pressMe = false;
		}
	}
}
