﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(HingeJoint))]
[RequireComponent(typeof(Interactable))]
public class CameraPanelLever : MonoBehaviour
{
    SteamVR_Action_Boolean grabObject;

    HingeJoint leverJoint;
    [SerializeField] float leverAngle;

    private bool beingHeldSteamVR;
    private bool beingHeldOculus;
    private Hand controllingHand;
    private OVRGrabber controllingGrabber;
    private Vector3 originalPosition;
    private Vector3 originalRotation;
    [SerializeField] float rotationSpeed = 200f;    //If you raise your arm 1 Unity unit, this is the amount of degrees we rotate.
    [SerializeField] float minAngle;
    [SerializeField] float maxAngle;

    private CinemachineVirtualCamera currentCam;
    private Dictionary<CinemachineVirtualCamera, float> initialFovValues;

    private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & (~Hand.AttachmentFlags.SnapOnAttach) & (~Hand.AttachmentFlags.DetachOthers);

    void Awake()
    {
        grabObject = SteamVR_Actions.baseballSet_GrabObject;
        leverJoint = gameObject.GetComponent<HingeJoint>();
        initialFovValues = new System.Collections.Generic.Dictionary<CinemachineVirtualCamera, float>();
    }

    void Start()
    {
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, ((minAngle + maxAngle) / 2));
    }

    void Update()
    {
        if (beingHeldSteamVR)
        {
            float yDifference = originalPosition.y - controllingHand.transform.position.y;
            transform.localEulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + yDifference * rotationSpeed);
            if (!grabObject.GetLastState(controllingHand.handType))
            {
                beingHeldSteamVR = false;
                //controllingHand.ShowHand();
                CustomHand customHand = (controllingHand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.ShowHand();
            }
        }
        else if (beingHeldOculus)
        {
            float yDifference = originalPosition.y - controllingGrabber.transform.position.y;
            transform.localEulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + yDifference * rotationSpeed);
        }

        float currZRotation = transform.localRotation.eulerAngles.z;
        //Debug.Log("Euler angle z is: " + currZRotation);
        if (currZRotation < minAngle)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, minAngle);
        }
        else if (currZRotation > maxAngle)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, maxAngle);
        }

        if (currentCam != SpectatorCameraController.Instance.currentCam)
        {
            currentCam = SpectatorCameraController.Instance.currentCam;
        }

        if (currentCam == null) return;

        if (!initialFovValues.ContainsKey(currentCam))
        {
            Debug.Log("Adding initial value for " + currentCam);
            initialFovValues.Add(currentCam, currentCam.m_Lens.FieldOfView);
        }
        currZRotation = transform.localRotation.eulerAngles.z;
        float diff = currZRotation - ((minAngle + maxAngle) / 2f);
        //Debug.Log("Diff is " + diff);
        float newFov = initialFovValues[currentCam] + diff;
        //Debug.Log("Setting FoV to " + newFov);
        currentCam.m_Lens.FieldOfView = newFov;
    }

    //-------------------------------------------------
    // Called every Update() while a Hand is hovering over this object
    //-------------------------------------------------
    private void HandHoverUpdate(Hand hand)
    {
        if (grabObject.GetLastStateDown(hand.handType))
        {
            if (!beingHeldSteamVR)
            {
                beingHeldSteamVR = true;
                controllingHand = hand;
                //controllingHand.HideHand();
                CustomHand customHand = (hand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.HideHand();
                leverJoint.useSpring = false;
                originalPosition = hand.transform.position;
                originalRotation = transform.localEulerAngles;
            }
        }
    }

    public void CustomGrabbableGrabBegin(OVRGrabber grabber)
    {
        if (!beingHeldOculus)
        {
            beingHeldOculus = true;
            controllingGrabber = grabber;
            //controllingGrabber.HideHand();
            controllingGrabber.HideGrabber();
            originalPosition = grabber.transform.position;
            originalRotation = transform.localEulerAngles;
        }
    }

    public void CustomGrabbableGrabEnd(OVRGrabber grabber)
    {
        beingHeldOculus = false;
        controllingGrabber.ShowGrabber();
    }
}