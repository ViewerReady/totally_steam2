﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]
public class TableMechanism : MonoBehaviour {

    /// <summary>
    /// This Script is for controlling the animations of the director mode desk.
    /// </summary>

    Animator tableAnimator;

    [SerializeField] UnityEvent OnAnimationStart;
    [SerializeField] UnityEvent OnAnimationEnd;
    bool animating;
    float targetProgress;
    float currentProgress;
    [SerializeField] float animationSpeed = 1f;

    [SerializeField] AudioSource tableSoundSource;
    [SerializeField] AudioClip introClip;
    [SerializeField] AudioClip outroClip;

    void Awake()
    {
        tableAnimator = gameObject.GetComponent<Animator>();
        tableSoundSource = gameObject.GetComponent<AudioSource>();
        animating = false;
        targetProgress = 0f;
        currentProgress = 0f;
    }

    void Update()
    {
        if(currentProgress > targetProgress)
        {
            currentProgress -= (Time.deltaTime * animationSpeed);
            if (currentProgress < targetProgress) currentProgress = targetProgress;
            tableAnimator.SetFloat("Progress", currentProgress);
        }
        else if (currentProgress < targetProgress)
        {
            currentProgress += (Time.deltaTime * animationSpeed);
            if (currentProgress > targetProgress) currentProgress = targetProgress;
            tableAnimator.SetFloat("Progress", currentProgress);
            if(currentProgress == targetProgress)
            {
                OnAnimationEnd.Invoke();
            }
        }
    }

    [ContextMenu("SHOW IT")]
    public void Show()
    {
        targetProgress = 1f;
        tableSoundSource.clip = introClip;
        tableSoundSource.Play();
        OnAnimationStart.Invoke();
    }

    [ContextMenu("HIDE IT")]
    public void Hide()
    {
        targetProgress = 0f;
        tableSoundSource.clip = outroClip;
        tableSoundSource.Play();
        OnAnimationStart.Invoke();
    }
}
