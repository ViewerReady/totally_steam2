﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class SpaceLevelController : MonoBehaviour {
    public float maxHealth = 100f;
    public float damagePerHit = 34f;
    public AudioClip gameOverSound;
    public UnityEvent OnGameEnded;
    private float currentHealth;
    private HeadsUpDisplay hud;
    private AudioSource sound;
	// Use this for initialization
	void Start () {
        hud = FindObjectOfType<HeadsUpDisplay>();
        currentHealth = maxHealth;
        sound = GetComponent<AudioSource>();
        if(!ScoreboardController.instance.scorable)
        {
            ScoreboardController.instance.scorable = true;
        }
	}

    public void BeginLevel()
    {
        StartCoroutine(GainScoreOverTime());
    }

    IEnumerator GainScoreOverTime()
    {
        yield return new WaitForSeconds(1f);

        while (ScoreboardController.instance.scorable)
        {
            //Debug.Log("tick");
            ScoreboardController.instance.AddScore();
            yield return new WaitForSeconds(1f);
        }
    }

    public void TakeDamage()
    {
        currentHealth -= damagePerHit;
        if(currentHealth<0)
        {
            currentHealth = 0f;
        }

        hud.SetHealthPercentage(currentHealth / maxHealth);
        //Debug.Log("curr health "+currentHealth);
        if(currentHealth<=0 && ScoreboardController.instance.scorable)
        {
            //Debug.Log("should be dead " + currentHealth);
            //stats.scorable = false;
            hud.DisplayFinalScore(ScoreboardController.instance.myScore);
            ScoreboardController.instance.FinalizeScore();

            sound.loop = false;
            sound.Stop();
            if (gameOverSound)
            {
                sound.clip = gameOverSound;
                sound.Play();
            }
            OnGameEnded.Invoke();
        }
    }
}
