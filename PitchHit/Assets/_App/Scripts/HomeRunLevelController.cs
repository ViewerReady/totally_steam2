﻿using UnityEngine;
using System.Collections;
//using Valve.VR.InteractionSystem;

public class HomeRunLevelController : MonoBehaviour {

    public static HomeRunLevelController instance;
    public Transform[] fenceCorners;//make sure homeplate is first and that they are arranged in a circle;
    public Transform homePlate;
    public Transform thirdBase;
    public Transform secondBase;
    public Transform firstBase;

    public int levelNumber = 1;
    //public AppearingMenu appearingMenu;
    public AudioSource sound;
    public AudioClip successClip;
    public int requiredPoints = -1;

    private Vector2[] polyPoints;
    private Vector2 homePlatePoint;
    private Vector2 thirdBasePoint;
    private Vector2 firstBasePoint;

    private bool stillCheckingHomerun;
    private bool homerunCheckResult;
    // Use this for initialization
    void Start()
    {
        polyPoints = new Vector2[fenceCorners.Length];
        for (int c = 0; c < fenceCorners.Length; c++)
        {
            polyPoints[c] = new Vector2(fenceCorners[c].position.x, fenceCorners[c].position.z);
        }
		if (homePlate) {
			homePlatePoint = new Vector2 (homePlate.localPosition.x, homePlate.localPosition.z);
			thirdBasePoint = new Vector2 (thirdBase.localPosition.x, thirdBase.localPosition.z);
			firstBasePoint = new Vector2 (firstBase.localPosition.x, firstBase.localPosition.z);
		
			TrajTester1.homeRunDetector = this;
			instance = this;
		}
    }

    public bool isPointFoulBall(Vector3 point)
    {
        Vector2 spot = new Vector2(point.x, point.z);

        float angleLeft = Vector2.Angle(thirdBasePoint - homePlatePoint, spot - homePlatePoint);
        float angleRight = Vector2.Angle(firstBasePoint - homePlatePoint, spot - homePlatePoint);
        float angleBoth = Vector2.Angle(thirdBasePoint - homePlatePoint, firstBasePoint - homePlatePoint);
        //Debug.Log("-------------------------------------Foulball? both"+ angleBoth+"   left"+angleLeft+" right"+angleRight+" solution" + Mathf.Abs(angleBoth - (angleLeft + angleRight)));
        return Mathf.Abs(angleBoth - (angleLeft + angleRight)) > .1f;
    }

    public bool isHomeRun(Vector3 point)
    {
        if (isPointFoulBall(point))
        {
            return false;
        }

        if (ContainsPoint(point))
        {
            return false;
        }

        

        return true;
    }
    public bool isOutsideFence(Vector3 point)
    {
        if (ContainsPoint(point))
        {
            return false;
        }
        return true;
    }

    public bool ContainsPoint(Vector3 s)
    {
        Vector2 p = new Vector2(s.x, s.z);

        int j = polyPoints.Length - 1;
        bool inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            if (((polyPoints[i].y <= p.y && p.y < polyPoints[j].y) ||
                    (polyPoints[j].y <= p.y && p.y < polyPoints[i].y)) &&
             (p.x < (polyPoints[j].x - polyPoints[i].x) * (p.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x))
                inside = !inside;
        }
        return inside;
    }

    public void StartHomerunCheck(Rigidbody ballBody)
    {
        StartCoroutine(CheckForHomerunRoutine(ballBody));
    }

    public bool IsStillHomerunChecking()
    {
        return stillCheckingHomerun;
    }

    public bool HomerunCheckResult()
    {
        return homerunCheckResult;
    }

    //Greg needs a real homerun check.
    IEnumerator CheckForHomerunRoutine(Rigidbody ballBody)
    {
        TrajTester1 judger = ballBody.GetComponent<TrajTester1>();
        if (!judger)
        {
            yield break;
        }
        stillCheckingHomerun = true;
        yield return new WaitForSeconds(.2f);
        Vector3 impactPosition = Trajectory.GetLandingSpot(ballBody);
        bool seemsToBeHomerun = isHomeRun(impactPosition);

        if (seemsToBeHomerun)//make sure!
        {
            if (judger)
            {
                
                judger.BeginJudgement();
                while (judger.isJudging())
                {
                    Debug.Log("judger still judging.");
                    yield return null;
                }
                seemsToBeHomerun = judger.isTrueHomeRun();
                if (seemsToBeHomerun)
                {
                    homerunCheckResult = true;
                }
                else
                {
                    homerunCheckResult = false;
                }
            }
        }
        else
        {
            homerunCheckResult = false;
        }

        stillCheckingHomerun = false;
        Debug.Log("judger done.");
    }

    public void CheckIfChallengePassed()
    {
		
    }
}
