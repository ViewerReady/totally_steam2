﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmetParticle : MonoBehaviour {

	void OnEnable ()
    {
        Material mat = FullGameDirector.currentOffensiveTeam.helmetMaterial;
        gameObject.GetComponent<ParticleSystemRenderer>().material = mat;
	}
}
