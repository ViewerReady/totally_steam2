﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VRAustinHotkeys : MonoBehaviour
{
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            HandManager.ToggleThrowingType();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            StartCoroutine(RestartAfterFiveSeconds());
        }
    }

    IEnumerator RestartAfterFiveSeconds()
    {
        float timer = 0.0f;
        while(timer < 5.0f)
        {
            if (Input.GetKey(KeyCode.X))
            {
                timer += Time.deltaTime;
                yield return null;
            }
            else
            {
                yield break;
            }
        }
        SceneManager.LoadScene(0);
    }
}
