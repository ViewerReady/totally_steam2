﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class TerrainSampler : MonoBehaviour {
#if UNITY_EDITOR
    public bool hoverDebug = true;
    private RaycastHit hit;


    private void OnEnable()
    {
        SceneView.onSceneGUIDelegate += OnScene;
    }

    private void OnDisable()
    {
        SceneView.onSceneGUIDelegate -= OnScene;

    }

    // Update is called once per frame
    void OnScene(SceneView scene) {
        Event e = Event.current;
        bool click = (e != null && e.type == EventType.MouseDown && e.button == 0);

        if (hoverDebug)
        {
            Ray mouseRay = HandleUtility.GUIPointToWorldRay(e.mousePosition);
            if (Physics.Raycast(mouseRay, out hit, LayerMask.GetMask("Ground")))
            {
                Terrain terrain = hit.transform.GetComponent<Terrain>();

                if (terrain)
                {
                    int g = GetMainTexture(terrain, hit.point);
                    Color c = Color.yellow;
                    c.a = .25f;
                    Handles.color = c;
                    Handles.DrawLine(hit.point, hit.point + Vector3.up);
                    Handles.DrawSolidDisc(hit.point, hit.normal, .5f);
                    Handles.Label(hit.point + hit.normal, g.ToString());
                    HandleUtility.Repaint();
                }
            }

        }

    }
#endif



    public static float[] GetTextureMix(Terrain terrain, Vector3 worldPos)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.
        // The number of values in the array will equal the number
        // of textures added to the terrain.
        TerrainData terrainData = terrain.terrainData;
        Vector3 terrainPos = terrain.transform.position;
        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((worldPos.x - terrainPos.x) / terrainData.size.x) * terrainData.alphamapWidth);
        int mapZ = (int)(((worldPos.z - terrainPos.z) / terrainData.size.z) * terrainData.alphamapHeight);
        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = terrainData.GetAlphamaps(mapX, mapZ, 1, 1);
        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];
        for (int n = 0; n < cellMix.Length; ++n)
     {
            cellMix[n] = splatmapData[0, 0, n];
        }
        return cellMix;
    }

    public static int GetMainTexture(Vector3 worldPos)
    {
        if(Terrain.activeTerrain)
            return GetMainTexture(Terrain.activeTerrain, worldPos);

        return -1;
    }

    public static int GetMainTexture(Terrain terrain, Vector3 worldPos)
    {
        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(terrain, worldPos);
        float maxMix = 0;
        int maxIndex = 0;
        // loop through each mix value and find the maximum
        for (int n = 0; n < mix.Length; ++n)
     {
            if (mix[n]> maxMix)
         {
                maxIndex = n;
                maxMix = mix[n];
            }
        }
        return maxIndex;
    }
}
