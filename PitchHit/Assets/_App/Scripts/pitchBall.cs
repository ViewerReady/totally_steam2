﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class pitchBall : MonoBehaviour {

	public float pitchingSpeed;
    public float maxTargetedPitchingSpeed = 25f;
    protected float minTargetedPitchingSpeed = 17f;
    public Transform pitchTarget;
    public AudioSource sound;
    public Image timerImage;

    protected float timeOfLastPitch;
    protected float targetVerticalOffset = .1f;
    protected float timeToRechargePitch = 3f;
    protected float maxPitchingSpeedRandomness = 5f;
    public bool keepPitching;
    public bool hasPitched;
    public bool shutDown;

    protected void OnEnable()
    {

    }

    protected void OnDisable()
    {
        keepPitching = false;
        hasPitched = false;
    }

	public virtual void pitch(GameObject prefab){
		Rigidbody theBall = Instantiate(prefab).GetComponent<Rigidbody>();
        theBall.transform.position = transform.position;

        if (pitchTarget)
        {
            float timePassed = Time.time - timeOfLastPitch;
            float currentMaxSpeed = maxTargetedPitchingSpeed - (maxPitchingSpeedRandomness * Random.value);
            float currentPitchSpeed = minTargetedPitchingSpeed + (Mathf.Clamp(timePassed / timeToRechargePitch, 0f, 1f) * (maxTargetedPitchingSpeed - minTargetedPitchingSpeed));
            theBall.velocity = Trajectory.GetLaunchVelocityByLanding(theBall.transform.position, pitchTarget.position+ (Vector3.up * targetVerticalOffset), currentPitchSpeed);
        }
        else
        {
            theBall.AddForce (transform.forward * pitchingSpeed);
        }

        hasPitched = true;
        timeOfLastPitch = Time.time;
        theBall.GetComponent<hittable>().SetOrigin(ballController.ballServiceType.pitcher,transform);
        //return theBall.GetComponent<hittable>();

        if(sound && sound.clip)
        {
            sound.Play();
        }
	}

    Coroutine automaticPitchingRoutine;
    public void StartConstantPitching(GameObject prefab, float yellowtime = 2.5f, float greenTime = .5f)
    {
        if(shutDown)
        {
            Debug.Log("pitcher already shut down.");
            return;
        }
        //Debug.Log("trying to start constant pitching.");
        if(!keepPitching)
        {
            if(automaticPitchingRoutine!=null)
            {
                StopCoroutine(automaticPitchingRoutine);
            }

            keepPitching = true;
            automaticPitchingRoutine = StartCoroutine(ConstantPitching(prefab, yellowtime, greenTime));
        }
    }

    public void StopConstantPitching(bool shutItDown)
    {
        if (automaticPitchingRoutine != null)
        {
            StopCoroutine(automaticPitchingRoutine);
        }

        keepPitching = false;
        hasPitched = false;
        shutDown = shutItDown;
    }


    public IEnumerator ConstantPitching(GameObject prefab, float yellowtime, float greenTime)
    {
        Debug.Log("constant pitch routine");
        if (timerImage)
        {
            timerImage.fillAmount = 0f;
        }
        while (!GameController.levelIsBegun)
        {
            yield return null;
        }

        float autoPitchTimer = 0;

        Transform camTransform = Camera.main.transform;

        while (keepPitching)
        {
            while (autoPitchTimer < yellowtime && !hasPitched)
            {
                while(GameController.isAtBat==false || (Vector3.Angle(camTransform.forward, (transform.position-camTransform.position))>25f && !hasPitched))
                {
                    if (timerImage)
                    {
                        timerImage.color = Color.red;
                    }
                    yield return null;
                }
                if (timerImage)
                {
                    timerImage.color = Color.yellow;
                }
                autoPitchTimer += Time.deltaTime;
                if (timerImage)
                {
                    timerImage.fillAmount = autoPitchTimer / yellowtime;
                }
                yield return null;
            }

            if (timerImage)
            {
                timerImage.color = Color.green;
            }

            if (!hasPitched)
            {
                pitch(prefab);
            }

            while(!hasPitched)
            {
                yield return null;

            }
            autoPitchTimer = 0f;

            while(autoPitchTimer<greenTime)
            {
                if(hasPitched)
                {
                    hasPitched = false;
                    autoPitchTimer = 0;
                }
                autoPitchTimer += Time.deltaTime;
                yield return null;
            }
            yield return new WaitForSeconds(greenTime);
        }

        if (timerImage)
        {
            timerImage.color = Color.red;
        }
        Debug.Log("routine over.");
    }
	
}
