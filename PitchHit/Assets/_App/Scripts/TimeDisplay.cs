﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeDisplay : MonoBehaviour {
    public Text[] timeTexts;
    private float startTime;

    public void BeginTime()
    {
        StartCoroutine(TickTockRoutine());

    }

    IEnumerator TickTockRoutine()
    {
        startTime = Time.timeSinceLevelLoad;
        yield return new WaitForSeconds(1f);
        while (ScoreboardController.instance.scorable)
        {
            string temp = "TIME\n" + (int)(Time.timeSinceLevelLoad-startTime);
            foreach (Text t in timeTexts)
            {
                t.text = temp;
            }
            yield return new WaitForSeconds(1f);
        }
    }
}
