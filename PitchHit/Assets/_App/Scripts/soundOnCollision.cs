﻿using UnityEngine;
using System.Collections;

public class soundOnCollision : MonoBehaviour {

	public float soundThreshold;

	public AudioClip[] CollisionSound;

	private AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
	}

	void OnCollisionEnter(Collision collisionInfo){
		if (collisionInfo.rigidbody.CompareTag("ball") && !source.isPlaying) {
            source.clip = CollisionSound [Random.Range (0, CollisionSound.Length)];
			source.Play ();

           // if (GetComponent<pointsOnShatter>() != null)
           // {
           //     GetComponent<pointsOnShatter>().PostSplit();
           // }
		}
	}

	// Update is called once per frame
	//void Update () {
	
	//}
}
