﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiplayerScoreboard : ScoreboardController {
    public Text currentOutsText;
    public Text currentFoulsText;

    public void UpdateMultiplayerScoreboard(int theirScore, int currentOuts, int currentFouls)
    {
        scoreboardText.text = myScore + " - " + theirScore;
        currentOutsText.text = "" + currentOuts;
        currentFoulsText.text = "" + currentFouls;
    }
}
