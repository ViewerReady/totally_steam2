﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitcherMenuFrame : MonoBehaviour {
    [SerializeField]
    Transform top;
    [SerializeField]
    Transform bottom;
    [SerializeField]
    Transform left;
    [SerializeField]
    Transform right;

    float buttonWidth;
    float buttonHeight;
	// Use this for initialization
	//void Start () {
		
	//}
	
	// Update is called once per frame
	//void Update () {
		
	//}

    public void Init(float buttonWidth, float buttonHeight)
    {
        top.localScale = new Vector3(buttonWidth + 10, top.localScale.y, top.localScale.z);
        bottom.localScale = new Vector3(buttonWidth + 10, bottom.localScale.y, bottom.localScale.z);

        top.localPosition = new Vector3(top.localPosition.x, buttonHeight / 2 - .5f, top.localPosition.z);
        bottom.localPosition = new Vector3(bottom.localPosition.x, -buttonHeight / 2 + .5f, bottom.localPosition.z);

        left.localScale = new Vector3(left.localScale.x, buttonHeight, left.localScale.z);
        right.localScale = new Vector3(right.localScale.x, buttonHeight, right.localScale.z);

        left.localPosition = new Vector3(-buttonWidth / 2 - 5f + (left.localScale.x / 2), left.localPosition.y, left.localPosition.z);
        right.localPosition = new Vector3(buttonWidth / 2 + 5f - (right.localScale.x / 2), right.localPosition.y, right.localPosition.z);
    }
}
