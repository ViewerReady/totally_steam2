﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeadsUpDisplay : MonoBehaviour {
    public RectTransform healthBarGreen;
    public RectTransform healthBarRed;
    public Text gameOverText;
    public CanvasGroup group;
    public bool healthAlwaysVisible = true;

    private float maxHealthBarWidth;
	// Use this for initialization
	void Awake () {
        maxHealthBarWidth = healthBarRed.rect.width;
        Debug.Log("bar width " + maxHealthBarWidth);
        if (!healthAlwaysVisible)
        {
            group.alpha = 0f;
        }
	}

    public void SetHealthPercentage(float p)
    {
        if(p<0)
        {
            p = 0f;
        }
        healthBarGreen.offsetMax = Vector2.right * (1f - p) * maxHealthBarWidth * -1f;
        group.alpha = 1f;
        if (p > 0 && !healthAlwaysVisible)
        {
            StartCoroutine(FadeAway());
        }
    }

    public void DisplayFinalScore(float finalScore)
    {
        if (gameOverText)
        {
            gameOverText.gameObject.SetActive(true);
            gameOverText.enabled = true;
            gameOverText.text = "FINAL: " + finalScore;

        }
    }

    IEnumerator FadeAway()
    {
        yield return new WaitForSeconds(1f);
        while(group.alpha>0)
        {
            group.alpha -= Time.deltaTime;
            yield return null;
        }
    }
}
