﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HandCannonAnimationHandler : MonoBehaviour
{
    public Animator animator;
    public new AudioSource audio;
    public AudioClip appearingSound;
    public AudioClip firingSound;
    public AudioClip disappearingSound;
    //public PitcherMenu pitcherMenu;

    public GameObject formationVisual;
    public GameObject fullyFormedVisual;
    public ParticleSystem firingVisual;

    public GameObject despawnVisualPrefab;
    public Transform despawnVisualLocation;
    public GameObject cannonSmokePrefab;
    public Transform cannonSmokeLocation;

    public bool isActive
    {
        get; protected set;
    }
    private float transitionProgress = 0f;
    private Coroutine currentProgression;
    private Coroutine powerRoutine;

    public float transitionInSpeed = 1.5f;
    public float transitionOutSpeed = 2f;


    private float timeFinishedAppearing = Mathf.Infinity;

    public Action OnFinishIn;
    public Action OnFinishOut;

    /*
    void OnDisable()
    {
        transitionProgress = 0;
        isActive = false;
    }
    */

    void Update()
    {
        UpdateTransition();
    }

    void OnEnable()
    {
        SetTransitionProgress(transitionProgress);
    }


    void UpdateTransition()
    {
        if (isActive && transitionProgress < 1)
        {
            TransitionForward();
        }
        else if (!isActive && transitionProgress > 0)
        {
            TransitionBackward();
        }
    }

    void TransitionForward()
    {
        SetTransitionProgress(transitionProgress + Time.deltaTime * transitionInSpeed);
    }

    void TransitionBackward()
    {
        SetTransitionProgress(transitionProgress - Time.deltaTime * transitionOutSpeed);
    }

    void SetTransitionProgress(float progress)
    {
        float lastProgress = transitionProgress;
        transitionProgress = progress;
        transitionProgress = Mathf.Clamp01(transitionProgress);
        animator.SetFloat("TransitionProgress", transitionProgress);
        if (lastProgress < 1 && transitionProgress == 1)
        {
            OnFinishAppearing();
        }
        else if (lastProgress > 0 && transitionProgress == 0)
        {
            OnFinishDisappearing();
        }
    }


    void SetAppearing(bool appear)
    {
        //Debug.Log("SET APPEARING--------------------- " + appear);
        bool wasActive = isActive;
        isActive = appear;
        //appear
        if (isActive && !wasActive)
        {
            /*if (audio)
            {
                audio.clip = appearingSound;
                if (transitionProgress > 0)
                {
                    audio.time = transitionProgress * audio.clip.length;
                }
                //audio.Play();
                DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_open");
            }*/
            //DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_open");
        }
        //disappear
        else if (!isActive && wasActive)
        {
            /*if (audio)
            {
                audio.clip = disappearingSound;
                if (transitionProgress < 1)
                {
                    audio.time = (1f - transitionProgress) * audio.clip.length;
                }
                //audio.Play();
                DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_close");
            }*/
            //DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_close");
        }
    }

    public void StartAppearing()
    {
        //Debug.Log("xxxxxxxx STARTAPPEARINGAFTERHANDAVAILABLE 3 ");

        if (isActive)
        {
            //Debug.Log("it's already active! " + isActive);
            return;
        }

        SetAppearing(true);

        //Debug.Log("start appearing now! " + isActive);

        if (currentProgression != null) StopCoroutine(currentProgression);


        gameObject.SetActive(true);

        //Greg: update visual effects.
        StartCoroutine(RestartVFX(formationVisual));
        //Debug.Log("OnStartFormation.");
        fullyFormedVisual.SetActive(false);
        //despawnVisual.SetActive(false);


        isActive = true;
        //Debug.Log("here");
        //animator.SetBool("IsActive", isActive);
        //Debug.Log("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy make appearing sound.");
        if (audio)
        {
            audio.clip = appearingSound;
            //audio.Play();
            DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_open");
        }
        if (currentProgression == null)
        {
            //Debug.Log("start the process to appear.");
            transitionProgress = 0;
            currentProgression = StartCoroutine(TransitionRoutine());
        }
        else
        {
            //Debug.Log("get the hand cannon transition to start appearing again.");
            if (audio)
            {
                audio.time = transitionProgress * audio.clip.length;
            }
        }

    }

    public void OnFinishAppearing()
    {
        powerRoutine = StartCoroutine(PowerMeterRoutine());
        timeFinishedAppearing = Time.time;
        //Debug.Log("On Finish Appearing.");

        if (OnFinishIn != null) OnFinishIn.Invoke();

        //		Debug.Log ("On Finish Appearing.");
        //pitcherMenu.OnFinishAppearing();
        StartCoroutine(PowerMeterRoutine());
        currentProgression = null;
        transitionProgress = 2f;
        timeFinishedAppearing = Time.time;

        fullyFormedVisual.SetActive(true);
        //firingVisual.SetActive(false);
        //despawnVisual.SetActive(false);

    }

    public void Fire()
    {
        //Debug.Log("START THE FIRING ANIMATION NOW!!!");
        //animator.SetLayerWeight(1, 0f);
        animator.SetTrigger("Fire");
//        Debug.Log("PLAYING CANNON FIRE SOUND");
        DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_shot");
        fullyFormedVisual.SetActive(true);

        firingVisual.Play();
        GameObject cannonSmokeVisualObj = Instantiate(cannonSmokePrefab, cannonSmokeLocation.position, cannonSmokeLocation.rotation);
        cannonSmokeVisualObj.GetComponentInChildren<ParticleSystem>().Play();
        Destroy(cannonSmokeVisualObj, 2f);
        //StartCoroutine(RestartVFX(firingVisual));
        //despawnVisual.SetActive(false);
    }

    public void OnFinishedFiring()
    {
        //Debug.Log("_______________________________________________________________________________OnFinishedFiring.");
        if (FullGameDirector.Instance && FullGameDirector.Instance.defense)
        {
            //Debug.Log("finished firing during runningplay "+ FieldManager.Instance.defense.runningPlay);
            FieldPlayType playAtTheMoment = FullGameDirector.Instance.defense.runningPlay;

            if (playAtTheMoment == FieldPlayType.PITCH || playAtTheMoment == FieldPlayType.NONE || playAtTheMoment == FieldPlayType.DISABLED)
            {
                StartCoroutine(WaitToSeeIfBallHit());
                return;
            }
        }
        //Debug.Log("Start Disappearing immediately.");
        isActive = true;
        transitionProgress = 1f;
        StartDisappearing();
    }

    IEnumerator WaitToSeeIfBallHit()
    {
       // Debug.Log("WaitToSeeIfBallHit");
        if (FieldMonitor.Instance && FieldMonitor.ballInPlay)
        {

            hittable ball = FieldMonitor.ballInPlay;
            while (ball && !ball.hasBeenCaught && !ball.GetWasHitByBat() && !ball.hitGround)
            {
                //Debug.Log("wait " + ball + " " + ball.hasBeenCaught + " " + ball.GetWasHitByBat() + " " + ball.hitGround);
                yield return null;
            }
            //Debug.Log("done waiting.");
            if (ball && ball.GetWasHitByBat())
            {
                //Debug.Log("do it.");
                isActive = true;
                transitionProgress = 1f;
                StartDisappearing();
            }
        }
    }

    public void StartDisappearing()
    {
       // Debug.Log("START DISAPPEARING=============================================");

        if (!isActive)
        {
           // Debug.Log("but already disappeared!.");
            OnFinishDisappearing();
            return;
        }
        SetAppearing(false);

        if (powerRoutine != null) StopCoroutine(powerRoutine);

        //Debug.Log("startdisappearing!");

        isActive = false;

        //Debug.Log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx make disappearing sound.");
        if (audio)
        {
            audio.clip = disappearingSound;
            audio.Play();
        }
        animator.SetLayerWeight(1, 0f);
        animator.SetFloat("Power", 0f);

        if (currentProgression == null)
        {
            //Debug.Log("start the process to disappear.");
            transitionProgress = 1f;
            currentProgression = StartCoroutine(TransitionRoutine());
        }
        else
        {
            //Debug.Log("just reverse it to disappear.");
            if (audio)
            {
                audio.time = (1f - transitionProgress) * audio.clip.length;
            }
        }

        //Greg: update VFX.
        formationVisual.SetActive(false);
        fullyFormedVisual.SetActive(false);
        //despawnVisual.SetActive(true);
        GameObject despawnVisualObj = Instantiate(despawnVisualPrefab, despawnVisualLocation.position, despawnVisualLocation.rotation);
        despawnVisualObj.GetComponentInChildren<ParticleSystem>().Play();
        Destroy(despawnVisualObj, 2f);
    }


    public void Disappear_Immediate()
    {
        SetAppearing(false);
        SetTransitionProgress(0);
    }

    public void OnFinishDisappearing()
    {
        //Debug.Log("did finish disappearing.");
        if (OnFinishOut != null) OnFinishOut.Invoke();

        //Debug.Log("did finish disappearing.");
        //pitcherMenu.OnFinishDisappearing();
        gameObject.SetActive(false);
        currentProgression = null;

        formationVisual.SetActive(false);
    }

    public bool IsFullyAppeared(float delay = 0f)
    {
        return transitionProgress >= 1f && (Time.time > (timeFinishedAppearing + delay));
        /*
		return (transitionProgress >= 2f && (Time.time>(timeFinishedAppearing+delay)));
        */
    }

    public void OnStartFiring()
    {
        StartCoroutine(FiringRoutine());
    }

    IEnumerator FiringRoutine()
    {
        if (!isActive)
        {
            yield break;
        }
        /*if (audio)
        {
            audio.time = 0f;
            audio.clip = firingSound;
            //audio.Play();
            //DarkTonic.MasterAudio.MasterAudio.PlaySound("hand_canon_shot");
            yield return new WaitForSeconds(firingSound.length);
            if (audio.clip != firingSound)
            {
                yield break;
            }
        }*/
        //pitcherMenu.TurnOff();
    }


    IEnumerator TransitionRoutine()
    {
#if RIFT
        /*
        yield return null;
        if (isActive)//Greg: I'm just putting this here to try and accomodate the Rift hand.
        {
            if (attachedHand)
            {
                attachedHand.isOccupied = true;
                attachedHand.MakeHandInvisible();
            }
        }
        */
#endif

        bool justStarting = true;
        //        Debug.Log("starting transition routine. " + transitionProgress);
        while (justStarting || (transitionProgress >= 0 && transitionProgress <= 1))
        {
            justStarting = false;
            if (isActive)
            {
                transitionProgress += Time.deltaTime * transitionInSpeed;
                //                Debug.Log("hand cannon transition in " + transitionProgress);
            }
            else
            {
                transitionProgress -= Time.deltaTime * transitionOutSpeed;
                //                Debug.Log("hand cannon transition out " + transitionProgress);
            }
            //            Debug.Log("transitioning. " + transitionProgress);
            animator.SetFloat("TransitionProgress", transitionProgress);

            yield return null;
        }

        //        Debug.Log("end of tranisition routine. " + transitionProgress);
        if (isActive)
        {
            animator.SetFloat("TransitionProgress", 1);
            OnFinishAppearing();
#if RIFT
            //attachedHand.MakeHandInvisible();
#endif
        }
        else
        {
            animator.SetFloat("TransitionProgress", 0);
            OnFinishDisappearing();
        }

    }


    float currentPower = 0f;
    IEnumerator PowerMeterRoutine()
    {
        //animator.SetLayerWeight(1, 1f);

        float timeToHold = (UnityEngine.Random.value * 1.5f) + 1;
        currentPower = 0f;

        while (timeToHold > 0 || true)
        {
            if (!isActive)
            {
                yield break;
            }

            timeToHold -= Time.deltaTime;

            currentPower += Time.deltaTime;
            if (currentPower > 1)
            {
                currentPower -= 1f;
            }
            //animator.SetFloat("Power", currentPower);
            animator.SetFloat("TurbineSpeed", (currentPower * 9f) + 1f);
            yield return null;
        }

        OnStartFiring();
    }

    public float GetCurrentPower()
    {
        return currentPower;
    }

    IEnumerator RestartVFX(GameObject vfx)
    {
        vfx.SetActive(false);
        yield return null;
        vfx.SetActive(true);
    }
}
