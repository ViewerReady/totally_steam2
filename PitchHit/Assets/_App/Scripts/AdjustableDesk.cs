﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;

[RequireComponent(typeof(Interactable))]
public class AdjustableDesk : MonoBehaviour
{
    SteamVR_Action_Boolean grabObject;
    private bool beingHeldSteamVR;
    private bool beingHeldOculus;
    private Hand controllingHand;
    private OVRGrabber controllingGrabber;
    private Vector3 originalPosition;
    private float previousHandPositionY;

    void Awake()
    {
        grabObject = SteamVR_Actions.baseballSet_GrabObject;
    }

    void Update()
    {
        if (beingHeldSteamVR)
        {
            float yDifference = previousHandPositionY - controllingHand.transform.position.y;
            transform.position = new Vector3(originalPosition.x, transform.position.y - yDifference, originalPosition.z);
            previousHandPositionY = controllingHand.transform.position.y;
            if (!grabObject.GetLastState(controllingHand.handType))
            {
                beingHeldSteamVR = false;
                CustomHand customHand = (controllingHand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.ShowHand();
                //controllingHand.ShowHand();
            }
        }
        else if (beingHeldOculus)
        {
            float yDifference = previousHandPositionY - controllingGrabber.transform.position.y;
            transform.position = new Vector3(originalPosition.x, transform.position.y - yDifference, originalPosition.z);
            previousHandPositionY = controllingGrabber.transform.position.y;
        }
    }

    //-------------------------------------------------
    // Called every Update() while a Hand is hovering over this object
    //-------------------------------------------------
    private void HandHoverUpdate(Hand hand)
    {
        if (grabObject.GetLastStateDown(hand.handType))
        {
            if (!beingHeldSteamVR)
            {
                beingHeldSteamVR = true;
                controllingHand = hand;
                //controllingHand.HideHand();
                CustomHand customHand = (hand.handType == SteamVR_Input_Sources.RightHand ? HandManager.currentRightCustomHand : HandManager.currentLeftCustomHand);
                if (customHand != null) customHand.HideHand();
                previousHandPositionY = controllingHand.transform.position.y;
                originalPosition = transform.position;
            }
        }
    }

    public void CustomGrabbableGrabBegin(OVRGrabber grabber)
    {
        if (!beingHeldOculus)
        {
            beingHeldOculus = true;
            controllingGrabber = grabber;
            controllingGrabber.HideGrabber();
            previousHandPositionY = controllingGrabber.transform.position.y;
            originalPosition = transform.position;
        }
    }

    public void CustomGrabbableGrabEnd(OVRGrabber grabber)
    {
        controllingGrabber.ShowGrabber();
        beingHeldOculus = false;
    }
}
