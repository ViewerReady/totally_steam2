﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FractureListener : MonoBehaviour {
    //public class OnBroken : UnityEvent<FractureListener>{
    //}
    //public bool slowMoBlock = false;
    //public AudioClip enterSlowMoSound;
    //public AudioClip exitSlowMoSound;
    //public UnityEvent OnFirstBreak;
    //public OnBroken OnFirstBreak = new OnBroken ();
    //public OnBroken OnChunkBreak = new OnBroken ();
    public UnityEvent onBreak;
    public GameObject soundPrefab;
	public AudioClip[] breakSounds;
	private bool beenHit;
	private float timeOfLastSound = 0f;
	//public static FractureListener ongoingSlowMoHolder;
	//private Coroutine ongoingSlowMo;
	private DestroyAfterSound ongoingSound;
	// Use this for initialization
	//void Update () {
	//	if (Input.GetKeyDown ("k")) {
	//		Break ();
	//	}
	//}
    /*
	public void SetToSlowMo()
	{
		initialGravity = Physics.gravity;

		//slowMoBlock = true;
		foreach (Renderer r in GetComponentsInChildren<Renderer>(true)) {
			r.material.color = new Color (1f, 0f, 1f, 1f);
		}
	}
    */

	public void Break()
	{

		//Debug.Log ("Break!");

		if (!beenHit) {
			//Debug.Log ("first break;");
			//OnFirstBreak.Invoke (this);
		}

		beenHit = true;
        /*
		if (slowMoBlock) {

			if (ongoingSlowMoHolder) {
				ongoingSlowMoHolder.InterruptSlowMo ();
			}

			ongoingSlowMoHolder = this;
			ongoingSlowMo = StartCoroutine (SlowMoRoutine ());
		}
        */
	}

	public void ChunkBreak ()
	{
		Debug.Log ("++++++++++++++++++++++++++++++++++++++++Chunk break got called.");
        //OnChunkBreak.Invoke (this);
        onBreak.Invoke();
		if (breakSounds.Length > 0 && soundPrefab) {
			if ((Time.time - timeOfLastSound) > .5f && Time.timeSinceLevelLoad>.2f) {
				Instantiate (soundPrefab, transform.position, Quaternion.identity).GetComponent<DestroyAfterSound> ().AssignSound (breakSounds[Random.Range(0,breakSounds.Length)]);
				timeOfLastSound = Time.time;
			}
		}

	}
    /*
	private Vector3 initialGravity;
	IEnumerator SlowMoRoutine()
	{
		initialGravity = Physics.gravity;
		Debug.Log ("start slowmo routine.");
		float slowSpeed = .3f;
		float lerpTime = .3f;
		float slowTime = 2f;
		bool done = false;

		if (enterSlowMoSound) {
			ongoingSound = Instantiate (soundPrefab, transform.position, Quaternion.identity).GetComponent<DestroyAfterSound> ();
			ongoingSound.AssignSound (enterSlowMoSound);
			ongoingSound.BecomeImmuneToTime ();
		}

		while (!done) {

			Time.timeScale = Mathf.MoveTowards (Time.timeScale, slowSpeed, Time.deltaTime * Mathf.Abs (1 - slowSpeed) / lerpTime);
			Physics.gravity = Vector3.Lerp(Vector3.zero,initialGravity,(Time.timeScale-slowTime)/(1f-slowSpeed));
			if (Time.timeScale == slowSpeed) {
				done = true;
			}
			yield return null;
		}
		
		Physics.gravity = Vector3.zero;
		Debug.Log ("full slow.");
		Time.timeScale = slowSpeed;
		yield return new WaitForSeconds (slowTime);
		Debug.Log ("get back to full speed.");

		if (enterSlowMoSound) {
			ongoingSound = Instantiate (soundPrefab, transform.position, Quaternion.identity).GetComponent<DestroyAfterSound> ();
			ongoingSound.AssignSound (exitSlowMoSound);
			ongoingSound.BecomeImmuneToTime ();
		}

		while (done) {

			Time.timeScale = Mathf.MoveTowards (Time.timeScale, 1f, Time.deltaTime * Mathf.Abs (1 - slowSpeed) / lerpTime);
			Physics.gravity = Vector3.Lerp(Vector3.zero,initialGravity,(Time.timeScale-slowTime)/(1f-slowSpeed));

			if (Time.timeScale == 1f) {
				done = false;
			}
			yield return null;
		}
		Debug.Log ("back to full speed");
		ongoingSlowMoHolder = null;
		Physics.gravity = initialGravity;
	}
    */
    /*
	public void InterruptSlowMo()
	{
		Physics.gravity = Vector3.up*-9.8f;

		if (ongoingSound) {
			ongoingSound.Interrupt ();
		}
		if (ongoingSlowMo != null) {
			StopCoroutine (ongoingSlowMo);
			ongoingSlowMoHolder = null;
		}
	}
    */
}
