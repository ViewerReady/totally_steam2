﻿using UnityEngine;
using System.Collections;

public class HoopsGenerator : MonoBehaviour {

    public GameObject hoopPrefab;
    public Transform[] hoopPositions;
    public float minHoopSpeed = .5f;
    public float maxHoopSpeed = 3f;
    //private Vector3[] positions = new Vector3[8];
    public float delay;

	void Start () 
    {
        //starting 8 positions
        /*
        positions[0] = new Vector3  (8.65f, 4.49f, 10.00f);
        positions[1] = new Vector3 (12.87f, 4.22f, 30.10f);
        positions[2] = new Vector3  (13.60f, 9.50f, 36.00f);
        positions[3] = new Vector3 (3.38f, 5.00f, 25.05f);
        positions[4] = new Vector3 (-5.13f, 3.48f, 30.75f);
        positions[5] = new Vector3 (-11.8f, 6.50f, 9.87f);
        positions[6] = new Vector3(-19.6f, 6.00f, 29.11f);
        positions[7] = new Vector3(-13f, 9f, 36f);
        */
        //additional positions 
        //positions[8] = new Vector3(4f, 15f, 38f);
        //positions[9] = new Vector3(-28f, 9f, 42f);
        //positions[10] = new Vector3();
        StartCoroutine(GenerateHoops());
	}
	
	IEnumerator GenerateHoops()
    {
        //GameObject hoop = hoopPrefabs[Random.Range(0, hoopPrefabs.Length)];
        Quaternion spawnRotation = Quaternion.identity;

        for (int i = 0; i < hoopPositions.Length; i++ )
        {
            OnCollisionHoop temp = (Instantiate(hoopPrefab, hoopPositions[i].position, spawnRotation) as GameObject).GetComponent<OnCollisionHoop>();
            if(temp)
            {
                temp.SetRandomRotationSpeed(minHoopSpeed, maxHoopSpeed);
            }

            yield return new WaitForSeconds(delay);
        }
        yield return null;
            
    }
	
}
