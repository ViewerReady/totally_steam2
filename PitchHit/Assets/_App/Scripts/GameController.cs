﻿using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
#if STEAM_VR
using Valve.VR;
using Valve.VR.InteractionSystem;
#endif

public class GameController : MonoBehaviour
{
    //public GameObject OVRManagerPrefab;
    public enum FullBatType { None, HTCRacquet, TrackerAtBase, TrackerAtTip };
    public static GameController instance;
    public static bool mixedRealityMode = false;
    public static bool fullBatterMode = false;
    public static bool appearingMenuMode = false;
    public static bool canTurnAround = true;
    public static FullBatType batType = FullBatType.HTCRacquet;


    public GameObject fullBatterMenu;
    public bool alwaysStartWithMusic;
    public GameObject[] disappearables;
    public static bool disappearNow;


    public static bool tutorialFirst = false;
    public static bool musicOn = true;
    public float customMusicVolume = -1f;

    private static float musicVolume = .5f;
    //public TutorialController tutorial;
    //private TimeController timeCon;
    public bool moonGravity = false;
    public float collisionOffset = .02f;
    public int levelNumber = 1;

    public RealButton musicButton;
    //public MenuButton musicMenuButton;
    //public roomAdjuster preGameRoom;
    public UnityEvent OnLevelBegin;
    public bool beginLevelAutomatically;
    public static bool levelIsBegun;
    public static bool isTrueOculus;
    public static bool isOculusInSteam;
    public static float oculusInSteamHandMovementMultipier = 2f;
    public static bool isAtBat = true;
    public static bool isMenuPointing;
    public static bool isPitchingMode;
    public bool shouldStartAtBat = true;
    public bool shouldStartMenuPointing = false;
#if STEAM_VR
    public Player thePlayer;
#endif

    public Text debugText;
    private AudioSource music;

    // Use this for initialization
    void Awake()
    {
        //fullBatterMode = true;
        instance = this;

        if(!appearingMenuMode || fullBatterMode)
        {
            if(fullBatterMenu)
            {
                fullBatterMenu.gameObject.SetActive(true);
            }
        }
        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);

        Physics.defaultContactOffset = collisionOffset;
        //music = GetComponent<AudioSource>();

        //if (alwaysStartWithMusic)
        //{
        //    SetMusicActive(true);
        //}

        /*if (music)
        {
            //Debug.Log("update the music buttttton");
            SetMusicActive(PlayerPrefs.GetFloat("MUSIC", 1f) != 0);
            if (musicButton)
            {
                musicButton.SetActivation(musicOn);
                //if (musicOn)
                //{
                    //musicButton.RefreshColor();
                //}
            }
            
        }*/

#if STEAM_VR
        StartCoroutine(VRSetUpRoutine());
#endif
        //StartCoroutine(CheckForOculusInSteamRoutine());

        isAtBat = shouldStartAtBat;
        isMenuPointing = shouldStartMenuPointing;
        if (isMenuPointing)
        {
            //hitThings.instance.pointer.enabled = true;
        }

        if (moonGravity)
        {
            Physics.gravity = new Vector3(0, -1.622f, 0);
        }
        else
        {
            Physics.gravity = new Vector3(0, -9.81f, 0);
        }



        //tutorial = GetComponent<TutorialController>();
        //stats = GetComponent<StatsController>();
        //if (!timeCon)
        {
        //    timeCon = GetComponent<TimeController>();
        }

        if(beginLevelAutomatically)
        {
            BeginTheGame();
        }
        else
        {
            levelIsBegun = false;
        }

        /*
        if (tutorial)
        {
            if (tutorialFirst)
            {
                tutorial.enabled = true;
            }
            else
            {
                BeginTheGame();
            }
        }
        else
        {
            BeginTheGame();
        }
        */

        foreach (GameObject g in disappearables)
        {
            //g.SetActive(!disappearNow);
        }
    }

    void Update()
    {
        if (debugText)
        {
            debugText.text = "fixedDeltaTime: " + Time.fixedUnscaledDeltaTime;
        }
    }

    /*
    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            ScoreboardController.instance.AddScore(15);
        }
    }
    */
    /*
    private CustomHand[] customHands;
    void Update()
    {
        if(Input.GetKeyDown("m"))
        {
            mixedRealityMode = !mixedRealityMode;
            if(customHands == null)
            {
                customHands = FindObjectsOfType<CustomHand>();
            }
            if(customHands !=null)
            {
                foreach(CustomHand c in customHands)
                {
                    c.showRenderModel = c.showRenderModel;
                }
            }

            HatManager.instance.RefreshHatStatus();
        }

        if(Input.GetKeyDown("n"))
        {
            disappearNow = !disappearNow;
            foreach (GameObject g in disappearables)
            {
                if (g)
                {
                    g.SetActive(!disappearNow);
                }
            }
        }
    }
    */
#if STEAM_VR
    IEnumerator CheckForOculusInSteamRoutine()
    {
        //This is incompatible with SteamVR 2.0 and we weren't using it anyway so yeah I commented it out. - Bradley.
        ///////////////////////////////////////////////////////////////////////////////
        /*
        float totalTime = 0f;
        float timeLimit = 3f;
        while (SteamVR.instance == null)
        {
            if (totalTime > timeLimit)
            {
                yield break;
            }
            totalTime += Time.deltaTime;
            yield return null;
        }

        while (SteamVR.instance.hmd_TrackingSystemName == null || SteamVR.instance.hmd_TrackingSystemName == "")
        {
            if (totalTime > timeLimit)
            {
                yield break;
            }
            totalTime += Time.deltaTime;
            yield return null;
        }

        if (SteamVR.instance.hmd_TrackingSystemName.ToLower().Contains("oculus"))
        {
            Debug.Log("oculus affirmative");
            isOculusInSteam = true;


            List<int> angles = new List<int>();
            Debug.Log("about to gather rough camera angles. " + OpenVR.k_unMaxTrackedDeviceCount);
            for (int i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++)
            {
                //if (i == relativeTo || system.GetTrackedDeviceClass((uint)i) != deviceClass)
                //    continue;

                if (OpenVR.System.GetTrackedDeviceClass((uint)i) == ETrackedDeviceClass.TrackingReference)
                {
                    Debug.Log("found a tracking reference.");
                    SteamVR_Controller.Device device = SteamVR_Controller.Input(i);
                    //Debug.DrawRay(thePlayer.transform.TransformPoint(device.transform.pos), Vector3.up, Color.blue, 10f);
                    //float tempAngle = Vector3.Angle(Vector3.forward, (new Vector3(device.transform.pos.x, 0f,device.transform.pos.z)));
                    float tempAngle = AngleAroundAxis(Vector3.forward, device.transform.pos, Vector3.up);
                    for (int a = -90; a <= 180; a += 90)
                    {
                        if (Mathf.Abs(a - tempAngle) <= 45)
                        {
                            Debug.Log("gathering " + a);
                            angles.Add(a);
                            break;
                        }
                    }
                }
            }

            if (angles.Count > 0)
            {
                bool anglesAreDifferent = false;
                int firstAngle = angles[0];
                for (int a = 1; a < angles.Count; a++)
                {
                    if (angles[a] != firstAngle)
                    {
                        anglesAreDifferent = true;
                        break;
                    }
                }

                if (!anglesAreDifferent && firstAngle != 0)
                {
                    Debug.Log("apply the camera angle rotation! ");
                    if (thePlayer == null)
                    {
                        thePlayer = FindObjectOfType<Player>();
                    }

                    if (thePlayer)
                    {
                        thePlayer.transform.Rotate(Vector3.up, -firstAngle, Space.World);
                    }
                }

            }
            //Valve.VR.OpenVR.k_unTrackedDeviceIndex_Hmd

        }
        */
        yield return null;
    }

    IEnumerator VRSetUpRoutine()
    {

        Debug.Log("loaded VR ? " + UnityEngine.XR.XRSettings.loadedDeviceName);

        while (UnityEngine.XR.XRSettings.loadedDeviceName == null || UnityEngine.XR.XRSettings.loadedDeviceName == "")
        {
            //Debug.Log("there's no vr");
            UnityEngine.XR.XRSettings.LoadDeviceByName("OpenVR");

            yield return null;
        }

        Debug.Log("loaded VR!!! " + UnityEngine.XR.XRSettings.loadedDeviceName);

        while (!UnityEngine.XR.XRDevice.isPresent)
        {
            Debug.Log("waiting for vr device " + UnityEngine.XR.XRDevice.model);
            yield return null;
        }
        Debug.Log("found vr device " + UnityEngine.XR.XRDevice.model);

        UnityEngine.XR.XRSettings.enabled = true;

        /*
        if(VRSettings.loadedDeviceName.ToLower().Contains("oculus"))
        {
            if (SteamVR.instance != null)
            {
                Debug.Log("OCULUS IS IN STEAM");
                isOculusInSteam = true;
            }
            else
            {
                Debug.Log("TRUE OCULUS");
                isTrueOculus = true;
            }
        }
        else
        {
            if (SteamVR.instance != null)
            {
                Debug.Log("OCULUS IS IN STEAM");
                isOculusInSteam = true;
            }
            else
            {
                Debug.Log("TRUE OCULUS");
                isTrueOculus = true;
            }
        }
        */

        if (SteamVR.instance != null)
        {
            //Debug.Log("steam is there with                      " + SteamVR.instance.hmd_TrackingSystemName);
            if (SteamVR.instance.hmd_TrackingSystemName.ToLower().Contains("oculus"))
            {
                //if (OVRManager.)
                //{
                //}
                //else
                //{
                Debug.Log("oculus affirmative");
                isOculusInSteam = true;
                //}
            }
        }
        else
        {
            Debug.Log("VRDevice model is " + UnityEngine.XR.XRDevice.model);

            if (OVRManager.isHmdPresent)
            {
                Debug.Log("OVR HMD IS PRESENT --------------------     ");
            }

            if (UnityEngine.XR.XRDevice.model.ToLower().Contains("oculus"))
            {
                //Instantiate(OVRManagerPrefab, transform.position, transform.rotation, transform);
                isTrueOculus = true;
                Debug.Log("NO STEAM VR IS TRUE OCULUS");
            }
            else
            {
                Debug.Log("oculus negative");
            }
        }
    }
#endif

    

    public void SetOnlyFullBatterAvailable(bool onlyBat)
    {
        if(fullBatterMenu)
        {
            fullBatterMenu.SetActive(onlyBat);
        }
    }

    public void SetBatType(int type)
    {
        switch (type)
        {
            case 0:
                batType = FullBatType.None;
                break;
            case 1:
                batType = FullBatType.HTCRacquet;
                break;
            case 2:
                batType = FullBatType.TrackerAtBase;
                break;
            case 3:
                batType = FullBatType.TrackerAtTip;
                break;
        }
    }

    public void SetMusicActive(bool a)
    {
        Debug.Log("set music active " + a);
        musicOn = a;

        if (musicOn)
        {
            Debug.Log("music on.");
            music.Play();
            if (customMusicVolume<0)
            {
                music.volume = musicVolume;
            }
            else
            {
                music.volume = customMusicVolume;
            }
        }
        else
        {
            Debug.Log("stop the music.");
            music.Pause();
            music.volume = 0f;
        }

        PlayerPrefs.SetFloat("MUSIC", a ? 1f : 0f);
    }

    public void BeginTheGame()
    {
        
        //if (tutorial)
        //{
        //    tutorial.enabled = false;
        //}

        //I'm not sure why I ever used this routine. let's simplify.
        levelIsBegun = true;
        OnLevelBegin.Invoke();

        //StartCoroutine(BeginGameRoutine());

    }

    IEnumerator BeginGameRoutine()
    {
        //yield return new WaitForSeconds(.5f);
		if (!beginLevelAutomatically) {
			yield return null;
		}
		//if (ScoreboardController.instance)
        //{
        //    ScoreboardController.instance.EnableScoring();
        //}

        //if (music)
        //{
        //music.Stop();
        //}

        

        levelIsBegun = true;

        OnLevelBegin.Invoke();
		/*
        if (timeCon)
        {
            yield return null;
            while(ballController.activeBalls==null || ballController.activeBalls.Count==0)
            {
                yield return null;
            }
            timeCon.ResetTimer();
        }
*/
        
    }

    //Antialiasing function #michaeltaylor
    public void ChangeAntiAliasing(int AntiAliasingIndex)
    {

        switch (AntiAliasingIndex)
        {

            case 1:
                {
                    QualitySettings.antiAliasing = 0;
                    break;
                }
            case 2:
                {
                    QualitySettings.antiAliasing = 2;
                    break;
                }
            case 3:
                {
                    QualitySettings.antiAliasing = 4;
                    break;
                }
            default:
                {
                    QualitySettings.antiAliasing = 8;
                    break;
                }
        }
    }

    // Update is called once per frame
    //void Update () {
    //Debug.Log("timesclae "+Time.timeScale);
    //Debug.Log("gravity "+Physics.gravity);
    /*

    //Short Cuts #michaeltaylor
    if (Input.GetKeyDown(KeyCode.Alpha6))
    {
        ChangeAntiAliasing(1);
    }

    if (Input.GetKeyDown(KeyCode.Alpha7))
    {
        ChangeAntiAliasing(2);
    }

    if (Input.GetKeyDown(KeyCode.Alpha8))
    {
        ChangeAntiAliasing(3);
    }

    if (Input.GetKeyDown(KeyCode.Alpha9))
    {
        ChangeAntiAliasing(0);
    }
    */
    //}

    // The angle between dirA and dirB around axis
    public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
    {
        // Project A and B onto the plane orthogonal target axis
        dirA = dirA - Vector3.Project(dirA, axis);
        dirB = dirB - Vector3.Project(dirB, axis);

        // Find (positive) angle between A and B
        float angle = Vector3.Angle(dirA, dirB);

        // Return angle multiplied with 1 or -1
        return angle * (Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) < 0 ? -1 : 1);
    }

    public static void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (null == obj)
        {
            return;
        }

        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }

}
