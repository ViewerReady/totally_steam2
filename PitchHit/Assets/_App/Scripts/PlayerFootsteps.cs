﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFootsteps : MonoBehaviour {

    float timer = 0f;
    Terrain terrain;
    Vector3 lastPosition;

    void Awake()
    {
        lastPosition = transform.position;
    }

	// Update is called once per frame
	void Update ()
    {
		if(terrain != null)
        {
            float y = ((80f / 10f) * ((transform.position - lastPosition).magnitude));
            timer += (Time.deltaTime * y);
            lastPosition = transform.position;
            //Debug.Log("MAIN TERRAIN TEXTURE: " + TerrainSampler.GetMainTexture(terrain, transform.position));
            //timer += Time.deltaTime;
            if(timer > 0.25f)
            {
                int terrainType = TerrainSampler.GetMainTexture(terrain, transform.position);
                if(terrainType == 0) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("footsteps_dirt", transform.position);
                else if(terrainType == 1) DarkTonic.MasterAudio.MasterAudio.PlaySound3DAtVector3("footsteps_grass", transform.position);
                timer = 0f;
            }
        }
        else
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
            {
                if (hit.collider.gameObject.CompareTag("ground"))
                {
                    if (hit.collider.gameObject.GetComponent<Terrain>() != null)
                    {
                        terrain = hit.collider.gameObject.GetComponent<Terrain>();
                    }
                }
            }
        }
	}
}
