﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;

[CustomPropertyDrawer(typeof(CameraShotDictionary))]
public class MyDictionaryDrawer3 : DictionaryDrawer<CameraShot, float> { }

#endif

[Serializable] public class CameraShotDictionary : SerializableDictionary<CameraShot, float> { }



[CreateAssetMenu(fileName = "CamProfile", menuName = "Spectator/CinematicProfile", order = 1)]
public class CinematicProfile : ScriptableObject {
    public float delay = 0;

    public CameraShotDictionary profile;

    public bool HasShot(CameraShot shot)
    {
        return profile.ContainsKey(shot);
    }

    public float ShotWeight(CameraShot shot)
    {
        float weight = 0;
        profile.TryGetValue(shot, out weight);

        return weight;
    }
}

