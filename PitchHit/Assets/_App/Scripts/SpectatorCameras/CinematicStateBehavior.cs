﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicStateBehavior : StateMachineBehaviour {

    public CinematicProfile profile;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SpectatorCameraController.LoadCinematicProfile_Static(profile);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }


}
