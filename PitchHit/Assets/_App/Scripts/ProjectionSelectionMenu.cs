﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectionSelectionMenu : MonoBehaviour {

    [SerializeField] GameObject onButton;
    [SerializeField] GameObject offButton;

    void Start()
    {
        if (HumanPosessionManager.locomotionProjection_saved)
        {
            offButton.SetActive(false);
            onButton.SetActive(true);
            HumanPosessionManager.SetLocomtionProjection(true);
        }
        else
        {
            offButton.SetActive(true);
            onButton.SetActive(false);
            HumanPosessionManager.SetLocomtionProjection(false);
        }
    }

    public void TurnProjectionOn()
    {
        offButton.SetActive(false);
        onButton.SetActive(true);
        HumanPosessionManager.SetLocomtionProjection(true);
    }

    public void TurnProjectionOff()
    {
        offButton.SetActive(true);
        onButton.SetActive(false);
        HumanPosessionManager.SetLocomtionProjection(false);
    }
}
