﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Valve.VR.InteractionSystem;
using CloudFine.ThrowLab;
using System;

[RequireComponent(typeof(hittable))]
public class CustomThrowable : MonoBehaviour {

    private CustomHand currentAttachedCustomHand;
    private ThrowHandle throwHandle;
    private hittable hitable;
    public static Action<hittable> OnBallThrown;


    private void Awake()
    {
        throwHandle = GetComponent<ThrowHandle>();
        if (throwHandle)
        {
            throwHandle.onFinalTrajectory += ThrowEvent;
        }
        hitable = GetComponent<hittable>();
    }

#if RIFT
    public void OnGrabbed(OVRGrabber grabber)
    {
        Debug.Log("OnGrabbed", this.gameObject);
        currentAttachedCustomHand = grabber.GetComponentInParent<CustomHand>();
    }

    public void OnUngrabbed()
    {
        currentAttachedCustomHand = null;
    }

#elif STEAM_VR
    public void OnAttachedToHand(Hand hand)
    {
        currentAttachedCustomHand = hand.GetComponent<CustomHand>();
    }


    public void OnDetachedFromHand(Hand hand)
    {
        currentAttachedCustomHand = null;
    }
#endif

    public CustomHand GetAttachedHand()
    {
        return currentAttachedCustomHand;
    }

    private void ThrowEvent(Vector3 velocity)
    {
        if (OnBallThrown != null) {
            OnBallThrown.Invoke(hitable);
        }
    }
}
