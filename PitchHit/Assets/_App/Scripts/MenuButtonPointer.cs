﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class MenuButtonPointer : ButtonPointer {
    RealButton previousButton;
    private float timeRequiredForFullBatPointing = 1.25f;
    private Transform camTransform;
    // Use this for initialization
    new void Start () {
        base.Start();
        //contactGraphic.parent = null;
        camTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (RealButton.buttonsOnAlert != 0)
        {
            NoHit();
            return;
        }
        CustomHand hand = null;
        bool ableToPoint = false;

        if(GameController.fullBatterMode)
        {
            if (hitThings.instance && hitThings.instance.matchingMixedRealityBat)
            {
                transform.position = hitThings.instance.transform.position;
                transform.rotation = hitThings.instance.transform.transform.rotation;
                ableToPoint = true;
            }
            else
            {
                hand = HandManager.GetAvailableDominantHand();
                if(hand)
                {
                    //in a hand.
                    bool controllerHoldingBat = hitThings.instance != null && (hitThings.GetCurrentGripType() != hitThings.gripType.always) && hitThings.instance.attached && hitThings.instance.attachedCustomHand == hand;
#if STEAM_VR
                    bool holdingSomething = hand.actualHand.currentAttachedObject != null;
#elif RIFT
                    bool holdingSomething = hand.GetComponentInChildren<OVRGrabber>().grabbedObject != null;
#else
                    bool holdingSomething = false;
#endif

                    if (! controllerHoldingBat && !holdingSomething)
                    {
                        transform.position = hand.transform.position;
                        transform.rotation = hand.transform.rotation;
                        ableToPoint = true;
                    }
                }
            }
        }
        else
        {
            hand = HandManager.GetAvailableDominantHand();
            if (hand)
            {
                //in a hand.
                bool controllerHoldingBat = hitThings.instance != null && (hitThings.GetCurrentGripType() != hitThings.gripType.always) && hitThings.instance.attached && hitThings.instance.attachedCustomHand == hand;
#if STEAM_VR
                bool holdingSomething = hand.actualHand.currentAttachedObject && hand.actualHand.currentAttachedObject.GetComponent<Interactable>();
#elif RIFT
                    bool holdingSomething = hand.GetComponentInChildren<OVRGrabber>().grabbedObject != null;
#else
                bool holdingSomething = false;
#endif

                if (!controllerHoldingBat && !holdingSomething)
                {
                    transform.position = hand.transform.position;
                    transform.rotation = hand.transform.rotation;
                    ableToPoint = true;
                }
            }
            else
            {
                if (hitThings.instance && hitThings.instance.matchingMixedRealityBat)
                {
                    transform.position = hitThings.instance.transform.position;
                    transform.rotation = hitThings.instance.transform.transform.rotation;
                    ableToPoint = true;
                }
            }
        }
        RaycastHit hit;
        if (ableToPoint && Physics.Raycast(transform.position, transform.forward, out hit, 3f * transform.lossyScale.z, buttonLayer.value | menuLayer.value, QueryTriggerInteraction.Collide) && hit.rigidbody)
        {
            if (hit.rigidbody.GetComponent<DirectorRealButton>() != null) //We don't want to register DirectorRealButtons
            {
                NoHit();
                if (hand)
                {
                    hand.StopPointing();
                }

                if (previousButton)
                {
                    previousButton.OnNotTouched();
                    previousButton.RefreshColor();
                    previousButton = null;
                }
                return;
            }

            if (hand)
            {
                hand.StartPointing();
            }
            MoveContactGraphic(hit, hit.rigidbody.transform);
            RealButton foundButton = hit.rigidbody.GetComponent<RealButton>();
            
            if (foundButton)
            {
                if (previousButton && foundButton != previousButton)
                {
                    timeOnSameButton = 0f;
                    previousButton.OnNotTouched();
                    previousButton.RefreshColor();
                }
                previousButton = foundButton;

                if (hand)
                {
                    bool handHoldingSomething = false;
#if RIFT
                    OVRGrabbable somethingHeld = hand.GetComponentInChildren<OVRGrabber>().grabbedObject;
#elif GEAR_VR
                    GameObject somethingHeld = null;
#elif STEAM_VR
                    GameObject somethingHeld = hand.actualHand.currentAttachedObject;
                    if (somethingHeld && somethingHeld.GetComponent<Interactable>() == null)
                    {
                        somethingHeld = null;
                    }
#endif

#if RIFT || GEAR_VR || STEAM_VR
                    handHoldingSomething = (somethingHeld != null);
#endif

                    if (hand.GetUsing())// && !handHoldingSomething)
                    {
                        foundButton.PointerPress();
                        //foundButton.Click();
                    }
                    else
                    {
                        //foundButton.Hover();
                        foundButton.PointerHover();
                    }
                }
                else
                {
                    if (Vector3.Angle(camTransform.forward, foundButton.transform.position - camTransform.position) < 45f)
                    {
                        if (timeOnSameButton > timeRequiredForFullBatPointing)
                        {
                            foundButton.PointerPress();
                        }
                        else
                        {
                            timeOnSameButton += Time.deltaTime;
                            foundButton.PointerHover();
                        }
                    }
                    else
                    {
                        timeOnSameButton = 0f;
                    }
                }
                
            }
            else
            {
                if (previousButton)
                {
                    timeOnSameButton = 0f;
                    previousButton.OnNotTouched();
                    previousButton.RefreshColor();
                    previousButton = null;
                }
            }
        }
        else
        {
            NoHit();
            if (hand)
            {
                hand.StopPointing();
            }

            if (previousButton)
            {
                previousButton.OnNotTouched();
                previousButton.RefreshColor();
                previousButton = null;
            }
        }
        
    }
}
