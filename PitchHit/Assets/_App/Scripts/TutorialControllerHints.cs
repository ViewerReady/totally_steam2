﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialControllerHints : MonoBehaviour {

    public GameObject trackpad;
    public GameObject menu;
    public GameObject grips;
    public GameObject trigger;

    public void ShowHideTrackpad(bool show)
    {
        trackpad.SetActive(show);
    }

    public void ShowHideMenu(bool show)
    {
        menu.SetActive(show);
    }

    public void ShowHideGrips(bool show)
    {
        grips.SetActive(show);
    }

    public void ShowHideTrigger(bool show)
    {
        trigger.SetActive(show);
    }

    public void HideAll()
    {
        menu.SetActive(false);
        trigger.SetActive(false);
        trackpad.SetActive(false);
        grips.SetActive(false);
    }
}
