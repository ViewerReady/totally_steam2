﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonitorViewToggle : MonoBehaviour
{
    [SerializeField] Camera renderTextureCam;
    [SerializeField] Cinemachine.CinemachineBrain cinemachineBrain;
    public bool defaultOn = true;


    private void Start()
    {
        if (defaultOn)
        {
            MonitorViewOn();
        }
        else
        {
            MonitorViewOff();
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            ToggleMonitorView();
        }
    }

    public void ToggleMonitorView()
    {
        renderTextureCam.gameObject.SetActive(!renderTextureCam.gameObject.activeSelf);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = !cinemachineBrain.GetComponent<Camera>().enabled;
        }
    }

    public void MonitorViewOn()
    {
        renderTextureCam.gameObject.SetActive(true);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = true;
        }
    }

    public void MonitorViewOff()
    {
        renderTextureCam.gameObject.SetActive(false);
        if (cinemachineBrain != null)
        {
            cinemachineBrain.GetComponent<Camera>().enabled = false;
        }
    }
}
