﻿using UnityEngine;
using System.Collections;

public class BatButtonPointer : ButtonPointer {
    public Transform buttonPressingTrigger;
    public hitThings bat;
	// Use this for initialization
	new void Start () {
        base.Start();
        buttonPressingTrigger.SetParent(transform);
	}

    void OnEnable()
    {
        buttonPressingTrigger.gameObject.SetActive(true);
    }

    void OnDisable()
    {
        buttonPressingTrigger.gameObject.SetActive(false);
    }

	// Update is called once per frame
	void Update ()
    {
#if GEAR_VR
        bat.onGrab();
#endif
        if (bat.attached)
        {
            buttonPressingTrigger.gameObject.SetActive(true);
            buttonPressingTrigger.LookAt(transform.position);
            RaycastHit hit;
            Debug.DrawRay(transform.position, -bat.gripTransform.right*3f, Color.red);
            if (Physics.Raycast(transform.position, -bat.gripTransform.right, out hit, 3f, buttonLayer.value, QueryTriggerInteraction.Collide))
            {
                Debug.DrawRay(hit.point, hit.normal, Color.blue);
                RealButton foundButton = hit.rigidbody.GetComponent<RealButton>();
                if (foundButton && !foundButton.keepOculusSimple)
                {
                    if (foundButton.onAlert)
                    {
                        HandManager.VibrateController(0,.1f, 50, .5f, bat.attachedCustomHand);
                    }

                    buttonPressingTrigger.position = hit.point;
                    MoveContactGraphic(hit, foundButton.transform);
                    isOnButton = true;
                    //buttonPressingTrigger.rotation = contactGraphic.rotation;
                    currentButton = foundButton;
                }
                else
                {
                    NoHit();
                }
            }
            else
            {
                NoHit();
            }

        }
        else
        {
            NoHit();
        }
        
    }

    void LateUpdate()
    {
        if (isOnButton && currentButton && Vector3.Angle(contactGraphic.forward, currentButton.transform.forward) > 175f && currentButton.GetType() == typeof(RealButton))
        {
            RealButton realButton = (RealButton)currentButton;
            contactGraphic.position += (contactGraphic.forward * realButton.GetPressedDepth());
        }
    }

    protected override void NoHit()
    {
        base.NoHit();
        if (buttonPressingTrigger)
        {
            buttonPressingTrigger.localPosition = Vector3.zero;
            buttonPressingTrigger.gameObject.SetActive(false);
        }


    }
}
