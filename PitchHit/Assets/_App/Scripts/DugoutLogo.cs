﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class DugoutLogo : MonoBehaviour
{
    [SerializeField] bool isHomeTeam;

    void Start ()
    {
        if(FullGameDirector.Instance != null)
        GetComponent<MeshRenderer>().material.mainTexture = (isHomeTeam ? FullGameDirector.homeTeam.teamLogoTex : FullGameDirector.awayTeam.teamLogoTex);
	}
}