﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuToggleSetting : MonoBehaviour {

    [SerializeField] Button buttonOne;
    [SerializeField] Button buttonTwo;
    [SerializeField] string settingKey;

    void OnEnable()
    {
        if (PlayerPrefs.HasKey(settingKey))
        {
            if (PlayerPrefs.GetInt(settingKey) == 0) buttonOne.onClick.Invoke();
            else buttonTwo.onClick.Invoke();
        }
    }

    public void SaveButtonChoice(int choice)
    {
        PlayerPrefs.SetInt(settingKey, choice); 
    }
}
