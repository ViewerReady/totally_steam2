﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCameraController : MonoBehaviour {

    MinimapCamera minimapCam;
    Vector3 targetPosition;
    Quaternion targetRotation;
    Vector3 velocity;
    float rotationalVelocity;

    // Update is called once per frame
    void Update ()
    {
		if(MinimapCamera.Instance != null)
        {
            targetPosition = MinimapCamera.Instance.GetRelativePosition();
            //targetRotation = MinimapCamera.Instance.GetRelativeRotation();
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, 0.1f);
            //float percentage = (1f - Vector3.Distance(transform.position, targetPosition));
            //Debug.Log("PERCENTAGE: " + percentage);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, percentage);
            //transform.eulerAngles = targetRotation;
            var target_rot = MinimapCamera.Instance.GetRelativeRotation();
            var delta = Quaternion.Angle(transform.rotation, target_rot);
            if (delta > 0.0f)
            {
                var t = Mathf.SmoothDampAngle(delta, 0.0f, ref rotationalVelocity, 0.1f);
                t = 1.0f - t / delta;
                transform.rotation = Quaternion.Slerp(transform.rotation, target_rot, t);
            }
        }
	}
}
