﻿using UnityEngine;
//using OVRTouchSample;


public class NewCustomTouchAnimator : MonoBehaviour
{
    public bool demoHighlights;

    [SerializeField]
    Material lightUpMaterial;
    [SerializeField]
    private OVRInput.Controller m_Controller;

    Material baseMaterial;
    private Animator m_animator = null;
    private bool bActive = false;
    private string prefix = "rctrl";
 
    public GameObject Abutton, Bbutton, Mbutton, Trigger, GripTrigger, Joystick;
    private Renderer AbuttonRNDR, BbuttonRNDR, MbuttonRNDR, TriggerRNDR, GripTriggerRNDR, JoystickRNDR;

    private void Awake()
    {
        if (m_Controller == OVRInput.Controller.LTouch)
        {
            prefix = "lctrl";
            // ACTALLY X AND Y !
            if (!Abutton)
            {
                Abutton = GameObject.Find(prefix + ":x_button_PLY");
            }
            if (!Bbutton)
            {
                Bbutton = GameObject.Find(prefix + ":y_button_PLY");
            }
        }
        else
        {
            if (!Abutton)
            {
                Abutton = GameObject.Find(prefix + ":a_button_PLY");
            }
            if (Bbutton)
            {
                Bbutton = GameObject.Find(prefix + ":b_button_PLY");
            }
        }

        if (!Trigger)
        {
            Trigger = GameObject.Find(prefix + ":main_trigger_PLY");
        }
        if (!GripTrigger)
        {
            GripTrigger = GameObject.Find(prefix + ":side_trigger_PLY");
        }
        if (!Joystick)
        {
            Joystick = GameObject.Find(prefix + ":thumbstick_ball_PLY");
        }
        Mbutton = GameObject.Find(prefix+":o_button_PLY");
        AbuttonRNDR = Abutton.GetComponent<Renderer>();
        BbuttonRNDR = Bbutton.GetComponent<Renderer>();
        MbuttonRNDR = Mbutton.GetComponent<Renderer>();
        TriggerRNDR = Trigger.GetComponent<Renderer>();
        GripTriggerRNDR = GripTrigger.GetComponent<Renderer>();
        JoystickRNDR = Joystick.GetComponent<Renderer>();

        baseMaterial = Abutton.GetComponent<Renderer>().material;
        m_animator = gameObject.GetComponentInChildren<Animator>();

    }

    private void Update()
    {
        /*
                    m_animator.SetFloat("Button 1", OVRInput.Get(OVRInput.Button.One, m_controller) ? 1.0f : 0.0f);
            m_animator.SetFloat("Button 2", OVRInput.Get(OVRInput.Button.Two, m_controller) ? 1.0f : 0.0f);
            m_animator.SetFloat("Joy X", OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, m_controller).x);
            m_animator.SetFloat("Joy Y", OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, m_controller).y);
            m_animator.SetFloat("Grip", OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller));
            m_animator.SetFloat("Trigger", OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_controller));
            */

        if (demoHighlights)
        {
            bool bone = OVRInput.Get(OVRInput.Button.One, m_Controller);
            SetButtonOneHighlight(OVRInput.Get(OVRInput.Button.One, m_Controller));

            SetButtonTwoHighlight(OVRInput.Get(OVRInput.Button.Two, m_Controller));

            SetButtonGripHighlight(OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_Controller) > 0);

            SetButtonTriggerHighlight(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_Controller) > 0);

            SetJosystickHighlight(OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, m_Controller).x != 0 || OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, m_Controller).y != 0);
        }
        // print(m_Controller.Button1);

    }

    public void SetButtonOneHighlight(bool on)
    {
        if(AbuttonRNDR==null)
        {
            return;
        }

        if (on)
        {
            AbuttonRNDR.material = lightUpMaterial;
        }
        else
        {
            AbuttonRNDR.material = baseMaterial;
        }
    }

    public void SetButtonTwoHighlight(bool on)
    {
        if (on)
        {
            BbuttonRNDR.material = lightUpMaterial;
        }
        else
        {
            BbuttonRNDR.material = baseMaterial;
        }
    }

    public void SetButtonThreeHighlight(bool on)
    {
        if (on)
        {
            MbuttonRNDR.material = lightUpMaterial;
        }
        else
        {
            MbuttonRNDR.material = baseMaterial;
        }
    }

    public void SetButtonGripHighlight(bool on)
    {
        if (on)
        {
            GripTriggerRNDR.material = lightUpMaterial;
        }
        else
        {
            GripTriggerRNDR.material = baseMaterial;
        }
    }

    public void SetButtonTriggerHighlight(bool on)
    {
        if (on)
        {
            TriggerRNDR.material = lightUpMaterial;
        }
        else
        {
            TriggerRNDR.material = baseMaterial;
        }
    }

    public void SetJosystickHighlight(bool on)
    {
        if (on)
        {
            JoystickRNDR.material = lightUpMaterial;
        }
        else
        {
            JoystickRNDR.material = baseMaterial;
        }
    }
}
