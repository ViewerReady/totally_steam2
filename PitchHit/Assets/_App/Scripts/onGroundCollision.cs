﻿using UnityEngine;
using System.Collections;

public class onGroundCollision : MonoBehaviour {

	public bool pointsGiven = false;
	public int numPoints = 5;

	void OnCollisionEnter(Collision collisionInfo){
		if (collisionInfo.gameObject.tag == "ground" && !pointsGiven) {
			pointsGiven = true;
            ScoreboardController.instance.AddScore(numPoints);
		}
	}

	// Update is called once per frame
	//void Update () {
	
	//}
}
