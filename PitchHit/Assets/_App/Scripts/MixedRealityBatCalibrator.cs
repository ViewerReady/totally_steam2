﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MixedRealityBatCalibrator : MonoBehaviour {
    public Text feedback;
    public Image progressImage;
    public Renderer indicator;
    private MixedRealityBat batHolder;
    private Transform noBatCollider;

    private bool overlapping;
    private Vector3 previousHolderPosition;

    private float countdown;
    private float holdStillTime = 2f;
    private Color currentColor;
    private AudioSource sound;

	// Use this for initialization
	void Start () {
        batHolder = FindObjectOfType<MixedRealityBat>();

        if(batHolder)
        {
            noBatCollider = batHolder.noBatCollider.transform;
        }
        sound = GetComponent<AudioSource>();
	}

    void OnEnable()
    {
        //text appear basic
        feedback.text = "Place your bat here to callibrate...";
        currentColor = Color.white;

        indicator.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, .35f);
    }
	
	// Update is called once per frame
	void Update () {
        if (overlapping)
        {
            indicator.material.color = Color.yellow;
            float angleToUp = Vector3.Angle(Vector3.up, batHolder.transform.forward);
            currentColor = Color.Lerp(Color.yellow, Color.black, (angleToUp - 10f) / 45f);
            feedback.text = "Hold your bat upright.";
            //text hold it upright
            if (angleToUp < 5f)
            {
                //now hold still.
                float movement = ((batHolder.transform.position - previousHolderPosition).magnitude) / Time.deltaTime;
                feedback.text = "Now hold still...";
                if (movement < .075f)
                {
                    progressImage.enabled = true;
                    countdown -= Time.deltaTime;
                    progressImage.fillAmount = 1f-(countdown / holdStillTime);
                    currentColor = Color.Lerp(Color.yellow, Color.green, progressImage.fillAmount);
                    progressImage.color = currentColor;
                    if (countdown<0)
                    {
                        batHolder.CalibrateTipLength();
                        overlapping = false;
                        feedback.text = "Your bat is now callibrated!";
                        currentColor = Color.white;
                        progressImage.color = Color.green;
                        if(sound && sound.clip)
                        {
                            sound.Play();
                        }
                    }
                }
                else
                {
                    currentColor = Color.yellow;
                    progressImage.enabled = false;
                    countdown = holdStillTime;
                }
            }
            else
            {
                progressImage.enabled = false;
                countdown = holdStillTime;
            }
        }
        else
        {
            
            countdown = holdStillTime;
        }
        previousHolderPosition = batHolder.transform.position;

        indicator.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, .35f);
	}

    void OnTriggerEnter(Collider c)
    {
        if(c.transform == noBatCollider || (c.attachedRigidbody && c.attachedRigidbody.tag=="bat"))
        {
            overlapping = true;

            progressImage.color = Color.yellow;
            progressImage.fillAmount = 0f;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.transform == noBatCollider || (c.attachedRigidbody && c.attachedRigidbody.tag == "bat"))
        {
            currentColor = Color.white;
            indicator.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, .35f);

            progressImage.enabled = false;
            overlapping = false;
            feedback.text = "Place your bat here to callibrate...";
        }
    }
}
