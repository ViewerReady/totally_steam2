﻿using UnityEngine;
using System.Collections;

public class CatchSoundController : MonoBehaviour {
    public AudioClip[] catchSounds;
    private new AudioSource audio;
	// Use this for initialization
	void Awake () {
        //audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	//void Update () {
	
	//}
    public void PlaySound()
    {
        DarkTonic.MasterAudio.MasterAudio.PlaySound("player_catch");
        /*audio.clip = catchSounds[Random.Range(0,catchSounds.Length)];
        audio.Play();*/

    }

    public void PlaySound(float volume)
    {
        DarkTonic.MasterAudio.MasterAudio.PlaySound("player_catch", volume);
        /*audio.volume = volume;
        audio.clip = catchSounds[Random.Range(0, catchSounds.Length)];
        audio.Play();*/
        //        Debug.Log("PlaySound "+audio.volume+" "+audio.clip.name);
    }
}
