﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TVOverlay : MonoBehaviour
{

    [SerializeField] FullGameDirector gameDirector;
    [SerializeField] float textScrollSpeed = 1f;

    [SerializeField] Image ballOneIndicator;
    [SerializeField] Image ballTwoIndicator;
    [SerializeField] Image ballThreeIndicator;

    [SerializeField] Image strikeOneIndicator;
    [SerializeField] Image strikeTwoIndicator;

    [SerializeField] Image outOneIndicator;
    [SerializeField] Image outTwoIndicator;

    [SerializeField] RectTransform tickerPanel;
    [SerializeField] Text[] tickerTextObjects;
    Queue<Text> tickerObjectQueue;
    [SerializeField] GameObject tickerTextPrefab;
    [SerializeField] List<string> tickerWordsToQueue;
    Queue<string> tickerWords;
    bool readyToSpawnNewTicker;

    [SerializeField] TextMeshProUGUI teamOneName;
    [SerializeField] TextMeshProUGUI teamTwoName;
    [SerializeField] TextMeshProUGUI teamOneScore;
    [SerializeField] TextMeshProUGUI teamTwoScore;
    int teamOneRuns;
    int teamTwoRuns;

    Dictionary<OffensePlayer, RectTransform> offensePlayers;
    [SerializeField] RectTransform runnersPanel;
    [SerializeField] GameObject runnerHelmetPrefab;
    [SerializeField] float runnerMapScaleFactor;

    [SerializeField] RectTransform batterNamePanel;
    [SerializeField] TextMeshProUGUI batterNameText;

    [SerializeField] TextMeshProUGUI inningText;
    [SerializeField] RectTransform inningArrowTransform;

    Canvas overlayCanvas;
    Camera renderCamera;

    void Awake()
    {
        //I'm saving the render camera we set in the editor and re-setting it if it ever changes because
        //for some reason it keeps getting set to something else at runtime. We'll have to figure out why later. 
        //- Bradley 12-13-2019
        overlayCanvas = GetComponent<Canvas>();
        renderCamera = overlayCanvas.worldCamera;

        readyToSpawnNewTicker = true;

        if (tickerWordsToQueue == null) tickerWordsToQueue = new List<string>();
        tickerWords = new Queue<string>();
        tickerObjectQueue = new Queue<Text>(tickerTextObjects);
        teamOneRuns = 0;
        teamTwoRuns = 0;
        offensePlayers = new Dictionary<OffensePlayer, RectTransform>();
    }

    void Start()
    {
        ballOneIndicator.gameObject.SetActive(false);
        ballTwoIndicator.gameObject.SetActive(false);
        ballThreeIndicator.gameObject.SetActive(false);
        strikeTwoIndicator.gameObject.SetActive(false);
        strikeOneIndicator.gameObject.SetActive(false);
        outOneIndicator.gameObject.SetActive(false);
        outTwoIndicator.gameObject.SetActive(false);

        teamOneName.text = FullGameDirector.homeTeam.teamName.Substring(0, 3).ToUpper();
        teamTwoName.text = FullGameDirector.awayTeam.teamName.Substring(0, 3).ToUpper();
        teamOneScore.text = "0";
        teamTwoScore.text = "0";

        inningText.text = "1st";

        foreach(string s in tickerWordsToQueue)
        {
            QueueWordsForTicker(s);
        }
    }

    void OnEnable()
    {
        FullGameDirector.OnBallsChange += UpdateBallIndicators;
        FullGameDirector.OnStrikesChange += UpdateStrikeIndicators;
        FullGameDirector.OnOutsChange += UpdateOutIndicators;
        FullGameDirector.OnScoreChange += SetRuns;
        FieldMonitor.OnRegisterRunner += CreateMiniRunner;
        FieldMonitor.OnUnRegisterRunner += DestroyMiniRunner;
        //FullGameDirector.OnShowBatter += NewBatter;
        FullGameDirector.OnBatterWalkToPlate += NewBatter;
        FullGameDirector.OnChangeover += UpdateInningText;
    }

    void OnDisable()
    {
        FullGameDirector.OnBallsChange -= UpdateBallIndicators;
        FullGameDirector.OnStrikesChange -= UpdateStrikeIndicators;
        FullGameDirector.OnOutsChange -= UpdateOutIndicators;
        FullGameDirector.OnScoreChange -= SetRuns;
        FieldMonitor.OnRegisterRunner -= CreateMiniRunner;
        FieldMonitor.OnUnRegisterRunner -= DestroyMiniRunner;
        //FullGameDirector.OnShowBatter -= NewBatter;
        FullGameDirector.OnBatterWalkToPlate -= NewBatter;
        FullGameDirector.OnChangeover -= UpdateInningText;
    }

    void Update()
    {
        if (overlayCanvas.worldCamera != renderCamera) overlayCanvas.worldCamera = renderCamera;

        if (readyToSpawnNewTicker)
        {
            if(tickerWords.Count > 0 && tickerObjectQueue.Count > 0)
            {
                StartCoroutine(ScrollTickerText(tickerObjectQueue.Dequeue(), tickerWords.Dequeue()));
                readyToSpawnNewTicker = false;
            }
        }

        foreach (KeyValuePair<OffensePlayer, RectTransform> player in offensePlayers)
        {
            player.Value.localPosition = new Vector3(player.Key.position.x * runnerMapScaleFactor, player.Key.position.z * runnerMapScaleFactor, 0f);
        }
    }

    void SetRuns(int team, int inning, int runs)
    {
        if (team % 2 == 0)
        {
            teamTwoScore.text = FullGameDirector.awayTeamRuns.ToString();
        }
        else
        {
            teamOneScore.text = FullGameDirector.homeTeamRuns.ToString();
        }
    }

    void UpdateBallIndicators(int balls)
    {
        if (balls >= 3) ballThreeIndicator.gameObject.SetActive(true);
        else ballThreeIndicator.gameObject.SetActive(false);

        if (balls >= 2) ballTwoIndicator.gameObject.SetActive(true);
        else ballTwoIndicator.gameObject.SetActive(false);

        if (balls >= 1) ballOneIndicator.gameObject.SetActive(true);
        else ballOneIndicator.gameObject.SetActive(false);
    }

    void UpdateStrikeIndicators(int strikes)
    {
        if (strikes >= 2) strikeTwoIndicator.gameObject.SetActive(true);
        else strikeTwoIndicator.gameObject.SetActive(false);

        if (strikes >= 1) strikeOneIndicator.gameObject.SetActive(true);
        else strikeOneIndicator.gameObject.SetActive(false);
    }

    void UpdateOutIndicators(int outs)
    {
        if (outs >= 2) outTwoIndicator.gameObject.SetActive(true);
        else outTwoIndicator.gameObject.SetActive(false);

        if (outs >= 1) outOneIndicator.gameObject.SetActive(true);
        else outOneIndicator.gameObject.SetActive(false);
    }

    public void QueueWordsForTicker(string text)
    {
        tickerWords.Enqueue(text);
    }

    IEnumerator ScrollTickerText(Text tickerText, string text)
    {
        tickerText.text = ("  -  " + text);
        RectTransform tickerTextTransform = tickerText.gameObject.GetComponent<RectTransform>();
        RectTransform TVOverlayTransform = gameObject.GetComponent<RectTransform>();

        tickerTextTransform.sizeDelta = new Vector2(tickerText.preferredWidth, tickerPanel.sizeDelta.y);
        float startXPos = ((tickerTextTransform.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float endXPos = -((tickerTextTransform.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float newSpawnXPos = (TVOverlayTransform.sizeDelta.x / 2f) - (tickerTextTransform.sizeDelta.x / 2f);
        bool reachedSpawnThreshold = false;
        float timer = 0f;
        float endTime = ((startXPos - endXPos) / 1000f) / textScrollSpeed;
        tickerTextTransform.transform.localPosition = new Vector3(startXPos, -(tickerPanel.rect.height / 2f), 0f);

        while (timer < endTime)
        {
            timer += Time.deltaTime;
            float xPos = Mathf.Lerp(startXPos, endXPos, timer / endTime);
            tickerTextTransform.localPosition = new Vector3(xPos, tickerTextTransform.localPosition.y, tickerTextTransform.localPosition.z);
            if (!reachedSpawnThreshold && xPos <= newSpawnXPos)
            {
                reachedSpawnThreshold = true;
                readyToSpawnNewTicker = true;
            }
            yield return null;
        }
        tickerTextTransform.localPosition = new Vector3(endXPos, tickerTextTransform.localPosition.y - (tickerTextTransform.sizeDelta.y / 2), tickerTextTransform.localPosition.z);
        tickerObjectQueue.Enqueue(tickerText);

        //This is temporary because JJ wanted the text to loop forever.
        QueueWordsForTicker(text);

        yield return null;
    }

    void CreateMiniRunner(OffensePlayer player)
    {
        RectTransform miniTransform = Instantiate(runnerHelmetPrefab, runnersPanel).GetComponent<RectTransform>();
        offensePlayers.Add(player, miniTransform);
    }

    void DestroyMiniRunner(OffensePlayer player)
    {
        if (offensePlayers.ContainsKey(player))
        {
            //Destroy(offensePlayers[player].gameObject);
            StartCoroutine(shrinkRunner(offensePlayers[player]));
            offensePlayers.Remove(player);
        }
    }

    IEnumerator shrinkRunner(RectTransform trans)
    {
        float timer = 0f;
        float timeToComplete = 0.5f;
        Vector2 startsize = trans.sizeDelta;
        Vector2 originalPivot = trans.pivot;

        while (timer < timeToComplete)
        {
            timer += Time.deltaTime;
            trans.sizeDelta = Vector2.Lerp(startsize, Vector2.zero, (timer / timeToComplete));
            float scaleFactor = (trans.sizeDelta.y / startsize.y);
            if (scaleFactor > 0)
            {
                trans.pivot = new Vector2(0.5f, (originalPivot.y / scaleFactor));
            }
            yield return null;
        }
        trans.sizeDelta = Vector2.zero;
        yield return null;
        Destroy(trans.gameObject);
        yield return null;
    }

    void NewBatter(Transform newBatter, PlayerInfo batterInfo)
    {
        if ((batterInfo.fullName + " #" + batterInfo.jerseyNumber) != batterNameText.text)
        {
            batterNameText.text = batterInfo.fullName + " #" + batterInfo.jerseyNumber;
            StartCoroutine(BatterNameScrollRoutine());
        }
    }

    IEnumerator BatterNameScrollRoutine()
    {
        RectTransform TVOverlayTransform = gameObject.GetComponent<RectTransform>();
        float startXPos = ((batterNamePanel.sizeDelta.x / 2f) + (TVOverlayTransform.sizeDelta.x / 2f));
        float endXPos = -((TVOverlayTransform.sizeDelta.x / 2f) - (batterNamePanel.sizeDelta.x / 2f));
        float timer = 0f;
        batterNamePanel.transform.localPosition = new Vector3(startXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);

        while(timer < 1f)
        {
            timer += Time.deltaTime;
            float newXPos = Mathf.SmoothStep(startXPos, endXPos, timer);
            batterNamePanel.transform.localPosition = new Vector3(newXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);
            yield return null;
        }
        batterNamePanel.transform.localPosition = new Vector3(endXPos, batterNamePanel.transform.localPosition.y, batterNamePanel.transform.localPosition.z);
        yield return null;
    }

    void UpdateInningText(int inning, bool isTopOfFrame)
    {
        string newText;
        switch (inning)
        {
            case 1:
                newText = "1st";
                break;
            case 2:
                newText = "2nd";
                break;
            case 3:
                newText = "3rd";
                break;
            default:
                newText = (inning.ToString() + "th");
                break;
        }
        inningText.text = newText;
        inningArrowTransform.localEulerAngles = new Vector3(0f, 0f, (isTopOfFrame ? 0f : 180f));
    }
}