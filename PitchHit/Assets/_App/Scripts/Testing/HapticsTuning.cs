﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticsTuning : MonoBehaviour {
    public float vibeDuration = 1;
    private float debug_amp = 0;
    private float debug_freq = 0;

    // Update is called once per frame
    void Update () {
        bool vibe = false;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            vibe = true;
            debug_amp += .1f;

        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            vibe = true;
            debug_amp -= .1f;

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            vibe = true;
            debug_freq += 10;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            vibe = true;
            debug_freq -= 10;
        }

        if (vibe)
        {
            debug_amp = Mathf.Clamp01(debug_amp);
            debug_freq = Mathf.Clamp(debug_freq, 0, 320);
            HandManager.VibrateController(0, vibeDuration, debug_freq, debug_amp, HandManager.currentRightCustomHand);
        }
    }
}
