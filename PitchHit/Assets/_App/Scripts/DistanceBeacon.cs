﻿using UnityEngine;
using System.Collections;

public class DistanceBeacon : MonoBehaviour {
    public Renderer particles;
    public float lifeSpan = 5f;
    private float countDown;

	// Use this for initialization
	void Start () {
        Deactivate();
	}

    public void SetColor(Color c)
    {
        particles.GetComponent<Renderer>().material.color = c;
    }

    public void Activate()
    {
        if (particles)
        {
            particles.gameObject.SetActive(true);
            StartCoroutine(CountDownToDeactivate());
        }
    }

    public void Deactivate()
    {
        StopAllCoroutines();
        if (particles)
        {
            particles.gameObject.SetActive(false);
        }
    }

    IEnumerator CountDownToDeactivate()
    {
        countDown = lifeSpan;

        while (countDown > 0)
        {
            countDown -= Time.deltaTime;
            yield return null;
        }
        Deactivate();
    }
}
