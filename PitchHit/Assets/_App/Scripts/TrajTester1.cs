﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Valve.VR.InteractionSystem;

//[RequireComponent(typeof(Rigidbody))]
public class TrajTester1 : MonoBehaviour
{
    //public static Vector3 initialPosition;

    //private Vector3 initialVelocity;
    public GameObject debugVisual;
    public GameObject markerPrefab;
    [HideInInspector]
    public Rigidbody rigid;
    private AudioSource sound;
    private BallLifetime life;
    public bool isCaught;
    public bool isInGlove;

    //public float turnSpeed = 1f;
    public bool homeRunsOnly;

    //public static GameObject CameraVR;
    // private Transform target;

    //public float marekerLifeTime;
    public static hitThings bat;
    //public GameObject glove;
    public HumanGloveController caught;
    // public static Hand hand;
    //public static Hand hand2;
    [HideInInspector]
    public hittable hittableComponent;
    private Vector3 previousPosition;

    public static HomeRunLevelController homeRunDetector;
    public static CatchLevelController catchLevelController;
    private static LayerMask gloveLayer;
    private static float ballRadius = .05f;
    //public static Quaternion initialVRRotation; 

    private bool isSweeping = false;
    private Renderer rend;

    private bool stillJudging;
    private bool isATrueHomeRun;

    // Use this for initialization
    //public GameObject glove;
    void Awake()
    {
        rend = gameObject.GetComponentInChildren<Renderer>();
        if (!homeRunDetector)
        {
            homeRunDetector = FindObjectOfType<HomeRunLevelController>();
        }
        rigid = gameObject.GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();
        hittableComponent = GetComponent<hittable>();
        life = GetComponent<BallLifetime>();

        previousPosition = transform.position;

        //if (!glove)
        //{
        //glove = GameObject.FindGameObjectWithTag("Glove");

        //if (glove)
        //{
        //    caught = glove.GetComponent<Ball_caught>();
        //this is for multiplayer but shouldn't be necessary for single player either.
        //caught.SetAttached(false);
        //}

        //}
        if (!bat)
        {
            GameObject tempBat = GameObject.FindGameObjectWithTag("bat");
            if (tempBat)
            {
                bat = tempBat.GetComponent<hitThings>();
            }
            gloveLayer = LayerMask.NameToLayer("Glove");
        }

    }

    void OnEnable()
    {
        if (hittableComponent)
        {
            hittableComponent.OnBatHit += GetHit;
        }
    }

    void OnDisable()
    {
        if (hittableComponent)
        {
            hittableComponent.OnBatHit -= GetHit;
        }
    }

    /*
    void Update()
    {
        if (hittableComponent)
        {
            if (debugVisual)
            {
                debugVisual.SetActive(hittableComponent.hitGround);
                //debugVisual.SetActive(false);
            }
            //hittableComponent.hasBeenCaught = isCaught;
        }
    }
    */

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "ground")
        {
            if (this.gameObject.tag == "ball_in_use")
            {
                //this.gameObject.tag = "ball";
                if (catchLevelController && !hittableComponent.isScored)
                {
                    catchLevelController.OnCatch(false);
                    hittableComponent.isScored = true;
                }
                //rend.material.color = Color.white;
                StopSweeping();
            }

            //hittableComponent.isScored = true;
        }
    }

    public void CancelScoring()
    {
        hittableComponent.isScored = true;
        this.enabled = false;
    }

    void GetHit(hittable hit)
    {
        if (hittableComponent.isScored)
        {
            return;
        }

//        Debug.Log("traj ball got hit.");
        if (homeRunsOnly)
        {
            Vector3 landingSpot = Trajectory.GetLandingSpot(rigid);

            if (homeRunDetector.isHomeRun(landingSpot))
            {
                CancelScoring();
                ScoreboardController.instance.HomeRun();
            }
            return;
        }

        if (catchLevelController)
        {
            //Debug.Log("talk to catch level controller");]
            catchLevelController.BeginSendToOutfield(this);
            //StartCoroutine(catchLevelController.SendToOutfield(this));
        }

        //this.gameObject.tag = "ball_in_use";
    }

    /*
    IEnumerator HomeRunRoutine()
    {
        Debug.Log("home run routine.");
        yield return new WaitForSeconds(1f);
        Debug.Log("now");
        homeRunDetector.TriggerFireworks();
        homeRunDetector.ScoreHomeRun();

        homeRunDetector.HomeRun();

    }
    */
    public void StartSweeping()
    {
        if (!isSweeping)
        {
            StartCoroutine("SweepForGloveCollision");
        }
    }

    public void StopSweeping()
    {
        //        Debug.Log("Stop Sweeping");
        isSweeping = false;
        //if (debugVisual)
        //{
        //    debugVisual.SetActive(false);
        //}
        //rend.material.color = Color.black;
        StopAllCoroutines();
    }

    IEnumerator SweepForGloveCollision()
    {
        //rend.material.color = Color.cyan;
        isSweeping = true;
        //if (debugVisual)
        //{
        //    debugVisual.SetActive(true);
        //}
        //Time.timeScale = .6f;
        //rend.material.color = Color.yellow;
        //        Debug.Log("start sweep routine");
        previousPosition = transform.position;

        while (isSweeping && !isCaught)
        {
//            Debug.Log("ball is sweeping! " + caught);
            yield return null;
            //Vector3 currentPosition = transform.position;
            //transform.position = previousPosition;
            RaycastHit hit;
            Vector3 sweep = transform.position - previousPosition;
            //Debug.Log("do sweep over distance " + sweep.magnitude +" with rad "+ballRadius+"   "+gloveLayer);

            if (Physics.SphereCast(previousPosition, ballRadius, sweep, out hit, sweep.magnitude, 1 << gloveLayer))//,QueryTriggerInteraction.Collide))
            //if(rigid.SweepTest(sweep, out hit,sweep.magnitude,QueryTriggerInteraction.Collide))
            {
                //Debug.Log("sweep hit something +  " + FieldManager.Instance + " " + isCaught);
                if (hit.collider.gameObject.layer == gloveLayer)
                {
                    //should check to make sure the ball is coming from the right direction.

                    //Debug.Log("<color=red>hit the glove layer</color>");

                    if ((hit.collider.name == "Solid Catch" || (FullGameDirector.Instance && FullGameDirector.Instance.defense)) && (!(caught.requireGrip && !hittableComponent.hitGround) || caught.isGripping) && !isCaught)//Greg: If we're playing full baseball... just make the whole glove catch it.
                    {
                        //transform.position = hit.collider.transform.position;
                        if (caught && !(caught.GetCurrentBall() && caught.GetCurrentBall().isHeld))
                        {
                            Debug.Log("CAUGHT");
                            BecomeCaught();
                        }
                        else
                        {
                            //Debug.Log("don't caught. "+caught);
                        }
                    }
                    else
                    {
//                        Debug.Log("xxxxxxxxxxxxxxxxxxxxx bounce off glove IN SWEEP");
                        rigid.velocity = Vector3.up;
                    }
                }
                else
                {
 //                    Debug.Log("xxxxxxxxxxxxx hit something not glove "+hit.collider.name);
                    previousPosition = transform.position;
                }
            }
            else
            {
                //Debug.Log("found nothing "+caught);
                previousPosition = transform.position;
            }
            if (caught)
            {
                Debug.Log("...................................................OK BUT SHOULD YOU JUST SNAP TO THE GLOVE?! " + caught.isGripping + "   " + (!isCaught));
                if (caught.isGripping && !(caught.requireGrip && !hittableComponent.hitGround) && !isCaught)
                {
                    Debug.Log("EXTRA BECOME CAUGHT.");
                    BecomeCaught();
                }
            }
        }
    }

    
    void BecomeCaught()
    {
        /*
        GetComponent<trailParticleManager>().StopEmission();

        isCaught = true;
        isInGlove = true;
        rigid.interpolation = RigidbodyInterpolation.None;

        caught.OnBallCaught(this);
        Debug.Log("____________________________________________ parent the ball to " +caught.catchPosition.name);
        transform.SetParent(caught.catchPosition);
        transform.localPosition = Vector3.zero;
        rigid.isKinematic = true;
        //Debug.Log("<color=red>" + caught.catchPosition + " parent " + transform.parent.name + "</color>");
        //Debug.Log("hit solid catch sweep " + transform.parent.name + "   " + transform.localScale);

        //controller.PlaySound();
        //hittableComponent.isActive = false;
        if (life != null)
        {
            life.StopKillCount();
        }
        if (catchLevelController && !hittableComponent.isScored)
        {
            catchLevelController.OnCatch(true);
            hittableComponent.isScored = true;
        }
        isSweeping = false;
        //if (debugVisual)
        //{
        //    debugVisual.SetActive(false);
        //}
        */
    }
    

    void OnTriggerEnter(Collider c)
    {

        if (!isCaught && c.gameObject.layer == gloveLayer)
        {
            bool isSolidCatch = c.name == "Solid Catch";
            if (isSolidCatch || FullGameDirector.Instance)
            {
//                Debug.Log("is a solid catch.");
                if(caught == null)
                {
                    caught = c.GetComponent<HumanGloveController>();
                }

//              Debug.Log("become caught. by caught "+caught);
                if (!caught && c.gameObject.transform.parent && c.gameObject.transform.parent.parent)
                {
                    caught = c.gameObject.transform.parent.parent.GetComponent<HumanGloveController>();   
                }
                if (caught)
                {
//                  Debug.Log("the caught exists.");
                    if ((caught.requireGrip && !hittableComponent.hitGround) && !caught.isGripping && rigid.isKinematic==false)
                    {
//                      Debug.Log("nevermind. it's not a solid catch. "+rigid.isKinematic);
                        isSolidCatch = false;
                    }
                }
                if (caught && !(caught.GetCurrentBall() && caught.GetCurrentBall().isHeld) && (isSolidCatch || (caught.isGripping && !(caught.requireGrip && !hittableComponent.hitGround))))
                {
//                  Debug.Log("become caught.");
                    BecomeCaught();
                }
               
            }
            else
            {
//              Debug.Log("xxxxxxx not a solid catch.");
                if (caught)
                {
                    if ((caught.gameObject != c.gameObject) && !(caught.GetCurrentBall() && caught.GetCurrentBall().isHeld))
                    {
                        rigid.velocity = Vector3.up;
                    }
                }

            }
        }
        /*
        else
        {
            Debug.Log(Time.time+" curreng ball :"+caught.GetCurrentBall());
            if (caught.GetCurrentBall())
            {
                Debug.Log("current ball is caught "+caught.GetCurrentBall().isCaught);
            }
        }
        */
    }

    public void OnGrabbed()
    {
        isInGlove = false;
        if (this.gameObject.tag == "ball_in_use")
        {
            if (catchLevelController && !hittableComponent.isScored)
            {
                catchLevelController.OnCatch(true);
                hittableComponent.isScored = true;
                //hittableComponent.isActive = false;
                rigid.isKinematic = true;
                isCaught = true;
                rigid.interpolation = RigidbodyInterpolation.None;
            }

        }
        life.StopKillCount();
    }

    public void OnReleased()
    {
        isCaught = false;
        isInGlove = false;
    }

    public void OnTossed()
    {
        isCaught = false;
        isInGlove = false;
        //Debug.Log("tossed.");
        transform.localScale = Vector3.one;
        //hittableComponent.isActive = true;
        rigid.isKinematic = false;
        //if (this.gameObject.tag == "ball_in_use")
        // {
        //rend.material.color = Color.green;
        //isSweeping = false;
        //Debug.Log("restart sweep coroutine");
        //StopAllCoroutines();
        //StartSweeping();
        //}
        life.StartKillCount();
    }

    public void BeginJudgement()
    {
        StopAllCoroutines();
        StartCoroutine(JudgementRoutine());
    }

    public bool isJudging()
    {
        return stillJudging;
    }

    public bool isTrueHomeRun()
    {
        return isATrueHomeRun;
    }

    IEnumerator JudgementRoutine()
    {
        //bool everyOther = false;
        stillJudging = true;

        yield return new WaitForSeconds(.1f);
        Rigidbody rb = GetComponent<Rigidbody>();
        Vector3 lastJudgedPosition = transform.position;
        // spotAtPeakElevation = transform.position;
        Vector3 lastJudgedVelocity = rb.velocity;
        //Queue<Vector3> oldSpots = new Queue<Vector3>();
        //Queue<Vector3> oldDirections = 

        float timeToMeasurePerFrame = .2f;
        float accumulatedFlightTime = 0f;
        float initialTime = Time.time;
        while (stillJudging)
        {
            Vector3 actualPosition = rb.transform.position;
            Vector3 actualVelocity = rb.velocity;

            rb.transform.position = lastJudgedPosition;
            rb.velocity = lastJudgedVelocity;
            //Debug.Log("still judging");
            RaycastHit ballHit;
            if (Trajectory.GetPointOfCollision(rb, out ballHit, timeToMeasurePerFrame))
            {

                stillJudging = false;
                switch (ballHit.collider.tag)
                {
                    case "wall":
                        if (Vector3.Angle(ballHit.normal, Vector3.up) < 40f)
                        {
                            isATrueHomeRun = true;//barely
                        }
                        else
                        {
                            isATrueHomeRun = false;//hit wall
                        }
                        break;
                    case "target":
                    case "lights":
                    case "stands":
                    case "sign":
                    case "ground"://this is assuming we've already checked to see
                        isATrueHomeRun = true;
                        break;
                    default://I guess it never came down?
                        isATrueHomeRun = true;
                        break;
                }
            }
            else
            {
                float futureElevation = lastJudgedPosition.y + (rb.velocity.y * timeToMeasurePerFrame) + (.5f * Physics.gravity.y * timeToMeasurePerFrame * timeToMeasurePerFrame);

                lastJudgedPosition = new Vector3(lastJudgedPosition.x + (rb.velocity.x * timeToMeasurePerFrame),
                                                   futureElevation,
                                                   lastJudgedPosition.z + (rb.velocity.z * timeToMeasurePerFrame));
                lastJudgedVelocity += Physics.gravity * timeToMeasurePerFrame;

                if (lastJudgedPosition.y < 0)//it must have fallen past everything and into the void.
                {
                    stillJudging = false;
                    isATrueHomeRun = true;
                }
                else
                {
                    accumulatedFlightTime += timeToMeasurePerFrame;
                }
            }

            rb.transform.position = actualPosition;
            rb.velocity = actualVelocity;
            //if (everyOther)
            //{
            //    yield return null;
            //}
            //else
            //{
            //    everyOther = true;
            //}
        }
    }
}
