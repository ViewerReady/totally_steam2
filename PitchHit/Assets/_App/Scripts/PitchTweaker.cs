﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PitchTweaker : MonoBehaviour
{
    public Text displayTextSpeed;
    public Text displayTextDirection;
    public Text displayTextAssist;

    public float tweakValue = .0001f;
    public float tweakValueAngle = .01f;
    public float tweakAssistValue = .1f;

    // Update is called once per frame
    void Update()
    {
        displayTextSpeed.text = "Speed:" + Pitchable.throwSpeedMultiplier.ToString("F4");
        displayTextDirection.text = "Angle:" + Pitchable.flickDirectionFactor.ToString("F4");
        displayTextAssist.text = "Assist:" + Pitchable.assistWeight.ToString("0.0");

    }

    public void TweakPitch(bool up)
    {
        Pitchable.throwSpeedMultiplier += (up ? tweakValue : (-1f * tweakValue));
    }

    public void TweakPitchAngle(bool up)
    {
        Pitchable.flickDirectionFactor += (up ? tweakValueAngle : (-1f * tweakValueAngle));
    }

    public void TweakPitchAssist(bool up)
    {
        Pitchable.assistWeight += (up ? tweakAssistValue : -tweakAssistValue);
    }
}
