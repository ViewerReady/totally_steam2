﻿/********************************************************************************//**
\file      TouchController.cs
\brief     Animating controller that updates with the tracked controller.
\copyright Copyright 2015 Oculus VR, LLC All Rights reserved.
************************************************************************************/

using UnityEngine;

namespace OVRTouchSample
{
    public class TouchAnimator : MonoBehaviour
    {
        public bool demoHighlights;

        [SerializeField]
        Material lightUpMaterial;
        Material baseMaterial;
        [SerializeField]
        private OVRInput.Controller m_handedness;
        [SerializeField]
        private Animator m_animator = null;

        private bool bActive = false;
        private string prefix = "rctrl";

        private TrackedController m_trackedController = null;

        private GameObject Abutton, Bbutton, Trigger, GripTrigger, Joystick;
        private Renderer AbuttonRNDR, BbuttonRNDR, TriggerRNDR, GripTriggerRNDR, JoystickRNDR;
        private void Start()
        {
            m_trackedController = TrackedController.GetController(m_handedness);

            if (m_handedness == OVRInput.Controller.LTouch)
            {
                prefix = "lctrl";
                // ACTALLY X AND Y !
                Abutton = GameObject.Find(prefix + ":x_button_PLY");
                Bbutton = GameObject.Find(prefix + ":y_button_PLY");

            }
            else
            {

                Abutton = GameObject.Find(prefix + ":a_button_PLY");
                Bbutton = GameObject.Find(prefix + ":b_button_PLY");
            }
    
        

            Trigger = GameObject.Find(prefix + ":main_trigger_PLY");
            GripTrigger = GameObject.Find(prefix + ":side_trigger_PLY");
            Joystick = GameObject.Find(prefix + ":thumbstick_ball_PLY");
            AbuttonRNDR = Abutton.GetComponent<Renderer>();
            BbuttonRNDR = Bbutton.GetComponent<Renderer>();
            TriggerRNDR = Trigger.GetComponent<Renderer>();
            GripTriggerRNDR = GripTrigger.GetComponent<Renderer>();
            JoystickRNDR = Joystick.GetComponent<Renderer>();

            baseMaterial = Abutton.GetComponent<Renderer>().material;

        }

        private void Update()
        {
            if (m_trackedController != null)
            {
                
                m_animator.SetFloat("Button 1", m_trackedController.Button1 ? 1.0f : 0.0f);
                m_animator.SetFloat("Button 2", m_trackedController.Button2 ? 1.0f : 0.0f);
                Vector2 joyStick = m_trackedController.Joystick;
                m_animator.SetFloat("Joy X", joyStick.x);
                m_animator.SetFloat("Joy Y", joyStick.y);
                m_animator.SetFloat("Grip", m_trackedController.GripTrigger);
                m_animator.SetFloat("Trigger", m_trackedController.Trigger);
            }

            if (demoHighlights)
            {
                SetButtonOneHighlight(m_trackedController.Button1);
                
                SetButtonTwoHighlight(m_trackedController.Button2);

                SetButtonGripHighlight(m_trackedController.GripTrigger > 0);

                SetButtonTriggerHighlight(m_trackedController.Trigger > 0);

                SetJosystickHighlight(m_trackedController.Joystick.x != 0 || m_trackedController.Joystick.y != 0);
            }
           // print(m_trackedController.Button1);

        }

        public void SetButtonOneHighlight(bool on)
        {
            if (on)
            {
                AbuttonRNDR.material = lightUpMaterial;
            }
            else
            {
                AbuttonRNDR.material = baseMaterial;
            }
        }

        public void SetButtonTwoHighlight(bool on)
        {
            if (on)
            {
                BbuttonRNDR.material = lightUpMaterial;
            }
            else
            {
                BbuttonRNDR.material = baseMaterial;
            }
        }

        public void SetButtonGripHighlight(bool on)
        {
            if (on)
            {
                GripTriggerRNDR.material = lightUpMaterial;
            }
            else
            {
                GripTriggerRNDR.material = baseMaterial;
            }
        }

        public void SetButtonTriggerHighlight(bool on)
        {
            if (on)
            {
                TriggerRNDR.material = lightUpMaterial;
            }
            else
            {
                TriggerRNDR.material = baseMaterial;
            }
        }

        public void SetJosystickHighlight(bool on)
        {
            if (on)
            {
                JoystickRNDR.material = lightUpMaterial;
            }
            else
            {
                JoystickRNDR.material = baseMaterial;
            }
        }
    }
}
