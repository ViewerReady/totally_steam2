﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Valve.VR;

public class DistanceScoreController : MonoBehaviour
{

    public enum distanceTypes
    {
        ZMagnitude, XZMagnitude//, arcPath
    }

    public enum endPointTypes
    {
        groundCollision//, lowElevation, trajectoryPrediction
    }

    public Text distanceText;
    public Text otherDistanceText;
    public AudioClip toneClip;
    private hitThings bat;
    private Rigidbody batBody; //used for finding the hit velocity

    private hittable watchingBall;
    //ivate Hv_DistanceTone_LibWrapper tone;
    private DistanceCounter counter;
    private float bestDistance = 0f;
    public Text best;

    public Color visualizationColor;
    public Color visualizationColorTwo;
    public Transform visualizerGroup;
    public Transform visualizerTransform;
    public Transform otherVisualizerTransform;
    private Renderer visualizerRenderer;
    private Renderer otherVisualizerRenderer;
    private Transform playerHead;

    public distanceTypes distanceType;
    public endPointTypes endPointType;

    public GameObject distanceBeaconPrefab;
    public float distanceBetweenBeacons = 100f;
    public float maxBeaconDistance = 1600f;
    public float beaconRowsWidth = 20f;
    public int numberOfBeaconColumns = 2;
    private DistanceBeacon[][] beacons;
    private int beaconRowsActivated = 0;
    public GameObject sound;
    float currentDistance = 0f;
    bool initialVelocityFound = false;
    float previousPitch;
    private Vector3 initialPosition;
    //private Hv_DistanceTone_LibWrapper tone;

    // private IEnumerator ballwatchingRoutine;
    private IEnumerator beaconDeactivatingRoutine;

    public int levelNumber = 4;
    public AudioSource scoreSound;
    public AudioSource challengeSound;
    public AudioClip successClip;
    //public float maxDistanceForChallenge = 120;
    //public float minDistanceforChallenge = 90f;
	public int minScoreForChallenge = 6;

    public int[] furthestRanges = { 150, 200, 250, 300, 350, 400, 450, 500 };
    public int[] closestRanges = { 0, 5, 10, 20, 50, 100 };
    public int[] rangeSpans = { 60, 30, 20, 10, 5, 3, 2, 1 };
    public int scoreBetweenSteps = 5;
    public int scoreBetweenLongShots = 10;
    public Text currentTargetText;
    public Text nextTargetText;
    //public Text scoreTitle;
   // public Text scoreText;
    public GameObject rangeBeaconPrefab;

    private Transform[] maxRangeMarkers;
    private Transform[] minRangeMarkers;


    private int currentIndex = 0;


    private float currentRangeMax = 0f;
    private float currentRangeMin = 0f;
    private float nextRangeMax = 0f;
    private float nextRangeMin = 0f;

    void Awake()
    {
        bat = (hitThings)FindObjectOfType(typeof(hitThings));
        batBody = bat.gameObject.GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {
        //tone = GetComponent<Hv_DistanceTone_LibWrapper>();
        
        if (visualizerTransform)
        {
            visualizerRenderer = visualizerTransform.GetComponent<Renderer>();

            visualizerTransform.localScale = new Vector3(350f, 1f, 1f);
            visualizerRenderer.enabled = false;
            visualizerTransform.rotation = Quaternion.LookRotation(Vector3.forward);


            otherVisualizerRenderer = otherVisualizerTransform.GetComponent<Renderer>();
            otherVisualizerTransform.localScale = new Vector3(350f, 1f, 1f);
            otherVisualizerRenderer.enabled = false;
            otherVisualizerTransform.rotation = Quaternion.LookRotation(-Vector3.up);

            SteamVR_TrackedObject[] trackedObjects = FindObjectsOfType<SteamVR_TrackedObject>();
            foreach (SteamVR_TrackedObject t in trackedObjects)
            {
                if (t.index == SteamVR_TrackedObject.EIndex.Hmd)
                {
                    playerHead = t.transform;
                    break;
                }
            }
        }

        if (distanceBeaconPrefab)
        {
            StartCoroutine(PopulateBeacons());
        }
        //ballwatchingRoutine = watchTheBall();
        beaconDeactivatingRoutine = DeactivateBeacons();

        DetermineTargetRange();

        visualizerRenderer.material.color = visualizationColor;
        otherVisualizerRenderer.material.color = visualizationColor;

       // if(currentTargetText && dominantHandManager.dominantHand == dominantHandManager.Hand.left)
       // {
       //     Transform temp = currentTargetText.canvas.transform;
       //     temp.position = new Vector3(-temp.position.x, temp.position.y, temp.position.z);
       //     temp.rotation = Quaternion.Euler(temp.eulerAngles.x, -temp.eulerAngles.y, temp.eulerAngles.z);
       // }
    }

    IEnumerator PopulateBeacons()
    {
        int numberOfBeaconRows = (int)(maxBeaconDistance / distanceBetweenBeacons);
        beacons = new DistanceBeacon[numberOfBeaconRows][];
        for(int r = 0; r < numberOfBeaconRows; r++ )
        {
            beacons[r] = new DistanceBeacon[numberOfBeaconColumns];
            for(int c= 0; c< numberOfBeaconColumns; c++)
            {
                if (numberOfBeaconColumns > 1)
                {
                    Vector3 beaconPosition = new Vector3((-beaconRowsWidth * .5f) + ((float)c * beaconRowsWidth / (float)(numberOfBeaconColumns - 1)), 0f, maxBeaconDistance * (float)r / numberOfBeaconRows);
                    GameObject temp = Instantiate(distanceBeaconPrefab, beaconPosition, Quaternion.identity, transform) as GameObject;
                    //Debug.Log(r+"/"+numberOfBeaconRows+"       "+c+"/"+numberOfBeaconColumns);
                    beacons[r][c] = temp.GetComponent<DistanceBeacon>();
                    beacons[r][c].SetColor(visualizationColor);
                }
            }
            yield return null;
        }
    }

    IEnumerator DeactivateBeacons()
    {
        ///Debug.Log("deactivate beacons");
        int rowsToDeactivate = beaconRowsActivated;
        if(rowsToDeactivate>beacons.Length-1)
        {
            rowsToDeactivate = beacons.Length - 1;
        }
        beaconRowsActivated = 0;
        for(int r=0; r<= rowsToDeactivate; r++)
        {
            foreach(DistanceBeacon b in beacons[r])
            {
                b.Deactivate();
            }
            yield return null;
        }
    }

    void OnEnable()
    {
        //Debug.Log("distancescorecontroller enabled");
        BatController.onBatHit.AddListener(measureVelocity);
        //bat.onBallHit.AddListener(measureVelocity);
    }

    void OnDisable()
    {
        //Debug.Log("distancescorecontroller disabled");
        BatController.onBatHit.RemoveListener(measureVelocity);

        //bat.onBallHit.RemoveListener(measureVelocity);
    }

   

    public void DetermineTargetRange()
    {
        if (ScoreboardController.instance.myScore != 0 && (ScoreboardController.instance.myScore) % scoreBetweenSteps == 0)
        {
            currentIndex++;
        }

        int newestRangMax = 0;
        int newestRangeMin = 0;

        int currentFurthestRange = furthestRanges[(int)Mathf.Min(furthestRanges.Length - 1, currentIndex)];

        if (scoreBetweenLongShots>0 && (ScoreboardController.instance.myScore+1) % scoreBetweenLongShots==0)
        {
            newestRangeMin = currentFurthestRange;
            newestRangMax = 9999;
        }
        else
        {
            int currentClosestRange = closestRanges[(int)Mathf.Min(closestRanges.Length - 1, currentIndex)];
            int currentSpan = rangeSpans[(int)Mathf.Min(rangeSpans.Length - 1, currentIndex)];

            newestRangeMin = Random.Range(currentClosestRange, currentFurthestRange  - currentSpan);
            
            //newestRangeMin -= newestRangeMin % 5;

            newestRangMax = newestRangeMin + currentSpan;
            
        }


        if(nextRangeMax==0 && nextRangeMin==0)// first time ever called.
        {
            nextRangeMax = newestRangMax;
            nextRangeMin = newestRangeMin;

            nextTargetText.text = nextRangeMin + "m - " + nextRangeMax+"m";

            if (nextRangeMax != 0)
            {
                if (rangeBeaconPrefab)
                {
                    maxRangeMarkers = new Transform[numberOfBeaconColumns];
                    minRangeMarkers = new Transform[numberOfBeaconColumns];
                    for (int x = 0; x < numberOfBeaconColumns; x++)
                    {
                        maxRangeMarkers[x] = (Instantiate(rangeBeaconPrefab) as GameObject).transform;
                        minRangeMarkers[x] = (Instantiate(rangeBeaconPrefab) as GameObject).transform;

                        minRangeMarkers[x].GetComponentInChildren<Renderer>().material.color = visualizationColor;
                        maxRangeMarkers[x].GetComponentInChildren<Renderer>().material.color = visualizationColorTwo;

                        maxRangeMarkers[x].gameObject.SetActive(false);
                        minRangeMarkers[x].gameObject.SetActive(false);
                    }
                }

                DetermineTargetRange();
            }
            else
            {
                Debug.LogError("TARGET RANGE SETUP FAILED!");
            }
        }
        else
        {
            currentRangeMax = nextRangeMax;
            currentRangeMin = nextRangeMin;

            currentTargetText.text = nextTargetText.text;

            if (rangeBeaconPrefab)
            {
                for (int x = 0; x < numberOfBeaconColumns; x++)
                {
                    Vector3 beaconStart = Vector3.right * ((-beaconRowsWidth * .5f) + ((float)x * beaconRowsWidth / (float)(numberOfBeaconColumns - 1)));

                    maxRangeMarkers[x].position = beaconStart + (Vector3.forward * currentRangeMax);
                    minRangeMarkers[x].position = beaconStart + (Vector3.forward * currentRangeMin);
                    maxRangeMarkers[x].gameObject.SetActive(true);
                    minRangeMarkers[x].gameObject.SetActive(true);
                }
            }
            nextRangeMin = newestRangeMin;
            nextRangeMax = newestRangMax;
            if (nextRangeMax > currentFurthestRange)
            {
                nextTargetText.text = nextRangeMin + "m+";
            }
            else
            {
                nextTargetText.text = nextRangeMin + "m - " + nextRangeMax + "m";
            }
        }
    }

    private void SetRangeMarkersActive(bool a)
    {
        if (rangeBeaconPrefab)
        {
            for (int m = 0; m < numberOfBeaconColumns; m++)
            {
                maxRangeMarkers[m].gameObject.SetActive(a);
                minRangeMarkers[m].gameObject.SetActive(a);
            }
        }
    }

    void FixedUpdate()
    {
        if (watchingBall)
        {
            if (!watchingBall.hitGround && watchingBall.transform.position.y > 0)
            {
                /*
                if (speedText && !initialVelocityFound)
                {
                    if (watchingBall.initialVelocity != Vector3.zero)
                    {
                        speedText.text = watchingBall.initialVelocity.magnitude.ToString("F") + " m/s";
                        initialVelocityFound = true;
                    }
                }
                */
                visualizerRenderer.enabled = true;
                otherVisualizerRenderer.enabled = true;
                currentDistance = (watchingBall.transform.position - initialPosition).z;

                visualizerGroup.position = new Vector3(initialPosition.x, 0f, Mathf.Max(-50f,watchingBall.transform.position.z));


                distanceText.text = currentDistance.ToString("F2") + " m";
                otherDistanceText.text = distanceText.text;
                //distanceText.text = sound.pitch.ToString();
                /*
                if (tone && currentDistance > 0 && Mathf.Floor(currentDistance / 10f) != tone.freq)
                {
                    tone.freq = Mathf.Floor(currentDistance / 10f);
                    if (tone.trig == 0)
                    {
                        tone.trig = 1;
                    }
                    else
                    {
                        tone.trig = (tone.trig % 2) + 1;
                    }
                }
                */
                /*
                if (sound && toneClip && currentDistance > 0 && Mathf.Floor(currentDistance / distanceBetweenBeacons)*.1f != previousPitch)
                {
                    Debug.Log("make a sound!");
                    previousPitch = Mathf.Floor(currentDistance / distanceBetweenBeacons) * .1f;
                    AudioSource tempSound = (Instantiate(sound, transform.position, Quaternion.identity) as GameObject).GetComponent<AudioSource>();
                    Destroy(tempSound, 20f);
                    tempSound.clip = toneClip;
                    tempSound.pitch = previousPitch;
                    tempSound.Play();
                }
                */

                if (beaconRowsActivated < beacons.Length)
                {
                    if (currentDistance > (float)beaconRowsActivated * distanceBetweenBeacons)
                    {
                        foreach (DistanceBeacon b in beacons[beaconRowsActivated])
                        {
                            b.Activate();

                        }
                        beaconRowsActivated++;
                    }
                }

            }
            else//the ball has hit the ground
            {
                if (watchingBall && !watchingBall.isScored)
                {
                    watchingBall.isScored = true;   
                    SetBallFinalDistance((watchingBall.transform.position - initialPosition).z);
                    //Destroy(watchingBall.GetComponent<TrailRenderer>());
                    watchingBall = null;
                }
            }
        }
    }

    void measureVelocity(hittable ballController)
    {
        //if (!stats.scorable)
        //{
        //    visualizerRenderer.enabled = false;
        //    return;
        //}
        if (watchingBall == ballController)
        {
            return;
        }

        
        //TrailRenderer trailRend;
        if (watchingBall)
        {
            Destroy(watchingBall.GetComponent<TrailRenderer>());
        }

        watchingBall = ballController;


        initialPosition = watchingBall.positionWhenHit;
        initialVelocityFound = false;
        currentDistance = 0f;

        

        if (distanceBeaconPrefab)
        {
            StopCoroutine(beaconDeactivatingRoutine);
            beaconDeactivatingRoutine = DeactivateBeacons();
            StartCoroutine(beaconDeactivatingRoutine);
        }
    }

    private void SetBallFinalDistance(float score)
    {

        distanceText.text = score.ToString("F2") + " m";
        if (best)
        {
            if (score >= bestDistance)
            {
                bestDistance = score;
                //stats.score = (int)bestDistance;
            }
            best.text = bestDistance.ToString("F2") + " m";
        }

        if (ScoreboardController.instance.scorable)
        {
            if (score >= currentRangeMin && score <= currentRangeMax)
            {
                scoreSound.Play();
                ScoreboardController.instance.AddScore();
                //scoreText.text = (++stats.score).ToString();
                DetermineTargetRange();
            }
        }
        else
        {
            SetRangeMarkersActive(false);
        }

    }

    public void CheckIfChallengePassed()
    {
        int levelsUnlocked = PlayerPrefs.GetInt("LEVELSUNLOCKED");

        if (levelsUnlocked == levelNumber)
        {
			if (ScoreboardController.instance.myScore >= minScoreForChallenge)
            {
                PlayerPrefs.SetInt("LEVELSUNLOCKED", levelsUnlocked + 1);
                if (challengeSound && successClip)
                {
                    challengeSound.clip = successClip;
                    challengeSound.Play();
                }
            }
        }
    }

    //public void ObjectReference()
   // {
    //    counter = GameObject.FindGameObjectWithTag("ball_in_use").GetComponent<DistanceCounter>();
    //}
}
