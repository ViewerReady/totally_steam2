﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomerunSettingUI : MonoBehaviour {

    [SerializeField] private Button[] buttons;

    private const string abridgeHomerunPrefsKey = "ABRIDGE_HOMERUN";
    public static bool abridgeHomerun
    {
        get
        {
            if (!PlayerPrefs.HasKey(abridgeHomerunPrefsKey))
            {
                Debug.Log("Didnt have homerun key. Returning True");
                abridgeHomerun = true;
            }
            Debug.Log("We did have homerun key! Returning: " + (PlayerPrefs.GetInt(abridgeHomerunPrefsKey) > 0));
            return (PlayerPrefs.GetInt(abridgeHomerunPrefsKey) > 0);
        }
        set
        {
            Debug.Log("Setting homerun key to " + (value ? 1 : 0));
            PlayerPrefs.SetInt(abridgeHomerunPrefsKey, value ? 1 : 0);
        }
    }

    private void OnEnable()
    {
        RefreshButtons();
    }

    void RefreshButtons()
    {
        int cur = Convert.ToInt32(abridgeHomerun);
        Debug.Log("Homerun Key Is " + cur);
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i == cur)
            {
                Debug.Log("Setting buttons[" + i + "]: " + buttons[i].gameObject.name + " to false.");
                buttons[i].interactable = false;
            }
            else
            {
                Debug.Log("Setting buttons[" + i + "]: " + buttons[i].gameObject.name + " to true.");
                buttons[i].interactable = true;
            }
        }
    }

    public void SetAbridgeHomerun(bool type)
    {
        abridgeHomerun = type;
        RefreshButtons();
    }
}
