﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class LocomotionHighlights : MonoBehaviour {

    SteamVR_Action_Boolean runButtonAction;
    SteamVR_Action_Boolean reverseButtonAction;

    void Awake()
    {
#if STEAM_VR
        runButtonAction = SteamVR_Actions.baseballSet_RunButton;
        reverseButtonAction = SteamVR_Actions.baseballSet_ReverseButton;
#endif
    }

    public void HighlightRun()
    {
        HandManager.currentRightCustomHand.SetRunHint(true);
        HandManager.currentLeftCustomHand.SetRunHint(true);
    }

    public void HideRun()
    {
        HandManager.currentRightCustomHand.SetRunHint(false);
        HandManager.currentLeftCustomHand.SetRunHint(false);
    }

    public void HighlightReverse()
    {
        HandManager.currentRightCustomHand.SetReverseHint(true);
        HandManager.currentLeftCustomHand.SetReverseHint(true);
    }

    public void HideReverse()
    {
        HandManager.currentRightCustomHand.SetReverseHint(false);
        HandManager.currentLeftCustomHand.SetReverseHint(false);
    }

    public void HideAllButtonHighlights()
    {
        HandManager.currentLeftCustomHand.HideHints();
        HandManager.currentRightCustomHand.HideHints();
    }
}
