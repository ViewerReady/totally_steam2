﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class BoothSpeakersController : MonoBehaviour {

    Animator speakerAnimator;
    bool isAnimating;

    void Awake()
    {
        speakerAnimator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (speakerAnimator != null && CalloutManager.isVOPlaying && !isAnimating)
        {
            speakerAnimator.SetFloat("Speed", 1f);
            isAnimating = true;
        }
        else if (speakerAnimator != null && !CalloutManager.isVOPlaying && isAnimating)
        {
            speakerAnimator.SetFloat("Speed", 0f);
            speakerAnimator.Play("Talk", 0, 0);
            isAnimating = false;
        }
    }
}
