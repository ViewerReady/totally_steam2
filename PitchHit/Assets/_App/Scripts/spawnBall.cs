﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class spawnBall : MonoBehaviour {

    private bool keepSpawning;
    private bool hasSpawned;
    public AudioSource sound;
    public Image timerImage;
    public float yellowTime = 1f;
	public bool spawnWithColliderOn = true;
    private ballController.ballServiceType serviceType;

    public hittable NewBall(GameObject prefab)
    {
        GameObject ball;
        //if(prefab.GetComponent<PhotonView>() != null)
        //{
        //    ball = PhotonNetwork.Instantiate(prefab.name, transform.position, transform.rotation, 0);
        //}
        //else
        {
            ball = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
        }

        if (sound && sound.clip)
        {
            sound.Play();
        }

		if (spawnWithColliderOn) {
			ball.GetComponentInChildren<Collider> ().enabled = true;
		}

        //Debug.DrawRay(transform.position, Vector3.right, Color.green, 10f);
        //ball.transform.position = transform.position;
        return ball.GetComponent<hittable>();
    }


    public void StartAutomaticSpawning(GameObject prefab, ballController.ballServiceType type, float greenTime = .1f)
    {
        serviceType = type;
        if (!keepSpawning)
        {
            keepSpawning = true;
            StartCoroutine(AutomaticSpawning(prefab, greenTime));
        }
    }

    public void StopAutomaticSpawning()
    {
        keepSpawning = false;
        StopAllCoroutines();
        if (timerImage)
        {
            timerImage.enabled = false;
        }
    }


    public IEnumerator AutomaticSpawning(GameObject prefab, float greenTime)
    {
        if(timerImage)
        {
            timerImage.enabled = true;
            timerImage.fillAmount = 0f;
        }
        while (!GameController.levelIsBegun)
        {

            yield return null;
        }
        float autoPitchTimer = 0;

        Transform camTransform = Camera.main.transform;

        while (keepSpawning)
        {
            while (autoPitchTimer < yellowTime && !hasSpawned)
            {
                
                while ((Physics.OverlapSphere(transform.position, prefab.transform.lossyScale.x * prefab.GetComponent<SphereCollider>().radius).Length != 0) && !hasSpawned)
                {
                    if (timerImage)
                    {
                        timerImage.color = Color.red;
                    }
                    yield return null;
                }
                if (timerImage)
                {
                    timerImage.color = Color.yellow;
                }
                if (GameController.isAtBat)
                {
                    autoPitchTimer += Time.deltaTime;
                }
                else
                {
                    autoPitchTimer = 0;
                }

                if (timerImage)
                {
                    timerImage.fillAmount = autoPitchTimer / yellowTime;
                }
                yield return null;
            }


            if (timerImage)
            {
                timerImage.color = Color.green;
            }

            if (!hasSpawned)
            {
                hittable temp = NewBall(prefab);

                if (serviceType == ballController.ballServiceType.tee)
                {
                    temp.SetOrigin(serviceType, transform);
                    ballController.currentlyActiveBall = temp;
                }
            }
            autoPitchTimer = 0f;

            while (autoPitchTimer < greenTime)
            {
                if (hasSpawned)
                {
                    hasSpawned = false;
                    autoPitchTimer = 0;
                }
                autoPitchTimer += Time.deltaTime;
                yield return null;
            }
            yield return new WaitForSeconds(greenTime);
        }

        if (timerImage)
        {
            timerImage.color = Color.red;
        }
    }
}
