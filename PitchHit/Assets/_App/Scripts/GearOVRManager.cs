﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearOVRManager : MonoBehaviour {

    /// <summary>
    /// This script positions the player and controller based on left or ritght handed
    /// for use with Gear Touch
    /// </summary>

    public GameObject mobileUIPrefab;
    public float popUpUIDistance = 400f;
    public Transform leftHeadTransform;
    public Transform rightHeadTransform;
    public Transform leftHandTransform;
    public Transform rightHandTransform;
    public Transform rootTransform;
    public CustomHand leftHand;
    public CustomHand rightHand;


#if GEAR_VR
    private bool _leftHanded = false;
    private Vector3 _leftHandPosition, _rightHandPosition, _leftHeadPosition, _rightHeadPosition;
    private GameObject popUpUI;
    private ButtonPointer pointer;
    //Set this to change hands
    public bool LeftHanded
    {
        get {
            return _leftHanded;
        }
        set
        {
            _leftHanded = value;
            //StartCoroutine(positionPlayer());
        }
    }
    //snooze
    IEnumerator positionPlayer()
    {
        //yield return new WaitForSeconds(2f);
        yield return new WaitForEndOfFrame();
        OVRManager ovrManager = GetComponent<OVRManager>();
        OVRCameraRig ovrCameraRig = GetComponent<OVRCameraRig>();
        if (ovrCameraRig)
        {
            ovrCameraRig.enabled = false;
        }
        //ovrManager.usePositionTracking = false;
        Vector3 position = gameObject.transform.position;





        //match height of current head;
        //_leftHeadPosition.y = position.y;
        //_rightHeadPosition.y = position.y;
        float playerRotation = 25;

        if (GameController.isMenuPointing)
        {
            rootTransform.position = (_leftHeadPosition + _rightHeadPosition)/2f;
        }
        else
        {
            if (_leftHanded)
            {
                //print("positionPlayer Left Handed");

                rootTransform.position = _leftHeadPosition;
                playerRotation = -Mathf.Abs(playerRotation);//cuz left
                                                            //rotate first
                gameObject.transform.Rotate(Vector3.up * playerRotation);
                //Debug 


                if (HandManager.currentLeftCustomHand)
                {
                    _leftHandPosition.y = HandManager.currentLeftCustomHand.transform.position.y;
                    HandManager.currentLeftCustomHand.transform.position = _leftHandPosition;

                }


                ///debug for testing
                //GameObject.Find("LeftHandAnchor").transform.position = _leftHandPosition;
                //GameObject.Find("RightHandAnchor").transform.position = _rightHandPosition * 100;
            }
            else
            {
                //print("positionPlayer Right Handed");

                this.transform.position = _rightHeadPosition;
                //rotate first
                gameObject.transform.Rotate(Vector3.up * playerRotation);
                //then position
                if (HandManager.currentRightCustomHand)
                {
                    _rightHandPosition.y = HandManager.currentRightCustomHand.transform.position.y;
                    HandManager.currentRightCustomHand.transform.position = _rightHandPosition;
                    HandManager.currentRightCustomHand.initialLocalPosition = HandManager.currentRightCustomHand.transform.localPosition;
                }

                ///debug for testing
                //GameObject.Find("LeftHandAnchor").transform.position = _leftHandPosition * 100;
                //GameObject.Find("RightHandAnchor").transform.position = _rightHandPosition;
            }
        }

        yield return null;
    }

    public void ResetPositions()
    {
       // headTransform.position = Vector3.zero;
        if (HandManager.currentRightCustomHand)
        {
            HandManager.currentRightCustomHand.transform.localPosition = Vector3.zero;
        }
        if (HandManager.currentLeftCustomHand)
        {
            HandManager.currentLeftCustomHand.transform.localPosition = Vector3.zero; 
        }

    }


    void Update()
    {
        if((GameController.isAtBat || popUpUI.activeSelf) && popUpUI)
        {
            if(OVRInput.GetDown(OVRInput.Button.Back) || Input.GetKeyDown("t"))
            {
                if(popUpUI.activeSelf)
                {
                    GameController.isAtBat = true;
                    GameController.isMenuPointing = false;
                    popUpUI.SetActive(false);
                    pointer.enabled = false;
                }
                else
                {
                    GameController.isAtBat = false;
                    GameController.isMenuPointing = true;
                    popUpUI.SetActive(true);
                    Vector3 flatHeadForward = HatManager.instance.helmet.transform.forward;
                    flatHeadForward = new Vector3(flatHeadForward.x, 0f, flatHeadForward.z).normalized;
                    popUpUI.transform.position = HatManager.instance.helmet.transform.position + (flatHeadForward*popUpUIDistance);
                  
                    popUpUI.transform.LookAt(HatManager.instance.helmet.transform);
                    pointer.enabled = true;
                    hitThings.instance.pointer.enabled = true;
                }
            }
        }
    }
    /*
	void OnValidate()
    {
        //for debugging only
        if (Application.isPlaying && Application.isEditor && gameObject.activeInHierarchy)
        {
           _leftHanded = isLeftHandedTest;
           StartCoroutine(positionPlayer());
        }
    }
    */

#endif

    void Start()
    {
#if GEAR_VR
        _leftHanded = false;

        //turn gear hand back on
       if (leftHand!= null)
            leftHand.gameObject.SetActive(true);
        if (rightHand != null)
            rightHand.gameObject.SetActive(true);

        //if (HandManager.dominantHand == HandManager.HandType.left)
        //{

        //    _leftHanded = true;
        //}
        _leftHandPosition = leftHandTransform.position;
        _rightHandPosition = rightHandTransform.position;
        _leftHeadPosition = leftHeadTransform.position;
        _rightHeadPosition = rightHeadTransform.position;

        StartCoroutine(positionPlayer());
        if (!GameController.isMenuPointing)
        {
            popUpUI = Instantiate(mobileUIPrefab, transform, false) as GameObject;
            popUpUI.SetActive(false);
        }
        pointer = FindObjectOfType<hitThings>().pointer;

#else
        //not gear hide hands and disable script
        if (leftHand!= null)
            leftHand.gameObject.SetActive(false);
        if (rightHand != null)
            rightHand.gameObject.SetActive(false);

        this.enabled = false;
   
#endif

    }


}
