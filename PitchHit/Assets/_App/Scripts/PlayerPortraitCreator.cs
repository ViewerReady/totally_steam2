﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class PlayerPortraitCreator : MonoBehaviour
{
    private Camera _camera
    {
        get
        {
            if (m_camera == null) m_camera = GetComponent<Camera>();
            return m_camera;
        }
    }
    private Camera m_camera;
    [SerializeField] FieldPlayerBodyControl bodyControl;

    //This can be used to generate portraits for everything in the list.
    //Set these in the inspector and let her roll.
    [SerializeField] List<PlayerInfo> portraitsToGenerate;
    [SerializeField] TeamInfo generationTeam;       //The generated player portraits will be wearing the jersey of THIS team.

    private static PlayerPortraitCreator _Instance;
    public static PlayerPortraitCreator Instance
    {
        get
        {
            if (!_Instance) CreateInstance();
            return _Instance;
        }
    }

    private static void CreateInstance()
    {
        PlayerPortraitCreator prefab = Resources.Load<PlayerPortraitCreator>("PlayerPortraits/PlayerPortraitRenderCam");
        _Instance = GameObject.Instantiate<PlayerPortraitCreator>(prefab);
        _Instance.transform.position = Vector3.down * 100;
    }

    public static Texture2D GetPlayerPortrait(PlayerInfo info, TeamInfo teamInfo)
    {
        string path = PlayerInfoToFileName(info);

        Texture2D tex = Resources.Load<Texture2D>(path);
        if (tex != null) return tex;

        Instance.Capture(info, teamInfo);

        tex = Resources.Load<Texture2D>(path);
        return tex;
    }

    public void Capture(string name, string number)
    {
        //nameText.text = name;
        //numberText.text = number;
        _camera.Render();
#if UNITY_EDITOR
        SaveTexture(_camera.targetTexture, PlayerInfoToFileName(name, number));
#endif
    }

    public void Capture(PlayerInfo info, TeamInfo teamInfo)
    {
        //Capture(info.jerseyName, info.jerseyNumber);
        bodyControl.DressAsPlayer(info, teamInfo);
        _camera.Render();
#if UNITY_EDITOR
        SaveTexture(_camera.targetTexture, PlayerInfoToFileName(info.jerseyName, info.jerseyNumber));
#endif
        _camera.targetTexture.Release();
    }

#if UNITY_EDITOR
    private void SaveTexture(RenderTexture tex, string assetPath)
    {
        byte[] bytes = toTexture2D(tex).EncodeToPNG();
        string path = Application.dataPath + "/_App/Resources/" + assetPath;
        System.IO.File.WriteAllBytes(path + ".png", bytes);


        UnityEditor.AssetDatabase.ImportAsset(path, UnityEditor.ImportAssetOptions.ForceUpdate);
        UnityEditor.AssetDatabase.Refresh();

        UnityEditor.TextureImporter importer = (UnityEditor.TextureImporter)UnityEditor.AssetImporter.GetAtPath(path);
        //importer.alphaIsTransparency = true;


        Debug.Log("Save Texture " + path);
    }
#endif

    private static string PlayerInfoToFileName(PlayerInfo info)
    {
        return "PlayerPortraits/" + info.jerseyName + info.jerseyNumber;
    }

    private static string PlayerInfoToFileName(string jerseyName, string jerseyNumber)
    {
        return "PlayerPortraits/" + jerseyName + jerseyNumber;
    }

    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGBA32, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    [ContextMenu("Generate ALL Portraits")]
    void GenerateAllPortraits()
    {
        foreach(PlayerInfo p in portraitsToGenerate)
        {
            Capture(p, generationTeam);
        }
    }
}
