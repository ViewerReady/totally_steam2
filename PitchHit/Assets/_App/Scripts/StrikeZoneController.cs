﻿using UnityEngine;
using System.Collections;

public class StrikeZoneController : MonoBehaviour
{
    //public int strikesThrown;
    //public int nonStrikesThrown;
    //public AIController theCatcher;

    public DefensePlayer catcherPrefab;
    private DefensePlayer theCatcher;


    public Transform ballSpot;
    public GameObject displayTextPrefab;
    //public GameObject[] strikeZones;
    public Renderer[] strikeZoneRenderers;
    public Renderer[] arrowRenderers;
    public Renderer[] colorRenderers;
    private Material[] initialColorRendererMaterials;
    private Color initialStrikeZoneColor;
    private Color initialArrowColor;
    private Color[] initialColors;
    public Material flashingColorMaterial;
    public bool useRadius;
    public Transform edgeOfRadius;
    public Vector2[] strikeZoneSizes;
    public Transform closestPosition;
    public AudioSource sound;
    private Vector3 initialLocalPosition;

    public int[] strikePoints;
    public int numberOfIntervals = 3;
    private int intervalsCompleted;

    public static StrikeZoneController instance;
    public static hittable currentPitchedBall;
    private static bool strikeZoneTouched;
    private float localRingRadiusSqr;

    public BlinkingRingController[] hallwayRings;
    private static int hallwayRingsPassed;


    private AdjustableBorder border;
    private static Renderer debugRenderer;
    private Vector3 previousBallPosition;
    private static int consecutiveStrikes;
    private static hittable previousStrikeBall;

    private Coroutine ballSpotting;
    private int longestrendererArray;

    void Awake()
    {
        instance = this;
    }


    // Use this for initialization
    void Start()
    {
        if (catcherPrefab)
        {
            theCatcher = GameObject.Instantiate(catcherPrefab);
            theCatcher.SetFieldPosition(DefenseFieldPosition.CATCHER, Vector3.back * 2);
            theCatcher.OnBallPosession += OnCatcherCatchBall;
            theCatcher.transform.position = Vector3.back * 2;
            theCatcher.transform.rotation = Quaternion.identity;
        }
        initialLocalPosition = transform.localPosition;
        border = GetComponent<AdjustableBorder>();
        debugRenderer = GetComponentInChildren<Renderer>();
        ballSpot.gameObject.SetActive(false);
        ballSpot.localPosition = Vector3.zero;
        //ballSpot.GetComponentInChildren<Renderer>().material.color = Color.blue;

        if (numberOfIntervals > 0)
        {
            transform.position = closestPosition.position;
        }

        SetStrikeZoneSize(0);
        //SetStrikeZoneColor(Color.red);

        if (useRadius && edgeOfRadius)
        {
            localRingRadiusSqr = edgeOfRadius.localPosition.sqrMagnitude;
        }

        initialColors = new Color[colorRenderers.Length];
        initialColorRendererMaterials = new Material[colorRenderers.Length];
        for (int m = 0; m < colorRenderers.Length; m++)
        {
            initialColorRendererMaterials[m] = colorRenderers[m].material;
            initialColors[m]    = initialColorRendererMaterials[m].color;
        }

        if (arrowRenderers.Length > 0)
        {
            initialArrowColor = arrowRenderers[0].material.GetColor("_EmissionColor");
        }

        if(strikeZoneRenderers.Length>0)
        {
            initialStrikeZoneColor = strikeZoneRenderers[0].material.color;
        }

        longestrendererArray = colorRenderers.Length;
        longestrendererArray = Mathf.Max(longestrendererArray, strikeZoneRenderers.Length);
        longestrendererArray = Mathf.Max(longestrendererArray, arrowRenderers.Length);
    }


    void OnCatcherCatchBall(DefensePlayer catcher)
    {
        hittable ball = catcher.DropBall();
        GameObject.Destroy(ball.gameObject);
        catcher.RunToTarget(Vector3.back * 2);
        catcher.LookAtTarget(Vector3.forward * 20);
    }

    public static void OnBallPitched(hittable ball)
    {
        if (instance)
        {
            instance.previousRingsPassed = -1;
            if (instance.theCatcher)
            {
                instance.theCatcher.CatchBallBehindHomePlate(ball);
            }
        }
        hallwayRingsPassed = 0;
        if (currentPitchedBall && currentPitchedBall != previousStrikeBall)
        {
            consecutiveStrikes = 0;
        }
        currentPitchedBall = ball;
        //currentPitchedBall.gameObject.tag = "ball_in_use";
        strikeZoneTouched = false;
        if (debugRenderer)
        {
            debugRenderer.material.color = Color.white;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (currentPitchedBall)
        {
            //Debug.Log("current pitched ball exists");
            if (currentPitchedBall.isScored == false)
            {
                //Debug.DrawRay(currentPitchedBall.transform.position, Vector3.up, Color.red);
                if (!currentPitchedBall.hitGround && hallwayRingsPassed < hallwayRings.Length)
                {
                    if (hallwayRings[hallwayRingsPassed].transform.InverseTransformPoint(currentPitchedBall.transform.position).z < 0)
                    {
                        Vector3 direction = currentPitchedBall.transform.position - previousBallPosition;
                        previousBallPosition = hallwayRings[hallwayRingsPassed].transform.InverseTransformPoint(previousBallPosition);
                        direction = hallwayRings[hallwayRingsPassed].transform.InverseTransformDirection(direction);
                        float approachAngle = Vector3.Angle(direction, -1f * Vector3.forward);
                        if (approachAngle > 90f)
                        {
                            //approachAngle = 180f - approachAngle;
                        }

                        Vector3 localPointOnPlane = previousBallPosition + ((previousBallPosition.z / Mathf.Cos(approachAngle * Mathf.Deg2Rad)) * direction.normalized);
                        Debug.Log("---------------------------judge hallway ring " + hallwayRingsPassed + " " + localPointOnPlane);
                        if (localPointOnPlane.sqrMagnitude < localRingRadiusSqr)
                        {
                            //Debug.DrawLine(hallwayRings[hallwayRingsPassed].position, hallwayRings[hallwayRingsPassed].TransformPoint(localPointOnPlane), Color.green, 20f);
                            hallwayRingsPassed++;

                        }
                        else
                        {
                            //currentPitchedBall = null;
                        }

                    }
                }

                SetRingsPassed(hallwayRingsPassed);

                if (currentPitchedBall && (currentPitchedBall.hitGround || (/*(hallwayRingsPassed>=hallwayRings.Length) &&*/ transform.InverseTransformPoint(currentPitchedBall.transform.position).z < 0)))
                {
                    strikeZoneTouched = false;
                    //it's gotten behind us.
                    if (!currentPitchedBall.hitGround)
                    {

                        Vector3 direction = currentPitchedBall.transform.position - previousBallPosition;
                        if (useRadius)
                        {
                            Debug.Log("++++++++++judge radius pitch.");
                            previousBallPosition = transform.InverseTransformPoint(previousBallPosition);
                            direction            = transform.InverseTransformDirection(direction);
                            float approachAngle  = Vector3.Angle(direction, -1f * Vector3.forward);
                            if (approachAngle > 90f)
                            {
                                Debug.Log("adjust angle.");
                                //approachAngle = 180f - approachAngle;
                            }

                            Vector3 localPointOnPlane = previousBallPosition + ((previousBallPosition.z / Mathf.Cos(approachAngle * Mathf.Deg2Rad)) * direction.normalized);
                            Debug.Log("local Point to judge strike? " + localPointOnPlane);
                            if (localPointOnPlane.sqrMagnitude < localRingRadiusSqr)
                            {
                                Debug.Log("it's a radius strike! "+Time.time);
                                strikeZoneTouched = true;
                                OnStrike();
                                //SetStrikeZoneColor(Color.green);
                            }

                        }
                        else
                        {

                            Ray ballRay = new Ray(previousBallPosition, direction);
                            RaycastHit[] hits = Physics.SphereCastAll(ballRay, currentPitchedBall.GetRadius(), direction.magnitude, LayerMask.GetMask("Default"));
                            foreach (RaycastHit h in hits)
                            {
                                if (h.collider.attachedRigidbody.gameObject == gameObject)
                                {
                                    strikeZoneTouched = true;
                                    OnStrike();
                                    //SetStrikeZoneColor(Color.green);

                                    break;
                                }
                            }
                        }
                    }

                    //so unless it's hit the trigger, it's a non-strike.
                    if ((!strikeZoneTouched) || currentPitchedBall.hitGround)
                    {
                        if (debugRenderer)
                        {
                            Debug.Log("make the strike red.");
                            debugRenderer.material.color = Color.red;
                        }

                        consecutiveStrikes = 0;
                        SetStrikeZoneSize(0);

                        if (lightBlinking != null)
                        {
                            StopCoroutine(lightBlinking);
                        }

                        lightBlinking = StartCoroutine(BlinkingLightsForMiss());

                        if (ScoreboardController.instance.scorable)
                        {
                            ScoreboardController.instance.ClearScore();
                        }

                    }

                    currentPitchedBall.isScored = true;
                    if (ballSpotting != null)
                    {
                        StopCoroutine(ballSpotting);
                    }
                    ballSpotting = StartCoroutine(DisplayBallSpot(currentPitchedBall.transform));
                    currentPitchedBall = null;
                }

                if (currentPitchedBall)
                {
                    previousBallPosition = currentPitchedBall.transform.position;
                }
            }
        }
    }

    private int previousRingsPassed = -1;
    private void SetRingsPassed(int passed)
    {
        //Debug.Log("set rings passed " + passed);
        if (passed != previousRingsPassed)
        {
            //Debug.Log("new number");
            for (int r = 0; r <= hallwayRings.Length; r++)
            {
                //if (hallwayRings[r])
                {
                    //Debug.Log("there's a ring");
                    if (passed >= r)
                    {
                        //Debug.Log("............................there's more");
                        if (r > previousRingsPassed && r>0)
                        {
                            //Debug.Log("//////////////////////////////////////////////////bit more");
                            hallwayRings[r-1].BlinkOnForTime(.5f, Color.green);
                        }
                    }
                    else
                    {
                        //hallwayRings[r].SetBlinkOff();
                    }
                    //hallwayRings[r].localRotation = passed > r ? Quaternion.AngleAxis(-25f, Vector3.right) : Quaternion.identity;
                    //foreach(Renderer rend in hallwayRings[r].GetComponentsInChildren<Renderer>())
                    //{
                    //    rend.material.color = passed > r ? Color.green : Color.black;
                    //}
                }
            }
            previousRingsPassed = passed;
        }
    }

    private void SetStrikeZoneSize(int zone)
    {
        if (useRadius)
        {
            zone = zone % strikeZoneSizes.Length;

            if(resizingRoutine!=null)
            {
                StopCoroutine(resizingRoutine);
            }

            resizingRoutine = StartCoroutine(ResizeStrikeZoneRoutine(strikeZoneSizes[zone]));
            //transform.localScale = strikeZoneSizes[zone].x * Vector3.one;
        }
        else
        {
            if (zone > -1 && zone < strikeZoneSizes.Length)
            {
                border.SetWidthHeight(strikeZoneSizes[zone].x, strikeZoneSizes[zone].y);
            }
            else if (zone >= strikeZoneSizes.Length && strikeZoneSizes.Length > 0)
            {
                border.SetWidthHeight(strikeZoneSizes[strikeZoneSizes.Length - 1].x, strikeZoneSizes[strikeZoneSizes.Length - 1].y);
            }
        }
        //for(int z=0; z < strikeZones.Length;z++)
        //{
        //    strikeZones[z].SetActive(z == zone);
        //}
    }

    private Coroutine resizingRoutine;
    IEnumerator  ResizeStrikeZoneRoutine(Vector2 targetSize)
    {
        Vector3 initialLocalScale = transform.localScale;
        //NEEDS ALTERNATIVE IF NOT USING RADIUS


        float duration = 1f;
        float timePassed = 0f;
        while (timePassed < duration)
        {
            timePassed += Time.deltaTime;
            if (useRadius)
            {
                transform.localScale = Vector3.Lerp(initialLocalScale, targetSize.x * Vector3.one,timePassed/duration);
            }
            else
            {

            }
            yield return null;
        }
    }

    private void SetStrikeZoneColor(Color c)
    {
        foreach (Renderer r in colorRenderers)
        {
            r.material.color = c;
        }
        //strikeZones[Mathf.Min(consecutiveStrikes, strikeZones.Length - 1)].GetComponent<Renderer>().material.color = c;
    }

    private Coroutine lightBlinking;
    private void OnStrike()
    {
        previousStrikeBall = currentPitchedBall;
        if (strikePoints.Length > 0)
        {
			
            Debug.Log("should award points");
            if (ScoreboardController.instance)
            {
                Debug.Log("there's an instance.");
                //ScoreboardController.instance.scorePopup(points, transform.position);

                if (ScoreboardController.instance.scorable)
                {
                    ScoreboardController.instance.AddScore(strikePoints[Mathf.Min(consecutiveStrikes, strikePoints.Length - 1)]);

                    Debug.Log("and it's scorable!");
                }
            }

        }

        //if (displayTextPrefab)
        //{
        //    ScoreDisplay temp = Instantiate(displayTextPrefab, transform.position + (Vector3.up * 2f) + (transform.forward * .25f), transform.rotation, null).GetComponent<ScoreDisplay>();
        //    temp.BeginDisplay("Strike " + (consecutiveStrikes + 1) + "!");
        //}

        sound.pitch = .9f + (((float)consecutiveStrikes - 1f) * .15f);
        sound.Play();
        consecutiveStrikes++;

        if (intervalsCompleted < numberOfIntervals)
        {
            if (consecutiveStrikes < 3)
            {
                SetStrikeZoneSize(consecutiveStrikes);
            }
            else
            {
                intervalsCompleted++;
                transform.localPosition = Vector3.Lerp(closestPosition.localPosition, initialLocalPosition, (float)intervalsCompleted / (float)numberOfIntervals);
                SetStrikeZoneSize(0);
                consecutiveStrikes = 0;
            }
        }
        else
        {
            SetStrikeZoneSize(consecutiveStrikes);
        }

        if(lightBlinking !=null)
        {
            StopCoroutine(lightBlinking);
        }

        lightBlinking = StartCoroutine(BlinkingLightsForStrike(consecutiveStrikes+1));
    }

    IEnumerator BlinkingLightsForStrike(int blinksToDo)
    {
        Debug.Log(" start blinking from! "+ gameObject.name);
        float maxTime = 2f;
        float timeToTake = (.5f * (maxTime / blinksToDo));
        while (blinksToDo > 0 && maxTime>0)
        {

            for (int r = 0; r < longestrendererArray; r++)
            {
                if (r < colorRenderers.Length)
                {
                    colorRenderers[r].material = flashingColorMaterial;
                    colorRenderers[r].material.color = Color.green;
                }

                if (r < strikeZoneRenderers.Length)
                {
                    strikeZoneRenderers[r].material.color = Color.green;

                }
                yield return null;
                if (r < arrowRenderers.Length)
                {
                    arrowRenderers[r].material.SetColor("_EmissionColor", Color.green);
                }
                yield return null;
            }

                Debug.Log("now wait "+timeToTake + " "+gameObject.activeSelf);
            yield return new WaitForSeconds(timeToTake);
            maxTime -= Mathf.Max(timeToTake,Time.deltaTime);
            Debug.Log("waited...");
           

            for( int r = 0; r < longestrendererArray; r++)
            {
                if (r < colorRenderers.Length)
                {
                    Debug.Log("set color back to " + initialColors[r]);

                    colorRenderers[r].material = initialColorRendererMaterials[r];
                    colorRenderers[r].material.color = initialColors[r];
                }
                if(r < strikeZoneRenderers.Length)
                {
                    strikeZoneRenderers[r].material.color = initialStrikeZoneColor;

                }
                if(r < arrowRenderers.Length)
                {
                    arrowRenderers[r].material.SetColor("_EmissionColor", initialArrowColor);
                }
            }

            yield return new WaitForSeconds(timeToTake);
            maxTime -= Mathf.Max(timeToTake, Time.deltaTime);
            blinksToDo--;
        }
        Debug.Log("finished blinking lights.");
    }

    IEnumerator BlinkingLightsForMiss()
    {
        for (int r = 0; r < longestrendererArray; r++)
        {
            if (r < colorRenderers.Length)
            {
                colorRenderers[r].material = flashingColorMaterial;
                colorRenderers[r].material.color = Color.red;
            }

            if (r < strikeZoneRenderers.Length)
            {
                strikeZoneRenderers[r].material.color = Color.red;
            }
            yield return null;
            if (r < arrowRenderers.Length)
            {
                arrowRenderers[r].material.SetColor("_EmissionColor", Color.red);
            }
            yield return null;
        }

        yield return new WaitForSeconds(.5f);
        for (int r = 0; r < longestrendererArray; r++)
        {
            
            if (r < colorRenderers.Length)
            {
                Debug.Log("set color back to " + initialColors[r]);
                colorRenderers[r].material = initialColorRendererMaterials[r];
                colorRenderers[r].material.color = initialColors[r];
            }
            if (r < strikeZoneRenderers.Length)
            {
                strikeZoneRenderers[r].material.color = initialStrikeZoneColor;

            }
            if (r < arrowRenderers.Length)
            {
                arrowRenderers[r].material.SetColor("_EmissionColor", initialArrowColor);
            }
        }
    }

    IEnumerator DisplayBallSpot(Transform ball)
    {
        Vector3 localPosition = transform.InverseTransformPoint(ball.position);

        if (localPosition.sqrMagnitude <= 9)
        {
            ballSpot.gameObject.SetActive(true);
            ballSpot.position = transform.TransformPoint(new Vector3(localPosition.x, localPosition.y, 0f));

            yield return new WaitForSeconds(3f);

            ballSpot.gameObject.SetActive(false);
        }
        else
        {
            ballSpot.gameObject.SetActive(false);
        }
    }
}
