﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;


public class UFOSpawnController : MonoBehaviour {
    public GameObject ufoPrefab;
    public float initialMaxUFOsAtOnce = 1f;
    public float finalMaxUFOsAtOnce = 20f;
    public float increaseMaxUFOsPerMinute = 2f;
    public float spawnRadius = 80f;
    public float attackRadius = 25f;
    public float spawnElevation = 5f;
    public float minimumPreferredElevation = 5f;
    public float maximumPreferredElevation = 15f;
    public float minimumApproachSpeed = 10f;
    public float maximumApproachSpeed = 15f;
    public float minTimeBetweenSpawns = 5f;
    public float maxTimeBetweenSpawns = 10f;
    public float minTimeToFire = 5f;
    public float maxTimeToFire = 10f;
    //private float shootSpeed = 30f;
    private float acceleration = 50f;
    private float tailgateDistance = 2f;
    private float farAheadToPlan = 5f;

    private Transform cannonTarget;
    public static int countUFO;
    private float startTime;
    Vector3 debugThing1;
    Vector3 debugThing2;
    // Use this for initialization
    void Start () {
        countUFO = 0;
#if RIFT
        cannonTarget = Camera.main.transform;
#elif STEAM_VR
        cannonTarget = Player.instance.hmdTransform;
#endif

        if (!cannonTarget)
        {
            cannonTarget = GetComponent<ballController>().teeController.transform;
        }

        debugThing1 = Vector3.forward * attackRadius;
        debugThing2 = Quaternion.AngleAxis(360 / 8, Vector3.up) * debugThing1;
        for (int x = 0; x <= 8; x++)
        {
            Debug.DrawLine(cannonTarget.position + debugThing1 + (Vector3.up * minimumPreferredElevation), cannonTarget.position + debugThing2 + (Vector3.up * minimumPreferredElevation), Color.red, 100f);
            Debug.DrawLine(cannonTarget.position + debugThing1 + (Vector3.up * maximumPreferredElevation), cannonTarget.position + debugThing2 + (Vector3.up * maximumPreferredElevation), Color.red, 100f);

            debugThing2 = debugThing1;
            debugThing1 = Quaternion.AngleAxis(360 / 8, Vector3.up) * debugThing1;
        }

        debugThing1 = Vector3.forward * spawnRadius;
        debugThing2 = Quaternion.AngleAxis(360 / 8, Vector3.up) * debugThing1;
        for (int x = 0; x <= 8; x++)
        {
            Debug.DrawLine(cannonTarget.position + debugThing1 + (Vector3.up * spawnElevation), cannonTarget.position + debugThing2 + (Vector3.up * spawnElevation), Color.green, 100f);
            Debug.DrawLine(cannonTarget.position + debugThing1 + (Vector3.up * spawnElevation), cannonTarget.position + debugThing2 + (Vector3.up * spawnElevation), Color.green, 100f);

            debugThing2 = debugThing1;
            debugThing1 = Quaternion.AngleAxis(360 / 8, Vector3.up) * debugThing1;
        }
    }

    public void BeginSpawning()
    {
        StartCoroutine(SpawnRoutine());
    }
    // Update is called once per frame


    private float finalMax;
    IEnumerator SpawnRoutine()
    {
        startTime = Time.timeSinceLevelLoad;
        SpaceCannon.ResetAllCannons();

        yield return new WaitForSeconds(3f);
        while (ScoreboardController.instance.scorable)
        {
            float currentMax = 0;
            //if (ScoreboardController.instance.scorable)
            //{
                currentMax = (float)initialMaxUFOsAtOnce + (((Time.timeSinceLevelLoad-startTime) / 60f) * (float)increaseMaxUFOsPerMinute);
            //}
            //else
            //{
            //    if(finalMax==0)
            //    {
            //        finalMax = (float)initialMaxUFOsAtOnce + (((Time.timeSinceLevelLoad-startTime) / 60f) * (float)increaseMaxUFOsPerMinute);
            //    }
            //    currentMax = finalMax;
            //}
            
            //Debug.Log(currentMax);
            if(currentMax>finalMaxUFOsAtOnce)
            {
                currentMax = finalMaxUFOsAtOnce;
            }
            if (countUFO < currentMax)
            {
                Vector3 spawnPosition = cannonTarget.position;
                spawnPosition += (Quaternion.AngleAxis(Random.value * 360f, Vector3.up) * Vector3.forward * spawnRadius);
                Ray lookDown = new Ray(spawnPosition + Vector3.up * 10f, -Vector3.up);
                RaycastHit hit;
                if (Physics.Raycast(lookDown, out hit, 100f, 1 << LayerMask.NameToLayer("Ground")))
                {
                    spawnPosition = hit.point + (Vector3.up * spawnElevation);
                }
                else
                {
                    spawnPosition += Vector3.up * spawnElevation;

                }

                EnemyUFO temp = (Instantiate(ufoPrefab, spawnPosition, Quaternion.identity, transform) as GameObject).GetComponent<EnemyUFO>();

                float tempPreferredElevation = (Random.value * (maximumPreferredElevation - minimumPreferredElevation)) + minimumPreferredElevation;
                temp.preferredElevation = tempPreferredElevation;
                //random prefered elevation

                temp.target = cannonTarget;
                //assign target

                temp.speed = (Random.value * (maximumApproachSpeed - minimumApproachSpeed)) + minimumApproachSpeed;
                //assign speed

                temp.attackRange = attackRadius;

                temp.timeToFire = Random.Range(minTimeToFire,maxTimeToFire);//should be assigned by spawner
                //temp.shootSpeed = shootSpeed;//should be assigned by spawner
                temp.acceleration = acceleration;//should be assigned by spawner
                temp.tailgateDistance = tailgateDistance;//should be assigned by spawner
                temp.farAheadToPlan = farAheadToPlan;//should be assigned by spawner
                
                countUFO++;
            }

            yield return new WaitForSeconds(Random.Range(minTimeBetweenSpawns,maxTimeBetweenSpawns));
        }
    }
}
