﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AnnouncerMinimap : MonoBehaviour {

    [SerializeField]
    private FullGameDirector gameDirector;
    [SerializeField] GameObject miniDefPlayerPrefab;
    [SerializeField] GameObject miniOffPlayerPrefab;
    [SerializeField] GameObject miniBallPrefab;

    [SerializeField] Transform minimapCener;

    Dictionary<OffensePlayer, Transform> offensePlayers;
    Dictionary<DefensePlayer, Transform> defensePlayers;
    KeyValuePair<Transform, Transform> batter;
    Transform miniBall;
    [SerializeField] Transform nameTagCanvas;
    [SerializeField] TextMeshProUGUI nameTagText;

    [SerializeField] Scrollbar battingOrderScrollBar;
    [SerializeField] List<TextMeshProUGUI> battingOrderNames;
    //private List<PlayerInfo> battingOrder;

    [SerializeField] GameObject batterCard;
    [SerializeField] TextMeshProUGUI batterNameText;
    [SerializeField] TextMeshProUGUI batterHeightText;
    [SerializeField] TextMeshProUGUI batterWeightText;
    [SerializeField] TextMeshProUGUI batterBioText;
    [SerializeField] TextMeshProUGUI batterHometownText;
    [SerializeField] TextMeshProUGUI batterPositionText;
    [SerializeField] Image batterProfileImage;
    [SerializeField] List<GameObject> itemsToShowOnFolderPopulate;
    [SerializeField] List<GameObject> itemsToHideOnFolderPopulate;

    private Transform trackedBallTransform;

    public void Awake()
    {
        offensePlayers = new Dictionary<OffensePlayer, Transform>();
        defensePlayers = new Dictionary<DefensePlayer, Transform>();
        batter = new KeyValuePair<Transform, Transform>();

        if (!gameDirector) gameDirector = FindObjectOfType<FullGameDirector>();
    }

    public void OnEnable()
    {
        FullGameDirector.OnShowBatter += CreateMiniBatter;
        FullGameDirector.OnHideBatter += DestroyMiniBatter;
        FieldMonitor.OnRegisterRunner += CreateMiniOffense;
        FieldMonitor.OnUnRegisterRunner += DestroyMiniOffense;
        FieldMonitor.OnRegisterDefensePlayer += CreateMiniDefense;
        FieldMonitor.OnUnRegisterDefensePlayer += DestroyMiniDefense;
        FieldMonitor.OnBallCreated += CreateMiniBall;
        FieldMonitor.OnBallDestroyed += DestroyMiniBall;
        FullGameDirector.OnBattingOrderUpdated += ScrollBattingOrder;
        FullGameDirector.OnChangeover += ChangeColorsOnChangeover;
        OffenseCoordinator.OnNewBatter += UpdateCurrentBatterInformation;
        gameDirector.OnFieldEvent += OnFieldEvent;
    }

    public void OnDisable()
    {
        FullGameDirector.OnShowBatter -= CreateMiniBatter;
        FullGameDirector.OnHideBatter -= DestroyMiniBatter;
        FieldMonitor.OnRegisterRunner -= CreateMiniOffense;
        FieldMonitor.OnUnRegisterRunner -= DestroyMiniOffense;
        FieldMonitor.OnRegisterDefensePlayer -= CreateMiniDefense;
        FieldMonitor.OnUnRegisterDefensePlayer -= DestroyMiniDefense;
        FieldMonitor.OnBallCreated -= CreateMiniBall;
        FieldMonitor.OnBallDestroyed -= DestroyMiniBall;
        FullGameDirector.OnBattingOrderUpdated -= ScrollBattingOrder;
        FullGameDirector.OnChangeover -= ChangeColorsOnChangeover;
        OffenseCoordinator.OnNewBatter -= UpdateCurrentBatterInformation;
        gameDirector.OnFieldEvent -= OnFieldEvent;
    }

    public void Update()
    {
        if(FieldMonitor.Instance == null)
        {
            //Debug.Log("FieldManagerInstance is null");
        }
        else
        {
            //Debug.Log("FieldManagerInstance is NOT null");
            if (miniBall != null && trackedBallTransform != null)
            {
                //Debug.Log("We made it");
                miniBall.localPosition = FieldMonitor.Instance.transform.InverseTransformPoint(trackedBallTransform.position);
            }
        }

        if(batter.Key != null)
        {
            batter.Value.localPosition = new Vector3(batter.Key.position.x, batter.Key.position.y + 4.5f, batter.Key.position.z);
        }

        bool ballPossesed = false;
        foreach (KeyValuePair<DefensePlayer, Transform> player in defensePlayers)
        {
            player.Value.localPosition = new Vector3(player.Key.position.x, player.Key.position.y + 4.5f, player.Key.position.z);
            if (player.Key.inPosessionOfBall && miniBall != null)
            {
                miniBall.localPosition = new Vector3(miniBall.localPosition.x, player.Key.position.y + 12f, miniBall.localPosition.z);
                nameTagCanvas.localPosition = new Vector3(miniBall.localPosition.x, player.Key.position.y + 16f, miniBall.localPosition.z);
                nameTagText.text = (player.Key.info.jerseyNumber + " - " + player.Key.info.jerseyName);
                nameTagCanvas.gameObject.SetActive(true);
                ballPossesed = true;
            }
        }
        if (!ballPossesed)
        {
            nameTagCanvas.gameObject.SetActive(false);
        }

        foreach (KeyValuePair<OffensePlayer, Transform> player in offensePlayers)
        {
            player.Value.localPosition = new Vector3(player.Key.position.x, player.Key.position.y + 4.5f, player.Key.position.z);
        } 
    }

    private void OnFieldEvent(hittable ball, FieldPlayType play)
    {
        switch (play)
        {
            case FieldPlayType.OPENNING:
                break;
            case FieldPlayType.SWITCH:
                DestroyAllOffense();
                break;
            case FieldPlayType.CLOSING:
                DestroyAllOffense();
                break;
        }
    }

    public void CreateMiniOffense(OffensePlayer fieldPlayer)
    {
        Transform miniTransform = Instantiate(miniOffPlayerPrefab, minimapCener).transform;
        offensePlayers.Add(fieldPlayer, miniTransform);
        miniTransform.GetComponent<MeshRenderer>().material.color = FullGameDirector.currentOffensiveTeam.awayColor;
    }

    public void CreateMiniBatter(Transform fieldPlayer, PlayerInfo playerInfo)
    {
        if(batter.Value != null)
        {
            Destroy(batter.Value.gameObject);
        }
        Transform miniTransform = Instantiate(miniOffPlayerPrefab, minimapCener).transform;
        batter = new KeyValuePair<Transform, Transform>(fieldPlayer, miniTransform);
        miniTransform.GetComponent<MeshRenderer>().material.color = FullGameDirector.currentOffensiveTeam.awayColor;
    }

    public void CreateMiniDefense(DefensePlayer fieldPlayer)
    {
        Transform miniTransform = Instantiate(miniDefPlayerPrefab, minimapCener).transform;
        defensePlayers.Add(fieldPlayer, miniTransform);
        miniTransform.GetComponent<MeshRenderer>().material.color = FullGameDirector.currentDefensiveTeam.awayColor;
    }

    public void ChangeColorsOnChangeover(int inning, bool topOfInning)
    {
        foreach (KeyValuePair<DefensePlayer, Transform> player in defensePlayers)
        {
            player.Value.GetComponent<MeshRenderer>().material.color = FullGameDirector.currentOffensiveTeam.awayColor;
        }
    }

    public void CreateMiniBall(Transform ballTransform)
    {
        DestroyMiniBall();
        trackedBallTransform = ballTransform;
        miniBall = Instantiate(miniBallPrefab, minimapCener).transform;
    }

    public void DestroyMiniOffense(OffensePlayer fieldPlayer)
    {
        if (offensePlayers.ContainsKey(fieldPlayer))
        {
            Destroy(offensePlayers[fieldPlayer].gameObject);
            offensePlayers.Remove(fieldPlayer);
        }
    }

    public void DestroyMiniBatter()
    {
        Destroy(batter.Value.gameObject);
        batter = new KeyValuePair<Transform, Transform>();
    }

    public void DestroyMiniDefense(DefensePlayer fieldPlayer)
    {
        Destroy(defensePlayers[fieldPlayer]);
        defensePlayers.Remove(fieldPlayer);
    }

    public void DestroyMiniBall()
    {
        if (miniBall) Destroy(miniBall.gameObject);
        trackedBallTransform = null;
    }

    public void DestroyAllOffense()
    {
        if(batter.Value != null) Destroy(batter.Value.gameObject);
        batter = new KeyValuePair<Transform, Transform>();

        foreach (KeyValuePair<OffensePlayer, Transform> player in offensePlayers)
        {
            Destroy(player.Value.gameObject);
        }
        offensePlayers = new Dictionary<OffensePlayer, Transform>();
    }

    public void UpdateCurrentBatterInformation(PlayerInfo batterInfo)
    {
        if (FullGameDirector.currentHumanPlayerRole == FullGameDirector.HumanPlayerRole.BROADCAST)
        {
            batterCard.SetActive(true);
            //foreach (GameObject go in itemsToShowOnFolderPopulate) go.SetActive(true);
            //foreach (GameObject go in itemsToHideOnFolderPopulate) go.SetActive(false);
            batterNameText.text = batterInfo.fullName;
            batterHeightText.text = batterInfo.height;
            batterWeightText.text = batterInfo.weight;
            batterBioText.text = batterInfo.bio;
            batterHometownText.text = batterInfo.hometown;
            batterPositionText.text = batterInfo.position.ToString();
            //Texture2D portrait = PlayerPortraitCreator.GetPlayerPortrait(batterInfo, FullGameDirector.currentOffensiveTeam);
            Texture2D portrait = batterInfo.playerPortrait;
            if (portrait == null) Debug.LogError("PORTRAIT WAS NULL");
            if(portrait != null) batterProfileImage.sprite = Sprite.Create(portrait, new Rect(0.0f, 0.0f, portrait.width, portrait.height), new Vector2(0.5f, 0.5f), 100.0f);
            Destroy(PlayerPortraitCreator.Instance.gameObject);
        }
    }

    public void ScrollBattingOrder(Queue<PlayerInfo> batOrder)
    {
        StartCoroutine(ScrollBattingOrderRoutine(batOrder));
    }

    IEnumerator ScrollBattingOrderRoutine(Queue<PlayerInfo> batOrder)
    {
        List<PlayerInfo> battingOrder = new List<PlayerInfo>();
        battingOrder.AddRange(batOrder);

        float timer = 0f;
        float timeToFinish = 1f;
        while(timer < timeToFinish)
        {
            timer += Time.deltaTime;
            battingOrderScrollBar.value = Mathf.Lerp(1f, 0.75f, (timer / timeToFinish));
            yield return null;
        }
        for (int i = 0; i < battingOrder.Count; i++)
        {
            if (i > battingOrderNames.Count - 1) continue;
            if (battingOrder[i] != null)
            {
                //Debug.Log("Setting " + battingOrderNames[i].gameObject.name + " to " + battingOrder[i]);
                battingOrderNames[i].text = battingOrder[i].jerseyNumber + " " + battingOrder[i].jerseyName;
            }
            else
            {
                battingOrderNames[i].text = "";
            }
        }
        battingOrderScrollBar.value = 1f;

        yield return null;
    }

    /*public void AddToBattingOrder(PlayerInfo info)
    {
        battingOrder.Add(info);
    }

    public void ClearBattingOrder()
    {
        battingOrder.Clear();
    }*/
}