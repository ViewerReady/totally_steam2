﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickMenu : MonoBehaviour {

    [SerializeField] GameObject projectionOnButton;
    [SerializeField] GameObject projectionOffButton;
    [SerializeField] GameObject cannonOnButton;
    [SerializeField] GameObject cannonOffButton;
    [SerializeField] GameObject rightHandButton;
    [SerializeField] GameObject leftHandButton;
    [SerializeField] GameObject armSwingButton;
    [SerializeField] GameObject smoothButton;

    void Start()
    {
        if (HumanPosessionManager.locomotionProjection_saved)
        {
            projectionOffButton.SetActive(false);
            projectionOnButton.SetActive(true);
            HumanPosessionManager.SetLocomtionProjection(true);
        }
        else
        {
            projectionOffButton.SetActive(true);
            projectionOnButton.SetActive(false);
            HumanPosessionManager.SetLocomtionProjection(false);
        }

        if (HandManager.useCannon)
        {
            cannonOffButton.SetActive(false);
            cannonOnButton.SetActive(true);
        }
        else
        {
            cannonOffButton.SetActive(true);
            cannonOnButton.SetActive(false);
        }

        if (HandManager.dominantHand == HandManager.HandType.right)
        {
            leftHandButton.SetActive(false);
            rightHandButton.SetActive(true);
        }
        else
        {
            leftHandButton.SetActive(true);
            rightHandButton.SetActive(false);
        }

        if (HumanPosessionManager.locomotionType_saved == HumanPosessionManager.LocomotionVariant.STANDARD)
        {
            smoothButton.SetActive(false);
            armSwingButton.SetActive(true);
        }
        else
        {
            smoothButton.SetActive(true);
            armSwingButton.SetActive(false);
        }
    }

    public void TurnProjectionOn()
    {
        projectionOffButton.SetActive(false);
        projectionOnButton.SetActive(true);
        HumanPosessionManager.SetLocomtionProjection(true);
    }

    public void TurnProjectionOff()
    {
        projectionOffButton.SetActive(true);
        projectionOnButton.SetActive(false);
        HumanPosessionManager.SetLocomtionProjection(false);
    }
}
