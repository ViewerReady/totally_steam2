﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "AvatarOpts", menuName = "Players/Avatar", order = 1)]
public class AvatarMeshFeature : ScriptableObject
{

    public AvatarMeshFeature()
    {

    }

    public Mesh[] meshes;
    public Color[] colors;

    public Mesh GetRandomMesh()
    {
        return meshes[UnityEngine.Random.Range(0, meshes.Length)];
    }

    public Color GetRandomColor()
    {
        return colors[UnityEngine.Random.Range(0, colors.Length)];

    }

}