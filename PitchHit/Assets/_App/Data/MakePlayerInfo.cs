﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

public class MakePlayerInfo
{
    [MenuItem("Assets/Create/PlayerInfo")]
    public static void CreateMyAsset()
    {
        PlayerInfo asset = ScriptableObject.CreateInstance<PlayerInfo>();

        AssetDatabase.CreateAsset(asset, "Assets/_App/Scriptable Objects/NewPlayerInfo.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif
