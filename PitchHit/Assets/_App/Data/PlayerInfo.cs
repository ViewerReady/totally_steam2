﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Roster", menuName = "Players/List", order = 1)]
public class PlayerInfo : ScriptableObject
{

    public PlayerInfo()
    {

    }

    public string fullName = "";
    public string jerseyName = "";
    public string jerseyNumber = "00";
    public string height = "0'0\"";
    public string weight = "0lbs.";
    public string bio = "lorum ipsum";
    public string hometown = "";
    public Texture2D playerPortrait;
    public Sprite jerseyNameNumber;
    public DefenseFieldPosition position = DefenseFieldPosition.PITCHER;
    public Mesh hairMesh;
    public Color hairColor;
    public bool isManly;
   // public Material skinMat;
    public Color skinColor;

    [Space(10)]
    //[Header("GENERAL")]
    //public float runSpeed = 6;

    [Header("DEFENSE")]
    public float runSpeedDefense = 2f;
    public float throwSpeed = 30;
    [Range(0, 1)]
    public float fumbleChance = 0;
    //public float maxCatchAltitude = 2.5f;
    public float chestHeight = 1.5f;
    public float armLength = 1;

    [Header("PITCHING")]
    public float pitchSpeed = 40;
    [Range(0, 1)]
    public float pitchStrikeChance = 1;

    [Header("OFFENSE")]
    public float runSpeedOffense = 7f;

    [Space(10)]
    [Header("BATTING")]
    [Range(0, 1)]
    public float missSwing = .5f;
    [Range(0, 1)]
    public float swingAtBalls = 0;
    [Range(0, 1)]
    public float swingAtStrikes = 1;
    [Range(0, 1)]
    public float homeRunChance = 0;
    [Range(0, 1)]
    public float foulChance = 0;
    public Vector2 powerRange = new Vector2(20, 30);
}