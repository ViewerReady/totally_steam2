﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "League", menuName = "Teams/List", order = 1)]
public class TeamInfo : ScriptableObject
{
    [Space(10)]
    [Header("BASIC INFO")]
    public string hometown = "Austin";
    public string teamName = "Viewers";
    public Sprite teamLogo;
    public Texture teamLogoTex;


    [Space(10)]
    [Header("OUTFITS")]
    public Texture jerseyHomeFront;
    public Texture jerseyHomeBack;
    public Texture jerseyAwayFront;
    public Texture jerseyAwayBack;

    [Space(10)]
    [Header("MATERIALS")]
    public Material jerseyMaterial;
    public Material helmetMaterial;

    //MARC: temp, should really be baked into the textures
    public Color homeColor = Color.white;
    public Color awayColor = Color.red;

    [Space(10)]
    [Header("DEFENSE")]
    public RosterAssignment[] defenseAssignments = new RosterAssignment[] {
        new RosterAssignment(DefenseFieldPosition.PITCHER, null) ,
        new RosterAssignment(DefenseFieldPosition.FIRST_BASE, null) ,
        new RosterAssignment(DefenseFieldPosition.SECOND_BASE, null) ,
        new RosterAssignment(DefenseFieldPosition.SHORTSTOP, null) ,
        new RosterAssignment(DefenseFieldPosition.THIRD_BASE, null) ,
        new RosterAssignment(DefenseFieldPosition.LEFT_FIELDER, null) ,
        new RosterAssignment(DefenseFieldPosition.CENTER_FIELDER, null) ,
        new RosterAssignment(DefenseFieldPosition.RIGHT_FIELDER, null) ,
        new RosterAssignment(DefenseFieldPosition.CATCHER, null) ,
    };

    [Space(10)]
    [Header("OFFENSE")]
    public PlayerInfo[] battingOrder;           //This is the batting order set in the inspector. The default order.


    public PlayerInfo GetRandomBatter()
    {
        int i = UnityEngine.Random.Range(0, battingOrder.Length);
        return battingOrder[i];
    }

    public PlayerInfo GetPlayerAssignedToPosition(DefenseFieldPosition position)
    {
        foreach (RosterAssignment assignment in defenseAssignments)
        {
            if (assignment.positionName == position)
            {
                if (assignment.player != null)
                {
                    return assignment.player;
                }
            }
        }
        return null;
    }


    
    [System.Serializable]
    public struct RosterAssignment 
    {
        public RosterAssignment(DefenseFieldPosition position, PlayerInfo player)
        {
            this.positionName = position;
            this.player = player;
        }
        public DefenseFieldPosition positionName;
        public PlayerInfo player;
    }
}