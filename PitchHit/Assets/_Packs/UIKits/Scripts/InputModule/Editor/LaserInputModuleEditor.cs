using UnityEngine;
using UnityEditor;

namespace VRUiKits.Utils
{
    [CustomEditor(typeof(LaserInputModule))]
    public class LaserInputModuleEditor : Editor
    {
        string[] uikitPlatformDefines = new string[] {
            "RIFT", "UIKIT_VIVE", "STEAM_VR"
        };

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            LaserInputModule _target = (LaserInputModule)target;
            if (_target.pointer == Pointer.Eye)
            {
                _target.gazeTimeInSeconds = EditorGUILayout.FloatField("Gaze Time In Seconds", _target.gazeTimeInSeconds);
                _target.delayTimeInSeconds = EditorGUILayout.FloatField("Delay Time In Seconds", _target.delayTimeInSeconds);
            }
            /*
            if (GUILayout.Button("Setup Input Module"))
            {
                switch (_target.platform)
                {
                    case VRPlatform.OCULUS:
                        SetPlatformCustomDefine("RIFT");
                        break;
                    case VRPlatform.VIVE:
                        SetPlatformCustomDefine("UIKIT_VIVE");
                        break;
                    case VRPlatform.VIVE_STEAM2:
                        SetPlatformCustomDefine("STEAM_VR");
                        break;
                    case VRPlatform.NONE:
                        SetPlatformCustomDefine("");
                        break;
                }
            }
            */

//#if STEAM_VR
            if (_target.setupHmdExplicitly)
            {
                _target.m_steamVrLeftHand = EditorGUILayout.ObjectField("SteamVR2 Left Hand", _target.m_steamVrLeftHand, typeof(Transform), true) as Transform;
                _target.m_steamVrRightHand = EditorGUILayout.ObjectField("SteamVR2 Right Hand", _target.m_steamVrRightHand, typeof(Transform), true) as Transform;
                _target.m_steamVrCenterEye = EditorGUILayout.ObjectField("SteamVR2 Center Eye", _target.m_steamVrCenterEye, typeof(Transform), true) as Transform;
            }
//#endif
        }

        void SetPlatformCustomDefine(string define)
        {
            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

            // Remove uikit platform defines
            foreach (string item in uikitPlatformDefines)
            {
                if (defines.Contains(item))
                {
                    if (defines.Contains((";" + item)))
                    {
                        defines = defines.Replace((";" + item), "");
                    }
                    else
                    {
                        defines = defines.Replace(item, "");
                    }
                }
            }

            if (define != "" && !defines.Contains(define))
            {
                defines += ";" + define;
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
        }
    }
}
