﻿using UnityEngine;
#if STEAM_VR
using Valve.VR;
using Valve.VR.InteractionSystem;
#endif

namespace VRUiKits.Utils
{
    public class UIKitPointer : MonoBehaviour
    {
        public GameObject gazePointer;
        public GameObject laser;
        Pointer? temp = null; // Used to detect if the pointer has changed in the input module.

        void Start()
        {
            if (null == LaserInputModule.instance)
            {
                return;
            }

            LaserInputModule.instance.SetController(this);
        }

#if RIFT
        OVRInput.Controller activeController;
#endif

#if RIFT || STEAM_VR
        // Change pointer when player clicks trigger on the other pointer.
        [SerializeField]
        bool allowAutoSwitchHand = false;
#endif
        void Update()
        {
            bool isEyePointer = LaserInputModule.instance.pointer == Pointer.Eye;
            if (temp != LaserInputModule.instance.pointer)
            {
                gazePointer.SetActive(isEyePointer);
                laser.SetActive(!isEyePointer);
#if RIFT || STEAM_VR
                SetPointer(LaserInputModule.instance.pointer);
#endif
                temp = LaserInputModule.instance.pointer;
            }

#if RIFT
            activeController = OVRInput.GetActiveController();
            /********* Oculus Go and Gear VR **********/
            if (activeController == OVRInput.Controller.LTrackedRemote
            && LaserInputModule.instance.pointer != Pointer.LeftHand)
            {
                SetPointer(Pointer.LeftHand);
            }
            else if (activeController == OVRInput.Controller.RTrackedRemote
            && LaserInputModule.instance.pointer != Pointer.RightHand)
            {
                SetPointer(Pointer.RightHand);
            }

            if (allowAutoSwitchHand)
            {
                /********* Oculus Rift **********/
                if (OVRInput.GetDown(LaserInputModule.instance.oculusTrigger, OVRInput.Controller.RTouch)
                && LaserInputModule.instance.pointer != Pointer.RightHand)
                {
                    SetPointer(Pointer.RightHand);
                }
                else if (OVRInput.GetDown(LaserInputModule.instance.oculusTrigger, OVRInput.Controller.LTouch)
                && LaserInputModule.instance.pointer != Pointer.LeftHand)
                {
                    SetPointer(Pointer.LeftHand);
                }
            }
#endif

#if STEAM_VR
            if (allowAutoSwitchHand)
            {
                if (LaserInputModule.instance.steamVrTriggerAction.GetStateDown(SteamVR_Input_Sources.RightHand)
                && LaserInputModule.instance.pointer != Pointer.RightHand)
                {
                    SetPointer(Pointer.RightHand);
                }
                else if (LaserInputModule.instance.steamVrTriggerAction.GetStateDown(SteamVR_Input_Sources.LeftHand)
                && LaserInputModule.instance.pointer != Pointer.LeftHand)
                {
                    SetPointer(Pointer.LeftHand);
                }
            }
#endif
        }

#if RIFT || STEAM_VR
        void SetPointer(Pointer targetPointer)
        {
            if (null != LaserInputModule.instance)
            {
                LaserInputModule.instance.pointer = targetPointer;
                transform.SetParent(LaserInputModule.instance.TargetControllerTransform);
                ResetTransform(transform);
            }
        }
#endif

        void ResetTransform(Transform trans)
        {
            trans.localPosition = Vector3.zero;
            trans.localRotation = Quaternion.identity;
            trans.localScale = Vector3.one;
        }

        void OnDestroy()
        {
            if (null != LaserInputModule.instance)
            {
                LaserInputModule.instance.RemoveController(this);
            }
        }
    }
}
