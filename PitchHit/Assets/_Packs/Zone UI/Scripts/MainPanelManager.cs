﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Michsky.UI.Zone
{
    public class MainPanelManager : MonoBehaviour
    {
        [Header("PANEL LIST")]
        public List<GameObject> panels = new List<GameObject>();

        private GameObject currentPanel;
        private GameObject nextPanel;

        [Header("TITLE")]
        public bool enableTitleAnim;
        public GameObject titleObject;
        public List<string> titles = new List<string>();
        private Animator titleAnimator;
        private TextMeshProUGUI title;
        private TextMeshProUGUI titleHelper;

        [Header("SETTINGS")]
        public int currentPanelIndex = 0;

        private Animator currentPanelAnimator;
        private Animator nextPanelAnimator;

        private CanvasGroup currentPanelCanvasGroup;
        private CanvasGroup nextPanelCanvasGroup;

        private Animator currentButtonAnimator;
        private Animator nextButtonAnimator;

        string panelFadeIn = "Panel In";
        string panelFadeOut = "Panel Out";
        string buttonFadeIn = "Hover to Pressed";
        string buttonFadeOut = "Pressed to Normal";

        void OnEnable()
        {
            if (panels.Count > 0)
            {
                currentPanel = panels[currentPanelIndex];
                //currentPanelAnimator = currentPanel.GetComponent<Animator>();
                //currentPanelAnimator.Play(panelFadeIn);
                currentPanelCanvasGroup = currentPanel.GetComponent<CanvasGroup>();
                currentPanelCanvasGroup.interactable = true;
                currentPanelCanvasGroup.blocksRaycasts = true;
                currentPanelCanvasGroup.alpha = 1;
            }

            if(enableTitleAnim == true)
            {
                titleAnimator = titleObject.GetComponent<Animator>();
                title = titleObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();
                titleHelper = titleObject.transform.Find("Helper").GetComponent<TextMeshProUGUI>();
            }
        }

        public void OpenFirstTab()
        {
            currentPanel = panels[currentPanelIndex];
            //currentPanelAnimator = currentPanel.GetComponent<Animator>();
            //currentPanelAnimator.Play(panelFadeIn);
            currentPanelCanvasGroup = currentPanel.GetComponent<CanvasGroup>();
            currentPanelCanvasGroup.interactable = true;
            currentPanelCanvasGroup.blocksRaycasts = true;
            currentPanelCanvasGroup.alpha = 1;

            if (enableTitleAnim == true)
            {
                title.text = titles[currentPanelIndex];
            }
        }

        public void PanelAnim(int newPanel)
        {
            if (newPanel != currentPanelIndex)
            {
                currentPanel = panels[currentPanelIndex];

                if (enableTitleAnim == true)
                {
                    titleHelper.text = titles[currentPanelIndex];
                    titleAnimator.Play("Switch");
                }

                currentPanelIndex = newPanel;
                nextPanel = panels[currentPanelIndex];

                //currentPanelAnimator = currentPanel.GetComponent<Animator>();
                //nextPanelAnimator = nextPanel.GetComponent<Animator>();
                currentPanelCanvasGroup = currentPanel.GetComponent<CanvasGroup>();
                nextPanelCanvasGroup = nextPanel.GetComponent<CanvasGroup>();

                //currentPanelAnimator.Play(panelFadeOut);
                //nextPanelAnimator.Play(panelFadeIn);
                currentPanelCanvasGroup.interactable = false;
                currentPanelCanvasGroup.blocksRaycasts = false;
                currentPanelCanvasGroup.alpha = 0;
                nextPanelCanvasGroup.interactable = true;
                nextPanelCanvasGroup.blocksRaycasts = true;
                nextPanelCanvasGroup.alpha = 1;

                if (enableTitleAnim == true)
                {
                    title.text = titles[currentPanelIndex];
                }

                /*if (buttons.Count > 0)
                {
                    currentButton = buttons[currentButtonlIndex];
                    currentButtonlIndex = newPanel;
                    nextButton = buttons[currentButtonlIndex];
                    currentButtonAnimator = currentButton.GetComponent<Animator>();
                    nextButtonAnimator = nextButton.GetComponent<Animator>();
                    currentButtonAnimator.Play(buttonFadeOut);
                    nextButtonAnimator.Play(buttonFadeIn);
                }*/
            }
        }

        public void NextPage()
        {
            if (currentPanelIndex <= panels.Count - 2)
            {
                if (enableTitleAnim == true)
                {
                    titleHelper.text = titles[currentPanelIndex];
                    titleAnimator.Play("Switch");
                }

                currentPanel = panels[currentPanelIndex];

                //currentPanelAnimator = currentPanel.GetComponent<Animator>();
                currentPanelCanvasGroup = currentPanel.GetComponent<CanvasGroup>();
                //currentButtonAnimator = currentButton.GetComponent<Animator>();

                //currentButtonAnimator.Play(buttonFadeOut);
                //currentPanelAnimator.Play(panelFadeOut);
                currentPanelCanvasGroup.interactable = false;
                currentPanelCanvasGroup.blocksRaycasts = false;
                currentPanelCanvasGroup.alpha = 0;

                currentPanelIndex += 1;
                nextPanel = panels[currentPanelIndex];

                //nextPanelAnimator = nextPanel.GetComponent<Animator>();
                nextPanelCanvasGroup = nextPanel.GetComponent<CanvasGroup>();
                //nextButtonAnimator = nextButton.GetComponent<Animator>();
                //nextPanelAnimator.Play(panelFadeIn);
                //nextButtonAnimator.Play(buttonFadeIn);
                nextPanelCanvasGroup.interactable = true;
                nextPanelCanvasGroup.blocksRaycasts = true;
                nextPanelCanvasGroup.alpha = 1;

                if (enableTitleAnim == true)
                {
                    title.text = titles[currentPanelIndex];
                }
            }
        }

        public void PrevPage()
        {
            if (currentPanelIndex >= 1)
            {
                if (enableTitleAnim == true)
                {
                    titleHelper.text = titles[currentPanelIndex];
                    titleAnimator.Play("Switch");
                }

                currentPanel = panels[currentPanelIndex];

                //currentPanelAnimator = currentPanel.GetComponent<Animator>();
                currentPanelCanvasGroup = currentPanel.GetComponent<CanvasGroup>();
                //currentButtonAnimator = currentButton.GetComponent<Animator>();

                //currentButtonAnimator.Play(buttonFadeOut);
                //currentPanelAnimator.Play(panelFadeOut);
                currentPanelCanvasGroup.interactable = false;
                currentPanelCanvasGroup.blocksRaycasts = false;
                currentPanelCanvasGroup.alpha = 0;

                currentPanelIndex -= 1;
                nextPanel = panels[currentPanelIndex];

                nextPanelCanvasGroup = nextPanel.GetComponent<CanvasGroup>();
                //nextPanelAnimator = nextPanel.GetComponent<Animator>();
                //nextButtonAnimator = nextButton.GetComponent<Animator>();
                //nextPanelAnimator.Play(panelFadeIn);
                //nextButtonAnimator.Play(buttonFadeIn);
                nextPanelCanvasGroup.interactable = true;
                nextPanelCanvasGroup.blocksRaycasts = true;
                nextPanelCanvasGroup.alpha = 1;

                if (enableTitleAnim == true)
                {
                    title.text = titles[currentPanelIndex];
                }
            }
        }
    }
}