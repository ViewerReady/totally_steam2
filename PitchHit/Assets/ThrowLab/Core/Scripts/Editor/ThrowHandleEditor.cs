﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace CloudFine.ThrowLab
{
    [CustomEditor(typeof(ThrowHandle))]
    public class ThrowHandleEditor : Editor
    {
        private SerializedProperty _configDict;
        private bool foldout = true;

        private void Awake()
        {
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            _configDict = serializedObject.FindProperty("_deviceConfigurations");
            _configDict.arraySize = System.Enum.GetValues(typeof(Device)).Length;
            foldout = EditorGUILayout.Foldout(foldout, "Device Configurations", true);
            if (foldout)
            {
                EditorGUI.indentLevel++;


                foreach (Device type in System.Enum.GetValues(typeof(Device)))
                {
                    EditorGUILayout.PropertyField(_configDict.GetArrayElementAtIndex((int)type), new GUIContent(type.ToString()), GUILayout.ExpandWidth(true));
                }
                EditorGUI.indentLevel--;

            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}