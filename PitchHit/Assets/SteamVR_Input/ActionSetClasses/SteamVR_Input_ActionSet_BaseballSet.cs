// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.1433
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public class SteamVR_Input_ActionSet_BaseballSet : Valve.VR.SteamVR_ActionSet
    {
        
        public virtual SteamVR_Action_Boolean ReverseButton
        {
            get
            {
                return SteamVR_Actions.baseballSet_ReverseButton;
            }
        }
        
        public virtual SteamVR_Action_Single TriggerDepth
        {
            get
            {
                return SteamVR_Actions.baseballSet_TriggerDepth;
            }
        }
        
        public virtual SteamVR_Action_Boolean MenuButton
        {
            get
            {
                return SteamVR_Actions.baseballSet_MenuButton;
            }
        }
        
        public virtual SteamVR_Action_Boolean GripPressed
        {
            get
            {
                return SteamVR_Actions.baseballSet_GripPressed;
            }
        }
        
        public virtual SteamVR_Action_Boolean TurnRight
        {
            get
            {
                return SteamVR_Actions.baseballSet_TurnRight;
            }
        }
        
        public virtual SteamVR_Action_Boolean TurnLeft
        {
            get
            {
                return SteamVR_Actions.baseballSet_TurnLeft;
            }
        }
        
        public virtual SteamVR_Action_Boolean GrabObject
        {
            get
            {
                return SteamVR_Actions.baseballSet_GrabObject;
            }
        }
        
        public virtual SteamVR_Action_Boolean RunButton
        {
            get
            {
                return SteamVR_Actions.baseballSet_RunButton;
            }
        }
        
        public virtual SteamVR_Action_Boolean ShootCannon
        {
            get
            {
                return SteamVR_Actions.baseballSet_ShootCannon;
            }
        }
        
        public virtual SteamVR_Action_Vector2 Move
        {
            get
            {
                return SteamVR_Actions.baseballSet_Move;
            }
        }
        
        public virtual SteamVR_Action_Boolean MoveActivate
        {
            get
            {
                return SteamVR_Actions.baseballSet_MoveActivate;
            }
        }
        
        public virtual SteamVR_Action_Vibration Haptic
        {
            get
            {
                return SteamVR_Actions.baseballSet_Haptic;
            }
        }
    }
}
