﻿using UnityEditor;
using UnityEngine;

namespace UnusedAssets.Editor
{
    class PrintAssetGUID : MonoBehaviour
    {
        [MenuItem("Tools/Unused Assets Dev/Print GUIDs", false, 104)]
        static void PrintGUIDs()
        {
            var selectedObjectGUIDs = Selection.assetGUIDs;

            foreach (var guid in selectedObjectGUIDs)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);

                string output = assetPath + " : " + guid;
                output += "\r\n";
                output += "\r\n";
                output += "\"" + guid + "\", //" + assetPath;
                output += "\r\n";
                output += "\r\n";

                Debug.Log(output);
            }
        }
    }
}
