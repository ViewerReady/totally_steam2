﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ArchimedsLab;


[RequireComponent(typeof(MeshFilter))]
//must accomodate for scale.
//must have a drag multiplier
//must have an angular drag multiplier
public class CustomFloatingEntity : GameEntity
{
    public Mesh buoyancyMesh;
    public float buoyancyMeshScale = 1f;//some objects may use a mesh that isn't naturally the right size.
   /* These 4 arrays are cache array, preventing some operations to be done each frame. */
    tri[] _triangles;
    tri[] worldBuffer;
    tri[] wetTris;
    tri[] dryTris;
    //These two variables will store the number of valid triangles in each cache arrays. They are different from array.Length !
    uint nbrWet, nbrDry;
    private Quaternion preForceRotation;

    private static float waterPeriod;
    private static float waterIntensity;
    private static float waterInverseWavelength;
    private float maxBuoyancyHeight;


    private Material sea;

    protected override void Awake()
    {
        if(buoyancyMesh == null)
        {
            buoyancyMesh = GetComponent<MeshFilter>().mesh;
        }
        base.Awake();
        sea = GameObject.Find("Sea").GetComponent<Renderer>().material;
        waterPeriod = sea.GetFloat("_Periode");
        waterIntensity = sea.GetFloat("_Intensity");
        waterInverseWavelength = sea.GetFloat("_WaveLengthInverse");

        maxBuoyancyHeight = (waterIntensity * .5f) + (transform.lossyScale.magnitude*.5f);

        rb.centerOfMass = centerOfMassOffset;
        //By default, this script will take the render mesh to compute forces. You can override it, using a simpler mesh.
        Mesh copyMesh = new Mesh();
        Mesh originalMesh = buoyancyMesh == null ? GetComponent<MeshFilter>().mesh : buoyancyMesh;
        copyMesh.vertices = originalMesh.vertices;
        copyMesh.triangles = originalMesh.triangles;
        copyMesh.uv = originalMesh.uv;
        copyMesh.normals = originalMesh.normals;
        copyMesh.colors = originalMesh.colors;
        copyMesh.tangents = originalMesh.tangents;
        //Debug.Log("try and scale by "+transform.lossyScale);
        Vector3[] tempVertices = copyMesh.vertices;
        //Debug.Log(tempVertices[0].x);

        for (int t = 0; t < copyMesh.vertices.Length; t++)
        {
            tempVertices[t] = Vector3.Scale(tempVertices[t],transform.lossyScale*buoyancyMeshScale);
        }
        copyMesh.vertices = tempVertices;
        copyMesh.RecalculateBounds();
        
        //Setting up the cache for the game. Here we use variables with a game-long lifetime.
        WaterCutter.CookCache(copyMesh, ref _triangles, ref worldBuffer, ref wetTris, ref dryTris);        
    }

    //I create my own delegate     
    private static float waterPeriodTime = 0f;
    private static float waterIntensityForDelegate = 0f;
    WaterSurface.GetWaterHeight myWaterHeight = delegate(Vector3 a)     
    {
        //return 0f;
        a *= waterInverseWavelength;
        return (Mathf.Cos(waterPeriodTime + a.x) + Mathf.Sin(waterPeriodTime + a.z)) * waterIntensityForDelegate;
    }; 

    //should be identical to myWaterHeight logic
    public static float GetWaterHeight(Vector3 a)
    {
        a *= waterInverseWavelength;
        return (Mathf.Cos(waterPeriodTime + a.x) + Mathf.Sin(waterPeriodTime + a.z)) * waterIntensityForDelegate;
    }
     
    protected override void FixedUpdate()
    {
        if(transform.position.y > maxBuoyancyHeight)
        {
            //Debug.DrawRay(transform.position + Vector3.forward*.5f, -Vector3.up *(transform.position.y-(waterIntensity *.5f)), Color.green);
            //Debug.DrawRay(transform.position + Vector3.forward,-Vector3.up* (transform.lossyScale.magnitude * .5f),Color.white);
            //Debug.DrawRay(transform.position, -Vector3.up*(transform.position.y-maxBuoyancyHeight), Color.red);
            return;
        }

        waterPeriodTime = Time.timeSinceLevelLoad * waterPeriod;
        waterIntensityForDelegate = waterIntensity * 0.25f;
        
        base.FixedUpdate();
        if (rb.IsSleeping())
            return;

        /* It's strongly advised to call these in the FixedUpdate function to prevent some weird behaviors */

        //This will prepare static cache, modifying vertices using rotation and position offset.
        WaterCutter.CookMesh(transform.position, transform.rotation, ref _triangles, ref worldBuffer);

        /*
            Now mesh ae reprensented in World position, we can split the mesh, and split tris that are partially submerged.
            Here I use a very simple water model, already implemented in the DLL.
            You can give your own. See the example in Examples/CustomWater.
        */
        WaterCutter.SplitMesh(worldBuffer, ref wetTris, ref dryTris, out nbrWet, out nbrDry, myWaterHeight);
        //This function will compute the forces depending on the triangles generated before.
        Archimeds.ComputeAllForces(wetTris, dryTris, nbrWet, nbrDry, speed, rb);
    }

    void Update()
    {
        //rb.drag = 10f;
    }
  

#if UNITY_EDITOR
    //Some visualizations for this buyoancy script.
    protected override void OnDrawGizmos()
    {
        /*
        base.OnDrawGizmos();

        if (!Application.isPlaying)
            return;

        Gizmos.color = Color.blue;
        for (uint i = 0; i < nbrWet; i++)
        {
            Gizmos.DrawLine(wetTris[i].a, wetTris[i].b);
            Gizmos.DrawLine(wetTris[i].b, wetTris[i].c);
            Gizmos.DrawLine(wetTris[i].a, wetTris[i].c);
        }

        Gizmos.color = Color.yellow;
        for (uint i = 0; i < nbrDry; i++)
        {
            Gizmos.DrawLine(dryTris[i].a, dryTris[i].b);
            Gizmos.DrawLine(dryTris[i].b, dryTris[i].c);
            Gizmos.DrawLine(dryTris[i].a, dryTris[i].c);
        }
        */
    }
#endif
}
